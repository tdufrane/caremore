﻿-- Populate initial data
insert into PostVisitSurveys (StartedOn, EndedOn) values ('5/1/2014', '7/27/2015')
insert into PostVisitSurveys (StartedOn, EndedOn) values ('8/1/2015', null)
go

insert into PostVisitSections (PostVisitSectionName) values ('')
insert into PostVisitSections (PostVisitSectionName) values ('Ease of Getting Care')
insert into PostVisitSections (PostVisitSectionName) values ('Waiting')
insert into PostVisitSections (PostVisitSectionName) values ('Office Staff, Medical Assistant, and/or Nurse')
insert into PostVisitSections (PostVisitSectionName) values ('Provider (Physician, Nurse Practioner, or Physician Assistant)')
go

insert into PostVisitQuestions (PostVisitSurveyId, PostVisitSectionId, QuestionOrder, PostVisitQuestionText) values (1, 0, 1,  'How convenient was it to make an appointment?')
insert into PostVisitQuestions (PostVisitSurveyId, PostVisitSectionId, QuestionOrder, PostVisitQuestionText) values (1, 0, 2,  'How would you rate our telephone service?')
insert into PostVisitQuestions (PostVisitSurveyId, PostVisitSectionId, QuestionOrder, PostVisitQuestionText) values (1, 0, 3,  'How well did the staff attend to your needs?')
insert into PostVisitQuestions (PostVisitSurveyId, PostVisitSectionId, QuestionOrder, PostVisitQuestionText) values (1, 0, 4,  'How would you rate the wait time in the office to see your doctor?')
insert into PostVisitQuestions (PostVisitSurveyId, PostVisitSectionId, QuestionOrder, PostVisitQuestionText) values (1, 0, 5,  'Did the physician answer your questions to your satisfaction?')
insert into PostVisitQuestions (PostVisitSurveyId, PostVisitSectionId, QuestionOrder, PostVisitQuestionText) values (1, 0, 6,  'Overall how would you rate this office?')
go
insert into PostVisitQuestions (PostVisitSurveyId, PostVisitSectionId, QuestionOrder, PostVisitQuestionText) values (2, 1, 1,  'Telephone is answered promptly when you call')
insert into PostVisitQuestions (PostVisitSurveyId, PostVisitSectionId, QuestionOrder, PostVisitQuestionText) values (2, 1, 2,  'Ability to get in to be seen')
insert into PostVisitQuestions (PostVisitSurveyId, PostVisitSectionId, QuestionOrder, PostVisitQuestionText) values (2, 2, 3,  'Time in waiting room ')
insert into PostVisitQuestions (PostVisitSurveyId, PostVisitSectionId, QuestionOrder, PostVisitQuestionText) values (2, 2, 4,  'Time in exam room')
insert into PostVisitQuestions (PostVisitSurveyId, PostVisitSectionId, QuestionOrder, PostVisitQuestionText) values (2, 3, 5,  'Friendly and courteous to you')
insert into PostVisitQuestions (PostVisitSurveyId, PostVisitSectionId, QuestionOrder, PostVisitQuestionText) values (2, 3, 6,  'Helpful')
insert into PostVisitQuestions (PostVisitSurveyId, PostVisitSectionId, QuestionOrder, PostVisitQuestionText) values (2, 4, 7,  'Listens to you')
insert into PostVisitQuestions (PostVisitSurveyId, PostVisitSectionId, QuestionOrder, PostVisitQuestionText) values (2, 4, 8,  'Has necessary medical information about you')
insert into PostVisitQuestions (PostVisitSurveyId, PostVisitSectionId, QuestionOrder, PostVisitQuestionText) values (2, 4, 9,  'Discusss your medications with you ')
insert into PostVisitQuestions (PostVisitSurveyId, PostVisitSectionId, QuestionOrder, PostVisitQuestionText) values (2, 4, 10, 'Aware of care you receive from other providers')
go


-- Insert PostVisits
set identity_insert PostVisits on
go
insert into PostVisits
([PostVisitId], [EnteredDate], [Doctor], [Name], [ContactInfo], [OkToContact], [Comments], [CreatedDate], [IpAddress])
select [PostVisitId], [EnteredDate], [Doctor], NULL, NULL, 0, [Comments], [CreatedDate], [IpAddress]
from PostVisit
go
set identity_insert PostVisits off
go

-- Insert existing PostVisit answers into new PostVisitAnswer table
insert into [PostVisitAnswers]
([PostVisitId], [PostVisitQuestionId], [PostVisitAnswerValue])
select
	PostVisitId,
	case Answer
		when 'AppointmentAnswer'  then 1
		when 'TelephoneAnswer'    then 2
		when 'AttendNeedsAnswer'  then 3
		when 'RateWaitTimeAnswer' then 4
		when 'SatisfactionAnswer' then 5
		else 6
	end as PostVisitQuestionId,
	PostVisitAnswer
from
(
	select PostVisitId, AppointmentAnswer, TelephoneAnswer, AttendNeedsAnswer, RateWaitTimeAnswer, SatisfactionAnswer, OverallRatingAnswer
	from PostVisit
) p
unpivot
(
	PostVisitAnswer for Answer in
	(AppointmentAnswer, TelephoneAnswer, AttendNeedsAnswer, RateWaitTimeAnswer, SatisfactionAnswer, OverallRatingAnswer)
) as up
