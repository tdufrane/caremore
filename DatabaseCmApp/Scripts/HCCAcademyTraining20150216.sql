﻿USE [CaremoreApplication] 
GO

--ProviderTrainingAccess

ALTER TABLE [dbo].[ProviderTrainingAccess](
	[RegistrationID] [int] NOT NULL,
	[ResourceID] [uniqueidentifier] NOT NULL,
	[AccessedOn] [datetime] NULL,
 CONSTRAINT [PK_ProviderTrainingAccess] PRIMARY KEY CLUSTERED 
(
	[RegistrationID] ASC,
	[ResourceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProviderTrainingAccess]  WITH CHECK ADD  CONSTRAINT [FK_ProviderTrainingAccess_ProviderTrainingRegistration] FOREIGN KEY([RegistrationID])
REFERENCES [dbo].[ProviderTrainingRegistration] ([RegistrationID])
GO

ALTER TABLE [dbo].[ProviderTrainingAccess] CHECK CONSTRAINT [FK_ProviderTrainingAccess_ProviderTrainingRegistration]
GO

ALTER TABLE [dbo].[ProviderTrainingAccess]  WITH CHECK ADD  CONSTRAINT [FK_ProviderTrainingAccess_ProviderTrainingResource] FOREIGN KEY([ResourceID])
REFERENCES [dbo].[ProviderTrainingResource] ([ResourceID])
GO

ALTER TABLE [dbo].[ProviderTrainingAccess] CHECK CONSTRAINT [FK_ProviderTrainingAccess_ProviderTrainingResource]
GO

--ProviderTrainingCategory

ALTER TABLE [dbo].[ProviderTrainingCategory](
	[CategoryID] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
 CONSTRAINT [PK_ProviderTrainingCategory] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO [dbo].[ProviderTrainingCategory] (Name) VALUES ('Provider Training')
GO

--ProviderTrainingRegistration

ALTER TABLE [dbo].[ProviderTrainingRegistration](
	[RegistrationID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](25) NULL,
	[LastName] [nvarchar](25) NULL,
	[Email] [nvarchar](100) NULL,
	[Address] [nvarchar](100) NULL,
	[City] [nvarchar](25) NULL,
	[State] [nvarchar](25) NULL,
	[ZipCode] [varchar](5) NULL,
	[Phone] [varchar](10) NULL,
	[JobTitle] [nvarchar](50) NULL,
	[ProviderName] [nvarchar](50) NULL,
	[Title] [nvarchar](10) NULL,
	[Organization] [nvarchar](50) NULL,
	[NPI] [varchar](10) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_ProviderTrainingRegistration] PRIMARY KEY CLUSTERED 
(
	[RegistrationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--ProviderTrainingResource

ALTER TABLE [dbo].[ProviderTrainingResource](
	[ResourceID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](256) NULL,
	[Active] [bit] NULL,
	[CategoryID] [smallint] NULL,
 CONSTRAINT [PK_ProviderTrainingResource] PRIMARY KEY CLUSTERED 
(
	[ResourceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProviderTrainingResource]  WITH CHECK ADD  CONSTRAINT [FK_ProviderTrainingResource_ProviderTrainingCategory] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[ProviderTrainingCategory] ([CategoryID])
GO

ALTER TABLE [dbo].[ProviderTrainingResource] CHECK CONSTRAINT [FK_ProviderTrainingResource_ProviderTrainingCategory]
GO

ALTER TABLE [dbo].[ProviderTrainingResource] ADD  CONSTRAINT [DF_ProviderTrainingResource_Active]  DEFAULT ((1)) FOR [Active]
GO

WHILE EXISTS (SELECT * FROM [dbo].[ProviderTrainingResource] WHERE [CategoryID] = NULL)
BEGIN
	UPDATE [dbo].[ProviderTrainingResource]
	SET [CategoryID] = (SELECT CategoryID FROM [dbo].[ProviderTrainingCategory] WHERE Name = 'Provider Training')
	WHERE [CategoryID] = NULL
END
GO

--HCCAcademyAccessReport
-- =============================================
-- Author:		Charles Hui
-- Create date: 2/16/2015
-- Description:	Reports provider access for HCC academy between selected dates
-- =============================================
CREATE PROCEDURE [dbo].[HCCAcademyAccessReport]
	@StartDate date = NULL,
	@EndDate date = NULL
	
AS

DECLARE @StopDate datetime
SET NOCOUNT ON

IF @EndDate IS NOT NULL
BEGIN
	SELECT @StopDate=DATEADD(day, 1, @EndDate)
END

SELECT	REG.FirstName, REG.LastName, REG.Email, REG.Address, REG.City, REG.State, REG.ZipCode, 
		REG.Phone, REG.JobTitle, REG.ProviderName, REG.Title, REG.Organization, REG.NPI, 
		RES.Name AS ResourceName, ACC.AccessedOn
FROM	ProviderTrainingAccess ACC INNER JOIN ProviderTrainingRegistration REG ON ACC.RegistrationID = REG.RegistrationID 
		INNER JOIN ProviderTrainingResource RES ON ACC.ResourceID = RES.ResourceID
WHERE	(@StartDate IS NULL OR @StartDate <= ACC.AccessedOn)
		AND (@EndDate IS NULL OR @StopDate > ACC.AccessedOn)
		AND (RES.Active = 1)
		AND (RES.CategoryID = (SELECT TOP 1 CAT.CategoryID FROM ProviderTrainingCategory CAT 
								WHERE CAT.Name = 'HCC Academy'))
ORDER BY ACC.AccessedOn
GO

--ProviderTrainingAccessReport
-- =============================================
-- Author:		Patricia Giurgiu
-- Create date: 5/19/14
-- Description:	Reports provider access to training resources between selected dates
-- Modify date: 2/16/14
-- Description: Update to show only Provider Training access info
-- =============================================
ALTER PROCEDURE [dbo].[ProviderTrainingAccessReport]
	@StartDate date = NULL,
	@EndDate date = NULL
	
AS

DECLARE @StopDate datetime
SET NOCOUNT ON

IF @EndDate IS NOT NULL
BEGIN
	SELECT @StopDate=DATEADD(day, 1, @EndDate)
END

SELECT	REG.FirstName, REG.LastName, REG.Email, REG.Address, REG.City, REG.State, REG.ZipCode, 
		REG.Phone, REG.JobTitle, REG.ProviderName, REG.Title, REG.Organization, REG.NPI, 
		RES.Name AS ResourceName, ACC.AccessedOn
FROM	ProviderTrainingAccess ACC INNER JOIN ProviderTrainingRegistration REG ON ACC.RegistrationID = REG.RegistrationID 
		INNER JOIN ProviderTrainingResource RES ON ACC.ResourceID = RES.ResourceID
WHERE	(@StartDate IS NULL OR @StartDate <= ACC.AccessedOn)
		AND (@EndDate IS NULL OR @StopDate > ACC.AccessedOn)
		AND (RES.Active = 1)
		AND (RES.CategoryID = (SELECT TOP 1 CAT.CategoryID FROM ProviderTrainingCategory CAT 
								WHERE CAT.Name = 'Provider Training'))
ORDER BY ACC.AccessedOn
GO

--ProviderTrainingRegistrationAdd
-- =============================================
-- Author:		Patricia Giurgiu
-- Create date: 5/16/2014
-- Description:	Add registration information for providers that sign up for duals training resources
-- Modify date: 2/16/2014
-- Description: Update Category info in ProviderTrainingCategory if category does not exist
-- =============================================
ALTER PROCEDURE [dbo].[ProviderTrainingRegistrationAdd]
@FirstName nvarchar(25),
@LastName nvarchar(25),
@Email nvarchar(100),
@Address nvarchar(100),
@City nvarchar(25),
@State nvarchar(25),
@ZipCode varchar(5),
@Phone varchar(10),
@JobTitle nvarchar(50),
@ProviderName nvarchar(50),
@Title nvarchar(10),
@Organization nvarchar(50),
@NPI varchar(10),
@ResourceID uniqueidentifier,
@ResourceName nvarchar(256),
@ResourceCategoryName varchar(100)

AS
DECLARE @RegistrationID int
set nocount on;

IF NOT EXISTS(SELECT TOP 1 CategoryID FROM ProviderTrainingCategory WHERE Name=@ResourceCategoryName)
BEGIN
	INSERT INTO ProviderTrainingCategory (Name)
	VALUES (@ResourceCategoryName)
END

IF NOT EXISTS(SELECT Top 1 ResourceID FROM ProviderTrainingResource WHERE ResourceID=@ResourceID)
BEGIN
	INSERT INTO ProviderTrainingResource (ResourceID, Name, Active, CategoryID)
	VALUES (@ResourceID, @ResourceName, 1, (SELECT CategoryID FROM ProviderTrainingCategory WHERE Name=@ResourceCategoryName))
END
	
INSERT INTO ProviderTrainingRegistration
			(FirstName,
			LastName,
			Email,
			[Address],
			City,
			[State],
			ZipCode,
			Phone,
			JobTitle,
			ProviderName,
			Title,
			Organization,
			NPI,
			CreatedOn,
			ModifiedOn)
		VALUES
			(@FirstName,
			@LastName, 
			@Email, 
			@Address, 
			@City, 
			@State, 
			@ZipCode, 
			@Phone, 
			@JobTitle, 
			@ProviderName, 
			@Title, 
			@Organization, 
			@NPI, 
			GetDate(),
			GetDate())

SET @RegistrationID=Scope_Identity()

INSERT INTO ProviderTrainingAccess	
			(RegistrationID,
			ResourceID,
			AccessedOn)
		VALUES
			(@RegistrationID,
			@ResourceID,
			GetDate())			

SELECT @RegistrationID as RegistrationID	
GO

--ProviderTrainingResourceAccessAdd
-- =============================================
-- Author:		Patricia Giurgiu
-- Create date: 5/19/2014
-- Description:	Record training resource access for providers 
-- that registered for duals, if access to resource not already recorded

-- Modify Date: 2/11/2015
-- Modified table to include foreign key CategoryID from table ProviderTrainingCategory
-- Query updated accordingly
-- =============================================
ALTER PROCEDURE [dbo].[ProviderTrainingResourceAccessAdd]
	@RegistrationID int,
	@ResourceID uniqueidentifier,
	@ResourceName nvarchar(256),
	@ResourceCategoryName varchar(100)
AS
BEGIN

	SET NOCOUNT ON;
	
	IF NOT EXISTS(SELECT TOP 1 CategoryID FROM ProviderTrainingCategory WHERE Name=@ResourceCategoryName)
	BEGIN
		INSERT INTO ProviderTrainingCategory (Name)
		VALUES (@ResourceCategoryName)
	END

	IF NOT EXISTS(SELECT TOP 1 ResourceID FROM ProviderTrainingResource WHERE ResourceID=@ResourceID)
	BEGIN
		INSERT INTO ProviderTrainingResource (ResourceID, Name, Active, CategoryID)
		VALUES (@ResourceID, @ResourceName, 1, (SELECT CategoryID FROM ProviderTrainingCategory WHERE Name=@ResourceCategoryName))
	END

	IF NOT EXISTS(SELECT TOP 1 RegistrationID FROM ProviderTrainingAccess WHERE RegistrationID=@RegistrationID AND ResourceID=@ResourceID)
	BEGIN
		INSERT INTO dbo.ProviderTrainingAccess(RegistrationID, ResourceID, AccessedOn)
		VALUES (@RegistrationID, @ResourceID, GetDate())
	END
END
GO
