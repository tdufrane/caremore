use CaremoreApplication
go

-- Fix ProviderTrainingCategory names if incorrect
if exists (select * from [ProviderTrainingCategory] where CategoryID = 1 and [Name] = 'HCC Academy')
BEGIN
	update ProviderTrainingCategory
	set [Name] = 'Provider Training'
	where CategoryID = 1

	update ProviderTrainingCategory
	set [Name] = 'HCC Academy'
	where CategoryID = 2

	update ProviderTrainingResource
	set CategoryID = 1
	where ResourceID = '6E4B410B-16DA-4AE6-A489-575C1C62FF16'

	update ProviderTrainingResource
	set CategoryID = 2
	where ResourceID in ('24314AF4-132E-4B58-B07D-7BC1C666FD7F', 'DD073BFA-0991-42F2-A937-BD07809F5652')
END


-- Add 'Provider Attestation' ProviderTrainingCategory
if not exists (select * from [ProviderTrainingCategory] where CategoryID = 3)
BEGIN
	set identity_insert ProviderTrainingCategory on

	insert into ProviderTrainingCategory
	([CategoryID], [Name])
	values
	(3, 'Provider Attestation')

	set identity_insert ProviderTrainingCategory off
END
go


-- Add 'ProviderTrainingRegistrations' table
if exists (select * from sysobjects where [type] = 'U' and [name] = 'ProviderTrainingRegistrations')
BEGIN
	drop table [ProviderTrainingRegistrations]
END

create table [ProviderTrainingRegistrations]
(
	[RegistrationID]       int NOT NULL  IDENTITY(1,1),
	[FirstName]            nvarchar(25)  NOT NULL,
	[LastName]             nvarchar(25)  NOT NULL,
	[Title]                nvarchar(10)      NULL,
	[Email]                nvarchar(100) NOT NULL,
	[Address]              nvarchar(100) NOT NULL,
	[City]                 nvarchar(25)  NOT NULL,
	[State]                varchar(20)   NOT NULL,
	[ZipCode]              varchar(5)    NOT NULL,
	[Phone]                varchar(12)   NOT NULL,
	[JobTitle]             nvarchar(50)      NULL,
	[ProviderName]         nvarchar(50)      NULL,
	[Organization]         nvarchar(50)      NULL,
	[Specialty]            varchar(50)       NULL,
	[PractitionerLicense]  varchar(20)       NULL,
	[NPI]                  varchar(10)       NULL,
	[CreatedOn]            datetime      NOT NULL,
	[ModifiedOn]           datetime          NULL,
	constraint [PK_ProviderTrainingRegistrations] primary key ([RegistrationID]),
	constraint [UK_ProviderTrainingRegistrations_Email] unique ([Email])
)
go


-- Copy old records to new table, exclude duplicates
set identity_insert ProviderTrainingRegistrations on

insert into [ProviderTrainingRegistrations]
(
	[RegistrationID],
	[FirstName],
	[LastName],
	[Title],
	[Email],
	[Address],
	[City],
	[State],
	[ZipCode],
	[Phone],
	[JobTitle],
	[ProviderName],
	[Organization],
	[NPI],
	[CreatedOn],
	[ModifiedOn]
)
select
	[RegistrationID],
	[FirstName],
	[LastName],
	case when [Title] = '' then null else [Title] end,
	[Email],
	[Address],
	[City],
	case when [State] = 'CA' then 'California' else [State] end,
	[ZipCode],
	case when len([Phone]) = 10 then left([Phone],3) + '-' + substring([Phone],4,3) + '-' + right([Phone],4) else [Phone] end,
	[JobTitle],
	[ProviderName],
	[Organization],
	[NPI] ,
	[CreatedOn],
	[ModifiedOn]
from [ProviderTrainingRegistration]
where [RegistrationID] in
(
	select max(RegistrationID) as MaxRegistrationID
	from [ProviderTrainingRegistration]
	group by [Email]
)
order by RegistrationID

set identity_insert [ProviderTrainingRegistration] off
go


-- Ensure all registration access for same email is to single id
update pta
set pta.RegistrationID = ptrs.RegistrationID
from [ProviderTrainingAccess] pta
join [ProviderTrainingRegistration] ptr on ptr.RegistrationID = pta.RegistrationID
join [ProviderTrainingRegistrations] ptrs on ptr.Email = ptrs.Email
where  pta.RegistrationID not in
(
	select RegistrationID
	from ProviderTrainingRegistrations
)
and ptrs.RegistrationID not in
(
	select RegistrationID
	from ProviderTrainingAccess
)
go


-- Remove registration access
delete [ProviderTrainingAccess]
where  [RegistrationID] not in
(
	select [RegistrationID]
	from ProviderTrainingRegistrations
)
go


-- Change the foreign key
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProviderTrainingAccess_ProviderTrainingRegistration]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProviderTrainingAccess]'))
ALTER TABLE [dbo].[ProviderTrainingAccess] DROP CONSTRAINT [FK_ProviderTrainingAccess_ProviderTrainingRegistration]
GO

ALTER TABLE [dbo].[ProviderTrainingAccess]  WITH CHECK ADD  CONSTRAINT [FK_ProviderTrainingAccess_ProviderTrainingRegistration] FOREIGN KEY([RegistrationID])
REFERENCES [dbo].[ProviderTrainingRegistrations] ([RegistrationID])
GO

ALTER TABLE [dbo].[ProviderTrainingAccess] CHECK CONSTRAINT [FK_ProviderTrainingAccess_ProviderTrainingRegistration]
GO
