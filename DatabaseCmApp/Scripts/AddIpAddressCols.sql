﻿use CaremoreApplication
go

alter table [BrokerRegistrations]
add [IPAddress]  varchar(15)  NULL;
go

alter table [Contacts]
add [IPAddress]  varchar(15)  NULL;
go

alter table [InformationRequests]
add [IPAddress]  varchar(15)  NULL;
go

alter table [ProviderTrainingRegistrations]
add [IPAddress]  varchar(15)  NULL;
go
