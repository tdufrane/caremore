﻿CREATE TABLE [dbo].[MemberOptIns]
(
	[MemberOptInId]     UNIQUEIDENTIFIER   NOT NULL,
	[FirstName]         NVARCHAR(25)       NOT NULL,
	[LastName]          NVARCHAR(25)       NOT NULL,
	[Email]             NVARCHAR(100)      NOT NULL,
	[ZipCode]           CHAR(5)            NOT NULL,
	[MemberId]          CHAR(9)            NOT NULL,
	[ProductPlanId]     CHAR(8)            NOT NULL,
	[ReceiveMaterials]  BIT                NOT NULL,
	[ReceiveEmails]     BIT                NOT NULL,
	[CreatedOn]         SMALLDATETIME      NOT NULL,
	[IpAddress]         VARCHAR(15)        NOT NULL,
	CONSTRAINT [PK_MemberOptIns] PRIMARY KEY ([MemberOptInId])
)
