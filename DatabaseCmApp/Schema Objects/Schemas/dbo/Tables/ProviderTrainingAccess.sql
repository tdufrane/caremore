CREATE TABLE [dbo].[ProviderTrainingAccess](
	[RegistrationID] [int] NOT NULL,
	[ResourceID] [uniqueidentifier] NOT NULL,
	[AccessedOn] [datetime] NULL,
 CONSTRAINT [PK_ProviderTrainingAccess] PRIMARY KEY CLUSTERED 
(
	[RegistrationID] ASC,
	[ResourceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProviderTrainingAccess]  WITH CHECK ADD  CONSTRAINT [FK_ProviderTrainingAccess_ProviderTrainingRegistration] FOREIGN KEY([RegistrationID])
REFERENCES [dbo].[ProviderTrainingRegistrations] ([RegistrationId])
GO

ALTER TABLE [dbo].[ProviderTrainingAccess] CHECK CONSTRAINT [FK_ProviderTrainingAccess_ProviderTrainingRegistration]
GO

ALTER TABLE [dbo].[ProviderTrainingAccess]  WITH CHECK ADD  CONSTRAINT [FK_ProviderTrainingAccess_ProviderTrainingResource] FOREIGN KEY([ResourceID])
REFERENCES [dbo].[ProviderTrainingResource] ([ResourceID])
GO

ALTER TABLE [dbo].[ProviderTrainingAccess] CHECK CONSTRAINT [FK_ProviderTrainingAccess_ProviderTrainingResource]
GO


