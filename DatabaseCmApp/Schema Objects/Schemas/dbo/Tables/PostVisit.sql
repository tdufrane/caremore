﻿CREATE TABLE [dbo].[PostVisits] (
    [PostVisitId]  INT            IDENTITY (1, 1) NOT NULL,
    [EnteredDate]  SMALLDATETIME  NOT NULL,
    [Doctor]       VARCHAR (50)   NOT NULL,
    [Name]         NVARCHAR (50)  NULL,
    [ContactInfo]  VARCHAR (50)   NULL,
    [OkToContact]  BIT            NOT NULL,
    [Comments]     VARCHAR (MAX)  NULL,
    [CreatedDate]  SMALLDATETIME  NOT NULL,
    [IpAddress]    VARCHAR (15)   NOT NULL,
    CONSTRAINT [PK_PostVisits] PRIMARY KEY CLUSTERED ([PostVisitId] ASC)
);
GO
CREATE NONCLUSTERED INDEX [IX_PostVisits_CreatedDate]
    ON [dbo].[PostVisits]([CreatedDate] ASC);
