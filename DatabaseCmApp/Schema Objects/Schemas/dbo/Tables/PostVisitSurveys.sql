﻿CREATE TABLE [dbo].[PostVisitSurveys]
(
	[PostVisitSurveyId]  SMALLINT       NOT NULL  IDENTITY,
	[StartedOn]          SMALLDATETIME  NOT NULL,
	[EndedOn]            SMALLDATETIME  NULL,
	CONSTRAINT [PK_PostVisitSurveys] PRIMARY KEY ([PostVisitSurveyId])
)
