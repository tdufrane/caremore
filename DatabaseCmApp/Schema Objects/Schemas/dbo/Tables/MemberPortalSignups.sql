﻿CREATE TABLE [dbo].[MemberPortalSignups] (
    [SignupId]    INT           IDENTITY (1, 1) NOT NULL,
    [SignupOn]    SMALLDATETIME NOT NULL,
    [IPAddress]   VARCHAR (12)  NOT NULL,
    [MemberId]    CHAR (9)      NOT NULL,
    [FirstName]   VARCHAR (15)  NOT NULL,
    [LastName]    VARCHAR (35)  NOT NULL,
    [DateOfBirth] SMALLDATETIME NOT NULL,
    [Email]       VARCHAR (100) NOT NULL,
    [Verified]    BIT           NOT NULL,
    CONSTRAINT [PK_MemberPortalSignups] PRIMARY KEY CLUSTERED ([SignupId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MemberPortalSignups_MemberId]
    ON [dbo].[MemberPortalSignups]([MemberId] ASC);

