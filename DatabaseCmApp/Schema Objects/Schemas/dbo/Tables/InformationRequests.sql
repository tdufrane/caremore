﻿CREATE TABLE [dbo].[InformationRequests]
(
	[InformationRequestId]    uniqueidentifier  NOT NULL,
	[CreatedOn]               smalldatetime     NOT NULL,
	[EventDate]               datetime          NULL,
	[EventName]               varchar(100)      NULL,
	[HearAboutEvent]          varchar(200)      NULL,
	[CurrentDoctor]           nvarchar(50)      NULL,
	[FirstName]               nvarchar(25)      NOT NULL,
	[LastName]                nvarchar(25)      NOT NULL,
	[Email]                   nvarchar(100)     NOT NULL,
	[Phone]                   varchar(14)       NULL,
	[Address]                 nvarchar(100)     NULL,
	[City]                    nvarchar(50)      NULL,
	[State]                   varchar(50)       NULL,
	[ZipCode]                 varchar(5)        NULL,
	[ReceiveMedicare]         bit               NOT NULL,
	[CurrentMember]           bit               NOT NULL,
	[CurrentPlan]             varchar(100)      NULL,
	[SpecialConditions]       nvarchar(200)     NULL,
	[Comments]                nvarchar(max)     NULL,
	[IPAddress]               varchar(15)       NULL,
	CONSTRAINT [PK_InformationRequests] PRIMARY KEY ([InformationRequestId])
)
