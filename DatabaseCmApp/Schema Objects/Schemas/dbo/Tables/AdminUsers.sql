﻿CREATE TABLE [dbo].[AdminUsers] (
    [Username]        NVARCHAR (50) NOT NULL,
    [Password]        NVARCHAR (20) NOT NULL,
    [AllowEnrollment] BIT           NOT NULL,
    [AllowBroker]     BIT           NOT NULL,
    [AllowLeads]      BIT           NOT NULL,
    [AllowSpecialty]  BIT           NOT NULL,
    [PasswordReset]   BIT           NOT NULL,
    CONSTRAINT [PK_AdminUsers] PRIMARY KEY CLUSTERED ([Username] ASC)
);

