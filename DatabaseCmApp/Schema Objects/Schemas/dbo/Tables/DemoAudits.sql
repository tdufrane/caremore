﻿CREATE TABLE [dbo].[DemoAudits] (
    [DemoAuditId] INT           IDENTITY (1, 1) NOT NULL,
    [AttemptedOn] SMALLDATETIME NOT NULL,
    [LoginName]   VARCHAR (20)  NOT NULL,
    [Successful]  BIT           NOT NULL,
    [IpAddress]   VARCHAR (15)  NOT NULL,
    CONSTRAINT [PK_DemoAudits] PRIMARY KEY CLUSTERED ([DemoAuditId] ASC)
);

