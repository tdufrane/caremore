﻿CREATE TABLE [dbo].[DemoSecurity] (
    [DemoSecurityId] SMALLINT     IDENTITY (1, 1) NOT NULL,
    [LoginName]      VARCHAR (20) NOT NULL,
    [Password]       VARCHAR (20) NOT NULL,
    CONSTRAINT [PK_DemoSecurity] PRIMARY KEY CLUSTERED ([DemoSecurityId] ASC)
);

