﻿CREATE TABLE [dbo].[PostVisitSections]
(
	[PostVisitSectionId]    SMALLINT      NOT NULL IDENTITY(0, 1),
	[PostVisitSectionName]  VARCHAR(100)  NOT NULL,
	CONSTRAINT [PK_PostVisitSections] PRIMARY KEY ([PostVisitSectionId])
)
