﻿CREATE TABLE [dbo].[Contacts]
(
	[ContactId]    uniqueidentifier  NOT NULL,
	[CreatedOn]    smalldatetime     NOT NULL,
	[LastName]     nvarchar(25)      NOT NULL,
	[FirstName]    nvarchar(25)      NOT NULL,
	[ZipCode]      varchar(5)        NOT NULL,
	[Phone]        varchar(14)       NOT NULL,
	CONSTRAINT [PK_Contacts] PRIMARY KEY ([ContactId])
)
