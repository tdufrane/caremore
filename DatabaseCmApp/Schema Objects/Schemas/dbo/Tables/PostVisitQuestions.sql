﻿CREATE TABLE [dbo].[PostVisitQuestions]
(
	[PostVisitQuestionId]    INT           NOT NULL  IDENTITY,
	[PostVisitSurveyId]      SMALLINT      NOT NULL,
	[PostVisitSectionId]     SMALLINT      NOT NULL,
	[PostVisitQuestionText]  VARCHAR(255)  NOT NULL,
	[QuestionOrder]          TINYINT       NOT NULL,
	CONSTRAINT [PK_PostVisitQuestions] PRIMARY KEY ([PostVisitQuestionId]),
	CONSTRAINT [FK_PostVisitQuestions_PostVisitSurvey] FOREIGN KEY ([PostVisitSurveyId]) REFERENCES [PostVisitSurveys]([PostVisitSurveyId]),
	CONSTRAINT [FK_PostVisitQuestions_PostVisitSection] FOREIGN KEY ([PostVisitSectionId]) REFERENCES [PostVisitSections]([PostVisitSectionId])
)
GO

CREATE INDEX [IX_PostVisitQuestions_PostVisitSurveyId] ON [dbo].[PostVisitQuestions] ([PostVisitSurveyId])
GO

CREATE INDEX [IX_PostVisitQuestions_PostVisitSectionId] ON [dbo].[PostVisitQuestions] ([PostVisitSectionId])
