﻿CREATE TABLE [dbo].[PostVisitAnswers]
(
	[PostVisitAnswerId]     INT          NOT NULL IDENTITY (1, 1),
	[PostVisitId]           INT          NOT NULL,
	[PostVisitQuestionId]   INT          NOT NULL,
	[PostVisitAnswerValue]  VARCHAR(10)  NOT NULL,
	CONSTRAINT [PK_PostVisitAnswers] PRIMARY KEY ([PostVisitAnswerId]),
	CONSTRAINT [FK_PostVisitAnswers_PostVisit] FOREIGN KEY ([PostVisitId]) REFERENCES [PostVisits]([PostVisitId]),
	CONSTRAINT [FK_PostVisitAnswers_PostVisitQuestion] FOREIGN KEY ([PostVisitQuestionId]) REFERENCES [PostVisitQuestions]([PostVisitQuestionId])
)
GO

CREATE INDEX [IX_PostVisitAnswers_PostVisitId] ON [dbo].[PostVisitAnswers] ([PostVisitId])
GO

CREATE INDEX [IX_PostVisitAnswers_PostVisitQuestionId] ON [dbo].[PostVisitAnswers] ([PostVisitQuestionId])
