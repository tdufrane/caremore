CREATE TABLE [dbo].[ProviderTrainingResource](
	[ResourceID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](256) NULL,
	[Active] [bit] NULL,
	[CategoryID] [smallint] NULL,
 CONSTRAINT [PK_ProviderTrainingResource] PRIMARY KEY CLUSTERED 
(
	[ResourceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProviderTrainingResource]  WITH CHECK ADD  CONSTRAINT [FK_ProviderTrainingResource_ProviderTrainingCategory] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[ProviderTrainingCategory] ([CategoryID])
GO

ALTER TABLE [dbo].[ProviderTrainingResource] CHECK CONSTRAINT [FK_ProviderTrainingResource_ProviderTrainingCategory]
GO

ALTER TABLE [dbo].[ProviderTrainingResource] ADD  CONSTRAINT [DF_ProviderTrainingResource_Active]  DEFAULT ((1)) FOR [Active]
GO