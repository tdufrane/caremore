CREATE TABLE [dbo].[ProviderTrainingRegistrations]
(
	[RegistrationID]       int NOT NULL  IDENTITY(1,1),
	[FirstName]            nvarchar(25)  NOT NULL,
	[LastName]             nvarchar(25)  NOT NULL,
	[Title]                nvarchar(10)      NULL,
	[Email]                nvarchar(100) NOT NULL,
	[Address]              nvarchar(100) NOT NULL,
	[City]                 nvarchar(25)  NOT NULL,
	[State]                varchar(20)   NOT NULL,
	[ZipCode]              varchar(5)    NOT NULL,
	[Phone]                varchar(12)   NOT NULL,
	[JobTitle]             nvarchar(50)      NULL,
	[ProviderName]         nvarchar(50)      NULL,
	[Organization]         nvarchar(50)      NULL,
	[Specialty]            varchar(50)       NULL,
	[PractitionerLicense]  varchar(20)       NULL,
	[NPI]                  varchar(10)       NULL,
	[CreatedOn]            datetime      NOT NULL,
	[ModifiedOn]           datetime          NULL,
	[IPAddress]            varchar(15)       NULL,
	CONSTRAINT [PK_ProviderTrainingRegistrations] PRIMARY KEY ([RegistrationID]),
	CONSTRAINT [UK_ProviderTrainingRegistrations_Email] UNIQUE ([Email])
)
GO
