﻿/*
	 Object:  MemberPortalSignupList

	Purpose:  List member portal signup attempts

	History:  05/08/2013 - C. Eckman.    Created
*/
create procedure MemberPortalSignupList
as
	set nocount on;

	select
		*
	from
		MemberPortalSignups