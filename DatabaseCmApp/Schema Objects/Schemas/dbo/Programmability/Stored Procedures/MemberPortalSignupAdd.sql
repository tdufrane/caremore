﻿
/*
	 Object:  MemberPortalSignupAdd

	Purpose:  Add a member portal signup attempt

	 Inputs:  @SignupOn   Date signup occured
	          @MemberId   Member's ID
	          @FirstName  Member's first name
	          @LastName   Member's last name
	          @IPAddress  IP Address attempt came from
	          @Verified   Whether member information was valid

	History:  05/08/2013 - C. Eckman.    Created
*/
create procedure MemberPortalSignupAdd
	@SignupOn     smalldatetime,
	@IPAddress    varchar(12),
	@MemberId     char(9),
	@FirstName    varchar(15),
	@LastName     varchar(35),
	@DateOfBirth  smalldatetime,
	@Email        varchar(100),
	@Verified     bit
as
	set nocount on;

	insert into MemberPortalSignups
	(
		SignupOn,
		IPAddress,
		MemberId,
		FirstName,
		LastName,
		DateOfBirth,
		Email,
		Verified
	)
	values
	(
		@SignupOn,
		@IPAddress,
		@MemberId,
		@FirstName,
		@LastName,
		@DateOfBirth,
		@Email,
		@Verified
	)