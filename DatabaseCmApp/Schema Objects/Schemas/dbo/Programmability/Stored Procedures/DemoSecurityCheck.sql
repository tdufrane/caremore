﻿create procedure DemoSecurityCheck
	@AttemptedOn smalldatetime,
	@LoginName   varchar(20),
	@Password    varchar(20),
	@IpAddress   varchar(15),
	@Success     bit output
as
	declare @Match int

	set nocount on;

	select @Match = count(DemoSecurityId)
	from DemoSecurity
	where LoginName  = @LoginName
	and   [Password] = @Password

	if (@Match = 1)
		select @Success = 1
	else
		select @Success = 0

	insert into DemoAudits
		(AttemptedOn, LoginName, Successful, IpAddress)
	values
		(@AttemptedOn, @LoginName, @Success, @IpAddress)