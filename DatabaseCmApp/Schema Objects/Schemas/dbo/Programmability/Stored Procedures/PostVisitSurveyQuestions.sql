﻿/*
	 Object:  PostVisitSurveyActive

	Purpose:  Get current post visit survey sections/questions

	History:  08/14/2015 - C. Eckman.    Created
*/
create procedure [dbo].[PostVisitSurveyQuestions]
	@PostVisitSurveyId int
as
	select
		pvc.PostVisitSectionName,
		pvq.PostVisitQuestionId,
		pvq.PostVisitQuestionText
	from
		PostVisitSurveys pvs
		join PostVisitQuestions pvq on
			pvs.PostVisitSurveyId = pvq.PostVisitSurveyId
		join PostVisitSections pvc on
			pvq.PostVisitSectionId = pvc.PostVisitSectionId
	where
		(pvs.PostVisitSurveyId = @PostVisitSurveyId)
	order by
		pvq.QuestionOrder
