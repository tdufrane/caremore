﻿/*
	 Object:  PostVisitSurveyActive

	Purpose:  Get current post visit survey id

	History:  07/28/2015 - C. Eckman.    Created
*/
create procedure [dbo].[PostVisitSurveyActive]
as
	declare @Now smalldatetime
	select @Now = getdate()

	select
		pvs.PostVisitSurveyId
	from
		PostVisitSurveys pvs
	where
		(@Now between pvs.StartedOn and isnull(pvs.EndedOn, @Now))
