-- =============================================
-- Author:      Patricia Giurgiu
-- Create date: 5/19/2014
-- Description: Record training resource access for providers 
-- that registered for duals, if access to resource not already recorded

-- Modify Date: 2/11/2015
-- Modified table to include foreign key CategoryID from table ProviderTrainingCategory
-- Query updated accordingly
-- =============================================
CREATE PROCEDURE [dbo].[ProviderTrainingResourceAccessAdd]
	@RegistrationID int,
	@ResourceID uniqueidentifier,
	@ResourceName nvarchar(256),
	@ResourceCategoryName varchar(100)
AS
	SET NOCOUNT ON;
	
	IF NOT EXISTS(SELECT TOP 1 CategoryID FROM ProviderTrainingCategory WHERE Name = @ResourceCategoryName)
	BEGIN
		INSERT INTO ProviderTrainingCategory (Name)
		VALUES (@ResourceCategoryName)
	END

	IF NOT EXISTS(SELECT TOP 1 ResourceID FROM ProviderTrainingResource WHERE ResourceID = @ResourceID)
	BEGIN
		INSERT INTO ProviderTrainingResource (ResourceID, Name, Active, CategoryID)
		VALUES (@ResourceID, @ResourceName, 1, (SELECT CategoryID FROM ProviderTrainingCategory WHERE Name = @ResourceCategoryName))
	END

	IF EXISTS(SELECT TOP 1 RegistrationID FROM ProviderTrainingAccess WHERE RegistrationID = @RegistrationID AND ResourceID = @ResourceID)
	BEGIN
		UPDATE dbo.ProviderTrainingAccess
		SET AccessedOn = GETDATE()
		WHERE (RegistrationID = @RegistrationID AND ResourceID = @ResourceID)
	END

	ELSE
	BEGIN
		INSERT INTO dbo.ProviderTrainingAccess(RegistrationID, ResourceID, AccessedOn)
		VALUES (@RegistrationID, @ResourceID, GETDATE())
	END
GO
