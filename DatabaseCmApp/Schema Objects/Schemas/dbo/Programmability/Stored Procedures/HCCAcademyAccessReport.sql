﻿-- =============================================
-- Author:		Charles Hui
-- Create date: 2/16/2015
-- Description:	Reports provider access for HCC academy between selected dates
-- =============================================
CREATE PROCEDURE [dbo].[HCCAcademyAccessReport]
	@StartDate date = NULL,
	@EndDate date = NULL
	
AS

DECLARE @StopDate datetime
SET NOCOUNT ON

IF @EndDate IS NOT NULL
BEGIN
	SELECT @StopDate=DATEADD(day, 1, @EndDate)
END

SELECT	REG.FirstName, REG.LastName, REG.Email, REG.Address, REG.City, REG.State, REG.ZipCode, 
		REG.Phone, REG.JobTitle, REG.ProviderName, REG.Title, REG.Organization, REG.NPI, 
		RES.Name AS ResourceName, ACC.AccessedOn
FROM	ProviderTrainingAccess ACC INNER JOIN ProviderTrainingRegistration REG ON ACC.RegistrationID = REG.RegistrationID 
		INNER JOIN ProviderTrainingResource RES ON ACC.ResourceID = RES.ResourceID
WHERE	(@StartDate IS NULL OR @StartDate <= ACC.AccessedOn)
		AND (@EndDate IS NULL OR @StopDate > ACC.AccessedOn)
		AND (RES.Active = 1)
		AND (RES.CategoryID = 2)
ORDER BY ACC.AccessedOn

GO