-- =============================================
-- Author:		Patricia Giurgiu
-- Create date: 5/19/14
-- Description:	Reports provider access to training resources between selected dates
-- =============================================
CREATE PROCEDURE [dbo].[ProviderTrainingAccessReport]
	@StartDate date = NULL,
	@EndDate date = NULL,
	@CategoryID int = NULL

AS

DECLARE @StopDate datetime
SET NOCOUNT ON

IF @EndDate IS NOT NULL
BEGIN
	SELECT @StopDate = DATEADD(day, 1, @EndDate)
END

SELECT REG.*,
       RES.Name AS ResourceName,
       ACC.AccessedOn
FROM   ProviderTrainingAccess ACC
       INNER JOIN ProviderTrainingRegistrations REG ON ACC.RegistrationID = REG.RegistrationID 
       INNER JOIN ProviderTrainingResource RES ON ACC.ResourceID = RES.ResourceID
WHERE  (ISNULL(@StartDate, ACC.AccessedOn) <= ACC.AccessedOn)
AND    (@EndDate IS NULL OR @StopDate > ACC.AccessedOn)
AND    (RES.Active = 1)
AND    (RES.CategoryID = ISNULL(@CategoryID, 0))
ORDER BY ACC.AccessedOn

GO


