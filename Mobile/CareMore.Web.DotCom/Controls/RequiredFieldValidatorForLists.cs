﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CareMore.Web.DotCom.Controls
{
	public class RequiredFieldValidatorForCheckBoxLists : BaseValidator
	{
		#region Private variables and constants

		private const string SCRIPTBLOCK = "RFV4CL";

		#endregion

		#region Overrides

		protected override bool ControlPropertiesValid()
		{
			Control ctrl = FindControl(ControlToValidate);
			if (ctrl != null)
			{
				CheckBoxList _listctrl = (CheckBoxList)ctrl;
				return (_listctrl != null);
			}
			else
			{
				return false;
			}
		}

		protected override bool EvaluateIsValid()
		{
			return EvaluateIsChecked();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (EnableClientScript)
			{
				this.ClientScript();
			}
		}

		#endregion

		#region Private methods

		private void ClientScript()
		{
			StringBuilder script = new StringBuilder();
			script.AppendLine("<script language=\"javascript\">");
			script.AppendLine("function cb_verify(sender) {");
			script.AppendLine("\tvar val = document.getElementById(document.getElementById(sender.id).controltovalidate);");
			script.AppendLine("\tvar col = val.getElementsByTagName(\"*\");");
			script.AppendLine();
			script.AppendLine("\tif ( col != null ) {");
			script.AppendLine("\t\tfor ( i = 0; i < col.length; i++ ) {");
			script.AppendLine("\t\t\tif (col.item(i).tagName == \"INPUT\") {");
			script.AppendLine("\t\t\t\tif (col.item(i).checked) {");
			script.AppendLine("\t\t\t\t\treturn true;");
			script.AppendLine("\t\t\t\t}");
			script.AppendLine("\t\t\t}");
			script.AppendLine("\t\t}");
			script.AppendLine("\t\treturn false;");
			script.AppendLine("\t}");
			script.AppendLine("}");
			script.AppendLine("</script>");

			Page.ClientScript.RegisterClientScriptBlock(GetType(), SCRIPTBLOCK, script.ToString());
			Page.ClientScript.RegisterExpandoAttribute(ClientID, "evaluationfunction", "cb_verify");
		}

		private bool EvaluateIsChecked()
		{
			CheckBoxList _cbl = ((CheckBoxList)FindControl(ControlToValidate));
			foreach (ListItem li in _cbl.Items)
			{
				if (li.Selected)
				{
					return true;
				}
			}
			return false;
		}

		#endregion
	}
}
