﻿using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;

namespace CareMore.Web.DotCom.Models.Events
{


	public partial interface IEventType
	{

		string Title
		{
			get;
			set;
		}
	}

	[Template(CareMore.Web.DotCom.Models.Events.EventType.TemplateID)]
	public partial class EventType : StandardTemplate, CareMore.Web.DotCom.Models.Events.IEventType
	{

		#region Members
		public const string TemplateID = "{D1881052-05E5-4359-9DD7-2319C964AD19}";

		public const string Title_FID = "{37021D80-C553-4F0F-BC2D-C802D3C0139E}";

		private string _title;
		#endregion

		#region Constructors
		public EventType(Item innerItem) :
			base(innerItem)
		{
		}
		#endregion

		#region Properties
		[Field(CareMore.Web.DotCom.Models.Events.EventType.Title_FID)]
		public virtual string Title
		{
			get
			{
				if (_title == null)
				{
					_title = this.GetString(CareMore.Web.DotCom.Models.Events.EventType.Title_FID);
				}
				return _title;
			}
			set
			{
				_title = null;
				this.SetString(CareMore.Web.DotCom.Models.Events.EventType.Title_FID, value);
			}
		}
		#endregion
	}
}