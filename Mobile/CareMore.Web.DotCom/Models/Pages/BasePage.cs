﻿using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;

namespace CareMore.Web.DotCom.Models.Pages
{


	public partial interface IBasePage
	{

		bool? ExcludeFromNavigation
		{
			get;
			set;
		}

		string PageTitle
		{
			get;
			set;
		}

		bool? ShowInSiteMap
		{
			get;
			set;
		}

		string NavigationTitle
		{
			get;
			set;
		}

		bool? SSL
		{
			get;
			set;
		}
	}

	[Template(CareMore.Web.DotCom.Models.Pages.BasePage.TemplateID)]
	public partial class BasePage : StandardTemplate, CareMore.Web.DotCom.Models.Pages.IBasePage
	{

		#region Members
		public const string TemplateID = "{DEF7754D-8F96-4EC3-9FA7-830B07680915}";

		public const string SSL_FID = "{23B0EE59-45E5-4589-B68F-53BDB3784F95}";

		public const string ExcludeFromNavigation_FID = "{03E98EE2-4BDB-45F9-B1E2-60E223B8CDF0}";

		public const string ShowInSiteMap_FID = "{F60C41DD-CC39-41BA-A9F9-E22E65F2885A}";

		public const string NavigationTitle_FID = "{B699C86E-09C4-4858-B0C5-30740B18FFC8}";

		public const string PageTitle_FID = "{1F06A67F-A6C3-4484-9768-A030800B61D1}";

		private bool? _excludeFromNavigation;

		private string _pageTitle;

		private bool? _showInSiteMap;

		private string _navigationTitle;

		private bool? _sSL;
		#endregion

		#region Constructors
		public BasePage(Item innerItem) :
			base(innerItem)
		{
		}
		#endregion

		#region Properties
		[Field(CareMore.Web.DotCom.Models.Pages.BasePage.ExcludeFromNavigation_FID)]
		public virtual bool? ExcludeFromNavigation
		{
			get
			{
				if (_excludeFromNavigation == null)
				{
					_excludeFromNavigation = this.GetBool(CareMore.Web.DotCom.Models.Pages.BasePage.ExcludeFromNavigation_FID);
				}
				return _excludeFromNavigation;
			}
			set
			{
				_excludeFromNavigation = null;
				this.SetBool(CareMore.Web.DotCom.Models.Pages.BasePage.ExcludeFromNavigation_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Pages.BasePage.PageTitle_FID)]
		public virtual string PageTitle
		{
			get
			{
				if (_pageTitle == null)
				{
					_pageTitle = this.GetString(CareMore.Web.DotCom.Models.Pages.BasePage.PageTitle_FID);
				}
				return _pageTitle;
			}
			set
			{
				_pageTitle = null;
				this.SetString(CareMore.Web.DotCom.Models.Pages.BasePage.PageTitle_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Pages.BasePage.ShowInSiteMap_FID)]
		public virtual bool? ShowInSiteMap
		{
			get
			{
				if (_showInSiteMap == null)
				{
					_showInSiteMap = this.GetBool(CareMore.Web.DotCom.Models.Pages.BasePage.ShowInSiteMap_FID);
				}
				return _showInSiteMap;
			}
			set
			{
				_showInSiteMap = null;
				this.SetBool(CareMore.Web.DotCom.Models.Pages.BasePage.ShowInSiteMap_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Pages.BasePage.NavigationTitle_FID)]
		public virtual string NavigationTitle
		{
			get
			{
				if (_navigationTitle == null)
				{
					_navigationTitle = this.GetString(CareMore.Web.DotCom.Models.Pages.BasePage.NavigationTitle_FID);
				}
				return _navigationTitle;
			}
			set
			{
				_navigationTitle = null;
				this.SetString(CareMore.Web.DotCom.Models.Pages.BasePage.NavigationTitle_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Pages.BasePage.SSL_FID)]
		public virtual bool? SSL
		{
			get
			{
				if (_sSL == null)
				{
					_sSL = this.GetBool(CareMore.Web.DotCom.Models.Pages.BasePage.SSL_FID);
				}
				return _sSL;
			}
			set
			{
				_sSL = null;
				this.SetBool(CareMore.Web.DotCom.Models.Pages.BasePage.SSL_FID, value);
			}
		}
		#endregion
	}
}