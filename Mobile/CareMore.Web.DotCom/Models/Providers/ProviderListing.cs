﻿using System;
using System.Collections.Generic;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using ClassySC.Data;

namespace CareMore.Web.DotCom.Models.Providers {
    
    
    public partial interface IProviderListing {
        
        string Title {
            get;
            set;
        }
        
        string ProviderType {
            get;
            set;
        }
        
        Sitecore.Data.Fields.ImageField KeyIcon {
            get;
            set;
        }
        
        Sitecore.Data.Fields.ImageField GoogleMapIcon {
            get;
            set;
        }
        
        Item DataSource {
            get;
            set;
        }
        
        Sitecore.Data.Fields.InternalLinkField SitecorePath {
            get;
            set;
        }
        
        System.Collections.Generic.IEnumerable<Sitecore.Data.Items.Item> SpecialtyFilter {
            get;
            set;
        }
        
        string ClassCode {
            get;
            set;
        }
        
        string Exclusions {
            get;
            set;
        }
        
        bool? ShowSubtypeFilter {
            get;
            set;
        }
        
        System.Collections.Generic.IEnumerable<Sitecore.Data.Items.Item> SubtypeFilter {
            get;
            set;
        }
        
        bool? ExcludeFromServiceMap {
            get;
            set;
        }
        
        bool? HideSearchForm {
            get;
            set;
        }
        
        string AccessToLangLine {
            get;
            set;
        }
        
        string CulturalCompetency {
            get;
            set;
        }
        
        string OnsiteInterpreter {
            get;
            set;
        }
    }
    
    [Template(CareMore.Web.DotCom.Models.Providers.ProviderListing.TemplateID)]
    public partial class ProviderListing : StandardTemplate, CareMore.Web.DotCom.Models.Providers.IProviderListing {
        
        #region Members
        public const string TemplateID = "{B70D54AD-8092-4B60-8E7F-3364199DDBA1}";
        
        public const string Title_FID = "{300E8AB7-5825-4BC5-AC98-39F44E937701}";
        
        public const string ProviderType_FID = "{87AB7E78-FBF8-4EAD-9B7D-9878019060B2}";
        
        public const string KeyIcon_FID = "{7C26773A-3F5E-45F9-BEE5-7F5C2FBBE34A}";
        
        public const string GoogleMapIcon_FID = "{4EE7981E-A0DD-43CD-818B-746F0D6FAB7A}";
        
        public const string DataSource_FID = "{ABDDF51E-5A51-488C-96F5-E4D6FDF3B20D}";
        
        public const string SitecorePath_FID = "{6857EDD2-FCCA-4183-A3D5-D12B3470C67A}";
        
        public const string SpecialtyFilter_FID = "{371077FE-3194-4D83-B197-BC4E551FBFB0}";
        
        public const string ClassCode_FID = "{5D4C45F1-F5C8-4E23-A40E-3B6B063AAD5A}";
        
        public const string Exclusions_FID = "{C13DC969-DB7B-42D0-80FA-E5D3180D835D}";
        
        public const string ShowSubtypeFilter_FID = "{B7262402-00C8-44BC-B95B-1026303ED4EE}";
        
        public const string SubtypeFilter_FID = "{10123B0C-8C8A-47C7-A27D-8FFE56CB2365}";
        
        public const string ExcludeFromServiceMap_FID = "{0FE1F9F6-C233-45EE-BB01-69B9D391E5A7}";
        
        public const string HideSearchForm_FID = "{F553ACF1-24D8-45E6-B852-88F72B328351}";
        
        public const string AccessToLangLine_FID = "{96455FD9-D567-4ED8-8890-649F9948F40F}";
        
        public const string CulturalCompetency_FID = "{BCEEF1EA-C3CC-4A6A-82CB-11F6B2C7931C}";
        
        public const string OnsiteInterpreter_FID = "{12051B1E-9100-40A4-9DCE-4F7436CF58B2}";
        
        private string _title;
        
        private string _providerType;
        
        private Item _dataSource;
        
        private IEnumerable<Item> _specialtyFilter;
        
        private string _classCode;
        
        private string _exclusions;
        
        private bool? _showSubtypeFilter;
        
        private IEnumerable<Item> _subtypeFilter;
        
        private bool? _excludeFromServiceMap;
        
        private bool? _hideSearchForm;
        
        private string _accessToLangLine;
        
        private string _culturalCompetency;
        
        private string _onsiteInterpreter;
        #endregion
        
        #region Constructors
        public ProviderListing(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(CareMore.Web.DotCom.Models.Providers.ProviderListing.Title_FID)]
        public virtual string Title {
            get {
                if (_title == null) {
_title = this.GetString(CareMore.Web.DotCom.Models.Providers.ProviderListing.Title_FID);
                }
                return _title;
            }
            set {
                _title = null;
                this.SetString(CareMore.Web.DotCom.Models.Providers.ProviderListing.Title_FID, value);
            }
        }
        
        [Field(CareMore.Web.DotCom.Models.Providers.ProviderListing.ProviderType_FID)]
        public virtual string ProviderType {
            get {
                if (_providerType == null) {
_providerType = this.GetString(CareMore.Web.DotCom.Models.Providers.ProviderListing.ProviderType_FID);
                }
                return _providerType;
            }
            set {
                _providerType = null;
                this.SetString(CareMore.Web.DotCom.Models.Providers.ProviderListing.ProviderType_FID, value);
            }
        }
        
        [Field(CareMore.Web.DotCom.Models.Providers.ProviderListing.KeyIcon_FID)]
        public virtual Sitecore.Data.Fields.ImageField KeyIcon {
            get {
                return this.GetField<ImageField>(CareMore.Web.DotCom.Models.Providers.ProviderListing.KeyIcon_FID);
            }
            set {
                this.SetField<ImageField>(CareMore.Web.DotCom.Models.Providers.ProviderListing.KeyIcon_FID, value);
            }
        }
        
        [Field(CareMore.Web.DotCom.Models.Providers.ProviderListing.GoogleMapIcon_FID)]
        public virtual Sitecore.Data.Fields.ImageField GoogleMapIcon {
            get {
                return this.GetField<ImageField>(CareMore.Web.DotCom.Models.Providers.ProviderListing.GoogleMapIcon_FID);
            }
            set {
                this.SetField<ImageField>(CareMore.Web.DotCom.Models.Providers.ProviderListing.GoogleMapIcon_FID, value);
            }
        }
        
        [Field(CareMore.Web.DotCom.Models.Providers.ProviderListing.DataSource_FID)]
        public virtual Item DataSource {
            get {
                if (_dataSource == null) {
_dataSource = this.GetItem(CareMore.Web.DotCom.Models.Providers.ProviderListing.DataSource_FID);
                }
                return _dataSource;
            }
            set {
                _dataSource = null;
                this.SetItem(CareMore.Web.DotCom.Models.Providers.ProviderListing.DataSource_FID, value);
            }
        }
        
        [Field(CareMore.Web.DotCom.Models.Providers.ProviderListing.SitecorePath_FID)]
        public virtual Sitecore.Data.Fields.InternalLinkField SitecorePath {
            get {
                return this.GetField<InternalLinkField>(CareMore.Web.DotCom.Models.Providers.ProviderListing.SitecorePath_FID);
            }
            set {
                this.SetField<InternalLinkField>(CareMore.Web.DotCom.Models.Providers.ProviderListing.SitecorePath_FID, value);
            }
        }
        
        [Field(CareMore.Web.DotCom.Models.Providers.ProviderListing.SpecialtyFilter_FID)]
        public virtual System.Collections.Generic.IEnumerable<Sitecore.Data.Items.Item> SpecialtyFilter {
            get {
                if (_specialtyFilter == null) {
_specialtyFilter = this.GetItems(CareMore.Web.DotCom.Models.Providers.ProviderListing.SpecialtyFilter_FID);
                }
                return _specialtyFilter;
            }
            set {
                _specialtyFilter = null;
                this.SetItems(CareMore.Web.DotCom.Models.Providers.ProviderListing.SpecialtyFilter_FID, value);
            }
        }
        
        [Field(CareMore.Web.DotCom.Models.Providers.ProviderListing.ClassCode_FID)]
        public virtual string ClassCode {
            get {
                if (_classCode == null) {
_classCode = this.GetString(CareMore.Web.DotCom.Models.Providers.ProviderListing.ClassCode_FID);
                }
                return _classCode;
            }
            set {
                _classCode = null;
                this.SetString(CareMore.Web.DotCom.Models.Providers.ProviderListing.ClassCode_FID, value);
            }
        }
        
        [Field(CareMore.Web.DotCom.Models.Providers.ProviderListing.Exclusions_FID)]
        public virtual string Exclusions {
            get {
                if (_exclusions == null) {
_exclusions = this.GetString(CareMore.Web.DotCom.Models.Providers.ProviderListing.Exclusions_FID);
                }
                return _exclusions;
            }
            set {
                _exclusions = null;
                this.SetString(CareMore.Web.DotCom.Models.Providers.ProviderListing.Exclusions_FID, value);
            }
        }
        
        [Field(CareMore.Web.DotCom.Models.Providers.ProviderListing.ShowSubtypeFilter_FID)]
        public virtual bool? ShowSubtypeFilter {
            get {
                if (_showSubtypeFilter == null) {
_showSubtypeFilter = this.GetBool(CareMore.Web.DotCom.Models.Providers.ProviderListing.ShowSubtypeFilter_FID);
                }
                return _showSubtypeFilter;
            }
            set {
                _showSubtypeFilter = null;
                this.SetBool(CareMore.Web.DotCom.Models.Providers.ProviderListing.ShowSubtypeFilter_FID, value);
            }
        }
        
        [Field(CareMore.Web.DotCom.Models.Providers.ProviderListing.SubtypeFilter_FID)]
        public virtual System.Collections.Generic.IEnumerable<Sitecore.Data.Items.Item> SubtypeFilter {
            get {
                if (_subtypeFilter == null) {
_subtypeFilter = this.GetItems(CareMore.Web.DotCom.Models.Providers.ProviderListing.SubtypeFilter_FID);
                }
                return _subtypeFilter;
            }
            set {
                _subtypeFilter = null;
                this.SetItems(CareMore.Web.DotCom.Models.Providers.ProviderListing.SubtypeFilter_FID, value);
            }
        }
        
        [Field(CareMore.Web.DotCom.Models.Providers.ProviderListing.ExcludeFromServiceMap_FID)]
        public virtual bool? ExcludeFromServiceMap {
            get {
                if (_excludeFromServiceMap == null) {
_excludeFromServiceMap = this.GetBool(CareMore.Web.DotCom.Models.Providers.ProviderListing.ExcludeFromServiceMap_FID);
                }
                return _excludeFromServiceMap;
            }
            set {
                _excludeFromServiceMap = null;
                this.SetBool(CareMore.Web.DotCom.Models.Providers.ProviderListing.ExcludeFromServiceMap_FID, value);
            }
        }
        
        [Field(CareMore.Web.DotCom.Models.Providers.ProviderListing.HideSearchForm_FID)]
        public virtual bool? HideSearchForm  {
            get {
                if (_hideSearchForm == null) {
                    _hideSearchForm = this.GetBool(CareMore.Web.DotCom.Models.Providers.ProviderListing.HideSearchForm_FID);
                }
                return _hideSearchForm;
            }
            set {
                _hideSearchForm = null;
                this.SetBool(CareMore.Web.DotCom.Models.Providers.ProviderListing.HideSearchForm_FID, value);
            }
        }
        
        [Field(CareMore.Web.DotCom.Models.Providers.ProviderListing.AccessToLangLine_FID)]
        public virtual string AccessToLangLine {
            get {
                if (_accessToLangLine == null) {
_accessToLangLine = this.GetString(CareMore.Web.DotCom.Models.Providers.ProviderListing.AccessToLangLine_FID);
                }
                return _accessToLangLine;
            }
            set {
                _accessToLangLine = null;
                this.SetString(CareMore.Web.DotCom.Models.Providers.ProviderListing.AccessToLangLine_FID, value);
            }
        }
        
        [Field(CareMore.Web.DotCom.Models.Providers.ProviderListing.CulturalCompetency_FID)]
        public virtual string CulturalCompetency {
            get {
                if (_culturalCompetency == null) {
_culturalCompetency = this.GetString(CareMore.Web.DotCom.Models.Providers.ProviderListing.CulturalCompetency_FID);
                }
                return _culturalCompetency;
            }
            set {
                _culturalCompetency = null;
                this.SetString(CareMore.Web.DotCom.Models.Providers.ProviderListing.CulturalCompetency_FID, value);
            }
        }
        
        [Field(CareMore.Web.DotCom.Models.Providers.ProviderListing.OnsiteInterpreter_FID)]
        public virtual string OnsiteInterpreter {
            get {
                if (_onsiteInterpreter == null) {
_onsiteInterpreter = this.GetString(CareMore.Web.DotCom.Models.Providers.ProviderListing.OnsiteInterpreter_FID);
                }
                return _onsiteInterpreter;
            }
            set {
                _onsiteInterpreter = null;
                this.SetString(CareMore.Web.DotCom.Models.Providers.ProviderListing.OnsiteInterpreter_FID, value);
            }
        }
        #endregion
    }
}
