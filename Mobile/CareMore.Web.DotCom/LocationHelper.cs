﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;


namespace CareMore.Web.DotCom
{
	public static class LocationHelper
	{
		public static string GetFormattedAddress(string addressLine1, string addressLine2, string city, string state, string zip)
		{
			return GetFormattedAddress(addressLine1, addressLine2, city, state, zip, null, null, false);
		}

		public static string GetFormattedAddress(string addressLine1, string addressLine2, string city, string state, string zip, string addressLink, string addressTooltip, bool newWindow)
		{
			StringBuilder address = new StringBuilder();

			if (!string.IsNullOrWhiteSpace(addressLink))
			{
				address.AppendFormat("<a href=\"{0}\" title=\"{1}\" tooltip=\"{1}\"", addressLink, addressTooltip);

				if (newWindow)
					address.Append(" target=\"_blank\">");
				else
					address.Append(">");
			}

			address.Append(addressLine1);

			if (!string.IsNullOrWhiteSpace(addressLink))
				address.Append("</a>");

			address.Append("<br />");

			if (!string.IsNullOrWhiteSpace(addressLine2))
			{
				address.AppendFormat("{0}<br />", addressLine2);
			}

			address.AppendFormat("{0}, {1}<br />{2}", city, state, GetFormattedZipCode(zip));

			return address.ToString();
		}

		public static string GetFormattedPhoneNo(object text)
		{
			string phoneNumber = string.Empty;

			if (text != null)
			{
				phoneNumber = Regex.Replace(text.ToString(), "[^0-9]", string.Empty);

				if (phoneNumber.Length == 10)
				{
					return string.Format("({0}) {1}-{2}",
						phoneNumber.Substring(0, 3),
						phoneNumber.Substring(3, 3),
						phoneNumber.Substring(6));
				}
			}

			return phoneNumber;
		}

		public static string GetFormattedContactInfo(string phoneText, string phoneNumber, string faxText, string faxNumber)
		{
			StringBuilder contactInfo = new StringBuilder();

			if (!string.IsNullOrWhiteSpace(phoneNumber))
				contactInfo.AppendFormat("{0}: {1}<br />", phoneText, phoneNumber);

			if (!string.IsNullOrWhiteSpace(faxNumber))
				contactInfo.AppendFormat("{0}: {1}<br />", faxText, faxNumber);

			if (contactInfo.Length > 0)
				contactInfo.Length -= 6;

			return contactInfo.ToString();
		}

		public static string GetFormattedAffiliationInfo(string ipa_id, string ipa_desc)
		{
			string affiliationInfo = string.Empty;

			if (ipa_id != null && ipa_id != string.Empty)
				affiliationInfo += ipa_id;

			if (ipa_desc != null && ipa_desc != string.Empty)
				affiliationInfo += (" - " + ipa_desc);

			return affiliationInfo;
		}

		public static string GetFormatted2ndHours(string fromHours, string toHours)
		{
			string formattedHours;

			if ((string.IsNullOrWhiteSpace(fromHours)) || (string.IsNullOrWhiteSpace(toHours)))
			{
				formattedHours = string.Empty;
			}
			else
			{
				formattedHours = string.Format("&nbsp;and&nbsp;</td><td>{0}</td><td>-</td><td>{1}", fromHours, toHours);
			}

			return formattedHours;
		}

		public static string GetFormattedZipCode(string zip)
		{
			string formattedZip;

			if (string.IsNullOrWhiteSpace(zip))
				formattedZip = string.Empty;
			else if (zip.Length > 5)
				formattedZip = string.Format("{0}-{1}", zip.Substring(0, 5), zip.Substring(5));
			else
				formattedZip = zip;

			return formattedZip;
		}

		public static string GetMapDirections(string addressLine1, string addressLine2, string city, string state, string zipCode)
		{
			string address = GetMapUrl(addressLine1, addressLine2, city, state, zipCode);
			return string.Format("http://maps.google.com/maps?saddr=&daddr={0}", address);
		}

		public static string GetMapQuery(string addressLine1, string addressLine2, string city, string state, string zipCode)
		{
			string address = GetMapUrl(addressLine1, addressLine2, city, state, zipCode);
			return string.Format("http://maps.google.com/maps?q={0}", address);
		}

		private static string GetMapUrl(string addressLine1, string addressLine2, string city, string state, string zipCode)
		{
			string address = string.Format("{0} {1}, {2}, {3} {4}", addressLine1, addressLine2, city, state, zipCode);
			return HttpContext.Current.Server.UrlEncode(address);
		}
	}
}
