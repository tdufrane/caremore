﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CareMore.Web.DotCom.Data;

namespace CareMore.Web.DotCom.Provider
{
	public class FacilityResultAddress
	{
		#region Constructors

		public FacilityResultAddress()
		{
		}

		public FacilityResultAddress(FacilitySearchResult item)
		{
			this.AddressType = item.AddressType;
			this.AddressEffectiveDate = item.AddressEffectiveDate;
			this.Address1 = item.Address1;
			this.Address2 = item.Address2;
			this.Address3 = item.Address3;
			this.City = item.City;
			this.State = item.State;
			this.Zip = item.Zip;
			this.Phone = item.Phone;
		}

		#endregion

		#region Properties

		public string AddressType { get; set; }
		public DateTime AddressEffectiveDate { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string Address3 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Zip { get; set; }
		public string Phone { get; set; }

		#endregion

		#region Methods

		#endregion
	}
}
