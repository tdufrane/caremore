﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CareMore.Web.DotCom.Data;

namespace CareMore.Web.DotCom.Provider
{
	public class FacilityResultHeader
	{
		public FacilityResultHeader(FacilitySearchResult item)
		{
			this.SearchResult = item;

			this.ProviderId = item.ProviderId;
			this.AddressId = item.AddressId;
			this.Name = item.Name;
			this.Distance = item.Distance;
			this.EffectiveDate = item.EffectiveDate;

			this.Addresses = new List<FacilityResultAddress>();

			FacilityResultAddress address = new FacilityResultAddress(item);
			this.Addresses.Add(address);
		}

		public string ProviderId { get; set; }
		public string AddressId { get; set; }
		public string Name { get; set; }
		public float Distance { get; set; }
		public DateTime EffectiveDate { get; set; }

		public FacilitySearchResult SearchResult { get; private set; }
		public List<FacilityResultAddress> Addresses { get; private set; }
	}
}
