﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;

using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;

using CareMore.Web.DotCom.Data;

namespace CareMore.Web.DotCom.Provider
{
	public static class ProviderHelper
	{
		public const string DetailsUrlFormat = "{0}?&id={1}&aid={2}&at={3}";

		public static List<ProviderAccessibility> GetAccessibilities(string accessibilities)
		{
			ProviderAccessibility accessibility = new ProviderAccessibility();

			if (string.IsNullOrEmpty(accessibilities))
				accessibility.MappingCode = "N/A";
			else
				accessibility.MappingCode = accessibilities;

			List<ProviderAccessibility> list = new List<ProviderAccessibility>();
			list.Add(accessibility);

			return list;
		}

		public static List<string> GetExclusions(string exclusions)
		{
			List<string> list = new List<string>();

			if (!string.IsNullOrWhiteSpace(exclusions))
			{
				string[] split = exclusions.Split(new char[] { '\r', '\n', '|' }, StringSplitOptions.RemoveEmptyEntries);

				foreach (string pair in split)
				{
					list.Add(pair);
				}
			}

			return list;
		}

		public static bool IsInDistance(string distanceText, decimal maxDistance)
		{
			bool inDistance = false;

			if (!string.IsNullOrWhiteSpace(distanceText))
			{
				decimal distance;

				if (decimal.TryParse(distanceText.Replace(" Mile", string.Empty), out distance))
				{
					if (distance <= maxDistance)
						inDistance = true;
				}
			}

			return inDistance;
		}

		public static string GetFormattedGenderInfo(string gender)
		{
			string info;
			switch (gender.ToUpper())
			{
				case "M":
					info = "Male";
					break;
				case "F":
					info = "Female";
					break;
				default:
					info = string.Empty;
					break;
			}
			return info;
		}

		public static List<ProviderHour> GetHoursOfOperation(string hoursOfOperation)
		{
			List<ProviderHour> hours = new List<ProviderHour>();

			if (!string.IsNullOrWhiteSpace(hoursOfOperation))
			{
				// Sitecore example:
				//  Monday: 8:00 am - 12:00 pm and 1:00 pm - 5:00 pm<br />Tuesday: 10:00 am - 6:00 pm<br />Wednesday: 10:00 am - 6:00 pm<br />Thursday: 10:00 am - 6:00 pm<br />Friday: 10:00 am - 6:00 pm<br />Saturday: 9:00 am - 2:00 pm

				string[] daysAndHours = hoursOfOperation.Split(new string[] { "<br />", "\n" }, StringSplitOptions.RemoveEmptyEntries);

				string[] multipleHoursSplit = new string[] { "and" };

				foreach (string dayHourItem in daysAndHours)
				{
					int firstColonIndex = dayHourItem.IndexOf(':');

					if (firstColonIndex > -1)
					{
						ProviderHour hour = new ProviderHour();
						hour.DaysOfWeek = dayHourItem.Substring(0, firstColonIndex);

						int andIndex = dayHourItem.IndexOf("and");

						if (andIndex == -1)
						{
							string[] hoursArray = GetHours(dayHourItem.Substring(firstColonIndex + 1));
							hour.FromHours = hoursArray[0];
							hour.ToHours = hoursArray[1];
							hour.FromHours2 = string.Empty;
							hour.ToHours2 = string.Empty;
						}
						else
						{
							string[] hoursArray = GetHours(dayHourItem.Substring(firstColonIndex + 1, andIndex - firstColonIndex - 1));
							hour.FromHours = hoursArray[0];
							hour.ToHours = hoursArray[1];

							hoursArray = GetHours(dayHourItem.Substring(andIndex + 3));
							hour.FromHours2 = hoursArray[0];
							hour.ToHours2 = hoursArray[1];
						}

						hours.Add(hour);
					}
				}
			}

			return hours;
		}

		private static string[] GetHours(string hours)
		{
			string[] hourSplit = hours.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
			string[] hourArray = new string[] { string.Empty, string.Empty };

			if (hourSplit.Length == 2)
			{
				hourArray[0] = hourSplit[0].Trim();
				hourArray[1] = hourSplit[1].Trim();
			}
			else if (hours.IndexOf('-') == -1)
			{
				hourArray[0] = hours;
				hourArray[1] = string.Empty;
			}

			return hourArray;
		}

		public static string GetLanguages(string languages)
		{
			if (string.IsNullOrEmpty(languages))
				return "English";
			else
				return languages;
		}

		public static string GetMapUrl(FacilitySearchDetail location)
		{
			return LocationHelper.GetMapDirections(location.Address1, location.Address2,
				location.City, location.State, location.Zip);
		}

		public static string GetMapUrl(Item location)
		{
			return LocationHelper.GetMapQuery(location["Address Line 1"], location["Address Line 2"],
				location["City"], location["State"], location["Postal Code"]);
		}

		public static string GetMapUrl(ProviderSearchDetail location)
		{
			return LocationHelper.GetMapQuery(location.Address1, location.Address2,
				location.City, location.State, location.Zip);
		}

		public static double GetMaxZipDistance(string siteSettings)
		{
			double maxDistance = 25.0;
			Item settings = Sitecore.Context.Database.GetItem(siteSettings);

			if (settings != null)
			{
				Field distanceField = settings.Fields["Max Zip Distance"];

				if (distanceField != null)
				{
					double distance = 0.0;
					if (double.TryParse(distanceField.Value, out distance))
					{
						maxDistance = distance;
					}
				}
			}

			return maxDistance;
		}

		public static byte GetPortalCode(string siteSettings)
		{
			byte code = 0;
			Item settings = Sitecore.Context.Database.GetItem(siteSettings);

			if (settings != null)
			{
				LookupField codeField = (LookupField)settings.Fields["Portal Code"];

				if (codeField != null)
				{
					byte.TryParse(codeField.TargetItem["Code"], out code);
				}
			}

			return code;
		}

		public static List<string> GetServices(List<ProviderService> services)
		{
			List<string> list = new List<string>();

			foreach (ProviderService service in services)
			{
				list.Add(service.Service);
			}

			return list;
		}

		public static List<string> GetServices(Item servicesItem)
		{
			List<string> list = new List<string>();

			MultilistField servicesField = servicesItem.Fields["Services"];

			if (servicesField != null)
			{
				Item[] serviceItems = servicesField.GetItems();
				foreach (Item serviceItem in serviceItems)
				{
					list.Add(serviceItem["Title"]);
				}
			}

			return list;
		}

		public static bool IsZipCodeCovered(string localizationId, string countyTemplateId, string zipCode)
		{
			Item localizationItem = Sitecore.Context.Database.GetItem(localizationId);
			Item countyTemplate = Sitecore.Context.Database.GetItem(countyTemplateId);
			IEnumerable<Item> countyItems = localizationItem.Axes.GetDescendants().Where(item => item.TemplateID.Equals(countyTemplate.ID));

			foreach (Item countyItem in countyItems)
			{
				if (IsZipCodeCovered(countyItem, zipCode))
					return true;
			}

			return false;
		}

		private static bool IsZipCodeCovered(Item item, string zipCode)
		{
			if (string.IsNullOrWhiteSpace(zipCode))
				return false;

			return item["Zipcodes"].Contains(zipCode);
		}

		public static void SetEffectiveDate(Label effectiveLabel, DateTime effectiveDate)
		{
			SetEffectiveDate(effectiveLabel, "Effective", effectiveDate);
		}

		public static void SetEffectiveDate(Label effectiveLabel, string effectiveText, DateTime effectiveDate)
		{
			if (effectiveDate > DateTime.Now)
				effectiveLabel.Text = string.Format("<br />{0}: {1:MM/dd/yyyy}", effectiveText, effectiveDate);
		}

		public static void SetEffectiveDate(StringBuilder html, DateTime effectiveDate)
		{
			SetEffectiveDate(html, "Effective", effectiveDate);
		}

		public static void SetEffectiveDate(StringBuilder html, string effectiveText, DateTime effectiveDate)
		{
			if (effectiveDate > DateTime.Now)
				html.AppendFormat("<br />{0}: {1:MM/dd/yyyy}", effectiveText, effectiveDate);
		}

		public static void SetNpi(Label idLabel, string npiId)
		{
			if ((string.IsNullOrWhiteSpace(npiId)) ||
			    (npiId.Equals("NOT NEEDED", StringComparison.OrdinalIgnoreCase)))
				idLabel.Visible = false;
			else
				idLabel.Text = "<br />NPI: " + npiId;
		}
	}
}
