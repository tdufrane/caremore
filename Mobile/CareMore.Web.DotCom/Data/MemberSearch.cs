using System;
using System.Configuration;

namespace CareMore.Web.DotCom.Data
{
	partial class MemberSearchDataContext
	{
		partial void OnCreated()
		{
			this.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["Facets"].ConnectionString;
		}
	}
}
