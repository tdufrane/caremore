/* ********************************************************************** *
* File   : MetaTags.cs                                   Part of SiteCore *
* Version: 3.0.0                                          www.sitecore.net *
*                                                                         *
* Purpose: Metatags web control implementation                            *
*                                                                         *
* Bugs   : None known.                                                    *
*                                                                         *
* Status : Published.                                                     *
*                                                                         *
* Copyright (C) 1999-2003 by SiteCore A/S. All rights reserved.           *
*                                                                         *
* This work is the property of:                                           *
*                                                                         *
*        SiteCore A/S                                                     *
*        Tornebuskegade 1                                                 *
*        1131 Copenhagen K.                                               *
*        Denmark                                                          *
*                                                                         *
* This is a SiteCore published work under SiteCore's                      *
* shared source license.                                                  *
*                                                                         *
* *********************************************************************** */
using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.Xml;

using Sitecore;
using Sitecore.Web.UI;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using WebControl = Sitecore.Web.UI.WebControl;

namespace Revere.West.SC.WebControls.MetaTags
{
   //===============================================================
   /// <summary>Metatag control</summary>
   //===============================================================
   //[Designer(typeof(WebControlDesigner))]

   [ToolboxData("<{0}:MetaTag runat=server></{0}:MetaTag>")]
   public class MetaTags : WebControl
   {

      #region Variables

      private string metatagPath = "/sitecore/content/global/metatags/";
      private string metatags = "metatags";
      private string sets = "sets";
      private string keywords = "keywords";
      private string globalName = "global";
      private string m_dateFormat = "yyyy-MM-dd";
      protected Item m_item = null;
      protected Item m_set = null;
      protected Item m_global = null;

      protected ArrayList tagsList = new ArrayList();

      #endregion

      #region Properties

      public string MetatagsPath
      {
         get
         {
            return metatagPath;
         }
         set
         {
            metatagPath = value;
         }
      }

      public string MetatagsNodeName
      {
         get
         {
            return metatags;
         }
         set
         {
            metatags = value;
         }
      }

      public string SetsNodeName
      {
         get
         {
            return sets;
         }
         set
         {
            sets = value;
         }
      }

      public string KeywordsNodeName
      {
         get
         {
            return keywords;
         }
         set
         {
            keywords = value;
         }
      }

      public string GlobalMetatagsNodeName
      {
         get
         {
            return globalName;
         }
         set
         {
            globalName = value;
         }
      }

      public string DateFormat
      {
         get
         {
            return m_dateFormat;
         }
         set
         {
            m_dateFormat = value;
         }
      }


      #endregion

      #region Protected methods

      //-------------------------------------------------------------
      /// <summary>Render control</summary>
      //-------------------------------------------------------------
      protected override void DoRender(HtmlTextWriter output)
      {
        
         m_item = this.GetItem();

         if (m_item != null)
         {
            Field setIDField = m_item.Fields["Metatags Set"];
            if (setIDField != null)
            {
               string setID = setIDField.Value;
               if (setID != string.Empty)
               {
                  m_set = Sitecore.Context.Database.Items[string.Format("{0}{1}/{2}", MetatagsPath, SetsNodeName, setID)];
               }

               m_global = Sitecore.Context.Database.Items[string.Format("{0}{1}", MetatagsPath, GlobalMetatagsNodeName)];

               RenderMetaTags(output);
               RenderKeywords(output);
               RenderDescription(output);
            }
         }
      }

      protected void RenderMetaTags(HtmlTextWriter output)
      {
         RenderMetaTags(output, m_item.Fields["Metatags Metatags"].Value);

         if (m_set != null)
         {
            RenderMetaTags(output, m_set.Fields["Metatags"].Value);
         }

         if (m_global != null)
         {
            RenderMetaTags(output, m_global.Fields["Metatags"].Value);
         }
      }

      protected void RenderMetaTags(HtmlTextWriter output, string metatags)
      {
         foreach (string metatagID in metatags.Split('|'))
         {
            if (metatagID != string.Empty)
            {
               Item metatagItem = Sitecore.Context.Database.Items[string.Format("{0}{1}/{2}", MetatagsPath, MetatagsNodeName, metatagID)];
               if (metatagItem != null)
               {
                  string name = metatagItem.Fields["name"].Value;
                  string httpequiv = metatagItem.Fields["httpequiv"].Value;
                  string content = metatagItem.Fields["content"].Value;

                  WriteTag(output, name, httpequiv, content);
               }
            }
         }
      }

      protected void RenderKeywords(HtmlTextWriter output)
      {
         string keywords = m_item.Fields["Metatags Other keywords"].Value;

         string predefinedKeywords = m_item.Fields["Metatags Predefined keywords"].Value;

         keywords = RenderKeywords(predefinedKeywords, keywords);

         if (m_set != null)
         {
            string otherKeywords = m_set.Fields["Other keywords"].Value;

            if (otherKeywords != string.Empty)
            {
               keywords += (keywords != string.Empty ? "," : string.Empty) + otherKeywords;
            }

            predefinedKeywords = m_set.Fields["Predefined keywords"].Value;
            keywords = RenderKeywords(predefinedKeywords, keywords);
         }

         if (m_global != null)
         {
            string otherKeywords = m_global.Fields["Other keywords"].Value;

            if (otherKeywords != string.Empty)
            {
               keywords += (keywords != string.Empty ? "," : string.Empty) + otherKeywords;
            }

            predefinedKeywords = m_global.Fields["Predefined keywords"].Value;
            keywords = RenderKeywords(predefinedKeywords, keywords);
         }

         if (keywords != string.Empty)
         {

            WriteTag(output, "keywords", string.Empty, RegulateKeywords(keywords));
         }
      }

      private string RegulateKeywords(string keywords)
      {
         string result = string.Empty;
         string[] keywordsArray = keywords.Split(new char[] { ',' });
         foreach (string word in keywordsArray)
         {
            string trimed = word.Trim();
            if ((trimed != string.Empty) && (result.IndexOf(trimed) == -1))
            {
               result += trimed;
               result += ",";
            }
         }
         return result.Length > 0 ? result.Remove(result.Length - 1) : string.Empty;
      }

      protected string RenderKeywords(string predefinedKeywords, string keywords)
      {
         foreach (string keywordID in predefinedKeywords.Split('|'))
         {
            if (keywordID != string.Empty)
            {
               Item keywordItem = Sitecore.Context.Database.Items[string.Format("{0}{1}/{2}", MetatagsPath, KeywordsNodeName, keywordID)];

               if (keywordItem != null)
               {
                  keywords += (keywords != string.Empty ? "," : string.Empty) + keywordItem.Fields["Keyword"].Value;
               }
            }
         }

         return keywords;
      }

      protected void RenderDescription(HtmlTextWriter output)
      {
         string description = m_item.Fields["Metatags Description"].Value;

         if (m_set != null)
         {
            string setDescription = m_set.Fields["Description"].Value;
            if (setDescription != string.Empty)
            {
               description += (description != string.Empty ? " " : string.Empty) + setDescription;
            }
         }

         if (m_global != null)
         {
            string globalDescription = m_global.Fields["Description"].Value;
            if (globalDescription != string.Empty)
            {
               description += (description != string.Empty ? " " : string.Empty) + globalDescription;
            }
         }

         if (description != string.Empty)
         {
            WriteTag(output, "description", string.Empty, description);
         }
      }

      protected void WriteTag(HtmlTextWriter output, string name, string httpequiv, string content)
      {
         content = content.Replace("$id", m_item.ID.ToString());
         content = content.Replace("$name", m_item.Name);
         content = content.Replace("$path", Sitecore.Links.LinkManager.GetItemUrl( m_item ) );
         content = content.Replace("$now", DateTime.Now.ToString(DateFormat));
         content = content.Replace("$url", Page.Request.Url.ToString());

         content = Regex.Replace(content, "\\$([^\\$]*)\\$", new MatchEvaluator(ReplaceField), RegexOptions.IgnoreCase | RegexOptions.Singleline);

         if (name != string.Empty)
         {
            string tagListEntry = name + ":" + content;
            if (tagsList.IndexOf(tagListEntry) == -1)
            {
               tagsList.Add(tagListEntry);
               output.WriteLine(String.Format("<meta name=\"{0}\" content=\"{1}\">", name, content));
            }
         }
         else if (httpequiv != string.Empty)
         {
            output.WriteLine(String.Format("<meta http-equiv=\"{0}\" content=\"{1}\">", httpequiv, content));
         }
      }

      public string ReplaceField(Match match)
      {
         string fieldName = match.Groups[1].Value;
         Field field = m_item.Fields[fieldName];
         return field == null ? string.Empty : field.Value;
      }

      #endregion
   }
}
