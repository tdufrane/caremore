﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  xmlns:dot="http://www.sitecore.net/dot"
  xmlns:sql="http://www.sitecore.net/sql"
  exclude-result-prefixes="dot sc sql">

  <!-- output directives -->
  <xsl:output method="html" indent="no" encoding="UTF-8"  />

  <!-- sitecore parameters -->
  <xsl:param name="lang" select="'en'"/>
  <xsl:param name="id" select="''"/>
  <xsl:param name="sc_item"/>
  <xsl:param name="sc_currentitem"/>

  <!-- variables -->
  <xsl:variable name="home" select="/*/item[@key='content']/item[@key='home']" />

  <!-- entry point -->
  <xsl:template match="*">
    <xsl:apply-templates select="$sc_item" mode="main" />
  </xsl:template>

  <!-- main -->
  <xsl:template match="*" mode="main">
    <xsl:for-each select="ancestor-or-self::item">
      <xsl:if test="position() > 2 and @template != 'folder'">
        <xsl:choose>
          <xsl:when test="position() != last()">
            <sc:link>
              <xsl:call-template name="GetNavTitle" />
            </sc:link>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="GetNavTitle" />
          </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="position() != last()">
          »
        </xsl:if>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <!-- GetNavTitle -->
  <xsl:template name="GetNavTitle">
    <xsl:param name="item" select="." />
    <xsl:choose>
      <xsl:when test="sc:fld( 'Title', $item )">
        <xsl:value-of select="sc:fld( 'Title', $item )" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="sc:fld( 'Header Title', $item )">
            <xsl:value-of select="sc:fld( 'Header Title', $item )" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$item/@name" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
