﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

namespace Website.Services
{
    public class MapPinLocationHelper
    {
        public static void SaveGeoLocation(string address, string location)
        {
            // remove paranthesis and empty space and then store to cache
            location = location.Replace("(", string.Empty).Replace(")", string.Empty).Replace(" ", string.Empty);

            // store in cache with sliding expiration of 10 days
            HttpContext.Current.Cache.Add(address,
                location,
                null,
                System.Web.Caching.Cache.NoAbsoluteExpiration,
                TimeSpan.FromDays(10),
                System.Web.Caching.CacheItemPriority.Normal,
                null);
        }
    }
}