﻿using System;
using System.Text.RegularExpressions;
using CareMore.Web.DotCom;
using Sitecore.Pipelines.HttpRequest;

namespace Website.SEO
{
	public class DescriptiveUrlResolver : HttpRequestProcessor
	{
		public override void Process(HttpRequestArgs args)
		{
			if (args.Url.ItemPath.StartsWith(CareMoreUtilities.PLANS_FOLDER + "/plan/", StringComparison.OrdinalIgnoreCase))
			{
				Match match = Regex.Match(args.Url.ItemPath, CareMoreUtilities.PLANS_FOLDER + "/plan/([^//]+)/([^//]+)/(.+)", RegexOptions.IgnoreCase);

				if (match.Groups.Count == 4)
				{
					Sitecore.Context.Item = Sitecore.Context.Database.SelectSingleItem(CareMoreUtilities.PLANS_FOLDER + "/plan/details");
					Sitecore.Web.WebUtil.RewriteUrl("/plans/plan/details.aspx?" + string.Format("id={0}&st={1}&co={2}", match.Groups[3], match.Groups[1], match.Groups[2]));
				}
			}
		}
	}

	public class DescriptiveUrlRewrite
	{
		public static string GetDescriptivePlanUrl(string planType, string state, string county)
		{
			return string.Format("/plans/plan/{0}/{1}/{2}.aspx", state, county, planType);
		}
	}
}
