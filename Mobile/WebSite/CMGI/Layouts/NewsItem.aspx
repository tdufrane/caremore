﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewsItem.aspx.cs" Inherits="CMGI.Layouts.NewsItem" MasterPageFile="Base.Master" %>
<%@ MasterType VirtualPath="Base.Master" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="/CMGI/Sublayouts/LeftNavigation.ascx" %>

<asp:content runat="server" id="Content1" contentplaceholderid="head">
</asp:content>

<asp:content runat="server" id="PatientBody" contentplaceholderid="MainBodyContent">
	<div class="container">
		<!-- begin left side navigation -->
		<cg:LeftNavigation runat="server" />
		<!-- end left side navigation -->

		<div class="copy">
			<div class="story">
				<p class="big-blue"><span style="color:Black;"><cg:Date Field="Date" runat="server" format="MM/dd/yyyy" /></span> - <cg:Text Field="Headline" runat="server" /></p>

				<cg:Text Field="Body" runat="server" />

				<p><a href="javascript:history.go(-1);">&lt; Go Back</a></p>
			</div>
			<div class="clear_both"></div>
		</div>
		<div class="clear_both"></div>
	</div>
</asp:content>
