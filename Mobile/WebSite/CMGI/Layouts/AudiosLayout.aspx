﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AudiosLayout.aspx.cs" Inherits="CMGI.Layouts.AudiosLayout" MasterPageFile="Base.Master" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="../Sublayouts/LeftNavigation.ascx" %>
<%@ Register Src="../Sublayouts/AudioPlayer.ascx" TagPrefix="apc" TagName="AudioPlayer" %>

<asp:Content runat="server" id="HeaderContent" contentplaceholderid="head">
	<link href="/player/jplayer.blue.monday.css" rel="stylesheet" type="text/css" />
	<script src="/js/jquery.jplayer.min.js" type="text/javascript"></script>
</asp:Content>

<asp:content runat="server" id="BodyContent" contentplaceholderid="MainBodyContent">
	<div class="container">
		<!-- begin left side navigation -->
		<cg:LeftNavigation runat="server" />
		<!-- end left side navigation -->

		<div class="copy">
			<div class="title">
				<h1><cg:Text Field="Title" runat="server" /></h1>
			</div>

			<cg:Text Field="Body" runat="server" />

			<table style="height:80px;">
				<tr>
					<td id="tdImage" runat="server"><img id="imgDrPicture" runat="server" alt="" width="78" height="78" src="~/media/{0}.ashx" /></td>
					<td style="padding:0px 15px;"><asp:Label ID="lblTitle" runat="server" /></td>
					<td>
						<apc:AudioPlayer id="apcAudioPlayerItem" runat="server" />
					</td>
				</tr>
			</table>

			<br />

			<div class="carousel-list">
				<div class="slider-wrap audio">
					<asp:ListView id="sliderMultiLV" runat="server" OnItemDataBound="sliderMultiLV_ItemDataBound">
						<LayoutTemplate>
							<ul id="slider-multi">
								<li style="margin-left:40px;">
									<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
								</li>
							</ul>
						</LayoutTemplate>
						<ItemTemplate>
							<div>
								<table border="0" cellpadding="0" cellspacing="0" style="width:280px;">
									<tr valign="top">
										<td style="width:100px;"><sc:Image runat="server" ID="scImage" Field="Picture" /></td>
										<td><asp:HyperLink runat="server" ID="hl"><sc:Text runat="server" ID="scText" Field="Title" /></asp:HyperLink></td>
									</tr>
								</table>
							</div>
							<asp:Literal runat="server" ID="litBreak" Text="</li><li>" Visible="false" />
						</ItemTemplate>
					</asp:ListView>
				</div>
			</div>

			<div style="margin-top: 10px;">
				<cg:Link ID="ArchivesLink" runat="server" Field="Footer Link" />
			</div>
		</div>

		<div class="clear_both"></div>
	</div>

<script type="text/javascript">
	$j(function ()
	{
		$j('#slider-multi').anythingSlider({
			expand: true,
			buildNavigation: false,
			buildStartStop: false,
			showMultiple: 2,
			changeBy: 2
		});
	});
</script>
</asp:content>
