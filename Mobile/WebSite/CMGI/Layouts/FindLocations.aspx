﻿<%@ Page Title="" Language="C#" MasterPageFile="Base.Master" AutoEventWireup="true"
    CodeBehind="FindLocations.aspx.cs" Inherits="CMGI.Layouts.FindLocations" %>

<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics" %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="/CMGI/Sublayouts/LeftNavigation.ascx" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.Controls" Assembly="CMGI" %>
<%@ Register TagPrefix="cg" TagName="GoogleMap" Src="/CMGI/Sublayouts/GoogleMap.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
.style1
{
	width: 84px;
}
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainBodyContent" runat="server">
	<div class="container">
		<div class="noSideMenu">
			<div class="title">
				<h1><cg:Text Field="Title" runat="server" /></h1>
			</div>
			<asp:Panel ID="MiddleContentFrame" runat="server" ClientIDMode="Static" DefaultButton="btnSearchLocations">
				<table width="700" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><strong>Search by:</strong></td>
						<td>
							<asp:TextBox CssClass="zip defaultValue" runat="server" ID="txtZip" />
							&nbsp;&nbsp;
							<asp:Button ID="btnSearchLocations" runat="server" CssClass="lgButton" OnClick="btnSubmit_Click" Text="Search" />
						</td>
						<td width="110"></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td align="right" valign="bottom" width="300">
							<span class="sort-nav">
								<asp:Panel ID="pnlPages" runat="server">
									<asp:HyperLink ID="lnkLast" runat="server" Visible="false">&lt; previous</asp:HyperLink><asp:Label
										ID="lblLast" runat="server" Visible="false" CssClass="light-gray">&lt; previous</asp:Label>
									|
									<asp:Repeater ID="rptPage" runat="server" OnItemDataBound="rptPage_ItemDataBound">
										<ItemTemplate>
											<asp:HyperLink ID="lnkPageNum" runat="server" Visible="false" /><asp:Label ID="lblPageNum"
												CssClass="black" runat="server" Visible="false" />
										</ItemTemplate>
									</asp:Repeater>
									|
									<asp:HyperLink ID="lnkNext" runat="server" Visible="false">next &gt;</asp:HyperLink><asp:Label
										ID="lblNext" runat="server" Visible="false" CssClass="light-gray">next &gt;</asp:Label>
									|
									<asp:HyperLink ID="lnkAll" runat="server" Visible="false">View All</asp:HyperLink><asp:Label
										ID="lblAll" runat="server" Visible="false" CssClass="light-gray">View All</asp:Label>
								</asp:Panel>
								<asp:Label ID="pageArea" runat="server" />
							</span>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<br />
							<i>
								<asp:Label ID="lblProx" runat="server" Visible="false" CssClass="proxLabel">Showing results in proximity to {0}.</asp:Label>
								<asp:Label ID="errorLabel" runat="server" Visible="false"><b>Error:</b> Zip code {0} was in an unrecognized format.</asp:Label></i>
								&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lnkShowAll" Text="(clear)" runat="server" OnClick="lnkClear_Click" Visible="false"></asp:LinkButton>
						</td>
						<td colspan="3">&nbsp;</td>
					</tr>
				</table>
				<div class="locations">
					<asp:Repeater ID="rptLocationInfo" runat="server" OnItemDataBound="rptLocationInfo_ItemDataBound">
						<ItemTemplate>
							<asp:Table Width="428" CellSpacing="0" CellPadding="0" class="address" runat="server" ID="tblCurrentItem">
								<asp:TableRow>
									<asp:TableCell bgcolor="#E7EFF7">
										&nbsp;
									</asp:TableCell>
									<asp:TableCell bgcolor="#E7EFF7">
										<h2><asp:Label ID="lblLocationName" runat="server" /></h2>
									</asp:TableCell>
									<asp:TableCell align="right" bgcolor="#E7EFF7">
										<div id="divDot" runat="server"><span class="indot"><asp:Literal ID="litDotNumber" runat="server"></asp:Literal></span></div>
									</asp:TableCell>
									<asp:TableCell bgcolor="#E7EFF7">
										&nbsp;
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow>
									<asp:TableCell Width="10">
										&nbsp;
									</asp:TableCell>
									<asp:TableCell Width="234">
										<p>
											<asp:Label runat="server" ID="lblAddress1"></asp:Label><br />
											<asp:Label runat="server" ID="lblAddress2"></asp:Label>
											<asp:Label runat="server" ID="lblCity"></asp:Label>,
											<asp:Label runat="server" ID="lblState"></asp:Label>&nbsp;
											<asp:Label runat="server" ID="lblZip"></asp:Label>
										</p>
										<p>
											<a class="directions" id="lnkGetDirections" runat="server" target="_blank">Get Directions</a>
										</p>
									</asp:TableCell>
									<asp:TableCell Width="124" valign="top">
										<p>
											<span class="sizeleft"><strong>p:</strong></span>
											<span class="sizeright"><asp:Label runat="server" ID="lblPhone"></asp:Label></span><br />
											<span class="sizeleft"><strong>f:</strong></span>
											<span class="sizeright"><asp:Label runat="server" ID="lblFax"></asp:Label></span><br />
											<asp:HyperLink ID="lnkViewDetails" runat="server">View Details</asp:HyperLink>
											<br />
											<i><asp:Label ID="lblDistance" runat="server" Visible="false" /></i>
										</p>
									</asp:TableCell>
									<asp:TableCell Width="10">
										&nbsp;
									</asp:TableCell>
								</asp:TableRow>
							</asp:Table>
						</ItemTemplate>
					</asp:Repeater>
				</div>
				<div class="map">
					<cg:GoogleMap runat="server" id="ctlGoogleMap"></cg:GoogleMap>
				</div>
				<div class="clear_both"></div>
			</asp:Panel>
		</div>
		<div class="clear_both"></div>
	</div>
</asp:Content>
