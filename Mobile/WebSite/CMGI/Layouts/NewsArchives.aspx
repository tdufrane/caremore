﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewsArchives.aspx.cs" Inherits="CMGI.Layouts.NewsArchives" MasterPageFile="Base.Master" %>

<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="/CMGI/Sublayouts/LeftNavigation.ascx" %>

<asp:content runat="server" id="Content1" contentplaceholderid="head">
</asp:content>

<asp:content runat="server" id="PatientBody" contentplaceholderid="MainBodyContent">
<div class="container">
    
	    <!-- begin left side navigation -->
        <cg:LeftNavigation runat="server" />
        <!-- end left side navigation -->
        
        
        <div class="copy">
        

        	<div class="title">
            	<h1><a title="top"><cg:Text Field="Title" runat="server" /></a></h1>
            </div>


        <asp:Repeater ID="rptArticles" runat="server" 
                onitemdatabound="rptArticles_ItemDataBound">
            <ItemTemplate>
                <div class="story">
                    <p class="big-blue"><span style="color:Black;"><asp:Label ID="newsDate" runat="server" /></span> - <asp:Label ID="newsHeadline" runat="server" /></p>
            
                    <asp:Label ID="newsBody" runat="server" />
                </div>
                <div class="clear_both"></div>
            </ItemTemplate>

            <SeparatorTemplate>
            <hr height="1" />
            </SeparatorTemplate>
        </asp:Repeater>

        <div class="right"><a href="#top">^ To top</a></div>
      
      
      <div class="clear_both"></div>
</div>

<div class="clear_both"></div>
    </div>
</asp:content>
