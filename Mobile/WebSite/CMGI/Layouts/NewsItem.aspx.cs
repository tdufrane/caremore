﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;

namespace CMGI.Layouts
{
	public partial class NewsItem : System.Web.UI.Page
	{
		protected void Page_PreRender(object sender, EventArgs e)
		{
			Item currItem = Sitecore.Context.Item;
			Master.SetPageTitle(currItem["Teaser"]);
		}
	}
}
