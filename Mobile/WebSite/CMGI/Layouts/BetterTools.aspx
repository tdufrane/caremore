﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BetterTools.aspx.cs" Inherits="CMGI.Layouts.BetterTools" MasterPageFile="Base.Master" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="/CMGI/Sublayouts/LeftNavigation.ascx" %>

<asp:content runat="server" id="Content1" contentplaceholderid="head">
</asp:content>

<asp:content runat="server" id="PatientBody" contentplaceholderid="MainBodyContent">
<div class="container">
      
     
	   <!-- begin left side navigation -->
        <cg:LeftNavigation runat="server" />
        <!-- end left side navigation -->
         
<div class="copy">

<div class="title">
<h1><cg:Text Field="Title" runat="server" /></h1>
</div>

<cg:Text Field="Body" runat="server" /> <br />

<cg:Image Field="Image" runat="server" />

</div>
      
      
      
      <div class="clear_both"></div>
      </div>
</asp:content>