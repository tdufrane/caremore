﻿<%@ Page Language="C#" MasterPageFile="Base.Master" AutoEventWireup="true" CodeBehind="BioLayout.aspx.cs" Inherits="CMGI.Layouts.BioLayout" %>
<%@ MasterType VirtualPath="Base.Master" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="/CMGI/Sublayouts/LeftNavigation.ascx" %>

<asp:content runat="server" id="Content1" contentplaceholderid="head">
</asp:content>

<asp:content runat="server" id="PatientBody" contentplaceholderid="MainBodyContent">
<div class="container">
	<!-- begin left side navigation -->
	<cg:LeftNavigation runat="server" />
	<!-- end left side navigation -->

	<div class="copy">
		<div>
			<cg:Image Field="Bio Image" runat="server"/>
		</div>

		<div class="bio">
			<hr />

			<div>
				<div style="float:left;">
					<h4>Location:</h4>

					<asp:Repeater ID="rptAddresses" runat="server" OnItemDataBound="rptAddresses_ItemDataBound">
						<ItemTemplate>
							<p><asp:Label ID="Address" runat="server" /></p>
							<p><asp:HyperLink ID="Link" runat="server" NavigateUrl="" Text="Get Directions" Target="_blank" /></p>
							<p><asp:Label ID="Numbers" runat="server" /></p>
							<p><span class="blue">Office Hours:</span><br /><asp:Label ID="Hours" runat="server" /></p>
						</ItemTemplate>
						<SeparatorTemplate><hr /></SeparatorTemplate>
					</asp:Repeater>
				</div>

				<div style="float:right;text-align:center;">
					<asp:PlaceHolder ID="MeetDocSection" runat="server">
						<h4><asp:Label runat="server" ID="MeetDocTitle" />:</h4>
						<p><asp:HyperLink runat="server" ID="MeetDocLink"><sc:Image runat="server" ID="MeetDocImage" Field="Video Thumb" MaxWidth="120" /></asp:HyperLink></p>
					</asp:PlaceHolder>

					<p><cg:Image Field="Special Image" runat="server"/></p>
				</div>
			
				<div class="clear_both"></div>
			</div>

			<hr />

			<h4>About <cg:Text Field="About" runat="server" />:</h4>

			<cg:Text Field="Bio" runat="server" ID="BioText" />

			<hr />

			<p><a href="javascript:history.go(-1);">&lt; Go Back</a></p>
		</div>
	</div>
	<div class="clear_both"></div>
</div>
</asp:content>
