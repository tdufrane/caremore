﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CMGI.BL;
using CMGI.BL.ListItem;
using Sitecore.Data.Items;
using Website;

namespace CMGI.Layouts
{
	public partial class PostVisitPage : System.Web.UI.Page
	{
		#region Private Variables

		private string thisSection = null;
		private string lastSection = null;
		private const string NotApplicable = "<em>n/a</em>";

		#endregion

		#region Page Events

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LoadForm();
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				// Prevent multiple submissions
				if (Session["PostVisitPage"] == null)
					SubmitForm();
				else
					CareMoreUtilities.DisplayNoMultiSubmit(plcSummary);

				pgBody.Visible = false;
				pnlForm.Visible = false;
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(plcSummary, ex);
			}
		}

		protected void RptSurvey_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				Panel pnlSection = (Panel)e.Item.FindControl("pnlSection");

				PostVisitSurveyQuestionsResult question = (PostVisitSurveyQuestionsResult)e.Item.DataItem;
				thisSection = question.PostVisitSectionName;

				if (string.IsNullOrWhiteSpace(thisSection))
				{
					pnlSection.Visible = false;
				}
				else if ((lastSection != null) && (thisSection.Equals(lastSection)))
				{
					pnlSection.Visible = false;
				}

				lastSection = thisSection;
			}
		}

		#endregion

		#region Private methods

		private void LoadForm()
		{
			Session["PostVisitPage"] = null;
			LoadPhysicians();
			LoadSurvey();
		}

		private void LoadPhysicians()
		{
			Item[] physList = Sitecore.Context.Database.SelectItems(Utility.PHYSICIANS_PATH + "//*[@@templatename='Physician']");
			IEnumerable<Item> ordered = physList.OrderBy(i => i["Last Name"]);
			foreach (Item physician in ordered)
			{
				ListItem listItem = new ListItem();
				listItem.Text = string.Format("{0} {1}", physician["First Name"], physician["Last Name"]);
				listItem.Value = listItem.Text;
				this.ddlDoctors.Items.Add(listItem);
			}
		}

		private void LoadSurvey()
		{
			CmgiApplicationDataContext db = new CmgiApplicationDataContext();
			var result = db.PostVisitSurveyActive().FirstOrDefault();
			if (result == null)
			{
				//TODO: hide form
			}
			else
			{
				short surveyId = result.PostVisitSurveyId;

				hidSurveyId.Value = surveyId.ToString();

				var questions = db.PostVisitSurveyQuestions(surveyId);
				rptSurvey.DataSource = questions;
				rptSurvey.DataBind();
			}
		}

		private void SubmitForm()
		{
			PostVisit visit = SubmitVisitCore();
			List<PostVisitAnswer> answers = SubmitVisitAnswers(visit.PostVisitId);
			visit.PostVisitAnswers.AddRange(answers);

			EmailForm(visit);

			Session["PostVisitPage"] = DateTime.Now;

			CareMoreUtilities.DisplayStatus(plcSummary, Sitecore.Context.Item["Email Thank You"]);
		}

		private PostVisit SubmitVisitCore()
		{

			PostVisit visit = new PostVisit()
			{
				EnteredDate = DateTime.Parse(txtDate.Text),
				Doctor = ddlDoctors.SelectedItem.Text,
				Name = string.IsNullOrWhiteSpace(txtName.Text) ? null : txtName.Text,
				ContactInfo = string.IsNullOrWhiteSpace(txtContactInfo.Text) ? null : txtContactInfo.Text,
				OkToContact = rblContactYou.SelectedValue == "Yes",
				Comments = txtComments.Text,
				CreatedDate = DateTime.Now,
				IpAddress = Request.UserHostAddress
			};

			CmgiApplicationDataContext db = new CmgiApplicationDataContext();
			db.PostVisits.InsertOnSubmit(visit);
			db.SubmitChanges();

			return visit;
		}

		private List<PostVisitAnswer> SubmitVisitAnswers(int postVisitId)
		{
			List<PostVisitAnswer> answers = new List<PostVisitAnswer>();

			foreach (RepeaterItem item in rptSurvey.Items)
			{
				// Get and save changes for DB first
				HiddenField hidQuestionId = (HiddenField)item.FindControl("hidQuestionId");
				int questionId = int.Parse(hidQuestionId.Value);

				HtmlInputRadioButton rbAnswerA = (HtmlInputRadioButton)item.FindControl("rbAnswerA");
				HtmlInputRadioButton rbAnswerB = (HtmlInputRadioButton)item.FindControl("rbAnswerB");
				HtmlInputRadioButton rbAnswerC = (HtmlInputRadioButton)item.FindControl("rbAnswerC");
				HtmlInputRadioButton rbAnswerD = (HtmlInputRadioButton)item.FindControl("rbAnswerD");
				HtmlInputRadioButton rbAnswerE = (HtmlInputRadioButton)item.FindControl("rbAnswerE");

				HtmlInputRadioButton rbAnswered = null;
				if (rbAnswerA.Checked)
					rbAnswered = rbAnswerA;
				else if (rbAnswerB.Checked)
					rbAnswered = rbAnswerB;
				else if (rbAnswerC.Checked)
					rbAnswered = rbAnswerC;
				else if (rbAnswerD.Checked)
					rbAnswered = rbAnswerD;
				else if (rbAnswerE.Checked)
					rbAnswered = rbAnswerE;

				string answerValue;
				if (rbAnswered == null)
					answerValue = string.Empty;
				else
					answerValue = rbAnswered.Attributes["title"];

				PostVisitAnswer answer = new PostVisitAnswer()
				{
					PostVisitId = postVisitId,
					PostVisitQuestionId = questionId,
					PostVisitAnswerValue = answerValue,
				};

				CmgiApplicationDataContext db = new CmgiApplicationDataContext();
				db.PostVisitAnswers.InsertOnSubmit(answer);
				db.SubmitChanges();

				// Save rest for later email
				// Added here to avoid causing ref update failure
				Panel pnlSection = (Panel)item.FindControl("pnlSection");
				PostVisitSection section = new PostVisitSection()
				{
					PostVisitSectionName = pnlSection.Visible ? ((DataBoundLiteralControl)pnlSection.Controls[0]).Text : string.Empty
				};

				Label lblQuestion = (Label)item.FindControl("lblQuestion");
				PostVisitQuestion question = new PostVisitQuestion()
				{
					PostVisitQuestionId = questionId,
					PostVisitQuestionText = lblQuestion.Text,
					PostVisitSection = section
				};

				answer.PostVisitQuestion = question;
				answers.Add(answer);
			}

			return answers;
		}

		private void EmailForm(PostVisit visit)
		{
			Item currentItem = Sitecore.Context.Item;

			MailMessage message = new MailMessage();
			message.From = new MailAddress(currentItem["Email From"]);
			message.To.Add(new MailAddress(currentItem["Email To"]));

			if (!string.IsNullOrWhiteSpace(currentItem["Email CC"]))
				message.CC.Add(new MailAddress(currentItem["Email CC"]));

			if (!string.IsNullOrWhiteSpace(currentItem["Email BCC"]))
				message.Bcc.Add(new MailAddress(currentItem["Email BCC"]));

			message.Subject = currentItem["Email Subject"];

			StringBuilder body = new StringBuilder();

			body.AppendLine(Utility.MailHeader(message.Subject));

			body.AppendFormat("<p>A post-visit questionaire was submitted on <b>{0:MM/dd/yyyy}</b> at <b>{0:hh:mm tt}</b>.</p>\n", visit.CreatedDate);

			body.AppendLine(Utility.MailTableOpen);
			body.Append(Utility.MailTableRow("Date:", visit.EnteredDate.ToString("MM/dd/yyyy}")));
			body.Append(Utility.MailTableRow("Doctor:", visit.Doctor));
			body.Append(Utility.MailTableRow("Name:", string.IsNullOrWhiteSpace(visit.Name) ? NotApplicable : visit.Name));

			if (string.IsNullOrWhiteSpace(visit.ContactInfo))
			{
				body.Append(Utility.MailTableRow("Contact Info:", NotApplicable));
				body.Append(Utility.MailTableRow("OK To Contact:", NotApplicable));
			}
			else
			{
				body.Append(Utility.MailTableRow("Contact Info:", visit.ContactInfo));
				body.Append(Utility.MailTableRow("OK To Contact:", visit.OkToContact ? "Yes" : "No"));
			}

			body.AppendLine(Utility.MailTableClose);
			body.AppendLine(Utility.MailBreak);
			body.AppendLine(Utility.MailTableOpen);

			thisSection = null;
			lastSection = null;

			foreach (PostVisitAnswer item in visit.PostVisitAnswers)
			{
				thisSection = item.PostVisitQuestion.PostVisitSection.PostVisitSectionName;

				if (!string.IsNullOrWhiteSpace(thisSection))
				{
					if ((lastSection == null) || (!thisSection.Equals(lastSection)))
					{
						body.Append(Utility.MailTableRow(thisSection));
					}
				}
				lastSection = thisSection;

				body.Append(Utility.MailTableRow(item.PostVisitQuestion.PostVisitQuestionText, item.PostVisitAnswerValue));
			}

			body.AppendLine(Utility.MailTableClose);

			string comments;
			if (string.IsNullOrEmpty(visit.Comments))
				comments = "<i>none</i>";
			else
				comments = visit.Comments;

			body.AppendFormat("<p class='bold'><span class='shaded'>Comments:</span><br/>{0}</p>", comments);
			body.AppendLine(Utility.MailFooter());

			message.Body = body.ToString();
			message.IsBodyHtml = true;

			//Send email
			CareMoreUtilities.SendEmail(message);
		}

		#endregion
	}
}
