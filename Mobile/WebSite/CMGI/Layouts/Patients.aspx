﻿<%@ Page Title="" Language="C#" MasterPageFile="Base.Master" AutoEventWireup="true" CodeBehind="Homepage.aspx.cs" Inherits="CMGI.Layouts.Homepage" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="/CMGI/Sublayouts/LeftNavigation.ascx" %>

<asp:content runat="server" id="Content1" contentplaceholderid="head">
</asp:content>

<asp:content runat="server" id="PatientBody" contentplaceholderid="MainBodyContent">
<div class="container">
      
      <!-- Left nav -->
      <cg:LeftNavigation runat="server" />
      <!-- End left nav -->

<div>This is the patient page.</div>
      
      
      
      <div class="clear_both"></div>
      </div>
</asp:content>