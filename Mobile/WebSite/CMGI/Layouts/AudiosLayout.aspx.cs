﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Links;
using Sitecore.Resources.Media;
using CMGI.BL.ListItem;
using CMGI.Sublayouts;
using Website;

namespace CMGI.Layouts
{
	public partial class AudiosLayout : System.Web.UI.Page
	{
		List<Item> audioList = new List<Item>();

		protected void Page_Load(object sender, EventArgs e)
		{
			GetAudioItems();
			BuildSlider();

			Item audioItem = null;
			string audioID = Request.QueryString["aid"];

			if (!string.IsNullOrWhiteSpace(audioID))
			{
				// look for id
				Guid guid = new Guid(audioID);
				audioItem = audioList.Find(
					delegate(Item item)
					{
						return item.ID.Guid == guid;
					}
				);
			}

			if (audioItem == null)
			{
				// pick random audio
				Random random = new Random();
				audioItem = audioList[random.Next(audioList.Count)];
			}

			AudioItem audio = new AudioItem(audioItem);

			if (audio.Picture.MediaItem == null)
			{
				tdImage.Visible = false;
			}
			else
			{
				MediaItem picture = new MediaItem(audio.Picture.MediaItem);
				imgDrPicture.Src = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(picture));
				imgDrPicture.Alt = picture.Alt;
			}

			lblTitle.Text = audio.Title;

			if (audio.File.TargetItem == null)
			{
				apcAudioPlayerItem.Visible = false;
			}
			else
			{
				MediaItem audioMedia = new MediaItem(audio.File.TargetItem);
				string url = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(audioMedia));

				apcAudioPlayerItem.Path = url;
			}
		}

		private void BuildSlider()
		{
			sliderMultiLV.DataSource = audioList;
			sliderMultiLV.DataBind();
		}

		private void GetAudioItems()
		{
			Item currItem = Sitecore.Context.Item;

			var children = currItem.Axes.GetDescendants();
			var audios = from child in children
			             where child.TemplateName == "Audio Item"
			             select child;

			foreach (Item item in audios)
			{
				audioList.Add(item);
			}
		}

		protected void sliderMultiLV_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				ListViewDataItem listViewDataItem = (ListViewDataItem)e.Item;
				Item item = (Item)listViewDataItem.DataItem;

				// Audio thumb
				Sitecore.Web.UI.WebControls.Image scImage = (Sitecore.Web.UI.WebControls.Image)e.Item.FindControl("scImage");
				scImage.Item = item;

				// link
				HyperLink hl = (HyperLink)e.Item.FindControl("hl");
				hl.NavigateUrl = string.Format("{0}?aid={1}#{2}",
					LinkManager.GetItemUrl(Sitecore.Context.Item),
					item.ID,
					Request.Url.Fragment);

				// link text
				Sitecore.Web.UI.WebControls.Text scText = (Sitecore.Web.UI.WebControls.Text)hl.FindControl("scText");
				scText.Item = item;

				// break on each 4th
				if ((e.Item.DataItemIndex + 1) % 4 == 0)
				{
					Literal litBreak = (Literal)e.Item.FindControl("litBreak");
					litBreak.Visible = true;
				}
			}
		}
	}
}
