﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Links;
using Sitecore.Resources.Media;
using CMGI.BL.ListItem;
using CMGI.Sublayouts;
using Website;

namespace CMGI.Layouts
{
	public partial class RadioInterviewsLayout : System.Web.UI.Page
	{
		protected void rptAudioItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				AudioItem item = e.Item.DataItem as AudioItem;

				if (item != null)
				{
					HtmlTableCell tdImage = e.Item.FindControl("tdImage") as HtmlTableCell;
					HtmlImage imgDrPicture = e.Item.FindControl("imgDrPicture") as HtmlImage;
					Label lblTitle = e.Item.FindControl("lblTitle") as Label;
					AudioPlayer player = e.Item.FindControl("apcAudioPlayerItem") as AudioPlayer;

					if (item.Picture.MediaItem == null)
					{
						tdImage.Visible = false;
					}
					else
					{
						MediaItem picture = new MediaItem(item.Picture.MediaItem);
						imgDrPicture.Src = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(picture));
						imgDrPicture.Alt = picture.Alt;
					}

					lblTitle.Text = item.Title;

					if (item.File.TargetItem == null)
					{
						player.Visible = false;
					}
					else
					{
						MediaItem audio = new MediaItem(item.File.TargetItem);
						string url = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(audio));

						player.Path = url;
					}
				}
			}
		}

		protected void rptAudioItems_PreRender(object sender, EventArgs e)
		{
			rptAudioItems.DataSource = GetAudioItems();
			rptAudioItems.DataBind();
		}

		private AudioItem[] GetAudioItems()
		{
			Item currItem = Sitecore.Context.Item;

			var children = currItem.Axes.GetDescendants();
			var audios = from child in children
						 where child.TemplateName == "Audio Item"
						 select child;

			List<AudioItem> audioList = new List<AudioItem>();

			foreach (Item item in audios)
			{
				AudioItem audio = new AudioItem(item);
				audioList.Add(audio);
			}

			if (audios.Count() == 0)
			{
				lblNoResults.Visible = true;
				return new AudioItem[] { };
			}
			else
			{
				lblNoResults.Visible = false;
				return audioList.ToArray();
			}
		}
	}
}
