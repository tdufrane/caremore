﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using CMGI.BL;
using System.Web.UI.HtmlControls;

namespace CMGI.Layouts
{
    public partial class Testimonials : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //rptPatTest.DataSource = GetPatientTestimonials();
            //rptPatTest.DataBind();

            //rptDocTest.DataSource = GetDoctorTestimonials();
            //rptDocTest.DataBind();

            //Item parent = ItemIds.GetItem("TESTIMONIALS_ID");
            //title.Text = parent["Title"];
            //lblIntro.Text = parent["Body"];

        }

        public List<Item> GetPatientTestimonials()
        {
            Item[] tests = Sitecore.Context.Database.SelectItems(Utility.TESTIMONIALS_PATH + "//*[@@templatename='Testimonial']");
            var s = from c in tests where
                               c["Type"] == "Patient" 
                           select c;

            return s.ToList();
        }

        public List<Item> GetDoctorTestimonials()
        {
            Item[] tests = Sitecore.Context.Database.SelectItems(Utility.TESTIMONIALS_PATH + "//*[@@templatename='Testimonial']");
            var s = from c in tests
                    where
                        c["Type"] == "Doctor"
                    select c;

            return s.ToList();
        }

        protected void rptPatTest_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Item item = (Item)e.Item.DataItem;
                Label first = e.Item.FindControl("patFirst") as Label;
                Label last = e.Item.FindControl("patLast") as Label;
                Label city = e.Item.FindControl("patCity") as Label;
                Label state = e.Item.FindControl("patState") as Label;
                Image image = e.Item.FindControl("patImage") as Image;
                HyperLink lnk = e.Item.FindControl("lnkPat") as HyperLink;

                Panel para = e.Item.FindControl("testimonial") as Panel;
                

                first.Text = item["First Name"];
                last.Text = item["Last Name"];
                city.Text = item["City"];
                state.Text = item["State"];

                Sitecore.Data.Fields.ImageField imgf = item.Fields["Picture"];
                string thumb = "~/" + Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgf.MediaItem) + "?w=50";
                image.ImageUrl = thumb;

                if (Sitecore.Context.Item.ID != item.ID)
                {
                    lnk.NavigateUrl = Sitecore.Links.LinkManager.GetItemUrl(item);
                    para.CssClass = "testlink";
                }
                else
                    para.CssClass = "nolink";

            }
        }

        protected void rptDocTest_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Item item = (Item)e.Item.DataItem;
                Label first = e.Item.FindControl("docFirst") as Label;
                Label last = e.Item.FindControl("docLast") as Label;
                Label city = e.Item.FindControl("docCity") as Label;
                Label state = e.Item.FindControl("docState") as Label;
                Image image = e.Item.FindControl("docImage") as Image;
                HyperLink lnk = e.Item.FindControl("lnkDoc") as HyperLink;
                Panel para = e.Item.FindControl("testimonial") as Panel;

                first.Text = item["First Name"];
                last.Text = item["Last Name"];
                city.Text = item["City"];
                state.Text = item["State"];

                Sitecore.Data.Fields.ImageField imgf = item.Fields["Picture"];
                string thumb = "~/" + Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgf.MediaItem) + "?w=50";
                image.ImageUrl = thumb;

                if (Sitecore.Context.Item.ID != item.ID)
                {
                    lnk.NavigateUrl = Sitecore.Links.LinkManager.GetItemUrl(item);
                    para.CssClass = "testlink";
                }
                else
                    para.CssClass = "nolink";
            }
        }
    }
}