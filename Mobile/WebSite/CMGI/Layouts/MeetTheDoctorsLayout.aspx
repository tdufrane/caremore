﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MeetTheDoctorsLayout.aspx.cs" Inherits="CMGI.Layouts.MeetTheDoctorsLayout" MasterPageFile="Base.Master" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>
<%@ Register Src="../Sublayouts/VideoPlayer.ascx" TagPrefix="vpc" TagName="VideoPlayer" %>

<asp:Content runat="server" id="Content1" contentplaceholderid="head">
	<link href="/player/jplayer.blue.monday.css" rel="stylesheet" type="text/css" />
	<script src="/js/jquery.jplayer.min.js" type="text/javascript"></script>
</asp:Content>

<asp:Content runat="server" id="PatientBody" contentplaceholderid="MainBodyContent">
<div class="container">
	<div class="title"><h1><sc:Text runat="server" Field="Title" Item="<%# Sitecore.Context.Item %>" /></h1></div>

	<div class="feature">
		<div class="video">
			<vpc:VideoPlayer ID="VideoPlayerDoctor" runat="server" />
		</div>
		<div class="video-content">
			<h2><sc:Text runat="server" ID="scFeatureVideoTitle" Field="Title" /></h2>			<p><sc:Text runat="server" ID="scFeatureVideoShortInfo" Field="Short Info" /></p>
		</div>
	</div>

	<div class="carousel-list">
		<div class="slider-wrap doctor">
			<asp:ListView id="sliderMultiLV" runat="server" OnItemDataBound="sliderMultiLV_ItemDataBound">
				<LayoutTemplate>
					<ul id="slider-multi">
						<li style="margin-left:50px;">
							<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
						</li>
					</ul>
				</LayoutTemplate>
				<ItemTemplate>
					<div><sc:Image runat="server" ID="scImage" Field="Video Thumb" MaxWidth="120" /><asp:HyperLink runat="server" ID="hl"><sc:Text runat="server" ID="scText" Field="Title" /></asp:HyperLink></div>
					<asp:Literal runat="server" ID="litBreak" Text="</li><li>" Visible="false" />
				</ItemTemplate>
			</asp:ListView>
		</div>
	</div>
	<div class="clear_both"></div>
</div>

<script type="text/javascript">
	$j(function () {
		$j('#slider-multi').anythingSlider({
			expand: true,
			buildNavigation: false,
			buildStartStop: false,
			showMultiple: 2,
			changeBy: 2
		});
	});
</script>
</asp:Content>
