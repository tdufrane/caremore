﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using CMGI.BL;
using CMGI.BL.ListItem;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;

namespace CMGI.Layouts
{
	public partial class Physicians : System.Web.UI.Page
	{
		// For paging
		private const int perPage = 5;

		private int PageNumber;

		private bool ViewAll;

		protected void Page_Load(object sender, EventArgs e)
		{
			txtSearch.Attributes.Add("alt", "Last Name");
			txtZip.Attributes.Add("alt", "Zip Code");

			FillSpecialties();

			if (!IsPostBack)
			{
				ParseQuery();
			}

			lvPhysicians.DataSource = GetPhysicians();
			lvPhysicians.DataBind();
		}

		private NameValueCollection ParseQuery()
		{
			var nvc = Request.QueryString;

			if (nvc["q"] != null)
			{
				txtSearch.Text = nvc["q"];
			}

			if (nvc["zip"] != null)
			{
				txtZip.Text = nvc["zip"];
			}

			if (nvc["sort"] != null)
			{
				int sindex = int.Parse(nvc["sort"]);
				if (sindex >= 0)
					sortBy.SelectedIndex = sindex;
			}

			if (nvc["p"] != null)
			{
				int pagenum = int.Parse(nvc["p"]);
				PageNumber = pagenum;
			}
			else
			{
				PageNumber = 1;
			}

			if (nvc["all"] != null && nvc["all"] == "1")
				ViewAll = true;
			else
				ViewAll = false;

			return nvc;
		}

		private String ConstructQueryString(NameValueCollection parameters)
		{
			List<string> items = new List<string>();

			foreach (String name in parameters)
				items.Add(String.Concat(name, "=", System.Web.HttpUtility.UrlEncode(parameters[name])));

			return String.Join("&", items.ToArray());
		}

		private void UpdatePageArea(int count)
		{
			var nvc = new NameValueCollection(Request.QueryString);
			if (nvc["all"] != null) nvc.Remove("all");
			if (nvc["p"] != null) nvc.Remove("p");

			string q = ConstructQueryString(nvc);

			int pages = (int)Math.Ceiling((decimal)count / perPage);

			if (PageNumber > 1)
			{
				lnkLast.Visible = true;
				lnkLast.NavigateUrl = Request.Path + "?p=" + (PageNumber - 1) + "&" + q;
				lnkLast2.Visible = true;
				lnkLast2.NavigateUrl = Request.Path + "?p=" + (PageNumber - 1) + "&" + q;
			}
			else
			{
				lblLast.Visible = true;
				lblLast2.Visible = true;
			}

			List<int> pageSrc = new List<int>();
			for (int x = 1; x <= pages; x++)
				pageSrc.Add(x);

			rptPage.DataSource = pageSrc;
			rptPage.DataBind();
			rptPage2.DataSource = pageSrc;
			rptPage2.DataBind();

			if (PageNumber < pages)
			{
				lnkNext.Visible = true;
				lnkNext.NavigateUrl = Request.Path + "?p=" + (PageNumber + 1) + "&" + q;
				lnkNext2.Visible = true;
				lnkNext2.NavigateUrl = Request.Path + "?p=" + (PageNumber + 1) + "&" + q;
			}
			else
			{
				lblNext.Visible = true;
				lblNext2.Visible = true;
			}

			if (ViewAll)
			{
				lblAll.Visible = true;
				lblAll2.Visible = true;
			}
			else
			{
				lnkAll.Visible = true;
				lnkAll.NavigateUrl = Request.Path + "?all=1";
				lnkAll2.Visible = true;
				lnkAll2.NavigateUrl = Request.Path + "?all=1";
			}

		}

		public bool ContainsSpec(string raw, string query)
		{
			string[] guids = raw.Split('|');
			foreach (string guid in guids)
			{
				if (guid != "")
				{
					Item i = Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(guid));
					if (i["Specialty"] == query)
						return true;
				}
			}
			return false;
		}

		public List<PhysicianItem> GetPhysicians()
		{
			string searchTerm = "";

			if (txtSearch.Text != "" && txtSearch.Text != txtSearch.Attributes["alt"])
			{
				searchTerm = txtSearch.Text;
				ViewAll = true;
			}

			int pageNum = PageNumber;

			List<PhysicianItem> phys = new List<PhysicianItem>();

			int c = 1;
			Item[] physList = Sitecore.Context.Database.SelectItems(Utility.PHYSICIANS_PATH + "//*[@@templatename='Physician']");
			IEnumerable<Item> ordered = physList.OrderBy(i => i["Last Name"]);//;.OrderBy(i => i["First Name"]);
			//IEnumerable<Item> ordered = null;

			if (sortBy.SelectedIndex != 0 && sortBy.SelectedIndex != 1)
			{
				//string srt = sortBy.Items[int.Parse(nvc["sort"])].Value;
				ordered = ordered.OrderBy(i => !ContainsSpec(i["Specialty"], sortBy.SelectedValue));
			}

			IEnumerable<Item> locations = Sitecore.Context.Database.SelectItems(Utility.FIND_LOCATIONS_PATH + "//*[@@templateID='{18D72752-668F-452B-BB4C-CB26307AF6AC}']");

			if (txtZip.Text != "" && txtZip.Text != txtZip.Attributes["alt"])
			{
				var ziploc = Geocoding.CallGeoWS(txtZip.Text + ", USA");
				if (ziploc.Results.Count() > 0)
				{
					var zlat = ziploc.Results[0].Geometry.Location.Lat;
					var zlng = ziploc.Results[0].Geometry.Location.Lng;

					DistanceComparer<Item> comp = new DistanceComparer<Item>(zlat, zlng);
					locations = locations.OrderBy(i => i, comp);

					List<Item> physicians = new List<Item>();
					foreach (Item location in locations)
					{
						foreach (Item physicianAtLocation in location.Children)
						{
							if (physicianAtLocation.Fields["Physician"].HasValue)
							{
								Item physicianItem = Sitecore.Context.Database.SelectSingleItem(Utility.PHYSICIANS_PATH + string.Format("//*[@@ID='{0}']", physicianAtLocation.Fields["Physician"].Value));
								if (physicians == null || physicians.Where(x => x.ID == physicianItem.ID).ToList().Count == 0)
								{
									physicians.Add(physicianItem);
								}
							}
						}
					}

					ordered = physicians.ToList();//.OrderBy(i => i["Last Name"]).OrderBy(i => i["First Name"]);
				}
			}

			foreach (Item e in ordered)
			{
				if (ViewAll || c >= perPage * (pageNum - 1) + 1)
				{
					// Get physician addresses
					var physician = Sitecore.Context.Database.SelectItems(Utility.FIND_LOCATIONS_PATH + string.Format("//*[@Physician='{0}']", e.ID)).ToList();

					List<AddressItem> addressItems = new List<AddressItem>(); ;
					foreach (Item physicianAtLocation in physician)
					{
						AddressItem addressItem = new AddressItem(physicianAtLocation.Parent, physicianAtLocation.Fields["Hours"].Value,
																	string.IsNullOrEmpty(physicianAtLocation.Fields["Phone"].Value)
																			? physicianAtLocation.Parent.Fields["Phone"].Value
																			: physicianAtLocation.Fields["Phone"].Value,
																	string.IsNullOrEmpty(physicianAtLocation.Fields["Fax"].Value)
																			? physicianAtLocation.Parent.Fields["Fax"].Value
																			: physicianAtLocation.Fields["Fax"].Value,
																	string.IsNullOrEmpty(physicianAtLocation.Fields["PhysicianSuite"].Value)
																			? string.Empty
																			: physicianAtLocation.Fields["PhysicianSuite"].Value);
						addressItems.Add(addressItem);
						
					}

					if (searchTerm == "")
					{
						phys.Add(new PhysicianItem(e, addressItems.ToArray()));

					}
					else
					{
						if (e["Last Name"].ToLower().Contains(searchTerm.ToLower()))
						{
							phys.Add(new PhysicianItem(e, addressItems.ToArray()));

						}
					}

				}

				if (c >= (perPage * pageNum) && !ViewAll) break;

				c++;
			}

			if (searchTerm != "")
			{
				UpdatePageArea(phys.Count());
			}
			else
			{
				UpdatePageArea(physList.Count());
			}
			return phys;
		}

		private void FillSpecialties()
		{
			if (sortBy.Items.Count == 0)
			{
				sortBy.Items.Add(new ListItem("Specialty", "Specialty"));
				sortBy.Items.Add(new ListItem("Any", "Any"));

				Item[] specs = Sitecore.Context.Database.SelectItems(Utility.SPECIALTY_PATH + "//*[@@templatename='Specialty']");
				foreach (Item e in specs)
				{
					sortBy.Items.Add(new ListItem(e["Specialty"], e["Specialty"]));
				}
			}
		}

		protected void lvPhysicians_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				PhysicianItem p = (PhysicianItem)e.Item.DataItem;

				Label firstName = e.Item.FindControl("PhysFirstName") as Label;
				Label lastName = e.Item.FindControl("PhysLastName") as Label;
				Image pic = e.Item.FindControl("PhysPic") as Image;
				Literal specialty = e.Item.FindControl("PhysSpec") as Literal;
				Label addresses = e.Item.FindControl("lblAddresses") as Label;

				firstName.Text = p.FirstName;
				lastName.Text = p.LastName;
				pic.ImageUrl = "~/" + p.Picture;

				specialty.Text = string.Join("<br />\n", p.Specialty);

				if (p.Addresses != null)
				{
					foreach (AddressItem ai in p.Addresses)
					{
						addresses.Text += ai.HTMLAddress() + "<br/><br/>\n\n";
					}
				}

				HyperLink bio = e.Item.FindControl("PhysBio") as HyperLink;
				HyperLink pbio = e.Item.FindControl("PicBio") as HyperLink;
				string url = Sitecore.Links.LinkManager.GetItemUrl(p.Item);

				bio.NavigateUrl = url;
				pbio.NavigateUrl = url;
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			string pageName = Request.PhysicalPath;
			pageName = System.IO.Path.GetFileName(pageName);

			string query = "?p=1";
			query += "&q=" + HttpUtility.UrlEncode(txtSearch.Text);
			query += "&zip=" + HttpUtility.UrlEncode(txtZip.Text);
			query += "&sort=" + HttpUtility.UrlEncode(sortBy.SelectedIndex.ToString());

			Session.Clear();
			Response.Redirect(pageName + query);
		}

		protected void rptPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				int pg = (int)e.Item.DataItem;
				var nvc = new NameValueCollection(Request.QueryString);
				if (nvc["all"] != null) nvc.Remove("all");
				if (nvc["p"] != null) nvc.Remove("p");

				string q = ConstructQueryString(nvc);

				HyperLink pagelink = e.Item.FindControl("lnkPageNum") as HyperLink;
				Label pagelabel = e.Item.FindControl("lblPageNum") as Label;

				if (PageNumber == pg)
				{
					pagelabel.Visible = true;
					pagelabel.Text = pg.ToString();
				}
				else
				{
					pagelink.Visible = true;
					pagelink.Text = pg.ToString();

					pagelink.NavigateUrl = Request.Path + "?p=" + pg.ToString() + "&" + q;
				}
			}
		}
	}
}