﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VideoContent.aspx.cs" Inherits="CMGI.Layouts.VideoContent" MasterPageFile="Base.Master" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register Src="../Sublayouts/VideoPlayer.ascx" TagPrefix="vpc" TagName="VideoPlayer" %>

<asp:Content runat="server" id="ContentHead" contentplaceholderid="head">
	<link href="/player/jplayer.blue.monday.css" rel="stylesheet" type="text/css" />
	<script src="/js/jquery.jplayer.min.js" type="text/javascript"></script>
</asp:Content>

<asp:Content runat="server" id="ContentBody" contentplaceholderid="MainBodyContent">
<div class="container">
	<div class="title"><h1><sc:Text runat="server" Field="Title" /></h1></div>

	<div class="feature">
		<div class="video-content">
			<p><sc:Text runat="server" Field="Body" /></p>
		</div>
		<div class="video">
			<vpc:VideoPlayer ID="VideoPlayerItem" runat="server" />
		</div>
	</div>
</div>
</asp:Content>
