﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PostVisit.aspx.cs" Inherits="CMGI.Layouts.PostVisitPage" MasterPageFile="Base.Master" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="../Sublayouts/LeftNavigation.ascx" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="/css/jquery-ui/cupertino/jquery-ui.min.css" />
	<link rel="stylesheet" href="/CMGI/css/jquery.rating.css" />
	<style type="text/css">
		.ui-datepicker-trigger
		{
			cursor: pointer;
			height: 32px;
			margin-bottom: 8px;
			margin-left: 6px;
			vertical-align: middle;
			width: 32px;
		}
	</style>
	<script src="/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="/CMGI/JS/jquery.rating.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		$j(function () {
			var pickerOptions = {
				showOn: "button",
				buttonImage: "/css/jquery-ui/cupertino/images/calendar.png",
				buttonImageOnly: true
			};
			$j("#txtDate").datepicker(pickerOptions);

			$j(".hover-star").rating({
				focus: function (value, link) {
					var spanId = this.id.substr(0, this.id.length - 9);
					var tip = $j('#' + spanId + "spnQuestion");

					tip[0].data = tip[0].data || tip.html();
					tip.html(link.title || 'value: ' + value);
				},
				blur: function (value, link) {
					var spanId = this.id.substr(0, this.id.length - 9);
					var tip = $j('#' + spanId + "spnQuestion");

					tip.html(starCheckedValue(this.name));
				}
			});
		});

		function starCheckedValue(radioName) {
			var checked = $j("input[name='" + radioName + "']:checked");

			if (checked.length == 0)
				return '';
			else
				return checked[0].title;
		}
	</script>
</asp:Content>

<asp:Content ID="contentBody" ContentPlaceHolderID="MainBodyContent" runat="server">
	<div class="container">
		<!-- begin left side navigation -->
		<cg:LeftNavigation runat="server" id="cmgiLeftNavigation"></cg:LeftNavigation>
		<!-- end left side navigation -->

		<div class="copy">
			<div class="title">
				<h1><cg:Text Field="Title" ID="pgTitle" runat="server" /></h1>
			</div>

			<p><cg:Text Field="Body" ID="pgBody" runat="server" /></p>

			<div><img alt="" src="/CMGI/images/contact-border.jpg" style="height:1px;width:100%;" /></div>

			<asp:PlaceHolder ID="plcSummary" runat="server" Visible="false"></asp:PlaceHolder>

			<asp:Panel ID="pnlForm" runat="server" CssClass="scfForm" DefaultButton="btnSubmitVisit">
				<asp:HiddenField ID="hidSurveyId" runat="server" Value="0" />
				<asp:ValidationSummary ID="valSumSurvey" runat="server" CssClass="scfValidationSummary"
					DisplayMode="BulletList" ForeColor="Red"
					HeaderText="Please fix the following errors and submit again"
					ShowMessageBox="false" ShowSummary="true" />

				<div class="scfSectionContent">
					<div class="scfDatePickerBorder">
						<div class="scfDatePickerLabel">Visit Date (mm/dd/yyyy)</div>
						<asp:TextBox ID="txtDate" runat="server" CssClass="scfDatePickerTextBox" ClientIDMode="Static" MaxLength="10" Width="100" />
						<asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate"
							CssClass="scfRequired" ErrorMessage="Date is required" Display="Dynamic" Text="*" />
						<asp:CompareValidator ID="cvDate" runat="server" ControlToValidate="txtDate"
							CssClass="scfRequired" ErrorMessage="Date is not valid" Display="Dynamic"
							Operator="DataTypeCheck" Text="*" Type="Date" />
					</div>

					<div class="scfDropListBorder">
						<div class="scfDropListLabel">Provider</div>
						<asp:DropDownList ID="ddlDoctors" runat="server" CssClass="scfDropList">
							<asp:ListItem Text="--select one--" Value="" />
						</asp:DropDownList>
						<asp:RequiredFieldValidator ID="rfvDoctor" runat="server" ControlToValidate="ddlDoctors"
							CssClass="scfRequired" ErrorMessage="Doctor is required" Display="Dynamic" Text="*"
							InitialValue="" />
					</div>

					<h3>Please rate the services you recieved</h3>

					<asp:Repeater ID="rptSurvey" runat="server" OnItemDataBound="RptSurvey_ItemDataBound">
						<ItemTemplate>
							<asp:Panel ID="pnlSection" runat="server" CssClass="rating-header"><%# Eval("PostVisitSectionName") %></asp:Panel>
							<div class="rating-items">
								<div class="scfRadioButtonListBorder">
									<div class="scfRadioButtonListLabel"><asp:Label ID="lblQuestion" runat="server" Text='<%# Eval("PostVisitQuestionText") %>' /></div>
									<div class="scfRadioButtonListGeneralPanel">
										<asp:HiddenField ID="hidQuestionId" runat="server" Value='<%# Eval("PostVisitQuestionId") %>' />
										<input name="rdoQuestion" id="rbAnswerA" runat="server" type="radio" class="hover-star" value="" title="Poor" />
										<input name="rdoQuestion" id="rbAnswerB" runat="server" type="radio" class="hover-star" value="" title="Fair" />
										<input name="rdoQuestion" id="rbAnswerC" runat="server" type="radio" class="hover-star" value="" title="Good" />
										<input name="rdoQuestion" id="rbAnswerD" runat="server" type="radio" class="hover-star" value="" title="Very Good" />
										<input name="rdoQuestion" id="rbAnswerE" runat="server" type="radio" class="hover-star" value="" title="Great" />
										<asp:Label id="spnQuestion" runat="server" class="rating-value"></asp:Label>
										<div class="clear_both"></div>
									</div>
								</div>
							</div>
						</ItemTemplate>
					</asp:Repeater>

					<div class="rating-header">&nbsp;</div>
					<div class="rating-items">
					</div>

					<div class="scfSingleLineTextBorder">
						<div class="scfSingleLineTextLabel">Name</div>
						<asp:TextBox ID="txtName" runat="server" CssClass="" MaxLength="50" Width="200" />
					</div>

					<div class="scfSingleLineTextBorder">
						<div class="scfSingleLineTextLabel">Contact Information <span style="font-weight:normal;">(Email or Phone #)</span></div>
						<asp:TextBox ID="txtContactInfo" runat="server" MaxLength="50" Width="300" />
					</div>

					<div class="scfRadioButtonListBorder">
						<div class="scfRadioButtonListLabel">Would you like to us to contact you?</div>
						<asp:RadioButtonList ID="rblContactYou" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
							<asp:ListItem Text="Yes" Value="Yes" />
							<asp:ListItem Text="No" Value="No" Selected="True" />
						</asp:RadioButtonList>
					</div>

					<div class="scfMultipleLineTextBorder">
						<span class="scfMultipleLineTextLabel">Is there anything else you want us to know or would like to share with us?</span><br />
						<span><em>(Your responses are anonymous unless you mention your name)</em></span>
						<div class="scfMultipleLineGeneralPanel">
							<asp:TextBox ID="txtComments" runat="server" Rows="6" Width="600" CssClass="scfMultipleLineTextBox" TextMode="MultiLine" />
						</div>
					</div>

					<div class="scfFooterBorder"></div>
					<div>
						<asp:Button ID="btnSubmitVisit" runat="server" CssClass="button" OnClick="BtnSubmit_Click" Text="Submit" />
					</div>
				</div>
			</asp:Panel>
		</div>
		<div class="clear_both"></div>
	</div>
</asp:Content>
