﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Data.Fields;

namespace CMGI.Layouts
{
	public partial class CareerItem : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Item homeItem = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath);
			Item currentItem = Sitecore.Context.Item;

			emailLink.NavigateUrl = "mailto:" + currentItem["Email"];
		}
	}
}