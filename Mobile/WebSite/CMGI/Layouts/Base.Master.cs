﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CareMore.Web.DotCom;

using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Data.Managers;
using Sitecore.Links;

using CMGI.BL;
using CMGI.BL.ListItem;

namespace CMGI.Layouts
{
	public partial class Base : System.Web.UI.MasterPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if ((Sitecore.Context.Item == null) || (string.IsNullOrEmpty(Sitecore.Context.Item["Title"])))
			{
				SetPageTitle(null);
			}
			else
			{
				SetPageTitle(Sitecore.Context.Item["Title"]);
			}

			Item homeItem = ItemIds.GetItem("CMGI_HOME_ID");
			HomeLink.NavigateUrl = LinkManager.GetItemUrl(homeItem);

			rptTopRtNav.DataSource = GetNav("CMGI_HDR_NAV_ID");
			rptTopRtNav.DataBind();

			rptMainNav.DataSource = GetNav("CMGI_TOP_NAV_ID");
			rptMainNav.DataBind();

			string copyright = ItemIds.GetItemValue("CMGI_FOOTER_ID", "Text", false);
			FooterLabel.Text = string.Format(copyright, DateTime.Now.Year);

			Item privacyItem = ItemIds.GetItem("CMGI_PRIVACY_POLICY");
			PrivacyLink.NavigateUrl = LinkManager.GetItemUrl(privacyItem);

			this.txtSearch.Attributes.Add("alt", "Search Terms");

			if (Common.IsInternalIpAddress(Request))
			{
				plcGoogleAnalytics.Visible = false;
			}
		}

		protected void BtnSearch_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/Search.aspx?q=" + HttpUtility.UrlEncode(txtSearch.Text));
		}

		public void SetPageTitle(string title)
		{
			if (string.IsNullOrEmpty(title))
			{
				this.Page.Title = Utility.BASE_TITLE;
			}
			else
			{
				this.Page.Title = string.Format("{0} - {1}", Utility.BASE_TITLE,
					title);
			}
		}

		private List<TopNavItem> GetNav(string id)
		{
			List<TopNavItem> list = new List<TopNavItem>();

			foreach (Item childItem in ItemIds.GetItem(id).Children)
			{
				TopNavItem navItem = new TopNavItem(childItem);
				LinkField linkField = childItem.Fields["Link"];

				navItem.Width = childItem["Width"];
				navItem.TargetItem = linkField.TargetItem;

				if (navItem.TargetItem != null)
				{
					if (Sitecore.Context.Item.ID == navItem.TargetItem.ID)
					{
						navItem.CssClass = "active";
					}
					else
					{
						Item parentItem = Sitecore.Context.Item.Parent;

						while (parentItem != null)
						{
							if ((parentItem.ID == navItem.TargetItem.ID) &&
								(!parentItem.Name.Equals("CMGI", StringComparison.OrdinalIgnoreCase)))
							{
								navItem.CssClass = "active";
								break;
							}

							parentItem = parentItem.Parent;
						}
					}
				}

				list.Add(navItem);
			}

			return list;
		}
	}
}
