﻿using System;
using System.Collections.Generic;
using System.Linq;
using CMGI.BL;
using CMGI.BL.ListItem;
using Sitecore.Data;
using Sitecore.Data.Items;
using System.Collections.Specialized;
using System.Web.UI.WebControls;
using System.Web;
using Sitecore.Data.Fields;
using System.Web.UI.HtmlControls;


namespace CMGI.Layouts
{
	public partial class FindLocations : System.Web.UI.Page
	{
		// For paging
		private const int perPage = 4;
		private int PageNumber;
		private bool ViewAll;

		protected void lnkClear_Click(object sender, EventArgs e)
		{
			string query = "?p=1";
			string pageName = Request.PhysicalPath;
			pageName = System.IO.Path.GetFileName(pageName);

			Session.Clear();

			Response.Redirect(pageName + query);
		}

		public CMGI.BL.ListItem.LocationItem[] GetAllLocations(bool sortzip)
		{
			// Get sitecore items
			Item[] locList = Sitecore.Context.Database.SelectItems(Utility.FIND_LOCATIONS_PATH + "//*[@@templatename='Locations']");
			locList = locList.OrderBy(i => ((DateField)i.Fields["City"]).DateTime).ToArray();

			if (sortzip)
			{
				if (!(new System.Text.RegularExpressions.Regex(@"^(\d{5})$").IsMatch(txtZip.Text)))
				{
					lblProx.Visible = false;
					errorLabel.Text = string.Format(errorLabel.Text, txtZip.Text);
					errorLabel.Visible = true;
					pnlPages.Visible = false;
					lnkShowAll.Visible = false;
					return null;
				}

				lblProx.Text = string.Format(lblProx.Text, txtZip.Text);
				lblProx.Visible = true;
				errorLabel.Visible = false;
				lnkShowAll.Visible = true;

				List<LocationItem> llist = new List<LocationItem>();
				llist = CMGI.BL.ListItem.LocationItem.GetLocations(txtZip.Text, false, false);

				//return only CMG locations
				return llist.Where(i => i.strLocationType.Contains("CMG")).OrderBy(i => i.Distance).ToArray();
			}
			else
			{
				List<LocationItem> llist = new List<LocationItem>();
				llist = CMGI.BL.ListItem.LocationItem.GetLocations(null, true, false);
				//return only CMG locations
				return llist.Where(i => i.strLocationType.Contains("CMG")).OrderBy(i => i.Distance).ToArray();
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			txtZip.Attributes.Add("alt", "Zip Code");

			if (!IsPostBack)
			{
				ParseQuery();
			}

			bool sortingzip = false;
			if (txtZip.Text != "" & txtZip.Text != txtZip.Attributes["alt"])
			{
				sortingzip = true;
			}

			LocationItem[] locations = GetAllLocations(sortingzip);
			LocationItem[] partiallocations;


			if (ViewAll)
			{
				locations = GetAllLocations(sortingzip);
				UpdatePageArea(locations.Count());
				rptLocationInfo.DataSource = locations;
				Session["list"] = null;
			}
			else
			{
				if (locations != null)
				{
					partiallocations = GetPageLocations(locations);
					UpdatePageArea(locations.Count());
					rptLocationInfo.DataSource = partiallocations;
					Session.Contents["list"] = partiallocations;
				}
			}

			if (locations != null)
			{
				rptLocationInfo.DataBind();
			}
		}

		public LocationItem[] GetPageLocations(LocationItem[] items)
		{
			// Use page number and per page number to find the right locations to display
			int c = 1;
			List<LocationItem> res = new List<LocationItem>();

			foreach (LocationItem e in items)
			{
				if (ViewAll || c >= perPage * (PageNumber - 1) + 1)
				{

					res.Add(e);
				}


				if (c >= (perPage * PageNumber) && !ViewAll) break;

				c++;
			}

			return res.OrderBy(i => i.Distance).ToArray();
		}

		private NameValueCollection ParseQuery()
		{
			var nvc = Request.QueryString;

			if (nvc["zip"] != null)
			{
				txtZip.Text = nvc["zip"];
			}

			if (nvc["p"] != null)
			{
				int pagenum = int.Parse(nvc["p"]);
				PageNumber = pagenum;
			}
			else
			{
				PageNumber = 1;
			}

			if (nvc["all"] != null && nvc["all"] == "1")
				ViewAll = true;
			else
				ViewAll = false;

			return nvc;
		}

		private String ConstructQueryString(NameValueCollection parameters)
		{
			List<string> items = new List<string>();

			foreach (String name in parameters)
				items.Add(String.Concat(name, "=", System.Web.HttpUtility.UrlEncode(parameters[name])));

			return String.Join("&", items.ToArray());
		}

		private void UpdatePageArea(int count)
		{
			var nvc = new NameValueCollection(Request.QueryString);
			if (nvc["all"] != null) nvc.Remove("all");
			if (nvc["p"] != null) nvc.Remove("p");

			string q = ConstructQueryString(nvc);

			int pages = (int)Math.Ceiling((decimal)count / perPage);

			if (PageNumber > 1)
			{
				lnkLast.Visible = true;
				lnkLast.NavigateUrl = Request.Path + "?p=" + (PageNumber - 1) + "&" + q;
			}
			else
			{
				lblLast.Visible = true;
			}

			List<int> pageSrc = new List<int>();
			for (int x = 1; x <= pages; x++)
				pageSrc.Add(x);

			rptPage.DataSource = pageSrc;
			rptPage.DataBind();

			if (PageNumber < pages)
			{
				lnkNext.Visible = true;
				lnkNext.NavigateUrl = Request.Path + "?p=" + (PageNumber + 1) + "&" + q;
			}
			else
			{
				lblNext.Visible = true;
			}

			if (ViewAll)
			{
				lblAll.Visible = true;
			}
			else
			{
				lnkAll.Visible = true;
				lnkAll.NavigateUrl = Request.Path + "?all=1";
			}
		}

		protected void rptLocationInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				LocationItem location = (LocationItem)e.Item.DataItem;

				location.markerNumber = ((e.Item.ItemIndex) + 1).ToString();

				((Label)e.Item.FindControl("lblAddress1")).Text = location.Address1;

				((Label)e.Item.FindControl("lblAddress2")).Text = string.IsNullOrEmpty(location.MultipleSuiteLocation) && (!string.IsNullOrEmpty(location.Address2))
					? location.Address2 + "<br />"
					: string.Empty;

				((Label)e.Item.FindControl("lblCity")).Text = location.City;
				((Label)e.Item.FindControl("lblState")).Text = location.State;
				((Label)e.Item.FindControl("lblZip")).Text = location.Zip;
				((Label)e.Item.FindControl("lblPhone")).Text = string.IsNullOrEmpty(location.MultipleSuiteLocation)
					? location.Phone
					: "See Details";
				((Label)e.Item.FindControl("lblFax")).Text = string.IsNullOrEmpty(location.MultipleSuiteLocation)
					? location.Fax
					: "See Details";

				((Table)e.Item.FindControl("tblCurrentItem")).Attributes["data-GUID"] = location.GUID;


				HtmlGenericControl divDot = (HtmlGenericControl)e.Item.FindControl("divDot");
				Literal litDotNumber = (Literal)e.Item.FindControl("litDotNumber");

				if (location.strLocationType.Contains("CMG"))
				{
					divDot.Attributes.Add("class", "bluedot");
				}
				else if (location.strLocationType.Contains("Urgent Care Centers"))
				{
					divDot.Attributes.Add("class", "pinkdot");
				}
				else if (location.strLocationType.Contains("Flu Shots Locations"))
				{
					divDot.Attributes.Add("class", "greendot");
				}

				litDotNumber.Text = location.markerNumber.ToString();

				Label lblName = e.Item.FindControl("lblLocationName") as Label;
				lblName.Text = location.Item["Title"];

				Label distlabel = e.Item.FindControl("lblDistance") as Label;
				double d = Math.Round((location.Distance.HasValue ? location.Distance.Value : 0), 1);

				if (d != double.NegativeInfinity)
				{
					distlabel.Text = "<br/>~" + d.ToString() + " mi";
					distlabel.Visible = true;
				}

				Item[] items = Sitecore.Context.Database.SelectItems(Utility.FIND_LOCATIONS_PATH + "//*[@@id='" + location.GUID + "']");
				Item currentItem = items.FirstOrDefault();
				((HyperLink)e.Item.FindControl("lnkViewDetails")).NavigateUrl = currentItem.Paths.GetFriendlyUrl();

				string address = String.Format("{0} {1} {2} {3}", location.Address1, location.City, location.State, location.Zip);
				string url = String.Format(@"http://maps.google.com/maps?q={0}&t=m&z=16", HttpUtility.UrlEncode(address));
				((System.Web.UI.HtmlControls.HtmlControl)e.Item.FindControl("lnkGetDirections")).Attributes["Href"] = url;
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			string query = "?zip=" + HttpUtility.UrlEncode(txtZip.Text);
			Response.Redirect(Request.Url.AbsolutePath + query);
		}

		protected void rptPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
			{
				int pg = (int)e.Item.DataItem;
				var nvc = new NameValueCollection(Request.QueryString);
				if (nvc["all"] != null) nvc.Remove("all");
				if (nvc["p"] != null) nvc.Remove("p");

				string q = ConstructQueryString(nvc);

				if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
				{
					HyperLink pagelink = e.Item.FindControl("lnkPageNum") as HyperLink;
					Label pagelabel = e.Item.FindControl("lblPageNum") as Label;

					if (PageNumber == pg)
					{
						pagelabel.Visible = true;
						pagelabel.Text = pg.ToString();
					}
					else
					{
						pagelink.Visible = true;
						pagelink.Text = pg.ToString();
						pagelink.NavigateUrl = Request.Path + "?p=" + pg.ToString() + "&" + q;
					}
				}
			}
		}
	}
}
