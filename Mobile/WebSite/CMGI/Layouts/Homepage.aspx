﻿<%@ Page Title="" Language="C#" MasterPageFile="Base.Master" AutoEventWireup="true" CodeBehind="Homepage.aspx.cs" Inherits="CMGI.Layouts.Homepage" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>
<%@ Register TagPrefix="cg" TagName="Carousel" Src="/CMGI/Sublayouts/Carousel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainBodyContent" runat="server">

<div class="container">
	<cg:Carousel ID="CarouselMain" runat="server" />

	<br />

	<div class="three-row">

	<div class="three-left">
		<div class="coltitle"><cg:Link Field="Callout Left Link" ID="LeftTitleLink" runat="server"><h2><cg:Text ID="ColLeftTitle" runat="server" Field="Callout Left Title"></cg:Text></h2>
		<cg:Image ID="ColLeftImage" runat="server" Field="Callout Left Image" /></cg:Link></div>

		<p><strong><cg:Text ID="ColLeftHeader" runat="server" Field="Callout Left Header" /></strong>
			<br /><cg:Text ID="ColLeftBody" runat="server" Field="Callout Left Body" />
			<br /><cg:Link ID="ColLeftLink" runat="server" Field="Callout Left Link" /></p>
	</div>

	<div class="three-center">
		<div class="coltitle"><cg:Link Field="Callout Center Link" runat="server" ID="CenterTitleLink"><h2><cg:Text ID="ColCenterTitle" runat="server" Field="Callout Center Title"></cg:Text></h2>
		<cg:Image ID="ColCenterImage" runat="server" Field="Callout Center Image" /></cg:Link></div>

		<p><strong><cg:Text ID="ColCenterHeader" runat="server" Field="Callout Center Header" /></strong>
			<br /><cg:Text ID="ColCenterBody" runat="server" Field="Callout Center Body" />
			<br /><cg:Link ID="ColCenterLink" runat="server" Field="Callout Center Link" /></p>
	</div>

	<div class="three-right">
		<div class="coltitle"><cg:Link Field="Callout Right Link" ID="RightTitleLink" runat="server"><h2><cg:Text ID="ColRightTitle" runat="server" Field="Callout Right Title"></cg:Text></h2>
		<cg:Image ID="ColRightImage" runat="server" Field="Callout Right Image" /></cg:Link></div>

		<p><strong><cg:Text ID="ColRightHeader" runat="server" Field="Callout Right Header" /></strong>
			<br /><cg:Text ID="ColRightBody" runat="server" Field="Callout Right Body" />
			<br /><cg:Link ID="ColRightLink" runat="server" Field="Callout Right Link" /></p>
	</div>

	<div class="clear_both"></div>
</div>

</asp:Content>
