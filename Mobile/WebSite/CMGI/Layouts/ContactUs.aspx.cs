﻿using System;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMGI.BL;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Website;

namespace CMGI.Layouts
{
	public partial class ContactUs : System.Web.UI.Page
	{
		protected void SendMessageClick(object sender, EventArgs e)
		{
			try
			{
				if (Page.IsValid)
				{
					SendEmail();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(SummaryPanel, ex);
			}
		}

		private void SendEmail()
		{
			Item currentItem = Sitecore.Context.Item;

			MailMessage message = new MailMessage();
			message.From = new MailAddress(currentItem["Email From"]);
			message.To.Add(new MailAddress(currentItem["Email To"]));

			if (!string.IsNullOrWhiteSpace(currentItem["Email CC"]))
				message.CC.Add(new MailAddress(currentItem["Email CC"]));

			if (!string.IsNullOrWhiteSpace(currentItem["Email BCC"]))
				message.Bcc.Add(new MailAddress(currentItem["Email BCC"]));

			message.Subject = currentItem["Email Subject"];

			StringBuilder body = new StringBuilder();

			body.AppendLine(Utility.MailHeader(message.Subject));

			body.AppendLine(Utility.MailTableOpen);
			body.Append(Utility.MailTableRow("Sender First Name:", FirstName.Text));
			body.Append(Utility.MailTableRow("Sender Last Name:", LastName.Text));
			body.Append(Utility.MailTableRow("Sender Phone Number:", PhoneNumber.Text));
			body.Append(Utility.MailTableRow("Email Address:", EmailAddress.Text));
			body.AppendLine(Utility.MailTableClose);

			string text;
			if (string.IsNullOrEmpty(Message.Text))
				text = "<i>none</i>";
			else
				text = Message.Text;

			body.AppendFormat("<p><b>Message:</b><br/>{0}</p>", text);
			body.AppendLine(Utility.MailFooter());

			message.Body = body.ToString();
			message.IsBodyHtml = true;

			//Send email
			CareMoreUtilities.SendEmail(message);

			//Show thank you message
			CareMoreUtilities.DisplayStatus(SummaryPanel, Sitecore.Context.Item["Email Thank You"]);
			MessagePanel.Visible = false;
		}
	}
}
