﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="CMGI.Layouts.ContactUs" MasterPageFile="Base.Master" %>

<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics" %>

<asp:Content runat="server" id="Content1" contentplaceholderid="head">
	<link href="/sitecore%20modules/shell/Web%20Forms%20for%20Marketers/themes/Default.css" rel="stylesheet" type="text/css"/>
</asp:Content>

<asp:Content runat="server" id="PatientBody" contentplaceholderid="MainBodyContent">
	<div class="container">
		<div class="noSideMenu">
			<div class="title">
				<h1><cg:Text Field="Title" ID="pgTitle" runat="server" /></h1>
			</div>

			<div class="contact-left">
				<div class="office">
					<h3>CMG Main Office</h3>
					<cg:Text Field="Address" runat="server" />
				</div>

				<div class="hours">
					<h3>Hours</h3>
					<cg:Text Field="Hours" runat="server" />
					<cg:Text Field="Phone Numbers" runat="server" />
				</div>
				<div class="clear_both"></div>

				<div><img alt="" src="/CMGI/images/contact-border.jpg" /></div>

				<cg:Text Field="Body" runat="server" />
			</div>

			<div class="fill-out">
				<h3>Request Information</h3>

				<asp:PlaceHolder ID="SummaryPanel" runat="server" Visible="false"></asp:PlaceHolder>

				<asp:Panel ID="MessagePanel" runat="server" CssClass="scfForm" DefaultButton="btnSendMessage">
					<asp:ValidationSummary ID="valSumRequestInfo" runat="server" CssClass="scfValidationSummary"
						ShowMessageBox="false" ShowSummary="true" DisplayMode="BulletList" />

					<div class="scfSectionBorder">
						<div class="scfSectionContent">
							<div class="cmgiField">
								<label class="scfSingleLineTextLabel">First Name</label>
								<div class="scfSingleLineGeneralPanel">
									<asp:TextBox ID="FirstName" runat="server" CssClass="scfSingleLineTextBox" />
									<asp:RequiredFieldValidator ID="FirstNameRequired" runat="server"
										ControlToValidate="FirstName" ErrorMessage="First Name must be filled in"
										Display="None" Text="*" />
								</div>
							</div>
							<div class="cmgiField">
								<label class="scfSingleLineTextLabel">Last Name</label>
								<div class="scfSingleLineGeneralPanel">
									<asp:TextBox ID="LastName" runat="server" CssClass="scfSingleLineTextBox" />
									<asp:RequiredFieldValidator ID="LastNameRequired" runat="server"
										ControlToValidate="LastName" ErrorMessage="Last Name must be filled in"
										Display="None" Text="*" />
								</div>
							</div>
							<div class="cmgiField">
								<label class="scfSingleLineTextLabel">Phone Number</label>
								<div class="scfSingleLineGeneralPanel">
									<asp:TextBox ID="PhoneNumber" runat="server" CssClass="scfSingleLineTextBox" />
								</div>
							</div>
							<div class="cmgiField">
								<label class="scfSingleLineTextLabel">Email Address</label>
								<div class="scfSingleLineGeneralPanel">
									<asp:TextBox ID="EmailAddress" runat="server" CssClass="scfSingleLineTextBox" />
									<asp:RequiredFieldValidator ID="EmailAddressValidator" runat="server"
										ControlToValidate="EmailAddress" ErrorMessage="Email Address must be filled in"
										Display="None" Text="*" />
								</div>
							</div>
							<div class="cmgiField">
								<label class="scfMultipleLineTextLabel">Message</label>
								<div class="scfMultipleLineGeneralPanel">
									<asp:TextBox ID="Message" runat="server" CssClass="scfMultipleLineTextBox"
										rows="5" Columns="20" TextMode="MultiLine" />
								</div>
							</div>
						</div>
					</div>
					<asp:Button ID="btnSendMessage" runat="server" Text="Submit" OnClick="SendMessageClick" />
				</asp:Panel>
			</div>

			<div class="clear_both"></div>
		</div>
	</div>
</asp:Content>
