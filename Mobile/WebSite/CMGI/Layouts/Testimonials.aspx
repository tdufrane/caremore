﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Testimonials.aspx.cs" Inherits="CMGI.Layouts.Testimonials" MasterPageFile="Base.Master" %>

<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>


<asp:content runat="server" id="Content1" contentplaceholderid="head">
</asp:content>

<asp:content runat="server" id="PatientBody" contentplaceholderid="MainBodyContent">

<div class="container">

	  <%--<div class="test-nav-container">
      <div class="test-nav">
       	
       	  <p class="test-nav-title">Patient Testimonials</p>
          <img class="border" src="/cmgi/images/nav-border.jpg" />

          <asp:Repeater ID="rptPatTest" runat="server" 
              onitemdatabound="rptPatTest_ItemDataBound">
            <ItemTemplate>
                    <asp:Panel ID="testimonial" runat="server">
                    <asp:HyperLink ID="lnkPat" runat="server">
                        <asp:Image ID="patImage" runat="server" CssClass="nav-pic" /> 
                        <asp:Label ID="patFirst" runat="server" />&nbsp;<asp:Label ID="patLast" runat="server" /> <br />

   	                    <asp:Label ID="patCity" runat="server" />, <asp:Label ID="patState" runat="server" />
                    </asp:HyperLink></asp:Panel><div class="clear_both"></div>
            </ItemTemplate>

            <SeparatorTemplate>
                <img class="border" src="/cmgi/images/nav-border.jpg" />
            </SeparatorTemplate>
          </asp:Repeater>
         
        </div>
      
      
      <div class="test-nav">
       	
       	  <p class="test-nav-title">Doctor Testimonials</p><img class="border" src="/cmgi/images/nav-border.jpg" /> <asp:Repeater ID="rptDocTest" runat="server" 
              onitemdatabound="rptDocTest_ItemDataBound">
            <ItemTemplate>
                    <asp:Panel ID="testimonial" runat="server">
                    <asp:HyperLink ID="lnkDoc" runat="server">
                        <asp:Image ID="docImage" runat="server" CssClass="nav-pic" /> 
                        <asp:Label ID="docFirst" runat="server" />&nbsp;<asp:Label ID="docLast" runat="server" /> <br />

   	                    <asp:Label ID="docCity" runat="server" />, <asp:Label ID="docState" runat="server" />
                    </asp:HyperLink>
                    </asp:Panel><div class="clear_both"></div>
            </ItemTemplate>

            <SeparatorTemplate>
                <img class="border" src="/cmgi/images/nav-border.jpg" />
            </SeparatorTemplate>
          </asp:Repeater>

     </div>
     </div>--%> 
      
        
        
        <%--<div class="copy">
        
        	<div class="title">
            	<h1><asp:Label ID="title" runat="server" /></h1>
          </div>
           
           
            
            <div class="message"><asp:Label ID="lblIntro" runat="server" /></div> <br />
            
            <div class="test-container">
            
            <div class="test-photo"><cg:Image Field="Picture" runat="server" />
          <p><cg:Text Field="First Name" runat="server" /> <cg:Text Field="Last Name" runat="server" /> - <cg:Text Field="City" runat="server" />, <cg:Text Field="State" runat="server" /></p></div>
            
          <div class="test-copy"><cg:Text Field="Testimonial" runat="server" /></div>

<div class="clear_both"></div>

         </div>   
        
        
      </div>--%>
           
      <div class="title" style="text-align:center;"><h1>Coming Soon</h1></div>
      
      
      <div class="clear_both"></div>
</div>

</asp:content>