﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;

namespace CMGI.Layouts
{
    public partial class Redirect : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LinkField lf = Sitecore.Context.Item.Fields["Redirect Link"];
            if (lf != null)
            {
                string url = Sitecore.Links.LinkManager.GetItemUrl(lf.TargetItem);

                Response.Redirect(url);
            }
        }
    }
}