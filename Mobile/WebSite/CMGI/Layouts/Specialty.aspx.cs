﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMGI.BL;
using Sitecore.Data.Items;

namespace CMGI.Layouts
{
	public partial class Specialty : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Item specItem = Sitecore.Context.Database.SelectSingleItem(Utility.SPECIALTY_PATH + string.Format("//*[@Specialty='{0}']", Sitecore.Context.Item["Title"]));
			string specId = specItem.ID.ToString();

			this.plCalifornia.Specialty = specId;
			this.plNevada.Specialty = specId;
		}
	}
}
