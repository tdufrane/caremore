﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Specialty.aspx.cs" Inherits="CMGI.Layouts.Specialty" MasterPageFile="Base.Master" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="../Sublayouts/LeftNavigation.ascx" %>
<%@ Register TagPrefix="cg" TagName="PhysicianList" Src="../Sublayouts/PhysicianList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainBodyContent" runat="server">
	<div class="container">
		<!-- begin left side navigation -->
		<cg:LeftNavigation runat="server" id="cmgiLeftNavigation">
		</cg:LeftNavigation>
		<!-- end left side navigation -->

		<div class="copy">
			<div class="title">
				<h1><sc:Text runat="server" Field="Title" /></h1>
			</div>

			<div><cg:Image runat="server" Field="Image" /></div>

			<div class="message">
				<sc:Text runat="server" Field="Body" />
			</div>

			<!--a href="#Nevada">go to Nevada</a-->
			<cg:PhysicianList ID="plCalifornia" runat="server" State="California" />

			<!--a href="#California">back to California</a-->
			<cg:PhysicianList ID="plNevada" runat="server" State="Nevada" />
		</div>
		<div class="clear_both">
		</div>
	</div>
</asp:Content>
