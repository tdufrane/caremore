﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;

namespace CMGI.Layouts
{
	public partial class NewsArchives : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			rptArticles.DataSource = GetAllNews();
			rptArticles.DataBind();
		}

		public Item[] GetAllNews()
		{
			Item currItem = Sitecore.Context.Item;

			var children = currItem.Parent.GetChildren();
			var news = from child in children
			           where child.TemplateName == "News Item"
			           select child;

			var results = news.OrderByDescending(i => ((DateField)i.Fields["Date"]).DateTime);
			return results.ToArray();
		}

		protected void rptArticles_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				Item item = ((Item)e.Item.DataItem);

				if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
				{
					Label date = e.Item.FindControl("newsDate") as Label;
					Label headline = e.Item.FindControl("newsHeadline") as Label;
					Label body = e.Item.FindControl("newsBody") as Label;

					DateField df = item.Fields["Date"];
					date.Text = df.DateTime.ToShortDateString();
					headline.Text = item["Headline"];
					body.Text = item["Body"];
				}
			}
		}
	}
}
