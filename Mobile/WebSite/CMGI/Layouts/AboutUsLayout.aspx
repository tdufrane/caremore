﻿<%@ Page Title="" Language="C#" MasterPageFile="Base.Master" AutoEventWireup="true"
    CodeBehind="AboutUsLayout.aspx.cs" Inherits="CMGI.Layouts.AboutUsLayout" %>

<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="/CMGI/Sublayouts/LeftNavigation.ascx" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainBodyContent" runat="server">
    <div class="container">
        <!-- begin left side navigation -->
        <cg:leftnavigation runat="server" id="cmgiLeftNavigation"></cg:leftnavigation>
        <!-- end left side navigation -->
        <div class="copy">
            <div class="title">
                <h1>
                    <cg:Text runat="server" id="scTitleText" field="Title" />
                </h1>
            </div>

            <div runat="server" id="divHeroCssClass" class="hero">           
                <div id="divHeroCopyCssClass" runat="server" class="hero-copy">                    
                    <cg:Text runat="server" id="scImageOverlayText" field="Image Overlay Text" />
                </div>
            </div>

            <p class="message">
                <cg:Text runat="server" id="scValuePropositionext" field="Value Proposition Text" />
            </p>

            <cg:Text runat="server" id="scBodyText" field="Body" />
        </div>

        <div class="clear_both"></div>
    </div>
</asp:Content>
