﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftNavigation.ascx.cs" Inherits="CMGI.Sublayouts.LeftNavigation" %>
<%@ Register tagPrefix="cg" namespace="CMGI.BL.WebControls" assembly="CMGI" %>

<!-- Begin SubLayouts/AboutUsPage -->
<div class="subLayout">
	<div class="left-nav" id="ctl1">
		<cg:LeftNav ID="leftNav" runat="server" />
	</div>
</div>
<!-- End SubLayouts/AboutUsPage -->
