﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Forms;
using Sitecore.Form.Core;
using CMGI.BL;
using CMGI.BL.ListItem;

namespace CMGI.Controls
{
	public partial class Carousel : System.Web.UI.UserControl
	{
		private StringBuilder customCss = null;

		protected void Page_Load(object sender, EventArgs e)
		{
			rptCarousel.DataSource = GetItems();
			rptCarousel.DataBind();
		}

		private List<CarouselItem> GetItems()
		{
			Item slideFolderItem = ItemIds.GetItem("CMGI_HOME_SLIDESHOW");
			List<CarouselItem> slides = new List<CarouselItem>();

			foreach (Item item in slideFolderItem.Children)
			{
				CarouselItem carousel = new CarouselItem(item);
				if (!carousel.Disabled)
				{
					slides.Add(carousel);
				}
			}

			return slides;
		}

		protected void rptCarousel_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			switch (e.Item.ItemType)
			{
				case ListItemType.Header:
					customCss = new StringBuilder();
					break;

				case ListItemType.AlternatingItem:
				case ListItemType.Item:
					CarouselItem item = (CarouselItem)e.Item.DataItem;
					Label title = e.Item.FindControl("SlideTitle") as Label;
					Label desc = e.Item.FindControl("SlideBody") as Label;

					title.Text = item.Title;
					desc.Text = item.Body;

					if (!string.IsNullOrWhiteSpace(item.Css))
					{
						customCss.AppendLine(item.Css);
					}

					if (item.ImageLink != null)
					{
						PlaceHolder container = e.Item.FindControl("SlideLinkContainer") as PlaceHolder;
						container.Controls.Add(item.ImageLink);
					}

					if (item.FullWidthImage)
					{
						// Hide text content
						Panel pnlCarouselContent = e.Item.FindControl("pnlCarouselContent") as Panel;
						pnlCarouselContent.Visible = false;
					}
					else if (item.Link != null)
					{
						// Set text link
						HtmlGenericControl container = e.Item.FindControl("SlideBodyContainer") as HtmlGenericControl;
						container.Controls.Add(item.Link);
					}
					break;

				case ListItemType.Footer:
					if (customCss.Length > 0)
					{
						HtmlGenericControl styles = new HtmlGenericControl();
						styles.TagName = "style";
						styles.Attributes.Add("type", "text/css");
						styles.InnerText= customCss.ToString();
						Page.Header.Controls.Add(styles);
					}
					break;

				default:
					break;
			}
		}
	}
}
