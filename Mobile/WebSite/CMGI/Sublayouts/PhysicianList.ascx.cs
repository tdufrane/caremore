﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMGI.BL;
using CMGI.BL.ListItem;
using Sitecore.Data.Items;

namespace CMGI.Sublayouts
{
	public partial class PhysicianList : System.Web.UI.UserControl
	{
		public string State { get; set; }
		public string Specialty { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			this.aState.Name = this.State;
			this.lblState.Text = this.State;

			List<PhysicianItem> list = GetPhysicians();

			if (list.Count == 0)
			{
				this.Visible = false;
			}
			else
			{
				lvPhysicians.DataSource = list;
				lvPhysicians.DataBind();
			}
		}

		protected void lvPhysicians_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			PhysicianItem p = (PhysicianItem)e.Item.DataItem;
			string pNumbers = p.Numbers();

			Label firstName = e.Item.FindControl("PhysFirstName") as Label;
			firstName.Text = p.FirstName;

			Label lastName = e.Item.FindControl("PhysLastName") as Label;
			lastName.Text = p.LastName;

			Image pic = e.Item.FindControl("PhysPic") as Image;
			pic.ImageUrl = "~/" + p.Picture;

			if (p.Addresses != null)
			{
				StringBuilder addressText = new StringBuilder();
				StringBuilder numbersText = new StringBuilder();
				string addressNumbers = string.Empty;

				foreach (AddressItem ai in p.Addresses)
				{
					addressText.AppendFormat("{0}<br/><br/>\n\n", ai.Address());

					addressNumbers = ai.Numbers();
					if (string.IsNullOrEmpty(addressNumbers))
						numbersText.AppendFormat("{0}<br/><br/>", pNumbers);
					else
						numbersText.AppendFormat("{0}<br/><br/>", addressNumbers);

					if (!string.IsNullOrEmpty(ai.Address2))
						numbersText.Append("<br/>");

					if (!string.IsNullOrEmpty(ai.PhysicianSuite))
						numbersText.Append("<br/>");

					numbersText.Append("\n\n");
				}

				Label addresses = e.Item.FindControl("lblAddresses") as Label;
				addresses.Text = addressText.ToString();

				Label numbers = e.Item.FindControl("lblNumbers") as Label;
				numbers.Text = numbersText.ToString();
			}

			HyperLink bio = e.Item.FindControl("PhysBio") as HyperLink;
			bio.NavigateUrl = Sitecore.Links.LinkManager.GetItemUrl(p.Item);
		}

		private List<PhysicianItem> GetPhysicians()
		{
			List<PhysicianItem> phys = new List<PhysicianItem>();

			Item[] physList = Sitecore.Context.Database.SelectItems(Utility.PHYSICIANS_PATH + "//*[@@templatename='Physician']");

			IEnumerable<Item> filtered = physList
					.Where(i => i["State"].Equals(this.State))
					.Where(i => i["Specialty"].Contains(this.Specialty))
					.OrderBy(i => i["Last Name"]);

			IEnumerable<Item> locations = Sitecore.Context.Database.SelectItems(Utility.FIND_LOCATIONS_PATH + "//*[@@templatename='Locations']");

			foreach (Item e in filtered)
			{
				// Get physician addresses
				var physician = Sitecore.Context.Database.SelectItems(Utility.FIND_LOCATIONS_PATH + string.Format("//*[@Physician='{0}']", e.ID));
				var addressItems = physician.ToList().ConvertAll(p => p.Parent).ToArray();

				Item physicianLocation = null;
				if (physician != null && (physician.Length > 0 && physician[0] != null))
				{
					physicianLocation = physician[0];
				}

				phys.Add(new PhysicianItem(e, addressItems, physicianLocation));
			}

			return phys;
		}
	}
}
