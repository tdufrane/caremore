﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PhysicianList.ascx.cs" Inherits="CMGI.Sublayouts.PhysicianList" %>

<hr />

<h4><a id="aState" runat="server" /><asp:Label ID="lblState" runat="server" /></h4>

<div class="phys-table">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td style="width:284px;" class="hdr left-cell">Name</td>
			<td style="width:214px;" class="hdr left-cell border">Address</td>
			<td style="width:155px;" class="hdr left-cell border">Contact</td>
		</tr>

	<asp:ListView ID="lvPhysicians" runat="server" OnItemDataBound="lvPhysicians_ItemDataBound">
		<ItemTemplate>
			<tr>
				<td class="left-cell">
					<p class="dr"><asp:Image ID="PhysPic" runat="server" CssClass="physician" Visible="false" />
						<strong><asp:Label ID="PhysFirstName" runat="server" /> <asp:Label ID="PhysLastName" runat="server" /></strong>
					</p>
					<p><asp:Hyperlink ID="PhysBio" runat="server">Read full bio</asp:Hyperlink></p>
				</td>
				<td class="left-cell border">
					<div class="physblock">
						<asp:Label ID="lblAddresses" runat="server" />
					</div>
				</td>
				<td class="left-cell border">
					<div class="physblock">
						<asp:Label ID="lblNumbers" runat="server" />
					</div>
				</td>
			</tr>
		</ItemTemplate>

		<AlternatingItemTemplate>
			<tr class="alt">
				<td class="left-cell">
					<p class="dr"><asp:Image ID="PhysPic" runat="server" CssClass="physician" Visible="false" />
						<strong><asp:Label ID="PhysFirstName" runat="server" /> <asp:Label ID="PhysLastName" runat="server" /></strong>
					</p>
					<p><asp:Hyperlink ID="PhysBio" runat="server">Read full bio</asp:Hyperlink></p>
				</td>
				<td class="left-cell border">
					<div class="physblock">
						<asp:Label ID="lblAddresses" runat="server" />
					</div>
				</td>
				<td class="left-cell border">
					<div class="physblock">
						<asp:Label ID="lblNumbers" runat="server" />
					</div>
				</td>
			</tr>
		</AlternatingItemTemplate>

		<EmptyDataTemplate>
			<tr>
				<td colspan="5"><div class="info"><center>No providers at this time.</center></div></td>
			</tr>
		</EmptyDataTemplate>
	</asp:ListView>

	</table>
</div>
