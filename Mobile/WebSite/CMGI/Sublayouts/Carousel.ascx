﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Carousel.ascx.cs" Inherits="CMGI.Controls.Carousel" %>

<asp:Repeater ID="rptCarousel" runat="server" onitemdatabound="rptCarousel_ItemDataBound">
	<HeaderTemplate><ul id="slider"></HeaderTemplate>
	<ItemTemplate>
		<li>
			<asp:PlaceHolder ID="SlideLinkContainer" runat="server" />
			<asp:Panel ID="pnlCarouselContent" runat="server" CssClass="carouselContent">
				<span class="carouselHeader"><asp:Label ID="SlideTitle" runat="server"></asp:Label> </span>
				<span class="carouselBody" id="SlideBodyContainer" runat="server"><asp:Label ID="SlideBody" runat="server"></asp:Label> <br /></span>
			</asp:Panel>
		</li>
	</ItemTemplate>
	<FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
