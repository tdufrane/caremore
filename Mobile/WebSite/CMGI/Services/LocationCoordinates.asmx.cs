﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Collections;
using System.Web.Script.Services;
using CMGI.BL.ListItem;
using Sitecore.Data.Items;
using System.Collections.Specialized;
using System.Web.UI.WebControls;
using CMGI.BL;
using System.Web.UI;

namespace CMGI.Services
{
	/// <summary>
	/// Summary description for LocationCoordinates
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	[System.Web.Script.Services.ScriptService]
	public class LocationCoordinates : System.Web.Services.WebService
	{
		GeoLocation geoLocation;

		public LocationCoordinates()
		{
		}

		public class JSONLocObj
		{
			public string Address1 { get; set; }
			public string Address2 { get; set; }
			public string City { get; set; }
			public string State { get; set; }
			public string Zip { get; set; }
			public string Phone { get; set; }
			public string Fax { get; set; }
			public string Latitude { get; set; }
			public string Longitude { get; set; }
			public string GUID { get; set; }
			public string[] LocationType { get; set; }
			public string IconUrl { get; set; }
			public string markerNumber { get; set; }
			public string ViewDetailsUrl { get; set; }
		}

		public static JSONLocObj ConvertToJSON(LocationItem locitem)
		{
			JSONLocObj js = new JSONLocObj();
			js.Address1 = locitem.Address1;
			js.Address2 = locitem.Address2;
			js.City = locitem.City;
			js.Fax = locitem.Fax;
			js.GUID = locitem.GUID;
			js.Latitude = locitem.Lat.ToString();
			js.Longitude = locitem.Long.ToString();
			js.Phone = locitem.Phone;
			js.State = locitem.State;
			js.Zip = locitem.Zip;
			js.LocationType = locitem.strLocationType;
			js.markerNumber = locitem.markerNumber;
			js.IconUrl = locitem.IconUrl;
			js.ViewDetailsUrl = locitem.ViewDetailsUrl;
			return js;
		}

		[WebMethod(EnableSession = true)]
		[ScriptMethod]
        public List<JSONLocObj> GetCoordinates(string zipcode)
		{
			LocationItem[] passthru = (LocationItem[])HttpContext.Current.Session["list"];
			List<LocationItem> li = new List<BL.ListItem.LocationItem>();

			if (passthru != null)
			{
                li = LocationItem.GetLocations(zipcode, true, true, passthru);
			}
			else
			{
                li = LocationItem.GetLocations(zipcode, true);                
			}

			return li.ConvertAll<JSONLocObj>(i => ConvertToJSON(i));
		}
	}
}
