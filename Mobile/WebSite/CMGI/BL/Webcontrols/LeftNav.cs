﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Web.UI;
using System.Web.UI;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Data.Fields;
using Website;
using CMGI.BL;

namespace CMGI.BL.WebControls
{
	public class LeftNav : Sitecore.Web.UI.WebControl
	{
		public LeftNav()
		{
		}

		private const string imgPath = @"/CMGI/Images/nav-border.jpg";

		protected override void DoRender(HtmlTextWriter writer)
		{
			ListItem rootItem = GetRootItem();

			if (rootItem != null)
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Id, "sideNav");
				writer.RenderBeginTag(HtmlTextWriterTag.Div);

				writer.RenderBeginTag(HtmlTextWriterTag.Ul);

				rootItem.Children.ForEach(p => Render(writer, p));

				writer.RenderEndTag();

				//Close sideNav Div
				writer.RenderEndTag();
			}
		}

		private void Render(HtmlTextWriter writer, ListItem listItem, bool child = false)
		{
			if (listItem.IsActive && child)
			{
				writer.AddAttribute("class", "expanded sub");
			}
			else if (listItem.IsActive)
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Class, "expanded");
			}
			else if (child)
			{
				writer.AddAttribute("class", "sub");
			}

			writer.RenderBeginTag(HtmlTextWriterTag.Li);

			if (!listItem.IsCurrent)
			{
				string url = listItem.Url;
				writer.AddAttribute(HtmlTextWriterAttribute.Href, url);
				writer.RenderBeginTag(HtmlTextWriterTag.A);

				if (listItem.ShowMore)
				{
					writer.AddAttribute(HtmlTextWriterAttribute.Alt, "Show more...");
					writer.AddAttribute(HtmlTextWriterAttribute.Title, "Show more...");
					writer.AddAttribute(HtmlTextWriterAttribute.Src, @"/CMGI/Images/expand.png");
					writer.RenderBeginTag(HtmlTextWriterTag.Img);
					writer.RenderEndTag();
				}
			}

			writer.Write(CareMoreUtilities.GetNavigationTitle(listItem.InnerItem));

			if (!listItem.IsCurrent)
			{
				writer.RenderEndTag();
			}

			if (listItem.Children.Count > 0)
			{
				RenderChildren(writer, listItem);
			}

			writer.RenderEndTag();

			if (!child)
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Src, imgPath);
				writer.RenderBeginTag(HtmlTextWriterTag.Img);
				writer.RenderEndTag();
			}
		}

		private static string GetUrl(Item item)
		{
			string url = null;

			if (item.TemplateID == CMGI.BL.ItemIds.GetID("EXTERNAL_LINK_TEMPLATE_ID"))
			{
				LinkField link = item.Fields["Link"];

				if (link != null && !string.IsNullOrEmpty(link.Url))
				{
					return link.Url;
				}
			}

			if (url == null && item != null)
			{
				url = LinkManager.GetItemUrl(item);
			}

			return url;
		}

		private static string GetText(Item item)
		{
			if (item.TemplateID == CMGI.BL.ItemIds.GetID("EXTERNAL_LINK_TEMPLATE_ID"))
			{
				LinkField link = item.Fields["Link"];

				return link.Text;
			}

			return CareMoreUtilities.GetNavigationTitle(item);

		}

		private void RenderChildren(HtmlTextWriter writer, ListItem listItem)
		{
			writer.RenderBeginTag(HtmlTextWriterTag.Ul);

			foreach (ListItem childListItem in listItem.Children)
			{
				Render(writer, childListItem, true);
			}

			writer.RenderEndTag();
		}

		protected ListItem GetRootItem()
		{
			Item currentItem = GetContext();
			Item contextItem = GetContext();

			while ((currentItem != null) &&
			       (currentItem.ParentID != ItemIds.GetID("CMGI_HOME_ID")) &&
			       (currentItem.ID != ItemIds.GetID("CMGI_CONTENT_ID")))
			{
				currentItem = currentItem.Parent;
			}

			if (currentItem == null)
				return null;

			ListItem rootListItem = GetListItem(currentItem, contextItem);

			return rootListItem;
		}

		protected Item GetContext()
		{
			Item contextItem = Sitecore.Context.Item;
			Item currentItem = Sitecore.Context.Item;

			while (currentItem != null && currentItem.ParentID != CMGI.BL.ItemIds.GetID("CMGI_HOME_ID") && currentItem.ID != CMGI.BL.ItemIds.GetID("CMGI_CONTENT_ID"))
			{
				LookupField leftNavField = currentItem.Fields["Left Navigation Item"];

				if (leftNavField != null && leftNavField.TargetItem != null)
				{
					contextItem = leftNavField.TargetItem;
					currentItem = leftNavField.TargetItem;
				}
				else
				{
					currentItem = currentItem.Parent;
				}
			}

			return contextItem;
		}

		private ListItem GetListItem(Item currentItem, Item contextItem)
		{
			if (!CareMoreUtilities.IsVisible(currentItem)
				|| currentItem.ParentID == ItemIds.GetID("PHYSICIANS_ID"))
				return null;

			Item displayItem = CareMoreUtilities.GetLocalizedItem(currentItem);

			ListItem listItem = new ListItem();

			listItem.ShowMore = ShowMoreImage(displayItem);
			listItem.InnerItem = displayItem;
			listItem.IsCurrent = contextItem.ID == displayItem.ID;
			listItem.Url = GetUrl(displayItem);

			if (currentItem.Paths.IsAncestorOf(contextItem) || listItem.IsCurrent)
			{
				listItem.IsActive = true;

				foreach (Item childItem in currentItem.Children)
				{
					ListItem listChildItem = GetListItem(childItem, contextItem);

					if (listChildItem != null)
						listItem.Children.Add(listChildItem);
				}
			}

			return listItem;
		}

		private bool ShowMoreImage(Item item)
		{
			int showMore = 0;

			if (item["Show More Image"] != null)
			{
				int.TryParse(item["Show More Image"], out showMore);
			}

			return (showMore == 1);
		}

		protected class ListItem
		{
			public List<ListItem> Children { get; set; }
			public Item InnerItem { get; set; }
			public bool IsActive { get; set; }
			public bool IsCurrent { get; set; }
			public bool ShowMore { get; set; }
			public string Url { get; set; }

			public ListItem()
			{
				Children = new List<ListItem>();
			}
		}
	}
}
