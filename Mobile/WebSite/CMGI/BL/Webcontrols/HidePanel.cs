﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using System.Web.UI.WebControls;

namespace CMGI.BL.WebControls
{
    public class HidePanel : Panel
    {
        public string Field { get; set; }
        public Item Item { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            if (Item == null)
                Item = Sitecore.Context.Item;

            Visible = true;

            foreach (string val in Field.Split('|'))
            {
                if (string.IsNullOrEmpty(Item[val]))
                    Visible = false;
            }
        }
    }
}