﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Events;

namespace CMGI.BL
{
    public class ItemSaveExtension
    {
        public static readonly string Master = "master";
        private static SynchronizedCollection<Sitecore.Data.ID> _inProcess = new SynchronizedCollection<Sitecore.Data.ID>();

        public void OnItemSaved(object sender, EventArgs args)
        {
            try
            {
                Item temp = Event.ExtractParameter(args, 0) as Item;
                if (temp != null && temp.Database.Name.ToLower() == Master)
                {
                    if (_inProcess.Contains(temp.ID)) return;

                    if (temp.TemplateID.ToString() == ItemIds.GetID("PHYSICIAN_ID").ToString() || temp.TemplateID.ToString() == ItemIds.GetID("EVENT_ID").ToString()
                        || temp.TemplateID.ToString() == ItemIds.GetID("LOCATION_TEMPLATE").ToString())
                    {
                        _inProcess.Add(temp.ID);

                        using (new EditContext(temp))
                        {
                            temp.Editing.BeginEdit();
                            string address = temp["Address 1"] + ", " + temp["Address 2"] + ", " + temp["City"] + " " + temp["State"] + " " + temp["Zip Code"];

                            var resp = Geocoding.CallGeoWS(address);
                            if (resp.Results.Count() > 0)
                            {
                                temp["Lat"] = resp.Results[0].Geometry.Location.Lat.ToString();
                                temp["Long"] = resp.Results[0].Geometry.Location.Lng.ToString();
                            }
                        }

                        _inProcess.Remove(temp.ID);
                    }
                }
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Fatal(e.Message, sender);
                
            }
        }
    }
}