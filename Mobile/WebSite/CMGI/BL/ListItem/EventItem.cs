﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;

namespace CMGI.BL.ListItem
{
	public class EventItem : StandardItem
	{
		public string Name;
		public DateTime Date;
		public string Description;
		public string ShortDesc;
		public string Address1, Address2, City, State, Zip;


		public double Lat, Long;
		public double? Distance;

		public EventItem(Item e, double startLat, double startLong) : base(e)
		{
			Name = e["Name"];
			Date = ((DateField)e.Fields["Date"]).DateTime;
			Description = e["Description"];
			ShortDesc = e["Short Description"];
			Address1 = e["Address 1"];
			Address2 = e["Address 2"];
			City = e["City"];
			State = e["State"];
			Zip = e["Zip Code"];

			double tempValue;
			if (double.TryParse(e["Lat"], out tempValue))
				Lat = tempValue;
			else
				Lat = double.NegativeInfinity;

			if (double.TryParse(e["Long"], out tempValue))
				Long = tempValue;
			else
				Long = double.NegativeInfinity;

			if (Lat == double.NegativeInfinity || Long == double.NegativeInfinity ||
				startLat == double.NegativeInfinity || startLong == double.NegativeInfinity)
			{
				Distance = double.NegativeInfinity;
			}
			else
			{
				Distance = Geocoding.Distance(startLat, startLong, Lat, Long);
			}
		}
	}
}
