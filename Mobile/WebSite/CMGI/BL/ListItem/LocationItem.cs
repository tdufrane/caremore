﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;

namespace CMGI.BL.ListItem
{
    public class LocationItem : StandardItem
    {
        public string Address1;
        public string Address2;
        public string City;
        public string State;
        public string Zip;
        public string Phone;
        public string Fax;
        public string Latitude;
        public string Longitude;
        public string GUID;
        public string IconUrl;
        public string ViewDetailsUrl;
        public string markerNumber;
        public string[] strLocationType;
        public double Lat, Long;
        public double? Distance;
        public string MultipleSuiteLocation;

        double StartLat, StartLong;

        public LocationItem(Item e, double StartLat, double StartLong, int index) :
            base(e)
        {
            this.StartLat = StartLat;
            this.StartLong = StartLong;


            if (e != null)
            {
                try
                {

                    Address1 = e["Address 1"];
                    Address2 = e["Address 2"];
                    City = e["City"];
                    State = e["State"];
                    Zip = e["Zip Code"];
                    Phone = e["Phone"];
                    Fax = e["fax"];
                    GUID = e.ID.ToString();
                    MultipleSuiteLocation = e["MultipleSuiteLocation"];
                    List<string> locationType = new List<string>();
                    MultilistField mlf = e.Fields["Location Type"];
                    foreach (Item i in mlf.GetItems())
                        locationType.Add(i.Name);
                    strLocationType = locationType.ToArray();
                    markerNumber = index.ToString();
                   //Get the Icon URL
                   ImageField imageField = e.Fields["Icon"];
                   if (imageField != null && imageField.MediaItem != null)
                   {
                       IconUrl = MediaManager.GetMediaUrl(imageField.MediaItem);
                   }
                    //Get the view details URL
                   ViewDetailsUrl = e.Paths.GetFriendlyUrl();

                    base.Item = e;


                    Lat = double.Parse(e["Lat"]);
                    Long = double.Parse(e["Long"]);

                    if (Lat == double.NegativeInfinity || Long == double.NegativeInfinity ||
                        this.StartLat == double.NegativeInfinity || this.StartLong == double.NegativeInfinity)
                    {
                        Distance = double.NegativeInfinity;
                    }
                    else
                    {
                        Distance = Geocoding.Distance(StartLat, StartLong, Lat, Long);
                    }
                }
                catch(Exception exception){
                    System.Diagnostics.Debug.Write(exception);
                }
            }

        }

        public static List<LocationItem> GetLocations(string zip, bool ignoreZip = false, bool isWebserviceCall = true, LocationItem[] passthruLocations = null)
        {
            // Get sitecore items
            List<Item> locList = new List<Item>();

            if (passthruLocations == null)
            {
                locList = Sitecore.Context.Database.SelectItems(Utility.FIND_LOCATIONS_PATH + "//*[@@templatename='Locations']").ToList();
            }
            else
            {
                List<LocationItem> list = passthruLocations.ToList();

                foreach (LocationItem li in list)
                {
                    Item i = Sitecore.Context.Database.GetItem(li.GUID);
                    locList.Add(i);
                }
            }

            locList = locList.OrderBy(i => ((DateField)i.Fields["City"]).DateTime).ToList();
            List<LocationItem> llist = new List<LocationItem>();

            //if zip code is being passed then process zipcode
            if (!String.IsNullOrEmpty(zip)) ignoreZip = false;

            if (!ignoreZip)
            {
                var geoloc = Geocoding.CallGeoWS(zip + ", USA");

                if (geoloc.Results.Count() > 0)
                {
                    double lng = geoloc.Results[0].Geometry.Location.Lng;
                    double lat = geoloc.Results[0].Geometry.Location.Lat;

                    ProxComparer<CMGI.BL.ListItem.LocationItem> prox = new CMGI.BL.ProxComparer<CMGI.BL.ListItem.LocationItem>(lat, lng);                    
                    foreach (Item i in locList)
                    {
                        int index = (locList.IndexOf(i))+1;
                        llist.Add(new LocationItem(i, lat, lng, index));
                    }


                    llist = llist.OrderBy(i => i, prox).ToList();

                    
                    List<LocationItem> cmgList = new List<LocationItem>();
                   
                    //Separate CMG list
                    cmgList = llist.Where(i => i.strLocationType.Contains("CMG")).ToList();

                    //Associate icon with sorted CMG
                    Item[] iconList = Sitecore.Context.Database.SelectItems(Utility.LOCATION_ICON_PATH + "//*[@@templatename='Location Icon']");
                    for (int i = 0, j=0; i < cmgList.Count; i++, j++)
                    {
                        ImageField imageField;

                        //If the request is from the webservice render Google Maps icon
                        if (isWebserviceCall)
                        {
                            imageField = iconList[j].Fields["Google Maps Icon"];
                        }
                        else
                        {
                            //imageField = iconList[j].Fields["Detail Icon"];
                        }                      
                    }
                   
                    //Remove the CMG list
                    llist.RemoveAll(i => i.strLocationType.Contains("CMG"));

                    //Concatenates sorted CMG list and remaining list and assign it back
                    llist = cmgList.Concat(llist).ToList();

                }
            }
            else
            {               
                foreach (Item i in locList)
                {
                    llist.Add(new LocationItem(i, double.NegativeInfinity, double.NegativeInfinity, (locList.IndexOf(i))+1));
                }

                List<LocationItem> cmgList = new List<LocationItem>();

                //Separate CMG list
                cmgList = llist.Where(i => i.strLocationType.Contains("CMG")).ToList();

                //Associate icon with sorted CMG
                Item[] iconList = Sitecore.Context.Database.SelectItems(Utility.LOCATION_ICON_PATH + "//*[@@templatename='Location Icon']");
                for (int i = 0, j = 0; i < cmgList.Count; i++, j++)
                {
                    ImageField imageField;

                    //If the request is from the webservice render Google Maps icon
                    if (isWebserviceCall)
                    {
                        imageField = iconList[j].Fields["Google Maps Icon"];
                    }
                    else
                    {
                        imageField = iconList[j].Fields["Detail Icon"];
                    }

                    if (imageField != null && imageField.MediaItem != null)
                    {
                        cmgList[i].IconUrl = MediaManager.GetMediaUrl(imageField.MediaItem);
                        if (i >= (iconList.Length - 1)) j = 0;
                    }
                }

                //Remove the CMG list
                llist.RemoveAll(i => i.strLocationType.Contains("CMG"));

                //Concatenates sorted CMG list and remaining list and assign it back
                llist = cmgList.Concat(llist).ToList();
            }

            return llist;
        }

    }

}