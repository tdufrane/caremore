﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;

namespace CMGI.BL.ListItem
{
	public class AudioItem : StandardItem
	{
		public Guid ID;
		public string Name;
		public string Title;
		public ImageField Picture;
		public LinkField File;

		public AudioItem(Item item) : base(item)
		{
			this.Name = item["Name"];
			this.Title = item["Title"];
			this.Picture = (ImageField)item.Fields["Picture"];
			this.File = (LinkField)item.Fields["AudioFile"];
			this.ID = item.ID.Guid;
		}
	}
}
