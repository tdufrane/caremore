﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Website.BL.ListItem;

namespace Website.Controls
{
	public partial class Header : Website.Sublayouts.BaseUserControl
	{
		#region Properties

		public Panel BannerPanel { get { return this.pnlHeaderBanner; } }
		public bool HideLangaugeLink { get; set; }
		public PlaceHolder HomePlaceHolder { get { return this.phHome; } }

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void LnkBtnSearch_OnClick(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtSearchInput.Text))
				Response.Redirect("/Search.aspx", false);
			else
				Response.Redirect("/Search.aspx?q=" + HttpUtility.UrlEncode(txtSearchInput.Text), false);
		}

		protected void LnkBtnLanguage_Click(object sender, EventArgs e)
		{
			try
			{
				ChangeLanguage();
				Response.Redirect(GetUrlWithoutLanguage(Request.Url), false);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			SetStaticText();
			SetTopNavigation();
			SetLogo();
			SetMainNavigation();

			if (string.IsNullOrEmpty(CurrentItem["Callout Subheadline"]))
			{
				pnlCalloutLt.CssClass = pnlCalloutLt.CssClass.Replace("col-md-3", "col-md-8");
				pnlCalloutRt.Visible = false;
			}
			else
			{
				pnlCalloutLt.CssClass = pnlCalloutLt.CssClass.Replace("col-md-8", "col-md-3");
				pnlCalloutRt.Visible = true;
			}
		}

		private void ChangeLanguage()
		{
			if (base.CurrentLanguage.Equals(CareMoreUtilities.SpanishKey, StringComparison.OrdinalIgnoreCase))
				CareMoreUtilities.SetLanguage(CareMoreUtilities.EnglishKey, false);
			else
				CareMoreUtilities.SetLanguage(CareMoreUtilities.SpanishKey, false);
		}

		private string GetUrlWithoutLanguage(Uri pageUrl)
		{
			string path = pageUrl.AbsolutePath;
			string query = pageUrl.Query;

			if (string.IsNullOrEmpty(query))
			{
				return path;
			}
			else
			{
				string noEnglish = Regex.Replace(query,
					CareMoreUtilities.LanguageQuery + "=" + CareMoreUtilities.EnglishKey,
					CareMoreUtilities.LanguageQuery, RegexOptions.IgnoreCase);

				return path + Regex.Replace(noEnglish,
					CareMoreUtilities.LanguageQuery + "=" + CareMoreUtilities.SpanishKey,
					CareMoreUtilities.LanguageQuery, RegexOptions.IgnoreCase);
			}
		}

		private void SetLogo()
		{
			InternalLinkField logoLocation = base.ConfigurationItem.Fields["CareMore Logo Location"];

			foreach (Item item in logoLocation.TargetItem.Children)
			{
				string locale = item["LocalizedForState"];

				if (locale.Contains(base.CurrentStateId))
				{
					imgLogo.Item = item;
					break;
				}
			}
		}

		private void SetMainNavigation()
		{
			Item parent = ItemIds.GetItem("PRIMARY_NAV_ID");
			List<Item> items = CareMoreUtilities.GetItemsCheckState(parent);

			List<PrimaryNavItem> list = new List<PrimaryNavItem>();

			foreach (Item item in items)
			{
				PrimaryNavItem navItem = new PrimaryNavItem(item);
				LinkField linkField = item.Fields["Link"];

				navItem.Width = item["Width"];
				navItem.TargetItem = linkField.TargetItem;

				list.Add(navItem);
			}

			// Update selected
			Item parentItem = CurrentItem;

			while (parentItem != null)
			{
				PrimaryNavItem matchItem = list.FirstOrDefault(p => p.TargetItem != null && p.TargetItem.ID == parentItem.ID);

				if (matchItem != null)
				{
					matchItem.CssClass = "active";
					break;
				}

				parentItem = parentItem.Parent;
			}

			rptMainNav.DataSource = list;
			rptMainNav.DataBind();
		}

		private void SetStaticText()
		{
			hlState.NavigateUrl = LinkManager.GetItemUrl(((InternalLinkField)base.ConfigurationItem.Fields["Change State Link"]).TargetItem);
			lblState.Item = base.CurrentLocaleItem;
			lblChangeState.Item = base.ConfigurationItem;

			if (this.HideLangaugeLink)
			{
				lnkBtnLanguage.Visible = false;
			}
			else
			{
				lnkBtnLanguage.Attributes["rel"] = "nofollow";
				lnkBtnLanguage.Visible = true;
				lblLanguage.Item = base.ConfigurationItem;
			}

			lblTextSize.Item = base.ConfigurationItem;

			txtSearchInput.Attributes["placeholder"] = base.ConfigurationItem["Search Terms Label"];
		}

		private void SetTopNavigation()
		{
			Item parent = ItemIds.GetItem("TOP_NAV_ID");
			List<Item> list = CareMoreUtilities.GetItemsCheckState(parent);

			rptTopNav.DataSource = list;
			rptTopNav.DataBind();
		}

		#endregion
	}
}
