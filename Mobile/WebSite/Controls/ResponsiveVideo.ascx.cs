﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Website.Controls
{
	public partial class ResponsiveVideo : System.Web.UI.UserControl
	{
		#region Properties

		public string Poster
		{
			get
			{
				return this.vidControl.Poster;
			}
			set
			{
				if (string.IsNullOrWhiteSpace(value))
					this.vidControl.Poster = string.Empty;
				else if (value.StartsWith("~"))
					this.vidControl.Poster = VirtualPathUtility.ToAbsolute(value);
				else
					this.vidControl.Poster = value;
			}
		}

		public string Source
		{
			get
			{
				return this.srcControl.Src;
			}
			set
			{
				if (string.IsNullOrWhiteSpace(value))
					this.srcControl.Src = string.Empty;
				else if (value.StartsWith("~"))
					this.srcControl.Src = VirtualPathUtility.ToAbsolute(value);
				else
					this.srcControl.Src = value;
			}
		}

		#endregion

		#region Page events

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
		}

		#endregion
	}
}
