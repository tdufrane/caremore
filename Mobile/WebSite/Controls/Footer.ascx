﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Footer.ascx.cs" Inherits="Website.Controls.Footer" %>

<!-- Begin Controls/Footer -->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-6 col-md-3 text-center">
<asp:Repeater ID="rptFooterPrimaryLeft" runat="server">
	<HeaderTemplate>
				<ul>
	</HeaderTemplate>
	<ItemTemplate>
					<li><sc:Link ID="lnkFooterPrimaryLeft" runat="server" Field="Link" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></li>
	</ItemTemplate>
	<FooterTemplate>
				</ul>
	</FooterTemplate>
</asp:Repeater>
			</div>

			<div class="col-xs-6 col-md-3 text-center">
<asp:Repeater ID="rptFooterPrimaryMiddle" runat="server">
	<HeaderTemplate>
				<ul>
	</HeaderTemplate>
	<ItemTemplate>
					<li><sc:Link ID="lnkFooterPrimaryMiddle" runat="server" Field="Link" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></li>
	</ItemTemplate>
	<FooterTemplate>
				</ul>
	</FooterTemplate>
</asp:Repeater>
			</div>

			<div class="col-xs-6 col-md-3 text-center">
<asp:Repeater ID="rptFooterPrimaryRight" runat="server">
	<HeaderTemplate>
				<ul>
	</HeaderTemplate>
	<ItemTemplate>
					<li><sc:Link ID="lnkFooterPrimaryRight" runat="server" Field="Link" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></li>
	</ItemTemplate>
	<FooterTemplate>
				</ul>
	</FooterTemplate>
</asp:Repeater>
			</div>
			<div class="col-xs-6 col-md-3 text-center">
<asp:Repeater ID="rptFooterSecondaryLinks" runat="server">
	<HeaderTemplate>
				<ul>
	</HeaderTemplate>
	<ItemTemplate>
					<li><sc:Link ID="lnkFooterSecondary" runat="server" Field="Link" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></li>
	</ItemTemplate>
	<FooterTemplate>
				</ul>
	</FooterTemplate>
</asp:Repeater>
			</div>
		</div>

		<div class="row">
			<asp:Panel id="pnlNonDiscrimination" runat="server" ClientIDMode="Static" CssClass="col-xs-12 text-xs-center col-md-6">
				<sc:FieldRenderer ID="frNonDiscrimination" runat="server" FieldName="Text" />
			</asp:Panel>
			<asp:Panel id="pnlDisclaimer" runat="server" ClientIDMode="Static" CssClass="col-xs-12 text-xs-center col-md-6 text-right">
				<sc:FieldRenderer ID="frDisclaimer" runat="server" FieldName="Text" />
			</asp:Panel>
		</div>

		<div class="row">
			<div class="col-xs-12 text-xs-center col-md-4" id="cmsApproval">
				<sc:FieldRenderer ID="frCmsApproval" runat="server" FieldName="Text" />
			</div>
			<div class="col-xs-12 text-xs-center col-md-4 col-md-push-4 text-right" id="lastUpdated">
				<sc:Text ID="txtLastUpdatedLabel" runat="server" Field="Last Updated Label" />
				<asp:Label ID="lblLastUpdated" runat="server" />
			</div>
			<div class="col-xs-12 col-md-4 col-md-pull-4 text-center" id="social">
<asp:Repeater ID="rptSocial" runat="server">
	<ItemTemplate>
				<sc:Link ID="lnkSocial" runat="server" Field="Link" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>">
					<sc:Image ID="imgSocial" runat="server" Field="Image" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></sc:Link>
	</ItemTemplate>
	<SeparatorTemplate>
				&nbsp;
	</SeparatorTemplate>
</asp:Repeater>
			</div>
		</div>
	</div>
</footer>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
<!-- End Controls/Footer -->
