﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using CareMore.Web.DotCom;

namespace Website.Controls
{
	public partial class GoogleMap : System.Web.UI.UserControl
	{
		#region Constructors

		public GoogleMap()
		{
			InitialZoom = 10;
		}

		#endregion

		#region Properties

		public byte InitialZoom { get; set; }
		public String GoogleMapID { get; set; }
		public List<LocationItem> Locations = new List<LocationItem>();

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			GoogleMapID = ConfigurationManager.AppSettings["GoogleMapsAPIKey"];
		}

		#endregion

		#region Public methods

		public void DisplayMap()
		{
			StringBuilder mapLocations = new StringBuilder();

			foreach (LocationItem location in Locations)
			{
				mapLocations.AppendFormat("\"{0}, {1} {2}|{3}|{4}|{5}|{6}|{7}\",\n",
					location.Address1, location.City, location.ZipCode,
					location.Name, location.IconImage, location.Description,
					LocationHelper.GetFormattedPhoneNo(location.Phone), location.ItemURL);
			}

			if (mapLocations.Length > 0)
			{
				mapLocations.Length -= 2;  // Remove trailing ,\n

				litLocationsArray.Text = string.Format("<script type=\"text/javascript\">var locations = new Array({0});</script>", mapLocations);
			}
		}

		#endregion
	}
}
