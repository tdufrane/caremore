﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;
using Website.WebControls;

namespace Website.Controls
{
	public partial class Video : Website.Sublayouts.BaseUserControl
	{
//		#region Constructors
//
//		public Video()
//		{
//			Height = "200";
//			Width = "328";
//			ImageFieldName = "Video Image";
//		}
//
//		#endregion

		#region Properties

//		public string Width { get; set; }
//		public string Height { get; set; }
		public Item VideoItem { get; set; }
//		public string ImageFieldName { get; set; }

//		public bool HasImage
//		{
//			get
//			{
//				if (VideoItem == null)
//					return false;
//				else
//					return !string.IsNullOrWhiteSpace(VideoItem[ImageFieldName]);
//			}
//		}

//		public bool HasVideo
//		{
//			get
//			{
//				if (VideoItem == null)
//					return false;
//				else
//					return !string.IsNullOrWhiteSpace(VideoItem["Video URL"]);
//			}
//		}
//
//		public string ImagePath
//		{
//			get
//			{
//				string url = string.Empty;
//
//				if (VideoItem != null)
//				{
//					ImageField previewFld = VideoItem.Fields[ImageFieldName];
//
//					if (previewFld != null)
//					{
//						url = GetMediaItemUrl(previewFld.MediaItem);
//					}
//				}
//
//				return url;
//			}
//		}
//
//		public string PlayerID
//		{
//			get
//			{
//				return pnlVideo.ClientID;
//			}
//		}
//
//		public string VideoPath
//		{
//			get
//			{
//				string url = string.Empty;
//
//				if (VideoItem != null)
//				{
//					LinkField link = VideoItem.Fields["Video URL"];
//
//					if (link != null)
//					{
//						if (link.IsMediaLink && link.TargetItem != null)
//						{
//							MediaItem media = new MediaItem(link.TargetItem);
//							url = MediaManager.GetMediaUrl(media).Replace(".ashx", "." + media.Extension);
//						}
//						else
//						{
//							url = link.Url;
//						}
//					}
//				}
//
//				return url;
//			}
//		}

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected override void OnDataBinding(EventArgs e)
		{
			base.OnDataBinding(e);

			try
			{
				if (VideoItem == null)
				{
					VideoItem = WebControlUtil.GetItemFromDataBind(NamingContainer);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			if (VideoItem == null && DataSource != null && DataSource.TemplateName == "Video")
			{
				VideoItem = DataSource;
			}

			if (VideoItem != null)
			{
				Sitecore.Data.Fields.ImageField videoImage = VideoItem.Fields["Video Image"];

				if (videoImage == null || videoImage.MediaItem == null)
					vidControl.Poster = string.Empty;
				else
					vidControl.Poster = MediaManager.GetMediaUrl(videoImage.MediaItem);

				LinkField videoUrl = VideoItem.Fields["Video URL"];

				if (string.IsNullOrWhiteSpace(videoUrl.Url))
					srcControl.Src = string.Empty;
				else
					srcControl.Src = videoUrl.Url;
			}
		}

		#endregion
	}
}
