﻿using System;
using Sitecore;
using Sitecore.Pipelines.HttpRequest;

namespace Website
{
	public class HTTPS
	{
		public void Process(HttpRequestArgs args)
		{
			Sitecore.Data.Items.Item item = Context.Item;

			if (item != null)
			{
				if ((item.Fields["SSL"] != null) && (item.Fields["SSL"].Value == "1"))
				{
					if (!args.Context.Request.IsSecureConnection)
					{
						args.Context.Response.Redirect(
							args.Context.Request.Url.AbsoluteUri.Replace("://", "s://"));
					}
				}
				else if (args.Context.Request.IsSecureConnection)
				{
					args.Context.Response.Redirect(
						args.Context.Request.Url.AbsoluteUri.Replace("s://", "://"));
				}
			}
		}
	}
}
