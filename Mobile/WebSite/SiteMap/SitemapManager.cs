﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using Sitecore.Data.Items;
using Sitecore.Data;
using Sitecore.Collections;
using System.Web.UI.WebControls;
using Sitecore.Links;

using System.Web.UI;
using Sitecore;
using System.Text.RegularExpressions;
using Sitecore.Web;
using Sitecore.Configuration;

namespace Website.SiteMap
{
    /// <summary>
    /// Organizes the graphing and rendering of sitemaps
    /// </summary>
    public class SitemapManager
    {
        private Database _db;
        private DeviceItem _defaultDevice;
        private Item _sitemapConfig;
        private bool _showProducts = false;

        public SitemapManager(bool showProducts)
            : this()
        {
            _showProducts = showProducts;
        }

        public SitemapManager()
        {
            _db = Factory.GetDatabase("master");

            _defaultDevice = _db.Resources.Devices["/sitecore/layout/Devices/Default"];
            _sitemapConfig = _db.GetItem("/sitecore/content/Home/Site settings/Sitemap Config");
        }

        #region htmlmap
        /// <summary>
        /// Takes the sitemap graph and creates an ul/li nested html representation
        /// </summary>
        /// <returns></returns>
        public HtmlGenericControl GenerateHtmlSiteMap()
        {
            DataNode<Item> _root = GenerateItemGraph();
            HtmlGenericControl _ul = new HtmlGenericControl("ul");

            _ul.Controls.Add(GenerateLI(_root));

            return _ul;
        }

        /// <summary>
        /// Generates UL/LI html tags from Sitemap Graph
        /// </summary>
        /// <param name="nodes"></param>
        /// <returns></returns>
        private HtmlGenericControl GenerateUL(List<DataNode<Item>> nodes)
        {
            HtmlGenericControl _ul = new HtmlGenericControl("ul");
            foreach (DataNode<Item> _node in nodes)
            {
                _ul.Controls.Add(GenerateLI(_node));
            }
            return _ul;
        }

        /// <summary>
        /// Generates LI html tags from Sitemap Graph
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private HtmlGenericControl GenerateLI(DataNode<Item> node)
        {
            HtmlGenericControl _li = new HtmlGenericControl("li");

            string _url = GetUrl(node.Value);
            if (_url.Length > 0)
            {
                HyperLink _hl = new HyperLink();
                _hl.NavigateUrl = _url;
                _hl.Text = GetTitle(node.Value);
                _li.Controls.Add(_hl);
            }
            else
            {
                _li.Controls.Add(new LiteralControl(GetTitle(node.Value)));
            }

            if (node.Children.Count > 0)
                _li.Controls.Add(GenerateUL(node.Children));

            return _li;
        }

        #endregion

        #region info

        /// <summary>
        /// Decides the url of the <a href="[url]"> generated for the Html map
        /// depending on item template type
        /// </summary>
        /// <param name="item"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        
        static ID _careCenterID = new ID("{DA7D2C44-C9A2-4164-B610-59B52CE4B1CA}");
        static ID _gymID = new ID("{D9E6FBCB-ED04-4010-B9F7-1AB11BCC4C4A}");
        static ID _hospitalID = new ID("{C35950A1-44C6-4875-A1DE-DA03D19FAE3C}");
        static ID _urgentCareID = new ID("{49D4BB1A-6FF1-4697-892B-9D9CAF01DEB4}");

        private string GetUrl(Item item, UrlOptions options)
        {
            if (item.TemplateName == "Care Center")
            {
                return GetItemUrl2(item, _careCenterID, options);
            }
            else if (item.TemplateName == "Gym")
            {
                return GetItemUrl2(item, _gymID, options);
            }
            else if (item.TemplateName == "Hospital")
            {
                return GetItemUrl2(item, _hospitalID, options);
            }
            else if (item.TemplateName == "Urgent Care")
            {
                return GetItemUrl2(item, _urgentCareID, options);
            }
            else if (item.TemplateName == "Plan") 
            {
                return GetPlanUrl(item, options);
            }
            //generic content case
            if (HasDefaultLayout(item))
            {
                return Sitecore.Links.LinkManager.GetItemUrl(item, options);
            }
            return "";
        }

        private string GetPlanUrl(Item item, UrlOptions options)
        {
            Item county = item.Parent.Parent;
            Item state = county.Parent;
            Item plan = Sitecore.Context.Database.Items[item["Plan Type"]];
            return SEO.DescriptiveUrlRewrite.GetDescriptivePlanUrl(plan.Name,state.Name,county.Name);
        }

        private string GetItemUrl2(Item item, ID _detailsID, UrlOptions options)
        {
            Item detailsPage = Sitecore.Context.Database.GetItem(_detailsID);
            return string.Format("{0}?facilityID={1}", Sitecore.Links.LinkManager.GetItemUrl(detailsPage, options), item.ID.ToString());
        }

        /// <summary>
        /// Prepends server url for xml sitemaps
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private string EnsureFullUrl(string url)
        {
            if (url.StartsWith("http"))
                return url;
            if (url.StartsWith("/"))
            {
                return string.Format("{0}{1}", WebUtil.GetServerUrl(), url);
            }
            return url;
        }

        /// <summary>
        /// Get Url of an Item
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private string GetUrl(Item item)
        {
            return GetUrl(item, LinkManager.GetDefaultUrlOptions());
        }

        /// <summary>
        /// Get Title of an item
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private string GetTitle(Item item)
        {
            string _title = item["Title"];
            if (_title.Length > 0)
                return _title;

            return item.DisplayName;
        }

        #endregion

        #region xmlmap

        /// <summary>
        /// Generates the flat xml sitemap from the Item Graph
        /// </summary>
        /// <returns></returns>
        public XDocument GenerateXmlSiteMap()
        {
            //string _emptyDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" />";

            XNamespace _ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XDocument _doc = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"),
                new XElement(_ns + "urlset"));


            Item _home = Sitecore.Context.Database.SelectSingleItem("/sitecore/content/home");
            DataNode<Item> _root = GetItemGraph(_home);
            DataNode<Item> _facilities = GetFacilities();
            _root.Add(_facilities);
            DataNode<Item> _events = GetEvents();
            _root.Add(_events);
            DataNode<Item> _plans = GetPlans();
            _root.Add(_plans);           
            
            List<Item> _items = _root.GetDescendantValues(); //Flattens the Graph

            foreach (Item _item in _items)
            {
                if (_item != null)
                {
                    XElement _urlElement = GetXmlUrlElement(_item, _ns);
                    if (_urlElement != null)
                    {
                        _doc.Root.Add(_urlElement);
                    }
                }
            }
            return _doc;
        }

        private DataNode<Item> GetEvents()
        {
            DataNode<Item> _eventsNode = new DataNode<Item>(null);
            Item[] _events = Sitecore.Context.Database.SelectItems("fast:/sitecore/content/Events DB/*/*/*");
            foreach (Item _event in _events)
            {
                _eventsNode.Add(_event);
            }
            return _eventsNode;
        }

        private DataNode<Item> GetFacilities()
        {
            DataNode<Item> _facilitiesNode = new DataNode<Item>(null);
            Item[] _facilities = Sitecore.Context.Database.SelectItems("fast:/sitecore/content/Facilities DB/*/*");
            foreach (Item _facility in _facilities)
            {
                _facilitiesNode.Add(_facility);
            }
            return _facilitiesNode;
        }

        private DataNode<Item> GetPlans() 
        {
            DataNode<Item> _plansNode = new DataNode<Item>(null);
            
            Item[] _locations = Sitecore.Context.Database.SelectItems("fast:/sitecore/content/global/regions/*/*");
            
            foreach (Item loc in _locations) 
            {
                Item [] coveragePlans = loc.Axes.SelectItems("*[@Hidden != '1']/*");

                foreach (Item plan in coveragePlans)
                {
                    _plansNode.Add(plan);
                }
            }

            return _plansNode;
        }

        /// <summary>
        /// generates individual xml element for item
        /// </summary>
        /// <param name="item"></param>
        /// <param name="ns"></param>
        /// <returns></returns>
        private XElement GetXmlUrlElement(Item item, XNamespace ns)
        {
            UrlOptions _options = LinkManager.GetDefaultUrlOptions();
            _options.AlwaysIncludeServerUrl = true;
            string _itemUrl = GetUrl(item, _options);

            if (_itemUrl.Length == 0)
                return null;
            XElement _url = new XElement(ns + "url",
                new XElement(ns + "loc", EnsureFullUrl(_itemUrl)),
                new XElement(ns + "lastmod", item.Statistics.Updated.ToString("yyyy-MM-dd"))
                );
            return _url;
        }


        #endregion

        #region graphing

        /// <summary>
        /// Generates the Item graph of the site. Staring from the HomePage
        /// </summary>
        /// <returns></returns>
        public DataNode<Item> GenerateItemGraph()
        {
            Item _home = Sitecore.Context.Database.SelectSingleItem("/sitecore/content/home");
            DataNode<Item> _root = GetItemGraph(_home);

            return _root;
        }


        /// <summary>
        /// Generates the current DataNode and depending on the item, the DataNode's children
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private DataNode<Item> GetItemGraph(Item item)
        {
            DataNode<Item> node = new DataNode<Item>(item);
            foreach (Item child in item.Children) 
            {
                if (HasDefaultLayout(child))
                {
                    node.Add(GetItemGraph(child));
                }
            }
            return node;
        }

        //Checks if this page is normally renderable on the content site
        private bool HasDefaultLayout(Item item)
        {
            return item.Visualization.GetLayout(_defaultDevice) != null;
        }

        #endregion



    }



    /// <summary>
    /// Represents a Tree structure Graph containing items to build the sitemap
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DataNode<T>
    {
        public T Value { get; set; }
        public List<DataNode<T>> Children;

        public DataNode(T value)
        {
            Value = value;
            Children = new List<DataNode<T>>();
        }

        public void Add(DataNode<T> node)
        {
            Children.Add(node);
        }

        public void Add(T value)
        {
            Add(new DataNode<T>(value));
        }

        /// <summary>
        /// Flattens out the graph
        /// </summary>
        /// <returns></returns>
        public List<T> GetDescendantValues()
        {
            List<T> _output = new List<T>();
            _output.Add(Value);
            foreach (DataNode<T> _child in Children)
            {
                _output.AddRange(_child.GetDescendantValues());
            }
            return _output;
        }

    }

}
