﻿function clearField(field) {
	if(field.value == field.title) {
		field.value = '';
	}
}

function initClearFields() {
	var mainForm = document.forms[0];
	for(i=0; i<mainForm.elements.length; i++){
		var obj = mainForm.elements[i];
		if(obj.type == 'text' || obj.type == 'textarea') {
			obj.title = obj.value;
			Event.observe(obj, 'focus', function() {
				clearField(this);
			}, false);
		}
	}
}

Event.observe(window, 'load', initClearFields, false);
