﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using AjaxControlToolkit;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Data.Managers;

namespace Website.Sublayouts
{
    /// <summary>
    /// This class acts as the base class for all pages and incorporates important functionality of redirecting to correct localized item/page
    /// </summary>
    public class BasePage : System.Web.UI.Page
    {
        #region Protected variables

        protected Database currentDB = Sitecore.Context.Database;
        protected Item currentItem = Sitecore.Context.Item;

        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
                DisplayLocalizedPageContent();
        }

        /// <summary>
        /// This function takes care of localized page/item redirection
        /// </summary>
        protected virtual void DisplayLocalizedPageContent()
        {
            Session.Remove("LastUpdated");

            string currentLanguage = CareMoreUtilities.GetCurrentLanguage();

            //select the region specific header
            if (Session["LocalizationStateSelected"] != null)
            {
                //redirect to appropriate localized item if not already
                if (currentItem == null)
                {
                    Response.Redirect(LinkManager.GetItemUrl(CareMoreUtilities.HomeItem));
                }
                else
                {
                    if ((!currentItem["IsNotLocalized"].Equals("1")) && (!string.IsNullOrWhiteSpace(currentItem["LocalizedForState"])))
                    {
                        Item localizedItem = CareMoreUtilities.GetLocalizedItem(currentItem);

                        if (localizedItem != null && localizedItem.ID != Sitecore.Context.Item.ID)
                        {
                            Response.Redirect(LinkManager.GetItemUrl(localizedItem));
                        }
                    }
                }
            }
        }

        protected virtual Item GetSourceItem(Item item)
        {
            Item thisItem = item;

            if (Request.QueryString["Source"] != null)
                thisItem = currentDB.Items[Request.QueryString["Source"]];

            return thisItem;
        }
    }
}
