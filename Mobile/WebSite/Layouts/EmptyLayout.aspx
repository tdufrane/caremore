<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="EmptyLayout.aspx.cs" Inherits="Website.Layouts.EmptyLayout" MaintainScrollPositionOnPostback="true" %>

<!doctype html>
<html>
<head id="head1" runat="server">
	<title id="pageTitle" runat="server"></title>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>

<form id="formMain" runat="server">
	<div class="container">
		<!-- begin main content -->
		<sc:Placeholder ID="placeholderMainContent" runat="server" Key="content" />
		<!-- end main content -->
	</div>
</form>

</body>
</html>
