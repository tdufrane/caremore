﻿using System;
using System.Web;
using CareMore.Web.DotCom;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Links;
using Website.Sublayouts;

namespace Website.Layouts
{
	public partial class PageLayout : System.Web.UI.Page
	{
		#region Private variables

		private Item ConfigurationItem { get; set; }
		private Database CurrentDb { get; set; }
		private Item CurrentItem { get; set; }

		#endregion

		#region Page events

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			this.ConfigurationItem = ItemIds.GetItem("MAPD_SITE_SETTINGS");
			this.CurrentDb = Sitecore.Context.Database;
			this.CurrentItem = Sitecore.Context.Item;

			if (!IsPostBack)
			{
				CheckEnglishOnly();
				CheckStatePage();
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			LoadPage();
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			if (!IsPostBack)
			{
				if (!string.IsNullOrEmpty(Request.Form["q"]))
				{
					Response.Redirect("/Search.aspx?q=" + Request.Form["q"].ToString(), false);
				}
				else if (Common.IsInternalIpAddress(Request))
				{
					// Check to see if IP Address is from CareMore
					phGoogleAnalytics.Visible = false;
				}

				pageTitle.Text = CareMoreUtilities.GetItemDisplayName(this.CurrentItem, true);
			}
			else if (scriptManager.IsInAsyncPostBack)
			{
				return;
			}

			// Select the region specific color theme
			SetStateStyleSheet();
			SetCanonicalUrl();
			SetCustomCode();
			SetThirdPartyLinksMessage();
		}

		private void CheckEnglishOnly()
		{
			// Check to ensure English only pages are only viewed in English
			if (CurrentItem != null)
			{
				Language english = Language.Parse(CareMoreUtilities.EnglishKey);
				if (english != null && Sitecore.Context.Language.Name != english.Name)
				{
					if (CurrentItem.Versions.Count == 0)
					{
						Item englishItem = CurrentDb.GetItem(CurrentItem.ID, english);
						Sitecore.Context.Item = englishItem;
					}
				}
				else if (CurrentItem["English Only"].Equals("1"))
				{
					ucHeader.HideLangaugeLink = true;
				}
			}
		}

		private void CheckStatePage()
		{
			if (!CurrentItem[CareMoreUtilities.IsNotLocalizedKey].Equals(CareMoreUtilities.CheckedTrueKey))
			{
				Item stateItem = CareMoreUtilities.GetCurrentLocaleItem();
				Item firstState = stateItem;
				bool stateIsInList = false;

				// Figure out what state(s) the current item belongs to
				MultilistField states = (MultilistField)CurrentItem.Fields[CareMoreUtilities.LocalizedForStateKey];
				if (states != null)
				{
					Item[] items = states.GetItems();

					if (items != null && items.Length > 0)
					{
						firstState = items[0];

						for (int index = 0; index < items.Length; index++)
						{
							if (stateItem.ID.Equals(items[index].ID))
							{
								stateIsInList = true;
								break;
							}
						}
					}
				}

				// If the current item belongs to different state(s)
				if (!stateIsInList && firstState.ID != stateItem.ID)
				{
					// If coming from this website, then try to find the state's page as a child
					// If coming from outside redirect to the specific page
					if (Request.UrlReferrer == null || Request.UrlReferrer.Host != Request.Url.Host)
					{
						CareMoreUtilities.SetState(firstState["Name"], true);
					}
					else
					{
						Item statePageItem = CareMoreUtilities.GetLocalizedItem(CurrentItem);
						Response.Redirect(LinkManager.GetItemUrl(statePageItem));
					}
				}
			}
		}

		private void SetCanonicalUrl()
		{
			HttpRequest request = HttpContext.Current.Request;
			Uri uri = new Uri(request.Url, request.RawUrl);

			string url = uri.AbsolutePath;
			string baseUrl = uri.GetComponents(UriComponents.Scheme | UriComponents.Host, UriFormat.Unescaped);

			if (Sitecore.Configuration.Settings.AliasesActive && CurrentDb.Aliases.Exists(url))
			{
				Item targetItem = CurrentDb.GetItem(CurrentDb.Aliases.GetTargetID(url));
				linkCanonical.Href = string.Format("{0}{1}", baseUrl, LinkManager.GetItemUrl(targetItem));
			}
			else
			{
				string itemUrl = LinkManager.GetItemUrl(Sitecore.Context.Item);
				if (url.Equals(itemUrl, StringComparison.OrdinalIgnoreCase))
				{
					linkCanonical.Visible = false;
				}
				else
				{
					linkCanonical.Href = string.Format("{0}{1}", baseUrl, itemUrl);
				}
			}
		}

		private void SetStateStyleSheet()
		{
			Item item = CareMoreUtilities.GetCurrentLocaleItem();

			if (item == null || string.IsNullOrEmpty(item["Style Sheet"]))
				pageStyle.Visible = false;
			else
				pageStyle.Href = "/css/" + item["Style Sheet"];
		}

		private void SetThirdPartyLinksMessage()
		{
			litConfirmExternal.Text = ConfigurationItem["Third Party Links Message"];
		}

		private void SetCustomCode()
		{
			litCustomCode.Text = Common.GetCustomCode(CurrentItem, Request, this.IsPostBack);
		}

		#endregion
	}
}
