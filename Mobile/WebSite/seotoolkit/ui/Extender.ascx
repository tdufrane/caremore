<%@ Control Language="C#" Inherits="System.Web.UI.UserControl" %>
<%@ Import namespace="Sitecore.SeoToolkit"%>
<%@ Import namespace="Sitecore.SeoToolkit"%>
<%@ Register TagPrefix="seo" TagName="SearchEngines" Src="~/SeoToolkit/UI/SearchEngines.ascx" %>
<%@ Register TagPrefix="seo" TagName="Keywords" Src="~/SeoToolkit/UI/Keywords.ascx" %>
<%@ Register TagPrefix="seo" TagName="PageInformation" Src="~/SeoToolkit/UI/PageInformation.ascx" %>
<%@ Import Namespace="Sitecore.SeoToolkit" %>
<link rel="stylesheet" href="<%= SeoPath.SeoToolKit %>/style.aspx" />
<%-- hides web edit floatie before the load event occurs. doesn't use prototype because it doesn't seem to behave well at this stage--%>

<script type="text/javascript">
  if (document.getElementById('scFloatie')) 
  { 
    document.getElementById('scFloatie').style.visibility = 'hidden';
  }
</script>

<script type="text/javascript">
    var helpButtonUrl = '<%=Sitecore.Resources.Images.GetThemedImageSource("Applications/16x16/help.png", Sitecore.Web.UI.ImageDimension.id16x16)%>';
    var deleteButtonUrl = '<%=Sitecore.Resources.Images.GetThemedImageSource("Applications/16x16/delete.png", Sitecore.Web.UI.ImageDimension.id16x16)%>';
    
    // Fill javaScript dictionary with translated resources according to context language    
    <%=new Sitecore.SeoToolkit.Localization.LocalizationClientHelper().TranslationDictionary.ToString()%>;
</script>

<script type="text/javascript" src="<%= SeoPath.Prototype%>/prototype.js"></script>

<script type="text/javascript" src="<%= SeoPath.Scriptaculous%>/scriptaculous.js?load=effects"></script>

<script type="text/javascript" src="<%= SeoPath.Lib %>/Console/ConsoleStub.js"></script>

<script type="text/javascript" src="<%= SeoPath.Lib %>/Base/Base.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/tabs.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/Util.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/Effects.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/seo.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/page.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/Path.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/location.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/InformationLine.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/information.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/ElementBase.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/CollectionBase.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/ImageCollection.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/image.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/imageRenderer.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/LinkCollection.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/link.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/linkRenderer.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/HeadingCollection.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/Heading.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/HeadingRenderer.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/metaTags.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/text.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/keywords.js"></script>

<script type="text/javascript" src="<%= SeoPath.SeoToolKit %>/PageEditorDisabler.js"></script>

<div id="extender">
   <ul id="tabnav">
      <li id="seoInfoTab" tab="infoContent" class="active"><a href="#"><%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("PAGE_INFORMATION_TAB_NAME"))%></a></li>
      <li id="seoTextTab" tab="textContent"><a href="#"><%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("TEXT_ONLY_VIEW_TAB_NAME"))%></a></li>
      <li id="seoKeywordsTab" tab="keywordsContent"><a href="#"><%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("KEYWORDS_TAB_NAME"))%></a></li>
      <li id="seoEnginesTab" tab="enginesContent"><a href="#"><%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("SEARCH_ENGINES_TAB_NAME"))%></a></li>
      <li id="seoHeadingsTab" tab="headingsContent"><a href="#"><%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("HEADINGS_TAB_NAME"))%></a></li>
      <li id="seoImagesTab" tab="imagesContent"><a href="#"><%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("IMAGES_TAB_NAME"))%></a></li>
      <li id="seoLinksTab" tab="linksContent"><a href="#"><%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("LINKS_TAB_NAME"))%></a></li>
      <li id="seoMetaTab" tab="metaContent"><a href="#"><%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("META_TAGS_TAB_NAME"))%></a></li>
   </ul>
   <div id="tabcontents">
      <seo:PageInformation runat="server" />
      <seo:SearchEngines runat="server" />
      <div style="display: none" id="imagesContent">
         <div id="seoImagePlaceholder"> 
         </div>
      </div>
      <div style="display: none" id="headingsContent">
         <div id="seoHeadingReport">
         </div>
      </div>
      <div style="display: none" id="linksContent">
         <div id="seoLinksLoadingPlaceholder">
         </div>
         <div id="seoInvalidLinksPlaceholder">
         </div>
         <div id="seoValidLinksPlaceholder">
         </div>
      </div>
      <div style="display: none" id="metaContent">
         <div id="seoMetaPlaceholder">
         </div>
      </div>
      <div style="display: none" id="textContent">
         <div id="seoTextPlaceholder">
         </div>
      </div>
      <seo:Keywords runat="server" />
   </div>
</div>
