<%@ Control Language="C#" Inherits="System.Web.UI.UserControl" %>

<script type="text/javascript">
    var warningImageUrl = '<%=Sitecore.Resources.Images.GetThemedImageSource("Applications/32x32/Warning.png", Sitecore.Web.UI.ImageDimension.id32x32)%>';
</script>

<div style="display: none" id="keywordsContent">
    <div id="seoKeywordsPlaceholder">
        <table id="seoKeywords" class="keywordContainer">
            <thead>
                <tr>
                    <th>
                        <%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("ONE_WORD_KEYPHRASES"))%>
                    </th>
                    <th>
                        <%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("TWO_WORD_KEYPHRASES"))%>
                    </th>
                    <th>
                        <%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("THREE_WORD_KEYPHRASES"))%>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td id="seoKeywords0">
                    </td>
                    <td id="seoKeywords1">
                    </td>
                    <td id="seoKeywords2">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
