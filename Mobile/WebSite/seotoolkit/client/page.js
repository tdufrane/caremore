var SeoPage = Base.extend({
  constructor: function() {
    this.images = new SeoImageCollection();
    this.links = new SeoLinkCollection();
    this.headings = new SeoHeadingCollection();
    
    this.meta = new SeoMetaTags();
    this.text = new SeoText();
    this.keywords = new SeoKeywords();
    this.location = new SeoLocation();
    this.information = new SeoInformation();
    
    /* obsolete 'safe move' shortcuts */
    this.baseUrl = this.location.baseUrl;
    this.href = this.location.path;
     
    this.charset = this.getCharset();
  },

  initialize: function() {
    // cache the page html as early after the load event as possible, so that it can be posted to server for analysis
    this._rawHTML = this.html();
  },

  html: function() {
    if (this._rawHTML) {
      return this._rawHTML;
    }
    else {
      if (document.documentElement.outerHTML) {
        return document.documentElement.outerHTML
      }
      
      // this looses <html> element attributes, but none of the server side services need it
      return '<html>' + document.documentElement.innerHTML + '</html>';
    }
  },

  /* parses the page into seo elements */
  parse: function() {
    console.group("page parse");
    console.time("page parse");

    this.headings.parse();
    this.images.parse();
    this.links.parse();
    this.meta.parse();
    this.text.parse();
    this.keywords.parse();
    
    console.timeEnd("page parse");
    console.groupEnd();
  },

  renderReport: function() {
    var placeholder = $('staticInformationPlaceholder');  
    
    placeholder.innerHTML += this.information.title.toHTML();
    placeholder.innerHTML += this.information.url.toHTML();
    placeholder.innerHTML += this.information.urlKeywords.toHTML();
    placeholder.innerHTML += this.information.description.toHTML();
    placeholder.innerHTML += this.information.keywords.toHTML();
    placeholder.innerHTML += this.information.language.toHTML();
    placeholder.innerHTML += this.information.charset.toHTML();
    
    $('dynamicInformationPlaceholder').innerHTML = 
      '<p style="height: 26px; line-height: 26px"><img src="' + 
      this.baseUrl + '/seotoolkit/client/img/progress.gif" style="float:left; height: 26px" /> ' + 
      localizationDictionary["ANALYZING_PAGE_STRUCTURE"] + '</p>';
  },

  /* renders the dynamic part of the page information, that is the one that depends on server ajax calls and cannot be shown immediately*/
  renderDynamicReport: function() {
    var placeholder = $('dynamicInformationPlaceholder');
    
    placeholder.innerHTML = ''; // hide 'progress' indicator
    
    placeholder.innerHTML += this.information.headings.toHTML();
    placeholder.innerHTML += this.information.images.toHTML();
    placeholder.innerHTML += this.information.links.toHTML();
    placeholder.innerHTML += this.information.metaTags.toHTML();
  },

  attachOverlays: function() {
    this.images.attachOverlays();
  },

  getCharset: function() {
    if (document.charset) return document.charset; // IE charset
    else return document.characterSet; // mozilla alternative
  }
});