var SeoImage = ElementBase.extend({
  constructor: function(element) {
    this.base(element, "img");

    this.alt = this.domElement.alt;
    
    /* remove alt to suppress browser tooltip */
    if (this.domElement.alt) {
      this.domElement.alt = "";
    }

    this.src = this.domElement.src;
    
    this.height = this.domElement.height;
    this.width = this.domElement.width;  
    
    this.duplicates = new Array();
    this.originalImage = null;
    
    this.attachEvents();    
  },
  
  isContentManaged: function() {
    return this.src.indexOf('.ashx') > 0;
  },
  
  equals: function(other) {
    return (this.src == other.src) && (this.alt == other.alt);  
  },
  
  occurrences: function() {
    if (this.isOriginal()) {
      return this.duplicates.length + 1;
    }
    
    return this.originalImage.occurrences();  
  },
  
  isOriginal: function() {
    return this.originalImage == null;
  },
  
  hasDuplicates: function() {
    return this.occurrences() > 1;
  },
  
  addDuplicate: function(image) {
    image.originalImage = this;
    this.duplicates.push(image);
  },
  
  attachEvents: function() {
    Event.observe(this.domElement, 'mouseover', this.onMouseOver.bindAsEventListener(this));
  },
  
  onMouseOver: function(evt) {
    if (this.domElement.getHeight() >= 8 && this.domElement.getWidth() >= 8) {
      this.enableHover();
      this.showTooltip();
    }
  },
  
  showTooltip: function(permanent) {
    if (this.frame == null) {
      this._showOverlayFrame(permanent);
      
      if (permanent) {
        this._showInfoWindow();
      }
      else {      
        setTimeout(this._onDelayedPopup.bind(this), 250);
      }
    }
  },
  
  _showOverlayFrame: function(permanent, offset) {
    var offset = this.cumulativeOffset();
  
    var link = this.domElement.up('a');
    var frame = document.createElement('div');
    frame.className = 'seoImageFrame';
    frame.style.left = offset[0] + 'px';
    frame.style.top = offset[1] + 'px';
    frame.style.height = this.domElement.getHeight() > 2 ? this.domElement.getHeight() - 2 + 'px' : '2px';
    frame.style.width = this.domElement.getWidth() > 2 ? this.domElement.getWidth() - 2 + 'px' : '2px';
    
    if (link != null) {
      frame.innerHTML = new Template('<div class="seoInnerImageFrame"><a style="height:100%; width:100%" href="#{href}"><div style="height:100%; width: 100%; float: left; cursor: pointer">&nbsp;</div></a></div>').evaluate(link);
    }
    else {    
      frame.innerHTML = '<div class="seoInnerImageFrame">&nbsp;</div>';
    }

    this.frame = frame;
    if (!permanent) {
      seo.page.images.activeTooltips.push(this);
    }
    document.body.appendChild(frame);
    frame.childNodes[0].style.background = 'black';
  },
  
  _onDelayedPopup: function() {
    if (!this.within(seo.pointer.x, seo.pointer.y) || this.frame == null) return;
    
    this._showInfoWindow();
  },
  
  _showInfoWindow: function() {
    var offset = this.cumulativeOffset();
  
    var tooltipLeft = offset[0] + 'px';
    var tooltipTop = offset[1] + this.domElement.getHeight() + 'px';
    
    var tooltip = document.createElement('div');
    tooltip.className = 'seoTooltip';
    tooltip.style.left = tooltipLeft;
    tooltip.style.top = tooltipTop;
    tooltip.style.display = 'none'
    tooltip.innerHTML = this.renderer.getTooltipHTML(this);    
    document.body.appendChild(tooltip);
    seo.effects.tooltipAppear(tooltip);

    var helpLink = $$(tooltip.id + 'help')[0]; 
    if (helpLink) {
      Event.observe(helpLink, 'click', this.showHelp.bindAsEventListener(this));
    }

    Event.observe(tooltip, 'click', this.hideTooltip.bindAsEventListener(this));    
    this.tooltip = tooltip;
  },
  
  hideTooltip: function(evt) {  
    if (this.frame != null) {
      this.frame.parentNode.removeChild(this.frame);
      this.frame = null;    
    }
    
    this.base();
  },
  
  within: function(x, y) {
    if (this.tooltip != null) {
      return this.withinElement(x, y) || Position.within(this.tooltip, x, y);
    }
    else {
      return this.withinElement(x, y);
    }  
  },

  showHelp: function(evt) {
    window.open(Event.element(evt).parentNode.href);  
    Event.stop(evt);
  },
  
  attachOverlays: function(evt) {
    if (this.getHeight < 10 || this.getWidth < 10) return;
    if (this.isValid()) return;

    var offset = this.cumulativeOffset();
    var overlayLeft = offset[0] + 'px';
    var overlayTop = offset[1] + this.domElement.getHeight() - 16 + 'px';
    
    var overlay = document.createElement('div');
    overlay.className = 'seoOverlay';
    overlay.style.left = overlayLeft;
    overlay.style.top = overlayTop;
    overlay.innerHTML = '<img src="' + seo.page.baseUrl + deleteButtonUrl + '" />';
    document.body.appendChild(overlay);
    
    Event.observe(overlay, 'mouseover', this.showTooltip.bind(this));
    Event.observe(overlay, 'mouseout', this.hideTooltip.bind(this));
    
    this.overlay = overlay;     
    
  },
  
  toString: function() {
    return this.controlID + "[" + this.alt + "]";
  }
});