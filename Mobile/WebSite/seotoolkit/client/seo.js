var scSeo = Base.extend({
  constructor: function() {
    this.extender = null;
    this.version = '0.0.0.0';
    
    this.idSequence = 0;
    this.progressTasks = 0;
    
    this.elements = new Object();
    
    this.effects = new SeoEffects();
    this.util = new seoUtility();
  },

  initialize: function() {
    if (!$('extender')) return;
    
    console.group('seo initialize');
    
    this.extender = $('extender');
    this.assertElement(this.extender, 'extender');
    
    this.page = new SeoPage();
    this.page.initialize();
    
    this.progressHTML = '<div class="seoProgress"><img src="' + this.page.baseUrl + '/sitecore/shell/themes/standard/images/progress.gif" /><p>' + 
      localizationDictionary["PLEASE_WAIT_WHILE_REPORT_GENERATED"] + '</p></div>';
    this.page.parse();
    
    this.registerProgress('seo initialize');
    this.page.attachOverlays();  
    
    this.render();
    
    Event.observe(document, 'mousemove', this.onMouseMove.bindAsEventListener(this));
    
    this.unregisterProgress('seo initialize');  
    console.groupEnd();
  },

  registerElement: function(element, prefix) {
    var id = prefix + this.idSequence++;
    this.elements[id] = element;
    return id;
  },

  focus: function(controlID) {
    console.log('focus %s', controlID);
    
    var element = this.findControl(controlID);
    this.assertElement(element, "focus element");
    this.assertElement(element.domElement, "focus inner dom element");
    
    element.focus();
  },

  findControl: function(controlID) {
    return this.elements[controlID];
  },

  assertElement: function(element, name) {
    console.assert(element != null, name + ' element not found');
  },

  render: function() { 
    console.group("render");
    
    this.page.renderReport();
    this.page.headings.renderReport($('seoHeadingReport'));
    this.page.images.renderReport($('seoImagePlaceholder'));
    this.page.meta.renderReport($('seoMetaPlaceholder'));
    
    console.groupEnd();
  },

  registerProgress: function() {
    if (this.progressTasks == 0) {
      this._displayProgress();
    }
    this.progressTasks++;
  },

  unregisterProgress: function() {
    this.progressTasks--;
    
    if (this.progressTasks == 0) {
      this._hideProgress();
      this.page.renderDynamicReport();
    }
  },

  onMouseMove: function(evt) {
    var x = Event.pointerX(evt);
    var y = Event.pointerY(evt);
    this.pointer = {x: x, y: y};
  },
  
  onResize: function(evt) {
    $H(this.elements).each(function(pair) {
      var element = pair.value;
      if (element.onWindowResize) {
        element.onWindowResize(evt);
      }
    });
  },

  _displayProgress: function() {
    var tooltip = document.createElement('div');
    tooltip.className = 'seoProgressTooltip';
    tooltip.style.left = document.body.offsetWidth - 150 + 'px';
    tooltip.innerHTML = '<img src="' + this.page.baseUrl + 
      '/seotoolkit/client/img/progress.gif" /><span>' + localizationDictionary["ANALYZING_PAGE"] + '</span>';
    document.body.appendChild(tooltip);  
    this.progress = tooltip;
  },

  _hideProgress: function() {
    if (this.progress) {
      this.progress.parentNode.removeChild(this.progress);
      this.progress = null;
    }
  },

  formatNoElements: function(text) {
    return '<div class="seoNoElements"><p>' + text + '</p></div>';
  },

  formatError: function(text) {
  return '<p class="seoLogError"><img alt="" src="' + seo.page.baseUrl + warningImageUrl + '"/>' + text + '</p>';
  },

  debug: function() {
    return this.debug;
  }
});

var seo = new scSeo();
Event.observe(window, 'load', seo.initialize.bind(seo));
Event.observe(window, 'resize', seo.onResize.bindAsEventListener(seo));