﻿using System;
using System.Collections.Generic;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Website
{
    public class ItemFieldComparer : IComparer<Item>
    {
        private string _fieldIdentifier = null;

        #region Constructors

        private ItemFieldComparer()
        {
        }

        public ItemFieldComparer(string fieldName)
        {
            this._fieldIdentifier = fieldName;
        }

        public ItemFieldComparer(ID fieldID)
        {
            this._fieldIdentifier = fieldID.ToString();
        }

        #endregion

        #region IComparer<Item> Members

        public int Compare(Item xItem, Item yItem)
        {
            if (xItem == null && yItem == null)
            {
                return 0;
            }
            else if (xItem != null && yItem == null)
            {
                return -1;
            }
            else if (xItem == null && yItem != null)
            {
                return 1;
            }

            Field xField = xItem.Fields[this._fieldIdentifier]; 
            Field yField = yItem.Fields[this._fieldIdentifier];

            if (yField == null && xField == null)
            {
                return 0;
            }
            else if (xField != null && yField == null)
            {
                return -1;
            }
            else if (yField != null && xField == null)
            {
                return 1;
            }

            if (string.IsNullOrEmpty(yField.Value) && !string.IsNullOrEmpty(xField.Value))
            {
                return -1;
            } 
            else if (string.IsNullOrEmpty(xField.Value) && !string.IsNullOrEmpty(yField.Value))
            {
                return 1;
            }
            else if (xField.Value == yField.Value)
            {
                return 0;
            }
            else
            {
                return this.Compare(xField, yField);
            }
        }

        private int Compare(Sitecore.Data.Fields.Field xField, Sitecore.Data.Fields.Field yField)
        {
            string fieldType = String.Empty;
            int test;

            if (xField.ID == FieldIDs.Sortorder || (Int32.TryParse(xField.Value, out test) && (Int32.TryParse(yField.Value, out test))))
            {
                fieldType = "integer"; 
            }
            else if (DateUtil.IsIsoDate(xField.Value) && DateUtil.IsIsoDate(yField.Value))
            {
                fieldType = "date";
            }

            switch (fieldType)
            {
                case "date":
                case "datetime":
                    DateTime xDate = DateUtil.IsoDateToDateTime(xField.Value);
                    DateTime yDate = DateUtil.IsoDateToDateTime(yField.Value);
                    return xDate.CompareTo(yDate);

                case "integer":
                    return Int32.Parse(xField.Value).CompareTo(Int32.Parse(yField.Value));

                default:
                    return xField.Value.CompareTo(yField.Value);
            }
        }
        #endregion
    }
}
