﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Security;
using Sitecore.Security.Accounts;

namespace Website.sitecore_modules.Providers
{
	public partial class ImportProviders : System.Web.UI.Page
	{
		Database currentDB = Factory.GetDatabase("master");

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Sitecore.Context.IsLoggedIn)
			{
				phForm.Visible = false;
				CareMoreUtilities.DisplayError(phStatus, "You are not authorized to access this page as you need to logon to Sitecore first.");
			}
			else if (!Sitecore.Context.User.IsAdministrator)
			{
				phForm.Visible = false;
				CareMoreUtilities.DisplayError(phStatus, "You are not authorized to access this page as you are not assigned as an 'Administrator'.");
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				ClearStatus();

				string fileName = UploadFile();
				string[] itemDetails = ddlFullPath.SelectedValue.Split(new char[]{'|'});

				LiveImport(fileName, itemDetails);
				// Comment out above and uncomment below to DEBUG
				//DebugImport(fileName, itemDetails);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phStatus, ex);
			}
		}

		private void LiveImport(string fileName, string[] itemDetails)
		{
			ImportProvidersJob providersJob = new ImportProvidersJob();
			providersJob.StartJob(fileName, txtActiveSheet.Text, itemDetails[0], new Guid(itemDetails[1]));
			Response.Redirect("ImportProvidersStatus.aspx?id=" + providersJob.Job.Handle.ToString(), false);
		}

		private void DebugImport(string fileName, string[] itemDetails)
		{
			ImportProvidersJob providersJob = new ImportProvidersJob();
			providersJob.ImportData(fileName, txtActiveSheet.Text, itemDetails[0], new Guid(itemDetails[1]));

			if (providersJob.Errors.Length > 0)
				CareMoreUtilities.DisplayError(phStatus, providersJob.Errors.ToString().Replace("\n", "<br />"));
		}

		private void ClearStatus()
		{
			phStatus.Controls.Clear();
			phStatus.Visible = false;
		}

		private string UploadFile()
		{
			string fileName = HttpContext.Current.Server.MapPath("~/userFiles") + "\\" + txtExcelFile.FileName;
			txtExcelFile.PostedFile.SaveAs(fileName);
			return fileName;
		}
	}
}
