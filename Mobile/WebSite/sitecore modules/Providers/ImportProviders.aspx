﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportProviders.aspx.cs" Inherits="Website.sitecore_modules.Providers.ImportProviders"
	MasterPageFile="~/sitecore modules/Site.Master" Title="CareMore > Providers > Import" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<h2>Provider Import</h2>

	<asp:PlaceHolder ID="phForm" runat="server">
		<p><asp:ValidationSummary id="valSumPage" runat="server" HeaderText="Please correct the following fields:" /></p>

		<table id="tblImport">
			<tr>
				<td class="label">Full Path of Excel File:</td>
				<td>
					<asp:FileUpload ID="txtExcelFile" runat="server" Width="400" />
					<asp:RequiredFieldValidator ID="reqFldValExcelFile" runat="server"
						ControlToValidate="txtExcelFile" ErrorMessage="Full Path of Excel File is required."
						Display="Dynamic" Text="*" />
					<asp:RegularExpressionValidator ID="regExpValExcelFile" runat="server"
						ControlToValidate="txtExcelFile" ErrorMessage="Only an Excel 97-2003 File (.xls) can be used."
						Display="Dynamic" Text="*"
						ValidationExpression="^.+(\.xls)$" />
				</td>
			</tr>
			<tr class="alt">
				<td class="label">Active Sheet Name:</td>
				<td>
					<asp:TextBox ID="txtActiveSheet" runat="server" Text="Sheet1" />
					<asp:RequiredFieldValidator ID="reqFldValActiveSheet" runat="server"
						ControlToValidate="txtActiveSheet" ErrorMessage="Active Sheet Name is required."
						Display="Dynamic" Text="*" />
				</td>
			</tr>
			<tr valign="top">
				<td class="label">Provider Type</td>
				<td>
					<asp:DropDownList ID="ddlFullPath" runat="server">
						<asp:ListItem Text="-select one-" Value="" Selected="True" />
						<asp:ListItem Text="MAPD - Gym" Value="/sitecore/content/Facilities DB/Gym|{690DCC1C-056F-4081-9C00-645469FBD7BD}" />
						<asp:ListItem Text="MAPD - Care Centers" Value="/sitecore/content/Facilities DB/Care Centers|{12D32D3A-8C8A-43CF-8BEE-2C40043D7C78}" />
						<asp:ListItem Text="DUALS - Acupuncture" Value="/sitecore/content/Duals/Global/Facilities DB/Acupuncture|{260A1692-A87C-4C19-8179-A9E25343B631}" />
						<asp:ListItem Text="DUALS - Health and Fitness" Value="/sitecore/content/Duals/Global/Facilities DB/Health and Fitness|{59871370-7471-4D2B-AFDC-E3732F0F411A}" />
						<asp:ListItem Text="DUALS - CareMore Care Centers" Value="/sitecore/content/Duals/Global/Facilities DB/CareMore Care Centers|{636BCF74-BF8E-40B6-94C7-C38C7DA30429}" />
						<asp:ListItem Text="DUALS - Mental Health" Value="/sitecore/content/Duals/Global/Facilities DB/Mental Health|{60F996F6-EE27-4F01-A533-76E92C61E4D6}" />
						<asp:ListItem Text="DUALS - Vision" Value="/sitecore/content/Duals/Global/Facilities DB/Vision|{B00B2BF3-49D8-4D72-A20A-79A4EB50EB19}" />
					</asp:DropDownList>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<asp:LinkButton ID="btnSubmit" runat="server" ClientIDMode="Static" OnClick="BtnSubmit_Click" CssClass="button btnSmall"><span>Import</span></asp:LinkButton>
				</td>
			</tr>
		</table>

		<br />

		<asp:Panel id="pnlProcessing" runat="server" ClientIDMode="Static" style="display: none;">
			<img id="imgLoading" src="/images/ajax-loader.gif" alt="..." />
			<div id="divStatus">Processing please wait...</div>
		</asp:Panel>
	</asp:PlaceHolder>

	<asp:PlaceHolder ID="phStatus" runat="server" Visible="false" />
</asp:Content>
