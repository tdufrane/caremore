﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportProvidersStatus.aspx.cs" Inherits="Website.sitecore_modules.Providers.ImportProvidersStatus"
	MasterPageFile="~/sitecore modules/Site.Master" Title="CareMore > Providers > Import Results" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<h2>Import Providers Results</h2>

	<asp:ScriptManager ID="scriptMgrPage" runat="server"></asp:ScriptManager>
	<asp:UpdatePanel ID="updatePnlPage" runat="server">
		<ContentTemplate>
			<p id="pProcessing" runat="server" visible="false">Processing <img id="imgLoading" src="/images/ajax-loader.gif" alt="..." /></p>

			<p id="pStatus" runat="server" visible="false" />

			<p id="pImport" runat="server" visible="false"><a href="ImportProviders.aspx">Import another provider file.</a></p>

			<asp:Timer ID="timerPage" runat="server" Interval="5000" OnTick="TimerPage_Tick" />

			<asp:HiddenField ID="hidHandle" runat="server" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
