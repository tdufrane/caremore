﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Web;
using Sitecore.Data.Items;

namespace Website.sitecore_modules.Providers
{
	public class ProviderItem
	{
		#region Constructors

		public ProviderItem()
		{
		}

		public ProviderItem(OleDbDataReader dr)
		{
			try
			{
				Accessibility = new List<string>();
				Languages = new List<string>();
				FromReader(dr);
			}
			catch (Exception ex)
			{
				ReaderError = ex.ToString();
			}
		}

		#endregion

		#region Properties

		public string ProviderSubType { get; set; }
		public string ProviderID { get; set; }
		public string ProviderNpiID { get; set; }
		public string ProviderName { get; set; }
		public string Location { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string County { get; set; }
		public string State { get; set; }
		public string ZipCode { get; set; }
		public string Title { get; set; }
		public string Phone { get; set; }
		public string Fax { get; set; }
		public List<string> Accessibility { get; set; }
		public string ReportedAs { get; set; }
		public bool AcceptingNewPatients { get; set; }
		public string HoursOfOperation { get; set; }
		public List<string> Languages { get; private set; }
		public bool Transportation { get; set; }
		public bool CulturalCompetency { get; set; }
		public bool SpecializedTraining { get; set; }
		public string ReaderError { get; private set; }

		#endregion

		#region Private methods

		private bool FromReader(OleDbDataReader dr)
		{
			ProviderSubType = CheckNull(dr["SUBTYPE"]);
			ProviderID = dr["OFFICE ID"].ToString();
			ProviderNpiID = CheckNull(dr["NPI"]);
			ProviderName = string.Format("{0} {1}", dr["PROVIDER FNAME"], dr["PROVIDER LNAME"]);
			Location = CheckNull(dr["PRACTICE NAME"]);
			Address1 = dr["OFFICE ADDRESS"].ToString();
			Address2 = CheckNull(dr["ADDRESS2"]);
			City = dr["OFFICE CITY"].ToString();
			ZipCode = dr["ZIP"].ToString();
			Title = CheckNull(dr["TITLE"]);
			County = dr["COUNTY"].ToString();
			State = dr["ST"].ToString();

			Phone = CheckPhoneNumber(dr["OFFICE PHONE"]);
			Fax = CheckPhoneNumber(dr["FAX NUMBER"]);

			CheckAccessibility(dr["EXAM ROOM HANDICAP ACCESS"], "E");
			CheckAccessibility(dr["EXTERNAL BUILDING HANDICAP ACCESS"], "EB");
			CheckAccessibility(dr["INTERNAL BUILDING HANDICAP ACCESS"], "IB");
			CheckAccessibility(dr["PARKING HANDICAP ACCESS"], "P");
			CheckAccessibility(dr["RESTROOM HANDICAP ACCESS"], "R");
			CheckAccessibility(dr["WHEELCHAIR WEIGHT SCALE"], "T");

			ReportedAs = CheckNull(dr["DISABILITY ACCESS S/A"], string.Empty);

			string monday    = CheckNull(dr["OFFICE MON HOURS"], string.Empty);
			string tuesday   = CheckNull(dr["OFFICE TUE HOURS"], string.Empty);
			string wednesday = CheckNull(dr["OFFICE WED HOURS"], string.Empty);
			string thursday  = CheckNull(dr["OFFICE THU HOURS"], string.Empty);
			string friday    = CheckNull(dr["OFFICE FRI HOURS"], string.Empty);
			string saturday  = CheckNull(dr["OFFICE SAT HOURS"], string.Empty);
			string sunday    = CheckNull(dr["OFFICE SUN HOURS"], string.Empty);

			StringBuilder hours = new StringBuilder();
			if ((monday.Length > 0) && (!monday.Equals("-")))
				hours.AppendFormat("Monday: {0}<br />", monday);

			if ((tuesday.Length > 0) && (!tuesday.Equals("-")))
				hours.AppendFormat("Tuesday: {0}<br />", tuesday);

			if ((wednesday.Length > 0) && (!wednesday.Equals("-")))
				hours.AppendFormat("Wednesday: {0}<br />", wednesday);

			if ((thursday.Length > 0) && (!thursday.Equals("-")))
				hours.AppendFormat("Thursday: {0}<br />", thursday);

			if ((friday.Length > 0) && (!friday.Equals("-")))
				hours.AppendFormat("Friday: {0}<br />", friday);

			if ((saturday.Length > 0) && (!saturday.Equals("-")))
				hours.AppendFormat("Saturday: {0}<br />", saturday);

			if ((sunday.Length > 0) && (!sunday.Equals("-")))
				hours.AppendFormat("Sunday: {0}<br />", sunday);

			if (hours.Length > 0)
			{
				hours.Length -= 6;  // Remove trailing "<br />"
				HoursOfOperation = hours.ToString();
			}

			AcceptingNewPatients = CheckYes(dr["ACCEPTING NEW PATIENTS"]);

			CheckLanguages(dr["OFFICE LANG 1"]);
			CheckLanguages(dr["OFFICE LANG 2"]);
			CheckLanguages(dr["OFFICE LANG 3"]);
			CheckLanguages(dr["OFFICE LANG 4"]);

			Transportation = CheckYes(dr["PUBLIC TRANSPORTATION HANDICAP ACCESS"]);
			CulturalCompetency = CheckYes(dr["CULTURAL COMPETENCY"]);
			SpecializedTraining = CheckYes(dr["SPECIALIZED TRAINING"]);

			return true;
		}

		private void CheckAccessibility(object field, string code)
		{
			if (CheckYes(field))
				Accessibility.Add(code);
		}

		private void CheckLanguages(object field)
		{
			string value = CheckNull(field, null);
			if (!string.IsNullOrEmpty(value))
			{
				Languages.Add(value);
			}
		}

		private string CheckNull(object field)
		{
			return CheckNull(field, null);
		}

		private string CheckNull(object field, string whenNullReturnThis)
		{
			if (field == null)
			{
				return whenNullReturnThis;
			}
			else if (DBNull.Value.Equals(field))
			{
				return whenNullReturnThis;
			}
			else
			{
				return field.ToString().Trim();
			}
		}

		private string CheckPhoneNumber(object field)
		{
			string formatted;
			string value = CheckNull(field);

			if (string.IsNullOrWhiteSpace(value))
				formatted = string.Empty;
			else if (value.Contains('(')) // most likely preformatted
				formatted = value;
			else if (value.Length == 10)  // most likely numbers only 1234567890
				formatted = string.Format("({0}) {1}-{2}", value.Substring(0, 3), value.Substring(3, 3), value.Substring(6, 4));
			else
				formatted = value;

			return formatted;
		}

		private bool CheckYes(object field)
		{
			string value = CheckNull(field, null);

			if (string.IsNullOrEmpty(value))
				return false;
			else if (value.Equals("Y", StringComparison.OrdinalIgnoreCase) || value.Equals("Yes", StringComparison.OrdinalIgnoreCase))
				return true;
			else
				return false;
		}

		#endregion
	}
}
