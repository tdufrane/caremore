﻿using System;
using Sitecore.Shell.Framework.Commands;

namespace Website.sitecore_modules.Providers
{
	[Serializable]
	public class ProvidersImport : Command
	{
		public override void Execute(CommandContext context)
		{
			Sitecore.Layouts.PageContext pageContext = Sitecore.Context.Page;
			Uri uri = pageContext.Page.Request.Url;
			string hostName = uri.Host;

			string url = "http://" + hostName + "/sitecore modules/Providers/ImportProviders.aspx";
			Sitecore.Shell.Framework.Windows.RunUri(url, string.Empty, "Import Providers");
		}
	}
}
