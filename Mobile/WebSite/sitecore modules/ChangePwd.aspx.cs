﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace Website.sitecore_modules
{
    public partial class ChangePwd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                phMessage.Visible = false;
            }
            else
            {
                if ((Session["RedirectPath"] == null) || (Session["RedirectUrl"] == null))
                {
                    this.aLogOff.Visible = false;
                    phForm.Visible = false;
                    CareMoreUtilities.DisplayError(phMessage, "You must be logged in to use this page.");
                }
                else
                {
                    this.aLogOff.HRef =  Session["RedirectPath"].ToString() + "Login.aspx";
                    PageSetup();
                }
            }
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ChangePassword();
        }

        private void PageSetup()
        {
            if (Session["AdminUserName"] != null)
            {
                SetupForm(Session["AdminUserName"].ToString());
            }
            else if (Session["BrokerAdminName"] != null)
            {
                SetupForm(Session["BrokerAdminName"].ToString());
            }
            else if (Session["LeadAdminName"] != null)
            {
                SetupForm(Session["LeadAdminName"].ToString());
            }
            else if (Session["OrderAdminName"] != null)
            {
                SetupForm(Session["OrderAdminName"].ToString());
            }
            else if (Session["SpecialtyAdminName"] != null)
            {
                SetupForm(Session["SpecialtyAdminName"].ToString());
            }
            else
            {
                phForm.Visible = false;
                CareMoreUtilities.DisplayError(phMessage, "You must be logged in to use this page.");
            }
        }

        private void SetupForm(string loginName)
        {
            CaremoreApplicationDataContext dbContext = new CaremoreApplicationDataContext();

            IEnumerable<AdminUser> users = from user in dbContext.AdminUsers
                                           where (user.Username == loginName)
                                           select user;

            AdminUser adminUser = users.First();
            lblLogin.Text = adminUser.Username;
        }

        private void ChangePassword()
        {
            CaremoreApplicationDataContext dbContext = new CaremoreApplicationDataContext();

            IEnumerable<AdminUser> users = from user in dbContext.AdminUsers
                                           where (user.Username == lblLogin.Text)
                                           select user;

            AdminUser adminUser = users.First();

            if (txtOldPassword.Text == adminUser.Password)
            {
                adminUser.Password = txtPassword.Text.Trim();
                adminUser.PasswordReset = true;
                dbContext.SubmitChanges();

				Response.Redirect(Session["RedirectPath"].ToString() + Session["RedirectUrl"].ToString(), false);
            }
            else
            {
                CareMoreUtilities.DisplayError(phMessage, "Current Password is not correct.");
            }
        }
    }
}
