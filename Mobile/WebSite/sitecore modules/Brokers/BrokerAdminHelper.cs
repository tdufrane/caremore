﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using Sitecore.Data.Items;

namespace Website.sitecore_modules.Brokers
{
	public static class BrokerAdminHelper
	{
		public static bool ApproveBroker(string registrationId, string updatedBy, Item brokerRegItem)
		{
			bool approved = false;

			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			BrokerRegistration profile = BrokerAdminHelper.GetBroker(db, registrationId);

			if ((profile != null) && (!profile.Verified))
			{
				profile.Verified = true;
				profile.Disabled = false;
				profile.UpdatedBy = updatedBy;
				profile.UpdatedOn = DateTime.Now;

				db.SubmitChanges();

				MailMessage message = new MailMessage();
				message.To.Add(new MailAddress(profile.Email));
				message.From = new MailAddress(brokerRegItem["From"]);
				message.Subject = brokerRegItem["Subject"];
				message.Body = brokerRegItem["Confirmation"];
				message.IsBodyHtml = true;

				CareMoreUtilities.SendEmail(message);

				approved = true;
			}

			return approved;
		}

		public static bool DeleteBroker(string registrationId)
		{
			bool deleted = false;

			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			BrokerRegistration profile = BrokerAdminHelper.GetBroker(db, registrationId);

			if (profile != null)
			{
				db.BrokerRegistrations.DeleteOnSubmit(profile);
				db.SubmitChanges();
				deleted = true;
			}

			return deleted;
		}

		public static bool DisableBroker(string registrationId, string updatedBy)
		{
			return ToggleDisableBroker(registrationId, updatedBy, true);
		}

		public static bool EnableBroker(string registrationId, string updatedBy)
		{
			return ToggleDisableBroker(registrationId, updatedBy, false);
		}

		private static bool ToggleDisableBroker(string registrationId, string updatedBy, bool disabled)
		{
			bool success = false;

			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			BrokerRegistration profile = BrokerAdminHelper.GetBroker(db, registrationId);

			if ((profile != null) && (profile.Disabled != disabled))
			{
				profile.Disabled = disabled;
				profile.UpdatedBy = updatedBy;
				profile.UpdatedOn = DateTime.Now;

				db.SubmitChanges();
				disabled = true;
			}

			return success;
		}

		public static BrokerRegistration GetBroker(CaremoreApplicationDataContext db, string registrationId)
		{
			IEnumerable<BrokerRegistration> profiles = db.BrokerRegistrations.Where(item => item.RegistrationId.ToString() == registrationId);

			if ((profiles == null) || (profiles.Count() == 0))
			{
				return null;
			}
			else
			{
				return profiles.FirstOrDefault();
			}
		}
	}
}
