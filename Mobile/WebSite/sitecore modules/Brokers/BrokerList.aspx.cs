﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom;
using Sitecore.Data;
using Sitecore.Data.Items;
using Website.Sublayouts;

namespace Website.sitecore_modules.Brokers
{
	public partial class BrokerList : System.Web.UI.Page
	{
		#region Page events

		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				if (!Sitecore.Context.IsLoggedIn)
				{
					phForm.Visible = false;
					CareMoreUtilities.DisplayError(phStatus, Master.NotAuthorized);
				}
				else if ((!Sitecore.Context.User.IsAdministrator) && (!Sitecore.Context.User.IsInRole("sitecore\\Broker Users")))
				{
					phForm.Visible = false;
					CareMoreUtilities.DisplayError(phStatus, Master.NotAuthorized);
				}
				else if (IsPostBack)
				{
					CareMoreUtilities.ClearStatus(phStatus);
				}
				else
				{
					LoadBrokersList(Request.QueryString["s"]);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phStatus, ex);
				throw;
			}
		}

		protected void BtnShowAll_Click(object sender, EventArgs e)
		{
			LoadBrokersList(string.Empty);
		}

		protected void BtnShowApproved_Click(object sender, EventArgs e)
		{
			LoadBrokersList("a");
		}

		protected void BtnShowPending_Click(object sender, EventArgs e)
		{
			LoadBrokersList("p");
		}

		protected void BtnShowDisabled_Click(object sender, EventArgs e)
		{
			LoadBrokersList("d");
		}

		protected void BtnApproveSelected_Click(object sender, EventArgs e)
		{
			try
			{
				int approved = ApproveBrokers(Sitecore.Context.User.Name);
				LoadBrokersList(hidListType.Value);
				CareMoreUtilities.DisplayStatus(phStatus, string.Format("{0} broker registrations were Approved.", approved));
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phStatus, ex);
			}
		}

		protected void BtnDeleteSelected_Click(object sender, EventArgs e)
		{
			try
			{
				int deleted = DeleteBrokers();
				LoadBrokersList(hidListType.Value);
				CareMoreUtilities.DisplayStatus(phStatus, string.Format("{0} broker registrations were Deleted.", deleted));
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phStatus, ex);
			}
		}

		protected void BtnDisableSelected_Click(object sender, EventArgs e)
		{
			try
			{
				int disabled = DisableBrokers(Sitecore.Context.User.Name);
				LoadBrokersList(hidListType.Value);
				CareMoreUtilities.DisplayStatus(phStatus, string.Format("{0} broker registrations were Disabled.", disabled));
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phStatus, ex);
			}
		}

		protected void BtnEnableSelected_Click(object sender, EventArgs e)
		{
			try
			{
				int enabled = EnableBrokers(Sitecore.Context.User.Name);
				LoadBrokersList(hidListType.Value);
				CareMoreUtilities.DisplayStatus(phStatus, string.Format("{0} broker registrations were Enabled.", enabled));
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phStatus, ex);
			}
		}

		#endregion

		#region Protected methods

		protected string Status(bool verified, bool disabled)
		{
			if (!verified)
				return "Pending";
			else if (disabled)
				return "Disabled";
			else
				return "Active";
		}

		#endregion

		#region Private methods

		private void LoadBrokersList(string status)
		{
			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			List<BrokerRegistration> brokers = new List<BrokerRegistration>();

			if (db.BrokerRegistrations.Count() > 0)
			{
				switch (status)
				{
					case "a":
						ldsBrokers.WhereParameters.Clear();
						ldsBrokers.WhereParameters.Add(new Parameter("Verified", TypeCode.Boolean, "true"));
						ldsBrokers.WhereParameters.Add(new Parameter("Disabled", TypeCode.Boolean, "false"));
						gvBrokers.EmptyDataText = "No active broker registrations.";
						ToggleTopButtons(false, true, true);
						ToggleBottomButtons(false, true, false);
						break;

					case "p":
						ldsBrokers.WhereParameters.Clear();
						ldsBrokers.WhereParameters.Add(new Parameter("Verified", TypeCode.Boolean, "false"));
						gvBrokers.EmptyDataText = "No pending broker registrations.";
						ToggleTopButtons(true, false, true);
						ToggleBottomButtons(true, false, false);
						break;

					case "d":
						ldsBrokers.WhereParameters.Clear();
						ldsBrokers.WhereParameters.Add(new Parameter("Disabled", TypeCode.Boolean, "true"));
						gvBrokers.EmptyDataText = "No disabled broker registrations.";
						ToggleTopButtons(true, true, false);
						ToggleBottomButtons(false, false, true);
						break;

					default:
						ldsBrokers.WhereParameters.Clear();
						gvBrokers.EmptyDataText = "No broker registrations.";
						ToggleTopButtons(true, true, true);
						ToggleBottomButtons(true, true, true);
						status = string.Empty;
						break;
				}
			}

			hidListType.Value = status;
			((HyperLinkField)gvBrokers.Columns[1]).DataNavigateUrlFormatString = "BrokerDetails.aspx?id={0}&s=" + status;
		}

		private void ToggleTopButtons(bool enableApproved, bool enablePending, bool enableDisabled)
		{
			btnApproved.Enabled = enableApproved;
			btnPending.Enabled = enablePending;
			btnDisabled.Enabled = enableDisabled;
		}

		private void ToggleBottomButtons(bool enableApproved, bool enableDisabled, bool enablePending)
		{
			btnApproveSelected.Enabled = enableApproved;
			btnDisableSelected.Enabled = enableDisabled;
			btnEnableSelected.Enabled = enablePending;
		}

		private int ApproveBrokers(string updatedBy)
		{
			Item brokerRegItem = ItemIds.GetItem("BROKER_REGISTRATION");
			List<string> brokerIds = GetCheckedBrokers();

			foreach (string brokerId in brokerIds)
			{
				BrokerAdminHelper.ApproveBroker(brokerId, updatedBy, brokerRegItem);
			}

			return brokerIds.Count;
		}

		private int DeleteBrokers()
		{
			List<string> brokerIds = GetCheckedBrokers();

			foreach (string brokerId in brokerIds)
			{
				BrokerAdminHelper.DeleteBroker(brokerId);
			}

			return brokerIds.Count;
		}

		private int DisableBrokers(string updatedBy)
		{
			List<string> brokerIds = GetCheckedBrokers();

			foreach (string brokerId in brokerIds)
			{
				BrokerAdminHelper.DisableBroker(brokerId, updatedBy);
			}

			return brokerIds.Count;
		}

		private int EnableBrokers(string updatedBy)
		{
			List<string> brokerIds = GetCheckedBrokers();

			foreach (string brokerId in brokerIds)
			{
				BrokerAdminHelper.EnableBroker(brokerId, updatedBy);
			}

			return brokerIds.Count;
		}

		private List<string> GetCheckedBrokers()
		{
			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			List<string> brokerIds = new List<string>();

			foreach (GridViewRow brokerListItem in gvBrokers.Rows)
			{
				string registrationId = gvBrokers.DataKeys[brokerListItem.RowIndex].Value.ToString();
				CheckBox cbApprove = (CheckBox)brokerListItem.FindControl("cbApprove");

				if (cbApprove.Checked)
				{
					brokerIds.Add(registrationId);
				}
			}

			return brokerIds;
		}

		#endregion
	}
}
