﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BrokerDetails.aspx.cs" Inherits="Website.sitecore_modules.Brokers.BrokerDetails"
	MasterPageFile="~/sitecore modules/Site.Master" Title="CareMore > Broker Admin > Broker Details" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<h1 class="heading">Broker Detail</h1>

	<asp:PlaceHolder ID="phStatus" runat="server"></asp:PlaceHolder>

<asp:PlaceHolder ID="phForm" runat="server">
	<table border="0" cellspacing="0">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" class="appDetails">
					<tr>
						<td class="label">Registration Id</td>
						<td><asp:Label ID="lblRegistrationId" runat="server" /></td>
					</tr>
					<tr class="alt">
						<td class="label">CareMore Id</td>
						<td><asp:Label ID="lblCareMoreId" runat="server" /></td>
					</tr>
					<tr>
						<td class="label">Last Name</td>
						<td><asp:Label ID="lblLastName" runat="server" /></td>
					</tr>
					<tr class="alt">
						<td class="label">First Name</td>
						<td><asp:Label ID="lblFirstName" runat="server" /></td>
					</tr>
					<tr>
						<td class="label">Middle Initial</td>
						<td><asp:Label ID="lblMiddleInitial" runat="server" /></td>
					</tr>
					<tr class="alt">
						<td class="label">Salutation</td>
						<td><asp:Label ID="lblSalutation" runat="server" /></td>
					</tr>
					<tr>
						<td class="label">Phone</td>
						<td><asp:Label ID="lblPhone" runat="server" /></td>
					</tr>
					<tr class="alt">
						<td class="label">Address</td>
						<td><asp:Label ID="lblAddress" runat="server" /></td>
					</tr>
					<tr>
						<td class="label">City</td>
						<td><asp:Label ID="lblCity" runat="server" /></td>
					</tr>
					<tr class="alt">
						<td class="label">State</td>
						<td><asp:Label ID="lblState" runat="server" /></td>
					</tr>
					<tr>
						<td class="label">Zip</td>
						<td><asp:Label ID="lblZip" runat="server" /></td>
					</tr>
					<tr class="alt">
						<td class="label">Email</td>
						<td><asp:Label ID="lblEmail" runat="server" /></td>
					</tr>
					<tr>
						<td class="label">Verified</td>
						<td><asp:Label ID="lblVerified" runat="server" /></td>
					</tr>
					<tr class="alt">
						<td class="label">Disabled</td>
						<td><asp:Label ID="lblDisabled" runat="server" /></td>
					</tr>
					<tr>
						<td class="label">Created On</td>
						<td><asp:Label ID="lblCreatedOn" runat="server" /></td>
					</tr>
					<tr class="alt">
						<td class="label">Updated On</td>
						<td><asp:Label ID="lblUpdatedOn" runat="server" /></td>
					</tr>
					<tr>
						<td class="label">Updated By</td>
						<td><asp:Label ID="lblUpdatedBy" runat="server" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="listButtons">
				<asp:Button ID="btnApprove" runat="server" Text="Approve" OnClick="BtnApprove_Click"></asp:Button>
				<asp:Label ID="lblSeparator1" runat="server"> | </asp:Label>
				<asp:Button ID="btnDisable" runat="server" Text="Disable" OnClick="BtnDisable_Click"></asp:Button>
				<asp:Label ID="lblSeparator2" runat="server"> | </asp:Label>
				<asp:Button ID="btnEnable" runat="server" Text="Enable" OnClick="BtnEnable_Click"></asp:Button>
				<asp:Label ID="lblSeparator3" runat="server"> | </asp:Label>
				<asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="BtnDelete_Click"></asp:Button>
			</td>
		</tr>
	</table>
</asp:PlaceHolder>

	<p><br />Back to <asp:HyperLink id="hlBackTo" runat="server" NavigateUrl="BrokerList.aspx?s=">Broker List</asp:HyperLink></p>
	<p>Back to <a href="Default.aspx">Broker Administration Home</a></p>
</asp:Content>
