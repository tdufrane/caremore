﻿using System;
using Sitecore.Shell.Framework.Commands;

namespace Website.sitecore_modules.Brokers
{
	[Serializable]
	public class BrokerAdminCommand : Command
	{
		public override void Execute(CommandContext context)
		{
			Sitecore.Layouts.PageContext pageContext = Sitecore.Context.Page;
			Uri uri = pageContext.Page.Request.Url;
			string hostName = uri.Host;

			string url = "http://" + hostName + "/sitecore%20modules/Brokers/Default.aspx";
			Sitecore.Shell.Framework.Windows.RunUri(url, string.Empty, "Broker Administration");
		}
	}
}
