﻿<%@ Page Language="C#" MasterPageFile="~/sitecore modules/Site.Master" AutoEventWireup="true" CodeBehind="UploadFile.aspx.cs" Inherits="Website.sitecore_modules.Support.UploadFile" Title="Upload File" %>

<asp:Content ID="contentPage" ContentPlaceHolderID="contentPlaceHolderRoot" runat="server">

<asp:PlaceHolder ID="plcForm" runat="server">
	<asp:ValidationSummary id="valSumPage" runat="server"
		HeaderText="Please correct the following fields:" />

	<table>
		<tr>
			<td>File Type:</td>
			<td>
				<asp:DropDownList ID="ddlFileTypes" runat="server">
					<asp:ListItem Text="Video" Value="video" Selected="True" />
				</asp:DropDownList>
			</td>
		</tr>
		<tr>
			<td>Full Path of <asp:Label ID="lblFileType" runat="server" /> File:</td>
			<td>
				<asp:FileUpload ID="FileToUpload" runat="server" Width="500" />
				<asp:RequiredFieldValidator ID="reqFldValFileToUpload" runat="server"
					ControlToValidate="FileToUpload" ErrorMessage="Full Path of {0} File is required." />
				<asp:RegularExpressionValidator ID="regExpValExcelFile" runat="server"
					ControlToValidate="FileToUpload" ErrorMessage="Only {0} File can be used." />
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<asp:Button ID="btnSubmit" runat="server" OnClick="BtnSubmit_Click" Text="Import" CssClass="orangeSubmitButton" />
			</td>
		</tr>
	</table>

	<br />
</asp:PlaceHolder>

	<asp:PlaceHolder ID="phStatus" runat="server" Visible="false" />
</asp:Content>
