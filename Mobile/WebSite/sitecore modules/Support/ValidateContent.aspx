﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ValidateContent.aspx.cs" Inherits="Website.sitecore_modules.Support.ValidateContent" MasterPageFile="~/sitecore modules/Site.Master" Title="Validate Content" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentHead" runat="server" ContentPlaceHolderID="contentPlaceHolderHeader">
	<script src="/js/jquery.min.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<h2>Validate Content</h2>


	<asp:PlaceHolder ID="phForm" runat="server">
		<asp:ValidationSummary id="valSumPage" runat="server" HeaderText="Please correct the following fields:" />

		<table border="0" cellspacing="0">
			<tr>
				<td class="label">Root Item</td>
				<td>
					<asp:DropDownList ID="ddlItems" runat="server" AutoPostBack="true"
						OnSelectedIndexChanged="DdlItems_SelectedIndexChanged">
						<asp:ListItem Text="Select one..." Value="" />
					</asp:DropDownList>
					<asp:RequiredFieldValidator ID="reqFldValItems" runat="server"
						ControlToValidate="ddlItems" ErrorMessage="Root Item is required."
						InitialValue="" Display="Dynamic" Text="*" />
				</td>
			</tr>
<asp:PlaceHolder ID="plcPath" runat="server" Visible="false">
			<tr class="alt" valign="top">
				<td class="label">Full Path</td>
				<td>
					<asp:TextBox ID="txtFullPath" runat="server" BackColor="Whitesmoke" TextMode="MultiLine" Rows="3" Width="400" />
					<asp:RequiredFieldValidator ID="reqFldValPath" runat="server"
						ControlToValidate="txtFullPath" ErrorMessage="Full Path is required."
						Display="Dynamic" Text="*" />

					<a id="aFullPath" runat="server" href="../PathPicker.aspx" style="margin-left:10px;"><img src="../text_tree.png" alt="Select Path" /></a>
				</td>
			</tr>
			<tr class="alt">
				<td class="label">Recursive</td>
				<td>
					<asp:CheckBox ID="chkRecursive" runat="server" />
				</td>
			</tr>
			<tr>
				<td align="right">
					<asp:Button ID="btnValidate" runat="server" Text="Validate" OnClick="BtnValidate_Click" />
				</td>
				<td>&nbsp;</td>
			</tr>
</asp:PlaceHolder>
		</table>
 
		<asp:GridView ID="gvValidated" runat="server" Visible="false"
			AutoGenerateColumns="false" CssClass="appList"
			EmptyDataText="No issues were found for selected item.">
			<AlternatingRowStyle CssClass="alt" VerticalAlign="Top" />
			<HeaderStyle CssClass="gridHeader" />
			<RowStyle VerticalAlign="Top" />
			<Columns>
				<asp:BoundField DataField="ShortPath" HeaderText="Path" />
				<asp:BoundField DataField="Field" HeaderText="Field" />
				<asp:TemplateField HeaderText="Errors">
					<ItemTemplate>
						<%# GetValidatedText(Container.DataItem) %>
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</asp:GridView>
	</asp:PlaceHolder>

	<asp:PlaceHolder ID="phStatus" runat="server" Visible="false" />
</asp:Content>
