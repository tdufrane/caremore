﻿using System;
using System.Configuration;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace Website.sitecore_modules.Support
{
    public partial class UploadFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // When this form is reused this will be dynamic
                string fileType =  "Video";
                string fileTypeName = "a Flash Video (.flv)";
                string validationExpression = @"^.+(\.flv)$";

                if ((User.Identity != null) && (!string.IsNullOrEmpty(User.Identity.Name)))
                    lblFileType.Text = fileType = " : " + User.Identity.Name;
                else
                    lblFileType.Text = fileType = " : No User.Identity.Name";
                reqFldValFileToUpload.ErrorMessage = string.Format(reqFldValFileToUpload.ErrorMessage, fileType);
                regExpValExcelFile.ErrorMessage = string.Format(regExpValExcelFile.ErrorMessage, fileTypeName);
                regExpValExcelFile.ValidationExpression = validationExpression;
            }
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ClearStatus();
            AddFile();
        }

        private void ClearStatus()
        {
            phStatus.Visible = false;
        }

        private void AddFile()
        {
            // When this form is reused this will be dynamic
            string folder = "videos";
            string filepath = Server.MapPath("~/" + folder) + "\\" + FileToUpload.FileName;

            FileToUpload.PostedFile.SaveAs(filepath);

            string copyUploadPath = ConfigurationManager.AppSettings["CopyUploadPath"];
            if (!string.IsNullOrEmpty(copyUploadPath))
            {
                if (copyUploadPath.EndsWith("\\"))
                    CopyFile(filepath, copyUploadPath + FileToUpload.FileName);
                else
                    CopyFile(filepath, copyUploadPath + "\\" + FileToUpload.FileName);
            }

            CareMoreUtilities.DisplayStatus(phStatus,
                string.Format("File <b>{0}</b> was uploaded.  URL to file is <b>http://{1}/{2}/{0}</b>.",
                    FileToUpload.FileName, Request.Url.Host, folder));
        }

        private void CopyFile(string source, string destination)
        {
            // use impersonation to access files on the server. 
            if ((User.Identity != null) && (!string.IsNullOrEmpty(User.Identity.Name)))
            {
                WindowsIdentity identity = (WindowsIdentity)User.Identity;
                WindowsImpersonationContext context = null; 

                try
                {
                    // impersonate current user 
                    context = identity.Impersonate();

                    File.Copy(source, destination, true);
                }
                finally
                {
                    // cancel impersonation 
                    if (context != null)
                        context.Undo();
                }
            }
        }
    }
}
