﻿using System;
using Sitecore.Shell.Framework.Commands;

namespace Website.sitecore_modules.Support
{
	[Serializable]
	public class ChangeTemplateCommand : Command
	{
		public override void Execute(CommandContext context)
		{
			Sitecore.Layouts.PageContext pageContext = Sitecore.Context.Page;
			Uri uri = pageContext.Page.Request.Url;
			string hostName = uri.Host;

			string url = "http://" + hostName + "/sitecore%20modules/Support/ChangeTemplate.aspx";
			Sitecore.Shell.Framework.Windows.RunUri(url, "", "Change Template");
		}
	}
}
