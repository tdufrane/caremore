﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Sitecore.Globalization;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;

namespace Website.sitecore_modules
{
	public class MassItemProcessing
	{
		#region Properties

		// SearchPath examples
		//	/sitecore/content/Global/Plan Types/* 2015/*/*/2015 Provider Pharmacy Directory
		//	/sitecore/content/Global/Plan Types/*[endswith(@@name,'2015')]/*/*/2015 Provider Pharmacy Directory
		//	/sitecore/content/Global/Regions/*/*/2015/*

		public Item FieldItem { get; set; }
		public string FieldName { get; set; }
		public bool FindIsCaseSensitive { get; set; }
		public FindMethods FindMethod { get; set; }
		public string FindText { get; set; }
		public LookAtTypes LookAt { get; set; }
		public Actions Action { get; set; }
		public string ReplaceWith { get; set; }
		public string SearchPath { get; set; }
		public Item TemplateItem { get; set; }
		public Item NewTemplateItem { get; set; }
		public bool Preview { get; set; }

		#endregion

		#region Private fields

		private List<MassItem> foundItems = null;
		private StringComparison comparison;
		private RegexOptions expressionOptions;
		private Sitecore.Data.Database master = null;

		#endregion

		#region Enumerations

		public enum FindMethods
		{
			Any = 0,
			ContainsText = 1,
			ExactMatch = 2,
			RegularExpression = 3
		}

		public enum LookAtTypes
		{
			FieldValue = 0,
			ItemName   = 1,
			Template   = 2
		}

		public enum Actions
		{
			DeleteItem         = 0,
			ReplaceFoundValue  = 1,
			ReplaceWholeValue  = 2,
			CopyToSpanish      = 3
		}

		#endregion

		#region Public methods

		public void ProcessItems()
		{
			this.foundItems = new List<MassItem>();

			if (SearchPath == null)
			{
				throw new InvalidOperationException("SearchPath property cannot be null");
			}
			else if (LookAt != LookAtTypes.Template && FindText == null)
			{
				throw new InvalidOperationException("FindText property cannot be null if not looking at Template");
			}
			else
			{
				SetupFind();

				this.master = Sitecore.Configuration.Factory.GetDatabase("master");
				Item[] items = this.master.SelectItems(SearchPath);

				foreach (Item item in items)
				{
					ProcessItem(item);
				}
			}
		}

		public string Results(HttpServerUtility server)
		{
			StringBuilder results = new StringBuilder();

			if (this.foundItems == null)
			{
				results.Append("<span class=\"errorMsg\">Please run ReplaceItems first.</span>");
			}
			else if (this.foundItems.Count == 0)
			{
				results.Append("<span class=\"errorMsg\">No matches were found.</span>");
			}
			else
			{
				results.AppendFormat("<span>Found <strong>{0}</strong> matches.</span><br />\n", this.foundItems.Count);
				results.AppendLine("<table class=\"appList\">");
				results.AppendLine("<tr class=\"gridHeader\" valign=\"top\"><td>Path</td><td>Field</td><td>Langauge</td>");

				if (LookAt == MassItemProcessing.LookAtTypes.FieldValue)
					results.AppendLine("<td>Old Value<br/>New Value</td></tr>");
				else if (LookAt == MassItemProcessing.LookAtTypes.Template)
					results.AppendLine("<td>Old Template<br/>New Template</td></tr>");
				else
					results.AppendLine("<td>Old Name<br/>New Name</td></tr>");

				int i = 1;
				foreach (MassItem item in this.foundItems)
				{
					if (i % 2 == 1)
						results.AppendLine("<tr valign=\"top\">");
					else
						results.AppendLine("<tr class=\"alt\" valign=\"top\">");

					i++;

					results.AppendFormat("<td>{0}</td><td>{1}</td><td>{2}</td><td>{3}<hr/>{4}</td>",
						item.ItemPath, item.FieldName, item.Language,
						server.HtmlEncode(item.OldValue), server.HtmlEncode(item.NewValue));

					results.AppendLine("</tr>");
				}
				results.AppendLine("</table>");
			}

			return results.ToString();
		}

		#endregion

		#region Private methods

		private void ProcessItem(Item item)
		{
			if ((TemplateItem == null) || (item.TemplateID.Equals(TemplateItem.ID)))
			{
				if (LookAt == LookAtTypes.FieldValue)
				{
					ProcessFields(item);
				}
				else if (LookAt == LookAtTypes.Template)
				{
					ProcessTemplate(item);
				}
				else
				{
					ProcessName(item);
				}
			}

			foreach (Item childItem in item.Children)
			{
				ProcessItem(childItem);
			}
		}

		private void ProcessName(Item item)
		{
			if (ValueIsMatch(item.Name))
			{
				if (Action == Actions.DeleteItem)
				{
					this.foundItems.Add(new MassItem(item.Paths.FullPath, "Name", "n/a", item.Name, "n/a"));

					if (!Preview)
						item.Recycle();
				}
				else if (Action == Actions.CopyToSpanish)
				{
					this.foundItems.Add(new MassItem(item.Paths.FullPath, "Name", "n/a", item.Name, "n/a"));

					if (!Preview)
						ProcessLanguage(item);
				}
				else
				{
					string newValue = GetNewValue(item.Name);
					this.foundItems.Add(new MassItem(item.Paths.FullPath, "Name", "n/a", item.Name, newValue));

					if (!Preview)
					{
						item.Editing.BeginEdit();
						item.Name = newValue;
						item.Editing.EndEdit();
					}
				}
			}
		}

		private void ProcessFields(Item item)
		{
			foreach (Language lang in item.Languages)
			{
				Item langItem = master.GetItem(item.ID, lang);

				if (langItem.Versions.Count > 0)
				{
					langItem.Fields.ReadAll();

					foreach (Field field in langItem.Fields)
					{
						// Dont change any system fields "__fieldname" unless exactly specified by an id
						if (((FieldItem == null) && (!field.Name.StartsWith("__"))) ||
							((FieldItem != null) && (field.ID.Equals(FieldItem.ID))))
						{
							if (ValueIsMatch(field.Value))
							{
								if (Action == Actions.DeleteItem)
								{
									this.foundItems.Add(new MassItem(langItem.Paths.FullPath, field.Name, lang.Name, field.Value, "n/a"));

									if (!Preview)
										item.Recycle();

									break; // No need to process all fields, found one
								}
								else if (Action == Actions.CopyToSpanish)
								{
									this.foundItems.Add(new MassItem(langItem.Paths.FullPath, field.Name, lang.Name, field.Value, "n/a"));

									if (!Preview)
										ProcessLanguage(item);

									break; // No need to process all fields, found one
								}
								else
								{
									string newValue = GetNewValue(field.Value);
									this.foundItems.Add(new MassItem(langItem.Paths.FullPath, field.Name, lang.Name, field.Value, newValue));

									if (!Preview)
									{
										langItem.Editing.BeginEdit();
										langItem[field.Name] = newValue;
										langItem.Editing.EndEdit();
									}

									if (FieldItem != null)
										break;  // No need to process all fields, found specific one
								}
							}
						}
					}
				}
			}
		}

		private void ProcessFields(Item englishItem, Item languageItem)
		{
			foreach (Field field in englishItem.Fields)
			{
				string newValue = null;

				// Dont change any system fields "__fieldname"
				if (!field.Name.StartsWith("__"))
				{
					if (string.IsNullOrEmpty(field.Value))
					{
						if (!string.IsNullOrEmpty(languageItem[field.Name]))
						{
							newValue = string.Empty;
						}
					}
					else if (string.IsNullOrEmpty(languageItem[field.Name]))
					{
						newValue = field.Value;
					}
					else if (LookAt == LookAtTypes.FieldValue)
					{
						if (string.IsNullOrEmpty(GetNewValue(languageItem[field.Name])))
							newValue = field.Value;
					}

					if (newValue != null)
					{
						this.foundItems.Add(new MassItem(languageItem.Paths.FullPath, field.Name, languageItem.Language.Name, field.Value, newValue));

						if (!Preview)
						{
							languageItem.Editing.BeginEdit();
							languageItem[field.Name] = newValue;
							languageItem.Editing.EndEdit();
						}
					}
				}
			}
		}

		private void ProcessLanguage(Item item)
		{
			Item englishItem = master.GetItem(item.ID, LanguageManager.GetLanguage("en"));
			Item languageItem = master.GetItem(item.ID, LanguageManager.GetLanguage("es-MX"));

			if (languageItem.Versions.Count < 1)
			{
				languageItem.Versions.AddVersion();
			}

			ProcessFields(englishItem, languageItem);
		}

		private void ProcessTemplate(Item item)
		{
			this.foundItems.Add(new MassItem(item.Paths.FullPath, "Template", "n/a", TemplateItem.Name, NewTemplateItem.Name));

			if (!Preview)
			{
				item.ChangeTemplate(NewTemplateItem);
			}
		}

		private bool ValueIsMatch(string lookAtValue)
		{
			bool found = false;

			switch (FindMethod)
			{
				case FindMethods.ContainsText:
					if (lookAtValue.IndexOf(FindText, this.comparison) > -1)
						found = true;
					break;

				case FindMethods.RegularExpression:
					if (FindText == "*")
						found = true;
					else if (Regex.IsMatch(lookAtValue, FindText, this.expressionOptions))
						found = true;
					break;

				case FindMethods.ExactMatch:
				default:
					if (lookAtValue.Equals(FindText, this.comparison))
						found = true;
					break;
			}

			return found;
		}

		private string GetNewValue(string existingValue)
		{
			string newValue;

			if (Action == Actions.ReplaceFoundValue)
			{
				if (LookAt == LookAtTypes.FieldValue)
					newValue = existingValue.Replace(FindText, ReplaceWith);
				else
					newValue = existingValue.Replace(FindText, ReplaceWith);
			}
			else
			{
				newValue = ReplaceWith;
			}

			return newValue;
		}

		private void SetupFind()
		{
			if (FindIsCaseSensitive)
			{
				this.comparison = StringComparison.Ordinal;
				this.expressionOptions = RegexOptions.None;
			}
			else
			{
				this.comparison = StringComparison.OrdinalIgnoreCase;
				this.expressionOptions = RegexOptions.IgnoreCase;
			}
		}

		#endregion
	}

	public class MassItem
	{
		public string ItemPath { get; set; }
		public string FieldName { get; set; }
		public string Language { get; set; }
		public string OldValue { get; set; }
		public string NewValue { get; set; }

		public MassItem()
		{
		}

		public MassItem(string itemPath, string fieldName, string language, string oldValue, string newValue)
		{
			this.ItemPath = itemPath;
			this.FieldName = fieldName;
			this.Language = language;
			this.OldValue = oldValue;
			this.NewValue = newValue;
		}
	}
}
