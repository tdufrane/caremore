﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Website.sitecore_modules.Admin
{
    public partial class Login : BaseAdminLogin
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.AllowType = AllowTypes.Enrollment;
            this.RedirectPath = "Admin/";
            this.RedirectUrl = "ApplicationList.aspx";
            this.SessionName = "AdminUserName";

            PageLoad(loginAdmin, cbSaveLogin);
        }

        protected void LoginAdmin_Authenticate(object sender, AuthenticateEventArgs e)
        {
            AuthenticateLogin(loginAdmin, cbSaveLogin);
        }
    }
}
