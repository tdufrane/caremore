﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Website.sitecore_modules.Admin.Login" MasterPageFile="~/sitecore modules/Site.Master" Title="CareMore > Admin Login" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<h1 class="heading">Login</h1>

	<asp:ValidationSummary id="valSumPage" runat="server"
		HeaderText="Please correct the following fields:" />

	<table border="0" cellspacing="0">
		<tr>
			<td>
				<asp:Login ID="loginAdmin" runat="server"
					DisplayRememberMe="false"
					LoginButtonStyle-CssClass="orangeSubmitButton"
					OnAuthenticate="LoginAdmin_Authenticate"
					PasswordRequiredErrorMessage="Password is required."
					TextLayout="TextOnLeft"
					TitleText=""
					UserNameRequiredErrorMessage="User Name is required." 
					LoginButtonText="&lt;span&gt;Log In&lt;/span&gt;" LoginButtonType="Link">
					<LoginButtonStyle CssClass="button btnSmall"></LoginButtonStyle>
				</asp:Login>
			</td>
		</tr>
		<tr>
			<td valign="bottom">
				<asp:CheckBox ID="cbSaveLogin" runat="server" Text="Remember User Name" CssClass="smallerText" />
			</td>
		</tr>
	</table>
</asp:Content>
