﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace Website.sitecore_modules.Admin
{
	public partial class ApplicationDetails : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				if (Session["AdminUsername"] == null)
				{
					Response.Redirect("Login.aspx", false);
				}
				else if (string.IsNullOrEmpty(Request.QueryString["Id"]))
				{
					Response.Redirect("ApplicationList.aspx", false);
				}
				else if (!IsPostBack)
				{
					LoadDetails();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phMessage, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				SaveChanges();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phMessage, ex);
			}
		}

		private MemberApplication GetApplication(CaremoreApplicationDataContext dbContext)
		{
			Guid applicationId = Guid.Empty;
			MemberApplication application = null;

			if (Guid.TryParse(Request.QueryString["Id"], out applicationId))
			{
				IEnumerable<MemberApplication> applicationList =
					from app in dbContext.MemberApplications
					where app.ApplicationId.Equals(applicationId)
					select app;

				if (applicationList != null && applicationList.Count() == 1)
				{
					application = applicationList.Single();
				}
			}

			return application;
		}

		private void LoadDetails()
		{
			CaremoreApplicationDataContext dbContext = new CaremoreApplicationDataContext();
			MemberApplication application = GetApplication(dbContext);

			if (application == null)
			{
				SetNotFoundMessage();
			}
			else
			{
				Database masterDB = Factory.GetDatabase("master");
				Item appItem = masterDB.GetItem(application.AppSitecoreId.ToString("B"));

				btnSubmit.Text = string.Format("Mark as {0}", application.Status ? "Unprocessed" : "Processed");

				lblConfirmationNumber.Text = application.ConfirmationNumber;
				lblLocation.Text = application.County;
				lblPlan.Text = application.PlanAppliedFor;

				trIpaMedicalGroup.Visible = appItem["IPA Group Number"].Equals("1");
				if (trIpaMedicalGroup.Visible)
					lblIPAGroupNumber.Text = application.IPAGroupNumber;

				trCoverageEffectiveDate.Visible = appItem["Coverage Effective Date"].Equals("1");
				if (trCoverageEffectiveDate.Visible)
					lblCoverageEffectiveDate.Text = CareMoreUtilities.DateFormat(application.CoverageEffectiveDate);

				lblLastName.Text = application.LastName;
				lblFirstName.Text = application.FirstName;
				lblMI.Text = application.MiddleInitial;
				lblSalutation.Text = application.Salutation;

				trSocialSecurity.Visible = appItem["Social Security"].Equals("1");
				if (trSocialSecurity.Visible)
					lblSSN.Text = application.SocialSecurityNumber;

				lblBirthDate.Text = application.BirthDate.ToString("MM/dd/yyyy");
				lblSex.Text = application.Sex.ToString();
				lblHomePhone.Text = application.HomePhone;
				lblAlternativePhone.Text = application.AltPhone;

				trIsCellPhone.Visible = appItem["Is Cell Phone"].Equals("1");
				if (trIsCellPhone.Visible)
					lblIsCellPhone.Text = CareMoreUtilities.YesNo(application.IsCellPhone);

				lblAddress.Text = application.Address;
				lblCity.Text = application.City;
				lblState.Text = application.State;
				lblZip.Text = application.Zip;
				lblAddress2.Text = application.Address2;
				lblCity2.Text = application.City2;
				lblState2.Text = application.State2;
				lblZip2.Text = application.Zip2;

				phMovedRows.Visible = appItem["Have You Moved"].Equals("1");
				if (phMovedRows.Visible)
				{
					lblMoved.Text = CareMoreUtilities.YesNo(application.Moved);
					lblDateOfMove.Text = CareMoreUtilities.DateFormat(application.DateOfMove);
				}

				lblEmergencyContact.Text = application.EmergencyContactName;
				lblEmergencyPhoneNumber.Text = application.EmergencyContactPhone;
				lblEmergencyContactRelationshipToYou.Text = application.EmergencyContactRelation;
				lblEmergencyContactEmailAddress.Text = application.EmailAddress;

				trEmailAddress.Visible = !appItem["Consent To Contact"].Equals("1");
				if (trEmailAddress.Visible)
					lblIsCellPhone.Text = CareMoreUtilities.YesNo(application.IsCellPhone);

				phConsentToContactRows.Visible = appItem["Consent To Contact"].Equals("1");
				if (phConsentToContactRows.Visible)
				{
					lblReceiveDocsByEmail.Text = CareMoreUtilities.YesNo(application.ReceiveDocsByEmail);
					lblReceiveInfoByEmail.Text = CareMoreUtilities.YesNo(application.ReceiveInfoByEmail);
					lblReceiveEmailAddress.Text = application.EmailAddress;
				}

				trPhysicianChoice.Visible = appItem["Personal Physician"].Equals("1");
				if (trPhysicianChoice.Visible)
					lblPhysicianChoice.Text = application.PhysicianChoice;

				trDentistChoice.Visible = appItem["Dentist Choice"].Equals("1");
				if (trDentistChoice.Visible)
					lblDentistChoice.Text = application.DentistChoice;

				lblMedicareInsuranceName.Text = application.MedicareName;
				lblMedicareNumber.Text = application.MedicareNumber;
				lblMedicareSex.Text = application.MedicareSex;
				lblEntitledToHospitalBenefit.Text = CareMoreUtilities.YesNo(application.EntitledToHospitalBenefits);
				lblHospitalBenefitDate.Text = CareMoreUtilities.DateFormat(application.HospitalBenefitsDate);
				lblEntitledToMedicalBenefit.Text = CareMoreUtilities.YesNo(application.EntitledToMedicalBenefits);
				lblMedicalBenefitDate.Text = CareMoreUtilities.DateFormat(application.MedicalBenefitsDate);

				trBenefitsVerified.Visible = appItem["Benefits Verified"].Equals("1");
				if (trBenefitsVerified.Visible)
					lblBenefitsVerifiedBy.Text = application.BenefitsVerifiedBy;

				trResidenceCounty.Visible = appItem["Permanent Residence County"].Equals("1");
				if (trResidenceCounty.Visible)
					lblResidenceCounty.Text = application.ResidenceCounty;

				trChronicConditions.Visible = appItem["Chronic Conditions"].Equals("1");
				if (trChronicConditions.Visible)
					lblChonicConditions.Text = application.ChronicConditions;

				lblPaymentOption.Text = application.PaymentOption;

				if (application.PaymentOption.Contains("(EFT)"))
				{
					lblAccountHolderNameEft.Text = application.AccountHolderName;
					lblAccountType.Text = application.AccountType;
					lblRoutingNumber.Text = application.RoutingNumber;
					lblAccountNumberEft.Text = application.AccountNumber;
					phCreditCard.Visible = false;
				}
				else if (application.PaymentOption.Equals("Credit Card"))
				{
					phEft.Visible = false;
					lblCreditCardType.Text = application.CreditCardType;
					lblAccountHolderNameCC.Text = application.AccountHolderName;
					lblAccountNumberCC.Text = application.AccountNumber;
					lblCreditCardExpirationDate.Text = application.CreditCardExpireDate;
				}
				else
				{
					phCreditCard.Visible = false;
					phEft.Visible = false;
				}

				phChronicSNPRows.Visible = appItem["Chronic SNP"].Equals("1");
				if (phChronicSNPRows.Visible)
				{
					lblChronicESRD.Text = CareMoreUtilities.YesNo(application.ChronicESRD);
					lblCurrentDialysisCenter.Text = application.CurrentDialysisCenter;
					lblCurrentDialysisPhone.Text = application.CurrentDialysisPhone;
					lblNephrologistFirstName.Text = application.NephrologistFirstName;
					lblNephrologistLastName.Text = application.NephrologistLastName;
					lblNewDialysisCenter.Text = application.NewDialysisCenter;
					lblAsthma.Text = CareMoreUtilities.YesNo(application.ChronicAsthma);
					lblEmphysema.Text = CareMoreUtilities.YesNo(application.ChronicEmphysema);
					lblDiabetes.Text = CareMoreUtilities.YesNo(application.ChronicDiabetes);
					lblInsulin.Text = CareMoreUtilities.YesNo(application.ChronicInsulin);
					lblCoronaryArteryDisease.Text = CareMoreUtilities.YesNo(application.ChronicCoronaryArteryDisease);
					lblHeartAttack.Text = CareMoreUtilities.YesNo(application.ChronicHeartAttack);
					lblHeartFailure.Text = CareMoreUtilities.YesNo(application.ChronicHeartFailure);
					lblFluidSwelling.Text = CareMoreUtilities.YesNo(application.ChronicFluidSwelling);
					lblTreatingFirstName.Text = application.TreatingFirstName;
					lblTreatingLastName.Text = application.TreatingLastName;
					lblTreatingSpecialty.Text = application.TreatingSpecialty;
					lblTreatingPhone.Text = application.TreatingPhone;
					lblEnrolleeInitials.Text = application.ChronicEnrolleeInitials;
					lblEnrolleeDate.Text = CareMoreUtilities.DateFormat(application.ChronicEnrolleeDate);
				}

				trMedAckAcknowledgment.Visible = appItem["Medications Acknowledgment"].Equals("1");
				if (trMedAckAcknowledgment.Visible)
					lblMedications.Text = CareMoreUtilities.YesNo(application.MedicationsAcknowledgment);

				phContinuityCareRows.Visible = appItem["Continuity Care Coordination"].Equals("1");
				if (phContinuityCareRows.Visible)
				{
					lblPlannedProcedures.Text = CareMoreUtilities.YesNo(application.PlannedProcedures);
					lblTreatmentDetails.Text = application.TreatmentDetails;
					lblPlannedSpecialistFirstName1.Text = application.PlannedSpecialistFirstName1;
					lblPlannedSpecialistLastName1.Text = application.PlannedSpecialistLastName1;
					lblPlannedSpecialistPhone1.Text = application.PlannedSpecialistPhone1;
					lblPlannedSpecialtyType1.Text = application.PlannedSpecialistType1;
					lblPlannedSpecialistFirstName2.Text = application.PlannedSpecialistFirstName2;
					lblPlannedSpecialistLastName2.Text = application.PlannedSpecialistLastName2;
					lblPlannedSpecialistPhone2.Text = application.PlannedSpecialistPhone2;
					lblPlannedSpecialtyType2.Text = application.PlannedSpecialistType2;
					lblPlannedSpecialistFirstName3.Text = application.PlannedSpecialistFirstName3;
					lblPlannedSpecialistLastName3.Text = application.PlannedSpecialistLastName3;
					lblPlannedSpecialistPhone3.Text = application.PlannedSpecialistPhone3;
					lblPlannedSpecialtyType3.Text = application.PlannedSpecialistType3;
					lblSpecialistPartOfVA.Text = CareMoreUtilities.YesNo(application.SpecialistPartOfVA);

					lblUsingOxygen.Text = CareMoreUtilities.YesNo(application.UsingOxygen);
					lblDiagnosedCopd.Text = CareMoreUtilities.YesNo(application.DiagnosedCopd);

					lblHomeHealthServices.Text = CareMoreUtilities.YesNo(application.HomeHealthServices);
					lblHomeHealthProviderName.Text = application.HomeHealthProviderName;
					lblHomeHealthProviderPhone.Text = application.HomeHealthProviderPhone;
					lblHomeHealthServicesUsed.Text = application.HomeHealthServicesUsed;
					lblUpcomingHomeHealthVisits.Text = application.UpcomingHomeHealthVisits;

					if (string.IsNullOrEmpty(application.RentedItemsOther))
						lblRentedItems.Text = application.RentedItems;
					else
						lblRentedItems.Text = string.Format("{0}, {1}", application.RentedItems, application.RentedItemsOther);

					lblDmeProviderName.Text = application.DmeProviderName;
					lblDmeProviderPhone.Text = application.DmeProviderPhone;
				}

				lblPrintFormat.Text = application.PrintFormat;

				trESRD.Visible = appItem["ESRD"].Equals("1");
				if (trESRD.Visible)
					lblESRD.Text = CareMoreUtilities.YesNo(application.ESRD);

				lblOtherCoverage.Text = CareMoreUtilities.YesNo(application.OtherCoverage);
				lblOtherCoverageName.Text = application.OtherCoverageName;
				lblOtherCoverageId.Text = application.OtherCoverageId;

				trOtherCoverageGroupNumber.Visible = !appItem["Coverage End Date"].Equals("1");
				if (trOtherCoverageGroupNumber.Visible)
					lblOtherCoverageGroupNumber.Text = application.OtherCoverageGroupNumber;

				trOtherCoverageEndDate.Visible = appItem["Coverage End Date"].Equals("1");
				if (trOtherCoverageEndDate.Visible)
					lblOtherCoverageDate.Text = CareMoreUtilities.DateFormat(application.OtherCoverageDate);

				lblCareResident.Text = CareMoreUtilities.YesNo(application.CareResident);
				lblCareResidentInfo.Text = application.CareResidentInfo;

				lblStateMedicaid.Text = CareMoreUtilities.YesNo(application.StateMedicaid);
				lblStateMedicaidNumber.Text = application.StateMedicaidNumber;

				phCurrentMember.Visible = appItem["Current Member"].Equals("1");
				if (phCurrentMember.Visible)
				{
					lblCurrentMember.Text = CareMoreUtilities.YesNo(application.CurrentMember);
					lblCurrentMemberPlan.Text = application.CurrentMemberPlan;
				}

				lblSpouseText.Text = appItem["Spouse Label"];
				lblSpouseWork.Text = CareMoreUtilities.YesNo(application.SpouseWork);

				trSpouseGroupCoveragePlan.Visible = appItem["Spouse Group Coverage"].Equals("1");
				if (trSpouseGroupCoveragePlan.Visible)
					lblSpouseGroupCoveragePlan.Text = application.SpouseGroupCoveragePlan;

				trLiveInLongTermCareFacility.Visible = appItem["Live in Long Term Care Facility"].Equals("1");
				if (trLiveInLongTermCareFacility.Visible)
					lblLiveLongTerm.Text = CareMoreUtilities.YesNo(application.LiveinLongTermCareFacility);

				trChronicLungDisorder.Visible = appItem["Chronic Lung Disorder"].Equals("1");
				if (trChronicLungDisorder.Visible)
					lblHasChronicLungDisorder.Text = CareMoreUtilities.YesNo(application.ChronicLung);

				trDiabetes.Visible = appItem["Diabetes"].Equals("1");
				if (trDiabetes.Visible)
					lblHasDiabetes.Text = CareMoreUtilities.YesNo(application.Diabetes);

				trChronicHeartFailure.Visible = appItem["Chronic Heart Failure"].Equals("1");
				if (trChronicHeartFailure.Visible)
					lblHasCardiovascularDisorder.Text = CareMoreUtilities.YesNo(application.ChronicHeart);

				lblPrimaryCarePhysicianName.Text = application.PrimaryPhysicianName;

				trReceivedSummary.Visible = appItem["Given Summary of Benefits"].Equals("1");
				if (trReceivedSummary.Visible)
					lblReceivedSummary.Text = CareMoreUtilities.YesNo(application.ReceivedSummary);

				lblSignature.Text = application.Signature;
				lblReleaseInitials.Text = application.ReleaseInitials;
				lblReleaseDate.Text = CareMoreUtilities.DateFormat(application.ReleaseDate);

				lblAuthorizedRep.Text = CareMoreUtilities.YesNo(application.IsAuthorizedRep);
				lblRepName.Text = application.RepName;
				lblRepAddress.Text = application.RepAddress;
				lblRepPhone.Text = application.RepPhone;
				lblRepRelation.Text = application.RepRelation;

				trSalesAgent.Visible = appItem["Sales Agent"].Equals("1");
				if (trSalesAgent.Visible)
				{
					lblSalesAgent.Text = CareMoreUtilities.YesNo(application.IsSalesAgent);
					lblSalesAgentCode.Text = application.SalesAgentCode;
					lblSalesAgentFirstName.Text = application.SalesAgentFirstName;
					lblSalesAgentLastName.Text = application.SalesAgentLastName;
				}
			}
		}

		private void SaveChanges()
		{
			CaremoreApplicationDataContext dbContext = new CaremoreApplicationDataContext();
			MemberApplication application = GetApplication(dbContext);

			if (application == null)
			{
				SetNotFoundMessage();
			}
			else
			{
				application.Status = !application.Status;
				application.UpdatedBy = Session["AdminUsername"].ToString();
				application.UpdatedOn = DateTime.Now;

				dbContext.SubmitChanges();

				btnSubmit.Text = string.Format("Mark as {0}", application.Status ? "Unprocessed" : "Processed");
			}
		}

		private void SetNotFoundMessage()
		{
			string id = Request.QueryString["Id"];
			if (string.IsNullOrEmpty(id))
			{
				CareMoreUtilities.DisplayError(phMessage, "An application ID was not provided.");
			}
			else
			{
				CareMoreUtilities.DisplayError(phMessage,
					string.Format("An application with the ID of <strong>{0}</strong> was not found.", id));
			}
		}
	}
}
