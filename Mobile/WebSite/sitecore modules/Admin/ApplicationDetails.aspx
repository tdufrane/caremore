﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApplicationDetails.aspx.cs" Inherits="Website.sitecore_modules.Admin.ApplicationDetails"
	MasterPageFile="~/sitecore modules/Site.Master" Title="CareMore > Admin > Application Details" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentHeader" runat="server" ContentPlaceHolderID="contentPlaceHolderHeader">
	<div><a href="ApplicationList.aspx">Back to List</a> | <a href="Login.aspx">Log Off</a></div>
</asp:Content>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<div>
		<h1 class="heading">Application Details</h1>

		<table cellpadding="0" cellspacing="0" border="0" class="appDetails">
			<tr>
				<td class="label">Confirmation Number</td>
				<td><asp:Label ID="lblConfirmationNumber" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">County Selected</td>
				<td><asp:Label ID="lblLocation" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Plan Selected</td>
				<td><asp:Label ID="lblPlan" runat="server" /></td>
			</tr>
			<tr id="trIpaMedicalGroup" runat="server">
				<td class="label">IPA Medical Group</td>
				<td><asp:Label ID="lblIPAGroupNumber" runat="server" /></td>
			</tr>
			<tr id="trCoverageEffectiveDate" runat="server">
				<td class="label">Coverage Effective Date</td>
				<td><asp:Label ID="lblCoverageEffectiveDate" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Last Name</td>
				<td><asp:Label ID="lblLastName" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">First Name</td>
				<td><asp:Label ID="lblFirstName" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">(M.I.)</td>
				<td><asp:Label ID="lblMI" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Salutation</td>
				<td><asp:Label ID="lblSalutation" runat="server" /></td>
			</tr>
			<tr id="trSocialSecurity" runat="server">
				<td class="label">Social Security #</td>
				<td><asp:Label ID="lblSSN" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Birth Date</td>
				<td><asp:Label ID="lblBirthDate" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Sex</td>
				<td><asp:Label ID="lblSex" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Home Phone</td>
				<td><asp:Label ID="lblHomePhone" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Alternative Phone</td>
				<td><asp:Label ID="lblAlternativePhone" runat="server" /></td>
			</tr>
			<tr id="trIsCellPhone" runat="server">
				<td class="label">Is Cell Phone</td>
				<td><asp:Label ID="lblIsCellPhone" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Permanent Residence Address</td>
				<td><asp:Label ID="lblAddress" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">City</td>
				<td><asp:Label ID="lblCity" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">State</td>
				<td><asp:Label ID="lblState" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Zip Code</td>
				<td><asp:Label ID="lblZip" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Mailing address (if different from above)</td>
				<td><asp:Label ID="lblAddress2" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">City</td>
				<td><asp:Label ID="lblCity2" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">State</td>
				<td><asp:Label ID="lblState2" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Zip Code</td>
				<td><asp:Label ID="lblZip2" runat="server" /></td>
			</tr>
<asp:PlaceHolder ID="phMovedRows" runat="server">
			<tr>
				<td class="label">Moved During Calendar Year</td>
				<td><asp:Label ID="lblMoved" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Date of Moved</td>
				<td><asp:Label ID="lblDateOfMove" runat="server" /></td>
			</tr>
</asp:PlaceHolder>
			<tr>
				<td class="label">Emergency Contact</td>
				<td><asp:Label ID="lblEmergencyContact" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Emergency Phone Number</td>
				<td><asp:Label ID="lblEmergencyPhoneNumber" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Relationship to you</td>
				<td><asp:Label ID="lblEmergencyContactRelationshipToYou" runat="server" /></td>
			</tr>
			<tr id="trEmailAddress" runat="server">
				<td class="label">Email Address</td>
				<td><asp:Label ID="lblEmergencyContactEmailAddress" runat="server" /></td>
			</tr>
<asp:PlaceHolder ID="phConsentToContactRows" runat="server">
			<tr>
				<td class="label">Agrees to receive ANOC, EOC, and Formulary by Email</td>
				<td><asp:Label ID="lblReceiveDocsByEmail" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Would like to Receive Information Regarding Plan by Email</td>
				<td><asp:Label ID="lblReceiveInfoByEmail" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Email Address</td>
				<td><asp:Label ID="lblReceiveEmailAddress" runat="server" /></td>
			</tr>
</asp:PlaceHolder>
			<tr id="trPhysicianChoice" runat="server">
				<td class="label">Personal Physician Choice / Name and I.D. Number</td>
				<td><asp:Label ID="lblPhysicianChoice" runat="server" /></td>
			</tr>
			<tr id="trDentistChoice" runat="server">
				<td class="label">Dentist Choice / Name and I.D. Number</td>
				<td><asp:Label ID="lblDentistChoice" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Medicare Insurance Name</td>
				<td><asp:Label ID="lblMedicareInsuranceName" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Medicare Number</td>
				<td><asp:Label ID="lblMedicareNumber" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Medicare Sex</td>
				<td><asp:Label ID="lblMedicareSex" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Entitled to Hospital Insurance Benefits (Part A):</td>
				<td>
					<asp:Label ID="lblEntitledToHospitalBenefit" runat="server" />
					<br />
					Date:
					<asp:Label ID="lblHospitalBenefitDate" runat="server" />
				</td>
			</tr>
			<tr>
				<td class="label">Entitled to Medical Insurance Benefits (Part B):</td>
				<td>
					<asp:Label ID="lblEntitledToMedicalBenefit" runat="server" />
					<br />
					Date:
					<asp:Label ID="lblMedicalBenefitDate" runat="server" />
				</td>
			</tr>
			<tr id="trBenefitsVerified" runat="server">
				<td class="label">Your Benefits have been verified by:</td>
				<td><asp:Label ID="lblBenefitsVerifiedBy" runat="server" /></td>
			</tr>
			<tr id="trResidenceCounty" runat="server">
				<td class="label">Permanent Residence County:</td>
				<td><asp:Label ID="lblResidenceCounty" runat="server" /></td>
			</tr>
			<tr id="trChronicConditions" runat="server">
				<td class="label">Any applicable chronic condition(s)</td>
				<td><asp:Label ID="lblChonicConditions" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Premium payment option:</td>
				<td>
					<asp:Label ID="lblPaymentOption" runat="server" />
					<br />
					<asp:PlaceHolder ID="phEft" runat="server">
						<br />
						Bank details for Electronic funds transfer (EFT)<br />
						Account holder name:
						<asp:Label ID="lblAccountHolderNameEft" runat="server" />
						<br />
						Account type:
						<asp:Label ID="lblAccountType" runat="server" />
						<br />
						Routing number:
						<asp:Label ID="lblRoutingNumber" runat="server" />
						<br />
						Account number:
						<asp:Label ID="lblAccountNumberEft" runat="server" />
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="phCreditCard" runat="server">
						<br />
						Credit Card Type:
						<asp:Label ID="lblCreditCardType" runat="server" />
						<br />
						Account holder name:
						<asp:Label ID="lblAccountHolderNameCC" runat="server" />
						<br />
						Account number:
						<asp:Label ID="lblAccountNumberCC" runat="server" />
						<br />
						Credit Card Expiration Date:
						<asp:Label ID="lblCreditCardExpirationDate" runat="server" />
					</asp:PlaceHolder>
				</td>
			</tr>
<asp:PlaceHolder ID="phChronicSNPRows" runat="server">
			<tr>
				<td class="label">On dialysis (ESRD)</td>
				<td><asp:Label ID="lblChronicESRD" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Current Dialysis Center</td>
				<td><asp:Label ID="lblCurrentDialysisCenter" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Current Dialysis Phone</td>
				<td><asp:Label ID="lblCurrentDialysisPhone" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">New Nephrologist First Name</td>
				<td><asp:Label ID="lblNephrologistFirstName" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">New Nephrologist Last Name</td>
				<td><asp:Label ID="lblNephrologistLastName" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">New Dialysis Center</td>
				<td><asp:Label ID="lblNewDialysisCenter" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Diagnosed with Asthma</td>
				<td><asp:Label ID="lblAsthma" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Diagnosed with Emphysema</td>
				<td><asp:Label ID="lblEmphysema" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Told by a Doctor Has Diabetes</td>
				<td><asp:Label ID="lblDiabetes" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Prescribed or Taking Insulin or an Oral Medication</td>
				<td><asp:Label ID="lblInsulin" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Told by a Doctor Has Coronary Artery Disease</td>
				<td><asp:Label ID="lblCoronaryArteryDisease" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Ever had a Heart Attack</td>
				<td><asp:Label ID="lblHeartAttack" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Told by a Doctor Has Heart Failure</td>
				<td><asp:Label ID="lblHeartFailure" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Problems with Fluid in Lungs and Swelling in Legs</td>
				<td><asp:Label ID="lblFluidSwelling" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Treating Specialist First Name</td>
				<td><asp:Label ID="lblTreatingFirstName" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Treating Specialist Last Name</td>
				<td><asp:Label ID="lblTreatingLastName" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Treating Specialist Specialty</td>
				<td><asp:Label ID="lblTreatingSpecialty" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Treating Specialist Phone</td>
				<td><asp:Label ID="lblTreatingPhone" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Enrollee Initials</td>
				<td><asp:Label ID="lblEnrolleeInitials" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Enrollee Date</td>
				<td><asp:Label ID="lblEnrolleeDate" runat="server" /></td>
			</tr>
</asp:PlaceHolder>
			<tr id="trMedAckAcknowledgment" runat="server">
				<td class="label">Acknowledged Responsible to Contact Existing Provider to Obtain Any Refills</td>
				<td><asp:Label ID="lblMedications" runat="server" /></td>
			</tr>
<asp:PlaceHolder ID="phContinuityCareRows" runat="server">
			<tr>
				<td class="label">Has Any Procedures or Surgeries Planned</td>
				<td><asp:Label ID="lblPlannedProcedures" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Treatment details</td>
				<td><asp:Label ID="lblTreatmentDetails" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Specialist(s) performing the procedure(s) or surgeries</td>
				<td>
					<table class="subDetails">
						<tr>
							<td>Name</td>
							<td>Phone</td>
							<td>Specialty</td>
						</tr>
						<tr>
							<td>
								<asp:Label ID="lblPlannedSpecialistFirstName1" runat="server" />&nbsp;
								<asp:Label ID="lblPlannedSpecialistLastName1" runat="server" /></td>
							<td><asp:Label ID="lblPlannedSpecialistPhone1" runat="server" /></td>
							<td><asp:Label ID="lblPlannedSpecialtyType1" runat="server" /></td>
						</tr>
						<tr>
							<td><asp:Label ID="lblPlannedSpecialistFirstName2" runat="server" />&nbsp;
								<asp:Label ID="lblPlannedSpecialistLastName2" runat="server" /></td>
							<td><asp:Label ID="lblPlannedSpecialistPhone2" runat="server" /></td>
							<td><asp:Label ID="lblPlannedSpecialtyType2" runat="server" /></td>
						</tr>
						<tr>
							<td><asp:Label ID="lblPlannedSpecialistFirstName3" runat="server" />&nbsp;
								<asp:Label ID="lblPlannedSpecialistLastName3" runat="server" /></td>
							<td><asp:Label ID="lblPlannedSpecialistPhone3" runat="server" /></td>
							<td><asp:Label ID="lblPlannedSpecialtyType3" runat="server" /></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="label">Specialist(s) Part of the Veterans Administration (VA)</td>
				<td><asp:Label ID="lblSpecialistPartOfVA" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Using oxygen daily to help breathe</td>
				<td><asp:Label ID="lblUsingOxygen" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Been diagnosed with Chronic Obstructive Pulmonary Disease</td>
				<td><asp:Label ID="lblDiagnosedCopd" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Currently receiving services from a Home Health Agency</td>
				<td><asp:Label ID="lblHomeHealthServices" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Home Health Provider</td>
				<td><asp:Label ID="lblHomeHealthProviderName" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Home Health Provider Phone</td>
				<td><asp:Label ID="lblHomeHealthProviderPhone" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Home Health Services Provided</td>
				<td><asp:Label ID="lblHomeHealthServicesUsed" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Dates of Upcoming Home Health Visits</td>
				<td><asp:Label ID="lblUpcomingHomeHealthVisits" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Uses the following Durable Medical Equipment</td>
				<td><asp:Label ID="lblRentedItems" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">DME Provider</td>
				<td><asp:Label ID="lblDmeProviderName" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">DME Provider Phone</td>
				<td><asp:Label ID="lblDmeProviderPhone" runat="server" /></td>
			</tr>
</asp:PlaceHolder>
			<tr id="trESRD" runat="server">
				<td class="label">Do you have End Stage Renal Disease(ESRD)?</td>
				<td><asp:Label ID="lblESRD" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Has other prescription drug coverage in addition to CareMore Health Plan?</td>
				<td>
					<asp:Label ID="lblOtherCoverage" runat="server" />
					<br />
					<table class="subDetails">
						<tr>
							<td>Name of Other Coverage:</td>
							<td><asp:Label ID="lblOtherCoverageName" runat="server" /></td>
						</tr>
						<tr>
							<td>ID# for this Coverage:</td>
							<td><asp:Label ID="lblOtherCoverageId" runat="server" /></td>
						</tr>
						<tr id="trOtherCoverageGroupNumber" runat="server">
							<td>Group# for this Coverage:</td>
							<td><asp:Label ID="lblOtherCoverageGroupNumber" runat="server" /></td>
						</tr>
						<tr id="trOtherCoverageEndDate" runat="server">
							<td>Coverage End Date:</td>
							<td><asp:Label ID="lblOtherCoverageDate" runat="server" /></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="label">Are you a resident in a long-term care facility, such as a nursing home?</td>
				<td>
					<asp:Label ID="lblCareResident" runat="server" />
					<br /><br />
					Name of Institution: Address & Phone Number of Institution (number and street)
					<br />
					<asp:Label ID="lblCareResidentInfo" runat="server" />
				</td>
			</tr>
			<tr>
				<td class="label">Are you enrolled in your State Medicaid program (Medi-Cal)?</td>
				<td>
					<asp:Label ID="lblStateMedicaid" runat="server" />
					<br /><br />
					State medicaid number:
					<asp:Label ID="lblStateMedicaidNumber" runat="server" />
				</td>
			</tr>
<asp:PlaceHolder ID="phCurrentMember" runat="server">
			<tr>
				<td class="label">Current CareMore Member</td>
				<td><asp:Label ID="lblCurrentMember" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Name of current medical insurance plan</td>
				<td><asp:Label ID="lblCurrentMemberPlan" runat="server" /></td>
			</tr>
</asp:PlaceHolder>
			<tr>
				<td class="label"><asp:Label id="lblSpouseText" runat="server" /></td>
				<td><asp:Label ID="lblSpouseWork" runat="server" /></td>
			</tr>
			<tr id="trSpouseGroupCoveragePlan" runat="server">
				<td class="label">Name of group coverage plan</td>
				<td><asp:Label ID="lblSpouseGroupCoveragePlan" runat="server" /></td>
			</tr>
			<tr id="trLiveInLongTermCareFacility" runat="server">
				<td class="label">Lives in a long term care facility</td>
				<td><asp:Label ID="lblLiveLongTerm" runat="server" /></td>
			</tr>
			<tr id="trChronicLungDisorder" runat="server">
				<td class="label">Has Chronic Lung Disorder</td>
				<td><asp:Label ID="lblHasChronicLungDisorder" runat="server" /></td>
			</tr>
			<tr id="trDiabetes" runat="server">
				<td class="label">Has Diabetes</td>
				<td><asp:Label ID="lblHasDiabetes" runat="server" /></td>
			</tr>
			<tr id="trChronicHeartFailure" runat="server">
				<td class="label">Has Cardiovascular Disorder or Chronic Heart Failure?</td>
				<td><asp:Label ID="lblHasCardiovascularDisorder" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Primary Care Physician Name</td>
				<td><asp:Label ID="lblPrimaryCarePhysicianName" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Print format</td>
				<td><asp:Label ID="lblPrintFormat" runat="server" /></td>
			</tr>
			<tr id="trReceivedSummary" runat="server">
				<td class="label">Was Given the CareMore Health Plan Summary of Benefits</td>
				<td><asp:Label ID="lblReceivedSummary" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Release of Information</td>
				<td>
					<table class="subDetails">
						<tr>
							<td>Enrollee Initials:</td>
							<td><asp:Label ID="lblReleaseInitials" runat="server" /></td>
						</tr>
						<tr>
							<td>Date:</td>
							<td><asp:Label ID="lblReleaseDate" runat="server" /></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="label">Signature</td>
				<td><asp:Label ID="lblSignature" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Are you the authorized representative?</td>
				<td>
					<asp:Label ID="lblAuthorizedRep" runat="server" />
					<br />
					<table class="subDetails">
						<tr>
							<td>Name:</td>
							<td><asp:Label ID="lblRepName" runat="server" /></td>
						</tr>
						<tr>
							<td>Address:</td>
							<td><asp:Label ID="lblRepAddress" runat="server" /></td>
						</tr>
						<tr>
							<td>Phone Number:</td>
							<td><asp:Label ID="lblRepPhone" runat="server" /></td>
						</tr>
						<tr>
							<td>Relationship to Enrollee:</td>
							<td><asp:Label ID="lblRepRelation" runat="server" /></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr id="trSalesAgent" runat="server">
				<td class="label">Sales agent completed this form</td>
				<td>
					<asp:Label ID="lblSalesAgent" runat="server" />
					<br />
					<table class="subDetails">
						<tr>
							<td>Agent Code:</td>
							<td><asp:Label ID="lblSalesAgentCode" runat="server" /></td>
						</tr>
						<tr>
							<td>Agent/Brokers First Name:</td>
							<td><asp:Label ID="lblSalesAgentFirstName" runat="server" /></td>
						</tr>
						<tr>
							<td>Agent/Brokers Last Name:</td>
							<td><asp:Label ID="lblSalesAgentLastName" runat="server" /></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
			
		<div style="text-align:center;">
			<p><br /><asp:Button ID="btnSubmit" runat="server" OnClick="BtnSubmit_Click" CssClass="orangeSubmitButton" /></p>

			<p><a href="ApplicationList.aspx">Back to List</a></p>
		</div>

		<asp:PlaceHolder ID="phMessage" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</asp:Content>
