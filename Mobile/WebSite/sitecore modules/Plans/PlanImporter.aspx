﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sitecore modules/Site.Master" AutoEventWireup="true" CodeBehind="PlanImporter.aspx.cs" Inherits="Website.sitecore_modules.Plans.PlanImporter" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="contentPlaceHolderHeader" runat="server">
	<script type="text/javascript" src="../../js/jquery.min.js"></script>
</asp:Content>

<asp:Content ID="contentContent" ContentPlaceHolderID="contentPlaceHolderRoot" runat="server">
	<h2>Mass Import Plans</h2>

	<asp:PlaceHolder ID="phForm" runat="server">
		<asp:ValidationSummary id="valSumPage" runat="server" />

		<table border="0" cellpadding="0" cellspacing="0" class="appForm">
			<tr>
				<td class="label">Excel File:</td>
				<td>
					<asp:FileUpload ID="txtExcelFile" runat="server" Width="400" />
					<asp:RequiredFieldValidator ID="reqFldValExcelFile" runat="server"
						ControlToValidate="txtExcelFile" ErrorMessage="Excel File is required."
						Display="Dynamic" Text="*" />
					<asp:RegularExpressionValidator ID="regExpValExcelFile" runat="server"
						ControlToValidate="txtExcelFile" ErrorMessage="Only an Excel 97-2003 File (.xls) can be used."
						Display="Dynamic" Text="*"
						ValidationExpression="^.+(\.xls)$" />
				</td>
			</tr>
			<tr class="alt">
				<td class="label">Active Sheet Name:</td>
				<td>
					<asp:TextBox ID="txtActiveSheet" runat="server" Text="Sheet1" />
					<asp:RequiredFieldValidator ID="reqFldValActiveSheet" runat="server"
						ControlToValidate="txtActiveSheet" ErrorMessage="Active Sheet Name is required."
						Display="Dynamic" Text="*" />
				</td>
			</tr>
			<tr>
				<td class="label">Physical Document Folder:</td>
				<td>
					<asp:TextBox ID="txtDocFolder" runat="server" ClientIDMode="Static" MaxLength="4" Text="C:\Documents\CareMore\AEP 2014" />
					<asp:RequiredFieldValidator ID="reqFldValDocFolder" runat="server"
						ControlToValidate="txtDocFolder" ErrorMessage="Physical Document Folder is required."
						Display="Dynamic" Text="*" />
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<asp:LinkButton ID="btnSubmit" runat="server" OnClick="BtnSubmit_Click" CssClass="button btnSmall"><span>Import</span></asp:LinkButton>
				</td>
			</tr>
		</table>
	</asp:PlaceHolder>

	<asp:PlaceHolder ID="phStatus" runat="server" Visible="false" />
</asp:Content>
