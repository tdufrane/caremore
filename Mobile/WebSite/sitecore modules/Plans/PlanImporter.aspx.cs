﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Sitecore;
using Sitecore.Collections;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;
using Sitecore.Jobs;
using Sitecore.Security.Accounts;
using Sitecore.Security.AccessControl;


namespace Website.sitecore_modules.Plans
{
	public partial class PlanImporter : System.Web.UI.Page
	{
		private Database currentDB = Factory.GetDatabase("master");

		protected void Page_Load(object sender, EventArgs e)
		{
//			if (!Sitecore.Context.IsLoggedIn)
//			{
//				phForm.Visible = false;
//				CareMoreUtilities.DisplayError(phStatus, "You are not authorized to access this page as you need to logon to Sitecore first.");
//			}
//			else if (!Sitecore.Context.User.IsAdministrator)
//			{
//				phForm.Visible = false;
//				CareMoreUtilities.DisplayError(phStatus, "You are not authorized to access this page as you are not assigned as an 'Administrator'.");
//			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				ClearStatus();
				string fileName = UploadFile();

				ImportPlansJob plansJob = new ImportPlansJob();
				//plansJob.StartJob(fileName,  txtActiveSheet.Text);
				//Response.Redirect("ImportPlansStatus.aspx?id=" + providersJob.Job.Handle.ToString());

				// Comment out above and uncomment below to DEBUG
				plansJob.ImportData(fileName, txtActiveSheet.Text, txtDocFolder.Text);

				if (!string.IsNullOrWhiteSpace(plansJob.JobMessages))
					CareMoreUtilities.DisplayStatus(phStatus, plansJob.JobMessages);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phStatus, ex);
			}
		}

		private void ClearStatus()
		{
			phStatus.Controls.Clear();
			phStatus.Visible = false;
		}

		private string UploadFile()
		{
			string fileName = HttpContext.Current.Server.MapPath("~/userFiles") + "\\" + txtExcelFile.FileName;
			txtExcelFile.PostedFile.SaveAs(fileName);
			return fileName;
		}
	}
}
