﻿using System;
using System.Data;
using System.Data.OleDb;

namespace Website.sitecore_modules.Plans
{
	public class PlanItem
	{
		#region Constructors

		public PlanItem()
		{
		}

		public PlanItem(OleDbDataReader dr)
		{
			try
			{
				FromReader(dr);
			}
			catch (Exception ex)
			{
				ReaderError = ex.ToString();
			}
		}

		#endregion

		#region Properties

		public string Year { get; set; }
		public string State { get; set; }
		public string County { get; set; }
		public string Plan { get; set; }
		public string PlanId { get; set; }
		public string Contract { get; set; }
		public string SummaryBenefitsEnglish { get; set; }
		public string SummaryBenefitsSpanish { get; set; }
		public string EvidenceCoverageEnglish { get; set; }
		public string EvidenceCoverageSpanish { get; set; }
		public string DrugEnglish { get; set; }
		public string DrugSpanish { get; set; }
		public string PharmDirEnglish { get; set; }
		public string PharmDirSpanish { get; set; }
		public string ApplicationEnglish { get; set; }
		public string ApplicationSpanish { get; set; }
		public string PlanChartEnglish { get; set; }
		public string PlanChartSpanish { get; set; }
		public string AnnualNoticeEnglish { get; set; }
		public string AnnualNoticeSpanish { get; set; }
		public string HowToEnglish { get; set; }
		public string HowToSpanish { get; set; }
		public string Premium { get; set; }
		public string PaymentInfo { get; set; }

		public string ReaderError { get; private set; }

		#endregion

		#region Methods

		private bool FromReader(OleDbDataReader dr)
		{
			Year = CheckNull(dr["Year"]);
			State = CheckNull(dr["State"]);
			County = CheckNull(dr["County"]);
			Plan = CheckNull(dr["Plan"]);
			PlanId = CheckNull(dr["Plan ID"]);
			Contract = CheckNull(dr["Contract"]);
			SummaryBenefitsEnglish = CheckNull(dr["SB EN"]);
			SummaryBenefitsSpanish = CheckNull(dr["SB SP"]);
			EvidenceCoverageEnglish = CheckNull(dr["EOC EN"]);
			EvidenceCoverageSpanish = CheckNull(dr["EOC SP"]);
			DrugEnglish = CheckNull(dr["Drug EN"]);
			DrugSpanish = CheckNull(dr["Drug SP"]);
			PharmDirEnglish = CheckNull(dr["Pharm Dir EN"]);
			PharmDirSpanish = CheckNull(dr["Pharm Dir SP"]);
			ApplicationEnglish = CheckNull(dr["Application EN"]);
			ApplicationSpanish = CheckNull(dr["Application SP"]);
			PlanChartEnglish = CheckNull(dr["Plan Chart EN"]);
			PlanChartSpanish = CheckNull(dr["Plan Chart SP"]);
			AnnualNoticeEnglish = CheckNull(dr["ANOC EN"]);
			AnnualNoticeSpanish = CheckNull(dr["ANOC SP"]);
			HowToEnglish = CheckNull(dr["How To EN"]);
			HowToSpanish = CheckNull(dr["How To SP"]);
			Premium = CheckNull(dr["Premium"]);
			PaymentInfo = CheckNull(dr["Payment Info"]);

			return true;
		}

		private string CheckNull(object field)
		{
			return CheckNull(field, null);
		}

		private string CheckNull(object field, string whenNullReturnThis)
		{
			if (field == null)
			{
				return whenNullReturnThis;
			}
			else if (DBNull.Value.Equals(field))
			{
				return whenNullReturnThis;
			}
			else
			{
				return field.ToString().Trim();
			}
		}

		#endregion
	}
}
