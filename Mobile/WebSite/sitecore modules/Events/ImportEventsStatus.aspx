﻿<%@ Language="C#" AutoEventWireup="true" CodeBehind="ImportEventsStatus.aspx.cs" Inherits="Website.sitecore_modules.Events.ImportEventsStatus"
	MasterPageFile="~/sitecore modules/Site.Master"  Title="CareMore > Events > Import Results" %>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<h2>Import Events Results</h2>

	<asp:ScriptManager ID="scriptMgrPage" runat="server"></asp:ScriptManager>
	<asp:UpdatePanel ID="updatePnlPage" runat="server">
		<ContentTemplate>
			<p id="pProcessing" runat="server" visible="false">Processing <img id="imgLoading" src="/images/ajax-loader.gif" alt="..." /></p>

			<p id="pStatus" runat="server" visible="false" />

			<p id="pImport" runat="server" visible="false"><a href="ImportEvents.aspx">Import another event file.</a></p>

			<asp:Timer ID="timerPage" runat="server" Interval="5000" OnTick="TimerPage_Tick" />

			<asp:HiddenField ID="hidHandle" runat="server" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
