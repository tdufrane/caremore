﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using CareMore.Web.DotCom;

using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;
using Sitecore.Jobs;
using Sitecore.Security.Accounts;
using Sitecore.Security.AccessControl;

namespace Website.sitecore_modules.Events
{
	public class ImportEventsJob
	{
		private const string EVENTS_DB_PATH = "/sitecore/content/Events DB";
		private const string DUALS_EVENTS_PATH = "/sitecore/content/Duals/Community";
		private const string DUALS_CONSUMER_EVENTS_PATH = DUALS_EVENTS_PATH + "/Consumer Events/Events";
		private const string DUALS_CONSUMER_STAKEHOLDER_EVENTS_PATH = DUALS_EVENTS_PATH + "/Consumer and Stakeholder Forums/Events";
		private const string DUALS_CONSUMER_EVENTTYPES_PATH = CareMoreUtilities.DUALS_GLOBAL_PATH + "/Event Types/Consumer Events";
		private const string DUALS_CONSUMER_STAKEHOLDER_EVENTTYPES_PATH = CareMoreUtilities.DUALS_GLOBAL_PATH + "/Event Types/Stakeholder Forums";

		public string JobName = "Import Events";
		Database currentDB = Factory.GetDatabase("master");

		public Job Job
		{
			get
			{
				return JobManager.GetJob(JobName);
			}
		}

		public void StartJob(string fileName, string sheetName, string eventGroup)
		{
			JobOptions options = new JobOptions(JobName,
			"Admin",
			Context.Site.Name,
			this,
			"ImportData",
			new object[] { fileName, sheetName, eventGroup });		

			JobManager.Start(options);
		}

		public void ImportData(string fileName, string sheetName, string itemType)
		{

			if (itemType == "Classic")
			{
				string strSql = string.Format("SELECT Date,Time,[Event Type],[Event Title],Location,Address,City,State,[Zip Code],County,Details,[Section],[Status] FROM [{0}$]", sheetName);
				GetData(strSql, fileName, itemType);
			}
			else if (itemType == "Duals")
			{
				string strSql = string.Format("SELECT [Event Date],[Event Start Time],[Event Type],[Event Name],[Location Name],Address,City,State,[Zip Code],[Event Description] FROM [{0}$]", sheetName);
				GetData(strSql, fileName, itemType);
			}
			
		}

		public void GetData(string strSql, string fileName, string itemType)
		{
			int count = 0, importedCount = 0;
			StringBuilder errors = new StringBuilder();
			DateTime started = DateTime.Now;

			try
			{
				Job.Status.State = JobState.Running;

				string connectionString = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=YES;""", fileName);
				OleDbConnection objConn = new OleDbConnection(connectionString);

				OleDbCommand objCmd = new OleDbCommand(strSql, objConn);

				objConn.Open();
				List<EventItem> classicList = new List<EventItem>();
				List<DualsEventItem> dualsList = new List<DualsEventItem>();
				
				using (OleDbDataReader dr = objCmd.ExecuteReader())
				{
					EventItem classicItem = null;
					DualsEventItem dualsItem = null;
					string errorMessage;

					while (dr.Read())
					{
						Job.AddMessage("Reading row {0}", count + 1);

						if (itemType == "Classic")
						{
							if (IsEmptyRow(dr)) break;
							classicItem = new EventItem(dr);
							errorMessage = classicItem.ReaderError;
						}
						else if (itemType == "Duals")
						{
							if (IsEmptyRowDuals(dr)) break;
							dualsItem = new DualsEventItem(dr);
							errorMessage = dualsItem.ReaderError;
						}
						else
						{
							throw new ArgumentNullException("Invalid item type!");
						}

						if (string.IsNullOrWhiteSpace(errorMessage))
						{
							if (itemType == "Classic")
							{
								classicList.Add(classicItem);
							}
							else if (itemType == "Duals")
							{
								dualsList.Add(dualsItem);
							}		
						}
						else
						{
							errors.AppendLine(errorMessage);
						}

						count++;
					}

					dr.Close();
				}

				objConn.Close();

				if (itemType == "Classic")
				{
					importedCount = CreateItems(classicList, errors);
				}
				else if (itemType == "Duals")
				{
					importedCount = CreateItems(dualsList, errors);
				}
				
			}
			catch (Exception ex)
			{
				Job.Status.State = JobState.Unknown;
				errors.AppendLine(ex.Message);
				Log.Error("ImportEventsJob.ImportData Exception", ex, this.GetType());
			}
			finally
			{
				Job.Status.State = JobState.Finished;

				StringBuilder message = new StringBuilder();
				message.AppendFormat("<p><strong>{0}</strong> events were read. <span style=\"color:Blue;\">{1}</span> events have been successfully imported.</p>",
					count, importedCount);

				if (errors.Length > 0)
				{
					errors.Length -= 1; // Remove trailing \n

					message.AppendFormat("<p><strong>Errors:</strong><br />{0}</p>", errors.ToString().Replace("\n", "<br />"));
				}

				TimeSpan processTime = (DateTime.Now - started);
				message.AppendFormat("<p>Processing time was {0} minutes, {1} seconds.</p>",
					processTime.Minutes, processTime.Seconds);

				Job.AddMessage(message.ToString());
			}
			
		}

		private int CreateItems(List<EventItem> list, StringBuilder errors)
		{
			int itemCount = 0, importedCount = 0;

			Item eventRootItem = currentDB.Items[EVENTS_DB_PATH];
			AccessRuleCollection accessRules = eventRootItem.Security.GetAccessRules();

			TemplateItem eventTemplateItem = currentDB.Templates[ItemIds.GetID("EVENT_TEMPLATE_ID")];
			Item[] eventTypes = currentDB.SelectItems(CareMoreUtilities.GLOBAL_PATH + "/Events/Types//*");

			foreach (EventItem item in list)
			{
				itemCount++;

				string eventName = string.Format("Event # {0} - {1:MM/dd/yy hh:mm tt} at location {2} in {3}, {4}",
						itemCount, item.Date, item.Location, item.City, item.State);

				try
				{
					Job.AddMessage(eventName);
					CreateEventItem(eventRootItem, eventTemplateItem, item, eventTypes, accessRules);
					importedCount++;
				}
				catch (Exception ex)
				{
					string eventError = string.Format("Error '{0}' loading {1}.\n",
						ex.Message, eventName);
					errors.Append(eventError);
					Log.Error(eventError, ex, this.GetType());
				}

				Job.Processed++;
			}

			return importedCount;
		}

		private int CreateItems(List<DualsEventItem> list, StringBuilder errors)
		{
			int itemCount = 0, importedCount = 0;

			Item consumerEventRootItem = currentDB.Items[DUALS_CONSUMER_EVENTS_PATH];
			Item stakeholderEventRootItem = currentDB.Items[DUALS_CONSUMER_STAKEHOLDER_EVENTS_PATH];
			AccessRuleCollection consumerAccessRules = consumerEventRootItem.Security.GetAccessRules();
			AccessRuleCollection stakeholderAccessRules = stakeholderEventRootItem.Security.GetAccessRules();

			TemplateItem consumerEventTemplateItem = currentDB.Templates[ItemIds.GetID("CONSUMER_EVENT_TEMPLATE_ID")];
			TemplateItem stakeholderEventTemplateItem = currentDB.Templates[ItemIds.GetID("STAKEHOLDER_EVENT_TEMPLATE_ID")];
			Item[] consumerEventTypes = currentDB.SelectItems(CareMoreUtilities.DUALS_GLOBAL_PATH + "/Event Types/Consumer Events//*");
			Item[] stakeholderEventTypes = currentDB.SelectItems(CareMoreUtilities.DUALS_GLOBAL_PATH + "/Event Types/Stakeholder Forums//*");

			foreach (DualsEventItem item in list)
			{
				itemCount++;

				string eventName = string.Format("Event # {0} - {1:MM/dd/yy hh:mm tt} at location {2} in {3}, {4}",
						itemCount, item.Date, item.Location, item.City, item.State);

				try
				{
					Job.AddMessage(eventName);

					if (item.EventGroup == "Consumer")
					{
						CreateEventItem(consumerEventRootItem, consumerEventTemplateItem, item, consumerEventTypes, consumerAccessRules);
					}
					else
					{
						CreateEventItem(stakeholderEventRootItem, stakeholderEventTemplateItem, item, stakeholderEventTypes, stakeholderAccessRules);
					}
					
					importedCount++;
				}
				catch (Exception ex)
				{
					string eventError = string.Format("Error '{0}' loading {1}.\n",
						ex.Message, eventName);
					errors.Append(eventError);
					Log.Error(eventError, ex, this.GetType());
				}

				Job.Processed++;
			}

			return importedCount;
		}

		private void CreateEventItem(Item Parent, TemplateItem ItemType, EventItem item, Item[] eventTypes, AccessRuleCollection accessRules)
		{
			string itemName = string.Format("{0} {1} {2} {3}", item.State, item.City, item.Location, item.Date.ToString("HHmm"));

			TemplateItem yearFolderItem = currentDB.Templates[ItemIds.GetID("YEAR_FOLDER_TEMPLATE_ID")];
			TemplateItem monthFolderItem = currentDB.Templates[ItemIds.GetID("MONTH_FOLDER_TEMPLATE_ID")];
			TemplateItem dayFolderItem = currentDB.Templates[ItemIds.GetID("DAY_FOLDER_TEMPLATE_ID")];
			Item yearItem = CareMoreUtilities.CreateItem(item.Date.ToString("yyyy"), Parent, yearFolderItem, true, accessRules);
			Item monthItem = CareMoreUtilities.CreateItem(item.Date.ToString("MM"), yearItem, monthFolderItem, true, accessRules);
			Item dayItem = CareMoreUtilities.CreateItem(item.Date.ToString("dd"), monthItem, dayFolderItem, true, accessRules);
			Item eventItem = CareMoreUtilities.CreateItem(itemName, dayItem, ItemType, true);

			using (new Sitecore.SecurityModel.SecurityDisabler())
			{
				eventItem.Editing.BeginEdit();

				try
				{
					eventItem.Fields["Place"].Value = item.Location;
					eventItem.Fields["Address Line 1"].Value = item.Address;
					eventItem.Fields["City"].Value = item.City;
					eventItem.Fields["Postal Code"].Value = item.ZipCode;
					eventItem.Fields["Title"].Value = item.Title;
					eventItem.Fields["County"].Value = item.County;
					eventItem.Fields["State"].Value = item.State;
					eventItem.Fields["EventContent"].Value = item.Content;
					eventItem.Fields["EventDate"].Value = item.Date.ToString("yyyyMMdd'T'HHmmss");
					eventItem.Fields["Status"].Value = item.Status;

					// Event Type can have multiple event types separated by commas
					foreach (string eventType in item.EventTypeNames)
					{
						Item eventTypeItem = GetEventType(eventTypes, eventType.Trim());

						if (eventTypeItem != null)
						{
							MultilistField multiListField = eventItem.Fields["EventType"];
							if (eventType != null)
							{
								if (!multiListField.Contains(eventTypeItem.ID.ToString()))
								{
									multiListField.Add(eventTypeItem.ID.ToString());
								}
							}
						}
					}
				}
				finally
				{
					eventItem.Editing.EndEdit();
				}
			}
		}

		private void CreateEventItem(Item Parent, TemplateItem ItemType, DualsEventItem item, Item[] eventTypes, AccessRuleCollection accessRules)
		{
			string itemName = string.Format("{0} {1} {2} {3}", item.State, item.City, item.Location, item.Date.ToString("MMddHHmm"));

			TemplateItem yearFolderItem = currentDB.Templates[ItemIds.GetID("DUALS_YEAR_FOLDER_TEMPLATE_ID")];
			TemplateItem monthFolderItem = currentDB.Templates[ItemIds.GetID("DUALS_MONTH_FOLDER_TEMPLATE_ID")];
			Item yearItem = CareMoreUtilities.CreateItem(item.Date.ToString("yyyy"), Parent, yearFolderItem, true, accessRules);
			Item monthItem = CareMoreUtilities.CreateItem(item.Date.ToString("MM"), yearItem, monthFolderItem, true, accessRules);
			Item eventItem = CareMoreUtilities.CreateItem(itemName, monthItem, ItemType, true);

			using (new Sitecore.SecurityModel.SecurityDisabler())
			{
				eventItem.Editing.BeginEdit();

				try
				{
					eventItem.Fields["Location Name"].Value = item.Location;
					eventItem.Fields["Location Address"].Value = item.Address + " " + item.City + ", " + item.State + " " + item.ZipCode;
					eventItem.Fields["Page Title"].Value = item.Title;
					eventItem.Fields["Name"].Value = item.Title;
					eventItem.Fields["Description"].Value = item.Content;
					eventItem.Fields["Date"].Value = item.Date.ToString("yyyyMMdd'T'HHmmss");

					// Event Type can have multiple event types separated by commas
					foreach (string eventType in item.EventTypeNames)
					{
						Item eventTypeItem = GetEventType(eventTypes, eventType.Trim(), item.EventGroup);

						if (eventTypeItem != null)
						{
							MultilistField multiListField = eventItem.Fields["Event Type"];
							if (eventType != null)
							{
								if (!multiListField.Contains(eventTypeItem.ID.ToString()))
								{
									multiListField.Add(eventTypeItem.ID.ToString());
								}
							}
						}
					}
				}
				finally
				{
					eventItem.Editing.EndEdit();
				}
			}
		}

		private Item GetEventType(Item[] eventTypes, string eType)
		{
			Item eventType = null;

			if (eventTypes.Any(x => x.Name.Equals(eType, StringComparison.OrdinalIgnoreCase)))
			{
				eventType = eventTypes.Single(x => x.Name.Equals(eType, StringComparison.OrdinalIgnoreCase));
			}
			else
			{
				Item targetEventTypeRoot = currentDB.Items[CareMoreUtilities.GLOBAL_PATH + "/Events/Types"];
				TemplateItem targetEventTypeTemplate = currentDB.Templates["System/Item"];
				eventType = CareMoreUtilities.CreateItem(eType, targetEventTypeRoot, targetEventTypeTemplate, true);
			}

			return eventType;
		}

		private Item GetEventType(Item[] eventTypes, string eType, string eventGroup)
		{
			Item eventType = null;

			if (eventTypes.Any(x => x.Name.Equals(eType, StringComparison.OrdinalIgnoreCase)))
			{
				eventType = eventTypes.Single(x => x.Name.Equals(eType, StringComparison.OrdinalIgnoreCase));
			}
			else
			{
				Item consumerEventTypeRoot = currentDB.Items[DUALS_CONSUMER_EVENTTYPES_PATH + "//*"];
				Item stakeholderEventTypeRoot = currentDB.Items[DUALS_CONSUMER_STAKEHOLDER_EVENTTYPES_PATH + "//*"];

				TemplateItem targetEventTypeTemplate = currentDB.Templates["System/Item"];

				if (eventGroup == "Consumer")
				{
					eventType = CareMoreUtilities.CreateItem(eType, consumerEventTypeRoot, targetEventTypeTemplate, true);
				}
				else
				{
					eventType = CareMoreUtilities.CreateItem(eType, stakeholderEventTypeRoot, targetEventTypeTemplate, true);
				}
			}

			return eventType;
		}

		private bool IsEmptyRow(OleDbDataReader dr)
		{
			if (dr["Date"].ToString().Length == 0 && dr["Time"].ToString().Length == 0 && dr["Event Type"].ToString().Length == 0
				&& dr["Location"].ToString().Length == 0 && dr["Address"].ToString().Length == 0 && dr["City"].ToString().Length == 0
				&& dr["Zip Code"].ToString().Length == 0 && dr["Status"].ToString().Length == 0)
				return true;
			else
				return false;
		}

		private bool IsEmptyRowDuals(OleDbDataReader dr)
		{
			if (dr["Event Date"].ToString().Length == 0 && dr["Event Start Time"].ToString().Length == 0 && dr["Event Type"].ToString().Length == 0
				&& dr["Location Name"].ToString().Length == 0 && dr["Address"].ToString().Length == 0 && dr["City"].ToString().Length == 0
				&& dr["Zip Code"].ToString().Length == 0 && dr["Event Name"].ToString().Length == 0)
				return true;
			else
				return false;
		}

		private EventItem getNewEventItem()
		{
			return new EventItem();
		}

	}
}
