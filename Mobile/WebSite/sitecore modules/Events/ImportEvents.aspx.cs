﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;
using Sitecore.Jobs;
using Sitecore.Security.Accounts;
using Sitecore.Security.AccessControl;
using Sitecore.Shell.Applications.Dialogs.ProgressBoxes;

namespace Website.sitecore_modules.Events
{
	public partial class ImportEvents : System.Web.UI.Page
	{
		Database currentDB = Factory.GetDatabase("master");

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Sitecore.Context.IsLoggedIn)
			{
				phForm.Visible = false;
				CareMoreUtilities.DisplayError(phStatus, "You are not authorized to access this page.");
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				ClearStatus();
				string fileName = UploadFile();
				string eventGroup = buttonClassic.Text;

				if (buttonDuals.Checked == true)
				{
					eventGroup = buttonDuals.Text;
				}

				ImportEventsJob eventsJob = new ImportEventsJob();
				eventsJob.StartJob(fileName, ActiveSheet.Text, eventGroup);

				Response.Redirect("ImportEventsStatus.aspx?id=" + eventsJob.Job.Handle.ToString(), false);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phStatus, ex);
			}
		}

		private void ClearStatus()
		{
			phStatus.Controls.Clear();
			phStatus.Visible = false;
		}

		private string UploadFile()
		{
			string fileName = HttpContext.Current.Server.MapPath("~/userFiles") + "\\" + ExcelFile.FileName;
			ExcelFile.PostedFile.SaveAs(fileName);
			return fileName;
		}
	}
}
