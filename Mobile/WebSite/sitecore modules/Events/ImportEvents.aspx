﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportEvents.aspx.cs" Inherits="Website.sitecore_modules.Events.ImportEvents"
	MasterPageFile="~/sitecore modules/Site.Master" Title="CareMore > Events > Import" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="contentPlaceHolderHeader" runat="server">
	<script src="/js/jquery-min.js" type="text/javascript"></script>
	<script type="text/javascript">
		function ProcessingFile() {
			$("#tblImport").css("display", "none");
			$("#pnlProcessing").css("display", "block");
			setTimeout(UpdateImage, 50);
		}
		function UpdateImage() {
			$("#imgLoading").attr("src", "/images/ajax-loader.gif");
		}
	</script>
</asp:Content>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<h2>Import Events</h2>

	<asp:PlaceHolder ID="phForm" runat="server">
		<p><asp:ValidationSummary id="valSumPage" runat="server" HeaderText="Please correct the following fields:" /></p>

		<table id="tblImport">
			<tr>
				<td>Event Category:</td>
				<td>
					<asp:RadioButton id="buttonClassic" Text="Classic" Checked="True" GroupName="EventRadioGroup" runat="server" />
					<span style="margin:0 40px;"></span>
					<asp:RadioButton id="buttonDuals" Text="Duals" GroupName="EventRadioGroup" runat="server" />
				</td>
			</tr>
			<tr>
				<td>Full Path of Excel File:</td>
				<td>
					<asp:FileUpload ID="ExcelFile" runat="server" Width="400" />
					<asp:RequiredFieldValidator ID="reqFldValExcelFile" runat="server"
						ControlToValidate="ExcelFile" ErrorMessage="Full Path of Excel File is required."
						Display="Dynamic" Text="*" />
					<asp:RegularExpressionValidator ID="regExpValExcelFile" runat="server"
						ControlToValidate="ExcelFile" ErrorMessage="Only an Excel 97-2003 File (.xls) can be used."
						Display="Dynamic" Text="*"
						ValidationExpression="^.+(\.xls)$" />
				</td>
			</tr>
			<tr>
				<td>Active Sheet Name:</td>
				<td>
					<asp:TextBox ID="ActiveSheet" runat="server" Text="Sheet1"></asp:TextBox>
					<asp:RequiredFieldValidator ID="reqFldValActiveSheet" runat="server"
						ControlToValidate="ActiveSheet" ErrorMessage="Active Sheet Name is required."
						Display="Dynamic" Text="*" />
				</td>
			</tr>
			<tr>
				<td></td>
				<td><!--  OnClientClick="ProcessingFile()" -->
					<asp:LinkButton ID="btnSubmit" runat="server" ClientIDMode="Static" OnClick="BtnSubmit_Click" CssClass="button btnSmall"><span>Import</span></asp:LinkButton>
				</td>
			</tr>
		</table>

		<br />

		<asp:Panel id="pnlProcessing" runat="server" ClientIDMode="Static" style="display: none;">
			<img id="imgLoading" src="/images/ajax-loader.gif" alt="..." />
			<div id="divStatus">Processing please wait...</div>
		</asp:Panel>
	</asp:PlaceHolder>

	<asp:PlaceHolder ID="phStatus" runat="server" Visible="false" />
</asp:Content>
