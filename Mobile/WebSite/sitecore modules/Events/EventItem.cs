﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;

namespace Website.sitecore_modules.Events
{
	public class EventItem
	{
		#region Constructors

		public EventItem()
		{
			EventTypeNames = new List<string>();
		}

		public EventItem(OleDbDataReader dr)
		{
			EventTypeNames = new List<string>();
			FromReader(dr);
		}

		#endregion

		#region Properties

		public string Name { get; set; }
		public string Location { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string ZipCode { get; set; }
		public string Title { get; set; }
		public string County { get; set; }
		public string State { get; set; }
		public string Content { get; set; }
		public DateTime Date { get; set; }
		public List<string> EventTypeNames { get; set; }
		public string Status { get; set; }

		public string ReaderError { get; private set; }

		#endregion

		#region Private methods

		private bool FromReader(OleDbDataReader dr)
		{
			string date = dr["Date"].ToString();
			DateTime dt = DateTime.MinValue;

			if (DateTime.TryParse(date, out dt))
			{
				date = dt.Date.ToShortDateString();
			}
			else
			{
				ReaderError = string.Format("Date not in proper format or missing for event at location {0} in {1}.",
					dr["Location"], dr["City"]);
				return false;
			}

			string time = dr["Time"].ToString();
			DateTime t = DateTime.MinValue;

			if (DateTime.TryParse(time, out t))
			{
				time = t.TimeOfDay.ToString();
			}
			else
			{
				ReaderError = string.Format("Time not in proper format or missing for event at location {0} in {1}.",
					dr["Location"], dr["City"]);
				return false;
			}

			Name = string.Format("{0} {1} {2}", dr["State"], dr["City"], dr["Location"]);

			Location = dr["Location"].ToString();
			Address = dr["Address"].ToString();
			City = dr["City"].ToString();
			ZipCode = dr["Zip Code"].ToString();
			Title = dr["Event Title"].ToString();
			County = dr["County"].ToString().Trim();
			State = dr["State"].ToString();
			Content = dr["Details"].ToString();
			Date = new DateTime(dt.Year, dt.Month, dt.Day, t.Hour, t.Minute, t.Second);

			string[] eventNames = dr["Event Type"].ToString().Split(new char[] {','},
				StringSplitOptions.RemoveEmptyEntries);
			EventTypeNames.AddRange(eventNames);

			string statusType = dr["Status"].ToString();
			if (statusType.Equals("Active", StringComparison.OrdinalIgnoreCase))
				Status = "Active";
			else
				Status = "Inactive";

			return true;
		}

		#endregion
	}
}
