﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Website.sitecore_modules
{
    public class BaseAdminLogin : System.Web.UI.Page
    {
        protected enum AllowTypes
        {
            Broker,
            Enrollment,
            Leads,
            Order,
            Specialty
        }

        protected AllowTypes AllowType { get; set; }
        protected string RedirectPath { get; set; }
        protected string RedirectUrl { get; set; }
        protected string SessionName { get; set; }

        protected void PageLoad(Login loginAdmin, CheckBox saveLogin)
        {
            Session.Remove(this.SessionName);

            if (Request.Cookies[this.SessionName] != null)
            {
                if (!string.IsNullOrEmpty(Request.Cookies[this.SessionName].Value))
                {
                    loginAdmin.UserName = Request.Cookies[this.SessionName].Value;
                    saveLogin.Checked = true;
                }
            }
        }

        protected void AuthenticateLogin(Login loginAdmin, CheckBox saveLogin)
        {
            CaremoreApplicationDataContext dbContext = new CaremoreApplicationDataContext();

            IEnumerable<AdminUser> users = from user in dbContext.AdminUsers
                                           where (user.Username == loginAdmin.UserName)
                                           select user;

            if (users.Count() == 0)
            {
                loginAdmin.FailureText = "The User Name you entered is not valid. Please try again.";
            }
            else
            {
                AdminUser user = users.First();

                if (user.Password != loginAdmin.Password)
                {
                    loginAdmin.FailureText = "The Password you entered is not valid. Please try again.";
                }
                else
                {
                    bool isAllowed = false;
                    switch (this.AllowType)
                    {
                        case AllowTypes.Broker:
                            isAllowed = user.AllowBroker;
                            break;

                        case AllowTypes.Enrollment:
                            isAllowed = user.AllowEnrollment;
                            break;

                        case AllowTypes.Leads:
                            isAllowed = user.AllowLeads;
                            break;

                        case AllowTypes.Order:
                            isAllowed = user.AllowBroker;
                            break;

                        case AllowTypes.Specialty:
                            isAllowed = user.AllowSpecialty;
                            break;

                        default:
                            break;
                    }

                    if (!isAllowed)
                    {
                        loginAdmin.FailureText = "The User Name you entered does not have access to this application.";
                    }
                    else
                    {
                        if (saveLogin.Checked)
                        {
                            HttpCookie aCookie = new HttpCookie(this.SessionName);
                            aCookie.Value = user.Username;
                            aCookie.Expires = DateTime.Now.AddYears(1);
                            Response.Cookies.Add(aCookie);
                        }

                        Session[this.SessionName] = user.Username;
                        Session["RedirectPath"] = this.RedirectPath;
                        Session["RedirectUrl"] = this.RedirectUrl;

                        if (user.PasswordReset)
                        {
							Response.Redirect(this.RedirectUrl, false);
                        }
                        else
                        {
							Response.Redirect("~/sitecore%20modules/ChangePwd.aspx", false);
                        }
                    }
                }
            }
        }
    }
}
