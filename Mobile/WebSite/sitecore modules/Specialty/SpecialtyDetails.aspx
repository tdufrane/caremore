﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SpecialtyDetails.aspx.cs" Inherits="Website.sitecore_modules.Specialty.SpecialtyDetails" MasterPageFile="~/sitecore modules/Site.Master" Title="CareMore > Specialty Admin > Details" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentHeader" runat="server" ContentPlaceHolderID="contentPlaceHolderHeader">
	<div>
		<a href="../ChangePwd.aspx">Change Password</a> |
		<a href="Login.aspx">Log Off</a>
	</div>
</asp:Content>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<div>
		<h1 class="heading">Specialty Details</h1>

		<table id="tblDetails" runat="server" cellpadding="0" cellspacing="0" border="0" class="appDetails">
			<tr>
				<td class="label">Code</td>
				<td><asp:Label ID="lblSpecialtyMappingId" runat="server" /></td>
			</tr>
			<tr class="alt">
				<td class="label">Facets Description</td>
				<td><asp:Label ID="lblFacetsDescription" runat="server" /></td>
			</tr>
			<tr>
				<td class="label">Display On Website</td>
				<td><asp:CheckBox ID="chkBoxDisplayOnWebsite" runat="server" /></td>
			</tr>
			<tr class="alt">
				<td class="label">Website Description</td>
				<td>
					<asp:TextBox ID="txtBoxWebsiteDescription" runat="server" MaxLength="255" />
				</td>
			</tr>
			<tr class="appButton">
				<td colspan="2" align="center">
					<p><asp:Button ID="btnSubmit" runat="server" Text="Save" OnClick="BtnSubmit_Click" CssClass="orangeSubmitButton" /></p>
				</td>
			</tr>
		</table>
	</div>

	<asp:PlaceHolder ID="phStatus" runat="server"></asp:PlaceHolder>

	<p><a href="SpecialtyList.aspx">Back to List</a></p>
</asp:Content>
