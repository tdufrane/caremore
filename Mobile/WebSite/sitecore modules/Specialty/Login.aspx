﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Website.sitecore_modules.Specialty.Login" MasterPageFile="~/sitecore modules/Site.Master" Title="CareMore > Specialty Admin > Login" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<asp:ValidationSummary id="valSumPage" runat="server"
		HeaderText="Please correct the following fields:" />

	<table border="0" cellspacing="0">
		<tr>
			<td>
				<asp:Login ID="loginAdmin" runat="server"
					DisplayRememberMe="false"
					LoginButtonText="&lt;span&gt;Log In&lt;/span&gt;" LoginButtonType="Link" LoginButtonStyle-CssClass="button btnSmall"
					OnAuthenticate="LoginAdmin_Authenticate"
					PasswordRequiredErrorMessage="Password is required."
					TextLayout="TextOnLeft"
					TitleTextStyle-CssClass="heading"
					UserNameRequiredErrorMessage="User Name is required.">
				</asp:Login>
			</td>
			<td>
				<asp:CheckBox ID="cbSaveLogin" runat="server" Text="Remember User Name" />
			</td>
		</tr>
	</table>
</asp:Content>
