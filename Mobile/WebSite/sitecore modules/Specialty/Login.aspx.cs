﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Website.sitecore_modules.Specialty
{
    public partial class Login : BaseAdminLogin
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.AllowType = AllowTypes.Specialty;
            this.RedirectPath = "Specialty/";
            this.RedirectUrl = "SpecialtyList.aspx";
            this.SessionName = "SpecialtyAdminName";

            PageLoad(loginAdmin, cbSaveLogin);
        }

        protected void LoginAdmin_Authenticate(object sender, AuthenticateEventArgs e)
        {
            AuthenticateLogin(loginAdmin, cbSaveLogin);
        }
    }
}
