﻿using System;
using Sitecore.Shell.Framework.Commands;

namespace Website.sitecore_modules.Specialty
{
	[Serializable]
	public class SpecialtyAdmin : Command
	{
		public override void Execute(CommandContext context)
		{
			Sitecore.Layouts.PageContext pageContext = Sitecore.Context.Page;
			Uri uri = pageContext.Page.Request.Url;
			string hostName = uri.Host;

			string url = "http://" + hostName + "/sitecore modules/Specialty/Login.aspx";
			Sitecore.Shell.Framework.Windows.RunUri(url, "", "Specialty Administration");
		}
	}
}
