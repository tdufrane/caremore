﻿using System;
using System.Collections.Generic;

namespace Website.sitecore_modules.Developer
{
    [Serializable]
    public class DataTemplateItem
    {
        #region Constructor

        public DataTemplateItem()
        {
            this._fieldItems = new List<DataTemplateFieldItem>();
        }

        #endregion

        #region Properties

        public string FullName { get; set; }

        private List<DataTemplateFieldItem> _fieldItems;
        public List<DataTemplateFieldItem> FieldItems
        {
            get { return this._fieldItems; }
        }

        #endregion
    }
}
