﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web.Services;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Globalization;

namespace Website.sitecore_modules.Developer
{
    /// <summary>
    /// Summary description for Data
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class Data : System.Web.Services.WebService
    {
        #region Constants

        private const string TemplatePath = "/sitecore/templates/";

        #endregion

        #region Web methods

        [WebMethod]
        public List<DataItem> ExportFields(string path)
        {
            List<DataItem> items;

            // Templates need to be handled differently as want section names too
            if (path.StartsWith(TemplatePath))
                items = ExportTemplateFieldNames(path);
            else
                items = ExportFieldNames(path);

            return items;
        }

        [WebMethod]
        public List<DataItem> ExportItems(string path)
        {
            return ExportItemPaths(path);
        }

        [WebMethod]
        public List<DataItem> ExportValues(List<DataItem> items)
        {
            // Export all items
            Database db = Sitecore.Configuration.Factory.GetDatabase("master");

            Item itemEnglish = null;
            Item itemSpanish = null;

            foreach (DataItem item in items)
            {
                itemEnglish = db.GetItem(item.Path);
                if (itemEnglish != null)
                {
                    Field field = itemEnglish.Fields[item.Field];
                    item.Value = field.Value;
                }

                itemSpanish = db.GetItem(item.Path, Language.Parse("es-mx"));
                if (itemSpanish != null)
                {
                    Field field = itemSpanish.Fields[item.Field];
                    item.ValueSpanish = field.Value;
                }
            }

            return items;
        }

        [WebMethod]
        public void ImportValues(List<DataItem> items)
        {
            using (new Sitecore.SecurityModel.SecurityDisabler())
            {
                Database db = Sitecore.Configuration.Factory.GetDatabase("master");

                Item itemEnglish = null;
                Item itemSpanish = null;

                foreach (DataItem sitecoreItem in items)
                {
                    itemEnglish = db.GetItem(sitecoreItem.Path);
                    if (itemEnglish != null)
                    {
                        EditItem(itemEnglish, sitecoreItem, false);
                    }

                    itemSpanish = db.GetItem(sitecoreItem.Path, Language.Parse("es-mx"));
                    if (itemSpanish != null)
                    {
                        EditItem(itemSpanish, sitecoreItem, true);
                    }
                }
            }
        }

        [WebMethod]
        public List<CheckDataItem> InvalidRichText(string path, bool recursive)
        {
            return CheckRichText(path, recursive);
        }

        [WebMethod]
        public List<DataTemplateItem> TemplateInformation(string rootPath, bool recursive)
        {
            Database db = Sitecore.Configuration.Factory.GetDatabase("master");
            TemplateItem templateItem = db.GetTemplate(rootPath.Substring(TemplatePath.Length));

            List<DataTemplateItem> templateItems = new List<DataTemplateItem>();
            ProcessTemplate(templateItem, templateItems);

            if (recursive)
            {
                Item itemRoot = db.GetItem(rootPath);
                List<string> itemPaths = new List<string>();

                RecurseItems(itemRoot, itemPaths);

                foreach (string path in itemPaths)
                {
                    Item item = db.GetTemplate(path.Substring(TemplatePath.Length));
                    ProcessTemplate(item, templateItems);
                }
            }

            return templateItems;
        }

        #endregion

        #region Private methods

        private List<CheckDataItem> CheckRichText(string path, bool recursive)
        {
            List<CheckDataItem> items = new List<CheckDataItem>();

            Database db = Sitecore.Configuration.Factory.GetDatabase("master");
            Item item = db.GetItem(path);

            if (item != null)
            {
                CheckRichText(db, item, items, recursive);
            }

            return items;
        }

        private void CheckRichText(Database db, Item item, List<CheckDataItem> items, bool recursive)
        {
            foreach (Field field in item.Fields)
            {
                if (field.Type.Equals("Rich Text"))
                {
                    CheckDataItem dataItem = new CheckDataItem(item.Paths.FullPath, field.Name, field.Value);

                    Item itemSpanish = db.GetItem(item.Paths.FullPath, Language.Parse("es-mx"));
                    if (itemSpanish != null)
                    {
                        dataItem.ValueSpanish = itemSpanish[field.Name];
                    }

                    dataItem.CheckForErrors();

                    items.Add(dataItem);
                }
            }

            if (recursive)
            {
                foreach (Item subItem in item.Children)
                {
                    CheckRichText(db, subItem, items, recursive);
                }
            }
        }

        private void EditItem(Item item, DataItem sitecoreItem, bool isSpanish)
        {
            item.Editing.BeginEdit();

            Field field = item.Fields[sitecoreItem.Field];

            if (isSpanish)
            {
                if (sitecoreItem.AllLanguages)
                    SetItem(field, sitecoreItem.Value);
                else
                    SetItem(field, sitecoreItem.ValueSpanish);
            }
            else
            {
                SetItem(field, sitecoreItem.Value);
            }

            item.Editing.AcceptChanges();
            item.Editing.EndEdit();
        }

        private List<DataItem> ExportFieldNames(string path)
        {
            List<DataItem> items = new List<DataItem>();

            Database db = Sitecore.Configuration.Factory.GetDatabase("master");
            Item item = db.GetItem(path);

            if (item != null)
            {
                foreach (Field field in item.Fields)
                {
                    items.Add(new DataItem(path, field.Name, field.Value));
                }
            }

            return items;
        }

        private List<DataItem> ExportItemPaths(string path)
        {
            List<DataItem> items = new List<DataItem>();

            Database db = Sitecore.Configuration.Factory.GetDatabase("master");
            Item item = db.GetItem(path);

            if (item != null)
            {
                foreach (Item subItem in item.Children)
                {
                    items.Add(new DataItem(subItem.Paths.FullPath, string.Empty, string.Empty));
                }
            }

            return items;
        }

        private List<DataItem> ExportTemplateFieldNames(string path)
        {
            List<DataItem> items = new List<DataItem>();

            Database db = Sitecore.Configuration.Factory.GetDatabase("master");
            TemplateItem templateItem = db.GetTemplate(path.Substring(TemplatePath.Length));

            if (templateItem != null)
            {
                foreach (TemplateSectionItem section in templateItem.GetSections())
                {
                    foreach (TemplateFieldItem field in section.GetFields())
                    {
                        items.Add(new DataItem(path, section.Name, field.Name));
                    }
                }
            }

            return items;
        }

        private void ProcessTemplate(TemplateItem item, List<DataTemplateItem> templateItems)
        {
            if (item != null)
            {
                DataTemplateItem templateItem = new DataTemplateItem();
                templateItem.FullName =  item.FullName;

                foreach (TemplateFieldItem field in item.OwnFields)
                {
                    DataTemplateFieldItem fieldItem = new DataTemplateFieldItem();
                    fieldItem.Name =  field.Name;
                    fieldItem.Type = field.Type;
                    templateItem.FieldItems.Add(fieldItem);
                }

                templateItems.Add(templateItem);
            }
        }

        private void RecurseItems(Item item, List<string> paths)
        {
            foreach (Item childItem in item.Children)
            {
                paths.Add(childItem.Paths.FullPath);
                RecurseItems(childItem, paths);
            }
        }

        private void SetItem(Field field, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                field.Reset();
            }
            else
            {
                field.Value = value;
            }
        }

        #endregion
    }
}
