﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Schema;

namespace Website.sitecore_modules.Developer
{
    public class CheckDataItem : DataItem
    {
        #region Constructors

        public CheckDataItem()
        {
        }

        public CheckDataItem(string path, string field, string value) : base(path, field, value)
        {
        }

        #endregion

        #region Constants

        private const string EmptyAttribute = "=\"\"";

        #endregion

        #region Properties

        public string ShortPath
        {
            get
            {
                if (string.IsNullOrEmpty(base.Path))
                {
                    return base.Path;
                }
                else
                {
                    return base.Path.Replace("/sitecore/content", string.Empty);
                }
            }
        }

        private string _errorsEnglish;
        public string ErrorsEnglish
        {
            get { return this._errorsEnglish; }
            set { this._errorsEnglish = value; }
        }

        private string _errorsSpanish;
        public string ErrorsSpanish
        {
            get { return this._errorsSpanish; }
            set { this._errorsSpanish = value; }
        }

        #endregion

        #region Methods

        public void CheckForErrors()
        {
            this._errorsEnglish = string.Empty;
            this._errorsSpanish = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(base.Value))
                {
                    this._errorsEnglish = IsValidXhtml(base.Value);
                }
            }
            catch (Exception ex)
            {
                this._errorsEnglish = "Exception: " + ex.Message;
            }

            try
            {
                if (!string.IsNullOrEmpty(base.ValueSpanish))
                {
                    this._errorsSpanish = IsValidXhtml(base.ValueSpanish);
                }
            }
            catch (Exception ex)
            {
                this._errorsSpanish = "Exception: " + ex.Message;
            }
        }

        private string IsValidXhtml(string xhtml)
        {
            StringBuilder validationErrors = new StringBuilder();

            if (xhtml.IndexOf("mso-bidi-font", StringComparison.InvariantCultureIgnoreCase) > -1)
            {
                validationErrors.Append("Microsoft Word artifact 'mso-bidi-font' exists.\n");
            }

            if (xhtml.IndexOf("MsoNormal", StringComparison.InvariantCultureIgnoreCase) > -1)
            {
                validationErrors.Append("Microsoft Word artifact 'MsoNormal' exists.\n");
            }

            if (xhtml.IndexOf("<font", StringComparison.InvariantCultureIgnoreCase) > -1)
            {
                validationErrors.Append("The tag 'font' is used.\n");
            }

            CheckForEmptyAttribute(xhtml, validationErrors);

            XhtmlValidator validator = new XhtmlValidator();
            string parseError = validator.Parse(xhtml);
            if (!string.IsNullOrEmpty(parseError))
            {
                validationErrors.Append(parseError);
                validationErrors.Append("\n");
            }

            if (validationErrors.Length == 0)
            {
                return string.Empty;
            }
            else
            {
                validationErrors.Length -= 2; // remove trailing \n
                return validationErrors.ToString();
            }
        }

        private void CheckForEmptyAttribute(string xhtml, StringBuilder validationErrors)
        {
            int emptyIndex = xhtml.IndexOf(EmptyAttribute);

            while (emptyIndex > -1)
            {
                if (!xhtml.Substring(emptyIndex - 3, 3).Equals("alt"))
                    validationErrors.Append("An empty tag attribute '=\"\"' exists.\n");

                emptyIndex = xhtml.IndexOf(EmptyAttribute, emptyIndex + EmptyAttribute.Length);
            }
        }

        #endregion
    }
}
