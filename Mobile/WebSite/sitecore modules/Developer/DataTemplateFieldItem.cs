﻿using System;

namespace Website.sitecore_modules.Developer
{
    [Serializable]
    public class DataTemplateFieldItem
    {
        #region Constructor

        public DataTemplateFieldItem()
        {
        }

        #endregion

        #region Properties

        public string Name { get; set; }
        public string Type { get; set; }

        #endregion
    }
}
