﻿using System;
using Sitecore.Data;

namespace Website.sitecore_modules
{
	public partial class Site : System.Web.UI.MasterPage
	{
		public Database CurrentDB = Sitecore.Context.Database;

		public string NotAuthorized
		{
			get
			{
				return "You are not authorized to access this page.";
			}
		}

		public string SuccessClass
		{
			get
			{
				return "text-success";
			}
		}
	}
}
