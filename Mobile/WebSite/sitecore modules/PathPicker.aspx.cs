﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace Website.sitecore_modules
{
	public partial class PathPicker : System.Web.UI.Page
	{
		private Database currentDB = Sitecore.Context.Database;

		protected void Page_Load(object sender, EventArgs e)
		{
			string root = Request.QueryString["root"];

			if (string.IsNullOrEmpty(root))
				root = "/sitecore/Content";

			Item itemRoot = currentDB.GetItem(root);

			TreeNode rootNode = new TreeNode(itemRoot.Name, itemRoot.Paths.FullPath);
			RecurseItems(itemRoot, rootNode);
			tvPaths.Nodes.Add(rootNode);
		}

		protected void TvPaths_SelectedNodeChanged(object sender, EventArgs e)
		{
			string controlName = Request.QueryString["control"];

			TreeNode node = this.tvPaths.SelectedNode;
			string fullPath = node.Value;

			StringBuilder script = new StringBuilder();
			script.Append("if ((window.opener != null) && (window.opener.aspnetForm != null))\r\n");
			script.Append("{\r\n");
			script.AppendFormat("var control = eval(\"window.opener.document.aspnetForm.{0}\");\r\n", controlName);
			script.AppendFormat("control.value = \"{0}\";\r\n", node.Value);
			script.Append("}\r\n");
			script.Append("window.close();");

			ClientScript.RegisterClientScriptBlock(this.GetType(), "SetPath", script.ToString(), true);
		}

		private void RecurseItems(Item item, TreeNode parentNode)
		{
			foreach (Item childItem in item.Children)
			{
				TreeNode node = new TreeNode(childItem.Name, childItem.Paths.FullPath);

				RecurseItems(childItem, node);

				parentNode.ChildNodes.Add(node);
			}
		}
	}
}
