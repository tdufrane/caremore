﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CareMore.Web.DotCom;
using Sitecore.SecurityModel;
using Sitecore.Data.Fields;

namespace Website.WebControls
{
	public class Link : Sitecore.Web.UI.WebControls.Link
	{
		#region Properties

		public string ItemReferenceName { get; set; }
		public string DisplayText { get; set; }
		public bool AddSpanTag { get; set; }
		public string SpanCssClass { get; set; }

		#endregion

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			if (!string.IsNullOrEmpty(ItemReferenceName))
				Item = ItemIds.GetItem(ItemReferenceName);
		}

		protected override void DoRender(System.Web.UI.HtmlTextWriter output)
		{
			if (Item != null && !string.IsNullOrEmpty(DisplayText))
			{
				Text = Item[DisplayText];
			}

			base.DoRender(output);
		}

		protected override void OnDataBinding(System.EventArgs e)
		{
			base.OnDataBinding(e);

			if (Item == null)
			{
				Item = WebControlUtil.GetItemFromDataBind(NamingContainer);
			}

			if (Item != null && !string.IsNullOrEmpty(DisplayText))
			{
				Text = Item[DisplayText];
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if (Item == null)
			{
				Item = Sitecore.Context.Item;
			}

			if (AddSpanTag)
			{
				if (Text == null)
				{
					LinkField linkField = Item.Fields[Field];

					if (linkField != null)
						Text = linkField.Text;
				}

				if (!string.IsNullOrEmpty(Text))
				{
					if (string.IsNullOrEmpty(SpanCssClass))
						Text = string.Format("<span>{0}</span>", Text);
					else
						Text = string.Format("<span class=\"{0}\"></span> {1}", SpanCssClass, Text);
				}
			}
		}
	}
}