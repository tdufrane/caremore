﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Sitecore.Data.Items;
using System.Web.UI.WebControls;
using Website.BL.ListItem;
using Website.Sublayouts.Widgets;
using Website.BL;

namespace Website.WebControls
{
    public class WebControlUtil
    {
        public static Item GetItemFromDataBind(Control namingContainer)
        {
            WidgetBase standardWidget = namingContainer as WidgetBase;
            if (standardWidget != null)
                return standardWidget.Item;

            RepeaterItem rpItem = namingContainer as RepeaterItem;
            if (rpItem == null)
                return null;

            if (rpItem.DataItem is Item)
                return rpItem.DataItem as Item;

            StandardItem navItem = rpItem.DataItem as StandardItem;
            if (navItem == null)
                return null;

            return navItem.Item;
        }
    }
}