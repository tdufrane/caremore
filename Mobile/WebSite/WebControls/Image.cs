﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CareMore.Web.DotCom;

namespace Website.WebControls
{
    public class Image : Sitecore.Web.UI.WebControls.Image
    {
        public string ItemReferenceName { get; set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (Item == null && !string.IsNullOrEmpty(ItemReferenceName))
                Item = ItemIds.GetItem(ItemReferenceName);
        }

        protected override void OnDataBinding(System.EventArgs e)
        {
            base.OnDataBinding(e);

            if (Item == null)
                Item = WebControlUtil.GetItemFromDataBind(NamingContainer);
        }
    }
}