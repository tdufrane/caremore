﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using CareMore.Web.DotCom;
using Website.BL.ListItem;

namespace Website.BL.Plans
{
	public class PlanManager
	{
		public static Item GetPlanCountyItem(Item planItem, Item countyItem)
		{
			if(countyItem == null)
				return null;

			return GetPlanCountyItem(planItem, countyItem["County"]);
		}

		public static Item GetPlanCountyItem(Item planItem, string countyValue, Item currentItem = null)
		{
			if (currentItem == null)
				currentItem = planItem;

			if (currentItem["State"] == CareMoreUtilities.GetCurrentLocale() &&
				GetComparisonValueCounty(currentItem["County"]).ToLower().Contains(countyValue.ToLower()))
				return currentItem;

			foreach (Item childItem in currentItem.Children)
			{
				Item result = GetPlanCountyItem(planItem, countyValue, childItem);

				if (result != null)
					return result;
			}

			return null;
		}

		private static string GetComparisonValueCounty(string county)
		{
			string itemPath = string.Format("{0}//*[@@templatename='County' and @Name='{1}']",
				CareMoreUtilities.LOCALIZATION_PATH, county);

			Item countyItem = Sitecore.Context.Database.SelectSingleItem(itemPath);
			return county;
		}

		public static PlanType GetPlanType(Item plan, Item countyItem)
		{
			if (plan == null)
				return null;

			LookupField planTypeField = plan.Fields["Plan Type"];

			if (planTypeField == null || planTypeField.TargetItem == null)
				return null;

			PlanType planType = new PlanType();

			planType.LocationItem = GetLocationItem(planTypeField.TargetItem, countyItem);

			return planType;
		}

		public static Item GetLocationItem(Item rootItem, Item countyItem)
		{
			if (rootItem["State"] == countyItem.Name && 
				GetComparisonValueCounty(rootItem["County"]).ToLower().Contains(countyItem["County"]))
				return rootItem;

			foreach(Item childItem in rootItem.Children)
			{
				Item result = GetLocationItem(childItem, countyItem);

				if(result != null)
					return result;
			}

			return null;
		}

		public static List<PlanFileItem> GetPlanFiles(Item plan, Item countyItem)
		{
			List<PlanFileItem> list = new List<PlanFileItem>();

			if ((plan == null) || (string.IsNullOrEmpty(plan["Plan Type"])))
				return list;

			Item linkItems = Sitecore.Context.Database.GetItem(plan.Fields["Plan Type"].Value);

			if (linkItems.Language != plan.Language)
				linkItems = Sitecore.Context.Database.GetItem(linkItems.ID, plan.Language);

			if (linkItems != null)
			{
				string templateId = ItemIds.GetString("LINKS_TEMPLATE_ID");

				foreach (Item childItem in linkItems.Axes.GetDescendants())
				{
					if (childItem.TemplateID.ToString() == templateId && childItem.Parent["Region Name"] == countyItem["Region Name"])
					{
						PlanFileItem fileItem = new PlanFileItem();

						fileItem.Url = CareMoreUtilities.GetItemUrl(childItem, "Link");
						fileItem.Text = childItem["Text"];
						LinkField link = childItem.Fields["Link"];
						fileItem.Target = link.Target;

						list.Add(fileItem);
					}
				}
			}

			FileField summaryFile = plan.Fields["Summary PDF File"];
			FileField evidenceFile = plan.Fields["Evidence PDF File"];
			FileField anocFile = plan.Fields["ANOC PDF File"];

			string planYear = plan.Parent.Name;

			if (summaryFile != null && !string.IsNullOrEmpty(summaryFile.Src))
			{
				PlanFileItem fileItem = new PlanFileItem();

				fileItem.Url = summaryFile.Src;
				fileItem.Text = planYear + " " + plan["Summary Button Text"];
				fileItem.Target = "_blank"; //Forced to open in new window

				list.Add(fileItem);
			}

			if (evidenceFile != null && !string.IsNullOrEmpty(evidenceFile.Src))
			{
				PlanFileItem fileItem = new PlanFileItem();

				fileItem.Url = evidenceFile.Src;
				fileItem.Text = planYear + " " + plan["Evidence Button Text"];
				fileItem.Target = "_blank"; //Forced to open in new window

				list.Add(fileItem);
			}

			if (anocFile != null && !string.IsNullOrEmpty(anocFile.Src))
			{
				PlanFileItem fileItem = new PlanFileItem();

				fileItem.Url = anocFile.Src;
				fileItem.Text = planYear + " " + plan["ANOC Button Text"];
				fileItem.Target = "_blank"; //Forced to open in new window

				list.Add(fileItem);
			}

			return list;
		}
	}
}
