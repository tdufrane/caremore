﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;

namespace Website.BL.Plans
{
    public class PlanType
    {
        public Item LocationItem { get; set; }
        public Item GeneralItem { get; set; }
    }
}