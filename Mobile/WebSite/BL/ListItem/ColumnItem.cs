﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.BL.ListItem
{
    public class ColumnItem
    {
        public string CssClass { get; set; }
        public List<DetailItem> Items = new List<DetailItem>();
    }
}