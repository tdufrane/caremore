﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;

namespace Website.BL.ListItem
{
    public class PlanFileItem
    {
        public string Url { get; set; }
        public string Text { get; set; }
		public string Target { get; set; }
    }
}