﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;

namespace Website.BL.ListItem
{
    public class StandardItem
    {
        public Item Item { get; set; }

        public StandardItem(Item item)
        {
            this.Item = item;
        }
    }
}