﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CareMore.Web.DotCom;
using Sitecore.Data.Items;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Managers;

namespace Website.BL
{
	public static class CoverageManager
	{
		public static string ZipCodeState { get; set; }
		public static string ZipCodesCounty { get; set; }

		public static bool IsZipCodeCovered(string zipCode)
		{
			ID coveredID = ItemIds.GetID("COVERED_ZIP_CODE");

			Item coveredItem = Sitecore.Context.Database.GetItem(coveredID, LanguageManager.GetLanguage("en"));

			foreach (Item childItem in coveredItem.Axes.GetDescendants())
			{
				if (IsZipCodeCovered(childItem, zipCode))
					return true;
			}

			return false;
		}

		private static bool IsZipCodeCovered(Item item, string zipCode)
		{
			List<String> list = new List<string>();

			if (string.IsNullOrWhiteSpace(zipCode))
				return false;

			foreach (String currentZip in StringUtil.Split(item["Zip Codes"], '\r', true))
			{
				String cleanZipCode = GetCleanZipCode(currentZip);

				if (cleanZipCode == zipCode)
				{
					ZipCodeState = GetStateName(item["State"]);
					ZipCodesCounty = GetCountyName(ZipCodeState, item["County"]);
					return true;
				}
			}

			return false;
		}

		private static String GetStateName(string state)
		{
			Item[] states = Sitecore.Context.Database.SelectItems(CareMoreUtilities.LOCALIZATION_PATH + "//*[@@templatename='State']");

			foreach (var i in states)
			{
				if (state.Contains(i.Name))
				{
					return i.Name;
				}
			}
			return state;
		}

		private static String GetCountyName(string state, string county)
		{
			Item[] counties = Sitecore.Context.Database.SelectItems(CareMoreUtilities.LOCALIZATION_PATH + "/" + state + "//*[@@templatename='County']");

			foreach (var i in counties)
			{
				if (county.Contains(i.Name))
				{
					return i.Name;
				}
			}
			return county;
		}

		private static String GetCleanZipCode(String zipCode)
		{
			string cleanCode = zipCode;

			if (cleanCode.Contains(','))
			{
				cleanCode = cleanCode.Split(',')[0];
			}

			cleanCode = cleanCode.Trim();

			if (cleanCode.Contains(' '))
			{
				cleanCode = cleanCode.Split(' ')[0];
			}

			return cleanCode;
		}
	}
}
