using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Widgets {
    
    
    public partial interface IContentImageBox {
        
        ImageField Image {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Widgets.ContentImageBox.TemplateID)]
    public partial class ContentImageBox : Duals.Models.Widgets.ContentTextBox, Duals.Models.Widgets.IContentImageBox {
        
        #region Members
        public const string TemplateID = "{16E001FA-D523-44B7-825D-090B2E650B7A}";
        
        public const string Image_FID = "{56856AF4-8D20-44DC-926B-703FD5CC92BC}";
        #endregion
        
        #region Constructors
        public ContentImageBox(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Widgets.ContentImageBox.Image_FID)]
        public virtual ImageField Image {
            get {
                return this.GetField<ImageField>(Duals.Models.Widgets.ContentImageBox.Image_FID);
            }
            set {
                this.SetField<ImageField>(Duals.Models.Widgets.ContentImageBox.Image_FID, value);
            }
        }
        #endregion
    }
}
