using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Widgets {
    
    
    public partial interface IMemberStory {
        
        string Name {
            get;
            set;
        }
        
        string Description {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Widgets.MemberStory.TemplateID)]
    public partial class MemberStory : StandardTemplate, Duals.Models.Widgets.IMemberStory, Duals.Models.Widgets.IVideo {
        
        #region Members
        public new const string TemplateID = "{CE2CFE28-5BE0-4F69-8FCE-98CC53F40CBC}";
        
        public const string Name_FID = "{316E4579-65EB-4154-ABBA-9C679124CF0C}";
        
        public const string Description_FID = "{6F301654-EA06-49FA-9B6C-D00F7733EF0A}";
        
        private string _name;
        
        private string _description;
        #endregion
        
        #region Constructors
        public MemberStory(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Widgets.MemberStory.Name_FID)]
        public virtual string Name {
            get {
                if (_name == null) {
_name = this.GetString(Duals.Models.Widgets.MemberStory.Name_FID);
                }
                return _name;
            }
            set {
                _name = null;
                this.SetString(Duals.Models.Widgets.MemberStory.Name_FID, value);
            }
        }
        
        [Field(Duals.Models.Widgets.MemberStory.Description_FID)]
        public virtual string Description {
            get {
                if (_description == null) {
_description = this.GetString(Duals.Models.Widgets.MemberStory.Description_FID);
                }
                return _description;
            }
            set {
                _description = null;
                this.SetString(Duals.Models.Widgets.MemberStory.Description_FID, value);
            }
        }
        
        [Field(Duals.Models.Widgets.Video.VideoThumbnail_FID)]
        public virtual Sitecore.Data.Fields.ImageField VideoThumbnail {
            get {
                return this.GetField<ImageField>(Duals.Models.Widgets.Video.VideoThumbnail_FID);
            }
            set {
                this.SetField<ImageField>(Duals.Models.Widgets.Video.VideoThumbnail_FID, value);
            }
        }
        
        [Field(Duals.Models.Widgets.Video.VideoLink_FID)]
        public virtual Sitecore.Data.Fields.LinkField VideoLink {
            get {
                return this.GetField<LinkField>(Duals.Models.Widgets.Video.VideoLink_FID);
            }
            set {
                this.SetField<LinkField>(Duals.Models.Widgets.Video.VideoLink_FID, value);
            }
        }
        #endregion
    }
}
