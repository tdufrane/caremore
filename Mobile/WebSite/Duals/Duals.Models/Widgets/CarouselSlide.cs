using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Widgets {
    
    
    public partial interface ICarouselSlide {
        
        Sitecore.Data.Fields.ImageField Image {
            get;
            set;
        }
        
        string Title {
            get;
            set;
        }
        
        string Text {
            get;
            set;
        }
        
        Sitecore.Data.Fields.LinkField Link {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Widgets.CarouselSlide.TemplateID)]
    public partial class CarouselSlide : StandardTemplate, Duals.Models.Widgets.ICarouselSlide {
        
        #region Members
        public new const string TemplateID = "{ABAA0D72-1434-4E3D-8D74-32DCB6A100E7}";
        
        public const string Title_FID = "{F3B675A5-BC1E-4699-B4C2-6573CB4A2A88}";
        
        public const string Image_FID = "{E5756414-18ED-4A62-B22C-BD99DA6B2859}";
        
        public const string Link_FID = "{6197EFF5-6C9E-4743-B66E-B7EC6796CF60}";
        
        public const string Text_FID = "{5E7AA63A-712B-4899-B1EB-516F4619D95F}";
        
        private string _title;
        
        private string _text;
        #endregion
        
        #region Constructors
        public CarouselSlide(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Widgets.CarouselSlide.Image_FID)]
        public virtual Sitecore.Data.Fields.ImageField Image {
            get {
                return this.GetField<ImageField>(Duals.Models.Widgets.CarouselSlide.Image_FID);
            }
            set {
                this.SetField<ImageField>(Duals.Models.Widgets.CarouselSlide.Image_FID, value);
            }
        }
        
        [Field(Duals.Models.Widgets.CarouselSlide.Title_FID)]
        public virtual string Title {
            get {
                if (_title == null) {
_title = this.GetString(Duals.Models.Widgets.CarouselSlide.Title_FID);
                }
                return _title;
            }
            set {
                _title = null;
                this.SetString(Duals.Models.Widgets.CarouselSlide.Title_FID, value);
            }
        }
        
        [Field(Duals.Models.Widgets.CarouselSlide.Text_FID)]
        public virtual string Text {
            get {
                if (_text == null) {
_text = this.GetString(Duals.Models.Widgets.CarouselSlide.Text_FID);
                }
                return _text;
            }
            set {
                _text = null;
                this.SetString(Duals.Models.Widgets.CarouselSlide.Text_FID, value);
            }
        }
        
        [Field(Duals.Models.Widgets.CarouselSlide.Link_FID)]
        public virtual Sitecore.Data.Fields.LinkField Link {
            get {
                return this.GetField<LinkField>(Duals.Models.Widgets.CarouselSlide.Link_FID);
            }
            set {
                this.SetField<LinkField>(Duals.Models.Widgets.CarouselSlide.Link_FID, value);
            }
        }
        #endregion
    }
}
