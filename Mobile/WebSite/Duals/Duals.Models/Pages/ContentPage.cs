using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Pages {
    
    
    public partial interface IContentPage {
        
        string PageContent {
            get;
            set;
        }
        
        string MastheadTitle {
            get;
            set;
        }
        
        string MastheadDescription {
            get;
            set;
        }
        
        ImageField MastheadImage {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Pages.ContentPage.TemplateID)]
    public partial class ContentPage : Duals.Models.Base.Page, Duals.Models.Pages.IContentPage {
        
        #region Members
        public const string TemplateID = "{A0996798-A0D8-45B9-BAB7-1117962D5F03}";
        
        public const string MastheadImage_FID = "{C131F4EC-DC51-4952-BD5A-FFBA2B97CB07}";
        
        public const string MastheadDescription_FID = "{D01CF765-2C42-4090-B0B1-E9B205B46F9E}";
        
        public const string PageContent_FID = "{E85BABA2-20A3-497B-BCB1-32395859B3A4}";
        
        public const string MastheadTitle_FID = "{6E8256DF-5B58-4E7D-91CF-F647E4DDDC34}";
        
        private string _pageContent;
        
        private string _mastheadTitle;
        
        private string _mastheadDescription;
        #endregion
        
        #region Constructors
        public ContentPage(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Pages.ContentPage.PageContent_FID)]
        public virtual string PageContent {
            get {
                if (_pageContent == null) {
_pageContent = this.GetString(Duals.Models.Pages.ContentPage.PageContent_FID);
                }
                return _pageContent;
            }
            set {
                _pageContent = null;
                this.SetString(Duals.Models.Pages.ContentPage.PageContent_FID, value);
            }
        }
        
        [Field(Duals.Models.Pages.ContentPage.MastheadTitle_FID)]
        public virtual string MastheadTitle {
            get {
                if (_mastheadTitle == null) {
_mastheadTitle = this.GetString(Duals.Models.Pages.ContentPage.MastheadTitle_FID);
                }
                return _mastheadTitle;
            }
            set {
                _mastheadTitle = null;
                this.SetString(Duals.Models.Pages.ContentPage.MastheadTitle_FID, value);
            }
        }
        
        [Field(Duals.Models.Pages.ContentPage.MastheadDescription_FID)]
        public virtual string MastheadDescription {
            get {
                if (_mastheadDescription == null) {
_mastheadDescription = this.GetString(Duals.Models.Pages.ContentPage.MastheadDescription_FID);
                }
                return _mastheadDescription;
            }
            set {
                _mastheadDescription = null;
                this.SetString(Duals.Models.Pages.ContentPage.MastheadDescription_FID, value);
            }
        }
        
        [Field(Duals.Models.Pages.ContentPage.MastheadImage_FID)]
        public virtual ImageField MastheadImage {
            get {
                return this.GetField<ImageField>(Duals.Models.Pages.ContentPage.MastheadImage_FID);
            }
            set {
                this.SetField<ImageField>(Duals.Models.Pages.ContentPage.MastheadImage_FID, value);
            }
        }
        #endregion
    }
}
