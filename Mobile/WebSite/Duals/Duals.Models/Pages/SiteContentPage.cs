using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Pages {
    
    
    public partial interface ISiteContentPage {
    }
    
    [Template(Duals.Models.Pages.SiteContentPage.TemplateID)]
    public partial class SiteContentPage : Duals.Models.Pages.SitePage, Duals.Models.Pages.ISiteContentPage, Duals.Models.Pages.IContentPage {
        
        #region Members
        public const string TemplateID = "{BAF5BB45-0485-48E5-8829-DBAA4A0BA706}";
        
        private string _pageContent;
        
        private string _mastheadTitle;
        
        private string _mastheadDescription;
        #endregion
        
        #region Constructors
        public SiteContentPage(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Pages.ContentPage.PageContent_FID)]
        public virtual string PageContent {
            get {
                if (_pageContent == null) {
_pageContent = this.GetString(Duals.Models.Pages.ContentPage.PageContent_FID);
                }
                return _pageContent;
            }
            set {
                _pageContent = null;
                this.SetString(Duals.Models.Pages.ContentPage.PageContent_FID, value);
            }
        }
        
        [Field(Duals.Models.Pages.ContentPage.MastheadTitle_FID)]
        public virtual string MastheadTitle {
            get {
                if (_mastheadTitle == null) {
_mastheadTitle = this.GetString(Duals.Models.Pages.ContentPage.MastheadTitle_FID);
                }
                return _mastheadTitle;
            }
            set {
                _mastheadTitle = null;
                this.SetString(Duals.Models.Pages.ContentPage.MastheadTitle_FID, value);
            }
        }
        
        [Field(Duals.Models.Pages.ContentPage.MastheadDescription_FID)]
        public virtual string MastheadDescription {
            get {
                if (_mastheadDescription == null) {
_mastheadDescription = this.GetString(Duals.Models.Pages.ContentPage.MastheadDescription_FID);
                }
                return _mastheadDescription;
            }
            set {
                _mastheadDescription = null;
                this.SetString(Duals.Models.Pages.ContentPage.MastheadDescription_FID, value);
            }
        }
        
        [Field(Duals.Models.Pages.ContentPage.MastheadImage_FID)]
        public virtual ImageField MastheadImage {
            get {
                return this.GetField<ImageField>(Duals.Models.Pages.ContentPage.MastheadImage_FID);
            }
            set {
                this.SetField<ImageField>(Duals.Models.Pages.ContentPage.MastheadImage_FID, value);
            }
        }
        #endregion
    }
}
