using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Pages {
    
    
    public partial interface ILandingPage {
        
        string PrimaryNavCSS {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Pages.LandingPage.TemplateID)]
    public partial class LandingPage : Duals.Models.Pages.ContentPage, Duals.Models.Pages.ILandingPage {
        
        #region Members
        public const string TemplateID = "{B9C4597E-57AB-42B3-887B-E63597D8832E}";
        
        public const string PrimaryNavCSS_FID = "{F8E71662-ECD7-4E78-BF58-FB3F85AC5C00}";
        
        private string _primaryNavCSS;
        #endregion
        
        #region Constructors
        public LandingPage(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Pages.LandingPage.PrimaryNavCSS_FID)]
        public virtual string PrimaryNavCSS {
            get {
                if (_primaryNavCSS == null) {
_primaryNavCSS = this.GetString(Duals.Models.Pages.LandingPage.PrimaryNavCSS_FID);
                }
                return _primaryNavCSS;
            }
            set {
                _primaryNavCSS = null;
                this.SetString(Duals.Models.Pages.LandingPage.PrimaryNavCSS_FID, value);
            }
        }
        #endregion
    }
}
