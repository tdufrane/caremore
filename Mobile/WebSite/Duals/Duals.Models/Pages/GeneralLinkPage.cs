using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Pages {
    
    
    public partial interface IGeneralLinkPage {
        
        LinkField Link {
            get;
            set;
        }
   }
    
    [Template(Duals.Models.Pages.GeneralLinkPage.TemplateID)]
    public partial class GeneralLinkPage : Duals.Models.Pages.SitePage, Duals.Models.Pages.IGeneralLinkPage {
        
        #region Members
        public const string TemplateID = "{CC2AD4E1-11A2-4E68-BBD6-E0AA36C12F5B}";

        public const string Link_FID = "{F0CEF7C6-55B9-4715-AC7C-84F4C605E7A5}";
        #endregion
        
        #region Constructors
        public GeneralLinkPage(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties

        [Field(Duals.Models.Pages.GeneralLinkPage.Link_FID)]
        public virtual LinkField Link {
            get {
                return this.GetField<LinkField>(Duals.Models.Pages.GeneralLinkPage.Link_FID);
            }
            set {
                this.SetField<LinkField>(Duals.Models.Pages.GeneralLinkPage.Link_FID, value);
            }
        }
        #endregion
    }
}
