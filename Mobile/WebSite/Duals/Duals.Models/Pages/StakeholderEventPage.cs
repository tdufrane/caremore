﻿using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Pages {
    
    
    public partial interface IStakeholderEventPage {
        
        System.Collections.Generic.IEnumerable<Sitecore.Data.Items.Item> EventType {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Pages.StakeholderEventPage.TemplateID)]
    public partial class StakeholderEventPage : CareMore.Web.DotCom.Models.Pages.EventPage, Duals.Models.Pages.IStakeholderEventPage {
        
        #region Members
        public const string TemplateID = "{4946734A-FDAC-411B-82F8-D0D129D87431}";
        
        public const string EventType_FID = "{39BF6947-F985-40F5-8EDB-EFB2688A360A}";
        
        private IEnumerable<Item> _eventType;
        #endregion
        
        #region Constructors
        public StakeholderEventPage(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Pages.StakeholderEventPage.EventType_FID)]
        public virtual System.Collections.Generic.IEnumerable<Sitecore.Data.Items.Item> EventType {
            get {
                if (_eventType == null) {
_eventType = this.GetItems(Duals.Models.Pages.StakeholderEventPage.EventType_FID);
                }
                return _eventType;
            }
            set {
                _eventType = null;
                this.SetItems(Duals.Models.Pages.StakeholderEventPage.EventType_FID, value);
            }
        }
        #endregion
    }
}