﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Sitecore.Data;

namespace Duals.Models
{
	public sealed class GlobalConsts
	{
		public const string ContentID = "{0DE95AE4-41AB-4D01-9EB0-67441B7C2450}";
		public const string DualsHomeID = "{C3852993-5F4C-4E68-B3A8-739E59962BE6}";
		public const string SiteSettingsID = "{756D461F-A8AE-4A50-98F0-8B5CD5D91873}";

		public const string CarouseSlideFolderID = "{5965BB81-8804-4168-A34C-BA257C5BC307}";
		public const string LocalizationFolderID = "{F65754A0-EE52-46B9-856C-4734DE1980AD}";
		public const string ProviderListingsFolderID = "{286CDDA0-2F47-4A31-88D3-46D117538F91}";
		public const string MemberMaterialsFolderID = "{663D0C6D-1BCF-4C50-BBCE-219739264A35}";

		public const string CountyTemplateID = "{99830985-795C-4716-9A60-B224591F4103}";

		#region Localization

		public const string DefaultCounty = "Los Angeles";
		public const string DefaultStateCode = "CA";
		public const string DefaultStateName = "California";

		#endregion

		public static ID ConvertStringToID(string id)
		{
			return new ID(id);
		}

		public static string GetPathFromStringID(string id)
		{
			return Sitecore.Context.Database.GetItem(id).Paths.Path;
		}

		public static string GetFullPathFromStringID(string id)
		{
			return Sitecore.Context.Database.GetItem(id).Paths.FullPath;
		}
	}
}
