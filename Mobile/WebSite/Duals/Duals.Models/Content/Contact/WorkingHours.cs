using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Content.Contact {
    
    
    public partial interface IWorkingHours {
    }
    
    [Template(Duals.Models.Content.Contact.WorkingHours.TemplateID)]
    public partial class WorkingHours : StandardTemplate, Duals.Models.Content.Contact.IWorkingHours, Duals.Models.Content.Contact.IContactNumber {
        
        #region Members
        public new const string TemplateID = "{B5E1447F-8041-4D51-880D-2217AEA4CD41}";
        
        private string _title;
        
        private string _content;
        #endregion
        
        #region Constructors
        public WorkingHours(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Content.Contact.ContactNumber.Title_FID)]
        public virtual string Title {
            get {
                if (_title == null) {
_title = this.GetString(Duals.Models.Content.Contact.ContactNumber.Title_FID);
                }
                return _title;
            }
            set {
                _title = null;
                this.SetString(Duals.Models.Content.Contact.ContactNumber.Title_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.ContactNumber.Content_FID)]
        public virtual string Content {
            get {
                if (_content == null) {
_content = this.GetString(Duals.Models.Content.Contact.ContactNumber.Content_FID);
                }
                return _content;
            }
            set {
                _content = null;
                this.SetString(Duals.Models.Content.Contact.ContactNumber.Content_FID, value);
            }
        }
        #endregion
    }
}
