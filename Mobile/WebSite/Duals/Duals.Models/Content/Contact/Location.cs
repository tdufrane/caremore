using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Content.Contact {
    
    
    public partial interface ILocation {
        
        string Name {
            get;
            set;
        }
        
        string AddressLine1 {
            get;
            set;
        }
        
        string AddressLine2 {
            get;
            set;
        }
        
        string City {
            get;
            set;
        }
        
        string State {
            get;
            set;
        }
        
        string PostalCode {
            get;
            set;
        }
        
        string County {
            get;
            set;
        }
        
        string Latitude {
            get;
            set;
        }
        
        string Longitude {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Content.Contact.Location.TemplateID)]
    public partial class Location : StandardTemplate, Duals.Models.Content.Contact.ILocation {
        
        #region Members
        public new const string TemplateID = "{23F50D62-9C8D-4CBC-80BC-05AB21038CBA}";
        
        public const string City_FID = "{6EA54482-ED0E-49A7-8C5D-855437BE1980}";
        
        public const string County_FID = "{D800657A-269F-46E0-9A48-A797E6F76A06}";
        
        public const string State_FID = "{E9E40A5A-6B93-43AD-8245-849584E19F6D}";
        
        public const string AddressLine1_FID = "{CAFC012F-D70C-4AF7-A3E4-F42450563D5F}";
        
        public const string AddressLine2_FID = "{C8556E0D-23A3-4450-9A88-2BC48747B2F5}";
        
        public const string Name_FID = "{A67C77F6-23D9-41CD-BDFE-5C82AA860923}";
        
        public const string Longitude_FID = "{C93F78FC-A6EE-4DDE-B3A9-4E6C1DC191E9}";
        
        public const string PostalCode_FID = "{D2DD2010-10BC-4B5D-9E58-3971B8629781}";
        
        public const string Latitude_FID = "{8E9CF757-5ECF-4E06-883B-C1C74867B90A}";
        
        private string _name;
        
        private string _addressLine1;
        
        private string _addressLine2;
        
        private string _city;
        
        private string _state;
        
        private string _postalCode;
        
        private string _county;
        
        private string _latitude;
        
        private string _longitude;
        #endregion
        
        #region Constructors
        public Location(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Content.Contact.Location.Name_FID)]
        public virtual string Name {
            get {
                if (_name == null) {
_name = this.GetString(Duals.Models.Content.Contact.Location.Name_FID);
                }
                return _name;
            }
            set {
                _name = null;
                this.SetString(Duals.Models.Content.Contact.Location.Name_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.Location.AddressLine1_FID)]
        public virtual string AddressLine1 {
            get {
                if (_addressLine1 == null) {
_addressLine1 = this.GetString(Duals.Models.Content.Contact.Location.AddressLine1_FID);
                }
                return _addressLine1;
            }
            set {
                _addressLine1 = null;
                this.SetString(Duals.Models.Content.Contact.Location.AddressLine1_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.Location.AddressLine2_FID)]
        public virtual string AddressLine2 {
            get {
                if (_addressLine2 == null) {
_addressLine2 = this.GetString(Duals.Models.Content.Contact.Location.AddressLine2_FID);
                }
                return _addressLine2;
            }
            set {
                _addressLine2 = null;
                this.SetString(Duals.Models.Content.Contact.Location.AddressLine2_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.Location.City_FID)]
        public virtual string City {
            get {
                if (_city == null) {
_city = this.GetString(Duals.Models.Content.Contact.Location.City_FID);
                }
                return _city;
            }
            set {
                _city = null;
                this.SetString(Duals.Models.Content.Contact.Location.City_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.Location.State_FID)]
        public virtual string State {
            get {
                if (_state == null) {
_state = this.GetString(Duals.Models.Content.Contact.Location.State_FID);
                }
                return _state;
            }
            set {
                _state = null;
                this.SetString(Duals.Models.Content.Contact.Location.State_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.Location.PostalCode_FID)]
        public virtual string PostalCode {
            get {
                if (_postalCode == null) {
_postalCode = this.GetString(Duals.Models.Content.Contact.Location.PostalCode_FID);
                }
                return _postalCode;
            }
            set {
                _postalCode = null;
                this.SetString(Duals.Models.Content.Contact.Location.PostalCode_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.Location.County_FID)]
        public virtual string County {
            get {
                if (_county == null) {
_county = this.GetString(Duals.Models.Content.Contact.Location.County_FID);
                }
                return _county;
            }
            set {
                _county = null;
                this.SetString(Duals.Models.Content.Contact.Location.County_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.Location.Latitude_FID)]
        public virtual string Latitude {
            get {
                if (_latitude == null) {
_latitude = this.GetString(Duals.Models.Content.Contact.Location.Latitude_FID);
                }
                return _latitude;
            }
            set {
                _latitude = null;
                this.SetString(Duals.Models.Content.Contact.Location.Latitude_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.Location.Longitude_FID)]
        public virtual string Longitude {
            get {
                if (_longitude == null) {
_longitude = this.GetString(Duals.Models.Content.Contact.Location.Longitude_FID);
                }
                return _longitude;
            }
            set {
                _longitude = null;
                this.SetString(Duals.Models.Content.Contact.Location.Longitude_FID, value);
            }
        }
        #endregion
    }
}
