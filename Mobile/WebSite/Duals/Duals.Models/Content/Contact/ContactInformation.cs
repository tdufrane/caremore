using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Content.Contact {
    
    
    public partial interface IContactInformation {
        
        string Phone {
            get;
            set;
        }
        
        string Fax {
            get;
            set;
        }
        
        Sitecore.Data.Fields.LinkField Email {
            get;
            set;
        }
        
        Sitecore.Data.Fields.LinkField Website {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Content.Contact.ContactInformation.TemplateID)]
    public partial class ContactInformation : StandardTemplate, Duals.Models.Content.Contact.IContactInformation {
        
        #region Members
        public new const string TemplateID = "{51524E8F-2A72-499E-9F7D-F387CC3499D0}";
        
        public const string Fax_FID = "{4159F049-5F38-4B9A-93D7-070AC743C3F8}";
        
        public const string Email_FID = "{2177EFA2-A3D8-4850-AF3B-5B6528A28E0E}";
        
        public const string Phone_FID = "{5586128A-238F-40ED-B7E3-CC9E65F3FF24}";
        
        public const string Website_FID = "{573D2EF9-31BE-413D-98FE-D5BF0BECE2F5}";
        
        private string _phone;
        
        private string _fax;
        #endregion
        
        #region Constructors
        public ContactInformation(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Content.Contact.ContactInformation.Phone_FID)]
        public virtual string Phone {
            get {
                if (_phone == null) {
_phone = this.GetString(Duals.Models.Content.Contact.ContactInformation.Phone_FID);
                }
                return _phone;
            }
            set {
                _phone = null;
                this.SetString(Duals.Models.Content.Contact.ContactInformation.Phone_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.ContactInformation.Fax_FID)]
        public virtual string Fax {
            get {
                if (_fax == null) {
_fax = this.GetString(Duals.Models.Content.Contact.ContactInformation.Fax_FID);
                }
                return _fax;
            }
            set {
                _fax = null;
                this.SetString(Duals.Models.Content.Contact.ContactInformation.Fax_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.ContactInformation.Email_FID)]
        public virtual Sitecore.Data.Fields.LinkField Email {
            get {
                return this.GetField<LinkField>(Duals.Models.Content.Contact.ContactInformation.Email_FID);
            }
            set {
                this.SetField<LinkField>(Duals.Models.Content.Contact.ContactInformation.Email_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.ContactInformation.Website_FID)]
        public virtual Sitecore.Data.Fields.LinkField Website {
            get {
                return this.GetField<LinkField>(Duals.Models.Content.Contact.ContactInformation.Website_FID);
            }
            set {
                this.SetField<LinkField>(Duals.Models.Content.Contact.ContactInformation.Website_FID, value);
            }
        }
        #endregion
    }
}
