﻿using System;
using System.Collections.Generic;
using System.IO;

using CareMore.Web.DotCom.Models;
using ClassySC.Data;

using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;


namespace Duals.Models.Content.Articles
{
	public partial interface ProviderIListArticle
	{
		Item ProviderListType { get; set; }
	}

	[Template(Duals.Models.Content.Articles.ProviderListArticle.TemplateID)]
	public partial class ProviderListArticle : StandardTemplate, Duals.Models.Content.Articles.ProviderIListArticle, Duals.Models.Content.Articles.IListArticle, Duals.Models.Content.Articles.IBaseArticle, Duals.Models.Base.IPage
	{
		#region Members

		public const string TemplateID = "{756270BB-8304-437A-9B60-FC40EA6A50E8}";
		public const string ProviderListType_FID = "{2E8A71FE-C204-4983-ACE1-EB37D919F696}";

		// IPage
		private bool? _excludeFromNavigation;
		private string _pageTitle;
		private bool? _showInSiteMap;
		private string _navigationTitle;
		private bool? _dontCollaspeChildren;

		// IBaseArticle
		private bool? _displayLeftMenu;

		// IListArticle
		private string _headerTitle;
		private string _header1;
		private string _header2;
		private string _header3;
		private string _header4;

		#endregion

		#region Constructors

		public ProviderListArticle(Item innerItem) : base(innerItem)
		{
		}

		#endregion

		#region Properties

		[Field(Duals.Models.Content.Articles.ProviderListArticle.ProviderListType_FID)]
		public virtual Item ProviderListType
		{
			get
			{
				return this.GetItem(Duals.Models.Content.Articles.ProviderListArticle.ProviderListType_FID);
			}
			set
			{
				this.SetItem(Duals.Models.Content.Articles.ProviderListArticle.ProviderListType_FID, value);
			}
		}

		#endregion

		#region IPage Properties

		[Field(Duals.Models.Base.Page.ExcludeFromNavigation_FID)]
		public virtual bool? ExcludeFromNavigation
		{
			get
			{
				if (_excludeFromNavigation == null)
				{
					_excludeFromNavigation = this.GetBool(Duals.Models.Base.Page.ExcludeFromNavigation_FID);
				}
				return _excludeFromNavigation;
			}
			set
			{
				_excludeFromNavigation = null;
				this.SetBool(Duals.Models.Base.Page.ExcludeFromNavigation_FID, value);
			}
		}

		[Field(Duals.Models.Base.Page.PageTitle_FID)]
		public virtual string PageTitle
		{
			get
			{
				if (_pageTitle == null)
				{
					_pageTitle = this.GetString(Duals.Models.Base.Page.PageTitle_FID);
				}
				return _pageTitle;
			}
			set
			{
				_pageTitle = null;
				this.SetString(Duals.Models.Base.Page.PageTitle_FID, value);
			}
		}

		[Field(Duals.Models.Base.Page.ShowInSiteMap_FID)]
		public virtual bool? ShowInSiteMap
		{
			get
			{
				if (_showInSiteMap == null)
				{
					_showInSiteMap = this.GetBool(Duals.Models.Base.Page.ShowInSiteMap_FID);
				}
				return _showInSiteMap;
			}
			set
			{
				_showInSiteMap = null;
				this.SetBool(Duals.Models.Base.Page.ShowInSiteMap_FID, value);
			}
		}

		[Field(Duals.Models.Base.Page.NavigationTitle_FID)]
		public virtual string NavigationTitle
		{
			get
			{
				if (_navigationTitle == null)
				{
					_navigationTitle = this.GetString(Duals.Models.Base.Page.NavigationTitle_FID);
				}
				return _navigationTitle;
			}
			set
			{
				_navigationTitle = null;
				this.SetString(Duals.Models.Base.Page.NavigationTitle_FID, value);
			}
		}

		[Field(Duals.Models.Base.Page.DontCollaspeChildren_FID)]
		public virtual bool? DontCollaspeChildren
		{
			get
			{
				if (_dontCollaspeChildren == null)
				{
					_dontCollaspeChildren = this.GetBool(Duals.Models.Base.Page.DontCollaspeChildren_FID);
				}
				return _dontCollaspeChildren;
			}
			set
			{
				_dontCollaspeChildren = null;
				this.SetBool(Duals.Models.Base.Page.DontCollaspeChildren_FID, value);
			}
		}

		#endregion

		#region IBaseArticle Properties

		[Field(Duals.Models.Content.Articles.BaseArticle.DisplayLeftMenu_FID)]
		public virtual bool? DisplayLeftMenu
		{
			get
			{
				if (_displayLeftMenu == null)
				{
					_displayLeftMenu = this.GetBool(Duals.Models.Content.Articles.BaseArticle.DisplayLeftMenu_FID);
				}
				return _displayLeftMenu;
			}
			set
			{
				_displayLeftMenu = null;
				this.SetBool(Duals.Models.Content.Articles.BaseArticle.DisplayLeftMenu_FID, value);
			}
		}

		[Field(Duals.Models.Content.Articles.BaseArticle.Url_FID)]
		public virtual LinkField Url
		{
			get
			{
				return this.GetField<LinkField>(Duals.Models.Content.Articles.BaseArticle.Url_FID);
			}
			set
			{
				this.SetField<LinkField>(Duals.Models.Content.Articles.BaseArticle.Url_FID, value);
			}
		}

		#endregion

		#region IListArticle Properties

		[Field(Duals.Models.Content.Articles.ListArticle.ListingSource_FID)]
		public virtual InternalLinkField ListingSource
		{
			get
			{
				return this.GetField<InternalLinkField>(Duals.Models.Content.Articles.ListArticle.ListingSource_FID);
			}
			set
			{
				this.SetField<InternalLinkField>(Duals.Models.Content.Articles.ListArticle.ListingSource_FID, value);
			}
		}

		[Field(Duals.Models.Content.Articles.ListArticle.HeaderTitle_FID)]
		public virtual string HeaderTitle
		{
			get
			{
				if (_headerTitle == null)
				{
					_headerTitle = this.GetString(Duals.Models.Content.Articles.ListArticle.HeaderTitle_FID);
				}
				return _headerTitle;
			}
			set
			{
				_headerTitle = null;
				this.SetString(Duals.Models.Content.Articles.ListArticle.HeaderTitle_FID, value);
			}
		}

		[Field(Duals.Models.Content.Articles.ListArticle.Header1_FID)]
		public virtual string Header1
		{
			get
			{
				if (_header1 == null)
				{
					_header1 = this.GetString(Duals.Models.Content.Articles.ListArticle.Header1_FID);
				}
				return _header1;
			}
			set
			{
				_header1 = null;
				this.SetString(Duals.Models.Content.Articles.ListArticle.Header1_FID, value);
			}
		}

		[Field(Duals.Models.Content.Articles.ListArticle.Header2_FID)]
		public virtual string Header2
		{
			get
			{
				if (_header2 == null)
				{
					_header2 = this.GetString(Duals.Models.Content.Articles.ListArticle.Header2_FID);
				}
				return _header2;
			}
			set
			{
				_header2 = null;
				this.SetString(Duals.Models.Content.Articles.ListArticle.Header2_FID, value);
			}
		}

		[Field(Duals.Models.Content.Articles.ListArticle.Header3_FID)]
		public virtual string Header3
		{
			get
			{
				if (_header3 == null)
				{
					_header3 = this.GetString(Duals.Models.Content.Articles.ListArticle.Header3_FID);
				}
				return _header3;
			}
			set
			{
				_header3 = null;
				this.SetString(Duals.Models.Content.Articles.ListArticle.Header3_FID, value);
			}
		}

		[Field(Duals.Models.Content.Articles.ListArticle.Header4_FID)]
		public virtual string Header4
		{
			get
			{
				if (_header4 == null)
				{
					_header4 = this.GetString(Duals.Models.Content.Articles.ListArticle.Header4_FID);
				}
				return _header4;
			}
			set
			{
				_header4 = null;
				this.SetString(Duals.Models.Content.Articles.ListArticle.Header4_FID, value);
			}
		}

		#endregion

		#region IProviderListing Properties
/*
		[Field(CareMore.Web.DotCom.Models.ProviderListing.ClassCode_FID)]
		public virtual string ClassCode
		{
			get
			{
				if (_classCode == null) { _classCode = this.GetString(CareMore.Web.DotCom.Models.ProviderListing.ClassCode_FID); }
				return _classCode;
			}
			set
			{
				_classCode = null; this.SetString(CareMore.Web.DotCom.Models.ProviderListing.ClassCode_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ProviderListing.DataSource_FID)]
		public virtual Item DataSource
		{
			get
			{
				return this.GetItem(CareMore.Web.DotCom.Models.ProviderListing.DataSource_FID);
			}
			set
			{
				this.SetItem(CareMore.Web.DotCom.Models.ProviderListing.DataSource_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ProviderListing.HideFirstColumn_FID)]
		public virtual bool? HideFirstColumn
		{
			get
			{
				if (_hideFirstColumn == null)
				{
					_hideFirstColumn = this.GetBool(CareMore.Web.DotCom.Models.ProviderListing.HideFirstColumn_FID);
				}
				return _hideFirstColumn;
			}
			set
			{
				_hideFirstColumn = null; this.SetBool(CareMore.Web.DotCom.Models.ProviderListing.HideFirstColumn_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ProviderListing.HideListing_FID)]
		public virtual bool? HideListing
		{
			get
			{
				if (_hideListing == null)
				{
					_hideListing = this.GetBool(CareMore.Web.DotCom.Models.ProviderListing.HideListing_FID);
				}
				return _hideListing;
			}
			set
			{
				_hideListing = null; this.SetBool(CareMore.Web.DotCom.Models.ProviderListing.HideListing_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ProviderListing.SitecorePath_FID)]
		public virtual Sitecore.Data.Fields.InternalLinkField SitecorePath
		{
			get
			{
				return this.GetField<InternalLinkField>(CareMore.Web.DotCom.Models.ProviderListing.SitecorePath_FID);
			}
			set
			{
				this.SetField<InternalLinkField>(CareMore.Web.DotCom.Models.ProviderListing.SitecorePath_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ProviderListing.KeyIcon_FID)]
		public virtual ImageField KeyIcon
		{
			get
			{
				return this.GetField<ImageField>(CareMore.Web.DotCom.Models.ProviderListing.KeyIcon_FID);
			}
			set
			{
				this.SetField<ImageField>(CareMore.Web.DotCom.Models.ProviderListing.KeyIcon_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ProviderListing.MapIcon_FID)]
		public virtual ImageField MapIcon
		{
			get
			{
				return this.GetField<ImageField>(CareMore.Web.DotCom.Models.ProviderListing.MapIcon_FID);
			}
			set
			{
				this.SetField<ImageField>(CareMore.Web.DotCom.Models.ProviderListing.MapIcon_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ProviderListing.ProviderType_FID)]
		public virtual Item ProviderType
		{
			get
			{
				return this.GetItem(CareMore.Web.DotCom.Models.ProviderListing.ProviderType_FID);
			}
			set
			{
				this.SetItem(CareMore.Web.DotCom.Models.ProviderListing.ProviderType_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ProviderListing.SpecialtyFilter_FID)]
		public virtual IEnumerable<Item> SpecialtyFilter
		{
			get
			{
				if (_specialtyFilter == null)
				{
					_specialtyFilter = this.GetItems(CareMore.Web.DotCom.Models.ProviderListing.SpecialtyFilter_FID);
				}
				return _specialtyFilter;
			}
			set
			{
				_specialtyFilter = null;
				this.SetItems(CareMore.Web.DotCom.Models.ProviderListing.SpecialtyFilter_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ProviderListing.Title_FID)]
		public virtual string Title
		{
			get
			{
				if (_title == null) { _title = this.GetString(CareMore.Web.DotCom.Models.ProviderListing.Title_FID); }
				return _title;
			}
			set
			{
				_title = null; this.SetString(CareMore.Web.DotCom.Models.ProviderListing.Title_FID, value);
			}
		}
*/
		#endregion
	}
}
