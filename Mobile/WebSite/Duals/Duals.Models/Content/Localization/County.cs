using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Content.Localization {
    
    
    public partial interface ICounty {
        
        string Name {
            get;
            set;
        }
        
        string Zipcodes {
            get;
            set;
        }
        
        string StyleSheetFile {
            get;
            set;
        }
        
        string HeaderImagePath {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Content.Localization.County.TemplateID)]
    public partial class County : StandardTemplate, Duals.Models.Content.Localization.ICounty {
        
        #region Members
        public const string TemplateID = "{99830985-795C-4716-9A60-B224591F4103}";
        
        public const string Zipcodes_FID = "{DA93A8D6-9A8C-4FA6-BE49-B1C119260F35}";
        
        public const string StyleSheetFile_FID = "{A2DE0AA0-9487-4D13-B521-3805142F433B}";
        
        public const string HeaderImagePath_FID = "{86F5E7B0-3356-40D8-B867-CBC8AD638D78}";
        
        public const string Name_FID = "{D4E578E6-A42E-4453-9EB3-192075CC70A1}";
        
        private string _name;
        
        private string _zipcodes;
        
        private string _styleSheetFile;
        
        private string _headerImagePath;
        #endregion
        
        #region Constructors
        public County(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Content.Localization.County.Name_FID)]
        public virtual string Name {
            get {
                if (_name == null) {
_name = this.GetString(Duals.Models.Content.Localization.County.Name_FID);
                }
                return _name;
            }
            set {
                _name = null;
                this.SetString(Duals.Models.Content.Localization.County.Name_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Localization.County.Zipcodes_FID)]
        public virtual string Zipcodes {
            get {
                if (_zipcodes == null) {
_zipcodes = this.GetString(Duals.Models.Content.Localization.County.Zipcodes_FID);
                }
                return _zipcodes;
            }
            set {
                _zipcodes = null;
                this.SetString(Duals.Models.Content.Localization.County.Zipcodes_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Localization.County.StyleSheetFile_FID)]
        public virtual string StyleSheetFile {
            get {
                if (_styleSheetFile == null) {
_styleSheetFile = this.GetString(Duals.Models.Content.Localization.County.StyleSheetFile_FID);
                }
                return _styleSheetFile;
            }
            set {
                _styleSheetFile = null;
                this.SetString(Duals.Models.Content.Localization.County.StyleSheetFile_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Localization.County.HeaderImagePath_FID)]
        public virtual string HeaderImagePath {
            get {
                if (_headerImagePath == null) {
_headerImagePath = this.GetString(Duals.Models.Content.Localization.County.HeaderImagePath_FID);
                }
                return _headerImagePath;
            }
            set {
                _headerImagePath = null;
                this.SetString(Duals.Models.Content.Localization.County.HeaderImagePath_FID, value);
            }
        }
        #endregion
    }
}
