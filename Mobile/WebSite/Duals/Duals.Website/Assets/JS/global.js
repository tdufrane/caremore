﻿$j = $.noConflict();
$j(document).ready(function () {
	$j('a').click(function () {
		href = $j(this).attr("href");

		if (href.charAt(0) == '#') {
			tagName = href.replace("#", "");

			if ($j("a[name=" + tagName + "]").length > 0) {
				tagTop = $j("a[name=" + tagName + "]").offset().top;
				$j('html, body').animate({ scrollTop: tagTop }, 'slow');

				return false;
			}
		}
	});

	// ensure all links leading to 3rd party sites show confirm dialog
	var domain = document.domain;
	var subdomain;
	var periodIndex = domain.indexOf('.', 0);
	if (periodIndex == -1) {
		subdomain = domain;
	}
	else {
		subdomain = domain.substr(periodIndex);
	}
	$j('a[href^="http"]:not([href*="' + subdomain + '"])').click(function () {
		return confirm("You are now leaving the CareMore Cal MediConnect site to enter a third party site.");
	});

	// set font-size
	var fontSize = readCookie('font-size');
	if (fontSize != null) {
		setFontSize(fontSize);
	}
});

function increaseFontSize() {
	var fontSize_p_ul = $j('html').css('font-size');

	var fontSize;
	if (fontSize_p_ul == '13px') {
		fontSize = 'medium';
	} else if (fontSize_p_ul == '16px') {
		fontSize = 'large';
	} else {
		return;
	}

	setFontSize(fontSize);
	createCookie('font-size', fontSize, 30);
}

function decreaseFontSize() {
	var fontSize_p_ul = $j('html').css('font-size');

	var fontSize;
	if (fontSize_p_ul == '19px') {
		fontSize = 'medium';
	} else if (fontSize_p_ul == '16px') {
		fontSize = 'small';
	} else {
		return;
	}

	setFontSize(fontSize);
	createCookie('font-size', fontSize, 30);
}

function setFontSize(size) {
	console.log($j('html').css('font-size'));
	if (size == 'large') {
		$j('link[title=large]')[0].disabled=false;
		$j('html').css('font-size', '19px');
	} else if (size == 'small') {
		$j('link[title=large]')[0].disabled=true;
		$j('html').css('font-size', '13px');
	} else {
		$j('link[title=large]')[0].disabled=true;
		$j('html').css('font-size', '16px');
	}
}

var enrollErrorLevel = 0;

function adjustHeights() {
    adjustCalloutHeight('.autoSize');
    adjustCalloutHeight('.autoSizeCenter');
    adjustCalloutHeight('.autoSizeBox');
    adjustCalloutHeight('.boundingBox');

    adjustCalloutHeight('.planLanding .gridItem h3');
    adjustCalloutHeight('.planLanding .gridItem');
    adjustCalloutHeight('.planLanding .gridItem .txt');
    adjustCalloutHeight('#homePage .gridItem .promo .txt p');

    autoHeight('#homePage .gridCol .gridColDouble .gridItem .txt p');
}

function autoHeight(target) {
    $j(target).each(function () {
        $j(this).attr('style', 'height:auto;');
    });
}

function adjustCalloutHeight(target) {
    var boxHeights = new Array();

    // Gather the heights of the shopping boxes
    $j(target).each(function () {
        $j(this).attr('style', 'height:auto;');
        boxHeights.push($j(this).height());
    });

    function sortArray(valA, valB) {
        return (valB - valA);
    }
    // Sort the heights in ascending order
    boxHeights.sort(sortArray);

    // Set the height of all three shopping boxes to the value of the tallest box
    $j(target).each(function () {
        $j(this).height(boxHeights[0]);
    });

    return true;
}

//create cookie
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

//read cookie
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

//Check what font size needs to persist
function FontSizePersistence() {
    var cookie = readCookie("style");
    if (cookie == "large") {
        $j('body').addClass('large');
    }
    else {
    $j('body').removeClass('large');
    }
}

function validateEnroll(errorLevel) {
    var result = true;

    if (errorLevel > enrollErrorLevel) {
        enrollErrorLevel = errorLevel;
    }

    $j(".errorMessage").hide();

    if (enrollErrorLevel > 0) {
        if ($j('.drpCountySel').val() == "") {
            $j(".countyErrorMessage").show();
            result = false;
        }
        else if ($j('.drpPlanSel').val() == "") {
            $j(".planErrorMessage").show();
            result = false;
        }
    }

    return result;
}

function applyStyle() {
    $j('select:not(.noUniform)').uniform();
    $j('select:not(.noUniform)').addClass('noUniform');
}

function loadEnroll(sender, args) {
    validateEnroll(0);
}

function setEqualHeight(items) {
    var tallestItem = 0;
    items.each(
       function () {
           currentHeight = $j(this).height();
           if (currentHeight > tallestItem) {
               tallestItem = currentHeight;
           }
       }
    );
    items.height(tallestItem);
}
function setFindPlanQueryStringFilter(id) {
    var planFilter = getParameterByName('plan_filter');
    var idIndex = planFilter.indexOf(id);
    var planFilterItems = planFilter.split('|');
    if (idIndex != -1) {
        planFilterItems.splice(idIndex, 1);
    } else {
        planFilter.length > 0 ? planFilterItems.push(id) : planFilterItems = new Array(id);
    }

    var url = location.href.split('?')[0];

    window.location = url + '?plan_filter=' + planFilterItems.join('|');
}
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
}

var players = new Array();

function playVideo(controlId, imagePath, videoPath, width, height) {
    var curPlayer = jwplayer(controlId).setup({
        'flashplayer': '/flash/player5.swf',
        'image': imagePath,
        'file': videoPath,
        'controlbar': 'over',
        'controlbar.idlehide': 'true',
        'fullscreen': 'true',
        'stretching': 'fill',
        'skin': '/flash/skewd.zip',
        'width': width,
        'height': height,
        events: {
            onPlay: function (event) {
                //stop other videos to play
                for (var i = 0; i < players.length; i++) {
                    var curPlayer = players[i];

                    if (curPlayer != this && curPlayer.getState() == "PLAYING") {
                        curPlayer.pause();
                    }
                }
            }
        }
    });

    players.push(curPlayer);
}

// Google Map on Region Google Map with services
var geocoder;
var map;
var infoWindow;
var firstLocation;

var alertMessage = "You are now leaving CareMore.com to enter a third party site.";

function gMapInitialize(centerOnPoint) {
	geocoder = new google.maps.Geocoder();
	infoWindow = new google.maps.InfoWindow();
	markers = new Array();

	var myOptions = {
		zoom: 10,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	map = new google.maps.Map(document.getElementById("map"), myOptions);

	for (var i = 0; i < locations.length; i++) {
		var item = locations[i].split('|');
		var address = item[0];
		var facilityName = item[1];
		var facilityTypeImage = item[2];
		var facilityTypeName = item[3];
		var facilityPhone = item[4];
		var itemUrl = item[5];

		geoCodeAddress(address, facilityName, facilityTypeImage, facilityTypeName, facilityPhone, itemUrl);
	}

	// center map after x time using first location found
	if (centerOnPoint) {
		setTimeout(function () {
			map.setCenter(firstLocation);
		}, 2000);
	}
}

function saveGeoLocation_OnSucceeded(result) {
	// geo location successfully cached
}

function saveGeoLocation_OnFailed(result) {
	// get location failed to save
}

function getGeoLocation_OnFailed(result) {
	// cannot retrieve geo location
}

function geoCodeAddress(address, facilityName, facilityTypeImage, facilityTypeName, facilityPhone, itemUrl) {
    PageMethod('/services/geocode.asmx/GetGeoLocation',
        "{ 'address': '" + address + "' }",
        function (location) { // success
            if (location.d != undefined && location.d != 'null') {
                var lat = parseFloat(location.d.split(',')[0]);
                var lng = parseFloat(location.d.split(',')[1]);
                setMarker(address, facilityName, facilityTypeImage, facilityTypeName, facilityPhone, null, lat, lng, itemUrl);
            } else {
                geocoder.geocode({ 'address': address }, function (results, status) {
                	if (status == google.maps.GeocoderStatus.OK) {
                        setMarker(address, facilityName, facilityTypeImage, facilityTypeName, facilityPhone, results[0].geometry.location, null, null, itemUrl);
                        PageMethod('/services/geocode.asmx/SaveGeoLocation',
                            "{ 'address': '" + address + "', 'location': '" + results[0].geometry.location + "' }",
                            saveGeoLocation_OnSucceeded,
                            saveGeoLocation_OnFailed);
                    } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                        setTimeout(function () {
                            geoCodeAddress(address, facilityName, facilityTypeImage, facilityTypeName, facilityPhone, itemUrl);
                        }, 1000);
                    } else {
                        console.warn("Geocode was not successful for " + address + " the following reason: " + status);
                    }
                });
            }
        },
        getGeoLocation_OnFailed,
        function () {
            // completed
        });
                   }

function setMarker(address, facilityName, facilityTypeImage, facilityTypeName, facilityPhone, loc, lat, lng, itemUrl) {
	var location;
	if (loc != undefined) {
		location = loc;
	} else {
		location = new google.maps.LatLng(lat, lng);
	}
	if (firstLocation == null) firstLocation = location;

	if (facilityTypeImage == "#") {
    	var marker = new google.maps.Marker({
    		map: map,
    		title: address,
    		animation: google.maps.Animation.DROP,
    		position: location
    	});
	}
	else {
    	var marker = new google.maps.Marker({
    		map: map,
    		title: address,
    		animation: google.maps.Animation.DROP,
    		position: location,
    		icon: facilityTypeImage
    	});
	}

	marker.setMap(map);
	google.maps.event.addListener(marker, 'click', function () {
		infoWindow.close();
		if (itemUrl != null && itemUrl != "") {
			infoWindow.setContent("<div style=\"margin-bottom:2px;\"><strong>" + facilityName +
				"</strong><div style=\"color:#666;\">" + facilityTypeName +
				"</div></div><div>" + address + "<br>" + facilityPhone + "</div><div><p style='float:left;'><a href='" + itemUrl + "'>See Details</a></p><p style='float:right;'><a href='http://maps.google.com/maps?saddr=&daddr=" + encodeURIComponent(address) + "' onclick='if (confirm(\"" + alertMessage + "\")) window.open(this.href, \"_blank\"); return false;'>Get Directions</a></div>");
		}
		else {
			infoWindow.setContent("<div style=\"margin-bottom:2px;\"><strong>" + facilityName +
			"</strong><div style=\"color:#666;\">" + facilityTypeName +
			"</div></div><div>" + address + "<br>" + facilityPhone + "</div><div><a href='http://maps.google.com/maps?saddr=&daddr=" + encodeURIComponent(address) + "' onclick='if (confirm(\"" + alertMessage + "\")) window.open(this.href, \"_blank\"); return false;'>Get Directions</a></div>");
		}
		infoWindow.open(map, marker);
	});
}

function setCenter(center) {
	if (center == "") {
		setTimeout(function () {
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function (position) {
				initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
						map.setCenter(initialLocation);
					});
				}
			}, 2500);
	}
	else {
		geocoder.geocode({ 'address': center }, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				map.setCenter(results[0].geometry.location);
			} else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
				setTimeout(function () {
					map.setCenter(results[0].geometry.location);
				}, 2500);
			} else {
				console.warn("Geocode was not successful for " + center + " the following reason: " + status);
			}
		});
	}
}
//-----------------------------------------------------------------------------+
// jQuery call AJAX Page Method                                                |
//-----------------------------------------------------------------------------+
function PageMethod(url, data, successFn, errorFn, complete) {
    //Call the page method
    $j.ajax({
        type: "POST",
        url: url,
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: successFn,
        error: errorFn,
        complete: complete
    });
}
