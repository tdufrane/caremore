﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GoogleMap.ascx.cs" Inherits="Duals.Website.Controls.GoogleMap" %>

<asp:PlaceHolder ID="phGoogleMap" runat="server" Visible="true">
	<div id="mapFrame">
		<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=<%= GoogleMapID %>&sensor=true"></script>
		<asp:Literal runat="server" ID="litLocationsArray" />
		<asp:Literal runat="server" ID="litInitializeMap" />
		<div id="map"></div>
	</div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phMapKeys" runat="server" Visible="false">
	<div class="boxNoBack">
		<div class="mastheadText" style="font-weight: bold; padding-top: 10px; padding-bottom: 10px;">KEY</div>

		<asp:ListView runat="server" ID="lvMapKeyLeft">
			<ItemTemplate>
				<div class="mapKeyItem">
					<div class="mapKeyIcon"><img src='<%# Eval("ImageUrl") %>' alt='<%# Eval("Text") %>' /></div>
					<div class="mapKeyTitle"><%# Eval("Text") %></div>
				</div>
			</ItemTemplate>
		</asp:ListView>
	</div><!-- end boxNoBack -->
</asp:PlaceHolder>
