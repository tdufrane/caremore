﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BaseContentPage.ascx.cs" Inherits="Duals.Website.Controls.BaseContentPage" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<asp:Panel ID="pnlContent" runat="server">
	<div class="masthead">
	<asp:PlaceHolder runat="server" ID="phWithImage" Visible="false">
		<div runat="server" id="imgIntro" class="img-intro">
			<h1><sc:Text runat="server" Field="Masthead Title" /></h1>
			<p><sc:Text runat="server" Field="Masthead Description" /></p>
		</div>
		<div class="bgImage">
			<sc:Image runat="server" ID="scImage" />
		</div>
	</asp:Placeholder>
	<asp:PlaceHolder runat="server" ID="phWithoutImage" Visible="false">
		<h1><sc:Text runat="server" Field="Masthead Title" /></h1>
		<p><sc:Text runat="server" Field="Masthead Description" /></p>
	</asp:Placeholder>
	</div>
	<sc:Text runat="server" Field="Page Content" />
</asp:Panel>