﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI.WebControls;

using ClassySC.Data;
using Duals.Models;
using Duals.Models.Content.Facilities;

using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Web.UI.WebControls;
using System.Text;


namespace Duals.Website.Controls
{
	public partial class GoogleMap : System.Web.UI.UserControl
	{
		public StringBuilder MapSettings = new StringBuilder();
		public bool CenterOnPoint = true;
		public string LocationsArrayString = string.Empty;
		public string GoogleMapID { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			SetThirdPartyLinkMessage();
			GoogleMapID = ConfigurationManager.AppSettings["GoogleMapsAPIKey"];
		}

		private void SetThirdPartyLinkMessage()
		{
			MapSettings.Append(string.Format("alertMessage = \"{0}\";\n", Sitecore.Context.Database.GetItem(GlobalConsts.SiteSettingsID).ToClass<Models.Settings.SiteSettings>().ThirdPartyLinksMessage));
		}

		public void SetZoomLevel(byte zoom)
		{
			MapSettings.Append(string.Format("map.setZoom({0});\n", zoom));
		}

		public void SetCenter(string center = "")
		{
			CenterOnPoint = false;
			MapSettings.Append(string.Format("setCenter(\"{0}\");\n", center));
		}

		public void SetLocationsArray(string locationAddresses)
		{
			litLocationsArray.Text = string.Format("<script type=\"text/javascript\">var locations = new Array({0});</script>", locationAddresses);
		}

		public void DisplayMap()
		{
			litInitializeMap.Text = string.Format("<script type=\"text/javascript\">$j(function () {{gMapInitialize({0}); {1}}});</script>", CenterOnPoint.ToString().ToLower(), MapSettings.ToString());
		}

		public void DisplayMapKey(List<GoogleMapKey> keys)
		{
			if ((keys != null) && (keys.Count > 0))
			{
				lvMapKeyLeft.DataSource = keys;
				lvMapKeyLeft.DataBind();
				phMapKeys.Visible = true;
			}
		}
	}

	public class GoogleMapKey
	{
		public string ImageUrl { get; set; }
		public string Text { get; set; }

		public GoogleMapKey() { }

		public GoogleMapKey(string imageUrl, string text)
		{
			ImageUrl = imageUrl;
			Text = text;
		}
	}
}
