﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Base;
using Duals.Models.Pages;

namespace Duals.Website.Controls
{
	public class BasePage : System.Web.UI.Page
	{
		/// <summary>
		/// CurrentPage Duals.Models.Base.IPage class
		/// </summary>
		public Duals.Models.Base.IPage CurrentPage
		{
			get
			{
				return Sitecore.Context.Item.ToClass<Duals.Models.Base.Page>();
			}
		}

		/// <summary>
		/// Browser Page Title
		/// 1st try to get PageTitle for the current item and if empty get Global Page Title
		/// </summary>
		public string PageTitle
		{
			get
			{
				if (CurrentPage != null && !string.IsNullOrEmpty(CurrentPage.PageTitle))
				{
					return CurrentPage.PageTitle;
				}
				else
				{
					Homepage hp = Sitecore.Context.Database.GetItem(GlobalConsts.DualsHomeID).ToClass<Homepage>();

					return hp.PageTitle;
				}
			}
		}

		/// <summary>
		/// Third Party Links Message
		/// </summary>
		public string ThirdPartyLinksMessage
		{
			get
			{
				Duals.Models.Settings.SiteSettings ss = Sitecore.Context.Database.GetItem(GlobalConsts.SiteSettingsID).ToClass<Duals.Models.Settings.SiteSettings>();

				return ss.ThirdPartyLinksMessage;
			}
		}
	}
}