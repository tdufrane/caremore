﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Pages;
using Duals.Models.Content.Articles;
using Duals.Models.Content.Contact;
using Duals.Models.Content.Facilities;

using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Comparers;
using Sitecore.Links;
using Sitecore.Resources.Media;
using Sitecore.Web.UI.WebControls;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class EventDetails : System.Web.UI.UserControl
	{
		private DateTime _selectedDate = DateTime.MinValue;

		public string GoogleMapID { get; set; }
		public string LocationAddress { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			BindEventDetails();
		}

		private void BindEventDetails()
		{
			GoogleMapID = ConfigurationManager.AppSettings["GoogleMapsAPIKey"];

			var selectedEvent = Sitecore.Context.Item.ToClass<CareMore.Web.DotCom.Models.Pages.EventPage>();
			// Event Date
			DateTime? date = selectedEvent.Date;

			// set location address used to drop pin on the map
			LocationAddress = selectedEvent.LocationAddress;

			litSelectedDate.Text = date.HasValue ? date.Value.ToLongDateString() : "N/A";
			litSelectedTime.Text = date.HasValue ? date.Value.ToShortTimeString() : "";

			// Get Directions click
			btnGetDirections.Attributes.Add("onclick",
					string.Format("if (confirm('You are now leaving CareMore.com to enter a third party site.')) window.open('{0}', '_blank');",
						string.Format("http://maps.google.com/maps?q=to:{0}", 
						HttpContext.Current.Server.UrlEncode(LocationAddress))));
		}
	}
}