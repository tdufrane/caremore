﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Pages;

using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class SiteNav : Controls.BaseSublayout
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			BuildSiteNav();
		}

		/// <summary>
		/// Get Duals home item Childern and generate SiteNav using LandingPage items
		/// </summary>
		private void BuildSiteNav()
		{
			try
			{
				Homepage homepage = Sitecore.Context.Database.GetItem(GlobalConsts.DualsHomeID).ToClass<Homepage>();

				siteNavLV.DataSource = homepage.InnerItem.Children.ToList()    // get all children of Duals home node
					.ConvertAll(item => item.ToClass<LandingPage>())    // convert to LandingPage class
					.Where(page => page != null && (page.ExcludeFromNavigation.HasValue ? !page.ExcludeFromNavigation.Value : true));   // exclude null and not included in nav
				siteNavLV.DataBind();

				// Set Home link
				HyperLink hlHome = (HyperLink)siteNavLV.FindControl("hlHome");
				hlHome.Text = homepage.NavigationTitle;
				hlHome.NavigateUrl = LinkManager.GetItemUrl(homepage.InnerItem);

				HtmlGenericControl li = (HtmlGenericControl)siteNavLV.FindControl("liHome");
				li.Attributes["class"] = homepage.ID != Sitecore.Context.Item.ID ? "home" : "home selected";
			}
			catch (Exception exc)
			{
				Sitecore.Diagnostics.Log.Error("Duals: Cannot build SiteNav", exc, this);
			}
		}

		protected void siteNavLV_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				ListViewDataItem LVDI = (ListViewDataItem)e.Item;
				LandingPage landingPage = (LandingPage)LVDI.DataItem;

				Item curItem = Sitecore.Context.Item;
				while (curItem.ToClass<LandingPage>() == null && curItem.ToClass<Homepage>() == null)
				{
					curItem = curItem.Parent;
				}

				HtmlGenericControl li = (HtmlGenericControl)e.Item.FindControl("li");
				li.Attributes["class"] = ((curItem.ToClass<LandingPage>() != null) && (landingPage.ID == curItem.ToClass<LandingPage>().ID)) ? string.Format("{0} selected", landingPage.PrimaryNavCSS) : landingPage.PrimaryNavCSS;

				HyperLink hl = (HyperLink)e.Item.FindControl("hl");
				hl.Text = landingPage.NavigationTitle;
				hl.NavigateUrl = LinkManager.GetItemUrl(landingPage.InnerItem);
			}
		}
	}
}