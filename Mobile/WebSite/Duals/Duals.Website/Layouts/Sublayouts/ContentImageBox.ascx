﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentImageBox.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.ContentImageBox" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="span4">
	<div class="box">
		<h2><sc:Text ID="scTitle" runat="server" Field="Title" /></h2>
		<sc:Link ID="scLink1" runat="server" Field="Link"><sc:Image ID="scImage" runat="server" Field="Image" /></sc:Link>
		<sc:Text ID="scContent" runat="server" Field="Content" />
		<p><sc:Link ID="scLink2" runat="server" Field="Link" /></p>
		<asp:Panel ID="pnlSpacer" runat="server" Visible="false"></asp:Panel>
	</div>
</div>
