﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class ProviderLoginWidget : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		private Literal ProviderControl
		{
			get
			{
				return (Literal)Page.FindControl("provider");
			}
		}

		private void CreateFormSubmit()
		{
			StringBuilder formString = new StringBuilder();

			formString.AppendFormat("<form method=\"post\" name=\"form1\" id=\"form1\" action=\"{0}\" style=\"display:none;\">",
				ConfigurationManager.AppSettings["ProviderPortalUrl"]);
			formString.AppendLine();
			formString.AppendLine("<input type=\"hidden\" name=\"JavascriptEnabled\" id=\"JavascriptEnabled\" value=\"true\" />");
			formString.AppendLine("<input name=\"UserName\" id=\"UserName\" size=\"15\" maxlength=\"255\" tabindex=\"1\" value=\"\" />");
			formString.AppendLine("<input type=\"password\" name=\"Password\" size=\"15\" maxlength=\"8\" tabindex=\"2\" value=\"\" />");
			formString.AppendLine("<input type=\"submit\" value=\"Submit\" name=\"B1\" tabindex=\"3\" />");
			formString.AppendLine("</form>");

			ProviderControl.Text = string.Empty;
			ProviderControl.Text = formString.ToString();
			ProviderControl.Visible = false;
		}

		private void SubmitFormSubmit()
		{
			ProviderControl.Visible = true;

			StringBuilder jScript = new StringBuilder();

			jScript.AppendLine("<script type=\"text/javascript\" language=\"javascript\">");
			jScript.AppendLine("function SubmitForm() {");
			jScript.AppendLine("document.form1.UserName.value='" + UserName.Text + "';");
			jScript.AppendLine("document.form1.Password.value='" + UserPasswd.Text + "';");
			jScript.AppendLine("document.form1.submit();");
			jScript.AppendLine("}");
			jScript.AppendLine("SubmitForm();");
			jScript.AppendLine("</script>");

			Type cstype = this.GetType();

			// Get a ClientScriptManager reference from the Page class.
			ClientScriptManager cs = Page.ClientScript;

			// Check to see if the startup script is already registered.
			if (!cs.IsStartupScriptRegistered(cstype, "regJSval"))
			{
				cs.RegisterStartupScript(cstype, "regJSval", jScript.ToString());
			}
		}

		protected void OnLoginSubmit(object sender, EventArgs e)
		{
			HttpCookie aCookie = new HttpCookie("BrokerUserName");
			aCookie.Value = UserName.Text;
			aCookie.Expires = DateTime.Now.AddYears(1);
			Response.Cookies.Add(aCookie);

			if ((UserName.Text.Length == 0) || (UserPasswd.Text.Length == 0))
			{
				ShowAlert("Invalid User name/Password. Please try again.");
			}
			else
			{
				CreateFormSubmit();
				SubmitFormSubmit();
			}
		}

		public static void ShowAlert(string message)
		{
			// Cleans the message to allow single quotation marks
			string cleanMessage = message.Replace("'", "\\'");
			string script = "<script type=\"text/javascript\">alert('" + cleanMessage + "');</script>";

			// Gets the executing web page
			Page page = HttpContext.Current.CurrentHandler as Page;

			// Checks if the handler is a Page and that the script isn't allready on the Page
			if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
			{
				page.ClientScript.RegisterClientScriptBlock(page.GetType(), "alert", script);
			}
		}
	}
}