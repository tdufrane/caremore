﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventDetails.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.EventDetails" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=<%=GoogleMapID %>&sensor=true"></script>
<script type="text/javascript">
	function initializeMap() {
		var geocoder = new google.maps.Geocoder();
		var address = '<%= HttpContext.Current.Server.UrlEncode(LocationAddress) %>';

		geocoder.geocode({ 'address': address }, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				var myOptions = {
					zoom: 17,
					center: results[0].geometry.location,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				}
				var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
				var marker = new google.maps.Marker({
					map: map,
					title: address,
					animation: google.maps.Animation.DROP,
					position: results[0].geometry.location
				});
			} else {
				console.warn("Geocode was not successful for '" + address + "' due to following reason: " + status);
			}
		});
	}

	$j(document).ready(function () {
		initializeMap();
	});
</script>
<div class="span8">
		<p><a onclick="javascript:history.go(-1);" style="cursor:pointer;">Back to Events</a></p>
	<div class="masthead">
		<h1><sc:Text ID="scTitle" runat="server" Field="Name" /></h1>
		<p></p>
	</div>
	<table id="eventDetailsTable">
		<tr>
			<td><strong>When: </strong></td>
			<td><asp:Literal runat="server" ID="litSelectedDate" /> - <asp:Literal runat="server" ID="litSelectedTime" /></td>
		</tr>
		<tr>
			<td style="vertical-align:top;"><strong>Location: </strong></td>
			<td>
				<sc:Text ID="scLocationName" runat="server" Field="Location Name" /><br />
				<sc:Text ID="scLocationAddress" runat="server" Field="Location Address" />
			</td>
		</tr>
		<tr>
			<td colspan="2"><sc:Text ID="scDescription" runat="server" Field="Description" /></td>
		</tr>
	</table>
	<br />
	<div id="map_canvas" style="width:580px; height:200px"></div>
	<br />
	<asp:Button runat="server" ID="btnGetDirections" Text="Get Directions" />
</div>