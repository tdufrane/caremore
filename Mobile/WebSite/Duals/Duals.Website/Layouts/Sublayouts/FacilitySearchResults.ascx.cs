﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using CareMore.Web.DotCom.Models.Providers;
using CareMore.Web.DotCom.Provider;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Content.Articles;
using Duals.Models.Pages;
using Duals.Website.Controls;

using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Links;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class FacilitySearchResults : System.Web.UI.UserControl
	{
		#region Private variables

		private string resultsSessionName = string.Empty;
		private string resultsSessionZip = string.Empty;
		private string detailsBaseUrl = string.Empty;
		private string lastProvider = string.Empty;
		private string thisProvider = string.Empty;
		private string thisId = string.Empty;
		private string lastId = "0"; // Set so lastId at start <> thisId
		private string searchZip = string.Empty;

		List<FacilitySearchResult> searchResults = null;

		#endregion

		#region Page events

		protected void Page_Load(object sender, EventArgs e)
		{
			resultsSessionName = Sitecore.Context.Item.ID.ToString();
			resultsSessionZip = resultsSessionName + "Zip";
			searchZip = Request.QueryString["zip"];

			Item detailsPage = Sitecore.Context.Item.Parent.GetChildren().Where(i => i.Name == "Details").FirstOrDefault();
			detailsBaseUrl = LinkManager.GetItemUrl(detailsPage);

			NewSearchLB.Text = Translate.Text("New Search");

			litTitle.Text = Sitecore.Context.Item.Parent.ToClass<ContentPage>().MastheadTitle;

			if (IsPostBack)
			{
				errorMessage.Visible = false;
			}
			else
			{
				if (!string.IsNullOrWhiteSpace(searchZip))
				{
					if (!ProviderHelper.IsZipCodeCovered(GlobalConsts.LocalizationFolderID, GlobalConsts.CountyTemplateID, searchZip))
					{
						ErrorMessage errorMessageCtl = (ErrorMessage)errorMessage;
						errorMessageCtl.SetNotZipcodeCoveredMessage(SearchResultsPnl);
						errorMessage.Visible = true;
					}
				}

				ProviderListArticle currentPage = new ProviderListArticle(Sitecore.Context.Item);
				ProviderListing listing = new ProviderListing(currentPage.ProviderListType);

				if (listing.HideSearchForm.HasValue && listing.HideSearchForm.Value)
					NewSearchPH.Visible = false;
				
				bool useCache = false;
				string cacheFlag = Request.QueryString["c"];

				if (!string.IsNullOrEmpty(cacheFlag) && cacheFlag.Equals("true", StringComparison.OrdinalIgnoreCase))
					useCache = true;

				ShowResults(useCache);
			}
		}

		protected void ItemsLV_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
		{
			ItemsDP.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
			ItemsDP2.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
			Session["DataPagerStartRowIndex"] = e.StartRowIndex;
			ShowResults(true);
		}

		protected void ItemsLV_LayoutCreated(object sender, EventArgs e)
		{
			ProviderListArticle currentPage = new ProviderListArticle(Sitecore.Context.Item);

			if (currentPage != null)
			{
				Label lblHeader1 = (Label)ItemsLV.FindControl("lblHeader1");
				Label lblHeader2 = (Label)ItemsLV.FindControl("lblHeader2");
				Label lblHeader3 = (Label)ItemsLV.FindControl("lblHeader3");

				lblHeader1.Text = currentPage.Header1;
				lblHeader2.Text = currentPage.Header2;

				if (!string.IsNullOrWhiteSpace(searchZip))
					lblHeader3.Text = currentPage.Header3;
				else
					lblHeader3.Text = currentPage.Header4;
			}
		}

		protected void ItemsLV_DataBound(object sender, EventArgs e)
		{
			var currentDB = Sitecore.Context.Database;

			Item WordingMatchesFoundText = currentDB.SelectSingleItem("/sitecore/content/Global/Wording/MatchesFoundText");

			NumResultsLbl.Text = ItemsDP.TotalRowCount.ToString() + " " + WordingMatchesFoundText.Fields["Text"].Value;
			NumResultsLbl2.Text = NumResultsLbl.Text;

			if (ItemsDP.TotalRowCount <= ItemsDP.PageSize)
			{
				ItemsDP.Visible = false;
				ItemsDP2.Visible = false;
			}
		}

		protected void ItemsLV_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				SetRowDetails(e.Item, (FacilitySearchResult)e.Item.DataItem);
			}
		}

		protected void NewSearchLB_OnClick(object sender, EventArgs e)
		{
			Item searchPage = Sitecore.Context.Item.Parent;
			Response.Redirect(Sitecore.Links.LinkManager.GetItemUrl(searchPage), true);
		}

		#endregion

		#region Private methods

		private void ShowResults(bool useCache)
		{
			try
			{
				if (useCache && Session[resultsSessionName] != null)
				{
					searchResults = (List<FacilitySearchResult>)Session[resultsSessionName];
					searchZip = (string)Session[resultsSessionZip];

					ItemsLV.DataSource = searchResults;
					ItemsLV.DataBind();

					if (Session["DataPagerStartRowIndex"] != null)
					{
						ItemsDP.SetPageProperties((int)Session["DataPagerStartRowIndex"], ItemsDP.MaximumRows, true);
						ItemsDP2.SetPageProperties((int)Session["DataPagerStartRowIndex"], ItemsDP2.MaximumRows, true);
					}
				}
				else
				{
					LoadResults();
				}
			}
			catch (Exception ex)
			{
				ErrorMessage errorMessageCtl = (ErrorMessage)errorMessage;
				errorMessage.Visible = true;
#if DEBUG
				errorMessageCtl.SetError(ex);
#else
				errorMessageCtl.SetDownMessage(ex, SearchResultsPnl);
				NewSearchPH.Visible = false;
#endif
			}
		}

		private void LoadResults()
		{
			ProviderListArticle currentPage = new ProviderListArticle(Sitecore.Context.Item);
			ProviderListing listing = new ProviderListing(currentPage.ProviderListType);

			string name = null;
			if (!string.IsNullOrWhiteSpace(Request.QueryString["name"]))
				name = HttpUtility.UrlDecode(Request.QueryString["name"]);

			string zip = null;
			if (!string.IsNullOrWhiteSpace(Request.QueryString["zip"]))
				zip = Request.QueryString["zip"];

			string city = null;
			if (!string.IsNullOrWhiteSpace(Request.QueryString["city"]))
				city = Request.QueryString["city"];

			string subtype = null;
			if (!string.IsNullOrWhiteSpace(Request.QueryString["type"]))
				subtype = Request.QueryString["type"];

			searchResults = new List<FacilitySearchResult>();

			string dataSource = listing.DataSource.Name;
			if ((dataSource.Equals("Both")) || (dataSource.Equals("Sitecore")))
			{
				List<FacilitySearchResult> sitecoreResults = LoadProvidersSitecore(listing.SitecorePath.TargetItem, name, zip, city, subtype);
				searchResults.AddRange(sitecoreResults);
			}

			if ((dataSource.Equals("Both")) || (dataSource.Equals("Database")))
			{
				List<FacilitySearchResult> databaseResults = LoadProvidersDatabase(listing.ClassCode, name, zip, city, subtype);
				searchResults.AddRange(databaseResults);
			}

			List<string> providerExclusions = ProviderHelper.GetExclusions(listing.Exclusions);

			foreach (string exclusion in providerExclusions)
			{
				IEnumerable<FacilitySearchResult> filteredList;

				if (exclusion.Contains('='))
				{
					filteredList = searchResults.Where(item => (string.Format("{0}={1}", item.ProviderId, item.AddressType.Trim()) != exclusion));
				}
				else
				{
					filteredList = searchResults.Where(item => (item.ProviderId != exclusion));
				}

				searchResults = filteredList.ToList();
			}

			LoadProviders(zip);
		}

		private List<FacilitySearchResult> LoadProvidersDatabase(string facilityCode, string name, string zip, string city, string subtype)
		{
			string searchCode;
			if (string.IsNullOrWhiteSpace(subtype))
				searchCode = facilityCode;
			else
				searchCode = subtype;

			double? maxDistance;
			if (string.IsNullOrWhiteSpace(zip))
				maxDistance = null;
			else
				maxDistance = ProviderHelper.GetMaxZipDistance(GlobalConsts.SiteSettingsID);

			return ProviderFinder.GetFacilityList(ProviderHelper.GetPortalCode(GlobalConsts.SiteSettingsID),
				searchCode, name, zip, city, null, GlobalConsts.DefaultStateCode,
				maxDistance);
		}

		private List<FacilitySearchResult> LoadProvidersSitecore(Item listRoot, string lastName, string zip, string city, string subtype)
		{
			IEnumerable<Item> list = listRoot.Axes.GetDescendants().Where(i => !i.TemplateID.ToString().Equals(Common.FolderTemplateID));

			if (!string.IsNullOrWhiteSpace(lastName))
			{
				IEnumerable<Item> filteredList = from item in list
				                                 where item["Place"].IndexOf(lastName, StringComparison.OrdinalIgnoreCase) > -1 ||
				                                       item["Provider Name"].IndexOf(lastName, StringComparison.OrdinalIgnoreCase) > -1
				                                 select item;
				list = filteredList;
			}

			if (!string.IsNullOrWhiteSpace(city))
			{
				IEnumerable<Item> filteredList = from item in list
				                                 where item["City"].Equals(city, StringComparison.OrdinalIgnoreCase)
				                                 select item;
				list = filteredList;
			}

			if (!string.IsNullOrWhiteSpace(subtype))
			{
				IEnumerable<Item> filteredList = from item in list
				                                 where item["Subtype"].Equals(subtype, StringComparison.OrdinalIgnoreCase)
				                                 select item;
				list = filteredList;
			}

			List<FacilitySearchResult> zipResults = new List<FacilitySearchResult>();

			List<ZipCode> zipCodes;
			if (string.IsNullOrWhiteSpace(zip))
				zipCodes = new List<ZipCode>();
			else
				zipCodes = ProviderFinder.GetZipCodes(zip, ProviderHelper.GetMaxZipDistance(GlobalConsts.SiteSettingsID));

			foreach (Item item in list)
			{
				FacilitySearchResult zipResult = ProviderSearchDataContext.ToFacilitySearchResult(item);

				if (string.IsNullOrWhiteSpace(zip))
				{
					zipResults.Add(zipResult);
				}
				else
				{
					ZipCode zipCode = zipCodes.Where(i => i.BaseZipCode == item["Postal Code"]).FirstOrDefault();
					if (zipCode != null)
					{
						zipResult.Distance = Convert.ToSingle(zipCode.DistanceMiles);
						zipResults.Add(zipResult);
					}
				}
			}

			return zipResults;
		}

		private void LoadProviders(string zip)
		{
			if (searchResults.Count() == 0)
			{
				ErrorMessage errorMessageCtl = (ErrorMessage)errorMessage;
				errorMessageCtl.SetNoResultMessage(SearchResultsPnl);
				errorMessage.Visible = true;
			}
			else
			{
				List<FacilitySearchResult> orderedList;

				if (string.IsNullOrWhiteSpace(zip))
				{
					orderedList = (from item in searchResults
					               orderby item.Name, item.City, item.Address1
					               select item).ToList();
				}
				else
				{
					orderedList = (from item in searchResults
					               orderby item.Distance, item.Name, item.City, item.Address1
					               select item).ToList();
				}

				ItemsLV.DataSource = orderedList;
				ItemsLV.DataBind();

				Session[resultsSessionName] = orderedList;
				Session[resultsSessionZip] = (zip == null) ? string.Empty : zip;

				Session["DataPagerStartRowIndex"] = null;
				SearchResultsPnl.Visible = true;
			}
		}

		private void SetRowDetails(ListViewItem control, FacilitySearchResult row)
		{
			HtmlTableRow trSeparator = (HtmlTableRow)control.FindControl("trSeparator");
			Label idLbl = (Label)control.FindControl("IdLbl");
			Label nameLbl = (Label)control.FindControl("NameLbl");
			Label effectiveLbl = (Label)control.FindControl("EffectiveLbl");
			PlaceHolder addressPH = (PlaceHolder)control.FindControl("AddressPH");
			PlaceHolder detailsPH = (PlaceHolder)control.FindControl("DetailsPH");

			string addressUrl = string.Format(ProviderHelper.DetailsUrlFormat,
				detailsBaseUrl, row.ProviderId, row.AddressId, row.AddressType.TrimEnd());

			thisId = row.ProviderId;
			thisProvider = row.Name;

			if (row.ClassCode != null && row.ClassCode.Equals("CCC"))
			{
				trSeparator.Visible = true;
				nameLbl.Text = string.Format("{0} Care Center", row.City, row.Name);
			}
			else if ((lastProvider.Equals(string.Empty)) || (!thisProvider.Equals(lastProvider)))
			{
				trSeparator.Visible = true;
				nameLbl.Text = row.Name;
				lastProvider = thisProvider;
			}
			else
			{
				trSeparator.Visible = false;
				nameLbl.Visible = false;
			}

			if (lastId.Equals(string.Empty) || !thisId.Equals(lastId))
			{
				if (row.ClassCode != null && row.ClassCode.Equals("CCC"))
					idLbl.Visible = false;
				else
					ProviderHelper.SetNpi(idLbl, row.NpiId);

				lastId = thisId;
			}

			if (nameLbl.Visible)
			{
				ProviderHelper.SetEffectiveDate(effectiveLbl, row.EffectiveDate);
			}

			StringBuilder address = new StringBuilder();

			address.Append(LocationHelper.GetFormattedAddress(row.Address1, row.Address2,
					row.City, row.State, row.Zip));

			if (!string.IsNullOrWhiteSpace(row.Phone))
				address.AppendFormat("<br />Ph: {0}", LocationHelper.GetFormattedPhoneNo(row.Phone));

			if (row.EffectiveDate < DateTime.Now)
				ProviderHelper.SetEffectiveDate(address, row.AddressEffectiveDate);

			Literal addressControl = new Literal();
			addressControl.Text = address.ToString();

			addressPH.Controls.Add(addressControl);

			if (!string.IsNullOrWhiteSpace(searchZip))
				detailsPH.Controls.Add(new Literal() { Text = string.Format("{0:0.00} miles<br />", row.Distance) });

			detailsPH.Controls.Add(new HyperLink() { Text = "View Details", NavigateUrl = addressUrl });
		}

		#endregion
	}
}
