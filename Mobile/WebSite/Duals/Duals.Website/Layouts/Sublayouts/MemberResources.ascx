﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MemberResources.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.MemberResources" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="span8">
	<asp:ListView id="LanguagesLV" runat="server"  OnItemDataBound="LanguagesLV_ItemDataBound">
		<LayoutTemplate>
			<div class="accordion" id="news-accordion">
				<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
			</div>
			<script type="text/javascript">
				function toggleChevron(e) {
					$j(e.target)
						.prev('.accordion-heading')
						.find('a')
						.toggleClass('glyphicon-chevron-right glyphicon-chevron-down');
				}
				$j('.accordion').on('hidden.bs.collapse', toggleChevron);
				$j('.accordion').on('shown.bs.collapse', toggleChevron);
			</script>
		</LayoutTemplate>
		<ItemTemplate>
			<div class="accordion-group">
				<div class="accordion-heading">
					<h3><asp:HyperLink ID="LanguageLink" runat="server" CssClass="accordion-toggle collapsed" data-toggle="collapse"
						data-parent="#news-accordion"></asp:HyperLink></h3>
				</div>
				<asp:Panel ID="LanguageDiv" CssClass="accordion-body collapse" runat="server">
					<asp:ListView ID="ResourcesLV" runat="server">
						<LayoutTemplate>
							<div class="accordion-inner">
								<ul class="quicklinks">
									<asp:PlaceHolder runat="server" ID="itemPlaceHolder"></asp:PlaceHolder>
								</ul>
							</div>
						</LayoutTemplate>
						<ItemTemplate>
							<li>
								<sc:Link runat="server" ID="scLink" Field="Link" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />
							</li>
						</ItemTemplate>
					</asp:ListView>
				</asp:Panel>
			</div>
		</ItemTemplate>
	</asp:ListView>

	<div><sc:Text  runat="server" Field="Secondary Page Content" /></div>
</div>

<script src="/js/accordion.js"></script>




