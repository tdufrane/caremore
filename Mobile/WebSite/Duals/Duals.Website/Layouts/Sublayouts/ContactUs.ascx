﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactUs.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.ContactUs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="subLayout" id="contactPage">
	<div class="leftCol">
		<div class="contactInfo" style="padding: 0 15px 0 15px;">
            <sc:FieldRenderer ID="ContactUsInfoTop" runat="server" EnclosingTag="p" />

			<hr style="clear: both;	border: 0; background-color: #006bb7; height: 3px;" />

			<sc:FieldRenderer ID="ContactUsInfoBottom" runat="server" EnclosingTag="p" />
		</div>
	</div>

<div class="rightCol">
	<div class="contactForm modalPopup contactFix">
		<div class="modalPopupInner contactFix">
			<sc:Placeholder ID="ContactUsForm" Key="formcontent" runat="server" />
			<div style="clear:both;">&nbsp;</div>
		</div>
	</div>

	<p>&nbsp;</p>
</div>

</div>
<!-- End SubLayouts/ContactUs -->