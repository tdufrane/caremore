﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Base;
using Duals.Models.Pages;

using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class LeftNav : Controls.BaseSublayout
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			BuildSideNav();
		}

		private void BuildSideNav()
		{
			try
			{
				// get Landing Page item
				Item currentItem = Sitecore.Context.Item;
				ContentPage landingPage = currentItem.ToClass<Duals.Models.Pages.LandingPage>();

				// if landingPage == null try SiteLandingPage
				if (landingPage == null)
				{
					landingPage = currentItem.ToClass<Duals.Models.Pages.SiteLandingPage>();
				}

				while (landingPage == null && currentItem != null && currentItem.ParentID.ToString() != GlobalConsts.DualsHomeID && currentItem.ID.ToString() != GlobalConsts.ContentID)
				{
					currentItem = currentItem.Parent;
					landingPage = currentItem.ToClass<Duals.Models.Pages.LandingPage>();

					if (landingPage == null)
					{
						landingPage = currentItem.ToClass<Duals.Models.Pages.SiteLandingPage>();
					}
				}

				if (landingPage != null)
				{
					sideNavLV.DataSource = landingPage.InnerItem.Children.ToList()    // get all children of Landing Page node
						.ConvertAll(item => item.ToClass<Duals.Models.Base.Page>())    // convert to Page class
						.Where(page => page != null && (page.ExcludeFromNavigation.HasValue ? !page.ExcludeFromNavigation.Value : true));   // exclude null and not included in nav
					sideNavLV.DataBind();

					// set siteNav title
					Literal navTitle = (Literal)sideNavLV.FindControl("navTitle");
					navTitle.Text = landingPage.NavigationTitle;
				}
			}
			catch (Exception exc)
			{
				Sitecore.Diagnostics.Log.Error("Duals: Cannot build SideNav", exc, this);
			}
		}

		protected void sideNavLV_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				ListViewDataItem LVDI = (ListViewDataItem)e.Item;
				HyperLink hl = (HyperLink)e.Item.FindControl("hl");

				if (LVDI.DataItem is Duals.Models.Pages.GeneralLinkPage)
				{
					Duals.Models.Pages.GeneralLinkPage page = LVDI.DataItem as Duals.Models.Pages.GeneralLinkPage;
					hl.NavigateUrl = page.Link.Url;
					hl.Target = page.Link.Target;

					if (string.IsNullOrWhiteSpace(page.Link.Text))
						hl.Text = page.NavigationTitle;
					else
						hl.Text = page.Link.Text;
				}
				else
				{
					Duals.Models.Base.Page page = LVDI.DataItem as Duals.Models.Base.Page;

					HtmlGenericControl li = (HtmlGenericControl)e.Item.FindControl("li");
					li.Attributes["class"] = (page.ID == Sitecore.Context.Item.ID) || (page.ID == Sitecore.Context.Item.Parent.ID) || (page.ID == Sitecore.Context.Item.Parent.Parent.ID) ? "selected" : string.Empty;

					hl.Text = page.NavigationTitle;
					hl.NavigateUrl = LinkManager.GetItemUrl(page.InnerItem);

					//Sub navigation items: only displays children of current item
					if ((page.ID == Sitecore.Context.Item.ID) || (page.ID == Sitecore.Context.Item.Parent.ID) || (page.ID == Sitecore.Context.Item.Parent.Parent.ID)
						|| (Sitecore.Context.Item.ID == page.InnerItem.Parent.ID && page.DontCollaspeChildren.HasValue && page.DontCollaspeChildren.Value))
					{
						ListView subListView = (ListView)e.Item.FindControl("sideSubNavLV");
						subListView.DataSource = page.InnerItem.Children.ToList()
							.ConvertAll(item => item.ToClass<Duals.Models.Base.Page>())
							.Where(pg => pg != null && (pg.ExcludeFromNavigation.HasValue ? !pg.ExcludeFromNavigation.Value : true));
						subListView.DataBind();
					}
				}
			}
		}
// ||    (Sitecore.Context.Item.ID == landingPage.ID && page.DontCollaspeChildren.HasValue && page.DontCollaspeChildren.Value)
		protected void sideSubNavLV_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				ListViewDataItem LVDI = (ListViewDataItem)e.Item;
				HyperLink subhl = (HyperLink)e.Item.FindControl("subhl");

				if (LVDI.DataItem is Duals.Models.Pages.GeneralLinkPage)
				{
					Duals.Models.Pages.GeneralLinkPage page = LVDI.DataItem as Duals.Models.Pages.GeneralLinkPage;
					subhl.CssClass = "subhl";
					subhl.NavigateUrl = page.Link.Url;
					subhl.Target = page.Link.Target;

					if (string.IsNullOrWhiteSpace(page.Link.Text))
						subhl.Text = page.NavigationTitle;
					else
						subhl.Text = page.Link.Text;
				}
				else
				{
					Duals.Models.Base.Page page = LVDI.DataItem as Duals.Models.Base.Page;

					subhl.CssClass = ((page.ID == Sitecore.Context.Item.ID) || (page.ID == Sitecore.Context.Item.Parent.ID) || (page.ID == Sitecore.Context.Item.Parent.Parent.ID)) ? "selected" : "subhl";
					subhl.NavigateUrl = LinkManager.GetItemUrl(page.InnerItem);
					subhl.Text = page.NavigationTitle;
				}
			}
		}
	}
}