﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using CareMore.Web.DotCom.Models.Providers;
using CareMore.Web.DotCom.Provider;

using ClassySC.Data;

using Duals.Business;
using Duals.Models;
using Duals.Models.Content.Articles;
using Duals.Website.Controls;

using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class FacilitySearchDetails : ProviderDetailsBase
	{
		#region Private variables

		private string lastState = string.Empty;
		private string lastCounty = string.Empty;
		private string thisState = string.Empty;
		private string thisCounty = string.Empty;
		private string providerID = string.Empty;
		private string addressID = string.Empty;

		#endregion

		#region Page events

		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				providerID = Request.QueryString["id"];
				addressID = Request.QueryString["aid"];

				Guid sitecoreId;
				long prprId;

				if (Guid.TryParse(providerID, out sitecoreId))
				{
					GetItemFromSitecore();
				}
				else if (long.TryParse(providerID, out prprId))
				{
					GetItemFromDatabase(Request.QueryString["at"]);
				}
				else
				{
					ShowNotFound();
				}
			}
			catch (Exception ex)
			{
				ErrorMessage errorMessageCtl = (ErrorMessage)errorMessage;
				errorMessage.Visible = true;
#if DEBUG
				errorMessageCtl.SetError(ex);
#else
				errorMessageCtl.SetDownMessage(ex, DetailsPH);
#endif
			}
		}

		protected void BackToResultsLB_Click(object sender, EventArgs e)
		{
			Item resultsPage = Sitecore.Context.Item.Parent.GetChildren().Where(i => i.Name == "Results").FirstOrDefault();
			Response.Redirect(Sitecore.Links.LinkManager.GetItemUrl(resultsPage) + "?c=true");
		}

		protected void NewSearchLB_OnClick(object sender, EventArgs e)
		{
			Item searchPage = Sitecore.Context.Item.Parent;
			Response.Redirect(Sitecore.Links.LinkManager.GetItemUrl(searchPage));
		}

		#endregion

		#region Protected methods

		protected string Format2ndHours(object fromHours, object toHours)
		{
			return LocationHelper.GetFormatted2ndHours(fromHours.ToString(), toHours.ToString());
		}

		#endregion

		#region Private methods

		private void GetItemFromDatabase(string addressType)
		{
			List<FacilitySearchDetail> details = ProviderFinder.GetFacilityDetail(ProviderHelper.GetPortalCode(Duals.Models.GlobalConsts.SiteSettingsID),
				providerID, addressType);

			if (details.Count == 0)
			{
				ShowNotFound();
			}
			else
			{
				FacilitySearchDetail detail = details[0];

				ShowDetail(detail,
					ProviderHelper.GetServices(ProviderFinder.GetProviderServices(providerID, addressID, addressType)),
					ProviderFinder.GetProviderHours(detail.ProviderId, detail.AddressId, addressType, detail.AddressEffectiveDate),
					GetProviderLanguages(providerID, addressID, addressType),
					ProviderFinder.GetProviderAccessibility(providerID, addressID, addressType),
					false);
			}
		}

		private void GetItemFromSitecore()
		{
			Item pageItem = Sitecore.Context.Item;
			Item providerItem = Sitecore.Context.Database.GetItem(providerID, Sitecore.Globalization.Language.Parse("en"));

			if (providerItem == null)
			{
				ShowNotFound();
			}
			else
			{
				FacilitySearchDetail detail = new FacilitySearchDetail();

				detail.Accessibility = !string.IsNullOrEmpty(providerItem["Accessibility"]);
				detail.Address1 = providerItem["Address Line 1"];
				detail.Address2 = providerItem["Address Line 2"];
				detail.City = providerItem["City"];
				detail.County = providerItem["County"];
				detail.Fax = providerItem["Fax"];

				if (string.IsNullOrWhiteSpace(providerItem["Place"]))
					detail.Name = providerItem["Provider Name"];
				else
					detail.Name = providerItem["Place"];

				detail.NpiId = providerItem["NPI ID"];
				detail.Phone = providerItem["Phone"];
				detail.ProviderId = providerItem["Provider ID"];
				detail.State = providerItem["State"];
				detail.Zip = providerItem["Postal Code"];

				detail.Transportation = providerItem["Transportation"];
				detail.SpecializedTraining = providerItem["Specialized Training"];
				detail.CredentialingStatus = providerItem["Other Credentials"];

				if (string.IsNullOrEmpty(providerItem["Subtype"]))
					detail.ClassDescription = Sitecore.Context.Item["Facility Type"];
				else
					detail.ClassDescription = providerItem["Subtype"];

				List<string> services = null;
				if (Sitecore.Context.Item["Show Services"].Equals("1"))
					services = ProviderHelper.GetServices(providerItem);

				ShowDetail(detail, services,
					ProviderHelper.GetHoursOfOperation(providerItem["Hours of Operation"]),
					ProviderHelper.GetLanguages(providerItem["Languages"]),
					ProviderHelper.GetAccessibilities(providerItem["Accessibility"]),
					true);
			}
		}

		private void ShowDetail(FacilitySearchDetail detail, List<string> services, List<ProviderHour> hours, string languages, List<ProviderAccessibility> accessibilities, bool fromSitecore)
		{
			// Name and ID
			if (detail.ClassDescription.Equals("Caremore Care Center", StringComparison.OrdinalIgnoreCase))
				FacilityText.Text = detail.City + " Care Center";
			else
				FacilityText.Text = detail.Name;

			ProviderHelper.SetNpi(IdText, detail.NpiId);

			// Contact Info
			PhoneText.Text = LocationHelper.GetFormattedPhoneNo(detail.Phone);
			FaxText.Text = LocationHelper.GetFormattedPhoneNo(detail.Fax);

			// Address
			LocationText.Text = LocationHelper.GetFormattedAddress(detail.Address1, detail.Address2,
				detail.City, detail.State, detail.Zip);

			// Google Directions
			DirectionsLink.NavigateUrl = ProviderHelper.GetMapUrl(detail);
			DirectionsLink.ToolTip = "To get directions using public transportation, enter your address in Google and click the BUS icon.";
			DirectionsLink.Target = "_blank";

			// Google Map
			string locationAddresses = string.Format("\"{0} {1}, {2} {3}|{4}|{5}|{6}|{7}|{8}\"",
				detail.Address1 ?? string.Empty,
				detail.Address2 ?? string.Empty,
				detail.City ?? string.Empty,
				detail.Zip ?? string.Empty,
				detail.Name ?? string.Empty,
				Common.GetKeyImagePath(Sitecore.Context.Item),
				string.Empty,
				LocationHelper.GetFormattedPhoneNo(detail.Phone) ?? string.Empty,
				string.Empty);

			GoogleMap gMap = (GoogleMap)googleMap;
			gMap.SetLocationsArray(locationAddresses);
			gMap.SetZoomLevel(15);
			gMap.DisplayMap();

			// Business Hours
			if (hours != null && hours.Any())
			{
				gvPrimaryHours.DataSource = hours;
				gvPrimaryHours.DataBind();

				BusinessHoursPH.Visible = true;
			}

			FacilityTypeText.Text = detail.ClassDescription;

			// Effective Date
			if (detail.EffectiveDate > DateTime.Now.Date)
			{
				EffectivePnl.Visible = true;
				EffectiveDateText.Text = detail.EffectiveDate.ToString("MM/dd/yyyy");
			}

			if (Sitecore.Context.Item["Show Languages"].Equals("1"))
			{
				LanguagesPnl.Visible = true;
				LanguagesText.Text = languages;
			}

			// Accessibility
			if (fromSitecore)
			{
				AccessibilityType.Visible = false;
				AccessibilityList.DataSource = accessibilities;
				AccessibilityList.DataBind();

				PublicTransportationText.Text = detail.Transportation.Equals("1") ? "Yes" : "No";

				if (Sitecore.Context.Item["Show Specialized Training"].Equals("1"))
				{
					SpecializedTrainingPnl.Visible = true;
					SpecializedTrainingText.Text = detail.SpecializedTraining.Equals("1") ? "Yes" : "No";
				}

				if (Sitecore.Context.Item["Show Credentialing"].Equals("1"))
				{
					OtherCredsCertsPnl.Visible = true;
					OtherCredsCertsText.Text = detail.CredentialingStatus;
				}
			}
			else
			{
				SetAccessibilityDb(accessibilities, AccessibilityType, AccessibilityList);

				if (string.IsNullOrWhiteSpace(detail.Transportation))
					PublicTransportationText.Text = string.Empty;
				else if (ProviderHelper.IsInDistance(detail.Transportation, decimal.Parse(Sitecore.Context.Item["Max Transportation Distance"])))
					PublicTransportationText.Text = "Yes";
				else
					PublicTransportationText.Text = "No";

				if (Sitecore.Context.Item["Show Specialized Training"].Equals("1"))
				{
					SpecializedTrainingPnl.Visible = true;
					SpecializedTrainingText.Text = detail.SpecializedTraining;
				}

				if (Sitecore.Context.Item["Show Credentialing"].Equals("1"))
				{
					OtherCredsCertsPnl.Visible = true;

					if (detail.CredentialingStatus.Equals("C"))
						OtherCredsCertsText.Text = Sitecore.Context.Item["Certified Text"];
					else if (detail.CredentialingStatus.Equals("N"))
						OtherCredsCertsText.Text = "Other";
				}
			}

			if (Sitecore.Context.Item["Show Services"].Equals("1"))
			{
				ServicesPnl.Visible = true;

				if (services != null && services.Count > 0)
					ServicesText.Text = string.Join(", ", services.ToArray());
			}

		}

		private void ShowNotFound()
		{
			ErrorMessage errorMessageCtl = (ErrorMessage)errorMessage;
			errorMessageCtl.SetNoResultMessage(DetailsPH);
			errorMessage.Visible = true;
		}

		#endregion
	}
}
