﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Carousel.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.Carousel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>

<!-- C O N T E N T  S L I D E R -->
<script>
	$j(function () {
		$j('#homeContentSlider').codaSlider();
	});
</script>

<div class="carousel">
	<div id="slider-id-wrapper" class="coda-slider-wrapper">
		<div class="coda-nav">
			<asp:ListView ID="lvNav" runat="server" OnItemDataBound="lvNav_ItemDataBound">
				<LayoutTemplate>
					<ul>
						<asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
					</ul>
				</LayoutTemplate>
				<ItemTemplate>
					<li id="listItem" runat="server">
						<a runat="server" ID="navLink">&nbsp;</a>
					</li>
				</ItemTemplate>
			</asp:ListView>
		</div>

		<div class="coda-slider" id="homeContentSlider">
			<asp:ListView ID="lvContent" runat="server" OnItemDataBound="lvContent_ItemDataBound">
				<LayoutTemplate>
					<asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
				</LayoutTemplate>
				<ItemTemplate>
					<div class="slide" style="background-repeat:no-repeat;" runat="server" id="carouselImage">
						<sc:Image runat="server" ID="scImage" />
						<div class="slideContent">
							<h1><sc:Text runat="server" ID="scTitle" /></h1>
							<sc:FieldRenderer runat="server" ID="scText" EnclosingTag="p" />
							<sc:Link runat="server" ID="scLink" CssClass="slideButton">Learn More</sc:Link>
						</div>
					</div>
				</ItemTemplate>
			</asp:ListView>
		</div>
	</div>
</div>
