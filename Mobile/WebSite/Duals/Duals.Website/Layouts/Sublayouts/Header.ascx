﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.Header" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<header class="page-head">
	<div class="brand"><a href="/"><sc:Image runat="server" ID="scLogo" Field="Logo Image" /></a></div>
  	
	<nav class="global">

		<asp:ListView id="globalNavLV" runat="server" OnItemDataBound="globalNavLV_ItemDataBound">
			<LayoutTemplate>
				<ul>
					<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
				</ul><!-- end globalNavLinks -->
			</LayoutTemplate>
			<ItemTemplate>
				<li><asp:HyperLink runat="server" ID="hl" /><asp:Literal runat="server" ID="litSeparator" Text="|" Visible="false" /></li>
			</ItemTemplate>
		</asp:ListView>

		<div class="language" style="display:none;">
			<label for="ctl06_languageSelectDDL">Select Language</label>
			<asp:DropDownList runat="server" ID="languageSelectDDL" AutoPostBack="True" 
				onselectedindexchanged="languageSelectDDL_SelectedIndexChanged"></asp:DropDownList>
		</div>
		<div class="resize">
			<p><sc:Text runat="server" ID="scTextSize" Field="Text Size" /></p>
			<div onclick="javascript:decreaseFontSize();" onkeypress="javascript:decreaseFontSize();"><sc:Image runat="server" ID="scTextSizeDownImage" Field="Text Size Down Image" /></div>
			<div onclick="javascript:increaseFontSize();" onkeypress="javascript:increaseFontSize();"><sc:Image runat="server" ID="scTextSizeUpImage" Field="Text Size Up Image" /></div>
		</div>	  		
	</nav>

	<div class="search">
		<label for="ctl06_btnSearch" class="accessible">Search</label>
		<input id="txtSearchInput" runat="server" type="text" placeholder="Search" /><asp:Button ID="btnSearch" CssClass="button" Text="GO" OnClick="btnSearch_OnClick" runat="server" />
	</div>

	<nav class="primary">
		<sc:Sublayout runat="server" Path="~/Duals/Duals.Website/Layouts/Sublayouts/SiteNav.ascx" />
	</nav>
</header>