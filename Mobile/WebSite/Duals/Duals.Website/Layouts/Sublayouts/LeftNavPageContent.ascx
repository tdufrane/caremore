﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftNavPageContent.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.LeftNavPageContent" %>
<%@ Register Src="../../Controls/BaseContentPage.ascx" TagPrefix="cm" TagName="BaseContentPage" %>

<cm:BaseContentPage ID="ucBaseContentPage" runat="server" ColumnSpanCssClass="span8" />
