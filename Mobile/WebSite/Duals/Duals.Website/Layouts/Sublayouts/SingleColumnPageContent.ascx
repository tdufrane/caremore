﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SingleColumnPageContent.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.SingleColumnPageContent" %>
<%@ Register Src="../../Controls/BaseContentPage.ascx" TagPrefix="cm" TagName="BaseContentPage" %>

<cm:BaseContentPage ID="ucBaseContentPage" runat="server" ColumnSpanCssClass="span12" />
