﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MemberStories.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.MemberStories" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<div class="span8">

    <div class="masthead videoFeature">                
        <div class="video"><div id="feature-video"></div><%= FeatureVideoJs %></div>
        <div class="description">
			<h3><sc:Text runat="server" ID="featureName" Field="Name" /></h3>
			<p><sc:Text runat="server" ID="featureDesc" Field="Description" /></p>
			</div>            
    </div><!-- / videoFeature -->

    <div class="row">
        <asp:ListView ID="lvLeftColumn" runat="server" OnItemDataBound="lvColumn_ItemDataBound">
            <LayoutTemplate>
                <ul class="videoThumbs span4">
                    <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                </ul><!-- / videoThumbs left column -->
            </LayoutTemplate>
            <ItemTemplate>
                <li>
				    <asp:HyperLink runat="server" ID="vidLink1"><sc:Image runat="server" ID="scImage" /></asp:HyperLink>
				    <h4><asp:HyperLink runat="server" id="vidLink2"><sc:Text runat="server" ID="scName" /></asp:HyperLink></h4>
				    <p><sc:Text runat="server" ID="scDesc" /></p>
				</li>
            </ItemTemplate>
        </asp:ListView>

        <asp:ListView ID="lvRightColumn" runat="server" OnItemDataBound="lvColumn_ItemDataBound">
            <LayoutTemplate>
                <ul class="videoThumbs span4">
                    <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                </ul><!-- / videoThumbs right column-->
            </LayoutTemplate>
            <ItemTemplate>
                <li>
				    <asp:HyperLink runat="server" ID="vidLink1"><sc:Image runat="server" ID="scImage" /></asp:HyperLink>
				    <h4><asp:HyperLink runat="server" ID="vidLink2"><sc:Text runat="server" ID="scName" /></asp:HyperLink></h4>
				    <p><sc:Text runat="server" ID="scDesc" /></p>
				</li>
            </ItemTemplate>
        </asp:ListView>
    </div>
</div><!-- / span8 -->