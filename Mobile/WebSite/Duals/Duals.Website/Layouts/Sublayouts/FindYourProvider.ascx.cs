﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using CareMore.Web.DotCom.Models.Providers;
using CareMore.Web.DotCom.Provider;

using ClassySC.Data;

using Duals.Business;
using Duals.Models;
using Duals.Models.Content.Articles;
using Duals.Models.Content.Contact;
using Duals.Models.Content.Facilities;
using Duals.Website.Controls;

using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Configuration;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class FindYourProvider : Controls.BaseSublayout
	{
		#region Variables

		#endregion

		#region Page events

		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				LoadText();

				if (!IsPostBack)
				{
					FillDropDownList();
				}
			}
			catch (Exception ex)
			{
				ErrorMessage errorMessageCtl = (ErrorMessage)errorMessage;
#if DEBUG
				errorMessageCtl.SetError(ex);
#else
				errorMessageCtl.SetDownMessage(ex);
#endif
				errorMessage.Visible = true;
			}
		}

		protected void BtnSubmit_OnClick(object sender, EventArgs e)
		{
			try
			{
				if (Page.IsValid)
				{
					CreateGoogleMap(ZipCodeTB.Text);
				}
			}
			catch (Exception ex)
			{
				ErrorMessage errorMessageCtl = (ErrorMessage)errorMessage;
#if DEBUG
				errorMessageCtl.SetError(ex);
#else
				errorMessageCtl.SetDownMessage(ex);
#endif
				errorMessage.Visible = true;
			}
		}

		#endregion

		#region Private methods

		private void CreateGoogleMap(string zipCode)
		{
			List<GoogleMapKey> keys = new List<GoogleMapKey>();
			List<LocationItem> locations = new List<LocationItem>();

			if (string.IsNullOrEmpty(MapKeyList.SelectedValue))
			{
				for (int index = 1; index < MapKeyList.Items.Count; index++)
				{
					LocationListing locationListing = GetListing(MapKeyList.Items[index].Value, zipCode);

					if ((locationListing.List != null) && (locationListing.List.Count > 0))
					{
						keys.Add(new GoogleMapKey(locationListing.ImageIconUrl, locationListing.ImageIconText));
						locations.AddRange(locationListing.List);
					}
				}
			}
			else
			{
				LocationListing locationListing = GetListing(MapKeyList.SelectedValue, zipCode);

				if ((locationListing.List != null) && (locationListing.List.Count > 0))
				{
					keys.Add(new GoogleMapKey(locationListing.ImageIconUrl, locationListing.ImageIconText));
					locations.AddRange(locationListing.List);
				}
			}

			LoadProviders(zipCode, locations, keys);
		}

		private void LoadProviders(string zipCode, List<LocationItem> locations, List<GoogleMapKey> keys)
		{
			GoogleMap gMap = (GoogleMap)googleMap;

			if (locations.Count == 0)
			{
				ErrorMessage errorMessageCtl = (ErrorMessage)errorMessage;
				errorMessage.Visible = true;
				errorMessageCtl.SetNoResultMessage();

				gMap.Visible = false;
			}
			else
			{
				StringBuilder mapLocations = new StringBuilder();

				foreach (LocationItem location in locations)
				{
					mapLocations.AppendFormat("\"{0}, {1} {2}|{3}|{4}|{5}|{6}|{7}\",\n",
						location.Address1, location.City, location.ZipCode,
						location.Name, location.IconImage, location.Description,
						LocationHelper.GetFormattedPhoneNo(location.Phone), location.ItemURL);
				}

				mapLocations.Length -= 2;  // Remove trailing ,\n

				gMap.DisplayMapKey(keys);
				gMap.SetLocationsArray(mapLocations.ToString());
				gMap.SetCenter(zipCode);
				gMap.SetZoomLevel(12);
				gMap.DisplayMap();
				gMap.Visible = true;
			}
		}

		private void FillDropDownList()
		{
			string allServicesString = Translate.Text("All Services");

			MapKeyList.Items.Add(new ListItem(allServicesString, string.Empty)); // All Services selection

			// MapKey Dropdown
			List<Item> mapKeyItems = Sitecore.Context.Database.GetItem(GlobalConsts.ProviderListingsFolderID).GetChildren()
				.OrderBy(i => i.Name).ToList();

			foreach (Item mapKey in mapKeyItems)
			{
				if (mapKey["Exclude From Service Map"] != "1")
					MapKeyList.Items.Add(new ListItem(mapKey["Title"], mapKey.ID.ToString()));
			}

			MapKeyList.SelectedIndex = 0;
		}

		private LocationListing GetListing(string id, string zipCode)
		{
			LocationListing locationListing = new LocationListing();

			Item listItem = Sitecore.Context.Database.GetItem(id);
			ProviderListing listing = new ProviderListing(listItem);

			if ((listing.DataSource != null) && (listing.ExcludeFromServiceMap.HasValue ? listing.ExcludeFromServiceMap.Value : true))
			{
				// Find the details page for this listing
				Item resultsItem = Sitecore.Context.Item.Axes.GetDescendants().Where(item => item.Name.Equals("Results") && item.Fields["Provider List Type"].Value.Equals(id)).FirstOrDefault();
				Item detailsPage = resultsItem.Parent.GetChildren().Where(i => i.Name == "Details").FirstOrDefault();
				string detailsBaseUrl = LinkManager.GetItemUrl(detailsPage);
				string imageIcon = Common.GetKeyImagePath(detailsPage);

				List<LocationItem> searchResults = new List<LocationItem>();
				string dataSource = listing.DataSource.Name;

				if ((dataSource.Equals("Both")) || (dataSource.Equals("Sitecore")))
				{
					List<LocationItem> sitecoreResults = GetSitecoreListing(listing.Title, listing.SitecorePath.TargetItem, zipCode, detailsBaseUrl, imageIcon);
					searchResults.AddRange(sitecoreResults);
				}

				if ((dataSource.Equals("Both")) || (dataSource.Equals("Database")))
				{
					List<LocationItem> databaseResults = GetDatabaseListing(listing.Title, zipCode, listing.ClassCode, listing.SpecialtyFilter, detailsBaseUrl, imageIcon);
					searchResults.AddRange(databaseResults);
				}

				locationListing.List = ApplyExclusions(searchResults, listing.Exclusions);
				locationListing.ImageIconText = listing.Title;
				locationListing.ImageIconUrl = imageIcon;
			}

			return locationListing;
		}

		private List<LocationItem> GetDatabaseListing(string providerTypeName, string zip, string classCode, IEnumerable<Item> specialtyFilters, string detailsBaseUrl, string imageIcon)
		{
			List<LocationItem> list = new List<LocationItem>();

			string specialties = string.Empty;
			if (specialtyFilters != null)
			{
				List<string> specialtyList = new List<string>();
				foreach (Item specialtyItem in specialtyFilters)
				{
					specialtyList.Add(specialtyItem["Value"].TrimEnd());
				}

				if (specialtyList.Count > 0)
					specialties = string.Join(",", specialtyList.ToArray());
			}

			if (string.IsNullOrEmpty(classCode))
			{
				List<ProviderSearchResult> practitioners = ProviderFinder.GetProviderList(ProviderHelper.GetPortalCode(GlobalConsts.SiteSettingsID),
					null, zip, null, GlobalConsts.DefaultCounty, GlobalConsts.DefaultStateCode,
					null, specialties, null, ProviderHelper.GetMaxZipDistance(GlobalConsts.SiteSettingsID));

				foreach (ProviderSearchResult practitioner in practitioners)
				{
					ProviderResultAddress address =  new ProviderResultAddress(practitioner);
					list.Add(new LocationItem(address.Address1, address.City, address.Zip, practitioner.Name,
					                          imageIcon, providerTypeName, address.Phone, practitioner.ProviderId, practitioner.AddressType,
					                          string.Format(ProviderHelper.DetailsUrlFormat,
					                                        detailsBaseUrl, practitioner.ProviderId,
					                                        practitioner.AddressId, practitioner.AddressType.Trim())));
				}
			}
			else
			{
				List<FacilitySearchResult> facilities = ProviderFinder.GetFacilityList(ProviderHelper.GetPortalCode(GlobalConsts.SiteSettingsID),
					classCode, null, zip, null, GlobalConsts.DefaultCounty, GlobalConsts.DefaultStateCode,
					ProviderHelper.GetMaxZipDistance(GlobalConsts.SiteSettingsID));

				foreach (FacilitySearchResult facility in facilities)
				{
					FacilityResultAddress address = new FacilityResultAddress(facility);
					list.Add(new LocationItem(address.Address1, address.City, address.Zip, facility.Name,
					                          imageIcon, providerTypeName, address.Phone, facility.ProviderId, facility.AddressType,
					                          string.Format(ProviderHelper.DetailsUrlFormat,
					                                        detailsBaseUrl, facility.ProviderId,
					                                        facility.AddressId, facility.AddressType.Trim())));
				}
			}

			return list;
		}

		private List<LocationItem> GetSitecoreListing(string providerTypeName, Item listRoot, string zip, string detailsBaseUrl, string imageIcon)
		{
			List<LocationItem> list = new List<LocationItem>();

			IEnumerable<Item> itemList = listRoot.Axes.GetDescendants().Where(i => !i.TemplateID.Equals(Common.FolderTemplateID));

			List<ZipCode> zipCodes = ProviderFinder.GetZipCodes(zip, ProviderHelper.GetMaxZipDistance(GlobalConsts.SiteSettingsID));

			IEnumerable<Item> filteredList = from item in itemList
			                                 join zipCode in zipCodes on item["Postal Code"] equals zipCode.BaseZipCode
			                                 select item;

			list = filteredList.ToList().ConvertAll(item =>
				new LocationItem(item, "Provider Name", imageIcon, providerTypeName,
				                 string.Format("{0}?id={1}&aid=&at=P", detailsBaseUrl, item.ID)));

			return list;
		}

		private List<LocationItem> ApplyExclusions(List<LocationItem> locations, string exclusions)
		{
			List<LocationItem> filtered = null;

			if (string.IsNullOrEmpty(exclusions))
			{
				filtered = locations;
			}
			else
			{
				List<string> providerExclusions = ProviderHelper.GetExclusions(exclusions);

				foreach (string exclusion in providerExclusions)
				{
					IEnumerable<LocationItem> filteredList;

					if (exclusion.Contains('='))
					{
						filteredList = locations.Where(item => (string.Format("{0}={1}", item.ItemID, item.AddressType.Trim()) != exclusion));
					}
					else
					{
						filteredList = locations.Where(item => (item.ItemID != exclusion));
					}

					filtered = filteredList.ToList();
				}
			}

			return filtered;
		}

		private void LoadText()
		{
			ZipCodeTitle.Text = Translate.Text("Zip Code");
			ShowTitle.Text = Translate.Text("Show");
			btnSubmit.Text = Translate.Text("Search");
			btnSubmit.ToolTip = Translate.Text("Search");
		}

		#endregion

		#region Private Classes

		private class LocationListing
		{
			public List<LocationItem> List;
			public string ImageIconUrl;
			public string ImageIconText;
		}

		#endregion
	}
}
