﻿using System;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom;
using Sitecore.Data.Items;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class MaterialRequest : Controls.BaseSublayout
	{
		#region Variables

		private const string PostBackSessionName = "MaterialRequest";

		#endregion

		#region Page events

		protected void Page_Load(object sender, EventArgs e)
		{
			//TODO prevent multiple postbacks
			lblError.Text = string.Empty;
		}

		protected void SendMessageClick(object sender, EventArgs e)
		{
			try
			{
				if (Session[PostBackSessionName] == null)
				{
					if (SendRequest())
					{
						//Set session to prevent refresh posts
						Session[PostBackSessionName] = DateTime.Now;

						SetMessage(Sitecore.Context.Item["Confirmation Message"], string.Empty);
					}
				}
				else
				{
					SetMessage(Common.GetNoMultiSubmit(), "error");
				}
			}
			catch (Exception ex)
			{
				lblError.Text = Common.GetErrorMessage(ex, Sitecore.Context.Item["Email Error Text"]);
			}
		}

		#endregion

		#region Private methods

		private bool SendRequest()
		{
			bool sent = false;
			Item currentItem = Sitecore.Context.Item;

			string toEmail = currentItem["Email To"];
			string fromEmail = currentItem["Email From"];

			if ((!string.IsNullOrWhiteSpace(toEmail)) && (!string.IsNullOrWhiteSpace(fromEmail)))
			{
				StringBuilder body = new StringBuilder();

				body.AppendFormat("Person Is A: <b>{0}</b><br />", CheckedText(MemberTypes.Items));
				body.AppendFormat("Current Cal MediConnect Plan Member: <b>{0}</b><br />", CheckedText(PlanMember.Items));

				body.AppendFormat("Last Name: <b>{0}</b><br />", LastName.Text.Trim());
				body.AppendFormat("First Name: <b>{0}</b><br />", FirstName.Text.Trim());
				body.AppendFormat("Middle Initial: <b>{0}</b><br /><br />", MiddleInitial.Text.Trim());

				body.AppendFormat("Address: <b>{0}</b><br />", Address.Text.Trim());
				body.AppendFormat("City: <b>{0}</b><br />", City.Text.Trim());
				body.AppendFormat("State: <b>{0}</b><br />", State.Text.Trim());
				body.AppendFormat("Zip Code: <b>{0}</b><br />", ZipCode.Text.Trim());

				body.AppendFormat("Phone: <b>{0}</b><br />", Phone.Text.Trim());
				body.AppendFormat("Member ID: <b>{0}</b><br /><br />", MemberID.Text.Trim());

				body.AppendFormat("Language(s) Requested: <b>{0}</b><br />", CheckedText(LanguageRequested.Items));
				body.AppendFormat("Alternate Format(s): <b>{0}</b><br />", CheckedText(AlternateFormat.Items));

				MailMessage message = new MailMessage();
				message.From = new MailAddress(fromEmail);
				message.To.Add(new MailAddress(toEmail));
				message.Subject = currentItem.Fields["Email Subject"].Value;
				message.Body = body.ToString();
				message.IsBodyHtml = true;

				CareMore.Web.DotCom.Common.SendEmail(message);

				sent = true;
			}

			return sent;
		}

		private string CheckedText(ListItemCollection items)
		{
			StringBuilder text = new StringBuilder();

			foreach (ListItem item in items)
			{
				if (item.Selected)
					text.AppendFormat("{0}, ", item.Text);
			}

			if (text.Length > 0)
				text.Length -= 2; // remove trailing ", "

			return text.ToString();
		}

		private void SetMessage(string text, string cssClass)
		{
			MessageLabel.Text = text;
			MessageLabel.CssClass = cssClass;
			MessagePanel.Visible = true;
			MaterialReqPanel.Visible = false;
		}

		#endregion

	}
}
