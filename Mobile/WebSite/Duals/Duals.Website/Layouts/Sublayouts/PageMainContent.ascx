﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageMainContent.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.PageMainContent" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="content row">
	<!-- begin content -->
	<sc:Placeholder runat="server" Key="duals-maincontent" />
	<!-- end content -->
</div>
<sc:Sublayout runat="server" Path="~/Duals/Duals.Website/Layouts/Sublayouts/MiniFooter.ascx" />
<sc:Sublayout runat="server" Path="~/Duals/Duals.Website/Layouts/Sublayouts/Footer.ascx" />