﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProviderSearch.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.ProviderSearch" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="cm" TagName="Error" Src="~/Duals/Duals.Website/Controls/ErrorMessage.ascx" %>

<!-- Begin SubLayouts/ProviderSearchPage -->
<div class="span8">
	<asp:Panel ID="SearchFormPnl" DefaultButton="SubmitBtn" runat="server">
		<div id="searchForProviders">
			<div class="form">
<asp:PlaceHolder ID ="SpecialtyPH" runat="server">
				<div class="formField long">
					<label>Specialty:</label>
					<div class="field">
						<asp:ListBox ID="SpecialtyDDL" CssClass="noUniform" runat="server" SelectionMode="Multiple" Rows="10" width = "340px" />
					</div>
				</div>
</asp:PlaceHolder>
				<div class="formField long">
					<label>Doctor's Last Name:</label>
					<div class="field">
						<asp:TextBox ID="LastNameTB" runat="server" width = "320px" />
					</div>
				</div>
				<div class="formField long">
					<label>Zip code:</label>
					<div class="field">
						<asp:TextBox ID="ZipCodeTB" runat="server" width = "320px" />
						<asp:RangeValidator ID="rngValidate_ZipCodeTB" runat="server"
							ControlToValidate="ZipCodeTB"
							Display="Dynamic"
							ErrorMessage="Enter a number between 10000 and 99999"
							MinimumValue="10000"
							MaximumValue="99999"
							Type="Integer"
							ValidationGroup="AllValidators">
						</asp:RangeValidator>
					</div>
				</div>
				<div class="formField long">
					<label>City:</label>
					<div class="field">
						<asp:TextBox ID="CityTB" runat="server" width = "320px" />
					</div>
				</div>
				<div class="formField long">
					<label>Language:</label>
					<div class="field">
						<asp:ListBox ID="LanguageDDL" runat="server" SelectionMode="Multiple" width = "340px" />
					</div>
				</div>
				<div class="formField long">
					<label>&nbsp;</label>
					<div class="field">
						<asp:LinkButton ID="SubmitBtn" runat="server" OnClick="SubmitBtn_OnClick" BackColor="Transparent" BorderStyle="None" CssClass="btn" ValidationGroup="AllValidators" />
					</div>
				</div>
			</div>
		</div>
	</asp:Panel>
	<cm:Error ID="errorMessage" runat="server" Visible="false" />
</div>
<!-- End SubLayouts/ProviderSearchPage -->
