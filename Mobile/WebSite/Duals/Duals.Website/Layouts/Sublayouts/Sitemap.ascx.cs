﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Pages;

using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Resources.Media;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class Sitemap : Controls.BaseSublayout
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			SetContent();
		}

		private void SetContent()
		{
			Homepage hp = Sitecore.Context.Database.GetItem(GlobalConsts.DualsHomeID).ToClass<Homepage>();

			// Start/Home node for TreeView
			TreeNode homeNode = new TreeNode();
			homeNode.Text = hp.NavigationTitle;
			homeNode.NavigateUrl = LinkManager.GetItemUrl(hp.InnerItem);

			AddChildPages(homeNode, hp);

			siteMapTV.Nodes.Add(homeNode);
		}

		/// <summary>
		/// Adds children nodes to the tree
		/// </summary>
		private void AddChildPages(TreeNode node, Duals.Models.Base.Page page)
		{
			// query all child pages with ShowInSiteMap checked
			var pages = page.InnerItem.Children.ToList()
				.ConvertAll(item => item.ToClass<Duals.Models.Base.Page>())
				.Where(p => p != null && p.ShowInSiteMap == (p.ShowInSiteMap.HasValue ? p.ShowInSiteMap.Value : true));

			// do only if child pages found
			foreach (var childPage in pages)
			{
				TreeNode childNode = new TreeNode();
				childNode.Text = childPage.NavigationTitle;
				childNode.NavigateUrl = LinkManager.GetItemUrl(childPage.InnerItem);

				AddChildPages(childNode, childPage);

				node.ChildNodes.Add(childNode);
			}
		}
	}
}