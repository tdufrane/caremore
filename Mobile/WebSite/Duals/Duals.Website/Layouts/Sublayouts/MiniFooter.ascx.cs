﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Pages;

using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class MiniFooter : Controls.BaseSublayout
	{
		private int _totalMiniFooterNavItems = 0;

		public string LastUpdatedOnDate
		{
			get
			{
				return Sitecore.Context.Item.Statistics.Updated.ToString("MM/dd/yy");
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			SetContent();
			BuildMiniFooterNav();
		}

		private void SetContent()
		{
			if (SiteSettings != null && SiteSettings.InnerItem != null)
			{
				scCode.Item = SiteSettings.InnerItem;
				scLastUpdatedOn.Item = SiteSettings.InnerItem;
			}
			else
			{
				Sitecore.Diagnostics.Log.Error("Duals: Cannot set MiniFooter content", this);
			}
		}

		/// <summary>
		/// Get Duals home item Childern and generate GlobalNav
		/// </summary>
		private void BuildMiniFooterNav()
		{
			try
			{
				var items = Sitecore.Context.Database.GetItem(GlobalConsts.DualsHomeID).Children.ToList()    // get all children of Duals home node
					.ConvertAll(item => item.ToClass<SitePage>())    // convert to SitePage class
					.Where(page => page != null && (page.ExcludeFromNavigation.HasValue ? !page.ExcludeFromNavigation.Value : true) && (page.AddToMiniFooterNav.HasValue ? page.AddToMiniFooterNav.Value : false));   // exclude null and not included in nav and specified to add to mini footer nav

				_totalMiniFooterNavItems = items.Count();

				miniFooterNavLV.DataSource = items;
				miniFooterNavLV.DataBind();
			}
			catch (Exception exc)
			{
				Sitecore.Diagnostics.Log.Error("Duals: Cannot build MiniFooterNav", exc, this);
			}
		}

		protected void miniFooterNavLV_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				ListViewDataItem LVDI = (ListViewDataItem)e.Item;
				Duals.Models.Base.Page page = (Duals.Models.Base.Page)LVDI.DataItem;

				HyperLink hl = (HyperLink)e.Item.FindControl("hl");
				hl.Text = page.NavigationTitle;
				hl.NavigateUrl = LinkManager.GetItemUrl(page.InnerItem);

				// if not last nav item append separator
				if (e.Item.DataItemIndex < _totalMiniFooterNavItems - 1)
				{
					Literal litSeparator = (Literal)e.Item.FindControl("litSeparator");
					litSeparator.Visible = true;
				}
			}
		}
	}
}