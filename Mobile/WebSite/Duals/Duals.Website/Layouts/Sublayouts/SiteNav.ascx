﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiteNav.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.SiteNav" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<asp:ListView id="siteNavLV" runat="server" OnItemDataBound="siteNavLV_ItemDataBound">
	<LayoutTemplate>
		<ul>
			<li runat="server" id="liHome"><asp:HyperLink runat="server" ID="hlHome" /></li>
			<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
		</ul><!-- end nav -->
	</LayoutTemplate>
	<ItemTemplate>
		<li runat="server" id="li"><asp:HyperLink runat="server" ID="hl" /></li>
	</ItemTemplate>
</asp:ListView>