﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProviderLoginWidget.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.ProviderLoginWidget" %>

<div style="clear: both;"></div>
<div id="secondaryPageContentWrap" style="margin-left: 249px; padding-top: 10px;">
<!-- Begin Sublayouts/Widgets/ProviderLoginWidget -->
<asp:Panel runat="server" DefaultButton="LinkButton1" ID="Panel1">
	<h2>CareMore Provider Portal Log-In</h2>
	<div class="brokerLogin">
		<div class="field username">
			<div class="left">
				<h3>User name:</h3>
			</div>
			<div class="right">
				<asp:TextBox ID="UserName" runat="server" MaxLength="100" ToolTip="User Name" CssClass="brokerLoginInput" />
			</div>
			<div style="clear: both;"></div>
		</div>

		<div class="field password">
			<div class="left">
				<h3>Password:</h3>
			</div>
			<div class="right">
				<asp:TextBox ID="UserPasswd" runat="server" MaxLength="50" TextMode="Password" ToolTip="Password" CssClass="brokerLoginInput" />
				
			</div>
			<div style="clear: both;"></div>
		</div>

		<div class="field button">
			<div class="left">
				<h3>&nbsp;</h3>
			</div>
			<div class="right">
				<asp:LinkButton ID="LinkButton1" CssClass="btn btnMed btnSubmit" runat="server" Text="<span>Submit</span>" OnClick="OnLoginSubmit" />
			</div>
		</div>
	</div>
</asp:Panel>
<!-- End Sublayouts/Widgets/ProviderLoginWidget -->
</div>