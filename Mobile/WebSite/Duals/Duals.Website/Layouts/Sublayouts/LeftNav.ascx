﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftNav.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.LeftNav" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="span4">
	<asp:ListView id="sideNavLV" runat="server" OnItemDataBound="sideNavLV_ItemDataBound" ItemPlaceholderID="itemPlaceholder1">
		<LayoutTemplate>
			<div id="sideNav">
				<h3><asp:Literal runat="server" ID="navTitle" /></h3>
				<ul>
					<asp:PlaceHolder ID="itemPlaceholder1" runat="server"></asp:PlaceHolder>
				</ul>
			</div><!-- end sideNav -->
		</LayoutTemplate>
		<ItemTemplate>
			<li runat="server" id="li"><asp:HyperLink runat="server" ID="hl" />
				<asp:ListView ID="sideSubNavLV" runat="server" OnItemDataBound="sideSubNavLV_ItemDataBound" ItemPlaceholderID="itemPlaceHolder2">
					<LayoutTemplate>
						<div style="margin-left: 20px; font-size: 95%; line-height: normal;"><asp:PlaceHolder ID="itemPlaceholder2" runat="server"></asp:PlaceHolder></div>
					</LayoutTemplate>
					<ItemTemplate>
						<p><asp:HyperLink runat="server" ID="subhl"></asp:HyperLink></p>
					</ItemTemplate>
				</asp:ListView>
			</li>
		</ItemTemplate>
	</asp:ListView>
</div>