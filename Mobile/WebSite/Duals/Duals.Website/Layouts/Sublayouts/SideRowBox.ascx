﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SideRowBox.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.SideRowBox" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="box">
	<sc:Image runat="server" ID="scImage" Field="Image" CssClass="pull-right" MaxWidth="31" MaxHeight="31" />
	<h3><sc:Text runat="server" ID="scTitle" Field="Title" /></h3>
	<sc:Text runat="server" ID="scContent" Field="Content" />
	<asp:ListView id="linksLV" runat="server">
		<LayoutTemplate>
			<ul class="quicklinks">
				<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
			</ul><!-- end globalNavLinks -->
		</LayoutTemplate>
		<ItemTemplate>
			<li><sc:Link runat="server" ID="scLink" Field="Link" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></li>
		</ItemTemplate>
	</asp:ListView>
</div>