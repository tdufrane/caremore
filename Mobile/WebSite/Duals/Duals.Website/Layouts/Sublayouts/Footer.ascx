﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.Footer" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<footer class="mainFooter">
	<div class="row">
		<div class="span12 language-help">
			<p><asp:hyperlink ID="languageFooterHL" runat="server"></asp:hyperlink></p>
		</div>
	</div>
	<div class="row">
		<div class="span8">
			<div class="row">
				<div class="span4">
					<asp:ListView id="siteNavLV" runat="server" OnItemDataBound="navLV_ItemDataBound">
						<LayoutTemplate>
							<ul>
								<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
							</ul>
						</LayoutTemplate>
						<ItemTemplate>
							<li><asp:HyperLink runat="server" ID="hl" /></li>
						</ItemTemplate>
					</asp:ListView>
				</div>
				<div class="span4">
					<asp:ListView id="middleNavLV" runat="server" OnItemDataBound="navLV_ItemDataBound">
						<LayoutTemplate>
							<ul>
								<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
							</ul>
						</LayoutTemplate>
						<ItemTemplate>
							<li><asp:HyperLink runat="server" ID="hl" /></li>
						</ItemTemplate>
					</asp:ListView>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<sc:Text runat="server" ID="scNonDiscrNotice" Field="Non Discrimination Notice" />
				</div>
			</div>
		</div>
		<div class="span4">
			<sc:Text runat="server" ID="scRightColumn" Field="Right Column Content" />
		</div>
	</div>
</footer>
