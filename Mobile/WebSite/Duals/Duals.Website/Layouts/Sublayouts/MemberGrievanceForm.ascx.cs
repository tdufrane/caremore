﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using Sitecore.Data.Items;
using System.Net.Mail;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class MemberGrievanceForm : Controls.BaseSublayout
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void SendMessageClick(object sender, EventArgs e)
		{
			DateTime confirmDate=DateTime.Now;
			lblError.Text = string.Empty;

			if (Page.IsValid && SendEmail(confirmDate))
			{
				if (SiteSettings != null && SiteSettings.InnerItem != null)
					scLogo.Item = SiteSettings.InnerItem;

				SubmitBtn.Enabled = false;

				modalConfirmPopup.Visible = true;
				ConfirmNumberLabel.Text = confirmDate.ToString("yyyy-dd-MM-hh-mm-ss");
				ConfirmDateLabel.Text = confirmDate.ToShortDateString();
				ConfirmTimeLabel.Text = confirmDate.ToShortTimeString();

				Type cstype = this.GetType();
				string scriptName = "submit_confirm";

				if (!Page.ClientScript.IsStartupScriptRegistered(cstype, scriptName))
				{
					String script = "$j(document).ready(function () {";
					script += "if ($j('#modalConfirmPopup').length) ";
					script += "$j('#ConfirmAnchor').click(); });";
					Page.ClientScript.RegisterStartupScript(cstype, scriptName, script, true);

					StringBuilder formContent = new StringBuilder();

					formContent.Append("<table>");
					formContent.Append("<tr><th style=\"width:40%;\">Member Last Name*</th><th style=\"width:40%;\">First*</th><th>Middle Initial</th></tr>");
					formContent.AppendFormat("<tr><td>{0}</td>", LastName.Text.Trim());
					formContent.AppendFormat("<td>{0}</td>", FirstName.Text.Trim());
					formContent.AppendFormat("<td>{0}</td></tr>", MiddleInitial.Text);
					formContent.Append("</table>");

					formContent.Append("<table>");
					formContent.Append("<tr><th>Address</th></tr>");
					formContent.AppendFormat("<tr><td>{0}</td></tr>", Address.Text.Trim());
					formContent.Append("</table>");

					formContent.Append("<table class=\"narrowTable\">");
					formContent.Append("<tr><th style=\"width:40%;\">City*</th><th style=\"width:40%;\">State*</th><th>Zip*</th></tr>");
					formContent.AppendFormat("<tr><td>{0}</td>", City.Text.Trim());
					formContent.AppendFormat("<td>{0}</td>", State.Text.Trim());
					formContent.AppendFormat("<td>{0}</td></tr>", ZipCode.Text);
					formContent.Append("</table>");

					formContent.Append("<table class=\"narrowTable\">");
					formContent.Append("<tr><th style=\"width:50%;\">Home Phone*</th><th style=\"width:50%;\">Work Phone</th></tr>");
					formContent.AppendFormat("<tr><td>{0}</td>", HomePhone.Text.Trim());
					formContent.AppendFormat("<td>{0}</td></tr>", WorkPhone.Text);
					formContent.Append("</table>");

					formContent.Append("<table class=\"narrowTable\">");
					formContent.AppendFormat("<tr><th style=\"width:30%;\">Date of Birth*</th><td style=\"width:70%;\">{0}</td></tr>", DateOfBirth.Text);
					formContent.AppendFormat("<tr><th>Member ID #*</th><td>{0}</td></tr>", MemberID.Text.Trim());
					formContent.Append("</table>");

					formContent.AppendFormat("<div>{0}</div>", RepFillText.InnerHtml);

					formContent.Append("<table class=\"narrowTable\">");
					formContent.AppendFormat("<tr><th style=\"width:40%;\">Name</th><td style=\"width:60%;\">{0}</td></tr>", RepName.Text.Trim());
					formContent.AppendFormat("<tr><th>Telephone</th><td>{0}</td></tr>",RepPhone.Text.Trim());
					formContent.AppendFormat("<tr><th>Relationship to Member</th><td>{0}</td></tr>", Relationship.Text.Trim());
					formContent.AppendFormat("<tr><th>Address</th><td>{0}</td></tr>", RepAddress.Text.Trim());
					formContent.AppendFormat("<tr><th>City</th><td>{0}</td></tr>", RepCity.Text.Trim());
					formContent.AppendFormat("<tr><th>State</th><td>{0}</td></tr>", RepState.Text.Trim());
					formContent.AppendFormat("<tr><th>Zip</th><td>{0}</td></tr>", RepZipCode.Text.Trim());
					formContent.Append("</table>");

					StringWriter sw = new StringWriter();
					HtmlTextWriter w = new HtmlTextWriter(sw);
					RepText.RenderControl(w);
					string divContent = sw.GetStringBuilder().ToString(); 
					w.Close();
					sw.Close();

					formContent.AppendFormat("<div>{0}</div>", divContent);

					formContent.AppendFormat("<div>{0}</div>", GrievanceLabel.InnerHtml);
					formContent.Append("<table>");
					formContent.AppendFormat("<tr><td><div style=\"min-height:50px;\">{0}</div></td></tr>", 
						Server.HtmlEncode(Grievance.Text.Trim()).Replace(Environment.NewLine, "<br />"));
					formContent.Append("</table>");

					formContent.AppendFormat("<div>{0}</div>", IllnessText.InnerHtml);

					formContent.Append("<div>");
					formContent.AppendFormat("[{0}]&nbsp;{1}<br />", RequestConferenceCheck.Checked ? "X" : "&nbsp;&nbsp;", RequestConferenceCheck.Text);
					formContent.AppendFormat("[{0}]&nbsp;{1}", ExpediteCheck.Checked ? "X" : "&nbsp;&nbsp;", ExpediteCheck.Text);
					formContent.Append("</div>");

					formContent.AppendFormat("<div style=\"padding: 10px 0px;\">{0}</div>", SubmitByText.InnerHtml);

					formContent.Append("<table class=\"noBorder\">");
					formContent.AppendFormat("<tr><th style=\"width:15%\">Signature:</th><td style=\"width:50%;\" class=\"bottomBorder\">{0}</td>",
								Signature.Text);
					formContent.AppendFormat("<th style=\"width:10%\">Date:</th><td style=\"width:25%;\" class=\"bottomBorder\">{0}</td></tr>", 
								SignatureDate.Text);
					formContent.AppendFormat("<tr><th>Representative:</th><td class=\"bottomBorder\">{0}</td>",
								Representative.Text);
					formContent.AppendFormat("<th>Date:</th><td class=\"bottomBorder\">{0}</td></tr>",
								RepDate.Text);
					formContent.Append("</table>");

					ConfirmFormContent.Text=formContent.ToString();
				}
			}
		}

		protected void CheckBoxRequired_ServerValidate(object sender, ServerValidateEventArgs e)
		{
			e.IsValid = ConfirmInfoRead.Checked;
		}

		private bool SendEmail(DateTime confirmDate)
		{
			Item currentItem = Sitecore.Context.Item;

			try
			{
				StringBuilder body = new StringBuilder();

				body.AppendFormat("Member Last Name: <b>{0}</b><br />", LastName.Text.Trim());
				body.AppendFormat("First Name: <b>{0}</b><br />", FirstName.Text.Trim());
				body.AppendFormat("Middle Initial: <b>{0}</b><br /><br />", MiddleInitial.Text.Trim());

				body.AppendFormat("Address: <b>{0}</b><br />", Address.Text.Trim());
				body.AppendFormat("City: <b>{0}</b><br />", City.Text.Trim());
				body.AppendFormat("State: <b>{0}</b><br />", State.Text.Trim());
				body.AppendFormat("Zip Code: <b>{0}</b><br />", ZipCode.Text.Trim());

				body.AppendFormat("Home Phone: <b>{0}</b><br />", HomePhone.Text.Trim());
				body.AppendFormat("Work Phone: <b>{0}</b><br /><br />", WorkPhone.Text.Trim());

				body.AppendFormat("Date of Birth: <b>{0}</b><br />", DateOfBirth.Text.Trim());
				body.AppendFormat("Member ID: <b>{0}</b><br /><br />", MemberID.Text.Trim());

				body.AppendFormat("<div>{0}</div>", RepFillText.InnerHtml);

				body.AppendFormat("Name: <b>{0}</b><br />", RepName.Text.Trim());
				body.AppendFormat("Telephone: <b>{0}</b><br />", RepPhone.Text.Trim());
				body.AppendFormat("Relationship to Member: <b>{0}</b><br />", Relationship.Text.Trim());
				body.AppendFormat("Address: <b>{0}</b><br />", RepAddress.Text.Trim());
				body.AppendFormat("City: <b>{0}</b><br />", RepCity.Text.Trim());
				body.AppendFormat("State: <b>{0}</b><br />", RepState.Text.Trim());
				body.AppendFormat("Zip: <b>{0}</b><br /><br />", RepZipCode.Text.Trim());

				body.AppendFormat("<div>{0}</div>", GrievanceLabel.InnerHtml);
				body.AppendFormat("<div><b>{0}</b></div>", Server.HtmlEncode(Grievance.Text.Trim()).Replace(Environment.NewLine, "<br />"));

				body.AppendFormat("<div>{0}<br /><br /></div>", IllnessText.InnerHtml);

				body.AppendFormat("[{0}]&nbsp;{1}<br />", RequestConferenceCheck.Checked ? "X" : "&nbsp;&nbsp;", RequestConferenceCheck.Text);
				body.AppendFormat("[{0}]&nbsp;{1}<br /><br />", ExpediteCheck.Checked ? "X" : "&nbsp;&nbsp;", ExpediteCheck.Text);

				body.AppendFormat("Signature: <u><b>{0}</b></u>Date: <u><b>{1}</b></u><br />", Signature.Text.PadRight(30).Replace(" ", "&nbsp"), 
					SignatureDate.Text.PadRight(12).Replace(" ", "&nbsp"));
				body.AppendFormat("Representative: <u><b>{0}</b></u>Date: <u><b>{1}</b></u><br /><br />", Representative.Text.PadRight(30).Replace(" ", "&nbsp"), 
					RepDate.Text.PadRight(12).Replace(" ", "&nbsp"));

				body.AppendFormat("Confirmation #: <b>{0}</b><br />", confirmDate.ToString("yyyy-dd-MM-hh-mm-ss"));
				body.AppendFormat("Date Received: <b>{0}</b><br />", confirmDate.ToShortDateString());
				body.AppendFormat("Time Received: <b>{0}</b><br />", confirmDate.ToShortTimeString());

				MailMessage message = new MailMessage();
				message.To.Add(new MailAddress(currentItem.Fields["Email To"].Value));
				message.From = new MailAddress(currentItem.Fields["Email From"].Value);
				message.Subject = currentItem.Fields["Email Subject"].Value;
				message.Body = body.ToString();
				message.IsBodyHtml = true;

				CareMore.Web.DotCom.Common.SendEmail(message);
			}
			catch 
			{
				lblError.Text = currentItem.Fields["Email Error Text"].HasValue ? currentItem.Fields["Email Error Text"].Value : "Submission error. Please try again later.";
				return false;
			}

			return true;
		}
	}
}
