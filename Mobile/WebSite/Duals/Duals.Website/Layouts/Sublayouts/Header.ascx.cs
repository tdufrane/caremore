﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Pages;

using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class Header : Controls.BaseSublayout
	{
		private int _totalGlobalNavItems = 0;

		protected void Page_Load(object sender, EventArgs e)
		{
			SetContent();
			BuildGlobalNav();
		}

		private void SetContent()
		{
			if (SiteSettings != null && SiteSettings.InnerItem != null)
			{
				scLogo.Item = SiteSettings.InnerItem;
				scTextSize.Item = SiteSettings.InnerItem;
				scTextSizeDownImage.Item = SiteSettings.InnerItem;
				scTextSizeUpImage.Item = SiteSettings.InnerItem;

				// Available languages
				if (!IsPostBack)
				{
					foreach (var lang in SiteSettings.AvailableLanguages)
					{
						ListItem li = new ListItem()
						{
							Value = lang.Name,
							Text = Sitecore.Data.Managers.LanguageManager.GetLanguage(lang.Name).CultureInfo.DisplayName,
							Selected = lang.Name == Sitecore.Context.Language.Name
						};
						languageSelectDDL.Items.Add(li);
					}
					languageSelectDDL.DataBind();
				}
			}
			else
			{
				Sitecore.Diagnostics.Log.Error("Duals: Cannot set header content", this);
			}
		}

		protected void btnSearch_OnClick(object sender, EventArgs args)
		{
			Response.Redirect("/Search.aspx?q=" + HttpUtility.UrlEncode(txtSearchInput.Value));
		}
		
		/// <summary>
		/// Get Duals home item Childern and generate GlobalNav
		/// </summary>
		private void BuildGlobalNav()
		{
			try
			{
				List<Duals.Models.Base.Page> pages = new List<Duals.Models.Base.Page>();

				pages.AddRange(Sitecore.Context.Database.GetItem(GlobalConsts.DualsHomeID).Children.ToList()    // get all children of Duals home node
					.ConvertAll(item => item.ToClass<SiteLandingPage>())    // convert to SiteLandingPage class
					.Where(page => page != null && (page.ExcludeFromNavigation.HasValue ? !page.ExcludeFromNavigation.Value : true)));   // exclude null and not included in nav and specified to add to global nav
	
				pages.AddRange(Sitecore.Context.Database.GetItem(GlobalConsts.DualsHomeID).Children.ToList()    // get all children of Duals home node
					.ConvertAll(item => item.ToClass<SitePage>())    // convert to SitePage class
					.Where(page => page != null && (page.ExcludeFromNavigation.HasValue ? !page.ExcludeFromNavigation.Value : true) && (page.AddToGlobalNav.HasValue ? page.AddToGlobalNav.Value : false)));   // exclude null and not included in nav and specified to add to global nav

				_totalGlobalNavItems = pages.Count();

				globalNavLV.DataSource = pages;
				globalNavLV.DataBind();
			}
			catch (Exception exc)
			{
				Sitecore.Diagnostics.Log.Error("Duals: Cannot build GlobalNav", exc, this);
			}
		}

		protected void globalNavLV_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				ListViewDataItem LVDI = e.Item as ListViewDataItem;
				Duals.Models.Base.Page page = LVDI.DataItem as Duals.Models.Base.Page;

				HyperLink hl = e.Item.FindControl("hl") as HyperLink;
				hl.Text = page.NavigationTitle;
				hl.NavigateUrl = LinkManager.GetItemUrl(page.InnerItem);

				// if not last nav item append separator
				if (e.Item.DataItemIndex < _totalGlobalNavItems - 1)
				{
					Literal litSeparator = e.Item.FindControl("litSeparator") as Literal;
					litSeparator.Visible = true;
				}
			}
		}

		protected void languageSelectDDL_SelectedIndexChanged(object sender, EventArgs e)
		{
			// set language
			Sitecore.Globalization.Language selectedLanguage = Sitecore.Data.Managers.LanguageManager.GetLanguage(languageSelectDDL.SelectedValue);
			Sitecore.Context.SetLanguage(selectedLanguage, true);

			// refresh page
			Page.Response.Redirect(Page.Request.Url.ToString(), true);
		}
	}
}