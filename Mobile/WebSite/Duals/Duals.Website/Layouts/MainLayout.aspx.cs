﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom;

namespace Duals.Website.Layouts
{
	public partial class MainLayout : Controls.BasePage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Common.IsInternalIpAddress(Request))
			{
				plcGoogleAnalytics.Visible = false;
			}
		}
	}
}
