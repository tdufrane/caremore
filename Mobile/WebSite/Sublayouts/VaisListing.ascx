﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="VaisListing.ascx.cs" Inherits="Website.Sublayouts.VaisListing" %>

<!-- Begin SubLayouts/VaisListing -->
<div class="row">
	<div class="col-xs-12">
<asp:Repeater ID="rptListing" runat="server">
	<ItemTemplate>
		<div class="row">
			<div class="col-xs-12">
				<sc:Link id="lnkWebsite1" runat="server" Field="Company URL" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>"><h2><sc:Text ID="txtCompanyName" runat="server" Field="Company Name" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></h2>
				<sc:Image ID="imgCompanyLogo" runat="server" Field="Company Logo" CssClass="pull-right" CssStyle="margin-left:10px;" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></sc:Link>
				<sc:Text ID="txtContent" runat="server" Field="Content" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p><sc:Text ID="txtPhone" runat="server" Field="Phone" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p><sc:Link id="lnkWebsite2" runat="server" Field="Company URL" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></p>
			</div>
		</div>
	</ItemTemplate>
	<SeparatorTemplate>
		<div class="row">
			<div class="col-xs-12">
				<hr />
			</div>
		</div>
	</SeparatorTemplate>
</asp:Repeater>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End SubLayouts/VaisListing -->
