﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ApplicationPage.ascx.cs" Inherits="Website.Sublayouts.ApplicationPage" %>
<%@ Register TagPrefix="cm" Namespace="CareMore.Web.DotCom.Controls" Assembly="CareMore.Web.DotCom"  %>

<!-- Begin SubLayouts/ApplicationPage -->
<div class="row">
	<div class="col-xs-12 appPage">
		<asp:HiddenField ID="hidState" runat="server" />
		<asp:HiddenField ID="hidPlanName" runat="server" />
		<asp:HiddenField ID="hidSitecoreId" runat="server" />

<asp:PlaceHolder ID="phApplicationDisclaimer" runat="server">
	<sc:Text ID="txtDisclaimer" runat="server" Field="Disclaimer" />
	<sc:Text ID="txtLocationPlan" runat="server" Field="Location and Plan" />
</asp:PlaceHolder>

<asp:PlaceHolder ID="phPlanHeader" runat="server" Visible="false">
		<div class="row">
			<div class="col-xs-4">
				<h3><asp:Label ID="lblPlanName" runat="server" /></h3>
				<p><strong><asp:Label ID="lblCountyName" runat="server" /> County</strong></p>
				<p><strong><asp:Label ID="lblPlanID" runat="server" /></strong></p>
				<p><strong><asp:Label ID="lblPlanPremium" runat="server" /></strong>
					<asp:PlaceHolder ID="phAdditionalPayment" runat="server">
						<br /><asp:Label ID="lblAdditionalPayment" runat="server" />
					</asp:PlaceHolder>
				</p>
			</div>
			<div class="col-xs-8">
				<asp:Literal ID="litNeedHelp" runat="server" />
			</div>
		</div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phConfirmation" runat="server" Visible="false">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>Thank you, your application has been received.</strong></div>
		<div class="panel-body">
			<p>Please note your confirmation number <asp:Label ID="lblConfNum" runat="server" Font-Bold="true" /> for future reference.
				We will contact you by phone with questions or updates about your application.
				We look forward to having you as a part of the CareMore family.</p>
			<p>Please save this confirmation for your records. Click the button below to print.</p>
			<p><a href="#" class="btn btn-info" onclick="javascript:window.print();" role="button">Print <span class="glyphicon glyphicon-print"></span></a></p>
		</div>
	</div>
	<asp:Literal ID="litCustomCodeSubmit" runat="server" />
</asp:PlaceHolder>

<asp:PlaceHolder ID="phStatement" runat="server" Visible="false">
	<asp:Literal ID="litStatementOfUnderstanding" runat="server" />
	<p><asp:LinkButton ID="lnkBtnAgreedSOU" runat="server" OnClick="LnkBtn_StatementAgreedClick" CssClass="btn btn-primary">I Agree</asp:LinkButton></p>
	<asp:Literal ID="litCustomCodeLoad" runat="server" />
</asp:PlaceHolder>

<asp:PlaceHolder ID="phApplicationForm" runat="server" Visible="false">
	<hr class="no-padding-top" />

	<asp:Literal ID="litBeginEnrollment" runat="server" />

	<p class="text-danger">* <asp:Label ID="lblRequired" runat="server" /></p>

	<asp:UpdatePanel ID="updPnlForm" runat="server">
		<ContentTemplate>

	<div class="form-horizontal">
		<div class="panel panel-default">
			<div class="panel-heading">
				To enroll in CareMore Health Plan, provide the following information
			</div>
			<div class="panel-body">
				<asp:Panel id="pnlCoverageEffectiveDate" runat="server" CssClass="form-group">
					<label for="txtCoverageEffectiveDate" class="col-xs-12 col-md-3 control-label">
						Coverage Effective Date <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-2">
						<asp:TextBox ID="txtCoverageEffectiveDate" runat="server" CssClass="form-control" MaxLength="15" placeholder="mm/dd/yyyy" />
					</div>
					<div class="col-xs-12 col-md-offset-3 col-md-4">
						<asp:RequiredFieldValidator ID="reqValCoverageEffectiveDate" runat="server" ControlToValidate="txtCoverageEffectiveDate"
							SetFocusOnError="true" ErrorMessage="Required" />
						<asp:CompareValidator ID="cmpValCoverageEffectiveDate" runat="server" ControlToValidate="txtCoverageEffectiveDate"
							SetFocusOnError="true" ErrorMessage="Format is incorrect"
							Operator="DataTypeCheck" Type="Date" />
					</div>
				</asp:Panel>
				<div class="form-group">
					<label for="txtLastName" class="col-xs-12 col-md-3 control-label">
						Last Name <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" MaxLength="50" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValLastName" runat="server" ControlToValidate="txtLastName"
							SetFocusOnError="true" ErrorMessage="Required" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtFirstName" class="col-xs-12 col-md-3 control-label">
						First Name <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" MaxLength="50" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValValFirstName" runat="server" ControlToValidate="txtFirstName"
							SetFocusOnError="true" ErrorMessage="Required" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtMiddleInitial" class="col-xs-12 col-md-3 control-label">
						Middle Initial
					</label>
					<div class="col-xs-2 col-md-1">
						<asp:TextBox ID="txtMiddleInitial" runat="server" CssClass="form-control" MaxLength="1" />
					</div>
				</div>
				<div class="form-group">
					<label for="rblSalutation" class="col-xs-12 col-md-3 control-label">
						Salutation
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblSalutation" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="Mr">Mr.</asp:ListItem>
							<asp:ListItem Value="Mrs">Mrs.</asp:ListItem>
							<asp:ListItem Value="Ms">Ms.</asp:ListItem>
						</asp:RadioButtonList>
					</div>
				</div>
				<div class="form-group">
					<label for="txtBirthDate" class="col-xs12 col-md-3 control-label">
						Birth Date <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-2">
						<asp:TextBox ID="txtBirthDate" runat="server" CssClass="form-control" MaxLength="10" placeholder="mm/dd/yyyy" />
					</div>
					<div class="col-xs-12 col-md-offset-3 col-md-4">
						<asp:RequiredFieldValidator ID="reqValBirthDate" runat="server" ControlToValidate="txtBirthDate"
							SetFocusOnError="true" ErrorMessage="Required" />
						<asp:CompareValidator ID="cmpValBirthDate" runat="server" ControlToValidate="txtBirthDate"
							SetFocusOnError="true" ErrorMessage="Format is incorrect"
							Operator="DataTypeCheck" Type="Date" />
					</div>
				</div>
				<div class="form-group">
					<label for="rblSex" class="col-xs-12 col-md-3 control-label">
						Sex <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:RadioButtonList ID="rblSex" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem>M</asp:ListItem>
							<asp:ListItem>F</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValSex" runat="server" ControlToValidate="rblSex"
							SetFocusOnError="true" ErrorMessage="Required" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtHomePhone" class="col-xs-12 col-md-3 control-label">
						Home Phone <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtHomePhone" runat="server" CssClass="form-control" MaxLength="15" placeholder="###-###-####" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValHomePhone" runat="server" ControlToValidate="txtHomePhone"
							SetFocusOnError="true" ErrorMessage="Required" />
						<asp:RegularExpressionValidator ID="regExValHomePhone" runat="server" ControlToValidate="txtHomePhone"
							SetFocusOnError="true" ErrorMessage="Format is incorrect"
							ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$" />
					</div>
				</div>
				<asp:Panel id="pnlAlternativePhone" runat="server" class="form-group">
					<label for="txtAltPhone" class="col-xs-12 col-md-3 control-label">
						Alternative Phone
					</label>
					<div class="col-xs-12 col-md-5">
						<div class="row">
							<div class="col-xs-12">
								<asp:TextBox ID="txtAltPhone" runat="server" CssClass="form-control" MaxLength="15" placeholder="###-###-####" />
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RegularExpressionValidator ID="regExValAltPhone" runat="server" ControlToValidate="txtAltPhone"
							SetFocusOnError="true" ErrorMessage="Format is incorrect"
							ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$" />
					</div>
				</asp:Panel>
				<asp:Panel ID="pnlIsCellPhone" runat="server" CssClass="form-group">
					<div class="col-xs-12 col-md-offset-3 col-md-6">
						<div class="row">
							<div class="col-xs-6 col-md-4">
								Is this a cellphone?
							</div>
							<div class="col-xs-6 col-md-8">
								<asp:RadioButtonList ID="rblIsCellPhone" runat="server" CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
						</div>
					</div>
				</asp:Panel>
				<div class="form-group">
					<label for="txtAddress" class="col-xs-12 col-md-3 control-label">
						Permanent Residence Address <br class="hidden-xs" />(P.O. Box is not allowed) <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" MaxLength="80" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValAddress" runat="server" ControlToValidate="txtAddress"
							SetFocusOnError="true" ErrorMessage="Required" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtCity" class="col-xs-12 col-md-3 control-label">
						City <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtCity" runat="server" CssClass="form-control" MaxLength="50" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValCity" runat="server" ControlToValidate="txtCity"
							SetFocusOnError="true" ErrorMessage="Required" />
					</div>
				</div>
				<div class="form-group">
					<label for="ddlState" class="col-xs-12 col-md-3 control-label">
						State <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:DropDownList ID="ddlState" runat="server" CssClass="form-control" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValState" runat="server" ControlToValidate="ddlState"
							SetFocusOnError="true" ErrorMessage="Required" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtZip" class="col-xs-12 col-md-3 control-label">
						Zip Code <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtZip" CssClass="form-control" runat="server" MaxLength="10" placeholder="#####" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValZip" runat="server" ControlToValidate="txtZip"
							SetFocusOnError="true" ErrorMessage="Required" />
						<asp:RegularExpressionValidator ID="regExValZip" runat="server" ControlToValidate="txtZip"
							SetFocusOnError="true" ErrorMessage="Format is incorrect"
							ValidationExpression="^[0-9]{5}$" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtAddress2" class="col-xs-12 col-md-3 control-label">
						Mailing address <br class="hidden-xs" />(only if different from above)
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control" MaxLength="80" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtCity2" class="col-xs-12 col-md-3 control-label">
						Mailing City
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtCity2" runat="server" CssClass="form-control" MaxLength="50" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtState2" class="col-xs-12 col-md-3 control-label">
						Mailing State
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtState2" runat="server" CssClass="form-control" MaxLength="50" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtZip2" class="col-xs-12 col-md-3 control-label">
						Mailing Zip Code
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtZip2" runat="server" CssClass="form-control" MaxLength="10" placeholder="#####" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RegularExpressionValidator ID="regExValZip2" runat="server" ControlToValidate="txtZip2"
							SetFocusOnError="true" ErrorMessage="Format is incorrect"
							ValidationExpression="^[0-9]{5}$" />
					</div>
				</div>
				<asp:Panel id="pnlMoved" runat="server" CssClass="form-group">
					<label for="rblMoved" class="col-xs-12 col-md-3 control-label">
						Have you moved during calendar year? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<div class="form-group">
							<div class="col-xs-12">
								<asp:RadioButtonList ID="rblMoved" runat="server" AutoPostBack="true" CssClass="radio-inline"
									CausesValidation="false" RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblMoved_SelectedIndexChanged">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<asp:Label id="lblDateOfMove" runat="server" Text="Date of Move" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:TextBox ID="txtDateOfMove" runat="server" CssClass="form-control" MaxLength="10" Enabled="false" placeholder="mm/dd/yyyy" />
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValDateOfMove" runat="server" ControlToValidate="txtDateOfMove"
									SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
								<asp:CompareValidator ID="cmpValDateOfMove" runat="server" ControlToValidate="txtDateOfMove"
									SetFocusOnError="true" ErrorMessage="Format is incorrect" Enabled="false" />
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValMoved" ControlToValidate="rblMoved" runat="server"
							SetFocusOnError="true" ErrorMessage="Required" />
					</div>
				</asp:Panel>
				<div class="form-group">
					<label for="txtEmergencyContact" class="col-xs-12 col-md-3 control-label">
						Emergency Contact <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtEmergencyContact" runat="server" CssClass="form-control" MaxLength="80" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValEmergencyContact" runat="server" ControlToValidate="txtEmergencyContact"
							SetFocusOnError="true" ErrorMessage="Required" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtEmergencyPhone" class="col-xs-12 col-md-3 control-label">
						Emergency Phone Number <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtEmergencyPhone" runat="server" CssClass="form-control" MaxLength="15" placeholder="###-###-####" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValEmergencyPhone" runat="server" ControlToValidate="txtEmergencyPhone"
							SetFocusOnError="true" ErrorMessage="Required" />
						<asp:RegularExpressionValidator ID="regExValEmergencyPhone" runat="server" ControlToValidate="txtEmergencyPhone"
							SetFocusOnError="true" ErrorMessage="Format is incorrect"
							ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtRelation" class="col-xs-12 col-md-3 control-label">
						Relationship to you <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtRelation" runat="server" CssClass="form-control" MaxLength="80" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValRelation" runat="server" ControlToValidate="txtRelation"
							SetFocusOnError="true" ErrorMessage="Required" />
					</div>
				</div>
				<asp:Panel ID="pnlEmail" runat="server" CssClass="form-group">
					<label for="txtEmailAddress" class="col-xs-12 col-md-3 control-label">
						Email Address <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtEmailAddress" runat="server" CssClass="form-control" MaxLength="100" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValEmailAddress" runat="server" ControlToValidate="txtEmailAddress"
							SetFocusOnError="true" ErrorMessage="Required" />
						<asp:RegularExpressionValidator  id="regExValEmailAddress" runat="server" EnableClientScript="true" ControlToValidate="txtEmailAddress"
							SetFocusOnError="true" ErrorMessage="Invalid email"
							ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
					</div>
				</asp:Panel>
			</div>
		</div>

<asp:PlaceHolder ID="phConsentToContact" runat="server">
		<div class="panel panel-default">
			<div class="panel-heading">
				Consent to Contact by Email
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						<asp:Literal ID="litConsentToContactText" runat="server"></asp:Literal>
					</div>
				</div>
				<div class="form-group">
					<label for="cbReceiveDocsByEmail" class="col-xs-12 col-md-3 control-label">
						Check one or both options
					</label>
					<div class="col-xs-12 col-md-8 top-spacer">
						<div class="row">
							<div class="col-xs-12">
								<asp:CheckBox ID="cbReceiveDocsByEmail" runat="server" />
								I agree to receive my Annual Notice of Changes, Evidence of Coverage and List of Covered Drugs (Formulary) by email.
							</div>
						</div>
						<div class="row top-spacer">
							<div class="col-xs-12">
								<asp:CheckBox ID="cbReceiveInfoByEmail" runat="server" />
								I would like to receive information regarding plan benefits, health programs, tips, newsletters, surveys, general information and other plan services by email.
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="txtReceiveEmailAddress" class="col-xs-12 col-md-3 control-label">
						Email Address
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtReceiveEmailAddress" runat="server" CssClass="form-control" MaxLength="100" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RegularExpressionValidator ID="regExValReceiveEmailAddress" runat="server" ControlToValidate="txtReceiveEmailAddress"
							SetFocusOnError="true" ErrorMessage="Format for 'Email Address' is incorrect."
							ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
					</div>
				</div>
				<div class="row no-top-margin">
					<div class="col-xs-12">
						<hr />
						If you choose not to participate in the email delivery program, CareMore will mail the information to you.
					</div>
				</div>
			</div>
		</div>
</asp:PlaceHolder>

		<div class="panel panel-default">
			<div class="panel-heading">
				Provide Your Medicare Insurance Information Below
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						<asp:Literal ID="litMedicareText" runat="server" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtMedicareName" class="col-xs-12 col-md-3 control-label">
						Name on Medicare Card <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtMedicareName" runat="server" CssClass="form-control" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValMedicareName" runat="server" ControlToValidate="txtMedicareName"
							SetFocusOnError="true" ErrorMessage="Required" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtMedicareClaimNumber" class="col-xs-12 col-md-3 control-label">
						Medicare Claim Number <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtMedicareClaimNumber" runat="server" CssClass="form-control" MaxLength="14" placeholder="###-##-####-a"/>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValMedicareClaimNumber" runat="server" ControlToValidate="txtMedicareClaimNumber"
							SetFocusOnError="true" ErrorMessage="Required" />
					</div>
				</div>
				<div class="form-group">
					<label for="rblMedicareSex" class="col-xs-12 col-md-3 control-label">
						Sex
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblMedicareSex" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem>M</asp:ListItem>
							<asp:ListItem>F</asp:ListItem>
						</asp:RadioButtonList>
					</div>
				</div>
				<div class="form-group">
					<label for="txtHospitalBenefitDate" class="col-xs-12 col-md-3 control-label">
						Is Entitled To - HOSPITAL (Part A) <br class="hidden-xs" />Effective Date
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtHospitalBenefitDate" runat="server" CssClass="form-control" MaxLength="10" placeholder="mm/dd/yyyy" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:CompareValidator ID="cmpValHospitalBenefitDate" runat="server" ControlToValidate="txtHospitalBenefitDate"
							SetFocusOnError="true" ErrorMessage="Format is incorrect"
							Operator="DataTypeCheck" Type="Date" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtMedicalBenefitDate" class="col-xs-12 col-md-3 control-label">
						Is Entitled To - MEDICAL (Part B) <br class="hidden-xs" />Effective Date
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtMedicalBenefitDate" runat="server" CssClass="form-control"  MaxLength="10" placeholder="mm/dd/yyyy" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:CompareValidator ID="cmpValMedicalBenefitDate" runat="server" ControlToValidate="txtMedicalBenefitDate"
							SetFocusOnError="true" ErrorMessage="Format is incorrect"
							Operator="DataTypeCheck" Type="Date" />
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				Paying Your Plan Premium
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						<asp:Label ID="lblPaymentPlan" runat="server" />
					</div>
				</div>
				<asp:PlaceHolder id="phPaymentPlans" runat="server">
					<div class="form-group top-spacer">
						<label for="rblPaymentOption" class="col-xs-12 col-md-3 control-label">
							Please select a premium <br class="hidden-xs" />payment option (if applicable):
						</label>
						<div class="col-xs-12 col-md-6">
							<asp:RadioButtonList ID="rblPaymentOption" runat="server" AutoPostBack="True" CausesValidation="false"
								CssClass="radio-inline" RepeatDirection="Vertical" RepeatLayout="Flow"
								OnSelectedIndexChanged="RblPaymentOption_SelectedIndexChanged">
								<asp:ListItem Value="Receive a bill">Get a bill</asp:ListItem>
								<asp:ListItem Value="Electronic funds transfer (EFT)">Electronic funds transfer (EFT) from your bank account each month</asp:ListItem>
								<asp:ListItem Value="Credit Card">Credit Card</asp:ListItem>
								<asp:ListItem Value="Automatic deduction">Automatic deduction from your monthly Social Security or Railroad Retirement Board (RRB) benefit check. (The Social Security/RRB deduction may take two or more months to begin after Social Security or RRB approves the deduction. In most cases, if Social Security or RRB accepts your request for automatic deduction, the first deduction from your Social Security or RRB benefit check will include all premiums due from your enrollment effective date up to the point withholding begins. If Social Security or RRB does not approve your request for automatic deduction, we will send you a paper bill for your monthly premiums.)</asp:ListItem>
							</asp:RadioButtonList>
						</div>
					</div>
					<asp:Panel ID="pnlPaymentPlanDetails" runat="server" CssClass="row" Visible="false">
						<div class="col-xs-12 col-md-offset-3 col-md-8">
							<asp:PlaceHolder ID="phBankAccount" runat="server" Visible="false">
								<p class="text-info">Bank details for Electronic funds transfer (EFT):</p>

								<div class="form-group">
									<label for="txtAccountHolderName" class="col-xs-12 col-md-3 control-label">
										Account holder name
									</label>
									<div class="col-xs-12 col-md-4">
										<asp:TextBox ID="txtAccountHolderName" runat="server" MaxLength="50" CssClass="form-control" Enabled="false" />
									</div>
									<div class="col-xs-12 col-md-4">
										<asp:RequiredFieldValidator ID="reqValAccountHolderName" runat="server" ControlToValidate="txtAccountHolderName"
											SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<label for="rblAccountType" class="col-xs-12 col-md-3 control-label">
										Account type
									</label>
									<div class="col-xs-12 col-md-4">
										<asp:RadioButtonList ID="rblAccountType" runat="server" CssClass="radio-inline"
											RepeatDirection="Horizontal" RepeatLayout="Flow" Enabled="false">
											<asp:ListItem>Checking</asp:ListItem>
											<asp:ListItem>Saving</asp:ListItem>
										</asp:RadioButtonList>
									</div>
									<div class="col-xs-12 col-md-4">
										<asp:RequiredFieldValidator ID="reqValAccountType" runat="server" ControlToValidate="rblAccountType"
											SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<label for="txtRoutingNumber" class="col-xs-12 col-md-3 control-label">
										Routing number
									</label>
									<div class="col-xs-12 col-md-4">
										<asp:TextBox ID="txtRoutingNumber" runat="server" MaxLength="25" CssClass="form-control" Enabled="false" />
									</div>
									<div class="col-xs-12 col-md-4">
										<asp:RequiredFieldValidator ID="reqValRoutingNumber" runat="server" ControlToValidate="txtRoutingNumber"
											SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<label for="txtAccountNumber" class="col-xs-12 col-md-3 control-label">
										Account number
									</label>
									<div class="col-xs-12 col-md-4">
										<asp:TextBox ID="txtAccountNumber" runat="server" MaxLength="25" CssClass="form-control" Enabled="false"/>
									</div>
									<div class="col-xs-12 col-md-4">
										<asp:RequiredFieldValidator ID="reqValAccountNumber" runat="server" ControlToValidate="txtAccountNumber"
											SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
									</div>
								</div>
							</asp:PlaceHolder>

							<asp:PlaceHolder ID="phCreditCardInfo" runat="server" Visible="false">
								<p class="text-info">Credit Card Information:</p>

								<div class="form-group">
									<label for="ddlCreditCardType" class="col-xs-12 col-md-3 control-label">Type of Card</label>
									<div class="col-xs-12 col-md-4">
										<asp:DropDownList ID="ddlCreditCardType" runat="server" CssClass="form-control">
											<asp:ListItem>Visa</asp:ListItem>
											<asp:ListItem>Mastercard</asp:ListItem>
											<asp:ListItem>Discover</asp:ListItem>
											<asp:ListItem>American Express</asp:ListItem>
										</asp:DropDownList>
									</div>
								</div>
								<div class="form-group">
									<label for="txtCreditCardAccountHolderName" class="col-xs-12 col-md-3 control-label">Name of Account holder as it appears on card</label>
									<div class="col-xs-12 col-md-4">
										<asp:TextBox ID="txtCreditCardAccountHolderName" runat="server" MaxLength="50" CssClass="form-control" />
									</div>
									<div class="col-xs-12 col-md-4">
										<asp:RequiredFieldValidator ID="reqValCCAccountName" runat="server" ControlToValidate="txtCreditCardAccountHolderName"
											SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<label for="txtCreditCardAccountNumber" class="col-xs-12 col-md-3 control-label">
										Account number
									</label>
									<div class="col-xs-12 col-md-4">
										<asp:TextBox ID="txtCreditCardAccountNumber" runat="server" MaxLength="25" CssClass="form-control" />
									</div>
									<div class="col-xs-12 col-md-4">
										<asp:RequiredFieldValidator ID="reqValCCAccountNumber" runat="server" ControlToValidate="txtCreditCardAccountNumber"
											SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<label for="ddlCCExpirationMonth" class="col-xs-12 col-md-3 control-label">Expiration Date (MM/YYYY)</label>
									<div class="col-xs-12 col-md-2">
										<asp:DropDownList ID="ddlCCExpirationMonth" runat="server" CssClass="form-control">
											<asp:ListItem>01</asp:ListItem>
											<asp:ListItem>02</asp:ListItem>
											<asp:ListItem>03</asp:ListItem>
											<asp:ListItem>04</asp:ListItem>
											<asp:ListItem>05</asp:ListItem>
											<asp:ListItem>06</asp:ListItem>
											<asp:ListItem>07</asp:ListItem>
											<asp:ListItem>08</asp:ListItem>
											<asp:ListItem>09</asp:ListItem>
											<asp:ListItem>10</asp:ListItem>
											<asp:ListItem>11</asp:ListItem>
											<asp:ListItem>12</asp:ListItem>
										</asp:DropDownList>
									</div>
									<div class="col-xs-12 col-md-2">
										<asp:DropDownList ID="ddlCCExpirationYear" runat="server" CssClass="form-control"></asp:DropDownList>
									</div>
								</div>
							</asp:PlaceHolder>
						</div>
					</asp:Panel>
				</asp:PlaceHolder>
			</div>
		</div>

<asp:PlaceHolder ID="phChronicSNP" runat="server">
		<div class="panel panel-default">
			<div class="panel-heading">
				PRE-ENROLLMENT QUALIFICATION ASSESSMENT FOR CHRONIC SPECIAL NEEDS PLANS
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						Complete if you have a chronic condition. If the answer is "Yes" to at least one of the questions, you pre-qualify for the condition(s).
					</div>
				</div>
				<div class="row">
					<label for="rblSNP_ESRD" class="col-xs-12 col-md-3">
						<div class="row">
							<div class="col-xs-12 control-label">
								Are you on dialysis <span class="text-danger">*</span><br />
								(End-Stage Renal Disease - ESRD)?
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 control-text">
								If you have had a successful kidney transplant and/or you don’t need regular dialysis any more, we will contact
								you for records from your doctor showing you have had a successful kidney transplant or you don't need dialysis.
							</div>
						</div>
					</label>
					<div class="col-xs-12 col-md-8">
						<div class="form-group">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblSNP_ESRD" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValSNP_ESRD" runat="server" ControlToValidate="rblSNP_ESRD"
									SetFocusOnError="true" ErrorMessage="Answer is required" />
							</div>
						</div>
						<div class="form-group top-spacer">
							<div class="col-xs-12 col-md-3">
								Current Dialysis Center
							</div>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtCurrentDialysisCenter" runat="server" CssClass="form-control" MaxLength="100" /></div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-3">
								Phone (no 800 #s)
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:TextBox ID="txtCurrentDialysisPhone" runat="server" CssClass="form-control" MaxLength="15" placeholder="###-###-####" />
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RegularExpressionValidator ID="regExValCurrentDialysisPhone" runat="server" ControlToValidate="txtCurrentDialysisPhone"
									SetFocusOnError="true" ErrorMessage="Format for 'Current Dialysis Center Phone' is incorrect."
									ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-3">
								New Nephrologist First Name
							</div>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtNephrologistFirstName" runat="server" CssClass="form-control" MaxLength="50" /></div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-3">
								New Nephrologist Last Name
							</div>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtNephrologistLastName" runat="server" CssClass="form-control" MaxLength="50" /></div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-3">
								New Dialysis Center
							</div>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtNewDialysisCenter" runat="server" CssClass="form-control" MaxLength="100" /></div>
						</div>
					</div>
				</div>
				<div class="row">
					<label class="col-xs-12 col-md-3 control-label">
						Have you been diagnosed with? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-8">
						<div class="form-group">
							<label class="col-xs-12 col-md-2 control-text">
								Asthma
							</label>
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblSNP_Asthma" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValSNP_Asthma" runat="server" ControlToValidate="rblSNP_Asthma"
									SetFocusOnError="true" ErrorMessage="Answer is required" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-12 col-md-2 control-text">
								Emphysema
							</label>
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblSNP_Emphysema" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValSNP_Emphysema" runat="server" ControlToValidate="rblSNP_Emphysema"
									SetFocusOnError="true" ErrorMessage="Answer is required" />
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3 control-label">
						Have you been told by a doctor that you have diabetes (too much sugar in the blood or urine)? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblSNP_Diabetes" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="True">Yes</asp:ListItem>
							<asp:ListItem Value="False">No</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValSNP_Diabetes" runat="server" ControlToValidate="RblSNP_Diabetes"
							SetFocusOnError="true" ErrorMessage="Answer is required" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3 control-label">
						Have you ever been prescribed or are you taking insulin or an oral medication that is supposed to lower the sugar in your blood? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblSNP_Insulin" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="True">Yes</asp:ListItem>
							<asp:ListItem Value="False">No</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValSNP_Insulin" runat="server" ControlToValidate="rblSNP_Insulin"
							SetFocusOnError="true" ErrorMessage="Answer is required" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3 control-label">
						Have you ever been told by a doctor that you have coronary artery disease, poor circulation due to hardening of the arteries or poor veins? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblSNP_CoronaryArteryDisease" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="True">Yes</asp:ListItem>
							<asp:ListItem Value="False">No</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValSNP_CoronaryArteryDisease" runat="server" ControlToValidate="rblSNP_CoronaryArteryDisease"
							SetFocusOnError="true" ErrorMessage="Answer is required" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3 control-label">
						Have you ever had a heart attack or been admitted to the hospital for angina (chest pain)? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblSNP_HeartAttack" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="True">Yes</asp:ListItem>
							<asp:ListItem Value="False">No</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValSNP_HeartAttack" runat="server" ControlToValidate="rblSNP_HeartAttack"
							SetFocusOnError="true" ErrorMessage="Answer is required" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3 control-label">
						Have you ever been told by a doctor that you have heart failure (weak heart)? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblSNP_HeartFailure" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="True">Yes</asp:ListItem>
							<asp:ListItem Value="False">No</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValSNP_HeartFailure" runat="server" ControlToValidate="rblSNP_HeartFailure"
							SetFocusOnError="true" ErrorMessage="Answer is required" />
					</div>
				</div>
				<div class="row">
					<label class="col-xs-12 col-md-3 control-label">
						Have you ever had problems with fluid in your lungs and swelling in your legs in the past, accompanied by shortness of breath, due to a heart problem? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblSNP_FluidSwelling" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="True">Yes</asp:ListItem>
							<asp:ListItem Value="False">No</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValSNP_FluidSwelling" runat="server" ControlToValidate="rblSNP_FluidSwelling"
							SetFocusOnError="true" ErrorMessage="Answer is required" />
					</div>
				</div>
				<div class="row">
					<label class="col-xs-12 col-md-3 control-label">
						<div class="row">
							<div class="col-xs-12 control-label">
								Treating Specialist for Special Needs Plan (SNP) Qualifying Disease
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 control-text">
								If you are seeing a Specialist for a chronic condition, please provide us with their information so we can contact them on your behalf.
							</div>
						</div>
					</label>
					<div class="col-xs-12 col-md-8">
						<div class="form-group">
							<label class="col-xs-12 col-md-2 control-text">
								First Name
							</label>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtTreatingFirstName" runat="server" CssClass="form-control" MaxLength="50" /></div>
						</div>
						<div class="form-group">
							<label class="col-xs-12 col-md-2 control-text">
								Last Name
							</label>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtTreatingLastName" runat="server" CssClass="form-control" MaxLength="50" /></div>
						</div>
						<div class="form-group">
							<label class="col-xs-12 col-md-2 control-text">
								Specialty Type
							</label>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtTreatingSpecialty" runat="server" CssClass="form-control" MaxLength="50" /></div>
						</div>
						<div class="form-group">
							<label class="col-xs-12 col-md-2 control-text">
								Phone
							</label>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtTreatingPhone" runat="server" CssClass="form-control" MaxLength="15" placeholder="###-###-####" /></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				Chronic Special Needs Acknowledgment
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						<asp:Literal ID="litChronicSpecialNeedsAcknowledgment" runat="server" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3">
						Enrollee Initials <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtEnrolleeInitials" runat="server" CssClass="form-control" MaxLength="3" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValEnrolleeInitials" runat="server" ControlToValidate="txtEnrolleeInitials"
							SetFocusOnError="true" ErrorMessage="Required" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3">
						Date <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtEnrolleeDate" runat="server" CssClass="form-control" MaxLength="10" placeholder="mm/dd/yyyy" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValEnrolleeDate" runat="server" ControlToValidate="txtEnrolleeDate"
							SetFocusOnError="true" ErrorMessage="Required" />
						<asp:CompareValidator ID="cmpValEnrolleeDate" runat="server" ControlToValidate="txtEnrolleeDate"
							SetFocusOnError="true" ErrorMessage="Format is incorrect"
							Operator="DataTypeCheck" Type="Date" />
					</div>
				</div>
			</div>
		</div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phMedicationsAcknowledgment" runat="server">
		<div class="panel panel-default">
			<div class="panel-heading">
				Medications Acknowledgment <span class="text-danger">*</span>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12 col-md-8">
						<asp:CheckBoxList ID="cblMedications" runat="server" CssClass="checkbox-inline">
							<asp:ListItem>
								I acknowledge I am responsible to contact my existing provider(s) to obtain any refills for my medications within the next 30 days prior to my coverage effective date with CareMore Health Plan.
							</asp:ListItem>
						</asp:CheckBoxList>
					</div>
					<div class="col-xs-12 col-md-4">
						<cm:RequiredFieldValidatorForCheckBoxLists ID="reqValMedications" runat="server" ControlToValidate="cblMedications"
							SetFocusOnError="true" ErrorMessage="Please acknowledge" />
					</div>
				</div>
			</div>
		</div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phContinuityCareCoordination" runat="server">
		<div class="panel panel-default">
			<div class="panel-heading">
				Continuity of Care Coordination
			</div>
			<div class="panel-body">
				<div class="form-group">
					<label class="col-xs-12 col-md-3">
						<span class="underline">Planned Procedures or Surgeries</span><br />
						Do you have any procedures or surgeries planned within 30 days of your effective date of coverage? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-8">
						<div class="form-group">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblPlannedProcedures" runat="server" AutoPostBack="true"
									CausesValidation="false" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblPlannedProcedures_SelectedIndexChanged">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValPlannedProcedures" runat="server" ControlToValidate="rblPlannedProcedures"
									SetFocusOnError="true" ErrorMessage="Answer is required" />
							</div>
						</div>
						<div class="row">
							<label class="col-xs-12">
								Treatment details
							</label>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-8">
								<asp:TextBox ID="txtTreatmentDetails" runat="server" CssClass="form-control" TextMode="MultiLine" Columns="50" Rows="4" MaxLength="500" Enabled="false" />
							</div>
						</div>
						<div class="row">
							<label class="col-xs-12">
								Specialist(s) performing the procedure(s) or surgeries
							</label>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-8">
								<div class="form-group">
									<div class="col-xs-12 col-md-2">
										First Name
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialistFirstName1" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
									</div>
									<div class="col-xs-12 col-md-2">
										Last Name
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialistLastName1" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-12 col-md-2">
										Phone
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialistPhone1" runat="server" CssClass="form-control" MaxLength="15" Enabled="false" placeholder="###-###-####" />
									</div>
									<div class="col-xs-12 col-md-2">
										Specialty Type
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialtyType1" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" /></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-8">
								<div class="form-group">
									<div class="col-xs-12 col-md-2">
										First Name
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialistFirstName2" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
									</div>
									<div class="col-xs-12 col-md-2">
										Last Name
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialistLastName2" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-12 col-md-2">
										Phone
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialistPhone2" runat="server" CssClass="form-control" MaxLength="15" Enabled="false" placeholder="###-###-####" />
									</div>
									<div class="col-xs-12 col-md-2">
										Specialty Type
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialtyType2" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" /></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-8">
								<div class="form-group">
									<div class="col-xs-12 col-md-2">
										First Name
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialistFirstName3" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
									</div>
									<div class="col-xs-12 col-md-2">
										Last Name
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialistLastName3" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-12 col-md-2">
										Phone
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialistPhone3" runat="server" CssClass="form-control" MaxLength="15" Enabled="false" placeholder="###-###-####" />
									</div>
									<div class="col-xs-12 col-md-2">
										Specialty Type
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialtyType3" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" /></div>
								</div>
							</div>
						</div>
						<div class="row">
							<label class="col-xs-12">
								Is specialist(s) part of the Veterans Administration (VA)?
							</label>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<asp:RadioButtonList ID="rblSpecialistVA" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow" Enabled="false">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3">
						Are you using oxygen daily to help you breathe? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblUsingOxygen" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="True">Yes</asp:ListItem>
							<asp:ListItem Value="False">No</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValUsingOxygen" runat="server" ControlToValidate="rblUsingOxygen"
							SetFocusOnError="true" ErrorMessage="Answer is required" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3">
						Have you been diagnosed with Chronic Obstructive Pulmonary Disease (COPD)? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblDiagnosedCopd" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="True">Yes</asp:ListItem>
							<asp:ListItem Value="False">No</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValDiagnosedCopd" runat="server" ControlToValidate="rblDiagnosedCopd"
							SetFocusOnError="true" ErrorMessage="Answer is required" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3">
						<span class="underline">Home Health Services</span><br />
						Are you currently receiving services from a home health agency? <span class="text-danger">*</span><br />
						<span class="control-text">(housekeepers / caregivers are not home health services)</span>
					</label>
					<div class="col-xs-12 col-md-8">
						<div class="form-group">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblHomeHealthServices" runat="server" AutoPostBack="true"
									CausesValidation="false" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblHomeHealthServices_SelectedIndexChanged">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValHomeHealthServices" runat="server" ControlToValidate="rblHomeHealthServices"
									SetFocusOnError="true" ErrorMessage="Answer is required" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-3">
								Home Health Provider
							</div>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtHomeHealthProviderName" runat="server" CssClass="form-control" MaxLength="100" Enabled="false" /></div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-3">
								Phone
							</div>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtHomeHealthProviderPhone" runat="server" CssClass="form-control" MaxLength="15" Enabled="false" placeholder="###-###-####" /></div>
						</div>
						<div class="row">
							<label class="col-xs-12">
								Services provided. Check all that apply:
							</label>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-8">
								<asp:CheckBoxList ID="cblHomeHealthServices" runat="server" CssClass="checkbox-inline" RepeatDirection="Vertical" RepeatLayout="Flow" Enabled="false">
									<asp:ListItem Value="Skilled nursing">Skilled nursing</asp:ListItem>
									<asp:ListItem Value="In-home physical therapy">In-home physical therapy</asp:ListItem>
									<asp:ListItem Value="In-home speech therapy / language therapy">In-home speech therapy / language therapy</asp:ListItem>
									<asp:ListItem Value="In-home occupational therapy">In-home occupational therapy</asp:ListItem>
								</asp:CheckBoxList>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-8">
								Dates of upcoming home health visits
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-6">
								<asp:TextBox ID="txtUpcomingHomeHealthVisits" runat="server" CssClass="form-control" MaxLength="100" Enabled="false" />
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3">
						<span class="underline">Rented Items Only</span><br />
						Do you currently use any of the following Durable Medical Equipment (DME) items?
					</label>
					<div class="col-xs-12 col-md-4">
						<div>
							<asp:CheckBoxList ID="cblRentedItems" runat="server" CssClass="checkbox-inline" RepeatDirection="Vertical" RepeatLayout="Flow">
								<asp:ListItem Value="Wheelchair">Wheelchair</asp:ListItem>
								<asp:ListItem Value="Scooter">Scooter</asp:ListItem>
								<asp:ListItem Value="Oxygen">Oxygen</asp:ListItem>
								<asp:ListItem Value="Oversized Wheelchair">Oversized Wheelchair</asp:ListItem>
								<asp:ListItem Value="Hospital Bed">Hospital Bed</asp:ListItem>
								<asp:ListItem Value="Nebulizer">Nebulizer</asp:ListItem>
								<asp:ListItem Value="Electric Wheelchair">Electric Wheelchair</asp:ListItem>
								<asp:ListItem Value="Lift">Lift</asp:ListItem>
							</asp:CheckBoxList>
						</div>
						<div>
							<asp:CheckBox ID="cbOtherDme" runat="server" CssClass="checkbox-inline" Text="Other" />
						</div>
						<div>
							<asp:TextBox ID="txtOtherDme" runat="server" CssClass="form-control" MaxLength="200" />
						</div>
						<hr />
						<div class="form-group">
							<div class="col-xs-12 col-md-4">
								DME Provider
							</div>
							<div class="col-xs-12 col-md-8"><asp:TextBox ID="txtDmeProviderName" runat="server" CssClass="form-control" MaxLength="50" /></div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-4">
								Phone
							</div>
							<div class="col-xs-12 col-md-8"><asp:TextBox ID="txtDmeProviderPhone" runat="server" CssClass="form-control" MaxLength="15" placeholder="###-###-####" /></div>
						</div>
					</div>
				</div>
			</div>
		</div>
</asp:PlaceHolder>

		<div class="panel panel-default">
			<div class="panel-heading">
				Please read and answer these important questions
			</div>
			<div class="panel-body">
				<asp:Panel id="pnlESRD" runat="server" CssClass="form-group">
					<label for="rblESRD" class="col-xs-12 col-md-3 control-label">
						Do you have End Stage Renal Disease (ESRD)? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblESRD" runat="server" CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValESRD" runat="server" ControlToValidate="rblESRD"
									SetFocusOnError="true" ErrorMessage="Answer is required" />
							</div>
						</div>
						<div class="row top-spacer">
							<div class="col-xs-12">
								<p>If you have had a successful kidney transplant and/or you don’t need regular dialysis any more, please attach a note or records
									from your doctor showing you have had a successful kidney transplant or you don’t need dialysis, otherwise we may need to contact
									you to obtain additional information.</p>
							</div>
						</div>
					</div>
				</asp:Panel>

				<div class="form-group">
					<label for="rblOtherCoverage" class="col-xs-12 col-md-3 control-label">
						Some individuals may have other drug coverage, including other private insurance,
						TRICARE, Federal employee health benefits coverage, VA benefits, or State pharmaceutical
						assistance programs. Will you have other prescription drug coverage in addition
						to CareMore Health Plan? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<div class="col-xs-12 col-md-4 no-padding-top">
								<asp:RadioButtonList ID="rblOtherCoverage" runat="server" AutoPostBack="True"
									CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblOtherCoverage_SelectedIndexChanged">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValOtherCoverage" runat="server" ControlToValidate="rblOtherCoverage"
									SetFocusOnError="true" ErrorMessage="Answer is required" />
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<p>If "yes", please list your other coverage and your identification (ID) number(s) for this coverage:</p>
							</div>
						</div>
						<div class="form-group">
							<label for="txtOtherCoverageName" class="col-xs-12 col-md-5 control-label">
								Name of other coverage
							</label>
							<div class="col-xs-12 col-md-4">
								<asp:TextBox ID="txtOtherCoverageName" runat="server" MaxLength="50" CssClass="form-control" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-3">
								<asp:RequiredFieldValidator ID="reqValOtherCoverageName" runat="server" ControlToValidate="txtOtherCoverageName"
									SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
							</div>
						</div>
						<div class="form-group">
							<label for="txtOtherCoverageId" class="col-xs-12 col-md-5 control-label">
								ID# for this coverage
							</label>
							<div class="col-xs-12 col-md-4">
								<asp:TextBox ID="txtOtherCoverageId" runat="server" MaxLength="50" CssClass="form-control" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-3">
								<asp:RequiredFieldValidator ID="reqValOtherCoverageId" runat="server" ControlToValidate="txtOtherCoverageId"
									SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
							</div>
						</div>
						<asp:Panel ID="pnlOtherCoverageGroup" runat="server" CssClass="form-group">
							<label for="txtOtherCoverageGroupNumber" class="col-xs-12 col-md-5 control-label">
								Group# for this coverage
							</label>
							<div class="col-xs-12 col-md-4">
								<asp:TextBox ID="txtOtherCoverageGroupNumber" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-3">
								<asp:RequiredFieldValidator ID="reqValOtherCoverageGroupNumber" runat="server" ControlToValidate="txtOtherCoverageGroupNumber"
									SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
							</div>
						</asp:Panel>
						<asp:Panel id="pnlCoverageEndDate" runat="server" CssClass="form-group">
							<label for="txtOtherCoverageDate" class="col-xs-12 col-md-5 control-label">
								Coverage End Date
							</label>
							<div class="col-xs-12 col-md-4">
								<asp:TextBox ID="txtOtherCoverageDate" runat="server" CssClass="form-control" MaxLength="10" Enabled="false" placeholder="mm/dd/yyyy" />
							</div>
							<div class="col-xs-12 col-md-3">
								<asp:RequiredFieldValidator ID="reqValOtherCoverageDate" runat="server" ControlToValidate="txtOtherCoverageDate"
									SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
								<asp:CompareValidator ID="cmpValOtherCoverageDate" runat="server" ControlToValidate="txtOtherCoverageDate"
									SetFocusOnError="true" ErrorMessage="Format is incorrect."
									Operator="DataTypeCheck" Type="Date" Enabled="false" />
							</div>
						</asp:Panel>
					</div>
				</div>

				<div class="form-group">
					<label for="rblCareResident" class="col-xs-12 col-md-3 control-label">
						Are you a resident in a long-term care facility, such as a nursing home? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblCareResident" runat="server" AutoPostBack="True"
									CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblCareResident_SelectedIndexChanged">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValCareResident" runat="server" ControlToValidate="rblCareResident"
									SetFocusOnError="true" ErrorMessage="Answer is required" />
							</div>
						</div>
						<div class="row top-spacer">
							<div class="col-xs-12">
								<p>If "yes", please provide the following information:<br />
								Name of Institution, Address, &amp; Phone Number of Institution (number and street)</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-9">
								<asp:TextBox ID="txtCareResident" runat="server" MaxLength="200" Rows="4" TextMode="MultiLine"
									CssClass="form-control" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-3">
								<asp:RequiredFieldValidator ID="reqValCareResidentInfo" runat="server" ControlToValidate="txtCareResident"
									SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="rblStateMedicaid" class="col-xs-12 col-md-3 control-label">
						Are you enrolled in your State Medicaid program? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblStateMedicaid" runat="server" AutoPostBack="True"
									CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblStateMedicaid_SelectedIndexChanged">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValStateMedicaid" runat="server" ControlToValidate="rblStateMedicaid"
									SetFocusOnError="true" ErrorMessage="Answer is required" />
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<p>If "yes", please provide your Medicaid number:</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-8">
								<asp:TextBox ID="txtStateMedicaidNumber" runat="server" MaxLength="50" CssClass="form-control" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValStateMedicaidNumber" runat="server" ControlToValidate="txtStateMedicaidNumber"
									SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
							</div>
						</div>
					</div>
				</div>

				<asp:Panel ID="pnlCurrentMember" runat="server" CssClass="form-group">
					<label for="rblCurrentMember" class="col-xs-12 col-md-3 control-label">
						Are you a current CareMore member?<span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblCurrentMember" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValCurrentMember" runat="server" ControlToValidate="rblCurrentMember"
									SetFocusOnError="true" ErrorMessage="Answer is required" />
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<p>Name of your current medical insurance plan</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-8">
								<asp:TextBox ID="txtCurrentMemberPlan" runat="server" CssClass="form-control" MaxLength="100" />
							</div>
						</div>
					</div>
				</asp:Panel>

				<div class="form-group">
					<label for="rblSpouseWork" class="col-xs-12 col-md-3 control-label">
						<asp:Literal id="litSpouseText" runat="server"></asp:Literal> <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblSpouseWork" runat="server" AutoPostBack="true"
									CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblSpouseWork_SelectedIndexChanged">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValSpouseWork" runat="server" ControlToValidate="rblSpouseWork"
									SetFocusOnError="true" ErrorMessage="Answer is required" />
							</div>
						</div>
				<asp:Placeholder ID="phSpouseGroupCoveragePlan" runat="server">
						<div class="row">
							<div class="col-xs-12">
								<p>If "yes", name of group coverage plan</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-8">
								<asp:TextBox ID="txtSpouseGroupCoveragePlan" runat="server" CssClass="form-control" MaxLength="100" Enabled="false" />
							</div>
						</div>
				</asp:Placeholder>
					</div>
				</div>

				<asp:Panel id="pnlLiveLongTerm" runat="server" CssClass="form-group">
					<label for="rblLiveLongTerm" class="col-xs-12 col-md-3 control-label">
						Do you live in a long term care facility? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblLiveLongTerm" runat="server" CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValRdbLiveLongTerm" runat="server" ControlToValidate="rblLiveLongTerm"
									SetFocusOnError="true" ErrorMessage="Answer is required" />
							</div>
						</div>
					</div>
				</asp:Panel>

				<asp:Panel id="pnlChronicLung" runat="server" CssClass="form-group">
					<label for="rblChronicLung" class="col-xs-12 col-md-3 control-label">
						Do you have a Chronic Lung Disorder? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblChronicLung" runat="server" CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValChronicLung" runat="server" ControlToValidate="rblChronicLung"
									SetFocusOnError="true" ErrorMessage="Answer is required" />
							</div>
						</div>
					</div>
				</asp:Panel>

				<asp:Panel id="pnlDiabetes" runat="server" CssClass="form-group">
					<label for="rblDiabetes" class="col-xs-12 col-md-3 control-label">
						Do you have Diabetes? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblDiabetes" runat="server" CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValDiabetes" runat="server" ControlToValidate="rblDiabetes"
									SetFocusOnError="true" ErrorMessage="Answer is required" />
							</div>
						</div>
					</div>
				</asp:Panel>

				<asp:Panel id="pnlChronicHeart" runat="server" CssClass="form-group">
					<label for="rblChronicHeart" class="col-xs-12 col-md-3 control-label">
						Do you have a Cardiovascular Disorder or Chronic Heart Failure? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblChronicHeart" runat="server" CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValChronicHeart" ControlToValidate="rblChronicHeart" runat="server"
									SetFocusOnError="true" ErrorMessage="Answer is required" />
							</div>
						</div>
					</div>
				</asp:Panel>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
				<label for="txtPrimaryPhysicianName" class="col-xs-12 col-md-3">
					Please choose name of a primary care physician
				</label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtPrimaryPhysicianName" runat="server" CssClass="form-control" MaxLength="100" />
				</div>
				<div class="col-xs-12 col-md-4">
					<asp:HyperLink ID="hlProviderSearch" runat="server" CssClass="btn btn-default" Target="_blank">Search for a Doctor</asp:HyperLink>
				</div>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<label for="rblPrintFormat" class="col-xs-12 col-md-3">
						Please check one of the boxes below if you would prefer us to send you information
						in a language other than English or in a different format
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<div class="col-xs-12">
								<asp:RadioButtonList ID="rblPrintFormat" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="" Selected="True">None</asp:ListItem>
									<asp:ListItem>Spanish</asp:ListItem>
									<asp:ListItem>Braille</asp:ListItem>
									<asp:ListItem>Large Print</asp:ListItem>
								</asp:RadioButtonList>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<p>Please contact CareMore Health Plan at <strong>(800) 499-2793</strong> if you need information in another format or language than what
								is listed above. Our office hours are 8:00 a.m. - 8:00 p.m., 7 days a week, (October 1 - February 14, except Thanksgiving
								and Christmas) and Monday - Friday (except holidays) from February 15 - September 30. TTY users should call 711.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				Please Read This Important Information
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						<asp:Label ID="lblPleaseReadInfo" runat="server" />
					</div>
				</div>
				<div class="row">
					<label class="col-xs-12 col-md-3 control-label">
						Release of Information <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-8">
						<div class="row">
							<div class="col-xs-12">
								<asp:Label ID="lblReleaseofInfo" runat="server" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-2">
								Enrollee Initials
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:TextBox ID="txtReleaseInitials" runat="server" CssClass="form-control" MaxLength="3" />
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValReleaseInitials" runat="server" ControlToValidate="txtReleaseInitials"
									SetFocusOnError="true" ErrorMessage="Required" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-2">
								Date
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:TextBox ID="txtReleaseDate" runat="server" CssClass="form-control" MaxLength="10" placeholder="mm/dd/yyyy" />
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValReleaseDate" runat="server" ControlToValidate="txtReleaseDate"
									SetFocusOnError="true" ErrorMessage="Required" />
								<asp:CompareValidator ID="cmpValReleaseDate" runat="server" ControlToValidate="txtReleaseDate"
									SetFocusOnError="true" ErrorMessage="Format is incorrect"
									Operator="DataTypeCheck" Type="Date" />
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="cblignature" class="col-xs-12 col-md-3 control-label">
						Enrollee's Signature <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<asp:CheckBoxList ID="cblSignature" runat="server" CssClass="radio" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem>Checking this box will serve as your electronic signature</asp:ListItem>
						</asp:CheckBoxList>
					</div>
					<div class="col-xs-12 col-md-3">
						<cm:RequiredFieldValidatorForCheckBoxLists ID="reqValCblSignature" runat="server" ControlToValidate="cblSignature"
							SetFocusOnError="true" ErrorMessage="Required" />
					</div>
				</div>
				<div class="form-group">
					<label for="rblAuthorizedRep" class="col-xs-12 col-md-3 control-label">
						Are you the authorized <br class="hidden-xs" />representative? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblAuthorizedRep" runat="server" AutoPostBack="True" CssClass="radio-inline"
									RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblAuthorizedRep_SelectedIndexChanged">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValAuthorizedRep" runat="server" ControlToValidate="rblAuthorizedRep"
									SetFocusOnError="true" ErrorMessage="Answer is required" />
							</div>
						</div>
						<div class="row top-spacer">
							<div class="col-xs-12">
								<p>If "yes", please provide the following information:</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
									<label for="txtRepName" class="col-xs-12 col-md-3 control-label">Name</label>
									<div class="col-xs-12 col-md-5">
										<asp:TextBox ID="txtRepName" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
									</div>
									<div class="col-xs-12 col-md-3">
										<asp:RequiredFieldValidator ID="reqValRepName" runat="server" ControlToValidate="txtRepName"
											SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<label for="txtRepAddress" class="col-xs-12 col-md-3 control-label">Address</label>
									<div class="col-xs-12 col-md-5">
										<asp:TextBox ID="txtRepAddress" runat="server" CssClass="form-control" MaxLength="100" Enabled="false" />
									</div>
									<div class="col-xs-12 col-md-3">
										<asp:RequiredFieldValidator ID="reqValRepAddress" runat="server" ControlToValidate="txtRepAddress"
											SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<label for="txtRepPhone" class="col-xs-12 col-md-3 control-label">Phone Number</label>
									<div class="col-xs-12 col-md-5">
										<asp:TextBox ID="txtRepPhone" runat="server" CssClass="form-control" MaxLength="15" Enabled="false" placeholder="###-###-####" />
									</div>
									<div class="col-xs-12 col-md-3">
										<asp:RequiredFieldValidator ID="reqValRepPhone" ControlToValidate="txtRepPhone" runat="server"
											SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
										<asp:RegularExpressionValidator ID="regExValRepPhone" runat="server" ControlToValidate="txtRepPhone"
											SetFocusOnError="true" ErrorMessage="Format is incorrect"
											ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$"
											Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<label for="txtRepRelation" class="col-xs-12 col-md-3 control-label">Relationship to Enrollee</label>
									<div class="col-xs-12 col-md-5">
										<asp:TextBox ID="txtRepRelation" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
									</div>
									<div class="col-xs-12 col-md-3">
										<asp:RequiredFieldValidator ID="reqValRepRelation" runat="server" ControlToValidate="txtRepRelation"
											SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<asp:Panel ID="pnlSalesAgent" runat="server" CssClass="row">
					<label class="col-xs-12 col-md-3 control-label">
						If a sales agent completed this form, please submit the following information
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<div class="col-xs-12 col-md-8">
								<span class="control-text">I helped the applicant fill out this application</span>
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblSalesAgent" runat="server" AutoPostBack="true"
									CausesValidation="false" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblSalesAgent_SelectedIndexChanged">
									<asp:ListItem Value="True">Yes</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-12 col-md-3 control-label">
								Agent Code
							</label>
							<div class="col-xs-12 col-md-5">
								<asp:TextBox ID="txtSalesAgentCode" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-3">
								<asp:RequiredFieldValidator ID="reqValSalesAgentCode" ControlToValidate="txtSalesAgentCode" runat="server"
									SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-12 col-md-3 control-label">
								Agent/Brokers First Name
							</label>
							<div class="col-xs-12 col-md-5">
								<asp:TextBox ID="txtSalesAgentFirstName" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-3">
								<asp:RequiredFieldValidator ID="reqValSalesAgentFirstName" ControlToValidate="TxtSalesAgentFirstName" runat="server"
									SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-12 col-md-3 control-label">
								Agent/Brokers Last Name
							</label>
							<div class="col-xs-12 col-md-5">
								<asp:TextBox ID="txtSalesAgentLastName" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-3">
								<asp:RequiredFieldValidator ID="reqValSalesAgentLastName" ControlToValidate="TxtSalesAgentLastName" runat="server"
									SetFocusOnError="true" ErrorMessage="Required" Enabled="false" />
							</div>
						</div>
					</div>
				</asp:Panel>
			</div>
		</div>
	</div>

		</ContentTemplate>
	</asp:UpdatePanel>

	<div class="form-group">
		<div class="col-xs-12 col-md-offset-3 col-md-6">
			<asp:LinkButton ID="lnkSubmit" runat="server" CssClass="btn btn-primary" OnClick="LnkBtnSubmit_Click">SUBMIT</asp:LinkButton>
		</div>
	</div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End SubLayouts/ApplicationPage -->
