﻿<%@ Control Language="C#" CodeBehind="SelectState.ascx.cs" Inherits="Website.Sublayouts.SelectState" %>

<!-- Begin Select State -->
<div class="row">
	<div class="col-xs-12">
		<asp:Panel ID="pnForm" runat="server" CssClass="form-horizontal" DefaultButton="btnRegionSubmit">
			<div class="form-group">
				<label for="txtStateHeader" class="col-xs-12 col-md-2 control-label"><sc:Text ID="txtStateHeader" runat="server" Field="State Header" /></label>
				<div class="col-xs-12 col-md-2">
					<asp:RadioButtonList ID="rblState" runat="server" CssClass="radio-inline" DataTextField="Text" DataValueField="Value" RepeatLayout="UnorderedList" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-12 col-md-2 control-label"><cm:Text ID="txtLanguageHeader" runat="server" Field="Language Header" /></label>
				<div class="col-xs-12 col-md-2">
					<asp:RadioButtonList ID="rblLanguage" runat="server" CssClass="radio-inline" DataTextField="Text" DataValueField="Value" RepeatLayout="UnorderedList" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12">
					<label class="checkbox-inline"><asp:CheckBox ID="chkRememberMySettings" runat="server" ClientIDMode="Static" />
					<sc:Text ID="txtCheckboxLabel" runat="server" Field="Checkbox Label" /></label>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-6 col-md-2">
					<asp:Button ID="btnRegionCancel" runat="server" CssClass="btn btn-danger" CausesValidation="false" OnClick="BtnCancel_OnClick" />
				</div>
				<div class="col-xs-5 col-md-2">
					<asp:Button ID="btnRegionSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_OnClick" />
				</div>
			</div>
		</asp:Panel>
		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End Select State -->
