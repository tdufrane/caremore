﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace Website.Sublayouts
{
	public partial class VaisListing : BaseUserControl
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				if (!IsPostBack)
					LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			ID templateID = new ID("{4114BFAB-80F8-44D8-A4DE-C6F94EF4BB63}");
			List<Item> list = base.CurrentItem.Children.Where(i => i.TemplateID == templateID).ToList();

			if (list == null || list.Count == 0)
			{
				rptListing.Visible = false;
			}
			else
			{
				rptListing.DataSource = list;
				rptListing.DataBind();
			}
		}

		#endregion
	}
}
