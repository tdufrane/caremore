﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;

namespace Website.Sublayouts
{
	public partial class ProviderTrainingContentPage : ProviderTrainingBasePage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				if (IsProviderRegistered(CurrentItem.Parent))
				{
					string itemId = CurrentItem.Parent.ID.Guid.ToString();

					if ((Session[itemId] != null) && ((bool)Session[itemId]))
					{
						ShowRegisteredMessage();
						Session[itemId] = false;
					}
				}
				else
				{
					RedirectToParent();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		private void RedirectToParent()
		{
			Response.Redirect(CareMoreUtilities.GetItemUrl(CurrentItem.Parent), false);
		}

		private void ShowRegisteredMessage()
		{
			HtmlGenericControl div = NamingContainer.FindControl("divMainContent") as HtmlGenericControl;

			if (div == null)
			{
				this.Controls.Add(new LiteralControl(CurrentItem["Registered Message"]));
			}
			else
			{
				div.Controls.AddAt(1, new LiteralControl(CurrentItem["Registered Message"]));
			}
		}
	}
}
