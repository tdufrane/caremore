﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ExecutiveTeamListPage.ascx.cs" Inherits="Website.Sublayouts.ExecutiveTeamListPage" %>

<!-- Begin SubLayouts/ExecutiveTeamListPage -->
<asp:ListView ID="lvExecutiveTeam" runat="server">
	<LayoutTemplate>
		<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
	</LayoutTemplate>
	<ItemTemplate>
		<div class="row">
			<hr />
			<div class="col-md-2">
				<sc:Image ID="imgExecutive" runat="server" CssClass="img-responsive execPic" Field="Picture" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />
			</div>
			<div class="col-md-10">
				<div class="row">
					<div class="col-md-12"><h3 class="execName"><sc:Text ID="txtName" runat="server" Field="Name" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></h3></div>
				</div>
				<div class="row">
					<div class="col-md-12"><h4 class="execTitle"><sc:Text ID="txtTitle" runat="server" Field="Title" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></h4></div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<sc:Text ID="txtDescription" runat="server" Field="Description" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />
					</div>
				</div>
			</div>
		</div>
	</ItemTemplate>
</asp:ListView>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
<!-- End SubLayouts/ExecutiveTeamListPage -->
