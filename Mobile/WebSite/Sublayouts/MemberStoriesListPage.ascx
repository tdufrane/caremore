﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MemberStoriesListPage.ascx.cs" Inherits="Website.Sublayouts.MemberStoriesListPage" %>
<%@ Register TagPrefix="cm" TagName="FlashVideo" Src="~/Controls/FlashVideo.ascx" %>

<!-- Begin SubLayouts/MembersStoriesPage -->
<asp:Label ID="MessageLbl" runat="server" />
<asp:Repeater ID="rptRow" runat="server" OnItemDataBound="rptRow_OnItemDataBound">
	<ItemTemplate>
		<div class="memberRow">
		<asp:Repeater ID="rptItem" OnItemDataBound="rptItem_OnDataBound" runat="server">
			<ItemTemplate>
				<div class="memberStory<%# Eval("CssClass") %>">
					<h3><sc:Text ID="TitleTxt" runat="server" Field="Title" /></h3>
					<div class="memberyStoryVideo"><!--206x300 -->
						<cm:FlashVideo ID="flvVideo" width="420" height="258" runat="server" />
					</div>
				</div>
			</ItemTemplate>
		</asp:Repeater>
		</div>
	</ItemTemplate>
</asp:Repeater>
<asp:Panel ID="pnNoMembers" runat="server">
	<p class="red">No member stories exist at this.
	Please check back later for updates.</p>
</asp:Panel>
<!-- End SubLayouts/MembersStoriesPage -->
