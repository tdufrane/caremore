﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemListPage.ascx.cs" Inherits="Website.Sublayouts.ItemListPage" %>

<!-- Begin SubLayouts/ItemListPage -->
<div class="row">
	<div class="col-xs-12">
<asp:Placeholder ID="phSearchResults" runat="server">
	<asp:UpdatePanel ID="pnlUpdate" runat="server">
		<ContentTemplate>
			<div class="row form-horizontal">
				<div class="col-xs-12 col-md-4">
					<label for="StateList"><cm:Text ID="txtState" runat="server" ItemReferenceName="STATE" Field="Text" /></label><br />
					<asp:DropDownList ID="ddlStateList" runat="server" AutoPostBack="true" CssClass="form-control" EnableViewState="true" OnSelectedIndexChanged="DdlStateList_SelectedIndexChanged"></asp:DropDownList>
				</div>
				<div class="col-xs-12 col-md-4">
					<label for="CountyList"><cm:Text ID="txtCounty" runat="server" ItemReferenceName="COUNTY" Field="Text" /></label><br />
					<asp:DropDownList ID="ddlCountyList" runat="server" AutoPostBack="true" CssClass="form-control" EnableViewState="true" OnSelectedIndexChanged="DdlCountyList_SelectedIndexChanged"></asp:DropDownList>
				</div>
			</div>

			<br />

			<asp:ListView ID="lvItems" runat="server" OnItemDataBound="LvItems_ItemDataBound">
				<LayoutTemplate>
					<div class="row">
						<div class="col-xs-12 col-md-10">
							<table class="table table-condensed table-responsive table-striped">
								<thead>
									<tr>
										<th class="col-md-4"><sc:Text id="txtHeader1" runat="server" Field="Header1" /></th>
										<th class="col-md-4"><sc:Text id="txtHeader2" runat="server" Field="Header2" /></th>
										<th class="col-md-2"><sc:Text id="txtHeader3" runat="server" Field="Header3" /></th>
										<th class="col-md-2"><sc:Text id="txtHeader4" runat="server" Field="Header4" /></th>
									</tr>
								</thead>
								<tbody>
									<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
								</tbody>
							</table>
						</div>
					</div>
				</LayoutTemplate>
				<ItemTemplate>
					<tr>
						<td>
							<asp:HyperLink ID="NameHL" runat="server" />
							<asp:Label ID="IdLbl" runat="server" />
							<asp:Label ID="EffectiveLbl" runat="server" Font-Bold="true" />
						</td>
						<td>
							<asp:PlaceHolder ID="AddressPH" runat="server" />
						</td>
						<td>
							<asp:Label ID="CountyTxt" runat="server" />
						</td>
						<td>
							<asp:PlaceHolder ID="DetailsPH" runat="server" />
						</td>
					</tr>
				</ItemTemplate>
				<EmptyDataTemplate>
					<div class="alert alert-danger" role="alert"><cm:Text ID="txtNoResults" runat="server" ItemReferenceName="NO_RESULTS" Field="Text" /></div>
				</EmptyDataTemplate>
			</asp:ListView>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Placeholder>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End SubLayouts/ItemListPage -->
