﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentList.ascx.cs" Inherits="Website.Sublayouts.DocumentList" %>
<%@ Register src="Widgets/AdobeReaderWidget.ascx" tagname="AdobeReaderWidget" tagprefix="uc1" %>

<!-- Begin SubLayouts/DocumentList -->
<asp:Panel ID="pnlMain" runat="server">
	<h3 id="h3ListName" runat="server"></h3>

	<asp:Panel ID="pnlDocumentListTextBefore" runat="server">
		<sc:Text ID="txtBefore" runat="server" Field="Text Before" />
	</asp:Panel>

	<asp:ListView ID="lvCollapsibleTop" OnItemDataBound="LvCollapsibleTop_ItemDataBound" Visible="false" runat="server">
		<LayoutTemplate>
			<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
		</LayoutTemplate>
		<ItemTemplate>
			<p><asp:HyperLink ID="hlDocument" runat="server" /></p>
		</ItemTemplate>
	</asp:ListView>


	<asp:ListView ID="lvCollapsible" OnItemDataBound="LvCollapsible_ItemDataBound" Visible="false" runat="server">
		<LayoutTemplate>
			<script type="text/javascript">
				$('glyphicon').click(function () {
					$(this).toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
				});

				//$('#serviceList').on('hidden.bs.collapse', function () {
				//	$(".servicedrop").addClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up');
				//});
			</script>
			<div class="accordion" id="news-accordion">
				<div class="panel-group" id="docsAccordian" role="tablist" aria-multiselectable="true">
					<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
				</div>
			</div>
		</LayoutTemplate>
		<ItemTemplate>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading<%# Container.DataItemIndex %>">
					<a href="#collapse<%# Container.DataItemIndex %>" aria-controls="collapse<%# Container.DataItemIndex %>"
						aria-expanded="false" data-toggle="collapse" data-parent="#docsAccordian" role="button">
						<div class="row">
							<div class="col-xs-6 col-md-9">
								<h3 class="panel-title"><%# ((Sitecore.Data.Items.Item)Container.DataItem).Name %></h3>
							</div>
							<div class="col-xs-6 col-md-2 col-md-offset-1"><span class="glyphicon glyphicon-chevron-down"></span></div>
						</div>
					</a>
				</div>
				<div id="collapse<%# Container.DataItemIndex %>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<%# Container.DataItemIndex %>">
					<asp:ListView ID="lvCollapsibleInner" runat="server" OnItemDataBound="LvCollapsibleInner_ItemDataBound">
						<LayoutTemplate>
							<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
						</LayoutTemplate>
						<ItemTemplate>
							<div class="row">
								<div class="col-xs-12">
									<p><asp:HyperLink ID="hlDocument" runat="server"></asp:HyperLink></p>
								</div>
							</div>
						</ItemTemplate>
					</asp:ListView>
				</div>
			</div>
		</ItemTemplate>
	</asp:ListView>

	<asp:ListView ID="lvIndent" OnItemDataBound="LvIndent_ItemDataBound" Visible="false" runat="server">
		<LayoutTemplate>
			<div><asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder></div>
		</LayoutTemplate>
		<ItemTemplate>
			<h4><asp:Label ID="CategoryName" runat="server" /></h4>

			<asp:ListView ID="lvIndent3" OnItemDataBound="LvIndent3_ItemDataBound" Visible="false" runat="server">
				<LayoutTemplate>
					<p><asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder></p>
				</LayoutTemplate>
				<ItemTemplate>
					<blockquote><asp:HyperLink ID="hlDocument" runat="server"></asp:HyperLink></blockquote>
				</ItemTemplate>
			</asp:ListView>

			<asp:ListView ID="lvIndent2" OnItemDataBound="LvIndent2_ItemDataBound" Visible="false" runat="server">
				<LayoutTemplate>
					<blockquote><asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder></blockquote>
				</LayoutTemplate>
				<ItemTemplate>
					<h4><asp:Label ID="SubCategoryName" runat="server" /></h4>
					<asp:ListView ID="lvIndent3" OnItemDataBound="LvIndent3_ItemDataBound" Visible="false" runat="server">
					<LayoutTemplate>
						<p><asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder></p>
					</LayoutTemplate>
					<ItemTemplate>
						<blockquote><asp:HyperLink ID="hlDocument" runat="server"></asp:HyperLink></blockquote>
					</ItemTemplate>
					</asp:ListView>
				</ItemTemplate>
			</asp:ListView>
		</ItemTemplate>
	</asp:ListView>

	<asp:UpdatePanel ID="pnlUpdate" runat="server" Visible="false">
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="ddlCategoryList" EventName="SelectedIndexChanged" />
			<asp:AsyncPostBackTrigger ControlID="ddlSubCategoryList" EventName="SelectedIndexChanged"/>
		</Triggers>
		<ContentTemplate>
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<div class="form-group">
						<label for="ddlCategoryList"><asp:Label ID="ddlCategoryLabel" runat="server" /></label>
						<asp:DropDownList ID="ddlCategoryList" runat="server" AutoPostBack="true" CssClass="form-control" EnableViewState="true" OnSelectedIndexChanged="OnCategorySelectionChange" ClientIDMode="Static"></asp:DropDownList>
					</div>
				</div>
				<asp:Panel ID="subCategoryDropDownPanel" runat="server" CssClass="col-xs-12 col-md-6" Visible="false">
					<div class="form-group">
						<label for="ddlSubCategoryList"><asp:Label ID="ddlSubCategoryLabel" runat="server" /></label>
						<asp:DropDownList ID="ddlSubCategoryList" runat="server" Enabled="false" AutoPostBack="true" CssClass="form-control" EnableViewState="true" OnSelectedIndexChanged="OnSubCategorySelectionChange" ClientIDMode="Static"></asp:DropDownList>
					</div>
				</asp:Panel>
			</div>
			<asp:ListView ID="DropDownLV" OnItemDataBound="DropdownLV_ItemDataBound" Visible="false" runat="server">
				<LayoutTemplate>
					<div class="row">
						<div class="col-xs-12">
							<h2><asp:Label ID="CategoryName" runat="server" /></h2>
							<ul class="links">
								<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
							</ul>
						</div>
					</div>
				</LayoutTemplate>
				<ItemTemplate>
					<li><asp:HyperLink ID="hlDocument" runat="server"></asp:HyperLink></li>
				</ItemTemplate>
			</asp:ListView>
		</ContentTemplate>
	</asp:UpdatePanel>

	<asp:ListView ID="lvList" runat="server" OnItemDataBound="LvList_ItemDataBound" Visible="false">
		<LayoutTemplate>
			<ul>
				<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
			</ul>
		</LayoutTemplate>
		<ItemTemplate>
			<li><asp:HyperLink ID="hlDocument" runat="server"></asp:HyperLink></li>
		</ItemTemplate>
	</asp:ListView>

	<asp:Panel ID="pnlDocumentListTextAfter" runat="server">
		<sc:Text ID="txtAfter" runat="server" Field="Text After" />
	</asp:Panel>
</asp:Panel>
<!-- End Sublayouts/DocumentList -->
