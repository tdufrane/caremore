﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using Website.Controls;
using Sitecore.Links;
using System.Web.UI.MobileControls;
using System.Collections.Generic;

namespace Website.Sublayouts
{
	/// <summary>
	/// This is used to render the member stories listings page ('http://www.caremore.com/About/MemberStories.aspx').
	/// </summary>
	public partial class MemberStoriesListPage : System.Web.UI.UserControl
	{
		Item currentItem = Sitecore.Context.Item;

		protected void Page_Load(object sender, EventArgs e)
		{
			List<Row> rows = GetList();

			pnNoMembers.Visible = rows.Count == 0;

			rptRow.DataSource = rows;
			rptRow.DataBind();
		}

		private List<Row> GetList()
		{
			List<VideoItem> list = currentItem.Axes.SelectItems("./*[@@templatename='Member Story']")
				.Where(i => !string.IsNullOrEmpty(i["Video URL"]))
				.Select(p => new VideoItem() { Item = p }).ToList(); 

			List<Row> rows = GetRows(list, 2);

			foreach (Row row in rows)
			{
				if (row.Items.Count() > 0)
				{
					row.Items.Last().CssClass = " memberStoryLast";
				}
			}

			return rows;
		}

		private List<Row> GetRows(List<VideoItem> items, int itemsPerRow)
		{
			List<Row> list = new List<Row>();
			Row currentRow = null;

			foreach (VideoItem item in items)
			{
				if (currentRow == null || currentRow.Items.Count >= itemsPerRow)
				{
					currentRow = new Row();
					list.Add(currentRow);
				}

				currentRow.Items.Add(item);
			}

			return list;
		}

		protected void rptRow_OnItemDataBound(object sender, RepeaterItemEventArgs args)
		{
			Row row = args.Item.DataItem as Row;
			Repeater rptItem = args.Item.FindControl("rptItem") as Repeater;

			rptItem.DataSource = row.Items;
			rptItem.DataBind();
		}

		protected void rptItem_OnDataBound(object sender, RepeaterItemEventArgs args)
		{
			VideoItem currentItem = (VideoItem)args.Item.DataItem;

			Text TitleTxt = (Text)args.Item.FindControl("TitleTxt");
			FlashVideo flvVideo = (FlashVideo)args.Item.FindControl("flvVideo");

			TitleTxt.Item = currentItem.Item;
			flvVideo.VideoItem = currentItem.Item;
		}

		private class Row
		{
			public List<VideoItem> Items = new List<VideoItem>();
		}

		public class VideoItem
		{
			public String CssClass { get; set; }
			public Item Item { get; set; }
		}
	}
}
