﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;
using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using CareMore.Web.DotCom.Provider;
using Website.Controls;
using Website.BL.ListItem;

namespace Website.Sublayouts
{
	public partial class ProviderDetailsPage : BaseUserControl
	{
		#region Private variables

		private int addressCounter = 0;
		private string lastState = string.Empty;
		private string lastCounty = string.Empty;
		private string thisState = string.Empty;
		private string thisCounty = string.Empty;
		private string providerID = string.Empty;
		private string addressID = string.Empty;

		#endregion

		#region Page events

		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack)
			{
				// Make sure previous error is hidden
				CareMoreUtilities.ClearControl(phError);
			}
			else
			{
				BackToResultsLB.Text = CurrentItem["Back To Results"];

				try
				{
					providerID = Request.QueryString["id"];
					addressID = Request.QueryString["aid"];

					Guid sitecoreId;
					long prprId;

					if (Guid.TryParse(providerID, out sitecoreId))
					{
						GetItemFromSitecore();
					}
					else if (long.TryParse(providerID, out prprId))
					{
						GetItemFromDatabase(Request.QueryString["at"]);
					}
					else
					{
						ShowNotFound();
					}
				}
				catch (Exception ex)
				{
#if DEBUG
					CareMoreUtilities.DisplayError(phError, ex, phDetails);
#else
					SetDownMessage(ex, phError, phDetails);
					phDetails.Visible = false;
#endif
				}
			}
		}

		protected void MedicalOffices_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			switch (e.Item.ItemType)
			{
				case ListItemType.AlternatingItem:
				case ListItemType.Item:
					thisState = ((ProviderLocation)e.Item.DataItem).StateName;
					thisCounty = ((ProviderLocation)e.Item.DataItem).County;

					if (!thisState.Equals(lastState))
					{
						Panel pnlState = (Panel)e.Item.FindControl("pnlState");
						pnlState.Controls.Add(new LiteralControl(thisState));
						pnlState.Visible = true;

						Panel pnlCounty = (Panel)e.Item.FindControl("pnlCounty");
						pnlCounty.Controls.Add(new LiteralControl(thisCounty));
						pnlCounty.Visible = true;

						// Reset counters
						lastState = thisState;
						lastCounty = thisCounty;
						addressCounter = 0;
					}
					else if (!thisCounty.Equals(lastCounty))
					{
						Panel pnlCounty = (Panel)e.Item.FindControl("pnlCounty");
						pnlCounty.Controls.Add(new LiteralControl(thisCounty));
						pnlCounty.Visible = true;

						// Reset counters
						lastCounty = thisCounty;
						addressCounter = 0;
					}

					addressCounter++;
					break;

				case ListItemType.Separator:
					if ((addressCounter % 2) != 0)
						e.Item.Visible = false;
					break;

				default:
					break;
			}
		}

		protected void BackToResultsLB_Click(object sender, EventArgs e)
		{
			Item resultsPage = Sitecore.Context.Item.Parent;
			Response.Redirect(Sitecore.Links.LinkManager.GetItemUrl(resultsPage), false);
		}

		protected void NewSearchLB_OnClick(object sender, EventArgs e)
		{
			Item searchPage = Sitecore.Context.Item.Parent;
			Response.Redirect(Sitecore.Links.LinkManager.GetItemUrl(searchPage), false);
		}

		#endregion

		#region Private methods

		private void GetItemFromDatabase(string addressType)
		{
			List<ProviderSearchDetail> details = ProviderFinder.GetProviderDetail(
				ProviderHelper.GetPortalCode(BL.SiteSettings.SiteSettingPath),
				providerID, addressType);

			if (details.Count == 0)
			{
				ShowNotFound();
			}
			else
			{
				string languages = GetProviderLanguages(addressType);

				ShowDetail(details, addressType, languages, false);

				// Additional Locations
				string stateEntered = string.Empty;
				if (Request.QueryString["state"] != null)
					stateEntered = Request.QueryString["state"];

				BindAddresses(addressType, stateEntered);
			}
		}

		private void GetItemFromSitecore()
		{
			Item providerItem = Sitecore.Context.Database.GetItem(providerID, Sitecore.Globalization.Language.Parse("en"));

			if (providerItem == null)
			{
				ShowNotFound();
			}
			else
			{
				ProviderSearchDetail detail = new ProviderSearchDetail();
				detail.Accessibility = !string.IsNullOrEmpty(providerItem["Handicap Accessibility"]);
				detail.Address1 = providerItem["Address Line 1"];
				detail.Address2 = providerItem["Address Line 2"];
				detail.City = providerItem["City"];
				detail.County = providerItem["County"];
				detail.Fax = providerItem["Fax"];

				if (string.IsNullOrWhiteSpace(providerItem["Location Name"]))
					detail.Name = providerItem["Provider Name"];
				else
					detail.Name = providerItem["Location Name"];

				detail.NpiId = providerItem["NPI ID"];
				detail.Phone = providerItem["Phone"];
				detail.ProviderId = providerItem["Provider ID"];
				detail.State = providerItem["State"];
				detail.Zip = providerItem["Postal Code"];

				List<ProviderSearchDetail> details = new List<ProviderSearchDetail>();
				details.Add(detail);

				string languages;
				if (string.IsNullOrEmpty(providerItem["Languages"]))
					languages = "English";
				else
					languages = providerItem["Languages"];

				ShowDetail(details, string.Empty, languages, true);
			}
		}

		private void ShowDetail(List<ProviderSearchDetail> details, string addressType, string languages, bool fromSitecore)
		{
			ProviderSearchDetail detail = details[0];

			if (string.IsNullOrWhiteSpace(detail.Title))
				lblPhysicianText.Text = string.Format("{0}<br />ID: {1}", detail.Name, detail.ProviderId);
			else
				lblPhysicianText.Text = string.Format("{0}, {1}<br />ID: {2}", detail.Name, detail.Title, detail.ProviderId);

			PhoneText.Text = LocationHelper.GetFormattedPhoneNo(detail.Phone);
			FaxText.Text = LocationHelper.GetFormattedPhoneNo(detail.Fax);

			LocationText.Text = LocationHelper.GetFormattedAddress(detail.Address1, detail.Address2,
				detail.City, detail.State, detail.Zip);

			DirectionsLink.NavigateUrl = ProviderHelper.GetMapUrl(detail);

			GoogleMap.Locations.Add(new LocationItem()
			{
				Address1 = detail.Address1 ?? string.Empty,
				City = detail.City ?? string.Empty,
				ZipCode = detail.Zip ?? string.Empty,
				Name = detail.Name ?? string.Empty,
				IconImage = GetIconImage(),
				Description = string.Empty,
				Phone = detail.Phone ?? string.Empty
			});
			GoogleMap.DisplayMap();

			// Effective date
			if (detail.EffectiveDate > DateTime.Now.Date)
			{
				EffectivePnl.Visible = true;
				EffectiveDateText.Text = detail.EffectiveDate.ToString("MM/dd/yyyy");
			}

			SpecialtiesText.Text = detail.PrimarySpecialty;

			if (!string.IsNullOrEmpty(detail.SecondarySpecialty))
			{
				SpecialtiesText.Text += "<br />" + detail.SecondarySpecialty;
			}

			LanguagesText.Text = languages;

			if (detail.AcceptingPatients.Equals("Y"))
				AcceptingNewPatientsText.Text = "Yes";
			else
				AcceptingNewPatientsText.Text = "No";
		}

		private string GetProviderLanguages(string addressType)
		{
			List<ProviderLanguage> list = ProviderFinder.GetProviderLanguages(providerID, addressID, addressType);

			List<string> languages = new List<string>();
			foreach (ProviderLanguage item in list)
			{
				if (!item.LanguageType.Equals("O")) // Exclude office language records
					languages.Add(item.Language);
			}

			return string.Join(", ", languages.ToArray());
		}

		private string GetIconImage()
		{
			string iconImage = "#";
			if (!string.IsNullOrEmpty(CurrentItem.Parent["Provider List Type"]))
			{
				Item providerListing = ((LookupField)CurrentItem.Parent.Fields["Provider List Type"]).TargetItem;
				iconImage = Common.GetKeyImagePath(providerListing);
			}
			return iconImage;
		}

		private void BindAddresses(string addressType, string state)
		{
			List<ProviderLocation> results = ProviderFinder.GetProviderLocations(addressID, state).ToList();

			if (results.Count < 2)
			{
				this.MedicalOffices.Visible = false;
			}
			else
			{
				this.MedicalOffices.DataSource = results;
				this.MedicalOffices.DataBind();
			}
		}

		private void ShowNotFound()
		{
			CareMoreUtilities.DisplayError(phError, CurrentItem["No Results Message"], phDetails);
			lblPhysicianText.Visible = false;
		}

		#endregion

		#region Page methods

		protected string AddressHtml(object item)
		{
			ProviderLocation location = (ProviderLocation)item;

			StringBuilder medOfficeText = new StringBuilder();
			medOfficeText.Append(LocationHelper.GetFormattedAddress(location.Address1, location.Address2,
				location.City, location.StateName, location.Zip,
				string.Format(ProviderHelper.DetailsUrlFormat,
					"Details.aspx", providerID, addressID, location.AddressType.Trim()),
				CurrentItem["Address Tootip"], false));

			if (!string.IsNullOrEmpty(location.Phone))
				medOfficeText.AppendFormat("<br />Ph: {0}", LocationHelper.GetFormattedPhoneNo(location.Phone));

			if (!string.IsNullOrEmpty(location.Fax))
				medOfficeText.AppendFormat("<br />Fax: {0}", LocationHelper.GetFormattedPhoneNo(location.Fax));

			return medOfficeText.ToString();
		}

		#endregion
	}
}
