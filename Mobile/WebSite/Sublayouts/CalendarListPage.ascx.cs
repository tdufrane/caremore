﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using Sitecore.Globalization;
using Sitecore.Data.Fields;

using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using CareMore.Web.DotCom.Provider;

namespace Website.Sublayouts
{
	public partial class CalendarListPage : BaseUserControl
	{
		#region Private variables

		private int currentPage = 0;
		private string queryString = string.Empty;
		private DateTime currentTime = DateTime.Now;

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void ItemsLV_Load(object sender, EventArgs e)
		{
			if (Page.IsPostBack)
			{
				ShowResults();
			}
		}

		protected void ItemsLV_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
		{
			ItemsDP.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
			currentPage = Convert.ToInt32(e.StartRowIndex / e.MaximumRows) + 1;
		//	queryString = "?zip=" + Server.UrlEncode(ZipCode.Text.Trim()) + "&city=" + Server.UrlEncode(City.Text.Trim()) + "&time=" + currentTime.ToString("HHmm") + "&evnt=" + EventTypeDDL.SelectedIndex.ToString() + "&page=" + currentPage.ToString();
			queryString = "?city=" + Server.UrlEncode(City.Text.Trim()) + "&time=" + currentTime.ToString("HHmm") + "&evnt=" + EventTypeDDL.SelectedIndex.ToString() + "&dist=" + ddlDistance.SelectedIndex.ToString() + "&page=" + currentPage.ToString(); 
			ItemsLV.DataBind();
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				CareMoreUtilities.SetCurrentZipCode(ZipCode.Text.Trim());
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void ItemsLV_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				ListViewDataItem LVDI = (ListViewDataItem)e.Item;
				Item ThisItem = (Item)LVDI.DataItem;

				Date DateDt = (Date)e.Item.FindControl("DateDt");
				DateDt.Item = ThisItem;

				Date Time = (Date)e.Item.FindControl("Time");
				Time.Item = ThisItem;

				Text CountyTxt = (Text)e.Item.FindControl("CountyTxt");
				CountyTxt.Item = ThisItem;
				CountyTxt.RenderAs = Sitecore.Web.UI.RenderAs.WebControl;

				Literal litEventType = (Literal)e.Item.FindControl("litEventType");
				litEventType.Text = RenderEventType(ThisItem);

				HyperLink NameHL = (HyperLink)e.Item.FindControl("NameHL");
				NameHL.NavigateUrl = Sitecore.Links.LinkManager.GetItemUrl(ThisItem) + queryString;
				NameHL.Text = CareMoreUtilities.GetItemDisplayName(ThisItem, "Title");
			
				Text CityTxt = (Text)e.Item.FindControl("CityTxt");
				CityTxt.Item = ThisItem;
				CityTxt.RenderAs = Sitecore.Web.UI.RenderAs.WebControl;

				Text SummaryTxt = (Text)e.Item.FindControl("SummaryTxt");
				SummaryTxt.Item = ThisItem;

				HyperLink LearnMoreHL = (HyperLink)e.Item.FindControl("LearnMoreHL");
				LearnMoreHL.NavigateUrl = Sitecore.Links.LinkManager.GetItemUrl(ThisItem) + queryString;
			}
		}

		protected void ItemsDP_PreRender(object sender, EventArgs e)
		{
			if (ItemsDP.Controls.Count > 0 )
			{
				LinkButton link = (LinkButton)ItemsDP.Controls[0].Controls[0];
				link.Text = "&lt;&lt;";
				link.Visible = (currentPage > 1);

				link = (LinkButton)ItemsDP.Controls[2].Controls[0];
				link.Visible = (currentPage * ItemsDP.PageSize) < ItemsDP.TotalRowCount;
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			if (CurrentState.ToLower() == "virginia")
				EventsDisclaimer.Item = Sitecore.Context.Database.GetItem(ItemIds.GetID("EVENTS_VA_DISCLAIMER"), Sitecore.Context.Language);
			else
				EventsDisclaimer.Item = Sitecore.Context.Database.GetItem(ItemIds.GetID("EVENTS_DISCLAIMER"), Sitecore.Context.Language);

			if (!Page.IsPostBack)
			{
				btnSubmit.Text = CurrentItem["Button Label"];
				SetMileLabels(ddlDistance);

				CareMoreUtilities.PopulateDropDownList(EventTypeDDL, CurrentDb.SelectSingleItem(CareMoreUtilities.EVENTS_SORTBY_PATH));

				if (Request.QueryString["evnt"] != null && Request.QueryString["evnt"].Length > 0)
				{
					int eventIndex;
					if (int.TryParse(Request.QueryString["evnt"], out eventIndex) && eventIndex < EventTypeDDL.Items.Count)
						EventTypeDDL.SelectedIndex = eventIndex;
				}

				if (Request.QueryString["dist"] != null && Request.QueryString["dist"].Length > 0)
				{
					int distIndex;
					if (int.TryParse(Request.QueryString["dist"], out distIndex) && distIndex < ddlDistance.Items.Count)
						ddlDistance.SelectedIndex = distIndex;
				}

				string currentzip = CareMoreUtilities.GetCurrentZipCode();

				if (Request.QueryString["city"] != null && Request.QueryString["city"].Length > 0)
					City.Text = Server.UrlDecode(Request.QueryString["city"]);

				if (Request.QueryString["time"] != null && Request.QueryString["time"].Length == 4
					&& DateTime.Now.ToString("HHmm").CompareTo(Request.QueryString["time"]) > 0
					&& !DateTime.TryParse(DateTime.Now.ToShortDateString() + " " + Request.QueryString["time"].Substring(0, 2) + " " + Request.QueryString["time"].Substring(2, 2), out currentTime))
					currentTime = DateTime.Now;

				string submitText=Sitecore.Context.Database.GetItem(ItemIds.GetID("EVENTS_SEARCH"), Sitecore.Context.Language).Fields["Text"].Value;
				btnSubmit.Text = submitText;
				btnSubmit.ToolTip = submitText;

				if (Request.QueryString["page"] != null && Request.QueryString["page"].Length > 0)
					int.TryParse(Request.QueryString["page"], out currentPage);

				ShowResults(currentPage);
			}
		}

		private void SetMileLabels(DropDownList list)
		{
			string mileLabel = CurrentItem["Mile Label"];

			foreach (ListItem item in ddlDistance.Items)
			{
				item.Text = item.Text.Replace("mile", mileLabel);
			}
		}

		private void ShowResults(int page=0)
		{
			Item[] itemList = null;

			var DB_ID = ItemIds.GetID("EVENTS_DB_ID");
			Item EventsDB = Sitecore.Context.Database.GetItem(DB_ID, Language.Parse("en"));

			if (EventsDB == null || string.IsNullOrEmpty(base.CurrentState)) return;
			
			string state = base.CurrentState.ToLower();
			Item eventTypeItem = null;
			
			string zipCode = ZipCode.Text.Trim();
			string city=City.Text.Trim();

		//	queryString = "?zip=" + Server.UrlEncode(zipCode) + "&city=" + Server.UrlEncode(city) + "&time=" + currentTime.ToBinary().ToString() + "&evnt=" + EventTypeDDL.SelectedIndex.ToString();
			queryString = "?city=" + Server.UrlEncode(city) + "&time=" + currentTime.ToString("HHmm") + "&evnt=" + EventTypeDDL.SelectedIndex.ToString() + "&dist=" + ddlDistance.SelectedIndex.ToString();			

			if (EventTypeDDL.SelectedIndex > 0)
			{
				var sortByID = ItemIds.GetID("EVENT_SORT_BY_ID");
				var sortByItem = CurrentDb.GetItem(sortByID, Language.Parse("en"));
				Item[] eventTypeChoices = sortByItem.Axes.GetDescendants();

				eventTypeItem = eventTypeChoices.Where(i => i["Value"] == EventTypeDDL.SelectedValue).FirstOrDefault();
			}

			string[] allowedEventGuids = eventTypeItem != null ? eventTypeItem["Event Type"].Split('|') : null;

			if (zipCode == "")
			{
				itemList = (from i in EventsDB.Axes.GetDescendants()
							where
								i.TemplateName == "Event" &&
								i["Status"] == "Active" &&
								((DateField)i.Fields["EventDate"]).DateTime >= currentTime &&
								GetComparisonValueState(i["State"]).ToLower().Equals(state) &&
								(city=="" || i.Fields["City"].Value.StartsWith(city, StringComparison.InvariantCultureIgnoreCase)) &&
								(allowedEventGuids == null || i["EventType"].Split('|').Any(e => allowedEventGuids.Contains(e)))                          
							select i).Distinct().OrderBy(i => i.Fields["EventDate"].Value).ToArray();
			}
			else
			{
				List<ZipCode> zipCodes = ProviderFinder.GetZipCodes(zipCode, double.Parse(ddlDistance.SelectedValue));

				if (city == "")
				{
					itemList = (from i in EventsDB.Axes.GetDescendants()
								join zip in zipCodes on i["Postal Code"] equals zip.BaseZipCode
								where
									i.TemplateName == "Event" &&
									i["Status"] == "Active" &&
									((DateField)i.Fields["EventDate"]).DateTime >= currentTime &&
									GetComparisonValueState(i["State"]).ToLower().Equals(state) &&
									(allowedEventGuids == null || i["EventType"].Split('|').Any(e => allowedEventGuids.Contains(e)))
								select i).Distinct().OrderBy(i => i.Fields["EventDate"].Value).ToArray();
				}
				else
				{
					itemList = (from i in EventsDB.Axes.GetDescendants()
								join zip in zipCodes on i["Postal Code"] equals zip.BaseZipCode
								where
									i.TemplateName == "Event" &&
									i["Status"] == "Active" &&
									((DateField)i.Fields["EventDate"]).DateTime >= currentTime &&
									GetComparisonValueState(i["State"]).ToLower().Equals(state) &&
									(allowedEventGuids == null || i["EventType"].Split('|').Any(e => allowedEventGuids.Contains(e)))
								select i).Union(from i in EventsDB.Axes.GetDescendants()
												where
													i.TemplateName == "Event" &&
													i["Status"] == "Active" &&
													((DateField)i.Fields["EventDate"]).DateTime >= currentTime &&
													GetComparisonValueState(i["State"]).ToLower().Equals(state) &&
													(i.Fields["City"].Value.StartsWith(city, StringComparison.InvariantCultureIgnoreCase)) &&
													(allowedEventGuids == null || i["EventType"].Split('|').Any(e => allowedEventGuids.Contains(e)))
												select i).Distinct().OrderBy(i => i.Fields["EventDate"].Value).ToArray();
				}
			}

			ItemsLV.DataSource = itemList;
			if (page > 0 && ItemsDP.PageSize <= itemList.Count())
			{
				if (page > Convert.ToInt32(itemList.Count() / ItemsDP.PageSize) + 1)
					page = Convert.ToInt32(itemList.Count() / ItemsDP.PageSize) + 1;
				ItemsDP.SetPageProperties(page * ItemsDP.PageSize - ItemsDP.PageSize, ItemsDP.PageSize, false);
			}
			currentPage = page;
			
			ItemsLV.DataBind();

			if (itemList.Count() > 0)
			{
				if (ItemsDP.PageSize >= ItemsDP.TotalRowCount)
					ItemsDP.Visible = false;
				else
					ItemsDP.Visible = true;
			}

			CareMoreUtilities.SetLastUpdateDates(itemList);
		}

		private string RenderEventType(Item item)
		{
			StringBuilder events = new StringBuilder();

			MultilistField eventTypeItem = item.Fields["EventType"];
			Item[] eventTypes = eventTypeItem.GetItems();

			if (eventTypes.Length > 0)
			{
				foreach (Item eventType in eventTypes)
				{
					events.Append(eventType.DisplayName);
					events.Append("<br />");
				}
			}

			return events.ToString();
		}

		#endregion
	}
}
