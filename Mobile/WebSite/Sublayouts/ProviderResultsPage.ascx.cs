﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;

using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using CareMore.Web.DotCom.Provider;

namespace Website.Sublayouts
{
	public partial class ProviderResultsPage : BaseUserControl
	{
		private bool zipEntered = false;
		private Item detailsPage = null;
		private string detailsBaseUrl = string.Empty;
		private string pageKey = string.Empty;
		private string thisId = string.Empty;
		private string lastId = "0"; // Set so lastId at start <> thisId
		private int idAddressCount = 0;

		#region Page events

		protected void Page_Load(object sender, EventArgs e)
		{
			zipEntered = !string.IsNullOrWhiteSpace(Request.QueryString["zip"]);
			NewSearchLB.Text = ItemIds.GetItemValue("NEW_SEARCH_WORDING", "Text", true);

			detailsPage = CurrentItem.Children["Details"];
			detailsBaseUrl = LinkManager.GetItemUrl(detailsPage);
			pageKey = CurrentItem.ID.ToString();

			if (IsPostBack)
			{
				// Make sure previous error is hidden
				MessageLbl.Visible = false;
			}
			else
			{
				bool useCache = false;

				if ((Request.UrlReferrer != null) &&
					(Request.UrlReferrer.AbsolutePath.EndsWith("Details.aspx", StringComparison.OrdinalIgnoreCase)))
				{
					useCache = true;
				}

				ShowResults(useCache);
			}
		}

		protected void ItemsLV_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				SetRowDetails(e.Item, (ProviderSearchResult)e.Item.DataItem);
			}
		}

		protected void ItemsLV_LayoutCreated(object sender, EventArgs e)
		{
			SetColumnHeadings();
		}

		protected void ItemsLV_DataBound(object sender, EventArgs e)
		{
			Item WordingMatchesFoundText = CurrentDb.SelectSingleItem(CareMoreUtilities.WORDING_PATH + "/MatchesFoundText");

			NumResultsLbl.Text = ItemsDP.TotalRowCount.ToString() + " " + WordingMatchesFoundText.Fields["Text"].Value;
			NumResultsLbl2.Text = NumResultsLbl.Text;
		}

		protected void ItemsLV_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
		{
			ItemsDP.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
			ItemsDP2.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
			Session["DataPagerStartRowIndex"] = e.StartRowIndex;
			ShowResults(true);
		}

		protected void NewSearchLB_OnClick(object sender, EventArgs e)
		{
			Response.Redirect(Sitecore.Links.LinkManager.GetItemUrl(CurrentItem.Parent), false);
		}

		#endregion

		#region Private methods

		private void ShowResults(bool useCache)
		{
			try
			{
				if (useCache && Session[pageKey] != null)
				{
					ItemsLV.DataSource = Session[pageKey];
					ItemsLV.DataBind();

					if (Session["DataPagerStartRowIndex"] != null)
					{
						ItemsDP.SetPageProperties((int)Session["DataPagerStartRowIndex"], ItemsDP.MaximumRows, true);
						ItemsDP2.SetPageProperties((int)Session["DataPagerStartRowIndex"], ItemsDP2.MaximumRows, true);
					}
				}
				else
				{
					LoadProviders();
				}
			}
			catch (Exception ex)
			{
#if DEBUG
				CareMoreUtilities.DisplayError(MessagePH, ex, SearchResultsPnl);
#else
				SetDownMessage(ex);
				SearchResultsPnl.Visible = false;
#endif
			}
		}

		private void LoadProviders()
		{
			string specialties;
			bool? isPCP = null;
			if (string.IsNullOrEmpty(Request.QueryString["specialty"]))
			{
				specialties = string.Empty;
			}
			else
			{
				List<string> specialtyList = Request.QueryString["specialty"].Split(new char[]{','}).ToList();

				// Check to see if PCP flag, remove from list if so as separate filter
				if (specialtyList.Contains("PCP"))
				{
					isPCP = true;
					specialtyList.Remove("PCP");
				}

				// Rebuild the delimited list
				specialties = string.Join(",", specialtyList.ToArray());
			}

			string lastName = null;
			if (!string.IsNullOrWhiteSpace(Request.QueryString["lastName"]))
				lastName = HttpUtility.UrlDecode(Request.QueryString["lastName"]);

			string zip = null;
			if (!string.IsNullOrWhiteSpace(Request.QueryString["zip"]))
				zip = Request.QueryString["zip"];

			string city = null;
			if (!string.IsNullOrWhiteSpace(Request.QueryString["city"]))
				city = Request.QueryString["city"];

			string stateName = null;
			if (!string.IsNullOrWhiteSpace(Request.QueryString["state"]))
				stateName = Request.QueryString["state"];

			string languages = null;
			if (!string.IsNullOrWhiteSpace(Request.QueryString["language"]))
				languages = Request.QueryString["language"];

			List<ProviderSearchResult> searchResults = new List<ProviderSearchResult>();

			Item listing = ((InternalLinkField)CurrentItem.Fields["Provider List Type"]).TargetItem;
			string dataSource = CurrentDb.GetItem(listing["Data Source"]).Name;

			if ((dataSource.Equals("Both")) || (dataSource.Equals("Sitecore")))
			{
				List<ProviderSearchResult> sitecoreResults = LoadProvidersSitecore(CurrentDb.GetItem(listing["Sitecore Path"]), lastName, city, zip, stateName, isPCP, languages);
				searchResults.AddRange(sitecoreResults);
			}

			if ((dataSource.Equals("Both")) || (dataSource.Equals("Database")))
			{
				List<ProviderSearchResult> databaseResults = LoadProvidersDatabase(lastName, city, zip, stateName, isPCP, specialties, languages);
				searchResults.AddRange(databaseResults);
			}

			List<string> providerExclusions = ProviderHelper.GetExclusions(listing["Exclusions"]);

			foreach (string exclusion in providerExclusions)
			{
				IEnumerable<ProviderSearchResult> filteredList;

				if (exclusion.Contains('='))
				{
					filteredList = searchResults.Where(item => (string.Format("{0}={1}", item.ProviderId, item.AddressType.Trim()) != exclusion));
				}
				else
				{
					filteredList = searchResults.Where(item => (item.ProviderId != exclusion));
				}

				searchResults = filteredList.ToList();
			}

			LoadProviders(searchResults, zip);
		}

		private List<ProviderSearchResult> LoadProvidersDatabase(string lastName, string city, string zip, string stateName, bool? isPCP, string specialties, string languages)
		{
			return ProviderFinder.GetProviderList(ProviderHelper.GetPortalCode(BL.SiteSettings.SiteSettingPath),
				lastName, zip, city, null, stateName, isPCP, specialties, languages,
				ProviderHelper.GetMaxZipDistance(BL.SiteSettings.SiteSettingPath));
		}

		private List<ProviderSearchResult> LoadProvidersSitecore(Item listRoot, string lastName, string city, string zip, string stateName, bool? isPCP, string languages)
		{
			Item englishItem = base.CurrentDb.GetItem(listRoot.ID, Sitecore.Globalization.Language.Parse("en"));

			IEnumerable<Item> list = englishItem.Axes.GetDescendants().Where(i => !i.TemplateID.Equals(Common.FolderTemplateID));

			if (!string.IsNullOrWhiteSpace(lastName))
			{
				IEnumerable<Item> filteredList = from item in list
												 where item["Provider Name"].IndexOf(lastName, StringComparison.OrdinalIgnoreCase) > -1
												 select item;
				list = filteredList;
			}

			if (!string.IsNullOrWhiteSpace(zip))
			{
				List<ZipCode> zipCodes = ProviderFinder.GetZipCodes(zip, ProviderHelper.GetMaxZipDistance(BL.SiteSettings.SiteSettingPath));

				IEnumerable<Item> filteredList = from item in list
												 join zipCode in zipCodes on item["Postal Code"] equals zipCode.BaseZipCode
												 select item;
				list = filteredList;
			}

			if (!string.IsNullOrWhiteSpace(city))
			{
				IEnumerable<Item> filteredList = from item in list
												 where item["City"].Equals(city, StringComparison.OrdinalIgnoreCase)
												 select item;
				list = filteredList;
			}

			if (isPCP != null && isPCP.Value)
			{
				IEnumerable<Item> filteredList = from item in list
												 where item.Fields["Is PCP"].Value.Equals("1")
												 select item;
				list = filteredList;
			}

			if (!string.IsNullOrWhiteSpace(languages))
			{
				string[] languageList = languages.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

				IEnumerable<Item> filteredList = from item in list
												 where Common.ContainsString(item["Languages"], languageList)
												 select item;
				list = filteredList;
			}

			List<ProviderSearchResult> searchResults = new List<ProviderSearchResult>();

			foreach (Item item in list)
			{
				ProviderSearchResult searchResult = ProviderSearchDataContext.ToProviderSearchResult(item);
				searchResults.Add(searchResult);
			}

			return searchResults;
		}

		private void LoadProviders(List<ProviderSearchResult> searchResults, string zip)
		{
			if (searchResults.Count == 0)
			{
				CareMoreUtilities.DisplayError(MessagePH, MessageLbl,
					ItemIds.GetItem("NO_RESULTS")["Text"], SearchResultsPnl);
			}
			else
			{
				List<ProviderSearchResult> orderedList;

				if (string.IsNullOrWhiteSpace(zip))
				{
					orderedList = (from item in searchResults
					               orderby item.Name, item.ProviderId, item.AddressType
					               select item).ToList();
				}
				else
				{
					orderedList = (from item in searchResults
					               orderby item.Distance, item.Name, item.ProviderId, item.AddressType
					               select item).ToList();
				}

				ItemsLV.DataSource = orderedList;
				ItemsLV.DataBind();

				Session[pageKey] = orderedList;
				Session["DataPagerStartRowIndex"] = null;
				SearchResultsPnl.Visible = true;
			}
		}

		private void SetColumnHeadings()
		{
			Item wordingColumnPhysicianText = CurrentDb.SelectSingleItem(CareMoreUtilities.WORDING_PATH + "/ColumnPhysicianText");
			Item wordingColumnSpecialtiesText = CurrentDb.SelectSingleItem(CareMoreUtilities.WORDING_PATH + "/ColumnSpecialtiesText");
			Item wordingColumnMedicalOfficeText = CurrentDb.SelectSingleItem(CareMoreUtilities.WORDING_PATH + "/ColumnMedicalOfficeText");
			Item wordingColumnDistanceText = CurrentDb.SelectSingleItem(CareMoreUtilities.WORDING_PATH + "/ColumnDistanceText");

			Label lblPhysician = (Label)ItemsLV.FindControl("lblPhysician");
			Label lblSpecialties = (Label)ItemsLV.FindControl("lblSpecialties");
			Label lblMedicalOffice = (Label)ItemsLV.FindControl("lblMedicalOffice");
			Label lblDistance = (Label)ItemsLV.FindControl("lblDistance");

			lblPhysician.Text = wordingColumnPhysicianText.Fields["Text"].Value;
			lblSpecialties.Text = wordingColumnSpecialtiesText.Fields["Text"].Value;
			lblMedicalOffice.Text = wordingColumnMedicalOfficeText.Fields["Text"].Value;
			lblDistance.Text = wordingColumnDistanceText.Fields["Text"].Value;
		}

		private void SetDownMessage(Exception ex)
		{
			Item message = ItemIds.GetItem("FIND_A_DOCTOR_DOWN_MESSAGE");
			CareMoreUtilities.DisplayError(MessagePH, MessageLbl,
				message["Text"], SearchResultsPnl);
			Common.EmailException(ex);
		}

		private void SetRowDetails(ListViewItem control, ProviderSearchResult row)
		{
			thisId = row.ProviderId;

			if (thisId.Equals(lastId))
			{
				idAddressCount++;
			}
			else
			{
				idAddressCount = 1;

				HtmlTableRow trSeparator = (HtmlTableRow)control.FindControl("trSeparator");
				trSeparator.Visible = true;

				HyperLink nameHL = (HyperLink)control.FindControl("NameHL");
				nameHL.NavigateUrl = string.Format(ProviderHelper.DetailsUrlFormat, detailsBaseUrl, row.ProviderId, row.AddressId, row.AddressType);

				if (string.IsNullOrWhiteSpace(row.Title))
					nameHL.Text = row.Name;
				else
					nameHL.Text = string.Format("{0}, {1}", row.Name, row.Title);

				Literal idDLit = (Literal)control.FindControl("IDLit");
				idDLit.Text = string.Format("<br />ID: {0}<br />", row.ProviderId);

				Label effectiveLbl = (Label)control.FindControl("EffectiveLbl");
				ProviderHelper.SetEffectiveDate(effectiveLbl, CurrentItem["Effective Date"], row.EffectiveDate);

				Label AcceptingNewPatientsLbl = (Label)control.FindControl("AcceptingNewPatientsLbl");
				if (row.AcceptingPatients.Equals("Y"))
					AcceptingNewPatientsLbl.Text = string.Format("{0}: Yes", detailsPage["Accepting New Patients"]);
				else
					AcceptingNewPatientsLbl.Text = string.Format("{0}: No", detailsPage["Accepting New Patients"]);

				Literal specialtiesLit = (Literal)control.FindControl("SpecialtiesLit");
				specialtiesLit.Text = row.PrimarySpecialty;
				if (!string.IsNullOrEmpty(row.SecondarySpecialty))
				{
					specialtiesLit.Text += "<br />" + row.SecondarySpecialty;
				}
			}

			Literal addressLit = (Literal)control.FindControl("AddressLit");
			StringBuilder addressHtml = new StringBuilder();

			Literal distanceLit = (Literal)control.FindControl("DistanceLit");
			StringBuilder distanceHtml = new StringBuilder();

			if (idAddressCount < 5)
			{
				if (idAddressCount > 1)
				{
					SetRowAddressSeparator(control, false);
				}

				addressHtml.Append(LocationHelper.GetFormattedAddress(row.Address1, row.Address2,
					row.City, row.State, row.Zip));

				if (!string.IsNullOrEmpty(row.Phone))
					addressHtml.AppendFormat("<br />Ph: {0}", LocationHelper.GetFormattedPhoneNo(row.Phone));

				if (row.AddressEffectiveDate < DateTime.Now)
					ProviderHelper.SetEffectiveDate(addressHtml, CurrentItem["Effective Date"], row.AddressEffectiveDate);

				addressLit.Text = addressHtml.ToString();

				if (zipEntered)
					distanceHtml.AppendFormat("{0:0.00} miles<br /><br />", row.Distance);

				distanceHtml.AppendFormat("<a href=\"{0}\">{1}</a>",
					string.Format(ProviderHelper.DetailsUrlFormat, detailsBaseUrl, row.ProviderId, row.AddressId, row.AddressType),
					CurrentItem["View Map"]);

				distanceLit.Text = distanceHtml.ToString();
			}
			else if (idAddressCount == 5)
			{
				SetRowAddressSeparator(control, true);

				addressHtml.AppendFormat("<a href=\"{0}\">{1}</a>",
					string.Format(ProviderHelper.DetailsUrlFormat, detailsBaseUrl, row.ProviderId, row.AddressId, row.AddressType),
					CurrentItem["More Addresses"]);
				addressLit.Text = addressHtml.ToString();
				distanceLit.Text = "&nbsp;";
			}
			else
			{
				HtmlTableRow trItemRow = (HtmlTableRow)control.FindControl("trItemRow");
				trItemRow.Visible = false;
			}

			lastId = thisId;
		}

		private void SetRowAddressSeparator(ListViewItem control, bool hideDistance)
		{
			HtmlTableCell tdAddress = (HtmlTableCell)control.FindControl("tdAddress");
			tdAddress.Attributes.Add("class", "addressSep");

			HtmlTableCell tdDistance = (HtmlTableCell)control.FindControl("tdDistance");

			if (hideDistance)
			{
				tdAddress.ColSpan = 2;
				tdDistance.Visible = false;
			}
			else
			{
				tdDistance.Attributes.Add("class", "addressSep");
			}
		}

		#endregion
	}
}
