﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ContactUsPage.ascx.cs" Inherits="Website.Sublayouts.ContactUsPage" %>

<!-- Begin SubLayouts/ContactUsPage -->
<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-2">
						<sc:Image ID="imgContactUs" runat="server" CssClass="img-responsive" Field="Contact Us Image" />
					</div>
					<div class="col-xs-10">
						<h2><sc:Text ID="txtContactUs" runat="server" Field="Contact Us Header" /></h2>
					</div>
				</div>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						<p class="text-primary"><sc:Text ID="txtImmediate" runat="server" Field="Immediate Assistance Text" /></p>
						<p><strong><sc:Text ID="txtMemberSvcs" Field="Member Services Text" runat="server" /></strong></p>

						<p class="text-primary"><strong><sc:Text ID="txtAddressHdr" runat="server" Field="Address Text Header" /></strong></p>
						<p><sc:Text ID="txtAddressText" Field="Address Text" runat="server" /></p>

						<p class="text-primary"><strong><sc:Text ID="txtCallHeader" runat="server" Field="Call Text Header" /></strong></p>
					</div>
				</div>
<asp:Repeater ID="rptCallText" runat="server">
	<HeaderTemplate>
				<div class="row">
	</HeaderTemplate>
	<ItemTemplate>
					<div class="col-xs-4"><sc:Text ID="txtCallTitle" runat="server" Field="Title" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /> </div>
					<div class="col-xs-8"><sc:Text ID="txtCallContent" runat="server" Field="Content" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></div>
	</ItemTemplate>
	<SeparatorTemplate>
				</div>
				<div class="row row-xs-margin-top">
	</SeparatorTemplate>
	<FooterTemplate>
				</div>
	</FooterTemplate>
</asp:Repeater>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-2">
						<sc:Image ID="imgHours" runat="server" CssClass="img-responsive" Field="Hours Image" />
					</div>
					<div class="col-xs-10">
						<h2><sc:Text ID="txtHoursHdr" runat="server" Field="Hours Header" /></h2>
					</div>
				</div>
			</div>
			<div class="panel-body">
				<sc:Text ID="txtHoursText" runat="server" Field="Hours Text" />
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-2">
						<sc:Image ID="imgForm" runat="server" CssClass="img-responsive" Field="Form Image" />
					</div>
					<div class="col-xs-10">
						<h2><sc:Text ID="txtFormHeader" runat="server" Field="Form Header" /></h2>
					</div>
				</div>
			</div>
			<div class="panel-body">
				<p><sc:Text ID="txtMessageContent" runat="server"  Field="MessageContent"/></p>

				<asp:PlaceHolder ID="phConfirmation" runat="server" Visible="false">
					<sc:Text ID="txtThankYouMessage" runat="server" Field="ThankYouMessage" />
				</asp:PlaceHolder>

				<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
					<div class="form-group">
						<label for="txtFirstName" class="col-md-3 control-label"><sc:Text ID="txtFirstNameLabel" runat="server" Field="First Name Label" /></label>
						<div class="col-md-5">
							<asp:TextBox ID="txtFirstName" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="50" TextMode="SingleLine" />
						</div>
					</div>
					<div class="form-group">
						<label for="txtLastName" class="col-md-3 control-label"><sc:Text ID="txtLastNameLabel" runat="server" Field="Last Name Label" /></label>
						<div class="col-md-5">
							<asp:TextBox ID="txtLastName" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="50" TextMode="SingleLine" />
						</div>
					</div>
					<div class="form-group">
						<label for="txtMemberId" class="col-md-3 control-label"><sc:Text ID="txtMemberIdLabel" runat="server" Field="Member ID Label" /></label>
						<div class="col-md-5">
							<asp:TextBox ID="txtMemberId" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="50" TextMode="SingleLine" />
						</div>
					</div>
					<div class="form-group">
						<label for="txtEmail" class="col-md-3 control-label"><sc:Text ID="txtEmailLabel" runat="server" Field="Email Label" /></label>
						<div class="col-md-9">
							<asp:TextBox ID="txtEmail" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="50" />
							<asp:RequiredFieldValidator id="reqValEmail" runat="server" ControlToValidate="txtEmail" Display="Static" OnServerValidate="ReqValEmail_OnServerValidate" />
						</div>
					</div>
					<div class="form-group">
						<label for="txtPhone" class="col-md-3 control-label"><sc:Text ID="txtPhoneNumberLabel" runat="server" Field="Phone Number Label" /></label>
						<div class="col-md-5">
							<asp:TextBox ID="txtPhone" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="50" TextMode="SingleLine" placeholder="###-###-####" />
						</div>
					</div>
					<div class="form-group">
						<label for="rblTimeToCall" class="col-md-3 control-label"><sc:Text ID="txtTimeToCallLabel" runat="server" Field="Time To Call Label" /></label>
						<div class="col-md-5">
							<asp:RadioButtonList ID="rblTimeToCall" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
								<asp:ListItem Text="AM" Value="AM" />
								<asp:ListItem Text="PM" Value="PM" />
							</asp:RadioButtonList>
						</div>
					</div>
					<div class="form-group">
						<label for="txtComments" class="col-md-3 control-label"><sc:Text ID="txtCommentLabel" runat="server" Field="Comment Label" /></label>
						<div class="col-md-9">
							<asp:TextBox ID="txtComments" runat="server" ClientIDMode="Static" CssClass="form-control" Rows="6" TextMode="MultiLine" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-3 col-md-4">
							<asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_Click" />
						</div>
					</div>
				</asp:Panel>

				<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
			</div>
		</div>
	</div>
</div>
<!-- End SubLayouts/ContactUsPage -->
