﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProviderDetailsPage.ascx.cs" Inherits="Website.Sublayouts.ProviderDetailsPage" %>
<%@ Register TagPrefix="cm" TagName="GoogleMap" Src="~/Controls/GoogleMap.ascx" %>

<!-- Begin SubLayouts/ProverDetailsPage -->
<div class="row">
	<div id="col-xs-12">
		<h2><asp:Label ID="lblPhysicianText" runat="server" /></h2>

		<div class="row">
			<div class="col-xs-3"><asp:LinkButton ID="BackToResultsLB" runat="server" OnClick="BackToResultsLB_Click" CssClass="icon icoArrow icoArrowBack link"><sc:Text ID="lblBack" runat="server" Field="Back To Results" /></asp:LinkButton></div>
			<div class="col-xs-1"><a href="#" onclick="window.print()" class="link"><sc:Text ID="lblPrint" runat="server" Field="print" /></a></div>
		</div>

		<hr />

		<asp:PlaceHolder ID="phDetails" runat="server">
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<h4><sc:Text ID="ContactInfoLabel" runat="server" Field="Contact Info" /></h4>

					<div class="row">
						<div class="col-xs-4"><em><sc:Text ID="PhoneLabel" runat="server" Field="Phone" />:</em></div>
						<div class="col-xs-8"><asp:Label ID="PhoneText" runat="server" /></div>
					</div>

					<asp:Panel ID="pnlFax" runat="server" CssClass="row">
						<div class="col-xs-4"><em><sc:Text ID="FaxLabel" runat="server" Field="Fax" />:</em></div>
						<div class="col-xs-8"><asp:Label ID="FaxText" runat="server" /></div>
					</asp:Panel>

					<hr />

					<h5><sc:Text ID="LocationLabel" runat="server" Field="Location" /></h5>

					<address>
						<asp:Literal ID="LocationText" runat="server" />
					</address>

					<p style="margin-top:20px;"><asp:HyperLink ID="DirectionsLink" CssClass="icon icoArrow link" runat="server" Target="_blank"><sc:Text ID="GetDirectionsLabel" runat="server" Field="Get Directions" /></asp:HyperLink></p>
				</div>

				<div class="col-xs-12 col-md-6">
					<cm:GoogleMap ID="GoogleMap" runat="server" InitialZoom="13" />
				</div>
			</div>

			<div class="row">
				<hr />

				<div class="col-xs-12">
					<asp:Panel ID="EffectivePnl" CssClass="row" runat="server" Visible="false">
						<div class="row">
							<div class="col-xs-4"><sc:Text ID="EffectiveDateLabel" runat="server" Field="Effective Date" /></div>
							<div class="col-xs-8"><asp:Label ID="EffectiveDateText" runat="server" Font-Bold="true" /></div>
						</div>
					</asp:Panel>
					<div class="row">
						<div class="col-xs-4"><sc:Text ID="SpecialtiesLabel" runat="server" Field="Specialties" /></div>
						<div class="col-xs-8"><asp:Label ID="SpecialtiesText" runat="server" /></div>
					</div>
					<div class="row">
						<div class="col-xs-4"><sc:Text ID="LanguagesLabel" runat="server" Field="Languages" /></div>
						<div class="col-xs-8">
							<asp:Label ID="LanguagesText" runat="server" />
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4"><sc:Text ID="AcceptingNewPatientsLabel" runat="server" Field="Accepting New Patients" /></div>
						<div class="col-xs-8">
							<asp:Label ID="AcceptingNewPatientsText" runat="server" />
						</div>
					</div>
				</div>

				<asp:Repeater ID="MedicalOffices" runat="server" OnItemDataBound="MedicalOffices_ItemDataBound">
					<HeaderTemplate>
						<hr />
						<h5 style="margin-bottom:10px;"><sc:Text ID="MedicalOfficesLabel" runat="server" Field="Medical Offices" /></h5>
						<div class="providerContent">
					</HeaderTemplate>
					<ItemTemplate>
						<asp:Panel ID="pnlState" runat="server" CssClass="state clear" Visible="false" />
						<asp:Panel ID="pnlCounty" runat="server" CssClass="county clear" Visible="false" />
						<div class="item address">
							<%# AddressHtml(Container.DataItem) %>
						</div>
					</ItemTemplate>
					<SeparatorTemplate>
						<hr />
					</SeparatorTemplate>
					<FooterTemplate>
						</div>
					</FooterTemplate>
				</asp:Repeater>
			</div>
		</asp:PlaceHolder>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End SubLayouts/ProverDetailsPage -->
