﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="LocateServices.ascx.cs" Inherits="Website.Sublayouts.LocateServices" %>
<%@ Register TagPrefix="cm" TagName="GoogleMap" Src="~/Controls/GoogleMap.ascx" %>

<!-- Begin SubLayouts/LocateServices -->
<div class="form-horizontal">
	<div class="form-group">
		<div class="col-xs-12 col-md-4">
			<label for="CountyList"><sc:Text runat="server" Field="County Label" /></label><br />
			<asp:DropDownList ID="ddlCountyList" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="DdlCountyList_SelectedIndexChanged"></asp:DropDownList>
		</div>
		<div class="col-xs-12 col-md-4">
			<label for="MapKeyList"><sc:Text runat="server" Field="Show Label" /></label><br />
			<asp:DropDownList ID="ddlMapKeyList" runat="server" AutoPostBack="true" CssClass="form-control" DataTextField="Text" DataValueField="Value" OnSelectedIndexChanged="DdlMapKeyList_SelectedIndexChanged"></asp:DropDownList>
		</div>
	</div>
</div>

<asp:PlaceHolder id="phLocationsMap" runat="server">
	<div class="row">
		<div class="col-xs-12 col-md-10">
			<cm:GoogleMap ID="googleMap" runat="server" />
		</div>
	</div>
	<div class="row top-spacer">
		<div class="col-xs-12">
			<p><strong><sc:Text ID="txtKey" runat="server" Field="Key Label" /></strong></p>
		</div>
	</div>
	<asp:Repeater ID="rptMapKeys" runat="server">
		<HeaderTemplate>
			<div class="row">
		</HeaderTemplate>
		<ItemTemplate>
				<div class="col-xs-6 col-md-4">
					<p><img src='<%# Eval("ImageUrl") %>' alt='<%# Eval("Text") %>' class="img-responsive pull-left margin-right" /><%# Eval("Text") %></p>
				</div>
		</ItemTemplate>
		<FooterTemplate>
			</div>
		</FooterTemplate>
	</asp:Repeater>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
<!-- End SubLayouts/LocateServices -->
