﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="RegistrationPage.ascx.cs" Inherits="Website.Sublayouts.Broker.RegistrationPage" %>
<%@ Register src="~/Sublayouts/Broker/BrokerProfileForm.ascx" TagName="ProfileForm" TagPrefix="uc2" %>

<!-- Begin SubLayouts/Broker/RegistrationPage -->
<div class="row">
	<div class="col-xs-12">
<asp:PlaceHolder ID="phConfirmation" runat="server" Visible="false">
		<div class="row">
			<div class="col-xs-12">
				<sc:Text ID="txtThankYouMessage" runat="server" Field="ThankYouMessage" />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<sc:Link ID="lnkNavigateTo" runat="server" Field="NavigateTo" CssClass="button btnMed">OK</sc:Link>
			</div>
		</div>
</asp:PlaceHolder>

		<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
			<uc2:ProfileForm id="ucProfileForm" runat="server" />
			<div class="row">
				<div class="col-xs-12">
					<sc:Text runat="server" Field="Agreement" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-6 text-center">
					<asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_Click" Text="I Agree" />
				</div>
				<div class="col-xs-6 text-center">
					<sc:Link ID="lnkCancel" runat="server" CssClass="btn btn-danger" Field="NavigateTo">Cancel</sc:Link>
				</div>
			</div>
		</asp:Panel>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End SubLayouts/Broker/RegistrationPage -->
