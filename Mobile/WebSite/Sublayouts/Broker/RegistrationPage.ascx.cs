﻿using System;
using System.Web.UI;

namespace Website.Sublayouts.Broker
{
	public partial class RegistrationPage : System.Web.UI.UserControl
	{
		private const string PostBackSessionName = "ContactUsPage";

		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				if (IsPostBack)
				{
					CareMoreUtilities.ClearControl(phError);
				}
				else
				{
					Session[PostBackSessionName] = null;
					ucProfileForm.LoadEmptyRegistration();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				if (Session[PostBackSessionName] == null)
				{
					if (ucProfileForm.FormCaptcha.IsValid() && Page.IsValid)
					{
						if (ucProfileForm.AddRegistration(phError))
						{
							//Set session to prevent refresh posts
							Session[PostBackSessionName] = DateTime.Now;
							phConfirmation.Visible = true;
							pnlForm.Visible = false;
						}
					}
				}
				else
				{
					CareMoreUtilities.DisplayNoMultiSubmit(phError);
					phConfirmation.Visible = false;
					pnlForm.Visible = false;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}
	}
}
