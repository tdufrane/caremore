﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BrokerProfileForm.ascx.cs" Inherits="Website.Sublayouts.Broker.BrokerProfileForm" %>
<%@ Register src="../CaptchaBox.ascx" tagname="CaptchaBox" tagprefix="uc1" %>

<p>Required fields are indicated with an asterisk (<span class="text-danger">*</span>)</p>

<asp:HiddenField ID="hidRegistrationId" runat="server" />

<div class="form-group">
	<label for="txtCareMoreId" class="col-xs-12 col-md-3 control-label">CareMore Id <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtCareMoreId" runat="server" CssClass="form-control" MaxLength="50" ToolTip="CareMore Id" />
	</div>
	<div class="col-xs-12 col-md-5">
		<asp:RequiredFieldValidator ID="reqValCareMoreId" runat="server" ControlToValidate="txtCareMoreId"
			SetFocusOnError="true" ErrorMessage="CareMore Id is required." />
		<asp:RegularExpressionValidator ID="regValCareMoreId" runat="server" ControlToValidate="txtCareMoreId"
			SetFocusOnError="true" ErrorMessage="Not a valid CareMore Id"
			ValidationExpression="BKR[0-9]{5}" />
	</div>
</div>
<div class="form-group">
	<label for="txtLastName" class="col-xs-12 col-md-3 control-label">Last Name <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" MaxLength="50" ToolTip="Last Name" />
	</div>
	<div class="col-xs-12 col-md-5">
		<asp:RequiredFieldValidator ID="reqValLastName" runat="server" ControlToValidate="txtLastName"
			SetFocusOnError="true" ErrorMessage="Last Name is required." />
	</div>
</div>
<div class="form-group">
	<label for="txtFirstName" class="col-xs-12 col-md-3 control-label">First Name <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" MaxLength="50" ToolTip="First Name" />
	</div>
	<div class="col-xs-12 col-md-5">
		<asp:RequiredFieldValidator ID="reqValFirstName" runat="server" ControlToValidate="txtFirstName"
			SetFocusOnError="true" ErrorMessage="First Name is required." />
	</div>
</div>
<div class="form-group">
	<label for="txtMiddleInitial" class="col-xs-12 col-md-3 control-label">Middle Initial</label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtMiddleInitial" runat="server" CssClass="form-control" MaxLength="1" ToolTip="Middle Initial" />
	</div>
</div>
<div class="form-group">
	<label for="rbLstSalutation" class="col-xs-12 col-md-3 control-label">Salutation</label>
	<div class="col-xs-12 col-md-4" style="margin-top:4px;">
		<asp:RadioButtonList ID="rbLstSalutation" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal"
			RepeatLayout="Flow" ToolTip="Salutation">
			<asp:ListItem>Mr.</asp:ListItem>
			<asp:ListItem>Mrs.</asp:ListItem>
			<asp:ListItem>Ms.</asp:ListItem>
		</asp:RadioButtonList>
	</div>
</div>
<div class="form-group">
	<label for="txtHomePhone" class="col-xs-12 col-md-3 control-label">Phone Number <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtHomePhone" runat="server" CssClass="form-control" MaxLength="12" ToolTip="Phone Number" placeholder="###-###-####" />
	</div>
	<div class="col-xs-12 col-md-5">
		<asp:RequiredFieldValidator ID="reqValHomePhone" runat="server" ControlToValidate="txtHomePhone"
			SetFocusOnError="true" ErrorMessage="Phone Number is required." />
		<asp:RegularExpressionValidator ID="regValHomePhone" runat="server" ControlToValidate="txtHomePhone"
			ValidationExpression="^([2-9]{1}[0-9]{2}-{0,1}){2}[0-9]{4}[ ]*$"
			SetFocusOnError="true" ErrorMessage="Phone Number is incorrect; Make sure number is valid and use the format ###-###-#### or ##########." />
	</div>
</div>
<div class="form-group">
	<label for="txtAddress" class="col-xs-12 col-md-3 control-label">Address <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" MaxLength="100" ToolTip="Address" />
	</div>
	<div class="col-xs-12 col-md-5">
		<asp:RequiredFieldValidator ID="reqValAddress" runat="server" ControlToValidate="txtAddress"
			SetFocusOnError="true" ErrorMessage="Address is required." />
	</div>
</div>
<div class="form-group">
	<label for="txtCity" class="col-xs-12 col-md-3 control-label">City <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtCity" runat="server" CssClass="form-control" MaxLength="50" ToolTip="City" />
	</div>
	<div class="col-xs-12 col-md-5">
		<asp:RequiredFieldValidator ID="reqValCity" runat="server" ControlToValidate="txtCity"
			SetFocusOnError="true" ErrorMessage="City is required." />
	</div>
</div>
<div class="form-group">
	<label for="ddlState" class="col-xs-12 col-md-3 control-label">State <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:DropDownList ID="ddlState" runat="server" CssClass="form-control">
			<asp:ListItem Text="-select one-" Value="" />
			<asp:ListItem Text="Alabama" Value="Alabama" />
			<asp:ListItem Text="Alaska" Value="Alaska" />
			<asp:ListItem Text="Arizona" Value="Arizona" />
			<asp:ListItem Text="Arkansas" Value="Arkansas" />
			<asp:ListItem Text="California" Value="California" />
			<asp:ListItem Text="Colorado" Value="Colorado" />
			<asp:ListItem Text="Connecticut" Value="Connecticut" />
			<asp:ListItem Text="Delaware" Value="Delaware" />
			<asp:ListItem Text="Florida" Value="Florida" />
			<asp:ListItem Text="Georgia" Value="Georgia" />
			<asp:ListItem Text="Hawaii" Value="Hawaii" />
			<asp:ListItem Text="Idaho" Value="Idaho" />
			<asp:ListItem Text="Illinois" Value="Illinois" />
			<asp:ListItem Text="Indiana" Value="Indiana" />
			<asp:ListItem Text="Iowa" Value="Iowa" />
			<asp:ListItem Text="Kansas" Value="Kansas" />
			<asp:ListItem Text="Kentucky" Value="Kentucky" />
			<asp:ListItem Text="Louisiana" Value="Louisiana" />
			<asp:ListItem Text="Maine" Value="Maine" />
			<asp:ListItem Text="Maryland" Value="Maryland" />
			<asp:ListItem Text="Massachusetts" Value="Massachusetts" />
			<asp:ListItem Text="Michigan" Value="Michigan" />
			<asp:ListItem Text="Minnesota" Value="Minnesota" />
			<asp:ListItem Text="Mississippi" Value="Mississippi" />
			<asp:ListItem Text="Missouri" Value="Missouri" />
			<asp:ListItem Text="Montana" Value="Montana" />
			<asp:ListItem Text="Nebraska" Value="Nebraska" />
			<asp:ListItem Text="Nevada" Value="Nevada" />
			<asp:ListItem Text="New Hampshire" Value="New Hampshire" />
			<asp:ListItem Text="New Jersey" Value="New Jersey" />
			<asp:ListItem Text="New Mexico" Value="New Mexico" />
			<asp:ListItem Text="New York" Value="New York" />
			<asp:ListItem Text="North Carolina" Value="North Carolina" />
			<asp:ListItem Text="North Dakota" Value="North Dakota" />
			<asp:ListItem Text="Ohio" Value="Ohio" />
			<asp:ListItem Text="Oklahoma" Value="Oklahoma" />
			<asp:ListItem Text="Oregon" Value="Oregon" />
			<asp:ListItem Text="Pennsylvania" Value="Pennsylvania" />
			<asp:ListItem Text="Rhode Island" Value="Rhode Island" />
			<asp:ListItem Text="South Carolina" Value="South Carolina" />
			<asp:ListItem Text="South Dakota" Value="South Dakota" />
			<asp:ListItem Text="Tennessee" Value="Tennessee" />
			<asp:ListItem Text="Texas" Value="Texas" />
			<asp:ListItem Text="Utah" Value="Utah" />
			<asp:ListItem Text="Vermont" Value="Vermont" />
			<asp:ListItem Text="Virginia" Value="Virginia" />
			<asp:ListItem Text="Washington" Value="Washington" />
			<asp:ListItem Text="West Virginia" Value="West Virginia" />
			<asp:ListItem Text="Wisconsin" Value="Wisconsin" />
			<asp:ListItem Text="Wyoming" Value="Wyoming" />
		</asp:DropDownList>
	</div>
	<div class="col-xs-4 col-md-5">
		<asp:RequiredFieldValidator ID="reqValState" runat="server" ControlToValidate="ddlState"
			SetFocusOnError="true" ErrorMessage="State is required." InitialValue="" />
	</div>
</div>
<div class="form-group">
	<label for="txtZip" class="col-xs-12 col-md-3 control-label">Zip Code <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtZip" runat="server" CssClass="form-control" MaxLength="5" ToolTip="Zip Code" placeholder="#####" />
	</div>
	<div class="col-xs-12 col-md-5">
		<asp:RequiredFieldValidator ID="reqValZip" runat="server" ControlToValidate="txtZip"
			SetFocusOnError="true" ErrorMessage="Zip Code is required." />
		<asp:RegularExpressionValidator ID="regValZip" runat="server" ControlToValidate="txtZip"
			SetFocusOnError="true" ErrorMessage="Format for Zip Code is incorrect."
			ValidationExpression="^[0-9]{5}$" />
	</div>
</div>
<div class="form-group">
	<label for="txtEmail" class="col-xs-12 col-md-3 control-label">Email <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="100" ToolTip="Email" />
		<div class="text-info">(This will be your Login Name)</div>
	</div>
	<div class="col-xs-12 col-md-5">
		<asp:RequiredFieldValidator ID="reqValEmail" ControlToValidate="txtEmail" runat="server"
			SetFocusOnError="true" ErrorMessage="Email is required." />
		<asp:RegularExpressionValidator ID="regValEmail" runat="server" ControlToValidate="txtEmail"
			SetFocusOnError="true" ErrorMessage="Format for Email is incorrect."
			ValidationExpression="^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*[ ]*$" />
	</div>
</div>
<asp:PlaceHolder ID="phUpdateOnly" runat="server">
	<div class="form-group">
		<label for="txtVerifyPassword" class="col-xs-12 col-md-3 control-label">Verify Password <span class="text-danger">*</span></label>
		<div class="col-xs-12 col-md-4">
			<asp:TextBox ID="txtVerifyPassword" runat="server" CssClass="form-control" MaxLength="20" TextMode="Password" ToolTip="Verify Password" />
		</div>
		<div class="col-xs-12 col-md-5">
			<asp:RequiredFieldValidator ID="reqValVerifyPassword" runat="server" ControlToValidate="txtVerifyPassword"
				SetFocusOnError="true" ErrorMessage="Verify Password is required." />
			<asp:RegularExpressionValidator ID="regValVerifyPassword" runat="server" ControlToValidate="txtVerifyPassword"
				SetFocusOnError="true" ErrorMessage="Verify Password must be at least 6 characters."
				ValidationExpression=".{6,20}" />
		</div>
	</div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phRegisterOnly" runat="server">
	<div class="form-group">
		<label for="txtPassword" class="col-xs-12 col-md-3 control-label">Password <span class="text-danger">*</span></label>
		<div class="col-xs-12 col-md-4">
			<asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" MaxLength="20" TextMode="Password" ToolTip="Password" />
		</div>
		<div class="col-xs-12 col-md-5">
			<asp:RequiredFieldValidator ID="reqValPassword" runat="server" ControlToValidate="txtPassword"
				SetFocusOnError="true" ErrorMessage="Password is required." />
			<asp:RegularExpressionValidator ID="regValPassword" runat="server" ControlToValidate="txtPassword"
				SetFocusOnError="true" ErrorMessage="Password must be at least 6 characters."
				ValidationExpression=".{6,20}" />
		</div>
	</div>
	<div class="form-group">
		<label for="txtPassword2" class="col-xs-12 col-md-3 control-label">Repeat Password <span class="text-danger">*</span></label>
		<div class="col-xs-12 col-md-4">
			<asp:TextBox ID="txtPassword2" runat="server" CssClass="form-control" MaxLength="20" TextMode="Password" ToolTip="Repeat Password" />
		</div>
		<div class="col-xs-12 col-md-5">
			<asp:RequiredFieldValidator ID="reqValRepeatPassword" runat="server" ControlToValidate="txtPassword2"
				SetFocusOnError="true" ErrorMessage="Repeat Password is required." />
			<asp:CompareValidator ID="comValPassword" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtPassword2"
				SetFocusOnError="true" ErrorMessage="Passwords do not match." />
		</div>
	</div>
	<div class="form-group">
		<label for="captchaBoxPage" class="col-xs-12 col-md-3 control-label">Enter the characters<br /> you see <span class="text-danger">*</span></label>
		<div class="col-xs-12 col-md-9">
			<uc1:CaptchaBox ID="captchaBoxPage" runat="server" />
		</div>
	</div>
</asp:PlaceHolder>
