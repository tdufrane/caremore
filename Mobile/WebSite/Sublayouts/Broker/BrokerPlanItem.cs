﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Website.BL.ListItem;

namespace Website.Sublayouts.Broker
{
	public class BrokerPlanItem
	{
		public BrokerPlanItem(Item planTypeItem, Item countyItem)
		{
			StateName = countyItem["State"];
			CountyName = countyItem["Region Name"];
			PlanName = planTypeItem["Title"];
			PlanDescription = countyItem["Description"];
		}

		public string StateName { get; set; }
		public string CountyName { get; set; }
		public string PlanName { get; set; }
		public string PlanDescription { get; set; }
		public List<PlanFileItem> Files { get; set; }
	}
}
