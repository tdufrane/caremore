﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CareMore.Web.DotCom;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace Website.Sublayouts.Broker
{
	public partial class BrokerLoginWidget : BrokerBaseUserControl
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				if (IsPostBack)
				{
					CareMoreUtilities.ClearControl(phError);
				}
				else
				{
					LoadPage();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				LoginBroker(txtEmail.Text, txtPassword.Text);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			if (Session[CareMoreUtilities.BROKER_SESSION] == null)
			{
				HttpCookie cookie = Request.Cookies[BrokerRegistrationKey];

				if ((cookie != null) && (!string.IsNullOrEmpty(cookie.Value)))
				{
					txtEmail.Text = cookie.Value;
				}
			}
			else
			{
				this.Visible = false;
			}
		}

		public void LoginBroker(string email, string password)
		{
			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			IEnumerable<BrokerRegistration> profiles = db.BrokerRegistrations.Where(item =>
				(item.Email.ToLower() == email.ToLower()) && (item.Password == Common.Encrypt(password)));

			if ((profiles == null) || (profiles.Count() == 0))
			{
				CareMoreUtilities.DisplayError(phError,
					string.Format(CurrentItem["InvalidLogin"], "Login"));
			}
			else
			{
				BrokerRegistration profile = profiles.SingleOrDefault();

				if (!profile.Verified)
				{
					CareMoreUtilities.DisplayError(phError,
						string.Format(CurrentItem[RegistrationNotVerifiedKey], "Login"));
					pnlForm.Visible = false;
				}
				else if (profile.Disabled)
				{
					CareMoreUtilities.DisplayError(phError,
						string.Format(CurrentItem[RegistrationDisabledKey], "Login"));
					pnlForm.Visible = false;
				}
				else
				{
					Session[CareMoreUtilities.BROKER_SESSION] = profile;

					// Save for next time user comes to page
					HttpCookie aCookie = new HttpCookie("BrokerRegistration");
					aCookie.Value = profile.Email;
					aCookie.Expires = DateTime.Now.AddYears(1);
					Response.Cookies.Add(aCookie);

					Item destItem;
					if (profile.PasswordUpdated)
					{
						destItem = ItemIds.GetItem("BROKERS_LOGGED_IN_HOME");
					}
					else
					{
						destItem = ItemIds.GetItem("BROKER_PASSWORDCHANGE");
					}

					//sitecore/content/Home/Brokers/Portal/Broker Portal Home/Broker Password Change
					string url = LinkManager.GetItemUrl(destItem);
					Page.Response.Redirect(url, false);
				}
			}
		}

		#endregion
	}
}
