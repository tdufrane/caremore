﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom;
using Sitecore.Data.Items;

namespace Website.Sublayouts.Broker
{
	public partial class BrokerProfileForm : System.Web.UI.UserControl
	{
		#region Properties

		public CaptchaBox FormCaptcha { get { return this.captchaBoxPage; } }

		#endregion

		#region Public Methods

		public bool AddRegistration(PlaceHolder phError)
		{
			bool added = false;

			string email = txtEmail.Text.TrimEnd();

			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			IEnumerable<BrokerRegistration> results = db.BrokerRegistrations.Where(item => (item.Email.ToLower() == email.ToLower()));

			if (results.Count() > 0)
			{
				CareMoreUtilities.DisplayError(phError, string.Format("The email '{0}' has already been registered.", email));
			}
			else
			{
				BrokerRegistration profile = new BrokerRegistration()
				{
					RegistrationId = Guid.NewGuid(),
					CreatedOn = DateTime.Now,
					PasswordUpdated = true,
					Verified = false
				};

				SetBaseBrokerRegistration(profile);
				profile.Password = Common.Encrypt(txtPassword.Text);

				db.BrokerRegistrations.InsertOnSubmit(profile);
				db.SubmitChanges();

				string body = string.Format(Sitecore.Context.Item["MailContent"], profile.CareMoreId, profile.FirstName, profile.LastName);
				BrokerHelper.SendEmailNotification(body);

				added = true;
			}

			return added;
		}

		public bool ChangeRegistration()
		{
			bool changed = false;

			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			IEnumerable<BrokerRegistration> profiles = db.BrokerRegistrations.Where(item =>
				item.RegistrationId.ToString() == hidRegistrationId.Value);

			if ((profiles != null) && (profiles.Count() > 0))
			{
				BrokerRegistration profile = profiles.SingleOrDefault();

				if (Common.Encrypt(txtVerifyPassword.Text).Equals(profile.Password))
				{
					SetBaseBrokerRegistration(profile);
					profile.RegistrationId = Guid.Parse(hidRegistrationId.Value);
					profile.UpdatedOn = DateTime.Now;
					profile.UpdatedBy = txtEmail.Text.TrimEnd();

					db.SubmitChanges();

					string body = string.Format(Sitecore.Context.Item["MailContent"], profile.CareMoreId,
						profile.FirstName, profile.LastName);

					BrokerHelper.SendEmailNotification(body);

					changed = true;
				}
			}

			return changed;
		}

		public void LoadEmptyRegistration()
		{
			phUpdateOnly.Visible = false;
		}

		public void LoadRegistration(BrokerRegistration profile)
		{
			hidRegistrationId.Value = profile.RegistrationId.ToString();
			txtCareMoreId.Text = profile.CareMoreId;
			txtLastName.Text = profile.LastName;
			txtFirstName.Text = profile.FirstName;
			txtMiddleInitial.Text = profile.MiddleInitial;
			rbLstSalutation.SelectedValue = profile.Salutation;
			txtHomePhone.Text = profile.Phone;
			txtAddress.Text = profile.Address;
			txtCity.Text = profile.City;
			ddlState.Text = profile.State;
			txtZip.Text = profile.Zip;
			txtEmail.Text = profile.Email;

			phRegisterOnly.Visible = false;
		}

		#endregion

		#region Private Methods

		private void SetBaseBrokerRegistration(BrokerRegistration profile)
		{
			profile.CareMoreId = txtCareMoreId.Text;
			profile.LastName = txtLastName.Text;
			profile.FirstName = txtFirstName.Text;
			profile.MiddleInitial = txtMiddleInitial.Text;
			profile.Salutation = rbLstSalutation.SelectedValue;

			string basePhone = txtHomePhone.Text.TrimEnd().Replace("-", string.Empty);
			if (basePhone.Length == 10)
				profile.Phone = string.Format("{0}-{1}-{2}", basePhone.Substring(0, 3), basePhone.Substring(3, 3), basePhone.Substring(6, 4));
			else
				profile.Phone = txtHomePhone.Text;

			profile.Address = txtAddress.Text;
			profile.City = txtCity.Text;
			profile.State = ddlState.Text;
			profile.Zip = txtZip.Text;
			profile.Email = txtEmail.Text.TrimEnd();
		}

		#endregion
	}
}
