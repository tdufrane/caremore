﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BrokerPasswordChange.ascx.cs" Inherits="Website.Sublayouts.Broker.BrokerPasswordChange" %>

<!-- Begin SubLayouts/Broker/BrokerPasswordChange -->
<div class="row">
	<div class="col-xs-12">
<asp:PlaceHolder ID="phConfirmation" runat="server" Visible="false">
		<div class="row">
			<div class="col-xs-12">
				<sc:Text ID="txtPasswordChanged" runat="server" Field="PasswordChanged" />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<sc:Link ID="lnkNavigateTo" runat="server" Field="NavigateTo" CssClass="btn btn-primary">OK</sc:Link>
			</div>
		</div>
</asp:PlaceHolder>

		<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
			<div class="form-group">
				<label for="txtOldPassword" class="col-xs-12 col-md-3 control-label">Old Password<span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:TextBox ID="txtOldPassword" runat="server" CssClass="form-control" MaxLength="20" TextMode="Password" ToolTip="Old Password" />
				</div>
				<div class="col-xs-4 col-md-5">
					<asp:RequiredFieldValidator ID="reqValOldPassword" runat="server" ControlToValidate="txtOldPassword"
						SetFocusOnError="true" ErrorMessage="Old Password is required." />
				</div>
			</div>
			<div class="form-group">
				<label for="txtPassword" class="col-xs-12 col-md-3 control-label">New Password<span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" MaxLength="20" TextMode="Password" ToolTip="Password" />
				</div>
				<div class="col-xs-4 col-md-5">
					<asp:RequiredFieldValidator ID="reqValPassword" runat="server" ControlToValidate="txtPassword"
						SetFocusOnError="true" ErrorMessage="Password is required." />
					<asp:RegularExpressionValidator ID="regValPassword" runat="server" ControlToValidate="txtPassword"
						SetFocusOnError="true" ErrorMessage="Password must be at least 6 characters."
						ValidationExpression=".{6,20}" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtPassword2" class="col-xs-12 col-md-3 control-label">Repeat Password<span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:TextBox ID="txtPassword2" runat="server" CssClass="form-control" MaxLength="20" TextMode="Password" ToolTip="Repeat Password" />
				</div>
				<div class="col-xs-4 col-md-5">
					<asp:RequiredFieldValidator ID="reqValRepeatPassword" runat="server" ControlToValidate="txtPassword2"
						SetFocusOnError="true" ErrorMessage="Repeat Password is required." />
					<asp:CompareValidator ID="comValPassword" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtPassword2"
						SetFocusOnError="true" ErrorMessage="Passwords do not match." />
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-4 col-md-offset-3">
					<asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_Click" Text="Modify" />
				</div>
				<div class="col-xs-2 col-md-pull-2 text-right">
					<sc:Link ID="lnkCancel" runat="server" CssClass="btn btn-danger" Field="NavigateTo">Cancel</sc:Link>
				</div>
			</div>
		</asp:Panel>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false" />
	</div>
</div>
<!-- End SubLayouts/Broker/BrokerPasswordChange -->
