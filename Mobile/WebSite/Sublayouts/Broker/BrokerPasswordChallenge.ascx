﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BrokerPasswordChallenge.ascx.cs" Inherits="Website.Sublayouts.Broker.BrokerPasswordChallenge" %>

<!-- Begin SubLayouts/Broker/BrokerPasswordChallenge -->
<div class="row">
	<div class="col-xs-12">
<asp:PlaceHolder ID="phConfirmation" runat="server" Visible="false">
		<div class="row">
			<div class="col-xs-12">
				<sc:Text ID="txtThankYouMessage" runat="server" Field="ThankYouMessage" />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<sc:Link runat="server" Field="NavigateTo" CssClass="btn btn-primary">OK</sc:Link>
			</div>
		</div>
</asp:PlaceHolder>

		<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnGenerate">
			<div class="form-group">
				<label for="txtEmail" class="col-xs-12 col-md-2 control-label">Email <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-5">
					<asp:TextBox ID="txtEmail" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="100" />
				</div>
				<div class="col-xs-4 col-md-6">
					<asp:RequiredFieldValidator ID="reqValEmail" runat="server" ControlToValidate="txtEmail"
						SetFocusOnError="true" ErrorMessage="Email is required." />
					<asp:RegularExpressionValidator id="regExValEmail" runat="server" ControlToValidate="txtEmail"
						ErrorMessage="Format for Email is incorrect."
						ValidationExpression="^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-4 col-md-offset-2">
					<asp:Button ID="btnGenerate" runat="server" CssClass="btn btn-primary" OnClick="BtnGenerate_Click" Text="Generate Password" />
				</div>
				<div class="col-xs-offset-2 col-xs-2 col-md-pull-3 text-right">
					<sc:Link ID="lnkCancel" runat="server" CssClass="btn btn-danger" Field="NavigateTo">Cancel</sc:Link>
				</div>
			</div>
		</asp:Panel>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false" />
	</div>
</div>
<!-- End SubLayouts/Broker/BrokerPasswordChallenge -->
