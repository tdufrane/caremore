﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="NewsletterListPage.ascx.cs" Inherits="Website.Sublayouts.NewsletterListPage" %>

<!-- Begin SubLayouts/NewsletterListPage -->
<div class="row">
	<div class="col-xs-12">

<asp:ListView ID="lvCurrentItems" runat="server">
	<LayoutTemplate>
		<div id="topNews">
			<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
		</div>
	</LayoutTemplate>
	<ItemTemplate>
		<h3><sc:Text ID="txtNewsTitle" runat="server" Field="Title" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></h3>
		<p><asp:HyperLink ID="hlNewsletter" runat="server" NavigateUrl="<%# GetLink(Container.DataItem) %>" Target="_blank">
			<span class="glyphicon glyphicon-chevron-right"></span>
			<sc:Text ID="txtDetailsLinkLabel" runat="server" Field="Newsletter Link Label" /></asp:HyperLink></p>
		<hr />
	</ItemTemplate>
</asp:ListView>

<asp:ListView ID="lvAllYears" runat="server" GroupItemCount="1" OnItemDataBound="LvAllYears_ItemDataBound">
	<LayoutTemplate>
		<div class="panel-group" id="newsAccordian" role="tablist" aria-multiselectable="true">
			<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
		</div>
	</LayoutTemplate>
	<ItemTemplate>
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="heading<%# Container.DataItemIndex %>">
				<a href="#collapse<%# Container.DataItemIndex %>" aria-controls="collapse<%# Container.DataItemIndex %>"
					aria-expanded="false" data-toggle="collapse" data-parent="#newsAaccordion" role="button">
					<div class="row">
						<div class="col-xs-6 col-md-9">
							<h3 class="panel-title"><%# ((Sitecore.Data.Items.Item)Container.DataItem).Name %></h3>
						</div>
						<div class="col-xs-6 col-md-2 col-md-offset-1 text-center"><span class="badge"><%# ((Sitecore.Data.Items.Item)Container.DataItem).Children.Count %></span></div>
					</div>
				</a>
			</div>
			<div id="collapse<%# Container.DataItemIndex %>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<%# Container.DataItemIndex %>">
				<asp:ListView ID="lvAllNewsletters" runat="server">
					<LayoutTemplate>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 bg-primary item-spacer"><sc:Text ID="txtTitleLabel" runat="server" Field="Title Label" /></div>
							</div>
							<asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
						</div>
					</LayoutTemplate>
					<ItemTemplate>
						<div class="row">
							<div class="col-xs-12">
								<p class="item-spacer"><asp:HyperLink ID="hlNewsletter" runat="server" NavigateUrl="<%# GetLink(Container.DataItem) %>" Target="_blank">
									<span class="glyphicon glyphicon-chevron-right"></span>
									<sc:Text ID="txtNewsTitle" runat="server" Field="Title" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></asp:HyperLink></p>
							</div>
						</div>
					</ItemTemplate>
				</asp:ListView>
			</div>
		</div>
	</ItemTemplate>
</asp:ListView>

	</div>
</div>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
<!-- End SubLayouts/NewsletterListPage -->
