﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Text;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace Website.Sublayouts
{
	public partial class ContactUsPage : BaseUserControl
	{
		#region Private variables

		private const string PostBackSessionName = "ContactUsPage";

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();

				if (IsPostBack)
				{
					phError.Controls.Clear();
					phError.Visible = false;
				}
				else
				{
					Session[PostBackSessionName] = null;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				if (Page.IsValid)
				{
					if (Session[PostBackSessionName] == null)
					{
						SendEmail();

						// Show thank you message
						phConfirmation.Visible = true;
						pnlForm.Visible = false;

						// Set session to prevent refresh posts
						Session[PostBackSessionName] = DateTime.Now;
					}
					else
					{
						phConfirmation.Visible = false;
						pnlForm.Visible = false;

						CareMoreUtilities.DisplayNoMultiSubmit(phError);
					}
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void ReqValEmail_OnServerValidate(object source, ServerValidateEventArgs arguments)
		{
			if (string.IsNullOrEmpty(txtEmail.Text))
			{
				arguments.IsValid = false;
			}
			else
			{
				arguments.IsValid = true;
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			reqValEmail.Text = CurrentItem["Email Required Label"];
			reqValEmail.ErrorMessage = CurrentItem["Email Required Label"];
			btnSubmit.Text = CurrentItem["Send Button Label"];

			LoadNumbers();
		}

		private void LoadNumbers()
		{
			ID contactNumberID = new Sitecore.Data.ID("{338AC110-191F-4F38-B321-9BB456963C5D}");

			List<Item> numbers = CurrentItem.Axes.GetDescendants()
				.Where(item => item.TemplateID.Equals(contactNumberID)).ToList();

			if (numbers == null || numbers.Count == 0)
			{
				rptCallText.Visible = false;
			}
			else
			{
				rptCallText.DataSource = numbers;
				rptCallText.DataBind();
			}
		}

		private void SendEmail()
		{
			StringBuilder body = new StringBuilder();

			if (txtFirstName.Text.Length > 0)
				body.AppendFormat("Sender First Name: {0}<br />\n", txtFirstName.Text);

			if (txtLastName.Text.Length > 0)
				body.AppendFormat("Sender Last Name: {0}<br />\n", txtLastName.Text);

			if (txtMemberId.Text.Length > 0)
				body.AppendFormat("Member ID: {0}<br />\n", txtMemberId.Text);

			if (txtPhone.Text.Length > 0)
				body.AppendFormat("Sender Phone Number: {0}<br />\n", txtPhone.Text);

			if (rblTimeToCall.SelectedItem != null)
				body.AppendFormat("Good Time to Contact: {0}<br />\n", rblTimeToCall.SelectedItem.Text);

			if (txtComments.Text.Length > 0)
				body.AppendFormat("Comments:<br/>{0}\n", txtComments.Text);

			MailMessage message = new MailMessage();
			message.To.Add(new MailAddress(CurrentItem["To"]));
			message.From = new MailAddress(txtEmail.Text);
			message.Subject = CurrentItem["Subject"];
			message.Body = body.ToString();
			message.IsBodyHtml = true;

			// Send email
			CareMoreUtilities.SendEmail(message);
		}

		#endregion
	}
}
