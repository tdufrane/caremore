﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Callouts.ascx.cs" Inherits="Website.Sublayouts.Controls.Callouts" %>
<%@ Register TagPrefix="cm" TagName="BaseWidget" Src="~/Sublayouts/Widgets/BaseWidget.ascx" %>

<asp:Repeater ID="rptItems" runat="server" OnItemDataBound="RptItems_OnItemDataBound">
	<ItemTemplate>
		<cm:BaseWidget ID="ucWidget" runat="server" />
	</ItemTemplate>
</asp:Repeater>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
