﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MemberLanding.ascx.cs" Inherits="Website.Sublayouts.MemberLanding" %>
<%@ Register Src="~/Controls/SideNav.ascx" TagPrefix="uc" TagName="SideNav" %>

<!-- Begin SubLayouts/MemberLanding -->
<div class="row">
	<div class="col-xs-12 col-md-9 col-md-push-3">
		<div class="row panel-hero">
			<div class="col-xs-5 panel-hero-image">
				<sc:Image ID="imgMain" runat="server" CssClass="img-responsive" Field="Image" />
			</div>
			<div class="col-xs-7 panel-hero-body">
				<sc:FieldRenderer ID="frCalloutTitle" runat="server" EnclosingTag="h2" FieldName="Callout Title" />
				<sc:Text ID="txtCalloutBody" runat="server" Field="Callout Body" />
			</div>
		</div>
		<div class="row panel-sub-hero">
			<div class="col-xs-12 col-md-6 sub-hero-left">
				<div class="panel-hero-title">
					<sc:FieldRenderer ID="frCalloutBodyLeft" runat="server" EnclosingTag="h3" FieldName="Callout Body Left" />
				</div>
				<div class="panel-hero-body">
					<cm:Link ID="lnkCalloutLeft" runat="server" AddSpanTag="true" Field="Callout Link Left" SpanCssClass="glyphicon glyphicon-chevron-right" />
				</div>
			</div>
			<div class="col-xs-12 col-md-6 sub-hero-right">
				<div class="panel-hero-title">
					<sc:FieldRenderer ID="frCalloutBodyRight" runat="server" EnclosingTag="h3" FieldName="Callout Body Right" />
				</div>
				<div class="panel-hero-body">
					<cm:Link ID="lnkCalloutRight" runat="server" AddSpanTag="true" Field="Callout Link Right" SpanCssClass="glyphicon glyphicon-chevron-right" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<sc:Text ID="txtContent" runat="server" Field="Content" />
			</div>
		</div>

		<sc:Placeholder ID="phCustomLayout" runat="server" key="custom-layout"/>
	</div>
	<div class="col-xs-12 col-md-3 col-md-pull-9">
		<uc:SideNav ID="ucSideNav" runat="server" />
	</div>
</div>
<!-- End SubLayouts/MemberLanding -->
