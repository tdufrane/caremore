﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProviderTrainingRegistrationPage.ascx.cs" Inherits="Website.Sublayouts.ProviderTrainingRegistrationPage" %>
<%@ Register src="CaptchaBox.ascx" tagname="CaptchaBox" tagprefix="uc1" %>

<!-- Begin SubLayouts/ProviderTrainingRegistrationPage -->
<div class="row">
	<div class="col-xs-12">
		<asp:Panel id="provRegister" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
			<div><sc:Text ID="txtRegister" runat="server" Field="Register Content" />
				<sc:Text ID="txtAlready" runat="server" Field="Already Registered Content" Visible="false" />
				<sc:Text ID="txtMoreInfo" runat="server" Field="More Register Info Content" Visible="false" /></div>

			<p><asp:LinkButton ID="lnkBtnAlreadyRegistered" runat="server" CssClass="btn btn-danger" OnClick="BtnAlreadyRegistered_Click" CausesValidation="false" /></p>

		<asp:PlaceHolder ID="phRegister1" runat="server">
			<div class="form-group">
				<label for="txtFirstName" class="col-xs-12 col-md-3 control-label">First Name <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" MaxLength="25" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValFirstName" runat="server"
						ControlToValidate="txtFirstName" ErrorMessage="First Name is required." />
				</div>
			</div>
			<div class="form-group">
				<label for="txtLastName" class="col-xs-12 col-md-3 control-label">Last Name <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" MaxLength="25" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValLastName" runat="server"
						ControlToValidate="txtLastName" ErrorMessage="Last Name is required." />
				</div>
			</div>
			<div class="form-group">
				<label for="ddlTitle" class="col-xs-12 col-md-3 control-label">Title<asp:PlaceHolder ID="phTitleReq" runat="server">*</asp:PlaceHolder></label>
				<div class="col-xs-12 col-md-4">
					<asp:DropDownList ID="ddlTitle" runat="server" CssClass="form-control" AppendDataBoundItems="true">
						<asp:ListItem Text="" Value=""/>
					</asp:DropDownList>
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValTitle" runat="server"
						ControlToValidate="ddlTitle" ErrorMessage="Title is required." InitialValue="" />
				</div>
			</div>
		</asp:PlaceHolder>
			<div class="form-group">
				<label for="txtEmail" class="col-xs-12 col-md-3 control-label">Email Address <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="100" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValEmail" runat="server"
						ControlToValidate="txtEmail" ErrorMessage="Email Address is required." />
					<asp:RegularExpressionValidator ID="regExValEmail" runat="server"
						ControlToValidate="txtEmail" ErrorMessage="Email Address format is not valid."
							ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
				</div>
			</div>
		<asp:PlaceHolder ID="phRegister2" runat="server">
			<div class="form-group">
				<label for="txtAddress" class="col-xs-12 col-md-3 control-label">Address <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" MaxLength="100" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValAddress" runat="server"
						ControlToValidate="txtAddress" ErrorMessage="Address is required." />
				</div>
			</div>
			<div class="form-group">
				<label for="txtCity" class="col-xs-12 col-md-3 control-label">City <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtCity" runat="server" CssClass="form-control" MaxLength="50" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValCity" runat="server"
						ControlToValidate="txtCity" ErrorMessage="City is required." />
				</div>
			</div>
			<div class="form-group">
				<label for="ddlState" class="col-xs-12 col-md-3 control-label">State <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:DropDownList ID="ddlState" runat="server" CssClass="form-control">
						<asp:ListItem Text="-select one-" Value="" />
						<asp:ListItem Text="Alabama" Value="Alabama" />
						<asp:ListItem Text="Alaska" Value="Alaska" />
						<asp:ListItem Text="Arizona" Value="Arizona" />
						<asp:ListItem Text="Arkansas" Value="Arkansas" />
						<asp:ListItem Text="California" Value="California" />
						<asp:ListItem Text="Colorado" Value="Colorado" />
						<asp:ListItem Text="Connecticut" Value="Connecticut" />
						<asp:ListItem Text="Delaware" Value="Delaware" />
						<asp:ListItem Text="Florida" Value="Florida" />
						<asp:ListItem Text="Georgia" Value="Georgia" />
						<asp:ListItem Text="Hawaii" Value="Hawaii" />
						<asp:ListItem Text="Idaho" Value="Idaho" />
						<asp:ListItem Text="Illinois" Value="Illinois" />
						<asp:ListItem Text="Indiana" Value="Indiana" />
						<asp:ListItem Text="Iowa" Value="Iowa" />
						<asp:ListItem Text="Kansas" Value="Kansas" />
						<asp:ListItem Text="Kentucky" Value="Kentucky" />
						<asp:ListItem Text="Louisiana" Value="Louisiana" />
						<asp:ListItem Text="Maine" Value="Maine" />
						<asp:ListItem Text="Maryland" Value="Maryland" />
						<asp:ListItem Text="Massachusetts" Value="Massachusetts" />
						<asp:ListItem Text="Michigan" Value="Michigan" />
						<asp:ListItem Text="Minnesota" Value="Minnesota" />
						<asp:ListItem Text="Mississippi" Value="Mississippi" />
						<asp:ListItem Text="Missouri" Value="Missouri" />
						<asp:ListItem Text="Montana" Value="Montana" />
						<asp:ListItem Text="Nebraska" Value="Nebraska" />
						<asp:ListItem Text="Nevada" Value="Nevada" />
						<asp:ListItem Text="New Hampshire" Value="New Hampshire" />
						<asp:ListItem Text="New Jersey" Value="New Jersey" />
						<asp:ListItem Text="New Mexico" Value="New Mexico" />
						<asp:ListItem Text="New York" Value="New York" />
						<asp:ListItem Text="North Carolina" Value="North Carolina" />
						<asp:ListItem Text="North Dakota" Value="North Dakota" />
						<asp:ListItem Text="Ohio" Value="Ohio" />
						<asp:ListItem Text="Oklahoma" Value="Oklahoma" />
						<asp:ListItem Text="Oregon" Value="Oregon" />
						<asp:ListItem Text="Pennsylvania" Value="Pennsylvania" />
						<asp:ListItem Text="Rhode Island" Value="Rhode Island" />
						<asp:ListItem Text="South Carolina" Value="South Carolina" />
						<asp:ListItem Text="South Dakota" Value="South Dakota" />
						<asp:ListItem Text="Tennessee" Value="Tennessee" />
						<asp:ListItem Text="Texas" Value="Texas" />
						<asp:ListItem Text="Utah" Value="Utah" />
						<asp:ListItem Text="Vermont" Value="Vermont" />
						<asp:ListItem Text="Virginia" Value="Virginia" />
						<asp:ListItem Text="Washington" Value="Washington" />
						<asp:ListItem Text="West Virginia" Value="West Virginia" />
						<asp:ListItem Text="Wisconsin" Value="Wisconsin" />
						<asp:ListItem Text="Wyoming" Value="Wyoming" />
					</asp:DropDownList>
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValState" runat="server"
						ControlToValidate="ddlState" ErrorMessage="State is required." InitialValue="" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtZipCode" class="col-xs-12 col-md-3 control-label">Zip Code <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control" MaxLength="5" placeholder="#####" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValZipCode" runat="server"
						ControlToValidate="txtZipCode" ErrorMessage="Zip Code is required." />
					<asp:RegularExpressionValidator ID="regExValZipCode" runat="server" 
						ControlToValidate="txtZipCode" ErrorMessage="Zip Code is not valid."
						ValidationExpression="^\d{5}$" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtPhone" class="col-xs-12 col-md-3 control-label">Phone <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" MaxLength="14" placeholder="###-###-####" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValPhone" runat="server"
						ControlToValidate="txtPhone" ErrorMessage="Phone is required." />
					<asp:RegularExpressionValidator ID="regExValPhone" runat="server"
						ControlToValidate="txtPhone" ErrorMessage="Phone is not valid. Make sure number is valid and use the format ###-###-#### or ##########."
						Text="*" ValidationExpression="^(\(?[2-9]\d{2}\)?)[- ]?([2-9]\d{2})[- ]?\d{4}$" />
				</div>
			</div>
		</asp:PlaceHolder>
		<asp:PlaceHolder ID="phRegister3" runat="server">
			<asp:Panel ID="pnlSpecialty" runat="server" CssClass="form-group">
				<label for="ddlSpecialty" class="col-xs-12 col-md-3 control-label">Specialty <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:DropDownList ID="ddlSpecialty" runat="server" AppendDataBoundItems="true" CssClass="form-control">
						<asp:ListItem Text="" Value=""/>
					</asp:DropDownList>
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValSpecialty" runat="server"
						ControlToValidate="ddlSpecialty" ErrorMessage="Specialty is required."
						InitialValue="" />
				</div>
			</asp:Panel>
			<asp:Panel ID="pnlJobTitle" runat="server" CssClass="form-group">
				<label for="txtJobTitle" class="col-xs-12 col-md-3 control-label">Job Title <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtJobTitle" runat="server" CssClass="form-control" MaxLength="50" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValJobTitle" runat="server"
						ControlToValidate="txtJobTitle" ErrorMessage="Job Title is required." />
				</div>
			</asp:Panel>
			<asp:Panel ID="pnlProviderName" runat="server" CssClass="form-group">
				<label for="txtProviderName" class="col-xs-12 col-md-3 control-label">Provider Name <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtProviderName" runat="server" CssClass="form-control" MaxLength="50" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValProviderName" runat="server"
						ControlToValidate="txtProviderName" ErrorMessage="Provider Name is required." />
				</div>
			</asp:Panel>
			<asp:Panel ID="pnlOrganization" runat="server" CssClass="form-group">
				<label for="txtOrganization" class="col-xs-12 col-md-3 control-label">Organization <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtOrganization" runat="server" CssClass="form-control" MaxLength="50" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValOrganization" runat="server"
						ControlToValidate="txtOrganization" ErrorMessage="Organization is required." />
				</div>
			</asp:Panel>
			<asp:Panel ID="pnlLicense" runat="server" CssClass="form-group">
				<label for="txtLicense" class="col-xs-12 col-md-3 control-label">Practitioner License Number <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtLicense" runat="server" CssClass="form-control" MaxLength="20" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValLicense" runat="server"
						ControlToValidate="txtLicense" ErrorMessage="Practitioner License Number is required." />
				</div>
			</asp:Panel>
			<asp:Panel ID="pnlNpi" runat="server" class="form-group">
				<label for="txtNpi" class="col-xs-12 col-md-3 control-label">National Provider Identification (NPI)<asp:PlaceHolder ID="phNpiReq" runat="server">*</asp:PlaceHolder></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtNpi" runat="server" CssClass="form-control" MaxLength="10" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValNpi" runat="server"
						ControlToValidate="txtNpi" ErrorMessage="National Provider Identification is required." />
				</div>
			</asp:Panel>
		</asp:PlaceHolder>
			<asp:Panel ID="pnlAcceptance" runat="server" CssClass="row">
				<div class="col-xs-12">
					<sc:Text ID="txtAcceptance" runat="server" CssClass="form-control" Field="Acceptance Content" />
				</div>
			</asp:Panel>
			<div class="form-group">
				<div class="col-xs-12 col-md-offset-3 col-md-4 text-xs-center">
					<asp:LinkButton ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_Click"><span>Register</span></asp:LinkButton>
					<asp:LinkButton ID="btnRegistered" runat="server" CssClass="btn btn-info" OnClick="BtnRegistered_Click" Visible="false"><span>Submit</span></asp:LinkButton>
				</div>
			</div>
		</asp:Panel>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End SubLayouts/ProviderTrainingRegistrationPage -->
