﻿using System;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Website.BL;

namespace Website.Sublayouts
{
	public partial class CountyServicesListControl : BaseUserControl
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();

				if (IsPostBack)
				{
					phError.Controls.Clear();
					phError.Visible = false;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSearch_Click(object sender, EventArgs e)
		{
			try
			{
				CheckIsCovered();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			reqValZip.Text = base.CurrentItem["Required"];
			regValZip.Text = base.CurrentItem["Invalid Format"];
			btnSearch.Text = base.CurrentItem["Search Button Label"];
		}

		private void CheckIsCovered()
		{
			bool isCovered = CoverageManager.IsZipCodeCovered(txtZipCode.Text);

			pnlSuccess.Visible = isCovered;
			pnlInvalid.Visible = !isCovered;

			if (isCovered)
				Session[CareMoreUtilities.LastZipEnteredKey] = txtZipCode.Text;
		}

		#endregion
	}
}
