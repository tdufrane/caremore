﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemDetailsPage.ascx.cs" Inherits="Website.Sublayouts.ItemDetailsPage" %>
<%@ Register Src="~/Controls/SideNav.ascx" TagPrefix="uc" TagName="SideNav" %>
<%@ Register TagPrefix="cm" TagName="GoogleMap" Src="~/Controls/GoogleMap.ascx" %>

<!-- Begin SubLayouts/ItemDetailsPage -->
<div class="row">
	<div class="col-xs-12 col-md-9 col-md-push-3">
		<h2><asp:Label ID="lblTitle" runat="server" /></h2>

		<div class="row">
			<div class="col-xs-6 col-md-4"><asp:LinkButton ID="BackToResultsLB" runat="server" OnClick="BackToResultsLB_Click"><span class="glyphicon glyphicon-chevron-left"></span> <sc:Text ID="lblBack" runat="server" Field="Back To Results" /></asp:LinkButton></div>
			<div class="col-xs-6 col-md-2"><a href="#" onclick="window.print();"><sc:Text ID="lblPrint" runat="server" Field="Print" /> <span class="glyphicon glyphicon-print"></span></a></div>
		</div>

		<hr />

		<asp:PlaceHolder ID="phDetails" runat="server">
			<div class="row">
				<div class="col-xs-12 col-md-4">
					<h4><sc:Text ID="lblContactInfo" runat="server" Field="Contact Info" /></h4>

					<div class="row">
						<div class="col-xs-4"><em><sc:Text ID="txtPhone" runat="server" Field="Phone" />:</em></div>
						<div class="col-xs-8"><asp:Label ID="lblPhone" runat="server" /></div>
					</div>

					<asp:Panel ID="pnlFax" runat="server" CssClass="row">
						<div class="col-xs-4"><em><sc:Text ID="txtFax" runat="server" Field="Fax" />:</em></div>
						<div class="col-xs-8"><asp:Label ID="lblFax" runat="server" /></div>
					</asp:Panel>

					<hr />

					<h4><sc:Text ID="lblLocation" runat="server" Field="Location" /></h4>

					<address>
						<asp:PlaceHolder ID="phLocation" runat="server" />
					</address>

					<p><asp:HyperLink ID="lnkDirections" CssClass="icon icoArrow link" runat="server" Target="_blank"><span class="glyphicon glyphicon-chevron-right"></span> <sc:Text ID="lblGetDirections" runat="server" Field="Get Directions" /></asp:HyperLink></p>
				</div>

				<div class="col-xs-12 col-md-8">
					<cm:GoogleMap ID="ucGoogleMap" runat="server" InitialZoom="13" />
				</div>
			</div>

			<asp:Panel ID="pnlServices" runat="server" CssClass="row">
				<div class="col-xs-12">
					<hr />

					<h4><sc:Text ID="lblServices" runat="server" Field="Services" /></h4>

					<ul>
						<asp:Repeater ID="rptServices" runat="server">
							<ItemTemplate>
								<li><%# Container.DataItem %></li>
							</ItemTemplate>
						</asp:Repeater>
					</ul>
				</div>
			</asp:Panel>

			<asp:Panel ID="pnlEffective" runat="server" CssClass="row" Visible="false">
				<div class="col-xs-4"><sc:Text ID="lblEffectiveDate" runat="server" Field="Effective Date" /></div>
				<div class="col-xs-8"><asp:Label ID="txtEffectiveDate" runat="server" Font-Bold="true" /></div>
			</asp:Panel>
		</asp:PlaceHolder>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
	<div class="col-xs-12 col-md-3 col-md-pull-9">
		<uc:SideNav id="ucSideNav" runat="server" />
	</div>
</div>
<!-- End SubLayouts/ItemDetailsPage -->
