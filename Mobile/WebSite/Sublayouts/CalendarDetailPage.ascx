﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="CalendarDetailPage.ascx.cs" Inherits="Website.Sublayouts.CalendarDetailPage" %>
<%@ Register TagPrefix="cm" TagName="GoogleMap" Src="~/Controls/GoogleMap.ascx" %>

<!-- Begin SubLayouts/CalendarDetailPage -->
<div class="row">
	<div class="col-xs-12 col-md-9 col-md-push-3">
		<asp:PlaceHolder ID="phEventHeader" runat="server">
			<h2><cm:Text ID="eventTitle" runat="server" Field="Title" /></h2>

			<div class="row">
				<div class="col-xs-6 col-md-4"><asp:HyperLink ID="CalendarHL" runat="server"><span class="glyphicon glyphicon-chevron-left"></span> BACK TO EVENTS LIST</asp:HyperLink></div>
				<div class="col-xs-6 col-md-2"><a href="#" onclick="window.print();" class="link"><cm:Text ID="eventPrint" Field="Print Button Text" runat="server" /></a></div>
			</div>

			<hr />
		</asp:PlaceHolder>

		<div class="eventDetail">
			<asp:Panel ID="RegConfirmHeader" runat="server" Visible="False" ClientIDMode="Static">
				<div style="float:left;"><sc:Image ID="imgLogo" Field="Image" runat="server" /></div>
				<div style="float:right; padding:10px;">
					<a href="#" class="link" onclick="javascript:window.print()">
						<cm:Text ID="eventPrint2" Field="Print Button Text" runat="server" /></a>
				</div>
			</asp:Panel>
			<div style="clear:both; float:none;"><cm:Text ID="RegConfirmText" Field="Confirmation Content" runat="server" Visible="False" /></div>

			<h2><cm:Text ID="txtTitle" Field="Title" runat="server" /></h2>

			<div class="row">
				<div class="col-xs-12 col-md-4">
					<div class="row">
						<div class="col-xs-4"><em><cm:Text ID="eventDay" Field="Day Text" runat="server" /></em></div>
						<div class="col-xs-8"><sc:Date ID="DayDt2" Field="EventDate" Format="dddd" runat="server" /></div>
					</div>
					<div class="row">
						<div class="col-xs-4"><em><cm:Text ID="eventDate" Field="Date Text" runat="server" /></em></div>
						<div class="col-xs-8"><sc:Date ID="DateDt2" Field="EventDate" Format="MMM d, yyyy" runat="server" /></div>
					</div>
					<div class="row">
						<div class="col-xs-4"><em><cm:Text ID="eventTime" Field="Time Text" runat="server" /></em></div>
						<div class="col-xs-8"><sc:Date ID="Time" Field="EventDate" Format="h:mm tt" runat="server" /></div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p><asp:Literal ID="litEventType" runat="server"></asp:Literal></p>
						</div>
					</div>

					<h4><cm:Text ID="eventLocation" Field="Location Text" runat="server" /></h4>

					<div class="row">
						<div class="col-xs-12">
							<address>
								<asp:PlaceHolder ID="AddressPH" runat="server" />
							</address>
						</div>
					</div>

					<p><asp:HyperLink ID="lnkDirections" CssClass="icon icoArrow link" runat="server" Target="_blank"><span class="glyphicon glyphicon-chevron-right"></span> <asp:Label ID="lblGetDirections" runat="server" /></asp:HyperLink></p>
				</div>
				<div class="col-xs-12 col-md-8">
					<cm:GoogleMap ID="googleMap" runat="server" InitialZoom="13" />
				</div>
			</div>
		</div>

		<cm:Text ID="DisclaimerText" runat="server" Field="Text" />

		<asp:PlaceHolder ID="phRegistration" runat="server">
			<asp:Panel ID="phRegConfirmFooter" runat="server" CssClass="row">
				<div class="col-xs-12">
					<cm:Text ID="txtRegConfirmFooter" runat="server" Field="Confirmation Footer" Visible="False" />
				</div>
			</asp:Panel>

			<hr />

			<div class="row">
				<div class="col-xs-12">
					<cm:Text ID="txtRegContent" runat="server" Field="Registration Content" />
				</div>
			</div>

			<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
				<div class="form-group">
					<label for="FirstName" class="col-xs-12 col-md-3 control-label"><cm:Text ID="RegFirstName" runat="server" Field="First Name Text" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="FirstName" runat="server" CssClass="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label for="LastName" class="col-xs-12 col-md-3 control-label"><cm:Text ID="RegLastName" Field="Last Name Text" runat="server" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="LastName" runat="server" CssClass="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label for="Address" class="col-xs-12 col-md-3 control-label"><cm:Text ID="RegAddress" Field="Address Text" runat="server" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="Address" runat="server" CssClass="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label for="City" class="col-xs-12 col-md-3 control-label"><cm:Text ID="RegCity" Field="City Text" runat="server" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="City" runat="server" CssClass="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label for="StateDDL" class="col-xs-12 col-md-3 control-label"><cm:Text ID="RegState" Field="State Text" runat="server" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:DropDownList ID="StateDDL" runat="server" AppendDataBoundItems="true" CssClass="form-control">
						</asp:DropDownList>
					</div>
				</div>
				<div class="form-group">
					<label for="ZipCode" class="col-xs-12 col-md-3 control-label"><cm:Text ID="RegZip" Field="Zip Code Text" runat="server" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="ZipCode" runat="server" CssClass="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label for="Phone" class="col-xs-12 col-md-3 control-label"><cm:Text ID="RegPhone" Field="Phone Text" runat="server" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="Phone" runat="server" CssClass="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label for="Email" class="col-xs-12 col-md-3 control-label"><cm:Text ID="RegEmail" Field="Email Text" runat="server" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="Email" runat="server" CssClass="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label for="BringGuestRBL" class="col-xs-12 col-md-3 control-label"><cm:Text ID="RegBringGuest" Field="Bring Guest Text" runat="server" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="BringGuestRBL" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Text="Yes" />
							<asp:ListItem Text="No" />
						</asp:RadioButtonList>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<cm:Text ID="txtRegFooter" runat="server" Field="Registration Footer" />
					</div>
				</div>
				<div class="col-xs-12 col-md-offset-3 col-md-4 top-spacer">
					<asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_Click" Text="SUBMIT" />
				</div>
			</asp:Panel>
			<!--END: Registration Form-->
		</asp:PlaceHolder>
		
		<!--START: Registration Confirm Popup-->
		<div style="display: none;">
			<asp:Panel CssClass="modalPopup" id="modalConfirmPopup" runat="server" ClientIDMode="Static" Visible="False">
			</asp:Panel>
			<p><a class="button popInfo" href="#modalConfirmPopup" id="ConfirmAnchor" >Confirm</a></p>
		</div>
		<!--END: Registration Confirm Popup-->
		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End SubLayouts/CalendarDetailPage -->
