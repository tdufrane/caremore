﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ProviderParticipation.ascx.cs" Inherits="Website.Sublayouts.Forms.ProviderParticipation" %>

<!-- Begin Sublayouts/Forms/ProviderParticipation -->
<div class="row">
	<div class="col-xs-12">
		<asp:PlaceHolder ID="phConfirmation" runat="server" Visible="false">
			<p><sc:Text ID="txtThankYouMessage" runat="server" Field="ThankYouMessage" /></p>
		</asp:PlaceHolder>

		<asp:PlaceHolder ID="phForm" runat="server">
			<script type="text/javascript">
				$(document).ready(function () {
					$("#<%= chkSameAsLocation.ClientID %>").change(function () {
						if ($(this).is(":checked")) {
							$("#<%= txtTaxAddress.ClientID %>").val($("#<%= txtAddress.ClientID %>").val());
							$("#<%= txtTaxCity.ClientID %>").val($("#<%= txtCity.ClientID %>").val());
							$("#<%= ddlTaxState.ClientID %>").val($("#<%= ddlState.ClientID %> option:selected").val());
							$.uniform.update("#<%= ddlTaxState.ClientID %>");
							$("#<%= txtTaxZipCode.ClientID %>").val($("#<%= txtZipCode.ClientID %>").val());
						}
					});
				});
	
				function OneDayHoursFilled(source, args) {
					var hoursFilled = false;
					var pairs = [
						{ open: "#<%= txtMondayOpen.ClientID %>", close: "#<%= txtMondayClose.ClientID %>" },
						{ open: "#<%= txtTuesdayOpen.ClientID %>", close: "#<%= txtTuesdayClose.ClientID %>" },
						{ open: "#<%= txtWednesdayOpen.ClientID %>", close: "#<%= txtWednesdayClose.ClientID %>" },
						{ open: "#<%= txtThursdayOpen.ClientID %>", close: "#<%= txtThursdayClose.ClientID %>" },
						{ open: "#<%= txtFridayOpen.ClientID %>", close: "#<%= txtFridayClose.ClientID %>" },
						{ open: "#<%= txtSaturdayOpen.ClientID %>", close: "#<%= txtSaturdayClose.ClientID %>" },
						{ open: "#<%= txtSundayOpen.ClientID %>", close: "#<%= txtSundayClose.ClientID %>" }
					];

					for (var i = 0; i < pairs.length; i++) {
						if (($(pairs[i].open).val() != "") && ($(pairs[i].close).val() != "")) {
							hoursFilled = true;
							break;
						}
					}

					args.IsValid = hoursFilled;
				}

				function PreferredCommunicationSet(source, args) {
					if (($("#<%= rdoPreferPhone.ClientID %>").is(":checked")) || ($("#<%= rdoPreferEmail.ClientID %>").is(":checked")))
						args.IsValid = true;
					else
						args.IsValid = false;
				}

				function PreferredPhoneSet(source, args) {
					if ($("#<%= rdoPreferPhone.ClientID %>").is(":checked")) {
						if ($("#<%= txtPreferredPhone.ClientID %>").val() == "")
							args.IsValid = false;
						else
							args.IsValid = true;
					}
					else {
						args.IsValid = true;
					}
				}

				function PreferredEmailSet(source, args) {
					if ($("#<%= rdoPreferEmail.ClientID %>").is(":checked")) {
						if ($("#<%= txtPreferredEmail.ClientID %>").val() == "")
							args.IsValid = false;
						else
							args.IsValid = true;
					}
					else {
						args.IsValid = true;
					}
				}
			</script>

			<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
				<p>Required fields are indicated with an asterisk (*)</p>

				<div class="form-group">
					<label for="txtLastName" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblLastName" runat="server" Text="Provider Last Name" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" MaxLength="50" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValLastName" runat="server" ControlToValidate="txtLastName"
							ErrorMessage="Provider Last Name is required." SetFocusOnError="true" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtFirstName" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblFirstName" runat="server" Text="Provider First Name" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" MaxLength="50" ToolTip="First Name" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValFirstName" runat="server" ControlToValidate="txtFirstName"
							ErrorMessage="Provider First Name is required." SetFocusOnError="true" />
					</div>
				</div>
				<div class="form-group">
					<label for="ddlTitle" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblTitle" runat="server" Text="Title/Degree" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:DropDownList ID="ddlTitle" runat="server" AppendDataBoundItems="true" CssClass="form-control">
							<asp:ListItem Text="" Value=""/>
						</asp:DropDownList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValTitle" runat="server" ControlToValidate="ddlTitle"
							ErrorMessage="Title is required." SetFocusOnError="true"
							InitialValue="" />
					</div>
				</div>
				<div class="form-group">
					<label for="ddlPrimarySpec" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblPrimarySpec" runat="server" Text="Primary Specialty" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:DropDownList ID="ddlPrimarySpec" runat="server" AppendDataBoundItems="true" CssClass="form-control">
							<asp:ListItem Text="-select one-" Value="" />
						</asp:DropDownList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValPrimarySpec" runat="server" ControlToValidate="ddlPrimarySpec"
							ErrorMessage="Primary Specialty is required." SetFocusOnError="true"
							InitialValue="" />
					</div>
				</div>
				<div class="form-group">
					<label for="ddlSecondarySpec" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblSecondarySpec" runat="server" Text="Secondary Specialty" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:DropDownList ID="ddlSecondarySpec" runat="server" AppendDataBoundItems="true" CssClass="form-control">
							<asp:ListItem Text="-select one-" Value="" />
						</asp:DropDownList>
					</div>
				</div>

				<hr />

				<div class="row">
					<div class="col-xs-12 text-info">
						<h4><asp:Label ID="lblPrimaryPracticeLocation" runat="server" Text="Primary Practice Location" /></h4>
					</div>
				</div>

				<div class="form-group">
					<label for="txtAddress" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblAddress" runat="server" Text="Address" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" MaxLength="100" ToolTip="Address" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValAddress" runat="server" ControlToValidate="txtAddress"
							ErrorMessage="Address is required." SetFocusOnError="true" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtCity" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblCity" runat="server" Text="City" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtCity" runat="server" CssClass="form-control" MaxLength="50" ToolTip="City" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValCity" runat="server" ControlToValidate="txtCity"
							ErrorMessage="City is required." SetFocusOnError="true" />
					</div>
				</div>
				<div class="form-group">
					<label for="ddlState" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblState" runat="server" Text="State" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:DropDownList ID="ddlState" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="DdlState_SelectedIndexChanged">
							<asp:ListItem Text="-select one-" Value="" />
						</asp:DropDownList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValState" runat="server" ControlToValidate="ddlState"
							ErrorMessage="State is required." SetFocusOnError="true"
							InitialValue="" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtZipCode" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblZipCode" runat="server" Text="Zip Code" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control" MaxLength="5" ToolTip="Zip Code" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValZipCode" runat="server" ControlToValidate="txtZipCode"
							ErrorMessage="Zip Code is required." SetFocusOnError="true" />
						<asp:RegularExpressionValidator ID="regValZipCode" runat="server" ControlToValidate="txtZipCode"
							ErrorMessage="Format for Zip Code is incorrect." SetFocusOnError="true"
							ValidationExpression="^[0-9]{5}$" />
					</div>
				</div>
				<div class="form-group">
					<label for="ddlCounty" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblCounty" runat="server" Text="County" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:DropDownList ID="ddlCounty" runat="server" CssClass="form-control">
							<asp:ListItem Text="-select state first-" Value="" />
						</asp:DropDownList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValCounty" runat="server" ControlToValidate="ddlCounty"
							ErrorMessage="County is required." SetFocusOnError="true"
							InitialValue="" />
					</div>
				</div>
				<div class="form-group">
					<label for="ddlLanguage" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblProviderLanguage" runat="server" Text="Provider Language(s)" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:ListBox ID="ddlProviderLanguage" runat="server" CssClass="form-control" SelectionMode="Multiple" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValProviderLanguage" runat="server" ControlToValidate="ddlProviderLanguage"
							ErrorMessage="Provider Language is required." SetFocusOnError="true"
							InitialValue="" />
					</div>
				</div>
				<div class="form-group">
					<label for="ddlStaffLanguage" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblStaffLanguage" runat="server" Text="Staff Language(s)" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:ListBox ID="ddlStaffLanguage" runat="server" CssClass="form-control" SelectionMode="Multiple" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValStaffLanguage" runat="server" ControlToValidate="ddlStaffLanguage"
							ErrorMessage="Staff Language is required." SetFocusOnError="true"
							InitialValue="" />
					</div>
				</div>

				<div class="row">
					<label for="txtMondayOpen" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblHours" runat="server" Text="Hours" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-6">
						<p>Leave hours blank for day(s) not open</p>
						<div><asp:CustomValidator ID="cusValHours" runat="server" ClientValidationFunction="OneDayHoursFilled" ControlToValidate="txtMondayOpen"
								ErrorMessage="At least one day's hours are required." SetFocusOnError="true" /></div>
						<table class="table table-condensed">
							<tr>
								<th class="text-info">Day</th>
								<th class="text-info">Open</th>
								<th class="text-info">Close</th>
							</tr>
							<tr>
								<td><asp:Label ID="lblMonday" runat="server" Text="Monday" /></td>
								<td>
									<div class="form-group">
										<div class="col-xs-12 col-md-5">
											<asp:TextBox ID="txtMondayOpen" runat="server" CssClass="form-control" MaxLength="2" />
										</div>
										<div class="col-xs-12 col-md-4 no-padding-top">
											<asp:RadioButtonList ID ="rblMondayOpen" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
												<asp:ListItem Text="am" Selected="True" Value="am" />
												<asp:ListItem Text="pm" Value="pm" />
											</asp:RadioButtonList>
										</div>
										<div class="col-xs-12">
											<asp:RegularExpressionValidator ID="regExpValMondayOpen" runat="server" ControlToValidate="txtMondayOpen"
												ErrorMessage="Monday open hour is incorrect." SetFocusOnError="true"
												ValidationExpression="^(([1-9])|([1][0-2])){1}$" />
										</div>
									</div>
								</td>
								<td>
									<div class="form-group">
										<div class="col-xs-12 col-md-5">
											<asp:TextBox ID="txtMondayClose" runat="server" CssClass="form-control" MaxLength="2" />
										</div>
										<div class="col-xs-12 col-md-4 no-padding-top">
											<asp:RadioButtonList ID ="rblMondayClose" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
												<asp:ListItem Text="am" Value="am" />
												<asp:ListItem Text="pm" Selected="True" Value="pm" />
											</asp:RadioButtonList>
										</div>
										<div class="col-xs-12">
											<asp:RegularExpressionValidator ID="regExpValMondayClose" runat="server" ControlToValidate="txtMondayClose"
												ErrorMessage="Monday close hour is incorrect." SetFocusOnError="true"
												ValidationExpression="^(([1-9])|([1][0-2])){1}$" />
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td><asp:Label ID="lblTuesday" runat="server" Text="Tuesday" /></td>
								<td>
									<div class="form-group">
										<div class="col-xs-12 col-md-5">
											<asp:TextBox ID="txtTuesdayOpen" runat="server" CssClass="form-control" MaxLength="2" />
										</div>
										<div class="col-xs-12 col-md-4 no-padding-top">
											<asp:RadioButtonList ID ="rblTuesdayOpen" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
												<asp:ListItem Text="am" Selected="True" Value="am" />
												<asp:ListItem Text="pm" Value="pm" />
											</asp:RadioButtonList>
										</div>
										<div class="col-xs-12">
											<asp:RegularExpressionValidator ID="regExpValTuesdayOpen" runat="server" ControlToValidate="txtTuesdayOpen"
												ErrorMessage="Tuesday open hour is incorrect." SetFocusOnError="true"
												ValidationExpression="^(([1-9])|([1][0-2])){1}$" />
										</div>
									</div>
								</td>
								<td>
									<div class="form-group">
										<div class="col-xs-12 col-md-5">
											<asp:TextBox ID="txtTuesdayClose" runat="server" CssClass="form-control" MaxLength="2" />
										</div>
										<div class="col-xs-12 col-md-4 no-padding-top">
											<asp:RadioButtonList ID ="rblTuesdayClose" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
												<asp:ListItem Text="am" Value="am" />
												<asp:ListItem Text="pm" Selected="True" Value="pm" />
											</asp:RadioButtonList>
										</div>
										<div class="col-xs-12">
											<asp:RegularExpressionValidator ID="regExpValTuesdayClose" runat="server" ControlToValidate="txtTuesdayClose"
												ErrorMessage="Tuesday close hour is incorrect." SetFocusOnError="true"
												ValidationExpression="^(([1-9])|([1][0-2])){1}$" />
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td><asp:Label ID="lblWednesday" runat="server" Text="Wednesday" /></td>
								<td>
									<div class="form-group">
										<div class="col-xs-12 col-md-5">
											<asp:TextBox ID="txtWednesdayOpen" runat="server" CssClass="form-control" MaxLength="2" />
										</div>
										<div class="col-xs-12 col-md-4 no-padding-top">
											<asp:RadioButtonList ID ="rblWednesdayOpen" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
												<asp:ListItem Text="am" Selected="True" Value="am" />
												<asp:ListItem Text="pm" Value="pm" />
											</asp:RadioButtonList>
										</div>
										<div class="col-xs-12">
											<asp:RegularExpressionValidator ID="regExpValWednesdayOpen" runat="server" ControlToValidate="txtWednesdayOpen"
												ErrorMessage="Wednesday open hour is incorrect." SetFocusOnError="true"
												ValidationExpression="^(([1-9])|([1][0-2])){1}$" />
										</div>
									</div>
								</td>
								<td>
									<div class="form-group">
										<div class="col-xs-12 col-md-5">
											<asp:TextBox ID="txtWednesdayClose" runat="server" CssClass="form-control" MaxLength="2" />
										</div>
										<div class="col-xs-12 col-md-4 no-padding-top">
											<asp:RadioButtonList ID ="rblWednesdayClose" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
												<asp:ListItem Text="am" Value="am" />
												<asp:ListItem Text="pm" Selected="True" Value="pm" />
											</asp:RadioButtonList>
										</div>
										<div class="col-xs-12">
											<asp:RegularExpressionValidator ID="regExpValWednesdayClose" runat="server" ControlToValidate="txtWednesdayClose"
												ErrorMessage="Wednesday close hour is incorrect." SetFocusOnError="true"
												ValidationExpression="^(([1-9])|([1][0-2])){1}$" />
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td><asp:Label ID="lblThursday" runat="server" Text="Thursday" /></td>
								<td>
									<div class="form-group">
										<div class="col-xs-12 col-md-5">
											<asp:TextBox ID="txtThursdayOpen" runat="server" CssClass="form-control" MaxLength="2" />
										</div>
										<div class="col-xs-12 col-md-4 no-padding-top">
											<asp:RadioButtonList ID ="rblThursdayOpen" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
												<asp:ListItem Text="am" Selected="True" Value="am" />
												<asp:ListItem Text="pm" Value="pm" />
											</asp:RadioButtonList>
										</div>
										<div class="col-xs-12">
											<asp:RegularExpressionValidator ID="regExpValThursdayOpen" runat="server" ControlToValidate="txtThursdayOpen"
												ErrorMessage="Thursday open hour is incorrect." SetFocusOnError="true"
												ValidationExpression="^(([1-9])|([1][0-2])){1}$" />
										</div>
									</div>
								</td>
								<td>
									<div class="form-group">
										<div class="col-xs-12 col-md-5">
											<asp:TextBox ID="txtThursdayClose" runat="server" CssClass="form-control" MaxLength="2" />
										</div>
										<div class="col-xs-12 col-md-4 no-padding-top">
											<asp:RadioButtonList ID ="rblThursdayClose" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
												<asp:ListItem Text="am" Value="am" />
												<asp:ListItem Text="pm" Selected="True" Value="pm" />
											</asp:RadioButtonList>
										</div>
										<div class="col-xs-12">
											<asp:RegularExpressionValidator ID="regExpValThursdayClose" runat="server" ControlToValidate="txtThursdayClose"
												ErrorMessage="Thursday close hour is incorrect." SetFocusOnError="true"
												ValidationExpression="^(([1-9])|([1][0-2])){1}$" />
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td><asp:Label ID="lblFriday" runat="server" Text="Friday" /></td>
								<td>
									<div class="form-group">
										<div class="col-xs-12 col-md-5">
											<asp:TextBox ID="txtFridayOpen" runat="server" CssClass="form-control" MaxLength="2" />
										</div>
										<div class="col-xs-12 col-md-4 no-padding-top">
											<asp:RadioButtonList ID ="rblFridayOpen" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
												<asp:ListItem Text="am" Selected="True" Value="am" />
												<asp:ListItem Text="pm" Value="pm" />
											</asp:RadioButtonList>
										</div>
										<div class="col-xs-12">
											<asp:RegularExpressionValidator ID="regExpValFridayOpen" runat="server" ControlToValidate="txtFridayOpen"
												ErrorMessage="Friday open hour is incorrect." SetFocusOnError="true"
												ValidationExpression="^(([1-9])|([1][0-2])){1}$" />
										</div>
									</div>
								</td>
								<td>
									<div class="form-group">
										<div class="col-xs-12 col-md-5">
											<asp:TextBox ID="txtFridayClose" runat="server" CssClass="form-control" MaxLength="2" />
										</div>
										<div class="col-xs-12 col-md-4 no-padding-top">
											<asp:RadioButtonList ID ="rblFridayClose" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
												<asp:ListItem Text="am" Value="am" />
												<asp:ListItem Text="pm" Selected="True" Value="pm" />
											</asp:RadioButtonList>
										</div>
										<div class="col-xs-12">
											<asp:RegularExpressionValidator ID="regExpValFridayClose" runat="server" ControlToValidate="txtFridayClose"
												ErrorMessage="Friday close hour is incorrect." SetFocusOnError="true"
												ValidationExpression="^(([1-9])|([1][0-2])){1}$" />
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td><asp:Label ID="lblSaturday" runat="server" Text="Saturday" /></td>
								<td>
									<div class="form-group">
										<div class="col-xs-12 col-md-5">
											<asp:TextBox ID="txtSaturdayOpen" runat="server" CssClass="form-control" MaxLength="2" />
										</div>
										<div class="col-xs-12 col-md-4 no-padding-top">
											<asp:RadioButtonList ID ="rblSaturdayOpen" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
												<asp:ListItem Text="am" Selected="True" Value="am" />
												<asp:ListItem Text="pm" Value="pm" />
											</asp:RadioButtonList>
										</div>
										<div class="col-xs-12">
											<asp:RegularExpressionValidator ID="regExpValSaturdayOpen" runat="server" ControlToValidate="txtSaturdayOpen"
												ErrorMessage="Saturday open hour is incorrect." SetFocusOnError="true"
												ValidationExpression="^(([1-9])|([1][0-2])){1}$" />
										</div>
									</div>
								</td>
								<td>
									<div class="form-group">
										<div class="col-xs-12 col-md-5">
											<asp:TextBox ID="txtSaturdayClose" runat="server" CssClass="form-control" MaxLength="2" />
										</div>
										<div class="col-xs-12 col-md-4 no-padding-top">
											<asp:RadioButtonList ID ="rblSaturdayClose" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
												<asp:ListItem Text="am" Value="am" />
												<asp:ListItem Text="pm" Selected="True" Value="pm" />
											</asp:RadioButtonList>
										</div>
										<div class="col-xs-12">
											<asp:RegularExpressionValidator ID="regExpValSaturdayClose" runat="server" ControlToValidate="txtSaturdayClose"
												ErrorMessage="Saturday close hour is incorrect." SetFocusOnError="true"
												ValidationExpression="^(([1-9])|([1][0-2])){1}$" />
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td><asp:Label ID="lblSunday" runat="server" Text="Sunday" /></td>
								<td>
									<div class="form-group">
										<div class="col-xs-12 col-md-5">
											<asp:TextBox ID="txtSundayOpen" runat="server" CssClass="form-control" MaxLength="2" />
										</div>
										<div class="col-xs-12 col-md-4 no-padding-top">
											<asp:RadioButtonList ID ="rblSundayOpen" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
												<asp:ListItem Text="am" Selected="True" Value="am" />
												<asp:ListItem Text="pm" Value="pm" />
											</asp:RadioButtonList>
										</div>
										<div class="col-xs-12">
											<asp:RegularExpressionValidator ID="regExpValSundayOpen" runat="server" ControlToValidate="txtSundayOpen"
												ErrorMessage="Sunday open hour is incorrect." SetFocusOnError="true"
												ValidationExpression="^(([1-9])|([1][0-2])){1}$" />
										</div>
									</div>
								</td>
								<td>
									<div class="form-group">
										<div class="col-xs-12 col-md-5">
											<asp:TextBox ID="txtSundayClose" runat="server" CssClass="form-control" MaxLength="2" />
										</div>
										<div class="col-xs-12 col-md-4 no-padding-top">
											<asp:RadioButtonList ID ="rblSundayClose" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
												<asp:ListItem Text="am" Value="am" />
												<asp:ListItem Text="pm" Selected="True" Value="pm" />
											</asp:RadioButtonList>
										</div>
										<div class="col-xs-12">
											<asp:RegularExpressionValidator ID="regExpValSundayClose" runat="server" ControlToValidate="txtSundayClose"
												ErrorMessage="Sunday close hour is incorrect." SetFocusOnError="true"
												ValidationExpression="^(([1-9])|([1][0-2])){1}$" />
										</div>
									</div>
								</td>
							</tr>
						</table>
					</div>
				</div>

				<hr class="no-margin-top" />

				<div class="form-group">
					<label for="rblProviderGender" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblProviderGender" runat="server" Text="Provider Gender" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblProviderGender" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Text="Male" Value="M" Selected="True" />
							<asp:ListItem Text="Female" Value="F" />
						</asp:RadioButtonList>
					</div>
				</div>

				<div class="form-group">
					<label for="rblBoardCertification" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblBoardCertification" runat="server" Text="Board Certification" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblBoardCertification" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Text="Yes" Value="Y" />
							<asp:ListItem Text="No" Value="N" Selected="True" />
						</asp:RadioButtonList>
					</div>
				</div>

				<div class="form-group">
					<label for="txtIndivMedicareNum" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblIndivMedicareNum" runat="server" Text="Individual Medicare Number" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtIndivMedicareNum" runat="server" CssClass="form-control" MaxLength="20" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValIndivMedicareNum" runat="server" ControlToValidate="txtIndivMedicareNum"
							ErrorMessage="Individual Medicare Number is required." SetFocusOnError="true" />
					</div>
				</div>

				<div class="form-group">
					<label for="txtMediNumber" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblMediNumber" runat="server" Text="Medicaid/Medi-Cal Number" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtMediNumber" runat="server" CssClass="form-control" MaxLength="20" />
					</div>
				</div>

				<div class="form-group">
					<label for="txtBirthDate" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblBirthDate" runat="server" Text="Date of Birth" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtBirthDate" runat="server" CssClass="form-control" MaxLength="10" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValBirthDate" runat="server" ControlToValidate="txtBirthDate"
							ErrorMessage="Date of Birth is required." SetFocusOnError="true" />
						<asp:CompareValidator ID="comValBirthDate" runat="server" ControlToValidate="txtBirthDate"
							ErrorMessage="Date of Birth is an incorrect date." SetFocusOnError="true"
							Operator="DataTypeCheck" Type="Date" />
					</div>
				</div>

				<div class="form-group">
					<label for="txtStateLicNumber" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblStateLicNumber" runat="server" Text="State License Number" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtStateLicNumber" runat="server" CssClass="form-control" MaxLength="20" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValStateLicNumber" runat="server" ControlToValidate="txtStateLicNumber"
							ErrorMessage="State License Number is required." SetFocusOnError="true" />
					</div>
				</div>

				<div class="form-group">
					<label for="txtIndivNpiNumber" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblIndivNpiNumber" runat="server" Text="Individual NPI Number" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtIndivNpiNumber" runat="server" CssClass="form-control" MaxLength="10" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValIndivNpiNumber" runat="server" ControlToValidate="txtIndivNpiNumber"
							ErrorMessage="Individual NPI Number is required." SetFocusOnError="true" />
					</div>
				</div>

				<div class="form-group">
					<label for="txtGroupNpiNumber" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblGroupNpiNumber" runat="server" Text="Group NPI Number" /><br /><span class="text-muted">(if applicable)</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtGroupNpiNumber" runat="server" CssClass="form-control" MaxLength="10" />
					</div>
				</div>

				<div class="form-group">
					<label for="txtHospitalAffiliations" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblHospitalAffiliations" runat="server" Text="Hospital Affiliations" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtHospitalAffiliations" runat="server" CssClass="form-control" MaxLength="500" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValHospitalAffiliations" runat="server" ControlToValidate="txtHospitalAffiliations"
							ErrorMessage="Hospital Affiliations is required." SetFocusOnError="true" />
					</div>
				</div>

				<div class="form-group">
					<label for="txtAscAffiliations" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblAscAffiliations" runat="server" Text="ASC Affiliations" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtAscAffiliations" runat="server" CssClass="form-control" MaxLength="500" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValAscAffiliations" runat="server" ControlToValidate="txtAscAffiliations"
							ErrorMessage="ASC Affiliations is required." SetFocusOnError="true" />
					</div>
				</div>

				<hr class="no-padding-top" />

				<div class="row">
					<div class="col-xs-12 col-md-4 text-info">
						<h4><asp:Label ID="lblTaxInformation" runat="server" Text="Tax Information" /></h4>
					</div>
					<div class="col-xs-12 col-md-4 checkbox">
						<label for="chkSameAsLocation"><asp:CheckBox ID="chkSameAsLocation" runat="server" /> Address same as location</label>
					</div>
				</div>

				<div class="form-group">
					<label for="txtTaxIdNumber" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblTaxIdNumber" runat="server" Text="Tax ID Number" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtTaxIdNumber" runat="server" CssClass="form-control" MaxLength="10" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValTaxIdNumber" runat="server" ControlToValidate="txtTaxIdNumber"
							ErrorMessage="Tax ID Number is required." SetFocusOnError="true" />
					</div>
				</div>

				<div class="form-group">
					<label for="txtLegalEntityName" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblLegalEntityName" runat="server" Text="Legal Entity Name" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtLegalEntityName" runat="server" CssClass="form-control" MaxLength="50" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValLegalEntityName" runat="server" ControlToValidate="txtLegalEntityName"
							ErrorMessage="Legal Entity Name is required." SetFocusOnError="true" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtTaxAddress" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblTaxAddress" runat="server" Text="Address" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtTaxAddress" runat="server" CssClass="form-control" MaxLength="100" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValTaxAddress" runat="server" ControlToValidate="txtTaxAddress"
							ErrorMessage="Tax Address is required." SetFocusOnError="true" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtTaxCity" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblTaxCity" runat="server" Text="City" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtTaxCity" runat="server" CssClass="form-control" MaxLength="50" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValTaxCity" runat="server" ControlToValidate="txtTaxCity"
							ErrorMessage="Tax City is required." SetFocusOnError="true" />
					</div>
				</div>
				<div class="form-group">
					<label for="ddlTaxState" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblTaxState" runat="server" Text="State" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:DropDownList ID="ddlTaxState" runat="server" CssClass="form-control">
							<asp:ListItem Text="-select one-" Value="" />
						</asp:DropDownList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqTaxValState" runat="server" ControlToValidate="ddlTaxState"
							ErrorMessage="Tax State is required." SetFocusOnError="true"
							InitialValue="" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtTaxZipCode" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblTaxZipCode" runat="server" Text="Zip Code" /> <span class="text-danger">*</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtTaxZipCode" runat="server" CssClass="form-control" MaxLength="5" ToolTip="Zip Code" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValTaxZipCode" runat="server" ControlToValidate="txtTaxZipCode"
							ErrorMessage="Tax Zip Code is required." SetFocusOnError="true" />
						<asp:RegularExpressionValidator ID="regValTaxZipCode" runat="server" ControlToValidate="txtTaxZipCode"
							ErrorMessage="Format for Tax Zip Code is incorrect." SetFocusOnError="true"
							ValidationExpression="^[0-9]{5}$" />
					</div>
				</div>

				<hr />

				<div class="form-group">
					<label for="txtBillingContactName" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblBillingContactName" runat="server" Text="Billing Department Contact Name" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtBillingContactName" runat="server" CssClass="form-control" MaxLength="50" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValBillingContactName" runat="server" ControlToValidate="txtBillingContactName"
							ErrorMessage="Billing Department Contact Name is required." SetFocusOnError="true" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtBillingContactPhone" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblBillingContactPhone" runat="server" Text="Billing Department Contact<br/>Phone Number" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtBillingContactPhone" runat="server" CssClass="form-control" MaxLength="14" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValBillingContactPhone" runat="server" ControlToValidate="txtBillingContactPhone"
							ErrorMessage="Billing Department Contact Phone Number is required." SetFocusOnError="true" />
						<asp:RegularExpressionValidator ID="regExValBillingContactPhone" runat="server" ControlToValidate="txtBillingContactPhone"
							ErrorMessage="Billing Department Contact Phone Number is invalid or incorrectly formatted." SetFocusOnError="true"
							ValidationExpression="^(\(?[2-9]\d{2}\)?)[- ]?([2-9]\d{2})[- ]?\d{4}$" />
					</div>
				</div>

				<hr />

				<div class="row">
					<div class="col-xs-12 text-info">
						<h4><asp:Label ID="lblOfficeCredential" runat="server" Text="Office Manager or Credentialing Contact" /></h4>
					</div>
				</div>

				<div class="form-group">
					<label for="txtOfficeCredentialContactName" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblOfficeCredentialContactName" runat="server" Text="Contact Name" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtOfficeCredentialContactName" runat="server" CssClass="form-control" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValOfficeCredentialContactName" runat="server" ControlToValidate="txtOfficeCredentialContactName"
							ErrorMessage="Office Manager or Credentialing Contact Name is required." SetFocusOnError="true" />
					</div>
				</div>

				<div class="row">
					<div class="col-xs-offset-1 col-xs-11 text-info">
						<asp:Label ID="lblPreferred" runat="server" Text="Preferred method of communication for credentialing requirement" />
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<asp:CustomValidator ID="cusValCommunication" runat="server" ClientValidationFunction="PreferredCommunicationSet"
							ControlToValidate="txtPreferredPhone" ErrorMessage="A preferred method of communication is required." SetFocusOnError="true" />
					</div>
				</div>

				<div class="form-group">
					<label for="txtPreferredPhone" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblPreferredPhone" runat="server" Text="Phone Number" /></label>
					<div class="col-xs-2 col-md-1 radio">
						<asp:RadioButton ID="rdoPreferPhone" runat="server" CssClass="no-margin-left" GroupName="Preferred" />
					</div>
					<div class="col-xs-10 col-md-3">
						<asp:TextBox ID="txtPreferredPhone" runat="server" CssClass="form-control" MaxLength="14" placeholder="###-###-####" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RegularExpressionValidator ID="regExValPreferredPhone" runat="server" ControlToValidate="txtPreferredPhone"
							ErrorMessage="Preferred Phone Number is invalid or incorrectly formatted." SetFocusOnError="true"
							ValidationExpression="^(\(?[2-9]\d{2}\)?)[- ]?([2-9]\d{2})[- ]?\d{4}$" />
						<asp:CustomValidator ID="cusValPreferredPhone" runat="server" ClientValidationFunction="PreferredPhoneSet"
							ErrorMessage="A Preferred Phone Number is required if selected." SetFocusOnError="true" />
					</div>
				</div>

				<div class="form-group">
					<label for="txtPreferredEmail" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblPreferredEmail" runat="server" Text="Email" /></label>
					<div class="col-xs-2 col-md-1 radio">
						<asp:RadioButton ID="rdoPreferEmail" runat="server" CssClass="no-margin-left" GroupName="Preferred" />
					</div>
					<div class="col-xs-10 col-md-3">
						<asp:TextBox ID="txtPreferredEmail" runat="server" CssClass="form-control" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RegularExpressionValidator  id="regExValPreferredEmail" runat="server" ControlToValidate="txtPreferredEmail"
							ErrorMessage="Preferred Phone Email is incorrect." SetFocusOnError="true"
							ValidationExpression="^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$" />
						<asp:CustomValidator ID="cusValPreferredEmail" runat="server" ClientValidationFunction="PreferredEmailSet"
							ErrorMessage="A preferred email is required if selected." SetFocusOnError="true" />
					</div>
				</div>

				<hr />

				<div class="row">
					<div class="col-xs-12 text-info">
						<h4><asp:Label ID="lblAccommodatePatients" runat="server" Text="Are you inclined to accommodate (not treat) patients with the following conditions/disabilities/needs?" /></h4>
					</div>
				</div>

				<div class="form-group">
					<label for="rblVisionImpaired" class="col-xs-12 col-md-4"><asp:Label ID="lblVisionImpaired" runat="server" Text="Blind/Visually Impaired" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblVisionImpaired" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Text="Yes" Value="Y" />
							<asp:ListItem Text="No" Value="N" Selected="True" />
						</asp:RadioButtonList>
					</div>
				</div>
				<div class="form-group">
					<label for="rblCoOccurringDisorders" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblCoOccurringDisorders" runat="server" Text="Co-Occurring Disorders (multiple conditions at one time)" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblCoOccurringDisorders" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Text="Yes" Value="Y" />
							<asp:ListItem Text="No" Value="N" Selected="True" />
						</asp:RadioButtonList>
					</div>
				</div>
				<div class="form-group">
					<label for="rblChronicIllness" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblChronicIllness" runat="server" Text="Chronic Illness (permanent or long lasting)" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblChronicIllness" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Text="Yes" Value="Y" />
							<asp:ListItem Text="No" Value="N" Selected="True" />
						</asp:RadioButtonList>
					</div>
				</div>
				<div class="form-group">
					<label for="rblHearing" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblHearing" runat="server" Text="Deaf/Hard of Hearing" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblHearing" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Text="Yes" Value="Y" />
							<asp:ListItem Text="No" Value="N" Selected="True" />
						</asp:RadioButtonList>
					</div>
				</div>
				<div class="form-group">
					<label for="rblHivAids" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblHivAids" runat="server" Text="HIV / AIDS" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblHivAids" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Text="Yes" Value="Y" />
							<asp:ListItem Text="No" Value="N" Selected="True" />
						</asp:RadioButtonList>
					</div>
				</div>
				<div class="form-group">
					<label for="rblMentalIllness" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblMentalIllness" runat="server" Text="Persons with Serious Mental Illness" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblMentalIllness" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Text="Yes" Value="Y" />
							<asp:ListItem Text="No" Value="N" Selected="True" />
						</asp:RadioButtonList>
					</div>
				</div>
				<div class="form-group">
					<label for="rblPhysicalDisabilities" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblPhysicalDisabilities" runat="server" Text="Physical Disabilities" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblPhysicalDisabilities" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Text="Yes" Value="Y" />
							<asp:ListItem Text="No" Value="N" Selected="True" />
						</asp:RadioButtonList>
					</div>
				</div>

				<hr />

				<div class="row">
					<div class="col-xs-12 text-info">
						<h4><asp:Label ID="lblCaqhRequired" runat="server" Font-Bold="true" Text="CareMore Requires all providers to use CAQH" /></h4>
					</div>
				</div>
				<div class="form-group">
					<label for="rblCaqhRegistered" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblCaqhRegistered" runat="server" Text="Are you registered with CAQH?" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblCaqhRegistered" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Text="Yes" Value="Y" />
							<asp:ListItem Text="No" Value="N" Selected="True" />
						</asp:RadioButtonList>
					</div>
				</div>
				<div class="form-group">
					<label for="txtCaqhNumber" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblCaqhNumber" runat="server" Text="If registered, provide CAQH Number" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtCaqhNumber" runat="server"  CssClass="form-control" />
					</div>
				</div>

				<hr />
				<div class="form-group">
					<label for="txtAdditionalOffices" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblAdditionalOffices" runat="server" Text="Please list any additional office locations" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtAdditionalOffices" runat="server" CssClass="form-control" Rows="6" TextMode="MultiLine" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtSpecialization" class="col-xs-12 col-md-4 control-label"><asp:Label ID="lblSpecialization" runat="server" Text="Please list if you specialize in a particular field" /><br /><span class="text-muted">(example: Back surgeon)</span></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtSpecialization" runat="server" CssClass="form-control" />
					</div>
				</div>

				<hr />

				<div class="form-group">
					<div class="col-md-offset-4 col-md-4">
						<asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_Click" Text="Submit" />
					</div>
				</div>
			</asp:Panel>
		</asp:PlaceHolder>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End Sublayouts/Forms/ProviderParticipation -->
