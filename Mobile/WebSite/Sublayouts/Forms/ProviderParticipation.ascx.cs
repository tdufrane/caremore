﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Website.Sublayouts;

namespace Website.Sublayouts.Forms
{
	public partial class ProviderParticipation : BaseUserControl
	{
		#region Private variables

		private const string PostBackSessionName = "ProviderParticipationPage";
		private const string NoEntry = "-";

		#endregion

		#region Public Methods

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				if (IsPostBack)
				{
					CareMoreUtilities.ClearControl(phError);
				}
				else
				{
					LoadForm();
					Session[PostBackSessionName] = null;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				if (Session[PostBackSessionName] == null)
				{
					SaveRequest();

					// Show thank you message
					phConfirmation.Visible = true;
					pnlForm.Visible = false;

					//Set session to prevent refresh posts
					Session[PostBackSessionName] = DateTime.Now;
				}
				else
				{
					CareMoreUtilities.DisplayNoMultiSubmit(phError);
					phConfirmation.Visible = false;
					phForm.Visible = false;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void DdlState_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetCounties(ddlCounty, ddlState.SelectedItem.Value);
		}

		#endregion

		#region Private Methods

		#region Form setup

		private void LoadForm()
		{
			SetTitles(ddlTitle);

			SetSpecialties(ddlPrimarySpec);
			CopyList(ddlPrimarySpec, ddlSecondarySpec);

			SetLanguages(ddlProviderLanguage);
			CopyList(ddlProviderLanguage, ddlStaffLanguage);

			SetStates(ddlState);
			CopyList(ddlState, ddlTaxState);
		}

		private void SetCounties(ListControl list, string stateName)
		{
			list.Items.Clear();

			if (string.IsNullOrEmpty(stateName))
			{
				list.Items.Add(new ListItem("-select state first-"));
			}
			else if (CurrentItem.Children.Count == 0)
			{
				list.Items.Add(new ListItem("-no states found-"));
			}
			else
			{
				var counties = CurrentItem.Children
					.Where(i => i["State"] == stateName)
					.OrderBy(i => i["County"]);

				if (counties == null)
				{
					list.Items.Add(new ListItem("-state not found-"));
				}
				else
				{
					list.Items.Add(new ListItem("-select one-"));

					foreach (Item county in counties)
					{
						list.Items.Add(new ListItem(county["County"], county["County"]));
					}
				}
			}
		}

		private void SetLanguages(ListControl list)
		{
			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
			IEnumerable<LanguageItem> langs;
			langs = from lang in dbContext.LanguageItems
					orderby lang.Description
					select lang;

			foreach (LanguageItem row in langs)
			{
				ListItem li = new ListItem() { Text = row.Description, Value = row.Code };
				list.Items.Add(li);
			}
		}

		private void SetSpecialties(ListControl list)
		{
			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
			IEnumerable<SpecialtyItem> specialties;
			specialties = from specialty in dbContext.SpecialtyItems
			              orderby specialty.Description
			              select specialty;

			foreach (SpecialtyItem row in specialties)
			{
				ListItem li = new ListItem() { Text = row.Description, Value = row.Code };
				list.Items.Add(li);
			}
		}

		private void SetStates(ListControl list)
		{
			if (CurrentItem.Children.Count == 0)
			{
				list.Items.Add(new ListItem("-no states found-"));
			}
			else
			{
				var counties = CurrentItem.Children.OrderBy(i => i["State"]);

				foreach (Item county in counties)
				{
					if (list.Items.FindByText(county["State"]) == null)
						list.Items.Add(new ListItem(county["State"], county["State"]));
				}
			}
		}

		private void SetTitles(ListControl list)
		{
			Item[] items = CurrentDb.SelectItems(CareMoreUtilities.WORDING_PATH + "/Medical Titles//*[@@templatename='Text']");

			if (items != null && items.Length > 0)
			{
				foreach (Item item in items)
				{
					list.Items.Add(new ListItem(item["Text"]));
				}
			}
		}

		private void CopyList(ListControl fromList, ListControl toList)
		{
			foreach (ListItem item in fromList.Items)
			{
				if (!string.IsNullOrEmpty(item.Value))
					toList.Items.Add(item);
			}
		}

		#endregion

		#region Form submit

		private void SaveRequest()
		{
			StringBuilder body = new StringBuilder();

			body.AppendLine(Common.MailHeader("Physician Participation Request"));

			body.AppendFormat(CurrentItem["MailContent"], DateTime.Now);

			body.AppendLine();
			body.AppendLine(Common.MailTableOpen);
			body.AppendLine(AddRow(lblLastName, txtLastName));
			body.AppendLine(AddRow(lblFirstName, txtFirstName));
			body.AppendLine(AddRow(lblTitle, ddlTitle));
			body.AppendLine(AddRow(lblPrimarySpec, ddlPrimarySpec));
			body.AppendLine(AddRow(lblSecondarySpec, ddlSecondarySpec));

			body.AppendLine(AddRow(lblPrimaryPracticeLocation));
			body.AppendLine(AddRow(lblAddress, txtAddress));
			body.AppendLine(AddRow(lblCity, txtCity));
			body.AppendLine(AddRow(lblState, ddlState));
			body.AppendLine(AddRow(lblZipCode, txtZipCode));
			body.AppendLine(AddRow(lblCounty, ddlCounty));
			body.AppendLine(AddRow(lblProviderLanguage, ddlProviderLanguage));
			body.AppendLine(AddRow(lblStaffLanguage, ddlStaffLanguage));

			body.AppendLine(AddRow(lblHours));
			body.AppendLine(AddHours(lblMonday, txtMondayOpen, rblMondayOpen, txtMondayClose, rblMondayClose));
			body.AppendLine(AddHours(lblTuesday, txtTuesdayOpen, rblTuesdayOpen, txtTuesdayClose, rblTuesdayClose));
			body.AppendLine(AddHours(lblWednesday, txtWednesdayOpen, rblWednesdayOpen, txtWednesdayClose, rblWednesdayClose));
			body.AppendLine(AddHours(lblThursday, txtThursdayOpen, rblThursdayOpen, txtThursdayClose, rblThursdayClose));
			body.AppendLine(AddHours(lblFriday, txtFridayOpen, rblFridayOpen, txtFridayClose, rblFridayClose));
			body.AppendLine(AddHours(lblSaturday, txtSaturdayOpen, rblSaturdayOpen, txtSaturdayClose, rblSaturdayClose));
			body.AppendLine(AddHours(lblSunday, txtSundayOpen, rblSundayOpen, txtSundayClose, rblSundayClose));

			body.AppendLine(AddRow("&nbsp;"));
			body.AppendLine(AddRow(lblProviderGender, rblProviderGender));
			body.AppendLine(AddRow(lblBoardCertification, rblBoardCertification));
			body.AppendLine(AddRow(lblIndivMedicareNum, txtIndivMedicareNum));
			body.AppendLine(AddRow(lblMediNumber, txtMediNumber));
			body.AppendLine(AddRow(lblBirthDate, txtBirthDate));
			body.AppendLine(AddRow(lblStateLicNumber, txtStateLicNumber));
			body.AppendLine(AddRow(lblIndivNpiNumber, txtIndivNpiNumber));
			body.AppendLine(AddRow(lblGroupNpiNumber, txtGroupNpiNumber));
			body.AppendLine(AddRow(lblHospitalAffiliations, txtHospitalAffiliations));
			body.AppendLine(AddRow(lblAscAffiliations, txtAscAffiliations));

			body.AppendLine(AddRow(lblTaxInformation));
			body.AppendLine(AddRow(lblTaxIdNumber, txtTaxIdNumber));
			body.AppendLine(AddRow(lblLegalEntityName, txtLegalEntityName));
			body.AppendLine(AddRow(lblTaxAddress, txtTaxAddress));
			body.AppendLine(AddRow(lblTaxCity, txtTaxCity));
			body.AppendLine(AddRow(lblTaxState, ddlTaxState));
			body.AppendLine(AddRow(lblTaxZipCode, txtTaxZipCode));

			body.AppendLine(AddRow(lblBillingContactName, txtBillingContactName));
			body.AppendLine(AddPhone(lblBillingContactPhone, txtBillingContactPhone));

			body.AppendLine(AddRow(lblOfficeCredential));
			body.AppendLine(AddRow(lblOfficeCredentialContactName, txtOfficeCredentialContactName));

			if (rdoPreferPhone.Checked)
			{
				body.AppendLine(AddPhone(lblPreferredPhone, txtPreferredPhone, " (preferred)"));
				body.AppendLine(AddRow(lblPreferredEmail, txtPreferredEmail));
			}
			else
			{
				body.AppendLine(AddPhone(lblPreferredPhone, txtPreferredPhone));
				body.AppendLine(AddRow(lblPreferredEmail, txtPreferredEmail, " (preferred)"));
			}

			body.AppendLine(AddRow(lblAccommodatePatients));
			body.AppendLine(AddRow(lblVisionImpaired, rblVisionImpaired));
			body.AppendLine(AddRow(lblCoOccurringDisorders, rblCoOccurringDisorders));
			body.AppendLine(AddRow(lblChronicIllness, rblChronicIllness));
			body.AppendLine(AddRow(lblHearing, rblHearing));
			body.AppendLine(AddRow(lblHivAids, rblHivAids));
			body.AppendLine(AddRow(lblMentalIllness, rblMentalIllness));
			body.AppendLine(AddRow(lblPhysicalDisabilities, rblPhysicalDisabilities));

			body.AppendLine(AddRow("&nbsp;"));
			body.AppendLine(AddRow(lblCaqhRegistered, rblCaqhRegistered));
			body.AppendLine(AddRow(lblCaqhNumber, txtCaqhNumber));
			body.AppendLine(AddRow(lblAdditionalOffices, txtAdditionalOffices));
			body.AppendLine(AddRow(lblSpecialization, txtSpecialization));
			body.AppendLine(Common.MailTableClose);

			body.AppendLine(Common.MailFooter());

			string toEmail = ToEmail(ddlCounty.SelectedItem.Text);

			MailMessage message = new MailMessage();
			message.From = new MailAddress(CurrentItem["From"]);
			message.To.Add(new MailAddress(toEmail));
			message.Subject = CurrentItem["Subject"];
			message.Body = body.ToString();
			message.IsBodyHtml = true;

			// Send email
			CareMoreUtilities.SendEmail(message);
		}

		private string AddHours(Label label, TextBox openHour, RadioButtonList openAmPm, TextBox closeHour, RadioButtonList closeAmPm)
		{
			if ((string.IsNullOrEmpty(openHour.Text)) || (string.IsNullOrEmpty(closeHour.Text)))
				return Common.MailTableRow(label.Text, NoEntry, NoEntry);
			else
				return Common.MailTableRow(label.Text, SetHoursRange(openHour, openAmPm, closeHour, closeAmPm));
		}

		private string AddPhone(Label label, TextBox phone)
		{
			return AddPhone(label, phone, string.Empty);
		}

		private string AddPhone(Label label, TextBox phone, string extraText)
		{
			if (string.IsNullOrEmpty(phone.Text))
				return Common.MailTableRow(label.Text, NoEntry);
			else
				return Common.MailTableRow(label.Text, CareMoreUtilities.FormattedPhone(phone.Text) + extraText);
		}

		private string AddRow(Label label)
		{
			return Common.MailTableRow(label.Text);
		}

		private string AddRow(string text)
		{
			return Common.MailTableRow(text);
		}

		private string AddRow(Label label, ListControl list)
		{
			StringBuilder selected = new StringBuilder();

			foreach (ListItem item in list.Items)
			{
				if (item.Selected)
				{
					selected.AppendFormat("{0}, ", item.Text);
				}
			}

			if (selected.Length == 0)
			{
				return Common.MailTableRow(label.Text, NoEntry);
			}
			else
			{
				selected.Length -= 2;
				return Common.MailTableRow(label.Text, selected.ToString());
			}
		}

		private string AddRow(Label label, TextBox textBox)
		{
			return AddRow(label, textBox, string.Empty);
		}

		private string AddRow(Label label, TextBox textBox, string extraText)
		{
			if (string.IsNullOrEmpty(textBox.Text))
				return Common.MailTableRow(label.Text, NoEntry);
			else
				return Common.MailTableRow(label.Text, textBox.Text + extraText);
		}

		private string SetHoursRange(TextBox openHour, RadioButtonList openAmPm, TextBox closeHour, RadioButtonList closeAmPm)
		{
			return string.Format("{0} {1} - {2} {3}", openHour.Text, openAmPm.SelectedItem.Text, closeHour.Text, closeAmPm.SelectedItem.Text);
		}

		private string ToEmail(string county)
		{
			Item countyItem = CurrentItem.Children.Where(i => i["County"] == county).FirstOrDefault();

			if (countyItem == null)
			{
				return CurrentItem["To"];
			}
			else
			{
				return countyItem["EmailTo"];
			}
		}

		#endregion

		#endregion
	}
}
