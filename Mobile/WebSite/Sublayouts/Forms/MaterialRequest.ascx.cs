﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Controls;

namespace Website.Sublayouts.Forms
{
	public partial class MaterialRequest : BaseUserControl
	{
		#region Variables

		private const string PostBackSessionName = "MaterialRequest";
		Item regionsItem = null;

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			try
			{
				base.Page.MaintainScrollPositionOnPostBack = false;
				base.OnLoad(e);

				regionsItem = CurrentDb.GetItem(CareMoreUtilities.REGIONS_PATH);

				if (IsPostBack)
				{
					phError.Controls.Clear();
					phError.Visible = false;
				}
				else
				{
					Session[PostBackSessionName] = null;
					LoadForm();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				if (Session[PostBackSessionName] == null)
				{
					if (SendRequest())
					{
						//Set session to prevent refresh posts
						Session[PostBackSessionName] = DateTime.Now;

						phHeader.Visible = false;
						phConfirmation.Visible = true;
						pnlForm.Visible = false;
					}
				}
				else
				{
					CareMoreUtilities.DisplayNoMultiSubmit(phError);
					phConfirmation.Visible = false;
					pnlForm.Visible = false;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void DdlStates_SelectedIndexChanged(object sender, EventArgs e)
		{
			ListItem currentCounty = ddlCounties.SelectedItem;
			ClearList(ddlCounties);

			if (ddlStates.SelectedValue != string.Empty)
			{
				LoadCounties(ddlStates.SelectedValue);
			}

			ResetCounty(currentCounty);
			DdlCounties_SelectedIndexChanged(sender, e);
		}

		protected void DdlCounties_SelectedIndexChanged(object sender, EventArgs e)
		{
			ListItem currentPlan = ddlPlans.SelectedItem;
			ClearList(ddlPlans);

			LoadPlans(ddlStates.SelectedValue, ddlCounties.SelectedValue);
			ResetPlan(currentPlan);
		}

		#endregion

		#region Private methods

		private void LoadForm()
		{
			Item requestItem = this.DataSource;
			txtHeader.Item = requestItem;

			string requiredText = requestItem["Required"];
			string oneRequiredText = requestItem["One Required"];

			SetLabelAndRequired(lblMemberTypes, reqValMemberTypes, requestItem["MemberTypeLabel"], requiredText);
			rblMemberTypes.Items.Add(new ListItem(requestItem["CurrentMember"]));
			rblMemberTypes.Items.Add(new ListItem(requestItem["NonMember"]));
			rblMemberTypes.Items.Add(new ListItem(requestItem["Caregiver"]));

			SetLabelAndRequired(lblLastName, reqValLastName, requestItem["LastName"], requiredText);
			SetLabelAndRequired(lblFirstName, reqValFirstName, requestItem["FirstName"], requiredText);
			lblMiddleInitial.Text = requestItem["MiddleInitial"];
			SetLabelAndRequired(lblAddress, reqValAddress, requestItem["Address"], requiredText);
			SetLabelAndRequired(lblCity, reqValCity, requestItem["City"], requiredText);

			SetLabelAndRequired(lblState, reqValState, requestItem["State"], requiredText);
			LoadStates();

			SetLabelAndRequired(lblZipCode, reqValZipCode, requestItem["ZipCode"], requiredText);
			lblPhone.Text = requestItem["Phone"];
			lblMemberId.Text = requestItem["MemberId"];

			SetLabelAndRequired(lblLanguageRequested, reqValLanguageRequested, requestItem["LanguageRequested"], oneRequiredText);
			if (CurrentLanguage == "es-MX")
			{
				cblLanguageRequested.Items.Add(new ListItem("Inglés"));
				cblLanguageRequested.Items.Add(new ListItem("Español"));
			}
			else
			{
				cblLanguageRequested.Items.Add(new ListItem("English"));
				cblLanguageRequested.Items.Add(new ListItem("Spanish"));
			}

			SetLabelAndRequired(lblChooseCounty, reqValCounties, requestItem["ChooseYourCounty"], requiredText);
			LoadCounties(CurrentState);

			SetLabelAndRequired(lblChoosePlan, reqValPlans, requestItem["ChooseYourPlan"], requiredText);
			LoadPlans(ddlStates.SelectedValue, null);

			SetLabelAndRequired(lblMaterialsRequested, reqValMaterialsRequested, requestItem["MaterialsRequested"], oneRequiredText);
			Item[] materialItems = requestItem.Axes.GetDescendants().Where(item => item.TemplateName == "Text").ToArray<Item>();
			foreach (Item materialItem in materialItems)
			{
				cblMaterialsRequested.Items.Add(new ListItem(materialItem["Text"]));
			}

			btnSubmit.Text = requestItem["Submit"];
		}

		private void SetLabelAndRequired(Label label, RequiredFieldValidator validator, string text, string requiredText)
		{
			label.Text = text;
			validator.ErrorMessage = requiredText;
		}

		private void SetLabelAndRequired(Label label, RequiredFieldValidatorForCheckBoxLists validator, string text, string requiredText)
		{
			label.Text = text;
			validator.ErrorMessage = requiredText;
		}

		private void LoadStates()
		{
			Item[] states = regionsItem.Children.Where(item => item.TemplateName == "Region State").ToArray<Item>();

			foreach (Item state in states)
			{
				if (state.Axes.GetDescendants().Where(item => ((item.TemplateName == "Year") && (item["Hidden"] != "1"))).Count() > 0)
				{
					ListItem item = new ListItem(state.Name, state.Name);

					if (CurrentState.Equals(state.Name))
						item.Selected = true;

					if (ddlStates.Items.IndexOf(item) == -1)
						ddlStates.Items.Add(item);
				}
			}

			ddlStates.Items.Insert(0, new ListItem(ItemIds.GetItemValue("ENROLL_CHOOSE_STATE_MESSAGE", "Text"), string.Empty));
		}

		private void LoadCounties(string state)
		{
			Item stateItem = regionsItem.Children.Where(item => ((item.TemplateName == "Region State") && (item.Name == state))).FirstOrDefault();

			if (stateItem != null)
			{
				Item[] counties = stateItem.Children.Where(item => item.TemplateName == "Region").ToArray<Item>();

				foreach (Item county in counties)
				{
					if (county.Children.Where(item => ((item.TemplateName == "Year") && (item["Hidden"] != "1"))).Count() > 0)
						ddlCounties.Items.Add(new ListItem(county["County"], county.Name));
				}

				ddlCounties.Items.Insert(0, new ListItem(ItemIds.GetItemValue("ENROLL_SELECT_COUNTY_MESSAGE", "Text"), string.Empty));
			}
		}

		private void LoadPlans(string state, string county)
		{
			List<Item> planTypes;

			Item[] plans = GetPlans(state, county);

			planTypes = new List<Item>();

			foreach (Item plan in plans)
			{
				LookupField planType = plan.Fields["Plan Type"];

				if (planType.TargetItem != null)
				{
					if (!planTypes.Any(item => item.ID.Guid.ToString() == planType.TargetItem.ID.Guid.ToString()))
						planTypes.Add(planType.TargetItem);
				}
			}

			if (planTypes.Count > 0)
			{
				foreach (Item plan in planTypes)
				{
					ddlPlans.Items.Add(new ListItem(plan.Name, plan.ID.Guid.ToString("B").ToUpper()));
				}
			}

			string choosePlan;

			if (string.IsNullOrEmpty(county))
			{
				choosePlan = ItemIds.GetItemValue("PLAN_DROPDOWN_WITH_NO_COUNTY_SELECTED_MESSAGE", "Text");
			}
			else
			{
				choosePlan = ItemIds.GetItemValue("ENROLL_SELECT_PLAN_MESSAGE", "Text");
			}

			ddlPlans.Items.Insert(0, new ListItem(choosePlan, string.Empty));
		}

		private void ClearList(DropDownList list)
		{
			if (list.Items.Count > 0)
				list.Items.Clear();
		}

		private Item[] GetPlans(string state, string county)
		{
			// Search path is: sitecore/content/Global/Regions/<State>/<County/<Year>/[@Plan Type='<Guid>']
			StringBuilder searchPath = new StringBuilder();

			searchPath.Append("./");
			if (string.IsNullOrEmpty(state))
			{
				searchPath.Append("*");
			}
			else
			{
				searchPath.Append(state);
			}

			searchPath.Append("/");
			if (string.IsNullOrEmpty(county))
			{
				searchPath.Append("*");
			}
			else
			{
				searchPath.Append(county);
			}

			searchPath.Append("/*/*[@@templatename='Plan']");

			return regionsItem.Axes.SelectItems(searchPath.ToString());
		}

		private void ResetCounty(ListItem lastCounty)
		{
			int countyIndex = ddlStates.Items.IndexOf(lastCounty);
			if (countyIndex > -1)
				ddlCounties.SelectedIndex = countyIndex;
		}

		private void ResetPlan(ListItem lastPlan)
		{
			int planIndex = ddlPlans.Items.IndexOf(lastPlan);
			if (planIndex > -1)
				ddlPlans.SelectedIndex = planIndex;
		}

		private bool SendRequest()
		{
			bool sent = false;

			Item requestItem = this.DataSource;

			string toEmail = requestItem["To"];
			string fromEmail = requestItem["From"];

			if ((!string.IsNullOrWhiteSpace(toEmail)) && (!string.IsNullOrWhiteSpace(fromEmail)))
			{
				MailMessage message = new MailMessage();

				message.From = new MailAddress(fromEmail);
				message.To.Add(toEmail.Replace(';', ',').Replace(" ", string.Empty));
				message.Subject = requestItem["Subject"];

				message.Body = string.Format(requestItem["MailContent"],
					rblMemberTypes.SelectedItem.Text, txtLastName.Text, txtFirstName.Text,
					txtMiddleInitial.Text, txtAddress.Text, txtCity.Text, ddlStates.SelectedItem.Text,
					txtZipCode.Text, txtPhone.Text, txtMemberId.Text,
					CheckedText(cblLanguageRequested), ddlCounties.SelectedItem.Text,
					ddlPlans.SelectedItem.Text, CheckedText(cblMaterialsRequested));

				message.IsBodyHtml = true;

				CareMoreUtilities.SendEmail(message);

				txtThankYouMessage.Item = requestItem;

				sent = true;
			}

			return sent;
		}

		private string CheckedText(CheckBoxList list)
		{
			StringBuilder text = new StringBuilder();

			foreach (ListItem item in list.Items)
			{
				if (item.Selected)
					text.AppendFormat("{0}, ", item.Text);
			}

			if (text.Length > 0)
				text.Length -= 2; // remove trailing ", "

			return text.ToString();
		}

		#endregion
	}
}
