﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="HCCForm.ascx.cs" Inherits="Website.Sublayouts.Forms.HCCForm" %>

<!-- Begin Sublayouts/Forms/HCCForm -->
<sc:Text ID="txtFormHeader" Field="Form Header" runat="server" Item="<%# FormItem %>" />

<div class="row">
	<div class="col-xs-12">
		<asp:PlaceHolder ID="phConfirmation" runat="server" Visible="false">
			<sc:Text ID="txtThankYouMessage" runat="server" Field="ThankYouMessage" Item="<%# FormItem %>" />
		</asp:PlaceHolder>

		<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
			<div class="form-group">
				<label for="txtFirstName" class="col-xs-12 col-md-2"><sc:Text ID="txtFirstNameLabel" runat="server" Field="First Name Label" Item="<%# FormItem %>" /></label>
				<div class="col-xs-8 col-md-6">
					<asp:TextBox ID="txtFirstName" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="50" TextMode="SingleLine" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtLastName" class="col-xs-12 col-md-2"><sc:Text ID="txtLastNameLabel" runat="server" Field="Last Name Label" Item="<%# FormItem %>" /></label>
				<div class="col-xs-8 col-md-6">
					<asp:TextBox ID="txtLastName" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="50" TextMode="SingleLine" />
				</div>
			</div>
			<div class="form-group">
				<label for="rblTimeToCall" class="col-xs-12 col-md-2"><sc:Text ID="txtTimeToCallLabel" runat="server" Field="Time To Call Label" Item="<%# FormItem %>" /></label>
				<div class="col-xs-8 col-md-6">
					<asp:RadioButtonList ID="rblTimeToCall" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
						<asp:ListItem Text="AM" Value="AM" />
						<asp:ListItem Text="PM" Value="PM" />
					</asp:RadioButtonList>
				</div>
			</div>
			<div class="form-group">
				<label for="txtEmail" class="col-xs-12 col-md-2"><sc:Text ID="txtEmailLabel" runat="server" Field="Email Label" Item="<%# FormItem %>" /> <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-6">
					<asp:TextBox ID="txtEmail" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="100" />
				</div>
				<div class="col-xs-4 col-md-4">
					<asp:RequiredFieldValidator id="reqValEmail" runat="server" ControlToValidate="txtEmail" Display="Static" OnServerValidate="ReqValEmail_OnServerValidate" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtPhone" class="col-xs-12 col-md-2"><sc:Text ID="txtPhoneNumberLabel" runat="server" Field="Phone Number Label" Item="<%# FormItem %>" /></label>
				<div class="col-xs-8 col-md-6">
					<asp:TextBox ID="txtPhone" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="20" TextMode="SingleLine" placeholder="###-###-####" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtComments" class="col-xs-12 col-md-2"><sc:Text ID="txtCommentLabel" runat="server" Field="Comment Label" Item="<%# FormItem %>" /></label>
				<div class="col-xs-12 col-md-10">
					<asp:TextBox ID="txtComments" runat="server" ClientIDMode="Static" CssClass="form-control" Rows="6" TextMode="MultiLine" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-offset-2 col-md-4">
					<asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_Click" />
				</div>
			</div>
		</asp:Panel>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End Sublayouts/Forms/HCCForm -->