﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Links;
using CareMore.Web.DotCom.Data;
using Website;


namespace Website.Sublayouts.Forms
{
	public partial class MaterialOptIn : BaseUserControl
	{
		#region Private variables

		private const string PostBackSessionName = "MaterialOptIn";

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();

				if (IsPostBack)
				{
					phError.Controls.Clear();
					phError.Visible = false;
				}
				else
				{
					Session[PostBackSessionName] = null;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				if (Session[PostBackSessionName] == null)
				{
					MemberIdValidationResult memberResult = ValidateMember();

					if (memberResult != null && memberResult.MaterialAvailable.HasValue && memberResult.MaterialAvailable.Value)
					{
						SaveForm(memberResult.ProductPlanId);
						SendEmail();

						phConfirmation.Visible = true;
						pnlForm.Visible = false;
					}
					else
					{
						ShowNotAvailable();
					}
				}
				else
				{
					phConfirmation.Visible = false;
					pnlForm.Visible = false;

					CareMoreUtilities.DisplayNoMultiSubmit(phError);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			SetRadioLabels(rblReceiveMaterials);
			SetRadioLabels(rblReceiveEmails);

			ValidationErrorMessages();

			btnSubmit.Text = CurrentItem["Submit Label"];
		}

		private void SetRadioLabels(RadioButtonList list)
		{
			list.Items[0].Text = CurrentItem["Yes Label"];
			list.Items[1].Text = CurrentItem["No Label"];
		}

		private MemberIdValidationResult ValidateMember()
		{
			// This determines if member ID exists and is in supported plan
			MemberSearchDataContext dbContext = new MemberSearchDataContext();
			return dbContext.CM_PT_CMC_MemberIdValidation(txtMemberId.Text).SingleOrDefault<MemberIdValidationResult>();
		}

		private void SaveForm(string productPlanId)
		{
			CaremoreApplicationDataContext dbContext = new CaremoreApplicationDataContext();

			MemberOptIn optIn = new MemberOptIn()
			{
				MemberOptInId = Guid.NewGuid(),
				FirstName = txtFirstName.Text,
				LastName = txtLastName.Text,
				Email = txtEmail.Text,
				ZipCode = txtZipCode.Text,
				MemberId = txtMemberId.Text,
				ProductPlanId = productPlanId,
				ReceiveMaterials = rblReceiveMaterials.SelectedValue == "Yes",
				ReceiveEmails = rblReceiveEmails.SelectedValue == "Yes",
				CreatedOn = DateTime.Now,
				IpAddress = Request.UserHostAddress
			};

			dbContext.MemberOptIns.InsertOnSubmit(optIn);
			dbContext.SubmitChanges();
		}

		private void SendEmail()
		{
			Item englishItem = GetEnglishItem(CurrentItem);

			MailMessage message = new MailMessage();
			message.To.Add(new MailAddress(englishItem["To"]));
			message.From = new MailAddress(englishItem["From"]);
			message.Subject = englishItem["Subject"];
			message.Body = string.Format(englishItem["MailContent"],
				txtFirstName.Text, txtLastName.Text, txtEmail.Text, txtZipCode.Text, txtMemberId.Text,
				rblReceiveMaterials.SelectedValue, rblReceiveEmails.SelectedValue);
			message.IsBodyHtml = true;

			CareMoreUtilities.SendEmail(message);
		}

		private void ShowNotAvailable()
		{
			CareMoreUtilities.DisplayError(phError, CurrentItem["NotFoundMessage"]);
		}

		private void ValidationErrorMessages()
		{
			reqValLastName.ErrorMessage = CurrentItem["Last Name Required"];
			reqValFirstName.ErrorMessage = CurrentItem["First Name Required"];
			reqValEmail.ErrorMessage = CurrentItem["Email Required"];
			regExValEmail.ErrorMessage = CurrentItem["Email Format"];
			reqValZipCode.ErrorMessage = CurrentItem["Zip Code Required"];
			regExValZipCode.ErrorMessage = CurrentItem["Zip Code Format"];
			reqValMemberId.ErrorMessage = CurrentItem["Member ID Required"];
			regExValMemberId.ErrorMessage = CurrentItem["Member ID Format"];
		}

		#endregion
	}
}
