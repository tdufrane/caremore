﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="CaptchaBox.ascx.cs" Inherits="Website.Sublayouts.CaptchaBox" %>

<!-- Begin SubLayouts/CatchaBox -->
<div class="row">
	<div class="col-xs-6 col-md-4">
		<asp:PlaceHolder ID="phImage" runat="server" />
	</div>
	<div class="col-xs-6 col-md-4">
		<asp:LinkButton ID="lbNewImage" runat="server" CausesValidation="false" CssClass="btn btn-default" Text="New Image" OnClick="NewImage_Click" />
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="tbCaptchaGuess" runat="server" CssClass="form-control" ToolTip="Enter the characters you see above" />
	</div>
	<div class="col-xs-12 col-md-offset-1 col-md-6">
		<asp:RequiredFieldValidator ID="reqCaptcha" runat="server" ControlToValidate="tbCaptchaGuess"
			SetFocusOnError="true" ErrorMessage="Captcha characters are required." />
		<asp:CustomValidator ID="cusCaptcha" runat="server" ControlToValidate="tbCaptchaGuess"
			SetFocusOnError="true" ErrorMessage="Entered captcha characters do not match image."
			OnServerValidate="CusCaptcha_ServerValidate" />
	</div>
</div>
<!-- End SubLayouts/CatchaBox -->
