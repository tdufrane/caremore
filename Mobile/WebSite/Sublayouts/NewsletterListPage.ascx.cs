﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace Website.Sublayouts
{
	public partial class NewsletterListPage : BaseUserControl
	{
		#region Private variables

		private ID FolderTemplateID = new ID("{9FD898A3-20EC-43DF-80D2-345A39FCB93C}");
		private ID ItemTemplateID = new ID("{5B265D98-9A99-4446-92CF-AB66870E6833}");

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				if (!Page.IsPostBack)
				{
					LoadPage();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void LvAllYears_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			try
			{
				LoadYearNewsletterItems(e.Item);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Protected methods

		protected string GetLink(object item)
		{
			Item newsletter = item as Item;

			if (newsletter == null || string.IsNullOrEmpty(newsletter["PDF File"]))
			{
				return "#";
			}
			else
			{
				FileField link = newsletter.Fields["PDF File"];
				return link.Src;
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			List<Item> allYears = base.CurrentItem.GetChildren()
				.Where(i => i.TemplateID == FolderTemplateID)
				.OrderByDescending(i => i.Name).ToList();

			if (allYears != null && allYears.Count > 0)
			{
				List<Item> itemList = allYears[0].GetChildren()
					.Where(i => i.TemplateID == ItemTemplateID).ToList();

				BindList(lvCurrentItems, itemList);

				if (allYears.Count == 1)
				{
					lvAllYears.Visible = false;
				}
				else
				{
					BindList(lvAllYears, allYears.GetRange(1, allYears.Count - 1));
				}
			}
		}

		private List<Item> GetNewsletterItems(Item parent)
		{
			return parent.Children
				.Where(i => i.TemplateID == ItemTemplateID).ToList();
		}

		private void LoadYearNewsletterItems(ListViewItem viewItem)
		{
			BindList((ListView)viewItem.FindControl("lvAllNewsletters"),
				GetNewsletterItems((Item)viewItem.DataItem));
		}

		#endregion
	}
}
