﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;
using CareMore.Web.DotCom;
using Website.BL;
using Website.BL.ListItem;
using Website.BL.Plans;

namespace Website.Sublayouts
{
	/// <summary>
	/// This is used to render all the plans available ('http://www.caremore.com/Plans/Plan.aspx') using a generic list control 'FilteredListControl.ascx'
	/// </summary>
	public partial class PlanPage : BaseUserControl
	{
		#region Constants

		private const string LastPlanFilterKey = "LastPlanFilter";
		private const string ZipSuccessKey = "ZipSuccessX";

		#endregion

		#region Properties

		public Item CountyItem
		{
			get
			{
				if (CurrentLocaleItem == null)
					return null;

				return CurrentLocaleItem.Children[ddlCountyList.SelectedValue];
			}
		}

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				Page.PreRenderComplete += new EventHandler(Page_PreRenderComplete);

				if (IsPostBack)
					CareMoreUtilities.ClearControl(phError);
				else
					LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void Page_PreRenderComplete(object sender, EventArgs e)
		{
			try
			{
				List<PlanItem> medicalPlans = GetPlans();

				if (medicalPlans == null || medicalPlans.Count == 0)
				{
					phNoPlansMessage.Visible = true;
					rptPlans.Visible = false;
				}
				else
				{
					phNoPlansMessage.Visible = false;
					rptPlans.Visible = true;

					rptPlans.DataSource = medicalPlans;
					rptPlans.DataBind();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnReset_OnClick(object sender, EventArgs e)
		{
			try
			{
				foreach (RepeaterItem childItem in rptQuestions.Items)
				{
					CheckBoxList checkQuestions = childItem.FindControl("checkQuestions") as CheckBoxList;
					RadioButtonList radioQuestions = childItem.FindControl("radioQuestions") as RadioButtonList;

					if (checkQuestions != null)
						checkQuestions.ClearSelection();

					if (radioQuestions != null)
						radioQuestions.ClearSelection();
				}

				Session.Remove(CareMoreUtilities.LastCountySelectedKey);
				Session.Remove(CareMoreUtilities.LastZipEnteredKey);
				Session.Remove(LastPlanFilterKey);
				Session.Remove(ZipSuccessKey);

				txtZip.Text = string.Empty;
				lblZipSuccess.Visible = false;
				lblError.Visible = false;
				ddlCountyList.SelectedIndex = 0;
				ddlCountyList.Enabled = true;
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RptQuestions_OnItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			try
			{
				CheckBoxList checkQuestions = e.Item.FindControl("checkQuestions") as CheckBoxList;
				RadioButtonList radioQuestions = e.Item.FindControl("radioQuestions") as RadioButtonList;
				PlanQuestionItem item = e.Item.DataItem as PlanQuestionItem;

				if (item.AnswerType == PlanQuestionItem.QuestionType.CHECKLIST)
				{
					checkQuestions.DataSource = item.Answers;
					checkQuestions.DataBind();
				}
				else if (item.AnswerType == PlanQuestionItem.QuestionType.RADIOLIST)
				{
					radioQuestions.DataSource = item.Answers;
					radioQuestions.DataBind();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void DdlCountyList_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				SaveLastCounty();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RptPlans_OnItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
				{
					PlanItem planItem = e.Item.DataItem as PlanItem;

					Text txtTitle = e.Item.FindControl("txtTitle") as Text;
					txtTitle.Item = planItem.Item;

					Text txtDescription = e.Item.FindControl("txtDescription") as Text;
					txtDescription.Item = planItem.PlanCountyItem;

					Text txtNoPlan = e.Item.FindControl("txtNoPlan") as Text;
					txtNoPlan.Visible = !planItem.HasApplication;
					txtNoPlan.Item = planItem.Item;

					HyperLink hlEnroll = e.Item.FindControl("hlEnroll") as HyperLink;
					hlEnroll.Visible = planItem.HasApplication;
					hlEnroll.NavigateUrl = planItem.EnrollUrl;
					hlEnroll.Text = CurrentItem["Enroll Button Text"];

					Repeater rptLinks = e.Item.FindControl("rptLinks") as Repeater;
					rptLinks.DataSource = planItem.Files;
					rptLinks.DataBind();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnZipSearch_Click(object sender, EventArgs e)
		{
			try
			{
				// Messages After Clicking Search Button
				System.Text.RegularExpressions.Regex zipreg = new System.Text.RegularExpressions.Regex(@"^([0-9]{5})$", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

				if (string.IsNullOrWhiteSpace(txtZip.Text))
				{
					lblError.Visible = true;
					lblError.Text = CurrentItem["Zip Empty"];
					lblZipSuccess.Visible = false;
					ddlCountyList.SelectedIndex = 0;
					ddlCountyList.Enabled = true;
				}
				else if (!zipreg.IsMatch(txtZip.Text))
				{
					lblError.Visible = true;
					lblError.Text = string.Format(CurrentItem["Zip Invalid"], txtZip.Text);
					lblZipSuccess.Visible = false;
					ddlCountyList.Enabled = true;
				}
				else if (CoverageManager.IsZipCodeCovered(txtZip.Text))
				{
					lblError.Visible = false;
					lblZipSuccess.Visible = true;
					lblZipSuccess.Text = string.Format(CurrentItem["Zip Success"], txtZip.Text);

					//remember entered selection
					Session[CareMoreUtilities.LastStateSelectedKey] = CoverageManager.ZipCodeState;
					Session[CareMoreUtilities.LastCountySelectedKey] = CoverageManager.ZipCodesCounty;
					Session["LastZipEntered"] = txtZip.Text;
					Session[LastPlanFilterKey] = GetPlanFiltersFromQueryString();

					if (CoverageManager.ZipCodeState != null && CoverageManager.ZipCodeState != CareMoreUtilities.GetCurrentLocale())
					{
						//User entered zip code not in current locale, redirect to correct stage page
						Session[ZipSuccessKey] = lblZipSuccess.Text;
						Session["LocalState"] = CoverageManager.ZipCodeState;

						RedirectToStatePage();
					}

					string currentState = CareMoreUtilities.GetCurrentLocale();
					var query = string.Format("fast:/sitecore/content/Home/Locate Services/Coverage Search/" + currentState + "/*[@Zip Codes = '%" + txtZip.Text + "%']");
					var item = Sitecore.Context.Database.SelectSingleItem(query);

					string county = item != null ? item["County"] : "";

					Item[] counties = Sitecore.Context.Database.SelectItems(CareMoreUtilities.LOCALIZATION_PATH + "/" + currentState + "//*[@@templatename='County']");
					foreach (var value in counties)
					{
						if (county.Contains(value.Name))
						{
							ddlCountyList.SelectedValue = value.Name;
							ddlCountyList.Enabled = false;
						}
					}
				}
				else
				{
					lblError.Visible = true;
					lblError.Text = string.Format(CurrentItem["Zip Search Error"], txtZip.Text);
					lblZipSuccess.Visible = false;
					ddlCountyList.Enabled = true;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			if (Session[ZipSuccessKey] != null)
			{
				lblError.Visible = false;
				lblZipSuccess.Visible = true;
				lblZipSuccess.Text = Session[ZipSuccessKey].ToString();
				Session.Remove(ZipSuccessKey);
			}

			// Populate county list and restore last selection
			ddlCountyList.Items.Clear();
			FillCountyList();
			SetSelectedCounty();

			SaveLastCounty();

			// Pre-populate zip field with last entry
			SetEnteredZip();

			// Find Plan Questions
			rptQuestions.DataSource = GetQuestions();
			rptQuestions.DataBind();
			ContractTitleTag.Visible = ContractTitle.Item.Fields[ContractTitle.Field].HasValue && !string.IsNullOrWhiteSpace(ContractTitle.Item.Fields[ContractTitle.Field].Value);

			if (Session[LastPlanFilterKey] != null)
			{
				// Preset checkbox and radio button from last entry
				SetPlanFilter();
			}
				
		}

		private List<PlanQuestionItem> GetQuestions()
		{
			List<PlanQuestionItem> list = new List<PlanQuestionItem>();
			Item questionParentItem = CareMoreUtilities.GetContentItem("Questions");

			if (questionParentItem == null)
				return list;

			foreach (Item childItem in questionParentItem.Children)
			{
				PlanQuestionItem questionItem = new PlanQuestionItem(childItem);

				questionItem.QuestionText = childItem["QuestionText"];
				questionItem.AnswerType = GetAnswerType(childItem);

				foreach(Item answerChildItem in childItem.Children)
				{
					PlanAnswerItem answerItem = new PlanAnswerItem();

					answerItem.Text = answerChildItem["Text"];
					answerItem.Value = answerChildItem.ID.ToString();

					questionItem.Answers.Add(answerItem); 
				}

				list.Add(questionItem);
			}

			return list.Distinct().ToList();
		}

		private PlanQuestionItem.QuestionType GetAnswerType(Item item)
		{
			LookupField answerType = item.Fields["AnswerType"];

			if (answerType != null && answerType.TargetID == ItemIds.GetID("RADIO_TYPE_ID"))
			{
				return PlanQuestionItem.QuestionType.RADIOLIST;
			}

			return PlanQuestionItem.QuestionType.CHECKLIST;
		}

		private List<ID> GetPlanFiltersFromQueryString()
		{
			List<ID> ids = new List<ID>();

			foreach(RepeaterItem childItem in rptQuestions.Items)
			{
				CheckBoxList checkQuestions = childItem.FindControl("checkQuestions") as CheckBoxList;
				RadioButtonList radioQuestions = childItem.FindControl("radioQuestions") as RadioButtonList;

				ids.AddRange(checkQuestions.Items.Cast<ListItem>().Where(p => p.Selected).Select(p => new ID(p.Value)));
				ids.AddRange(radioQuestions.Items.Cast<ListItem>().Where(p => p.Selected).Select(p => new ID(p.Value)));
			}
			Session[LastPlanFilterKey] = ids;

			return ids;
		}

		private List<PlanItem> GetPlans()
		{
			List<PlanItem> plans = new List<PlanItem>();
			List<ID> planTypeIds = GetPlanFiltersFromQueryString();

			string state = CareMoreUtilities.GetCurrentLocale();
			string county = ddlCountyList.SelectedValue;
			Item selectedLocation = CurrentDb
				.SelectItems(CareMoreUtilities.REGIONS_PATH + "/" + state + "//*")
				.Where(item => item["State"] == state && GetComparisonValueCounty(item["County"]).Equals(county, StringComparison.OrdinalIgnoreCase))
				.FirstOrDefault();

			// List of all plan types to filter by
			List<ID> filterPlanIds = new List<ID>();
			List<ID> removePlanIds = new List<ID>();

			foreach (ID id in planTypeIds)
			{
				string selectPlanTypes = this.CurrentDb.Items.GetItem(id)["Select Plan Types"];
				string removePlanTypes = this.CurrentDb.Items.GetItem(id)["Hide Plan Types"];

				if (!string.IsNullOrEmpty(removePlanTypes))
				{
					removePlanIds.AddRange(removePlanTypes.Split('|').ToList().ConvertAll(x => new ID(x)));
				}


				if (!string.IsNullOrEmpty(selectPlanTypes))
				{
					filterPlanIds.AddRange(selectPlanTypes.Split('|').ToList().ConvertAll(x => new ID(x)));
				}
			}

			List<ID> final = new List<ID>();
			foreach (ID x in filterPlanIds)
			{
				if (!final.Contains(x))
				{
					if (!removePlanIds.Contains(x))
					{
						final.Add(x);
					}
				}
			}

			List<Item> itemList = new List<Item>();

			if (selectedLocation != null)
			{
				foreach (Item year in selectedLocation.Children)
				{
					foreach (Item plan in year.Children.Where(p => !IsFilteredPlan(p, final, removePlanIds)))
					{
						itemList.Add(plan);
					}
				}
			}

			if (itemList == null)
				return null;

			foreach (Item plan in itemList)
			{
				LookupField planType = plan.Fields["Plan Type"];

				if (planType == null || planType.TargetItem == null)
					continue;

				PlanItem planItem = new PlanItem(planType.TargetItem);
				Item countyItem = GetCountyItem();

				planItem.PlanCountyItem = PlanManager.GetPlanCountyItem(planType.TargetItem, ddlCountyList.SelectedValue);
				planItem.Files = PlanManager.GetPlanFiles(plan, countyItem);
				planItem.EnrollUrl = string.Format("{0}?countyID={1}&planTypeID={2}", ItemIds.GetUrl("ENROLL_ID"), countyItem.ID, plan.ID);
				planItem.HasApplication = !string.IsNullOrEmpty(plan["Application"]);
				planItem.Visible = planType.TargetItem["Visible"] == "1";

				if (planItem.Visible)
				{
					plans.Add(planItem);
				}
			}

			return plans.ToList();
		}

		private bool IsFilteredPlan(Item planItem, List<ID> filterPlanIds, List<ID> removePlanIds)
		{
			CheckboxField planYearHiddenField = planItem.Parent.Fields["Hidden"];
			LookupField selectedPlanTypeField = planItem.Fields["Plan Type"];

			if (planYearHiddenField != null && planYearHiddenField.Checked)
				return true;

			if (filterPlanIds.Count == 0)
			{
				if (removePlanIds.Any(id => id == new ID(planItem["Plan Type"])))
				{
					return true;
				}
				return false;
			}

			return filterPlanIds.Any(id => id == new ID(planItem["Plan Type"])) == false;
		}

		private string SetSelectedCounty()
		{
			string countySelected = null;
			if (ddlCountyList.Items.Count > 0)
			{
				if (Session[CareMoreUtilities.LastCountySelectedKey] != null)
				{
					countySelected = Session[CareMoreUtilities.LastCountySelectedKey] as string;
					if (ddlCountyList.Items.FindByValue(countySelected) != null)
					{
						ddlCountyList.Items.FindByValue(countySelected).Selected = true;
					}
				}
				else
				{
					countySelected = ddlCountyList.Items[0].Value;
					ddlCountyList.Items[0].Selected = true;
				}
			}
			return countySelected;
		}

		private void SetEnteredZip()
		{
			if (Session["LastZipEntered"] != null)
			{
				string zip = Session["LastZipEntered"] as string;
				if (!string.IsNullOrEmpty(zip))
				{
					txtZip.Text = zip;
					ddlCountyList.Enabled = false;
				}
			}
		}

		private void SetPlanFilter()
		{
			List<ID> filters = Session[LastPlanFilterKey] as List<ID>;

			foreach (ID i in filters)
			{
				foreach (RepeaterItem childItem in rptQuestions.Items)
				{
					CheckBoxList checkQuestions = childItem.FindControl("checkQuestions") as CheckBoxList;
					RadioButtonList radioQuestions = childItem.FindControl("radioQuestions") as RadioButtonList;

					List<ListItem> checkbox = checkQuestions.Items.Cast<ListItem>().Where(p => new ID(p.Value) == i).ToList();
					List<ListItem> radiobox = radioQuestions.Items.Cast<ListItem>().Where(p => new ID(p.Value) == i).ToList();

					if ((checkbox.Count > 0 && checkbox[0] != null) ||
						(radiobox.Count > 0 && radiobox[0] != null))
					{
						if (checkbox.Count > 0)
						{
							checkbox[0].Selected = true;
						}
						else if (radiobox.Count > 0)
						{
							radiobox[0].Selected = true;
						}
						break;
					}
				}
			}
			
		}

		private void SaveLastCounty()
		{
			Session[CareMoreUtilities.LastCountySelectedKey] = ddlCountyList.SelectedValue;
		}

		private void FillCountyList()
		{
			ddlCountyList.Items.Clear();

			Item[] counties = Sitecore.Context.Database.SelectItems(CareMoreUtilities.LOCALIZATION_PATH + "/" + base.CurrentState + "//*[@@templatename='County']");
			if (counties.Count() > 0)
			{
				ddlCountyList.DataSource = counties;
				ddlCountyList.DataTextField = "Name";
				ddlCountyList.DataValueField = "Name";
				ddlCountyList.DataBind();
			}
		}

		private Item GetCountyItem()
		{
			Item selectedLocation = CurrentDb.SelectItems(CareMoreUtilities.REGIONS_PATH + "/" + base.CurrentState + "//*").Where(item =>
				item["State"] == base.CurrentState && GetComparisonValueCounty(item["County"]).Equals(ddlCountyList.SelectedValue, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

			return selectedLocation;
		}

		private void RedirectToStatePage()
		{
			Item localizedItem = CareMoreUtilities.GetLocalizedItem(CurrentItem);

			if (localizedItem != null && localizedItem.ID != Sitecore.Context.Item.ID)
			{
				Response.Redirect(LinkManager.GetItemUrl(localizedItem), false);
			}
			else
			{
				Response.Redirect(Request.RawUrl, false);
			}
		}

		#endregion
	}
}
