﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using CareMore.Web.DotCom.Provider;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Website.Controls;
using Website.BL.ListItem;

namespace Website.Sublayouts
{
	public partial class ItemDetailsPage : BaseUserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				string id = Request.QueryString["id"];

				Guid sitecoreId;
				long prprId;

				if (Guid.TryParse(id, out sitecoreId))
				{
					GetItemFromSitecore(id);
				}
				else if (long.TryParse(id, out prprId))
				{
					GetItemFromDatabase(id, Request.QueryString["aid"], Request.QueryString["at"]);
				}
				else
				{
					ShowNotFound();
				}
			}
			catch (Exception ex)
			{
#if DEBUG
				CareMoreUtilities.DisplayError(phError, ex, phDetails);
#else
				SetDownMessage(ex, phError, phDetails);
				phDetails.Visible = false;
#endif
			}
		}

		protected void BackToResultsLB_Click(object sender, EventArgs e)
		{
			Item facilityItem = Sitecore.Context.Item.Parent;

			if (facilityItem != null)
			{
				Response.Redirect(LinkManager.GetItemUrl(facilityItem), false);
			}
		}

		private void GetItemFromDatabase(string id, string addressId, string addressType)
		{
			List<FacilitySearchDetail> details = ProviderFinder.GetFacilityDetail(ProviderHelper.GetPortalCode(BL.SiteSettings.SiteSettingPath),
				id, addressType);

			if (details.Count == 0)
			{
				ShowNotFound();
			}
			else
			{
				FacilitySearchDetail detail = details[0];

				List<string> services = null;
				if (CurrentItem.Fields["Show Services"].Value == "1")
					services = ProviderHelper.GetServices(ProviderFinder.GetProviderServices(id, addressId, addressType));

				ShowDetail(detail, services);

				LocationItem location = new LocationItem();

				location.Address1 = detail.Address1;
				location.City = detail.City;
				location.ZipCode = detail.Zip;
				location.Name = detail.Name;
				location.IconImage = GetIconImage();
				location.Description = string.Empty;
				location.Phone = detail.Phone;
				location.ItemID = detail.ProviderId;

				ucGoogleMap.Locations.Add(location);
				ucGoogleMap.DisplayMap();
			}
		}

		private void GetItemFromSitecore(string id)
		{
			ID facilityId = new ID(id);
			Item facilityItem = CurrentDb.GetItem(facilityId, Sitecore.Globalization.Language.Parse("en"));

			if (facilityItem == null)
			{
				ShowNotFound();
			}
			else
			{
				FacilitySearchDetail detail = new FacilitySearchDetail();
				detail.Accessibility = !string.IsNullOrEmpty(facilityItem["Handicap Accessibility"]);
				detail.Address1 = facilityItem["Address Line 1"];
				detail.Address2 = facilityItem["Address Line 2"];
				detail.City = facilityItem["City"];
				detail.County = facilityItem["County"];
				detail.Fax = facilityItem["Fax"];

				string fieldName;
				if (string.IsNullOrWhiteSpace(facilityItem["Location Name"]))
					fieldName = "Provider Name";
				else
					fieldName = facilityItem["Location Name"];
				detail.Name = facilityItem[fieldName];

				detail.Phone = facilityItem["Phone"];
				detail.ProviderId = facilityItem["Provider ID"];
				detail.State = facilityItem["State"];
				detail.Zip = facilityItem["Postal Code"];

				List<string> services = null;
				if (CurrentItem.Fields["Show Services"].Value == "1")
					services = ProviderHelper.GetServices(facilityItem);

				ShowDetail(detail, services);

				ucGoogleMap.Locations.Add(new LocationItem(facilityItem, fieldName));
				ucGoogleMap.DisplayMap();
			}
		}

		private string GetIconImage()
		{
			string iconImage = "#";
			if (!string.IsNullOrEmpty(CurrentItem.Parent["Provider List Type"]))
			{
				Item providerListing = ((LookupField)CurrentItem.Parent.Fields["Provider List Type"]).TargetItem;
				iconImage = Common.GetKeyImagePath(providerListing);
			}
			return iconImage;
		}

		private void ShowDetail(FacilitySearchDetail detail, List<string> services)
		{
			lblTitle.Text = detail.Name;

			lblPhone.Text = LocationHelper.GetFormattedPhoneNo(detail.Phone);

			if (string.IsNullOrWhiteSpace(detail.Fax))
				pnlFax.Visible = false;
			else
				lblFax.Text = LocationHelper.GetFormattedPhoneNo(detail.Fax);

			if (services ==null || services.Count == 0)
			{
				pnlServices.Visible = false;
			}
			else
			{
				rptServices.DataSource = services;
				rptServices.DataBind();
			}

			if (detail.EffectiveDate > DateTime.Now.Date)
			{
				pnlEffective.Visible = true;
				txtEffectiveDate.Text = detail.EffectiveDate.ToString("MM/dd/yyyy");
			}

			lnkDirections.NavigateUrl = LocationHelper.GetMapQuery(detail.Address1, detail.Address2,
				detail.City, detail.State, detail.Zip);

			Literal addressControl = new Literal();
			addressControl.Text = LocationHelper.GetFormattedAddress(detail.Address1,
				detail.Address2, detail.City, detail.State, detail.Zip);

			phLocation.Controls.Add(addressControl);
		}

		private void ShowNotFound()
		{
			CareMoreUtilities.DisplayError(phError, CurrentItem["No Results Message"], phDetails);
		}
	}
}
