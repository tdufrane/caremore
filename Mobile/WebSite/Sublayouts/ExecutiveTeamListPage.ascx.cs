﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace Website.Sublayouts
{
	public partial class ExecutiveTeamListPage : BaseUserControl
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		protected void LoadPage()
		{
			ID templateID = new ID("{41A406E4-174A-4910-9FE0-EF7801F6EC5A}");
			List<Item> list = base.CurrentItem.Children.Where(i => i.TemplateID == templateID).ToList();

			if (list == null || list.Count == 0)
			{
				lvExecutiveTeam.Visible = false;
			}
			else
			{
				lvExecutiveTeam.DataSource = list;
				lvExecutiveTeam.DataBind();
			}
		}

		#endregion
	}
}
