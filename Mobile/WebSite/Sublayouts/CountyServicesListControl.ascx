﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="CountyServicesListControl.ascx.cs" Inherits="Website.Sublayouts.CountyServicesListControl" %>

<!-- Begin SubLayouts/CountyServicesListControl -->
<asp:UpdatePanel ID="pnlUpdate" runat="server">
	<ContentTemplate>
		<div class="row">
			<div class="col-xs-12">
				<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSearch">
					<div class="form-group">
						<label for="txtZipCode" class="col-xs-12 col-md-2 control-label"><sc:Text ID="txtZipCodeLabel" runat="server" Field="Zip Code Label" /></label>
						<div class="col-xs-12 col-md-2">
							<asp:TextBox ID="txtZipCode" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="5" placeholder="#####" ValidationGroup="CoverageSearch" />
						</div>
						<div class="col-xs-12 col-md-4">
							<asp:RequiredFieldValidator ID="reqValZip" runat="server" ControlToValidate="txtZipCode" Display="Dynamic"
								SetFocusOnError="true" ValidationGroup="CoverageSearch" />
							<asp:RegularExpressionValidator ID="regValZip" runat="server" ControlToValidate="txtZipCode"
								Display="Dynamic" SetFocusOnError="true" ValidationExpression="^[0-9]{5}$" ValidationGroup="CoverageSearch" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12 col-md-offset-2 col-md-2">
							<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" OnClick="BtnSearch_Click" ValidationGroup="CoverageSearch" />
						</div>
					</div>
				</asp:Panel>
				<asp:Panel id="pnlSuccess" runat="server" CssClass="row row-margin-top" Visible="false">
					<div class="col-xs-12 col-md-8">
						<div class="panel panel-success">
						<div class="panel-heading"><sc:Text ID="txtValidHeader" runat="server" Field="Zip Code Valid Header" /></div>
							<div class="panel-body">
								<sc:Text ID="txtValidMessage" runat="server" Field="Zip Code Valid Text" />
							</div>
						</div>
					</div>
				</asp:Panel>
				<asp:Panel id="pnlInvalid" runat="server" CssClass="row row-margin-top" Visible="false">
					<div class="col-xs-12 col-md-8">
						<div class="panel panel-warning">
						<div class="panel-heading"><sc:Text ID="txtInvalidHeader" runat="server" Field="Zip Code Invalid Header" /></div>
							<div class="panel-body">
								<sc:Text ID="txtInvalidMessage" runat="server" Field="Zip Code Invalid Text" />
							</div>
						</div>
					</div>
				</asp:Panel>
				<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
			</div>
		</div>
	</ContentTemplate>
</asp:UpdatePanel>
<!-- End SubLayouts/CountyServicesListControl -->
