﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="CalendarListPage.ascx.cs" Inherits="Website.Sublayouts.CalendarListPage" EnableViewState="true" %>

<!-- Begin SubLayouts/CalendarListPage -->
<asp:UpdatePanel ID="pnlUpdate" runat="server">
	<ContentTemplate>
<div class="row">
	<div class="col-xs-12">
		<asp:Panel ID="pnlEventSearch" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
			<div class="row">
				<div class="col-xs-6 col-md-2">
					<div class="form-group">
						<label for="ZipCode" class="col-xs-12"><cm:Text ID="txtZipCodeLabel" runat="server" Field="Zip Code Label" /></label>
						<div class="col-xs-12"><asp:TextBox ID="ZipCode" runat="server" CssClass="form-control" placeholder="#####"></asp:TextBox></div>
					</div>
				</div>
				<div class="col-xs-6 col-md-2">
					<div class="form-group">
						<label for="DistanceDDL" class="col-xs-12"><cm:Text ID="txtDistanceLabel" runat="server" Field="Distance Label" /></label>
						<div class="col-xs-12">
							<asp:DropDownList ID="ddlDistance" runat="server" AutoPostBack="false" CssClass="form-control" EnableViewState="true">
								<asp:ListItem Value="1">1 mile</asp:ListItem>
								<asp:ListItem Value="2">2 miles</asp:ListItem>
								<asp:ListItem Selected="True" Value="5">5 miles</asp:ListItem>
								<asp:ListItem Value="10">10 miles</asp:ListItem>
								<asp:ListItem Value="25">25 miles</asp:ListItem>
							</asp:DropDownList>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-1 text-center">
					<br class="hidden-xs" /><cm:Text ID="txtOr" runat="server" ItemReferenceName="EVENTS_OR" Field="Text" />
				</div>
				<div class="col-xs-12 col-md-4">
					<div class="form-group">
						<label for="City" class="col-xs-12"><cm:Text ID="txtCityLabel" runat="server" Field="City Label" /></label>
						<div class="col-xs-12">
							<asp:TextBox ID="City" runat="server" CssClass="form-control" />
						</div>
					</div>
				</div>
			</div>
			<div class="row no-margin">
				<div class="col-xs-12 col-md-6">
					<div class="form-group">
						<label for="EventTypeDDL" class="col-xs-12"><cm:Text ID="txtEventTypeLabel" runat="server" Field="Event Type Label" /></label>
						<div class="col-xs-12">
							<asp:DropDownList ID="EventTypeDDL" runat="server" AutoPostBack="false" CssClass="form-control" EnableViewState="true" />
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12">
					<asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary spinnerBtn" onclick="BtnSubmit_Click" />
				</div>
			</div>
		</asp:Panel>

		<div class="row">
			<div class="col-xs-12">
				<cm:Text ID="EventsDisclaimer" Field="Text" runat="server" />
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<hr />
			</div>
		</div>

		<div class="row spinnerDisplay" style="display: none;">
			<div class="col-xs-12 spinnerImage">
				<img src="/images/spinner.gif" alt="Please wait" />
			</div>
		</div>

		<asp:ListView ID="ItemsLV" runat="server" OnItemDataBound="ItemsLV_ItemDataBound" OnLoad="ItemsLV_Load" OnPagePropertiesChanging="ItemsLV_PagePropertiesChanging">
			<EmptyDataTemplate>
				<div class="row hideOnSpinner">
					<div class="col-xs-12">
						<div class="text-danger"><sc:Text ID="txtNoEvents" runat="server" Field="No Events" /></div>
					</div>
				</div>
			</EmptyDataTemplate>
			<LayoutTemplate>
				<div class="hideOnSpinner">
					<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
				</div>
			</LayoutTemplate>
			<ItemSeparatorTemplate>
				<div class="row">
					<div class="col-xs-12">
						<hr />
					</div>
				</div>
			</ItemSeparatorTemplate>
			<ItemTemplate>
				<div class="row">
					<div class="col-xs-12 col-md-4 eventDate">
						<h4><sc:Date ID="DateDt" runat="server" Field="EventDate" Format="MMM d, yyyy" /> : <sc:Date ID="Time" runat="server" Field="EventDate" Format="h:mm tt" /></h4>
						<p><sc:Text ID="CountyTxt" Field="County" runat="server" /></p>
						<p><asp:Literal ID="litEventType" runat="server"></asp:Literal></p>
					</div>
					<div class="col-xs-12 col-md-4 eventInfo">
						<h4><asp:HyperLink ID="NameHL" runat="server" /></h4>
						<h5><sc:Text ID="CityTxt" Field="City" runat="server" /></h5>
						<p><sc:Text ID="SummaryTxt" Field="Summary" runat="server" /></p>
						<p><asp:HyperLink ID="LearnMoreHL" runat="server"><span class="glyphicon glyphicon-chevron-right"></span>
							<sc:Text ID="txtLearnMore" runat="server" Field="Details Link Label" /></asp:HyperLink></p>
					</div>
				</div>
			</ItemTemplate>
		</asp:ListView>

		<div class="searchResultsPaging hideOnSpinner">
			<asp:DataPager ID="ItemsDP" runat="server" PagedControlID="ItemsLV" PageSize="30" OnPreRender="ItemsDP_PreRender">
				<Fields>
					<asp:NextPreviousPagerField PreviousPageText="<<" ShowFirstPageButton="False" ShowPreviousPageButton="True"
						ShowNextPageButton="False" ShowLastPageButton="False" /> 
					<asp:NumericPagerField NumericButtonCssClass="lnkSpinnerDisplay" />
					<asp:NextPreviousPagerField NextPageText=">>" ShowFirstPageButton="False" ShowPreviousPageButton="False"
						ShowNextPageButton="True" ShowLastPageButton="False" LastPageText="" />
				</Fields>
			</asp:DataPager>
		</div>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
	</ContentTemplate>
	<Triggers>
		<asp:AsyncPostBackTrigger ControlID="btnSubmit" />
	</Triggers>
</asp:UpdatePanel>
<!-- End SubLayouts/CalendarListPage -->