﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;
using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using CareMore.Web.DotCom.Provider;

namespace Website.Sublayouts
{
	public partial class LocateServices : System.Web.UI.UserControl
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				if (!IsPostBack)
				{
					LoadPage();
				}
			}
			catch (Exception ex)
			{
#if DEBUG
				CareMoreUtilities.DisplayError(phError, ex, phLocationsMap);
#else
				SetDownMessage(ex);
#endif
			}
		}

		protected void DdlCountyList_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (IsPostBack)
					CreateGoogleMap(ddlCountyList.SelectedValue, ddlMapKeyList.SelectedValue);
			}
			catch (Exception ex)
			{
#if DEBUG
				CareMoreUtilities.DisplayError(phError, ex, phLocationsMap);
#else
				SetDownMessage(ex);
#endif
			}
		}

		protected void DdlMapKeyList_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (IsPostBack)
					CreateGoogleMap(ddlCountyList.SelectedValue, ddlMapKeyList.SelectedValue);
			}
			catch (Exception ex)
			{
#if DEBUG
				CareMoreUtilities.DisplayError(phError, ex, phLocationsMap);
#else
				SetDownMessage(ex);
#endif
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			FillDropDownLists();
			CreateGoogleMap(ddlCountyList.SelectedValue, ddlMapKeyList.SelectedValue);
		}

		private void CreateGoogleMap(string county, string filter)
		{
			List<GoogleMapKey> keys = new List<GoogleMapKey>();
			List<LocationItem> locations = new List<LocationItem>();

			if (string.IsNullOrEmpty(ddlMapKeyList.SelectedValue))
			{
				for (int index = 1; index < ddlMapKeyList.Items.Count; index++)
				{
					LocationListing locationListing = GetListing(ddlMapKeyList.Items[index].Value, county);

					if ((locationListing.List != null) && (locationListing.List.Count > 0))
					{
						keys.Add(new GoogleMapKey(locationListing.ImageIconUrl, locationListing.ImageIconText));
						locations.AddRange(locationListing.List);
					}
				}
			}
			else
			{
				LocationListing locationListing = GetListing(ddlMapKeyList.SelectedValue, county);

				if ((locationListing.List != null) && (locationListing.List.Count > 0))
				{
					keys.Add(new GoogleMapKey(locationListing.ImageIconUrl, locationListing.ImageIconText));
					locations.AddRange(locationListing.List);
				}
			}

			LoadProviders(locations, keys);
		}

		private void LoadProviders(List<LocationItem> locations, List<GoogleMapKey> keys)
		{
			if (locations.Count == 0)
			{
				CareMoreUtilities.DisplayError(phError, ItemIds.GetItem("NO_RESULTS")["Text"], phLocationsMap);

				googleMap.Visible = false;
			}
			else
			{
				// build map keys underneath the map
				rptMapKeys.DataSource = keys;
				rptMapKeys.DataBind();

				googleMap.Locations = locations;
				googleMap.Visible = true;
				googleMap.DisplayMap();
			}
		}

		private void FillDropDownLists()
		{
			string state = CareMoreUtilities.GetCurrentLocale();

			// MapKey Dropdown
			string allServicesString = ItemIds.GetItemValue("ALL_SERVICES", "Text", false);
			ddlMapKeyList.Items.Add(new ListItem(allServicesString, string.Empty)); // All Services selection

			List<Item> mapKeyItems = Sitecore.Context.Database.GetItem("{9CC39229-CE6F-48FD-82B8-0C8FCB7DCB7B}").GetChildren()
				.OrderBy(i => i.Name).ToList();

			foreach (Item mapKey in mapKeyItems)
			{
				if (mapKey["Exclude From Service Map"] != "1")
					ddlMapKeyList.Items.Add(new ListItem(mapKey["Title"], mapKey.ID.ToString()));
			}

			ddlMapKeyList.SelectedIndex = 0;

			// County Dropdown
			ddlCountyList.DataSource = Sitecore.Context.Database.SelectItems(string.Format("{0}/{1}//*[@@templatename='County']", CareMoreUtilities.LOCALIZATION_PATH, state))
			                        .ToList().ConvertAll(county => new ListItem(county.Fields["Name"].Value));
			ddlCountyList.DataBind();
		}

		private LocationListing GetListing(string id, string county)
		{
			LocationListing locationListing = new LocationListing();

			Database currentDB = Sitecore.Context.Database;
			Item listing = currentDB.GetItem(id);

			if ((!string.IsNullOrEmpty(listing["Data Source"])) && (listing["Exclude From Service Map"] != "1"))
			{
				// Find the details page for this listing
				Item resultsItem = Sitecore.Context.Item.Axes.GetDescendants().Where(item => item["Provider List Type"].Equals(id)).FirstOrDefault();
				Item detailsPage = resultsItem.GetChildren().Where(i => i.Name == "Details").FirstOrDefault();
				string detailsBaseUrl = LinkManager.GetItemUrl(detailsPage);
				string imageIcon = Common.GetKeyImagePath(listing);

				List<LocationItem> searchResults = new List<LocationItem>();
				string dataSource = currentDB.GetItem(listing["Data Source"]).Name;

				if ((dataSource.Equals("Both")) || (dataSource.Equals("Sitecore")))
				{
					List<LocationItem> sitecoreResults = GetSitecoreListing(listing["Title"], currentDB.GetItem(listing["Sitecore Path"]), county, detailsBaseUrl, imageIcon);
					searchResults.AddRange(sitecoreResults);
				}

				if ((dataSource.Equals("Both")) || (dataSource.Equals("Database")))
				{
					//IEnumerable<Item> specialtyFilters
					object x = listing["Specialty Filter"];

					List<LocationItem> databaseResults = GetDatabaseListing(listing["Title"], county, listing["Class Code"], null, detailsBaseUrl, imageIcon);
					searchResults.AddRange(databaseResults);
				}

				locationListing.List = ApplyExclusions(searchResults, listing["Exclusions"]);
				locationListing.ImageIconText = listing["Title"];
				locationListing.ImageIconUrl = imageIcon;
			}

			return locationListing;
		}

		private List<LocationItem> GetDatabaseListing(string providerTypeName, string county, string classCode, IEnumerable<Item> specialtyFilters, string detailsBaseUrl, string imageIcon)
		{
			List<LocationItem> list = new List<LocationItem>();

			string specialties = string.Empty;
			if (specialtyFilters != null)
			{
				List<string> specialtyList = new List<string>();
				foreach (Item specialtyItem in specialtyFilters)
				{
					specialtyList.Add(specialtyItem["Value"].TrimEnd());
				}

				if (specialtyList.Count > 0)
					specialties = string.Join(",", specialtyList.ToArray());
			}

			if (string.IsNullOrEmpty(classCode))
			{
				List<ProviderSearchResult> practitioners = ProviderFinder.GetProviderList(ProviderHelper.GetPortalCode(BL.SiteSettings.SiteSettingPath),
					null, null, null, county, null,
					null, specialties, null, ProviderHelper.GetMaxZipDistance(BL.SiteSettings.SiteSettingPath));

				foreach (ProviderSearchResult practitioner in practitioners)
				{
					ProviderResultAddress address = new ProviderResultAddress(practitioner);
					list.Add(new LocationItem(address.Address1, address.City, address.Zip, practitioner.Name,
					                          imageIcon, providerTypeName, address.Phone, practitioner.ProviderId, practitioner.AddressType,
					                          string.Format(ProviderHelper.DetailsUrlFormat,
					                                        detailsBaseUrl, practitioner.ProviderId,
					                                        practitioner.AddressId, practitioner.AddressType.Trim())));
				}
			}
			else
			{
				List<FacilitySearchResult> facilities = ProviderFinder.GetFacilityList(ProviderHelper.GetPortalCode(BL.SiteSettings.SiteSettingPath),
					classCode, null, null, null, county, null,
					ProviderHelper.GetMaxZipDistance(BL.SiteSettings.SiteSettingPath));

				foreach (FacilitySearchResult facility in facilities)
				{
					FacilityResultAddress address = new FacilityResultAddress(facility);
					list.Add(new LocationItem(address.Address1, address.City, address.Zip, facility.Name,
					                          imageIcon, providerTypeName, address.Phone, facility.ProviderId, facility.AddressType,
					                          string.Format(ProviderHelper.DetailsUrlFormat,
					                                        detailsBaseUrl, facility.ProviderId,
					                                        facility.AddressId, facility.AddressType.Trim())));
				}
			}

			return list;
		}

		private List<LocationItem> GetSitecoreListing(string providerTypeName, Item listRoot, string county, string detailsBaseUrl, string imageIcon)
		{
			List<LocationItem> list = new List<LocationItem>();

			Item englishItem = Sitecore.Context.Database.GetItem(listRoot.ID, Sitecore.Globalization.Language.Parse("en"));

			IEnumerable<Item> itemList = englishItem.Axes.GetDescendants().Where(item => item["County"] == county);

			list = itemList.ToList().ConvertAll(item =>
				new LocationItem(item, "Provider Name", imageIcon, providerTypeName,
				                 string.Format("{0}?id={1}&aid=&at=P", detailsBaseUrl, item.ID)));

			return list;
		}

		private List<LocationItem> ApplyExclusions(List<LocationItem> locations, string exclusions)
		{
			List<LocationItem> filtered = null;

			if (string.IsNullOrEmpty(exclusions))
			{
				filtered = locations;
			}
			else
			{
				List<string> providerExclusions = ProviderHelper.GetExclusions(exclusions);

				foreach (string exclusion in providerExclusions)
				{
					IEnumerable<LocationItem> filteredList;

					if (exclusion.Contains('='))
					{
						filteredList = locations.Where(item => (string.Format("{0}={1}", item.ItemID, item.AddressType.Trim()) != exclusion));
					}
					else
					{
						filteredList = locations.Where(item => (item.ItemID != exclusion));
					}

					filtered = filteredList.ToList();
				}
			}

			return filtered;
		}

		private void SetDownMessage(Exception ex)
		{
			Item message = ItemIds.GetItem("FIND_A_DOCTOR_DOWN_MESSAGE");
			CareMoreUtilities.DisplayError(phError, message["Text"], phLocationsMap);
#if !DEBUG
			Common.EmailException(ex);
#endif
			phLocationsMap.Visible = false;
		}

		#endregion

		#region Private Classes

		private class LocationListing
		{
			public List<LocationItem> List;
			public string ImageIconUrl;
			public string ImageIconText;
		}

		private class GoogleMapKey
		{
			public string ImageUrl { get; set; }
			public string Text { get; set; }

			public GoogleMapKey() { }

			public GoogleMapKey(string imageUrl, string text)
			{
				ImageUrl = imageUrl;
				Text = text;
			}
		}

		#endregion
	}
}
