﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Links;
using Website;

namespace Website.Sublayouts
{
	public partial class StateLandingPage : BaseUserControl
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			ReferenceField stateField = CurrentItem.Fields["State"];
			Item stateItem = CurrentDb.GetItem(stateField.TargetID);

			SetState(stateItem["Name"]);
			Response.Redirect(LinkManager.GetItemUrl(CareMoreUtilities.HomeItem(stateItem["Name"])), false);
		}

		private void SetState(string state)
		{
			if (Request.Cookies[CareMoreUtilities.LocalLanguageKey] != null)
			{
				Request.Cookies.Remove(CareMoreUtilities.LocalLanguageKey);
				Response.Cookies.Remove(CareMoreUtilities.LocalLanguageKey);
			}

			if (Request.Cookies[CareMoreUtilities.LocalStateKey] != null)
			{
				Request.Cookies.Remove(CareMoreUtilities.LocalStateKey);
				Response.Cookies.Remove(CareMoreUtilities.LocalStateKey);
			}

			// Clear state & county from plan finder page
			Session.Remove(CareMoreUtilities.LastCountySelectedKey);
			Session.Remove(CareMoreUtilities.LastStateSelectedKey);

			string newLanguage = CareMoreUtilities.EnglishKey;
			Session[CareMoreUtilities.LocalStateKey] = state;

			HttpCookie aCookie = new HttpCookie("hasVisit");
			aCookie.Value = "true";
			aCookie.Expires = DateTime.Now.AddYears(50);
			Response.Cookies.Add(aCookie);

			aCookie = new HttpCookie(CareMoreUtilities.LocalStateKey);
			aCookie.Value = (string)Session[CareMoreUtilities.LocalStateKey];
			aCookie.Expires = DateTime.Now.AddYears(50);
			Response.Cookies.Add(aCookie);

			if (!CareMoreUtilities.GetCurrentLanguage().Equals(newLanguage, StringComparison.OrdinalIgnoreCase))
			{
				Session[CareMoreUtilities.LocalLanguageKey] = newLanguage;
				Sitecore.Context.SetLanguage(LanguageManager.GetLanguage(newLanguage), true);

				aCookie = new HttpCookie(CareMoreUtilities.LocalLanguageKey);
				aCookie.Value = newLanguage;
				aCookie.Expires = DateTime.Now.AddYears(50);
				Response.Cookies.Add(aCookie);
			}

			Session[CareMoreUtilities.LocalizationStateSelectedKey] = true;
		}

		#endregion
	}
}
