﻿using System;
using System.Linq;
using CareMore.Web.DotCom;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Globalization;
using Website.Controls;
using Website.Sublayouts.Widgets;
using Website.BL.ListItem;
using System.Text;
using System.Web.UI.WebControls;
using System.Net.Mail;

namespace Website.Sublayouts
{
	public partial class CalendarDetailPage : System.Web.UI.UserControl
	{
		#region Private variables

		private const string PostBackSessionName = "CalendarDetailPage";

		private string registrationItemID { get; set; }
		private string disclaimerItemID { get; set; }
		private Item calendarItem = Sitecore.Context.Database.GetItem(Sitecore.Context.Item.ID, Sitecore.Globalization.Language.Parse("en"));

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				string state = CareMoreUtilities.GetCurrentLocale();

				if (state.ToLower() == "virginia")
				{
					disclaimerItemID = "EVENTS_VA_DISCLAIMER";
					registrationItemID = "EVENTS_VA_REGISTRATION";
				}
				else
				{
					disclaimerItemID = "EVENTS_DISCLAIMER";
					registrationItemID = "EVENTS_REGISTRATION";
				}

				SetStateItems();

				if (Sitecore.Context.Language != Sitecore.Globalization.Language.Parse("en"))
				{
					Sitecore.Context.Item = calendarItem;
				}

				if (IsPostBack)
				{
					CareMoreUtilities.ClearControl(phError);
				}
				else
				{
					Session[PostBackSessionName] = null;
					LoadPage();
				}

				AddressPH.Controls.Add(CareMoreUtilities.GetInlineEditableAddress(calendarItem));

				googleMap.Locations.Add(new LocationItem(calendarItem, "Place"));
				googleMap.DisplayMap();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				if (Page.IsValid)
				{
					if (Session[PostBackSessionName] == null)
					{
						if (SendEmail())
						{
							//Set session to prevent refresh posts
							Session[PostBackSessionName] = DateTime.Now;

							phRegistration.Visible = false;
							modalConfirmPopup.Visible = true;

							Type cstype = this.GetType();
							string scriptName = "event_confirm";
							if (!Page.ClientScript.IsStartupScriptRegistered(cstype, scriptName))
							{
								String script = "$j(document).ready(function () {";
								script += "if ($j('#modalConfirmPopup').length)";
								script += "$j('#ConfirmAnchor').click(); });";
								Page.ClientScript.RegisterStartupScript(cstype, scriptName, script, true);

								Literal lit = new Literal { Text = "<div class=\"modalPopupInner\"><iframe class=\"styledFrame\" src=\"" + Request.Url.AbsolutePath + "?p=1&amp;r=y\" title=\"Event Registration Confirmation\">Event Registration Confirmation</iframe></div>" };
								modalConfirmPopup.Controls.Add(lit);
							}
						}
					}
					else
					{
						RegConfirmHeader.Visible = false;
						phRegistration.Visible = false;
						modalConfirmPopup.Visible = false;
						CareMoreUtilities.DisplayNoMultiSubmit(phError);
					}
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			Item regItem = Sitecore.Context.Database.GetItem(ItemIds.GetID(registrationItemID), Sitecore.Context.Language);
			if (regItem != null)
			{
				btnSubmit.Text = regItem["Submit Button Text"];

				BringGuestRBL.Items[0].Text = BringGuestRBL.Items[0].Value = regItem.Fields["Yes Text"].Value;
				BringGuestRBL.Items[1].Text = BringGuestRBL.Items[1].Value = regItem.Fields["No Text"].Value;

				FillStateList((string)Session["LocalState"]);

				CalendarHL.Text = regItem.Fields["Back Link Text"].Value;
				lblGetDirections.Text = regItem.Fields["Directions Text"].Value;
			}

			lnkDirections.NavigateUrl = LocationHelper.GetMapQuery(calendarItem["Address Line 1"], calendarItem["Address Line 2"],
			calendarItem["City"], calendarItem["State"], calendarItem["Postal Code"]);

			if (Request.QueryString["r"] != null && Request.QueryString["r"] == "y")
			{
				phRegistration.Visible = false;
				RegConfirmText.Visible = true;
				phRegConfirmFooter.Visible = true;
				phEventHeader.Visible = false;
				RegConfirmHeader.Visible = true;
				lnkDirections.Visible = false;

				string currentStateId = CareMoreUtilities.GetCurrentLocaleItem().ID.ToString();
				SetLogo(currentStateId);
			}
			else
			{
				RenderEventType();
				OutputCalendarHyperLink();
			}
		}

		private void OutputCalendarHyperLink()
		{
			const string EVENTS_LIST_PATH = CareMoreUtilities.HOME_PATH + "/About/Events Near You";

			string url = Request.UrlReferrer != null ? Request.UrlReferrer.AbsolutePath : string.Empty;
			CalendarHL.NavigateUrl = (url.Length > 0 ? url : Sitecore.Links.LinkManager.GetItemUrl(Sitecore.Context.Database.SelectSingleItem(EVENTS_LIST_PATH))) +
				(Request.QueryString.Count > 0 ? "?" + Request.QueryString.ToString() : string.Empty) +
				(url == "/" ? "#events" : string.Empty);
		}

		private void RenderEventType()
		{
			MultilistField EventType = calendarItem.Fields["EventType"];
			Item[] eventTypes = EventType.GetItems();
			litEventType.Text = string.Empty;

			foreach (Item eventType in eventTypes)
			{
				litEventType.Text += eventType.DisplayName + "<br />";
			}
		}

		private void SetLogo(string stateId)
		{
			Item logoFolder = ItemIds.GetItem("HEADER_LOGO_ID");

			foreach (Item item in logoFolder.Children)
			{
				string locale = item["LocalizedForState"];
				if (locale.Contains(stateId))
				{
					imgLogo.Item = item;
					break;
				}
			}
		}

		private bool SendEmail()
		{
			bool result = false;
			Item regItem = Sitecore.Context.Database.GetItem(ItemIds.GetID(registrationItemID), Sitecore.Context.Language);
			Item calendarItem = Sitecore.Context.Item;

			StringBuilder body = new StringBuilder();
			bool dataEntered = false;

			body.AppendFormat("<div>{0}</div><br /><br />", regItem.Fields["Email Content"].Value);

			if (FirstName.Text.Trim().Length > 0)
				dataEntered = true;
			body.AppendFormat("First Name: {0}<br />", FirstName.Text.Trim());

			if (LastName.Text.Trim().Length > 0)
				dataEntered = true;
			body.AppendFormat("Last Name: {0}<br />", LastName.Text.Trim());

			if (Address.Text.Trim().Length > 0)
				dataEntered = true;
			body.AppendFormat("Address: {0}<br />", Address.Text.Trim());

			if (City.Text.Trim().Length > 0)
				dataEntered = true;
			body.AppendFormat("City: {0}<br />", City.Text.Trim());

			body.AppendFormat("State: {0}<br />", StateDDL.SelectedValue);

			if (ZipCode.Text.Trim().Length > 0)
				dataEntered = true;
			body.AppendFormat("Zip Code: {0}<br />", ZipCode.Text.Trim());

			if (Phone.Text.Trim().Length > 0)
				dataEntered = true;
			body.AppendFormat("Phone: {0}<br />", Phone.Text.Trim());

			if (Email.Text.Trim().Length > 0)
				dataEntered = true;
			body.AppendFormat("Email: {0}<br />", Email.Text.Trim());

			if (BringGuestRBL.SelectedItem != null)
				body.AppendFormat("Extra guest attending: {0}<br />", BringGuestRBL.SelectedItem.Text);

			if (dataEntered)
			{
				DateTime eventDate = Sitecore.DateUtil.IsoDateToDateTime(calendarItem.Fields["EventDate"].Value);
				body.AppendFormat("<br /><br /><u>Event Information</u>:<br />");
				body.AppendFormat("Event Title: {0}<br />", txtTitle.RenderAsText());
				body.AppendFormat("Event Type: {0}<br /><br />", litEventType.Text.Replace("<br /><br />", ", "));
				body.AppendFormat("Day: {0}<br />", eventDate.ToString("dddd"));
				body.AppendFormat("Date: {0}<br />", eventDate.ToShortDateString());
				body.AppendFormat("Time: {0}<br /><br />", eventDate.ToShortTimeString());
				body.AppendFormat("Location: <br />");

				if (CareMoreUtilities.HasFieldValue(calendarItem.Fields["Place"]))
				{
					body.AppendFormat("{0}<br />", calendarItem.Fields["Place"].Value);
				}

				if (CareMoreUtilities.HasFieldValue(calendarItem.Fields["Address Line 1"]))
				{
					body.AppendFormat("{0}<br />", calendarItem.Fields["Address Line 1"].Value);
				}

				if (CareMoreUtilities.HasFieldValue(calendarItem.Fields["AddressLine2"]))
				{
					body.AppendFormat("{0}<br />", calendarItem.Fields["Address Line 2"].Value);
				}

				if (CareMoreUtilities.HasFieldValue(calendarItem.Fields["City"]))
				{
					body.AppendFormat("{0}", calendarItem.Fields["City"].Value);
				}

				if (CareMoreUtilities.HasFieldValue(calendarItem.Fields["State"]))
				{
					body.AppendFormat(", {0}", calendarItem.Fields["State"].Value);
				}

				if (CareMoreUtilities.HasFieldValue(calendarItem.Fields["Postal Code"]))
				{
					body.AppendFormat(" {0}", calendarItem.Fields["Postal Code"].Value);
				}

				body.AppendFormat("<br /><br />Registration Date: {0}<br />", DateTime.Now.ToShortDateString());
				body.AppendFormat("Registration Time: {0}<br />", DateTime.Now.ToShortTimeString());

				MailMessage message = new MailMessage();
				message.To.Add(new MailAddress(regItem.Fields["Destination Email"].Value));
				message.From = new MailAddress(Email.Text.Trim().Length > 0 ? Email.Text.Trim() : regItem.Fields["From Email"].Value);
				message.Subject = regItem.Fields["Email Subject"].Value;
				message.Body = body.ToString();
				message.IsBodyHtml = true;

				//Send email
				CareMoreUtilities.SendEmail(message);

				result = true;
			}
			else
			{
				CareMoreUtilities.DisplayError(phError, regItem.Fields["No Data Entered Error"].Value);
			}

			return result;
		}

		private void FillStateList(string state)
		{
			Item[] states = Sitecore.Context.Database.SelectItems(CareMoreUtilities.LOCALIZATION_PATH + "//*[@@templatename='State']");

			if (states.Count() > 0)
			{
				foreach (Item stateItem in states)
				{
					StateDDL.Items.Add(new ListItem(stateItem["Name"], stateItem["Code"]));
				}

				if (state != null)
				{
					StateDDL.Items.FindByText(state).Selected = true;
				}
			}
		}

		private void SetStateItems()
		{
			Item disclaimerItem = Sitecore.Context.Database.GetItem(ItemIds.GetID(disclaimerItemID), Sitecore.Context.Language);
			Item registrationItem = Sitecore.Context.Database.GetItem(ItemIds.GetID(registrationItemID), Sitecore.Context.Language);

			DisclaimerText.Item = disclaimerItem;
			eventTitle.Item = registrationItem;
			eventPrint.Item = registrationItem;
			eventPrint2.Item = registrationItem;
			RegConfirmText.Item = registrationItem;
			eventDay.Item = registrationItem;
			eventDate.Item = registrationItem;
			eventTime.Item = registrationItem;
			eventLocation.Item = registrationItem;
			//phRegConfirmFooter.Item = registrationItem;
			txtRegContent.Item = registrationItem;
			RegFirstName.Item = registrationItem;
			RegLastName.Item = registrationItem;
			RegAddress.Item = registrationItem;
			RegCity.Item = registrationItem;
			RegState.Item = registrationItem;
			RegZip.Item = registrationItem;
			RegPhone.Item = registrationItem;
			RegEmail.Item = registrationItem;
			RegBringGuest.Item = registrationItem;
			txtRegFooter.Item = registrationItem;
		}

		#endregion
	}
}
