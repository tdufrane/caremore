﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using System.Data.Linq;
using Sitecore.Data.Fields;

namespace Website.Sublayouts
{
	public partial class ProviderTrainingRegistrationPage : ProviderTrainingBasePage
	{
		#region Page events

		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				if (Page.IsPostBack)
				{
					phError.Controls.Clear();
					phError.Visible = false;
				}
				else
				{
					if (IsProviderRegistered(CurrentItem))
					{
						RedirectToContent();
					}
					else
					{
						LoadForm();

						if (ProviderTrainingRegistrationId > 0)
						{
							ProviderTrainingRegistration registration = ProviderRegistration(ProviderTrainingRegistrationId);

							if (registration != null)
								LoadRegistration(registration, true);
						}
					}
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnAlreadyRegistered_Click(object sender, EventArgs e)
		{
			SetAlreadyRegistered();
		}

		protected void BtnRegistered_Click(object sender, EventArgs e)
		{
			try
			{
				ProviderTrainingRegistration registration = ProviderRegistration(txtEmail.Text);

				if (registration == null)
				{
					CareMoreUtilities.DisplayError(phError, "Unable to match email entered, please check your entry and try again.");
				}
				else if (phRegister3.Visible)
				{
					SaveRequest();
					RedirectToContent();
				}
				else
				{
					LoadRegistration(registration, false);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				SaveRequest();
				RedirectToContent();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private ProviderTrainingRegistration ProviderRegistration(string email)
		{
			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			IEnumerable<ProviderTrainingRegistration> results = db.ProviderTrainingRegistrations.Where(item => (item.Email.ToLower() == email.ToLower()));

			if (results.Count() > 0)
				return results.First();
			else
				return null;
		}

		private ProviderTrainingRegistration ProviderRegistration(int registrationId)
		{
			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			IEnumerable<ProviderTrainingRegistration> results = db.ProviderTrainingRegistrations.Where(item => (item.RegistrationID == registrationId));

			if (results.Count() > 0)
				return results.First();
			else
				return null;
		}

		private void LoadForm()
		{
			lnkBtnAlreadyRegistered.Text = CurrentItem["Already Registered Link Text"];

			bool titleRequired = CurrentItem["Title Required"].Equals("1");
			phTitleReq.Visible = titleRequired;
			FillTitlesList();

			pnlSpecialty.Visible = CurrentItem["Specialty Visible"].Equals("1");
			FillSpecialtyList();

			pnlJobTitle.Visible = CurrentItem["Job Title Visible"].Equals("1");
			pnlProviderName.Visible = CurrentItem["Provider Name Visible"].Equals("1");
			pnlOrganization.Visible = CurrentItem["Organization Visible"].Equals("1");
			pnlLicense.Visible = CurrentItem["Practitioner License Visible"].Equals("1");

			bool npiRequired = CurrentItem["NPI Required"].Equals("1");
			phNpiReq.Visible = npiRequired;
			reqValNpi.Visible = npiRequired;
		}

		private void LoadRegistration(ProviderTrainingRegistration registration, bool isFullRegisterForm)
		{
			bool infoFilled = true;

			txtFirstName.Text = registration.FirstName;
			txtLastName.Text = registration.LastName;
			ddlTitle.Text = registration.Title;
			txtEmail.Text = registration.Email;
			txtAddress.Text = registration.Address;
			txtCity.Text = registration.City;
			ddlState.SelectedValue = registration.State;
			txtZipCode.Text = registration.ZipCode;
			txtPhone.Text = registration.Phone;

			// Check each of the required; show only if originally visible and has no db value
			ddlSpecialty.SelectedValue = NullToEmpty(registration.Specialty);
			if (CurrentItem["Specialty Visible"].Equals("1"))
			{
				if (ddlSpecialty.SelectedValue == string.Empty)
					infoFilled = false;
				else
					pnlSpecialty.Visible = false;
			}

			txtJobTitle.Text = NullToEmpty(registration.JobTitle);
			if (CurrentItem["Job Title Visible"].Equals("1"))
			{
				if (txtJobTitle.Text == string.Empty)
					infoFilled = false;
				else
					pnlJobTitle.Visible = false;
			}

			txtProviderName.Text = NullToEmpty(registration.ProviderName);
			if (CurrentItem["Provider Name Visible"].Equals("1"))
			{
				if (txtProviderName.Text == string.Empty)
					infoFilled = false;
				else
					pnlProviderName.Visible = false;
			}

			txtOrganization.Text = NullToEmpty(registration.Organization);
			if (CurrentItem["Organization Visible"].Equals("1"))
			{
				if (txtOrganization.Text == string.Empty)
					infoFilled = false;
				else
					pnlOrganization.Visible = false;
			}

			txtLicense.Text = NullToEmpty(registration.PractitionerLicense);
			if (CurrentItem["Practitioner License Visible"].Equals("1"))
			{
				if (txtLicense.Text == string.Empty)
					infoFilled = false;
				else
					pnlLicense.Visible = false;
			}

			txtNpi.Text = NullToEmpty(registration.NPI);
			if (CurrentItem["NPI Required"].Equals("1"))
			{
				if (txtNpi.Text == string.Empty)
					infoFilled = false;
				else
					pnlNpi.Visible = false;
			}

			if (infoFilled)
			{
				RecordAccess(registration.RegistrationID);
				SetCookie(registration.RegistrationID.ToString());
				RedirectToContent();
			}
			else
			{
				if (isFullRegisterForm)
				{
					SetAlreadyRegistered();
				}

				txtAlready.Visible = false;
				txtMoreInfo.Visible = true;
				phRegister3.Visible = true;
			}
		}

		private void FillSpecialtyList()
		{
			Item[] items = CurrentItem.Axes.SelectItems(CareMoreUtilities.WORDING_PATH + "/Specialties//*[@@templatename='Text']");

			if (items != null && items.Length > 0)
			{
				foreach (Item item in items)
				{
					ddlSpecialty.Items.Add(new ListItem(item["Text"]));
				}
			}
		}

		private void FillTitlesList()
		{
			Item[] items = CurrentDb.SelectItems(CareMoreUtilities.WORDING_PATH + "/Medical Titles//*[@@templatename='Text']");

			if (items != null && items.Length > 0)
			{
				foreach (Item item in items)
				{
					ddlTitle.Items.Add(new ListItem(item["Text"]));
				}
			}
		}

		private void RedirectToContent()
		{
			Item article = CurrentItem.Children.Where(item => item.TemplateName == "Provider Training Article").FirstOrDefault();

			if (article == null)
			{
				CareMoreUtilities.DisplayError(phError, "No training content to display.");
			}
			else
			{
				Response.Redirect(CareMoreUtilities.GetItemUrl(article), false);
			}
		}

		private void SaveRequest()
		{
			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();

			ProviderTrainingRegistration registration;

			IEnumerable<ProviderTrainingRegistration> results = db.ProviderTrainingRegistrations.Where(item => (item.Email.ToLower() == txtEmail.Text.ToLower()));

			if (results.Count() == 0)
			{
				registration = new ProviderTrainingRegistration();
				registration.CreatedOn = DateTime.Now;
			}
			else
			{
				registration = results.First();
				registration.ModifiedOn = DateTime.Now;
			}

			registration.FirstName = txtFirstName.Text;
			registration.LastName = txtLastName.Text;
			registration.Title = ddlTitle.Text;
			registration.Email = txtEmail.Text;
			registration.Address = txtAddress.Text;
			registration.City = txtCity.Text;
			registration.State = ddlState.SelectedValue;
			registration.ZipCode = txtZipCode.Text;
			registration.Phone = CareMoreUtilities.FormattedPhone(txtPhone.Text);
			registration.NPI = txtNpi.Text;

			if (pnlJobTitle.Visible)
				registration.JobTitle = EmptyToNull(txtJobTitle.Text);

			if (pnlProviderName.Visible)
				registration.ProviderName = EmptyToNull(txtProviderName.Text);

			if (pnlOrganization.Visible)
				registration.Organization = EmptyToNull(txtOrganization.Text);

			if (pnlSpecialty.Visible)
				registration.Specialty = EmptyToNull(ddlSpecialty.SelectedValue);

			if (pnlLicense.Visible)
				registration.PractitionerLicense = EmptyToNull(txtLicense.Text);

			if (results.Count() == 0)
				db.ProviderTrainingRegistrations.InsertOnSubmit(registration);

			db.SubmitChanges();

			RecordAccess(registration.RegistrationID);
			SetCookie(registration.RegistrationID.ToString());
		}

		private void SetAlreadyRegistered()
		{
			lnkBtnAlreadyRegistered.Visible = false;
			provRegister.DefaultButton = btnRegistered.ID;
			txtRegister.Visible = false;
			txtAlready.Visible = true;
			phRegister1.Visible = false;
			phRegister2.Visible = false;
			phRegister3.Visible = false;
			pnlNpi.Visible = reqValNpi.Visible;
			pnlAcceptance.Visible = false;
			btnSubmit.Visible = false;
			btnRegistered.Visible = true;
		}

		private string NullToEmpty(string value)
		{
			if (value == null)
				return string.Empty;
			else
				return value;
		}

		private string EmptyToNull(string value)
		{
			if (string.IsNullOrEmpty(value))
				return null;
			else
				return value;
		}

		#endregion
	}
}
