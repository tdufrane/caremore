﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Website.BL;
using CareMore.Web.DotCom;

namespace Website.Sublayouts.Widgets
{
	public partial class CallbackWidget : WidgetBase
	{
		private const string PostBackSessionName = "CallbackWidget";

		#region Properties

		private Item formItem = null;
		public Item FormItem
		{
			get
			{
				if (formItem == null)
				{
					LookupField form = Item.Fields["Web Form"];

					if (form != null && form.TargetItem != null)
						formItem = form.TargetItem;
				}

				return formItem;
			}
		}

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if (IsPostBack)
			{
				phError.Controls.Clear();
				phError.Visible = false;
			}
			else
			{
				Session[PostBackSessionName] = null;
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				if (Session[PostBackSessionName] == null)
				{
					Save();
					SubmitForm();
					ClearForm();

					//Set session to prevent refresh posts
					Session[PostBackSessionName] = DateTime.Now;

					phThankYouMessage.Visible = true;
					phSubmit.Visible = false;
				}
				// In this case due to it being a popup, not alerting user about multiple posts
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void Save()
		{
			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();

			Contact contact = new Contact()
			{
				ContactId = Guid.NewGuid(),
				CreatedOn = DateTime.Now,
				LastName = txtLastName.Text,
				FirstName = txtFirstName.Text,
				ZipCode = txtZipCode.Text,
				Phone = CareMoreUtilities.FormattedPhone(txtPhone.Text)
			};

			db.Contacts.InsertOnSubmit(contact);
			db.SubmitChanges();
		}

		private void SubmitForm()
		{
			MailMessage message = new MailMessage();
			message.From = new MailAddress(FormItem["From"]);

			Common.SetEmail(message.To, FormItem["To"]);
			Common.SetEmail(message.CC, FormItem["CC"]);

			message.Subject = FormItem["Subject"];
			message.Body = string.Format(FormItem["MailContent"],
				txtFirstName.Text, txtLastName.Text, txtZipCode.Text, txtPhone.Text);
			message.IsBodyHtml = true;

			// Send email
			CareMoreUtilities.SendEmail(message);
		}

		private void ClearForm()
		{
			txtLastName.Text = string.Empty;
			txtFirstName.Text = string.Empty;
			txtZipCode.Text = string.Empty;
			txtPhone.Text = string.Empty;
		}

		#endregion
	}
}
