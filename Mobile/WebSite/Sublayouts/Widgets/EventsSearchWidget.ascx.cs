﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Sitecore.Data.Items;
using Sitecore.Data;
using Sitecore.Web.UI.WebControls;
using Sitecore.Globalization;
using Sitecore.Data.Fields;

using Website.BL;
using Website.BL.ListItem;

using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using CareMore.Web.DotCom.Provider;

namespace Website.Sublayouts.Widgets
{
	public partial class EventsSearchWidget : WidgetBase
	{
		#region Private variables

		private Database currentDB = Sitecore.Context.Database;
		private string queryString = string.Empty;
		
		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void LvItems_LayoutCreated(object sender, EventArgs e)
		{
			Text txtEventDateHeader = (Text)lvItems.FindControl("txtEventDateHeader");
			Text txtEventNameHeader = (Text)lvItems.FindControl("txtEventNameHeader");
			Text txtEventCityHeader = (Text)lvItems.FindControl("txtEventCityHeader");
			Text txtEventTypeHeader = (Text)lvItems.FindControl("txtEventTypeHeader");

			txtEventDateHeader.Item = Item;
			txtEventNameHeader.Item = Item;
			txtEventCityHeader.Item = Item;
			txtEventTypeHeader.Item = Item;
		}

		protected void LvItems_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				Item eventItem = e.Item.DataItem as Item;

				Date txtEventDate = (Date)e.Item.FindControl("txtEventDate");
				txtEventDate.Item = eventItem;

				HyperLink hlEventName = (HyperLink)e.Item.FindControl("hlEventName");
				hlEventName.NavigateUrl = Sitecore.Links.LinkManager.GetItemUrl(eventItem) + queryString;
				hlEventName.Text = CareMoreUtilities.GetItemDisplayName(eventItem, "Title");

				Text txtEventCity = (Text)e.Item.FindControl("txtEventCity");
				txtEventCity.Item = eventItem;

				Literal litEventType = (Literal)e.Item.FindControl("litEventType");
				litEventType.Text = RenderEventType(eventItem);
			}
		}

		protected void LvItems_ItemCreated(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.EmptyItem)
			{
				Text NoEventsMsg = (Text)e.Item.FindControl("NoEventsMsg");
				NoEventsMsg.Item = Item;
			}
		}

		protected void BtnSort_Click(object sender, CommandEventArgs e)
		{
			try
			{
				SortResults();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				CareMoreUtilities.SetCurrentZipCode(txtZipCode.Text.Trim());
				ShowResults();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			CareMoreUtilities.PopulateDropDownList(ddlEventType, currentDB.SelectSingleItem(CareMoreUtilities.EVENTS_SORTBY_PATH));

			if (Request.QueryString["evnt"] != null && Request.QueryString["evnt"].Length > 0)
			{
				int eventIndex;
				if (int.TryParse(Request.QueryString["evnt"], out eventIndex) && eventIndex < ddlEventType.Items.Count)
					ddlEventType.SelectedIndex=eventIndex;
			}

			if (Request.QueryString["dist"] != null && Request.QueryString["dist"].Length > 0)
			{
				int distIndex;
				if (int.TryParse(Request.QueryString["dist"], out distIndex) && distIndex < ddlDistance.Items.Count)
					ddlDistance.SelectedIndex = distIndex;
			}

			txtBody.Item = Item;
			txtTitle.Item = Item;
			lnkMain.Item = Item;

			lnkMain.Text = Item.Fields["MoreEvents Link Text"].Value;
			txtZipCodeLabel.Item = Sitecore.Context.Database.GetItem(ItemIds.GetID("EVENTS_ZIP_CODE"), Sitecore.Context.Language);
			txtDistanceLabel.Item = Sitecore.Context.Database.GetItem(ItemIds.GetID("EVENTS_DISTANCE"), Sitecore.Context.Language);
			txtEventTypeLabel.Item = Sitecore.Context.Database.GetItem(ItemIds.GetID("EVENTS_TYPE"), Sitecore.Context.Language);

			string state = CareMoreUtilities.GetCurrentLocale();
			if (state.ToLower() == "virginia")
			{
				txtEventsDisclaimer.Item = Sitecore.Context.Database.GetItem(ItemIds.GetID("EVENTS_VA_DISCLAIMER"), Sitecore.Context.Language);
			}
			else 
			{
				txtEventsDisclaimer.Item = Sitecore.Context.Database.GetItem(ItemIds.GetID("EVENTS_DISCLAIMER"), Sitecore.Context.Language);
			}

			string submitText = Sitecore.Context.Database.GetItem(ItemIds.GetID("EVENTS_SEARCH"), Sitecore.Context.Language).Fields["Text"].Value;
			btnSubmit.Text = submitText;

			string currentzip = CareMoreUtilities.GetCurrentZipCode();
		}

		private void ShowResults()
		{
			int displayCount;
			string orderBy = "EventDate";
			string sortDirection = "ASC";
			int.TryParse(Item.Fields["Display Count"].Value, out displayCount);

			queryString = "?evnt=" + ddlEventType.SelectedIndex.ToString() + "&dist=" + ddlDistance.SelectedIndex.ToString() + "&sortby=" + orderBy + "&dir=" + sortDirection;

			var DB_ID = ItemIds.GetID("EVENTS_DB_ID");
			Item EventsDB = Sitecore.Context.Database.GetItem(DB_ID, Language.Parse("en"));
			string state = CareMoreUtilities.GetCurrentLocale();
			string zipCode = txtZipCode.Text.Trim();

			Item eventTypeItem = null;

			if (ddlEventType.SelectedIndex > 0)
			{
				var sortByID = ItemIds.GetID("EVENT_SORT_BY_ID");
				var sortByItem = currentDB.GetItem(sortByID, Language.Parse("en"));
				Item[] eventTypeChoices = sortByItem.Axes.GetDescendants();

				eventTypeItem = eventTypeChoices.Where(i => i["Value"] == ddlEventType.SelectedValue).FirstOrDefault();
			}

			string[] allowedEventGuids= eventTypeItem != null ? eventTypeItem["Event Type"].Split('|') : null;

			var itemList = (zipCode == "" ?
							from i in EventsDB.Axes.GetDescendants()
							where
								i.TemplateName == "Event" &&
								i["Status"] == "Active" &&
								((DateField)i.Fields["EventDate"]).DateTime >= DateTime.Now &&
								GetComparisonValueState(i["State"]).ToLower().Equals(state.ToLower()) &&
								(allowedEventGuids == null || i["EventType"].Split('|').Any(e => allowedEventGuids.Contains(e)))
							select i
							:
							from i in EventsDB.Axes.GetDescendants()
							join zip in ProviderFinder.GetZipCodes(zipCode, double.Parse(ddlDistance.SelectedValue)) on i["Postal Code"] equals zip.BaseZipCode
							where
								i.TemplateName == "Event" &&
								i["Status"] == "Active" &&
								((DateField)i.Fields["EventDate"]).DateTime >= DateTime.Now &&
								GetComparisonValueState(i["State"]).ToLower().Equals(state.ToLower()) &&
								(allowedEventGuids == null || i["EventType"].Split('|').Any(e => allowedEventGuids.Contains(e)))
							select i);

			List<Item> eventItems;
			if (sortDirection == "ASC")
				eventItems = itemList.OrderBy(i => i.Fields[orderBy].Value).Take(displayCount).ToList();
			else
				eventItems = itemList.OrderByDescending(i => i.Fields[orderBy].Value).Take(displayCount).ToList();

			lvItems.DataSource = eventItems;
			lvItems.DataBind();
		}

		private string GetComparisonValueState(string state)
		{
			string itemPath = string.Format("{0}//*[@@templatename='State' and (@Name='{1}' or @Code='{1}' or @@ID='{1}')]",
				CareMoreUtilities.LOCALIZATION_PATH, state);

			Item stateItem = this.currentDB.SelectSingleItem(itemPath);
			if (stateItem != null)
			{
				state = stateItem.Fields["Name"].Value;
			}

			return state;
		}

		protected void SortResults()
		{
			//string[] args = Request.Params.Get("__EVENTARGUMENT").Split(' ');
			//string SortBy = args[0];
			//string SortDirection = args[1];
			//
			//ShowResults(SortBy, SortDirection); 
		}

		private string RenderEventType(Item item)
		{
			StringBuilder events = new StringBuilder();
			MultilistField EventType = item.Fields["EventType"];

			Item[] eventTypes = EventType.GetItems();
			if (eventTypes.Length > 0)
			{
				foreach (Item eventType in eventTypes)
				{
					events.Append(eventType.DisplayName);
					events.AppendLine("<br />");
				}

				events.Length -= 6; //remove last <br />
			}

			return events.ToString();
		}

		#endregion
	}
}
