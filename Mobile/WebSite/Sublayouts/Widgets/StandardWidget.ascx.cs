﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Website.WebControls;
using Website.BL;

namespace Website.Sublayouts.Widgets
{
	public partial class StandardWidget : WidgetBase
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected override void OnDataBinding(EventArgs e)
		{
			base.OnDataBinding(e);

			try
			{
				if (Item == null)
				{
					Item = WebControlUtil.GetItemFromDataBind(NamingContainer);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			string anchorName = Item["Anchor Name"];
			if (string.IsNullOrEmpty(anchorName))
				aTitle.Visible = false;
			else
				aTitle.Name = anchorName;

			if (string.IsNullOrEmpty(Item["Title"]))
				pnlHeading.Visible = false;
			else
				pnlHeading.Visible = true;

			if (string.IsNullOrEmpty(Item["Image"]))
			{
				pnlImage.Visible = false;
			}
			else
			{
				pnlImage.Visible = true;

				// Didnt use Sitecore media image control as it sets image width/height
				Sitecore.Data.Fields.ImageField image = Item.Fields["Image"];
				imgWidget.ImageUrl = MediaManager.GetMediaUrl(image.MediaItem);
			}

			if (string.IsNullOrEmpty(Item["Link"]))
			{
				pnlLink.Visible = false;
			}
			else
			{
				hlWidgetLink.NavigateUrl = CareMoreUtilities.GetItemUrl(Item, "Link");
				lblWidgetLinkText.Text = CareMoreUtilities.GetNavigationTitle(Item, "Link");
			}
		}

		#endregion
	}
}
