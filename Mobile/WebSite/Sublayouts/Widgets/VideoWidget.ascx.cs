﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Website.WebControls;
using Website.BL;


namespace Website.Sublayouts.Widgets
{
	public partial class VideoWidget : WidgetBase
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected override void OnDataBinding(EventArgs e)
		{
			base.OnDataBinding(e);

			try
			{
				if (Item == null)
				{
					Item = WebControlUtil.GetItemFromDataBind(NamingContainer);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{

			if (string.IsNullOrEmpty(Item["Video URL"]))
			{
				ucVideo.Visible = false;
			}
			//else
			//{
			//	//if (!string.IsNullOrEmpty(Item["Height"]))
			//	//{
			//	//	ucVideo.Height = Item["Height"];
			//	//}
			//	//
			//	//if (!string.IsNullOrEmpty(Item["Width"]))
			//	//{
			//	//	ucVideo.Width = Item["Width"];
			//	//}
			//}

			if (string.IsNullOrEmpty(Item["Link"]))
			{
				pnlLink.Visible = false;
			}
			else
			{
				hlWidgetLink.NavigateUrl = CareMoreUtilities.GetItemUrl(Item, "Link");
				lblWidgetLinkText.Text = CareMoreUtilities.GetNavigationTitle(Item, "Link");
			}
		}

		#endregion
	}
}
