﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VideoWidget.ascx.cs" Inherits="Website.Sublayouts.Widgets.VideoWidget" %>
<%@ Register TagPrefix="cm" TagName="Video" Src="~/Controls/Video.ascx" %>

<!-- Begin Sublayouts/Widgets/VideoWidget -->
<div class="panel panel-default">
	<div class="panel-heading">
		<h3><cm:Text ID="txtTitleLink" runat="server" Field="Title" /></h3>
	</div>
	<div class="panel-video">
		<cm:Video id="ucVideo" runat="server" />
	</div>
	<div class="panel-body no-padding-top">
		<div class="widget-text">
			<cm:Text ID="ucBody" runat="server" Field="Body" />
		</div>

		<asp:Panel ID="pnlLink" runat="server" CssClass="widget-link">
			<p><asp:HyperLink ID="hlWidgetLink" runat="server"><span class="glyphicon glyphicon-chevron-right"></span> <asp:Label ID="lblWidgetLinkText" runat="server" /></asp:HyperLink></p>
		</asp:Panel>
	</div>
</div>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
<!-- End Sublayouts/Widgets/VideoWidget -->
