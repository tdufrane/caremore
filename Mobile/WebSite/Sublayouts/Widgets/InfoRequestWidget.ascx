﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="InfoRequestWidget.ascx.cs" Inherits="Website.Sublayouts.Widgets.InfoRequestWidget" %>

<!-- Begin Sublayouts/Forms/InformationRequest -->
<script type="text/javascript">
	$(function () {
		$(".input-group.date").datepicker({ orientation: "top" });
	});
</script>
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<div class="col-xs-2 col-md-3">
				<img src="/images/i-icon.png" class="img-responsive pull-left" />
			</div>
			<h3 class="col-xs-10 col-md-9 no-margin-top"><cm:Text ID="txtTitleLink" runat="server" Field="Title" /></h3>
		</div>
	</div>
	<div class="panel-body">
		<div class="widget-text">
			<cm:Text ID="ucBody" runat="server" Field="Body" />
		</div>
		<div class="widget-link">
			<p><a href="javascript:;" data-toggle="modal" data-target="#infoReqModal"><span class="glyphicon glyphicon-chevron-right"></span>
				<cm:Text ID="txtWidgetLinkText" runat="server" Field="Link Text" /></a></p>
		</div>
	</div>
</div>
<div id="infoReqModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><%# FormItem["Title"] %></h4>
			</div>
			<div class="modal-body">
				<asp:PlaceHolder ID="phConfirmation" runat="server" Visible="false">
					<sc:Text ID="txtThankYouMessage" runat="server" Field="ThankYouMessage" Item="<%# FormItem %>" />
				</asp:PlaceHolder>

				<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
					<div class="form-group">
						<label for="txtDate" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtDateLabel" runat="server" Field="Date Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-4">
							<div class="input-group date">
								<asp:TextBox ID="txtEventDate" runat="server" CssClass="form-control" MaxLength="50" TextMode="SingleLine" ValidationGroup="InfoRequestWidget" />
								<span class="input-group-addon" id="date-addon1"><i class="glyphicon glyphicon-th"></i></span>
							</div>
							<div class="col-xs-12 col-md-offset-2 col-md-3">
								<asp:CompareValidator ID="cmpValEventDate" runat="server" ControlToValidate="txtEventDate"
									SetFocusOnError="true" ErrorMessage="Format is incorrect"
									Operator="DataTypeCheck" Type="Date" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="txtEvent" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtEventLabel" runat="server" Field="Event Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtEventName" runat="server" CssClass="form-control" MaxLength="100" TextMode="SingleLine" ValidationGroup="InfoRequestWidget" />
						</div>
					</div>
					<div class="form-group">
						<label for="cblHearAboutEvent" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtHearAboutEventLabel" runat="server" Field="Hear About Event Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-9">
							<asp:CheckBoxList ID="cblHearAboutEvent" runat="server" CssClass="radio" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="2" ValidationGroup="InfoRequestWidget">
								<asp:ListItem Text="Direct Mail" Value="Direct Mail" />
								<asp:ListItem Text="Website" Value="Website" />
								<asp:ListItem Text="Friend" Value="Friend" />
								<asp:ListItem Text="CareMore Care Center" Value="CareMore Care Center" />
								<asp:ListItem Text="Flyer" Value="Flyer" />
								<asp:ListItem Text="DoctorTV" Value="DoctorTV" />
								<asp:ListItem Text="Sales " Value="Sales " />
								<asp:ListItem Text="Flyer" Value="Flyer" />
								<asp:ListItem Text="Newspaper" Value="Newspaper" />
								<asp:ListItem Text="Radio" Value="Radio" />
							</asp:CheckBoxList>
						</div>
					</div>
					<div class="form-group">
						<label for="txtCurrentDoctor" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtCurrentDoctorLabel" runat="server" Field="Current Doctor Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtCurrentDoctor" runat="server" CssClass="form-control" MaxLength="50" TextMode="SingleLine" ValidationGroup="InfoRequestWidget" />
						</div>
					</div>
					<div class="form-group">
						<label for="txtFirstName" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtFirstNameLabel" runat="server" Field="First Name Label" Item="<%# FormItem %>" /> <span class="text-danger">*</span></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" MaxLength="50" TextMode="SingleLine" ValidationGroup="InfoRequestWidget" />
						</div>
						<div class="col-xs-12 col-md-3">
							<asp:RequiredFieldValidator id="reqValFirstName" runat="server" ControlToValidate="txtFirstName" Display="Static" ValidationGroup="InfoRequestWidget" />
						</div>
					</div>
					<div class="form-group">
						<label for="txtLastName" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtLastNameLabel" runat="server" Field="Last Name Label" Item="<%# FormItem %>" /> <span class="text-danger">*</span></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" MaxLength="50" TextMode="SingleLine" ValidationGroup="InfoRequestWidget" />
						</div>
						<div class="col-xs-12 col-md-3">
							<asp:RequiredFieldValidator id="reqValLastName" runat="server" ControlToValidate="txtLastName" Display="Static" ValidationGroup="InfoRequestWidget" />
						</div>
					</div>
					<div class="form-group">
						<label for="txtEmail" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtEmailLabel" runat="server" Field="Email Label" Item="<%# FormItem %>" /> <span class="text-danger">*</span></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="100" ValidationGroup="InfoRequestWidget" />
						</div>
						<div class="col-xs-12 col-md-3">
							<asp:RequiredFieldValidator id="reqValEmail" runat="server" ControlToValidate="txtEmail" Display="Static" OnServerValidate="ReqValEmail_OnServerValidate" ValidationGroup="InfoRequestWidget" />
						</div>
					</div>
					<div class="form-group">
						<label for="txtPhone" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtPhoneNumberLabel" runat="server" Field="Phone Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" MaxLength="20" TextMode="SingleLine" placeholder="###-###-####" ValidationGroup="InfoRequestWidget" />
						</div>
					</div>
					<div class="form-group">
						<label for="txtAddress" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtAddressLabel" runat="server" Field="Address Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" MaxLength="100" ValidationGroup="InfoRequestWidget" />
						</div>
					</div>
					<div class="form-group">
						<label for="txtCity" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtCityLabel" runat="server" Field="City Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtCity" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="InfoRequestWidget" />
						</div>
					</div>
					<div class="form-group">
						<label for="ddlState" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtStateLabel" runat="server" Field="State Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:DropDownList ID="ddlState" runat="server" CssClass="form-control" ValidationGroup="InfoRequestWidget">
								<asp:ListItem Text="-select one-" Value="" />
								<asp:ListItem Text="Arizona" Value="Arizona" />
								<asp:ListItem Text="California" Value="California" />
								<asp:ListItem Text="Nevada" Value="Nevada" />
							</asp:DropDownList>
						</div>
					</div>
					<div class="form-group">
						<label for="txtZipCode" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtZipCodeLabel" runat="server" Field="Zip Code Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-4">
							<asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control" MaxLength="5" placeholder="#####" ValidationGroup="InfoRequestWidget" />
						</div>
						<div class="col-xs-12 col-md-offset-2 col-md-3">
							<asp:RegularExpressionValidator ID="regValZipCode" runat="server" ControlToValidate="txtZipCode" ValidationGroup="InfoRequestWidget"
								ErrorMessage="Format for Zip Code is incorrect." SetFocusOnError="true"
								ValidationExpression="^[0-9]{5}$" />
						</div>
					</div>
					<div class="form-group">
						<label for="rblReceiveMedicare" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtReceiveMedicareLabel" runat="server" Field="Receive Medicare Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:RadioButtonList ID="rblReceiveMedicare" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow" ValidationGroup="InfoRequestWidget">
								<asp:ListItem Text="Yes" Value="Yes" />
								<asp:ListItem Text="No" Value="No" Selected="True" />
							</asp:RadioButtonList>
						</div>
					</div>
					<div class="form-group">
						<label for="rblCareMoreMember" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtCareMoreMemberLabel" runat="server" Field="Current Member Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:RadioButtonList ID="rblCareMoreMember" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow" ValidationGroup="InfoRequestWidget">
								<asp:ListItem Text="Yes" Value="Yes" />
								<asp:ListItem Text="No" Value="No" Selected="True" />
							</asp:RadioButtonList>
						</div>
					</div>
					<div class="form-group">
						<label for="txtCurrentHealthPlan" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtCurrentHealthPlanLabel" runat="server" Field="Current Health Plan Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtCurrentHealthPlan" runat="server" CssClass="form-control" MaxLength="100" TextMode="SingleLine" ValidationGroup="InfoRequestWidget" />
						</div>
					</div>
					<div class="form-group">
						<label for="cblCondition" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtConditionLabel" runat="server" Field="Condition Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-9">
							<asp:CheckBoxList ID="cblCondition" runat="server" CssClass="radio" RepeatDirection="Vertical" RepeatLayout="Flow" ValidationGroup="InfoRequestWidget">
								<asp:ListItem Text="Diabetes" Value="Diabetes" />
								<asp:ListItem Text="Cardiovascular Disorder / Chronic Heart Failure" Value="Cardiovascular Disorder / Chronic Heart Failure" />
								<asp:ListItem Text="Chronic Lung Disorder" Value="Chronic Lung Disorder" />
								<asp:ListItem Text="ESRD" Value="ESRD" />
							</asp:CheckBoxList>
						</div>
					</div>
					<div class="form-group">
						<label for="txtComments" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtCommentLabel" runat="server" Field="Comments Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-9">
							<asp:TextBox ID="txtComments" runat="server" CssClass="form-control" Rows="6" TextMode="MultiLine" ValidationGroup="InfoRequestWidget" />
						</div>
					</div>
				</asp:Panel>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"><%# FormItem["Cancel Label"] %></button>
				<asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_Click" Text='<%# FormItem["Submit Label"] %>' ValidationGroup="InfoRequestWidget" />
			</div>
		</div>
	</div>
</div>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
<!-- End Sublayouts/Forms/InformationRequest -->
