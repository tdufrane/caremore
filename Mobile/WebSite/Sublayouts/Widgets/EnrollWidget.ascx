﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EnrollWidget.ascx.cs" Inherits="Website.Sublayouts.Widgets.EnrollWidget" %>

<!-- Begin Sublayouts/Widgets/EnrollWidget -->
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<div class="col-xs-10 col-md-9">
				<h3><cm:Text ID="txtTitle" runat="server" Field="Title" /></h3>
			</div>
			<div class="col-xs-2 col-md-3 pull-right">
				<cm:Image ID="imgWidget" runat="server" CssClass="widget-image img-responsive" Field="Top Right Image" />
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="widget-text">
			<cm:Text ID="ucBody" runat="server" Field="Body" />
		</div>

		<asp:Panel ID="pnlEnroll" runat="server" CssClass="enrollByMail">
			<asp:HyperLink ID="txtLnk" runat="server" Target="_blank" />
		</asp:Panel>

		<cm:Text ID="txtBottomContent" Field="Bottom Content" runat="server" />
	</div>
</div>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
<!-- End Sublayouts/Widgets/EnrollWidget -->
