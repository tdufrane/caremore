﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Website.BL;
using CareMore.Web.DotCom;

namespace Website.Sublayouts.Widgets
{
	public partial class InfoRequestWidget : WidgetBase
	{
		#region Properties

		private Item formItem = null;
		public Item FormItem
		{
			get
			{
				if (formItem == null)
				{
					LookupField form = Item.Fields["Web Form"];

					if (form != null && form.TargetItem != null)
						formItem = form.TargetItem;
				}

				return formItem;
			}
		}

		#endregion

		#region Page events

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				SaveForm();
				EmailForm();
				ClearForm();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void SaveForm()
		{
			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();

			InformationRequest infoRequest = new InformationRequest()
			{
				InformationRequestId = Guid.NewGuid(),
				CreatedOn = DateTime.Now,
				EventDate = CareMoreUtilities.GetDateNull(txtEventDate),
				EventName = txtEventName.Text,
				HearAboutEvent = CareMoreUtilities.GetSelectedItems(cblHearAboutEvent.Items),
				CurrentDoctor = txtCurrentDoctor.Text,
				FirstName = txtFirstName.Text,
				LastName = txtLastName.Text,
				Email = txtEmail.Text,
				Phone = CareMoreUtilities.FormattedPhone(txtPhone.Text),
				Address = txtAddress.Text,
				City = txtCity.Text,
				State = ddlState.SelectedValue,
				ZipCode = txtZipCode.Text,
				ReceiveMedicare = (rblReceiveMedicare.SelectedIndex == 0),
				CurrentMember = (rblCareMoreMember.SelectedIndex == 0),
				CurrentPlan = txtCurrentHealthPlan.Text,
				SpecialConditions = CareMoreUtilities.GetSelectedItems(cblCondition.Items),
				Comments = txtComments.Text
			};

			db.InformationRequests.InsertOnSubmit(infoRequest);
			db.SubmitChanges();
		}

		private void EmailForm()
		{
			Item englishItem = CareMoreUtilities.GetEnglishItem(FormItem);

			MailMessage message = new MailMessage();
			message.From = new MailAddress(englishItem["From"]);

			Common.SetEmail(message.To, englishItem["To"]);
			Common.SetEmail(message.CC, englishItem["CC"]);

			message.Subject = englishItem["Subject"];
			message.Body = string.Format(englishItem["MailContent"],
				DateTime.Now, txtEventDate.Text, txtEventName.Text, CareMoreUtilities.GetSelectedItems(cblHearAboutEvent.Items),
				txtCurrentDoctor.Text, txtFirstName.Text, txtLastName.Text, txtEmail.Text,
				txtPhone.Text, txtAddress.Text, txtCity.Text, ddlState.SelectedValue, txtZipCode.Text,
				CareMoreUtilities.GetSelectedItems(rblReceiveMedicare.Items),
				CareMoreUtilities.GetSelectedItems(rblCareMoreMember.Items),
				CareMoreUtilities.GetSelectedItems(cblCondition.Items),
				txtComments.Text);
				
			message.IsBodyHtml = true;

			// Send email
			CareMoreUtilities.SendEmail(message);
		}

		private void ClearForm()
		{
			txtEventDate.Text = string.Empty;
			txtEventName.Text = string.Empty;
			cblHearAboutEvent.ClearSelection();
			txtCurrentDoctor.Text = string.Empty;
			txtLastName.Text = string.Empty;
			txtFirstName.Text = string.Empty;
			txtEmail.Text = string.Empty;
			txtPhone.Text = string.Empty;
			txtAddress.Text = string.Empty;
			txtCity.Text = string.Empty;
			ddlState.ClearSelection();
			txtZipCode.Text = string.Empty;
			rblReceiveMedicare.ClearSelection();
			rblCareMoreMember.ClearSelection();
			cblCondition.ClearSelection();
			txtComments.Text = string.Empty;
		}

		#endregion
	}
}
