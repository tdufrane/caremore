﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EventsSearchWidget.ascx.cs" Inherits="Website.Sublayouts.Widgets.EventsSearchWidget" EnableViewState="true" %>

<!-- Begin SubLayouts/Widgets/EventsSearchWidget -->
<sc:FieldRenderer ID="txtTitle" EnclosingTag="h2" Field="Title" runat="server" />

<sc:Text ID="txtBody" Field="Body" runat="server" />

<asp:UpdatePanel ID="pnlUpdate" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
		<asp:Panel ID="pnlEventSearch" runat="server" CssClass="form-horizontal" ClientIDMode="Static" DefaultButton="btnSubmit">
			<div class="form-group">
				<div class="col-xs-6 col-md-2">
					<label for="txtZipCode"><sc:Text ID="txtZipCodeLabel" Field="Text" runat="server" /></label><br />
					<asp:TextBox ID="txtZipCode" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="5" placeholder="#####" />
				</div>
				<div class="col-xs-6 col-md-2">
					<label for="ddlDistance"><sc:Text ID="txtDistanceLabel" Field="Text" runat="server" /></label><br />
					<asp:DropDownList ID="ddlDistance" runat="server" AutoPostBack="false" CssClass="form-control" EnableViewState="true" ClientIDMode="Static">
						<asp:ListItem Value="1">1 mile</asp:ListItem>
						<asp:ListItem Value="2">2 miles</asp:ListItem>
						<asp:ListItem Selected="True" Value="5">5 miles</asp:ListItem>
						<asp:ListItem Value="10">10 miles</asp:ListItem>
						<asp:ListItem Value="25">25 miles</asp:ListItem>
					</asp:DropDownList>
				</div>
				<div class="col-xs-6 col-md-4">
					<label for="ddlEventType"><sc:Text ID="txtEventTypeLabel" Field="Text" runat="server" /></label><br />
					<asp:DropDownList ID="ddlEventType" runat="server" AutoPostBack="false" CssClass="form-control" EnableViewState="true" ClientIDMode="Static" />
				</div>
				<div class="col-xs-2 col-md-2">
					<label>&nbsp;</label><br />
					<asp:LinkButton ID="btnSubmit" runat="server" ClientIDMode="Static" CssClass="btn btn-primary spinnerBtn" OnClick="BtnSubmit_Click" />
				</div>
			</div>
		</asp:Panel>

		<div class="row spinnerDisplay" style="display: none;">
			<div class="col-xs-12 spinnerImage">
				<img src="/images/spinner.gif" alt="Please wait" />
			</div>
		</div>

		<asp:HiddenField ID="hidSortExpression" runat="server" />
		<asp:HiddenField ID="hidSortDirection" runat="server" />

		<asp:ListView ID="lvItems" runat="server" OnItemDataBound="LvItems_ItemDataBound" OnLayoutCreated="LvItems_LayoutCreated" OnItemCreated="LvItems_ItemCreated">
			<LayoutTemplate>
				<table class="table table-responsive hideOnSpinner">
					<tr>
						<th>
							<asp:LinkButton id="lbSortDate" runat="server" ClientIDMode="Static" OnCommand="BtnSort_Click" CommandArgument="EventDate" CssClass="spinnerDisplay">
								<cm:Text ID="txtEventDateHeader" Field="Date Header" runat="server" />
							</asp:LinkButton>
						</th>
						<th>
							<asp:LinkButton id="lbSortTitle" runat="server" ClientIDMode="Static" OnCommand="BtnSort_Click" CommandArgument="Title" CssClass="spinnerDisplay">
								<cm:Text ID="txtEventNameHeader" Field="Name Header" runat="server" />
							</asp:LinkButton>
						</th>
						<th>
							<asp:LinkButton id="lbSortCity" runat="server" ClientIDMode="Static" OnCommand="BtnSort_Click" CommandArgument="City" CssClass="spinnerDisplay">
								<cm:Text ID="txtEventCityHeader" Field="Name Header" runat="server" />
							</asp:LinkButton>
						</th>
						<th>
							<asp:LinkButton id="lbSortEventType" runat="server" ClientIDMode="Static" OnCommand="BtnSort_Click" CommandArgument="EventType" CssClass="spinnerDisplay">
								<cm:Text ID="txtEventTypeHeader" Field="EventType Header" runat="server" />
							</asp:LinkButton>
						</th>
					</tr>
					<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
				</table>
			</LayoutTemplate>
			<ItemTemplate>
				<tr>
					<td><sc:Date ID="txtEventDate" Field="EventDate" Format="MM/dd/yy hh:mm tt" runat="server" /></td>
					<td><asp:HyperLink ID="hlEventName" runat="server" /></td>
					<td><sc:Text ID="txtEventCity" runat="server" Field="City" /></td>
					<td><asp:Literal ID="litEventType" runat="server"></asp:Literal></td>
				</tr>
			</ItemTemplate>
			<EmptyDataTemplate>
				<p class="text-danger"><sc:Text ID="NoEventsMsg" runat="server" Field="No Events Found Text" /></p>
			</EmptyDataTemplate>
		</asp:ListView>
	</ContentTemplate>
	<Triggers>
		<asp:AsyncPostBackTrigger ControlID="btnSubmit" />
	</Triggers>
</asp:UpdatePanel>

<div class="bottom-spacer">
	<cm:Link ID="lnkMain" runat="server" Field="Link" AddSpanTag="true" SpanCssClass="glyphicon glyphicon-chevron-right" />
</div>
<div class="eventDisclaimer">
	<sc:Text ID="txtEventsDisclaimer" runat="server" Field="Text" />
</div>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>

<script lang="javascript" type="text/javascript">
	$(document).ready(function () {
		__doPostBack("<%= btnSubmit.UniqueID %>", "");
	});
</script>
<!-- End SubLayouts/Widgets/EventsSearchWidget -->