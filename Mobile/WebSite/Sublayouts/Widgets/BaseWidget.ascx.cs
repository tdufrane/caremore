﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Website.WebControls;
using Sitecore.Diagnostics;
using Website.BL;
using Website.BL.ListItem;
using Sitecore.Data.Fields;
using ImageField = Sitecore.Data.Fields.ImageField;
using Sitecore.Data;
using Sitecore.Resources.Media;
using CareMore.Web.DotCom;

namespace Website.Sublayouts.Widgets
{
	public partial class BaseWidget : System.Web.UI.UserControl
	{
		#region Properties

		public DetailItem DetailItem { get; set; }
		public Item AppItem { get; set; }
		public string CssClass { get; set; }

		public Item Item
		{
			get
			{
				return DetailItem == null ? null : DetailItem.Item; 
			}
		}

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			string itemCssClass = Item["CssClass"];
			if (!string.IsNullOrWhiteSpace(itemCssClass))
				pnlWidget.CssClass = itemCssClass;
			else if (!string.IsNullOrEmpty(CssClass))
				pnlWidget.CssClass = CssClass;

			LoadWidget();
		}

		private void LoadWidget()
		{
			if (Item != null)
			{
				WidgetBase widget = null;

				if (Item.TemplateID == new ID("{5F6B9868-E92F-4821-92FE-6338FCC5A692}"))
				{
					widget = (StandardWidget)Page.LoadControl("~/Sublayouts/Widgets/StandardWidget.ascx");
				}
				else if (Item.TemplateID == new ID("{560DC6D1-2522-4DD8-84B3-C6742E985125}"))
				{
					widget = (EventsSearchWidget)Page.LoadControl("~/Sublayouts/Widgets/EventsSearchWidget.ascx");
				}
				else if (Item.TemplateID == new ID("{420076E2-4690-4CA4-A01C-06E826DDABAF}"))
				{
					widget = (StandardWidget)Page.LoadControl("~/Sublayouts/Widgets/StandardWidget.ascx");
				}
				else if (Item.TemplateID == new ID("{038C5EEE-3660-498A-8B48-F0E57FC00675}"))
				{
					widget = (StandardWidget)Page.LoadControl("~/Sublayouts/Widgets/StandardWidget.ascx");
				}
				else if (Item.TemplateID == new ID("{7985F0A9-F885-4922-8AD5-3200DF89F4E7}"))
				{
					widget = (VideoWidget)Page.LoadControl("~/Sublayouts/Widgets/VideoWidget.ascx");
				}
				else if (Item.TemplateID == new ID("{BD002C2D-F40B-4196-90AC-9885319D5717}"))
				{
					widget = (EnrollWidget)Page.LoadControl("~/Sublayouts/Widgets/EnrollWidget.ascx");
				}
				else if (Item.TemplateID == new ID("{C7C2FE68-E83F-4790-AA5E-009E2887F4CB}"))
				{
					widget = (CallbackWidget)Page.LoadControl("~/Sublayouts/Widgets/CallbackWidget.ascx");
				}
				else if (Item.TemplateID == new ID("{5779D289-4088-41EF-8E41-7A31CF7191C8}"))
				{
					widget = (AdobeReaderWidget)Page.LoadControl("~/Sublayouts/Widgets/AdobeReaderWidget.ascx");
				}
				else if (Item.TemplateID == new ID("{53A1B76A-6F86-4F4E-8C24-85AF4B22451B}"))
				{
					widget = (InfoRequestWidget)Page.LoadControl("~/Sublayouts/Widgets/InfoRequestWidget.ascx");
				}

				if (widget == null)
				{
					LiteralControl body = new LiteralControl(Item["Body"]);
					phItem.Controls.Add(body);
				}
				else
				{
					if (DetailItem.ColumnItem != null)
					{
						widget.ColumnItem = DetailItem.ColumnItem;
						widget.AppItem = AppItem;
					}

					widget.Item = Item;
					widget.DataBind();

					phItem.Controls.Add((Control)widget);
				}
			}
		}

		#endregion
	}
}
