﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="PlanningLanding.ascx.cs" Inherits="Website.Sublayouts.PlanningLanding" %>
<%@ Register TagPrefix="cm" TagName="Grid" Src="~/Sublayouts/GridLayout.ascx" %>

<div class="row top-spacer">
	<div class="col-xs-12 col-md-offset-1 col-md-10">
		<div class="row panel-hero">
			<div class="col-xs-12 col-md-4 panel-hero-image">
				<cm:Image ID="imgMain" Field="Image" CssClass="img-rounded img-responsive" runat="server" />
			</div>
			<div class="col-xs-12 col-md-offset-1 col-md-6">
				<div class="row">
					<div class="col-xs-12">
						<sc:FieldRenderer ID="txtTitle" runat="server" EnclosingTag="h2" FieldName="Title" />
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<sc:Text ID="txtBody" runat="server" Field="Content" />
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 text-xs-center">
						<sc:Text ID="txtSubTitle" runat="server" Field="Subtitle"/>
					</div>
				</div>
				<div class="row bottom-spacer">
					<div class="col-xs-12 col-md-8 text-xs-center">
						<sc:Text ID="txtSpeak" runat="server" Field="Telesales Text" />
					</div>
					<div class="col-xs-12 col-md-4 text-xs-center">
						<cm:Link ID="lnkMain" runat="server" CssClass="btn btn-primary btn-lg" Field="Button Link" />
					</div>
				</div>
			</div>
		</div>

		<asp:Repeater ID="rptPlanningGrid" runat="server">
			<HeaderTemplate>
				<div class="row top-spacer">
			</HeaderTemplate>
			<ItemTemplate>
					<div class="col-xs-12 col-md-6">
						<div class="panel panel-primary panel-landing">
							<div class="panel-heading">
								<sc:Text ID="txtPanelHeading" runat="server" Field="Title" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />
							</div>
							<div class="panel-image pull-left">
								<sc:Image ID="imgPanelImage" runat="server" CssClass="img-responsive" Field="Image" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />
							</div>
							<div class="panel-body panel-margin">
								<div>
									<sc:Text ID="txtPanelBody" runat="server" Field="Body" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />
								</div>
								<div class="widget-link">
									<br />
									<cm:Link id="lnkWidget" runat="server" AddSpanTag="true" Field="Link" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>"
										SpanCssClass="glyphicon glyphicon-chevron-right" />
								</div>
							</div>
						</div>
					</div>
			</ItemTemplate>
			<FooterTemplate>
				</div>
			</FooterTemplate>
		</asp:Repeater>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
