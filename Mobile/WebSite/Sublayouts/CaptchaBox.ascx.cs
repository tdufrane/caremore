﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Website.Sublayouts
{
    public partial class CaptchaBox : System.Web.UI.UserControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (IsPostBack)
                RenderCaptcha();
            else
                NewCaptcha();
        }

        protected void NewImage_Click(object sender, EventArgs e)
        {
            NewCaptcha();
        }

        /// <summary>
        /// call this from the parent page to establish the new captcha code
        /// </summary>
        public void NewCaptcha()
        {
            Session["CaptchaImageText"] = GenerateRandom(6);
            phImage.Controls.Clear();
            RenderCaptcha();
        }

        /// <summary>
        /// call this during the parent page's processing function to validate entered information
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            bool valid = false;

            if (Session["CaptchaImageText"] != null && Session["CaptchaImageText"].ToString().Length > 0 &&
                Session["CaptchaImageText"].ToString().Equals(tbCaptchaGuess.Text.Trim(), StringComparison.OrdinalIgnoreCase))
            {
                //match
                valid = true;
            }

            return valid;
        }

        protected void CusCaptcha_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = IsValid();
        }

        private string GenerateRandom(int length)
        {
            string pool = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789"; //left out O/0 I/1 to prevent abiguous characters
            char[] chararr = pool.ToCharArray();

            string output = string.Empty;
            Random rnd = new Random();

            for (int i = 0; i < length; i++)
            {
                int index = (int)Math.Floor((double)rnd.Next(chararr.Length));
                output += chararr[index];
            }

            return output;
        }

        private void RenderCaptcha()
        {
            Image captchaImage = new Image();
            captchaImage.ID = "CaptchaImage";
            captchaImage.ImageUrl = string.Format("/Captcha.ashx?guid={0}", Guid.NewGuid());

            phImage.Controls.Add(captchaImage);
        }
    }
}
