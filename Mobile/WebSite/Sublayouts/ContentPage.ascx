﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ContentPage.ascx.cs" Inherits="Website.Sublayouts.ContentPage" %>
<%@ Register Src="~/Controls/SideNav.ascx" TagPrefix="uc" TagName="SideNav" %>
<%@ Register Src="~/Sublayouts/OneColumn.ascx" TagPrefix="uc" TagName="OneColumn" %>

<!-- Begin SubLayouts/ContentPage -->
<div class="row">
	<div class="col-xs-12 col-md-9 col-md-push-3">
		<uc:OneColumn ID="ucOneColumn" runat="server" />
	</div>
	<div class="col-xs-12 col-md-3 col-md-pull-9">
		<uc:SideNav id="ucSideNav" runat="server" />
	</div>
</div>
<!-- End SubLayouts/ContentPage -->
