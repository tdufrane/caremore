﻿using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.IO;
using System.Configuration;
using System.Web.SessionState;
using System.Drawing.Imaging;

namespace Website.Sublayouts
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class CaptchaHandler : IHttpHandler, IReadOnlySessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            CaptchaImage ci = new CaptchaImage(context.Session["CaptchaImageText"].ToString(), 200, 50, "Century Schoolbook");

            context.Response.Clear();
            context.Response.ContentType = "image/jpeg";

            // Write the image to the response stream in JPEG format.
            ci.Image.Save(context.Response.OutputStream, ImageFormat.Jpeg);

            // Dispose of the CAPTCHA image object.
            ci.Dispose();
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}
