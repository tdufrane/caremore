﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;

namespace Website.Sublayouts
{
	public class ProviderTrainingBasePage : BaseUserControl
	{
		#region Variables / Constants

		private const string cookieName = "ProviderTrainingRegistrationPage";

		#endregion

		#region Properties

		public int ProviderTrainingRegistrationId { get; set; }

		#endregion

		#region Methods

		protected bool IsProviderRegistered(Item item)
		{
			bool isRegistered = false;
			string itemId = item.ID.Guid.ToString();

			HttpCookie regCookie = Request.Cookies[cookieName];
			if (regCookie != null)
			{
				string regId = regCookie.Values["RegistrationID"];

				if (!string.IsNullOrEmpty(regId))
				{
					ProviderTrainingRegistrationId = int.Parse(regId);

					string cookieId = regCookie.Values[itemId];

					if (!string.IsNullOrEmpty(cookieId))
					{
						isRegistered = true;
					}
				}
			}

			return isRegistered;
		}

		protected void RecordAccess(int registrationId)
		{
			CaremoreApplicationDataContext dbContext = new CaremoreApplicationDataContext();
			dbContext.ProviderTrainingResourceAccessAdd(registrationId,
				CurrentItem.ID.Guid, CurrentItem.Name, CurrentItem["Training Category"]);
		}

		protected void SetCookie(string registrationID)
		{
			HttpCookie regCookie = Request.Cookies[cookieName];
			if (regCookie == null)
			{
				regCookie = new HttpCookie(cookieName);
			}

			string regId = regCookie.Values["RegistrationID"];
			if (string.IsNullOrEmpty(regId))
			{
				regCookie.Values.Add("RegistrationID", registrationID);
			}

			string itemId = CurrentItem.ID.Guid.ToString();
			string itemReg = regCookie.Values[itemId];
			if (string.IsNullOrEmpty(itemReg))
			{
				regCookie.Values.Add(itemId, "true");
			}

			regCookie.Expires = DateTime.Now.AddYears(50);
			Response.Cookies.Add(regCookie);
		}


		#endregion
	}
}
