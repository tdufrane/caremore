﻿using System;
using System.Web.UI;
using Sitecore.Data.Fields;
using CareMore.Web.DotCom;
using Website.BL.Plans;

namespace Website.Sublayouts
{
	public partial class PlanMaterials : BaseUserControl
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void DdlCounty_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				phPlanInfo.Visible = false;
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void DdlPlan_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				ShowMaterials();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			ddlPlan.CountyDropDown = ddlCounty;
			ucAdobeReaderWidget.Item = CurrentItem;
			reqValCountyRequired.ErrorMessage = CurrentItem["County Required Message"];
			reqValPlanRequired.ErrorMessage = CurrentItem["Plan Required Message"];
		}

		private void ShowMaterials()
		{
			phPlanInfo.Visible = ddlPlan.PlanItem != null;

			if (ddlPlan.PlanItem != null)
			{
				LookupField planTypeField = ddlPlan.PlanItem.Fields["Plan Type"];

				if (planTypeField != null && planTypeField.TargetItem != null)
				{
					txtPlanTitle.Item = planTypeField.TargetItem;
					txtDescription.Item = PlanManager.GetPlanCountyItem(planTypeField.TargetItem, ddlCounty.CountyItem);
				}

				rptLinks.DataSource = PlanManager.GetPlanFiles(ddlPlan.PlanItem, ddlCounty.CountyItem);
				rptLinks.DataBind();
			}
		}

		#endregion
	}
}
