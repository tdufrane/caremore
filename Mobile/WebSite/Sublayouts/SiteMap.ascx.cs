﻿using System;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace Website.Sublayouts
{
	public partial class SiteMap : BaseUserControl
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			Item rootItem = CareMoreUtilities.HomeItem();
			Item homeItem = CareMoreUtilities.HomeItem(base.CurrentState);

			TreeNode homeNode = new TreeNode();
			homeNode.Text = CareMoreUtilities.GetLanguageItem(homeItem)["Title"];
			homeNode.NavigateUrl = LinkManager.GetItemUrl(homeItem);

			AddChildItems(homeNode, rootItem, base.CurrentState);

			tvSiteMap.Nodes.Add(homeNode);
		}

		private void AddChildItems(TreeNode node, Item item, string state)
		{
			string californiaID = ItemIds.GetString("CALIFORNIA");

			foreach (Item childItem in item.Children)
			{
				if ((childItem[CareMoreUtilities.IsNotLocalizedKey].Equals(CareMoreUtilities.CheckedTrueKey)) || (childItem[CareMoreUtilities.LocalizedForStateKey].Contains(californiaID)))
				{
					Item childLanguageItem = CareMoreUtilities.GetLanguageItem(childItem);

					if (childLanguageItem != null)
					{
						bool addNode = false;

						TreeNode childNode = new TreeNode();

						// Should prefer Navigation Title over Title if present (for sitemap)
						string title = childLanguageItem["Navigation Title"];

						if (string.IsNullOrEmpty(title))
							title = childLanguageItem["Title"];

						if (string.IsNullOrEmpty(title))
							title = childLanguageItem["Subtitle"];

						if (string.IsNullOrEmpty(title))
							title = childLanguageItem["Header Title"];

						if (string.IsNullOrEmpty(title))
							title = childLanguageItem.Name;

						childNode.Text = title;

						if (childLanguageItem["Show In SiteMap"].Equals(CareMoreUtilities.CheckedTrueKey))
						{
							if (childItem.TemplateName == "Newsletter")
							{
								Sitecore.Data.Fields.FileField fileField = childItem.Fields["PDF File"];
								if (fileField != null && fileField.MediaItem != null) 
								{
									Sitecore.Data.Items.MediaItem media = new Sitecore.Data.Items.MediaItem(fileField.MediaItem);
									childNode.NavigateUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(media);
									childNode.Target = "_blank";
									addNode = true;
								}
							}
							else
							{
								childNode.NavigateUrl = LinkManager.GetItemUrl(childItem);
								addNode = true;
							}
						}

						if (childLanguageItem["Process Children"].Equals(CareMoreUtilities.CheckedTrueKey) && childItem.HasChildren)
						{
							AddChildItems(childNode, childItem, state);
							addNode = true;
						}

						if (addNode)
						{
							node.ChildNodes.Add(childNode);
						}
					}
				}
			}
		}

		#endregion
	}
}
