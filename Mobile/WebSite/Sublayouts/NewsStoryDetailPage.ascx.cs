﻿using System;
using Sitecore.Globalization;

namespace Website.Sublayouts
{
	public partial class NewsStoryDetailPage : BaseUserControl
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		private void LoadPage()
		{
			txtContentTitle.Item = CurrentItem.Parent.Parent;

			base.SetToEnglishItem();

			if (string.IsNullOrEmpty(CurrentItem["Subtitle"]))
			{
				pnlSubtitle.Visible = false;
			}
		}
	}
}
