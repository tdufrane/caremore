﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="PlanPage.ascx.cs" Inherits="Website.Sublayouts.PlanPage" %>
<%@ Register TagPrefix="cm" TagName="Callouts" Src="~/Sublayouts/Controls/Callouts.ascx" %>

<!-- Begin SubLayouts/PlanPage -->
<div class="row">
	<div class="col-xs-12">
		<h1><cm:Text ID="txtTitle" runat="server" Field="Title" /></h1>
<asp:UpdatePanel ID="pnlUpdate" runat="server">
	<ContentTemplate>
		<div class="row">
			<asp:Panel ID="pnlStepContainer" runat="server" CssClass="col-xs-12 col-md-8" DefaultButton="btnZipSearch">
				<h5><cm:Text ID="txtBody" runat="server" Field="Content" /></h5>

				<div class="row planStep">
					<div class="col-xs-12">
						<h3><span class="icon">1</span> <cm:Text ID="txtPlanQuestion1" runat="server" Field="Plan Question 1" /></h3>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-offset-1 col-md-4">
						<asp:TextBox ID="txtZip" runat="server" onkeypress="return isNum(event)" ClientIDMode="Static" CssClass="form-control" MaxLength="5" placeholder="#####" />
					</div>
					<div class="col-xs-12 col-md-4">
						<p><asp:LinkButton ID="btnZipSearch" runat="server" CssClass="btn btn-primary" OnClick="BtnZipSearch_Click"><sc:Text ID="lblZipSearch" runat="server" Field="Search Button Label" /></asp:LinkButton></p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-offset-1 col-md-8">
						[ <asp:LinkButton ID="btnResetTop" OnClick="BtnReset_OnClick" runat="server" CausesValidation="false"><sc:Text ID="txtShowAllTop" runat="server" Field="Show All" /></asp:LinkButton> ]
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-offset-1 col-md-8">
						<p class="errorMsg"><asp:Label ID="lblError" runat="server" Visible="false" /></p>
						<p><asp:Label ID="lblZipSuccess" runat="server" Visible="false" /></p>
					</div>
				</div>
				<hr />
				<div class="row planStep">
					<div class="col-xs-12">
						<h3><span class="icon">2</span> <cm:Text ID="txtPlanQuestion2" Field="Plan Question 2" runat="server" /></h3>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-offset-1 col-md-11">
						<asp:Repeater ID="rptQuestions" runat="server" OnItemDataBound="RptQuestions_OnItemDataBound">
							<ItemTemplate>
								<p><strong><%# Eval("QuestionText") %></strong></p>
								<div>
									<asp:CheckBoxList ID="checkQuestions" runat="server" AutoPostBack="true" CssClass="radio"
										RepeatDirection="Vertical" RepeatLayout="Flow" DataValueField="Value" DataTextField="Text">
									</asp:CheckBoxList>
									<asp:RadioButtonList ID="radioQuestions" runat="server" AutoPostBack="true" CssClass="radio-inline"
										RepeatDirection="Horizontal" RepeatLayout="Flow" DataValueField="Value" DataTextField="Text">
									</asp:RadioButtonList>
								</div>
							</ItemTemplate>
						</asp:Repeater>
					</div>
				</div>
			</asp:Panel>
			<div class="col-xs-12 col-md-4">
				<div class="row">
					<div class="col-xs-12">
						<h4><cm:Text ID="txtSidebarHeadline" Field="Sidebar Headline" runat="server" /></h4>
						<cm:Text ID="txtSidebarBody" Field="Sidebar Body" runat="server" />
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<cm:Callouts ID="ucCallouts" runat="server" WidgetCssClass="callout-rt-side" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<hr />
				<div class="row planStep">
					<div class="col-xs-12">
						<h3><span class="icon">3</span> <cm:Text ID="txtPlanQuestion3" Field="Plan Question 3" runat="server" /></h3>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-offset-1 col-md-4">
						<asp:DropDownList ID="ddlCountyList" runat="server" AutoPostBack="true" CssClass="form-control" EnableViewState="true" OnSelectedIndexChanged="DdlCountyList_SelectedIndexChanged"></asp:DropDownList>
					</div>
				</div>
				<hr />
				<div class="row planStep">
					<div class="col-xs-12 col-md-6">
						<h3><span class="icon">4</span> <cm:Text ID="txtPlanQuestion4" Field="Plan Question 4" runat="server" /></h3>
					</div>
					<div class="col-xs-12 col-md-6 text-right text-xs-left">
						<cm:Text ID="txtPlanSelectionNote" Field="Plan Selection Note" runat="server" /><br />
						[ <asp:LinkButton ID="btnReset" runat="server" OnClick="BtnReset_OnClick" CausesValidation="false"><sc:Text ID="txtShowAll" runat="server" Field="Show All" /></asp:LinkButton> ]
					</div>
				</div>
			</div>
		</div>
		<hr />
		<div class="row">
			<div class="col-xs-12">
				<asp:Repeater ID="rptPlans" runat="server" OnItemDataBound="RptPlans_OnItemDataBound">
					<HeaderTemplate>
						<div class="row">
							<div class="col-xs-12 col-md-3"><sc:Text ID="txtPlanName" runat="server" Field="Plan Name Header" /></div>
							<div class="col-xs-12 col-md-6"><sc:Text ID="txtDescription" runat="server" Field="Description Header" /></div>
							<div class="col-xs-12 col-md-3"><sc:Text ID="txtLinks" runat="server" Field="Links Header" /></div>
						</div>
					</HeaderTemplate>
					<ItemTemplate>
						<div class="row">
							<div class="col-xs-12 col-md-3">
								<h4><sc:Text ID="txtTitle" runat="server" Field="Title" /></h4>
							</div>
							<div class="col-xs-12 col-md-6">
								<sc:Text ID="txtDescription" runat="server" Field="Description" />
							</div>
							<div class="col-xs-12 col-md-3">
								<p><strong><sc:Text ID="txtNoPlan" runat="server" Field="No Application Message" /></strong></p>
								<p class="text-xs-center"><asp:HyperLink ID="hlEnroll" runat="server" CssClass="btn btn-primary" /></p>
								<ul>
									<asp:Repeater ID="rptLinks" runat="server">
										<ItemTemplate>
											<li><a href="<%# Eval("Url") %>" target="<%# Eval("Target") %>"><%# Eval("Text") %></a></li>
										</ItemTemplate>
									</asp:Repeater>
								</ul>
							</div>
						</div>
					</ItemTemplate>
					<SeparatorTemplate>
						<div class="row">
							<div class="col-xs-12">
								<hr />
							</div>
						</div>
					</SeparatorTemplate>
				</asp:Repeater>
				<asp:Placeholder id="phNoPlansMessage" runat="server">
					<h5><sc:Text ID="txtNoPlanResults" runat="server" Field="No Plan Results" /></h5>
				</asp:Placeholder>
			</div>
		</div>
		<hr />
		<div class="row">
			<div class="col-xs-12 col-md-8">
				<cm:Text ID="PharmacyNote" Field="Pharmacy Note" runat="server" />
			</div>
			<div class="col-xs-12 col-md-4 text-right text-xs-left">
				<cm:Text ID="Text11" Field="Plan Selection Note" runat="server" /><br />
				[ <a href="#"><cm:Text ID="txtToTop" runat="server" ItemReferenceName="TO_TOP" Field="text" /></a> |
				<asp:LinkButton ID="btnResetBottom" runat="server" OnClick="BtnReset_OnClick" CausesValidation="false"><sc:Text ID="txtShowAllBottom" runat="server" Field="Show All" /></asp:LinkButton> ]
			</div>
		</div>
		<hr />
		<div class="row">
			<div class="col-xs-12">
				<h6 id="ContractTitleTag" runat="server"><cm:Text ID="ContractTitle" Field="text" runat="server" ItemReferenceName="CONTRACT_TITLE" /></h6>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<cm:Text ID="Text16" Field="text" runat="server" ItemReferenceName="CONTRACT_BODY" />
			</div>
		</div>
		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</ContentTemplate>
</asp:UpdatePanel>
	</div>
</div>

<script type="text/javascript">
	function isNum(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		else
			return true;
	}
</script>
<!-- End SubLayouts/PlanPage -->
