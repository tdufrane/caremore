﻿namespace CreateSiteCoreUpdateXml
{
	partial class FormMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.labelPackageName = new System.Windows.Forms.Label();
			this.textBoxPackageName = new System.Windows.Forms.TextBox();
			this.labelAuthor = new System.Windows.Forms.Label();
			this.textBoxAuthor = new System.Windows.Forms.TextBox();
			this.labelVersion = new System.Windows.Forms.Label();
			this.labelPublisher = new System.Windows.Forms.Label();
			this.textBoxPublisher = new System.Windows.Forms.TextBox();
			this.numericUpDownVersion = new System.Windows.Forms.NumericUpDown();
			this.labelSourceName = new System.Windows.Forms.Label();
			this.textBoxSourceName = new System.Windows.Forms.TextBox();
			this.buttonCreate = new System.Windows.Forms.Button();
			this.buttonExit = new System.Windows.Forms.Button();
			this.menuStripMain = new System.Windows.Forms.MenuStrip();
			this.toolStripMenuItemFile = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItemOpen = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItemClose = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItemSave = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItemSaveAs = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItemRecent = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItemExit = new System.Windows.Forms.ToolStripMenuItem();
			this.folderBrowserDialogSettings = new System.Windows.Forms.FolderBrowserDialog();
			this.openFileDialogSettings = new System.Windows.Forms.OpenFileDialog();
			this.saveFileDialogSettings = new System.Windows.Forms.SaveFileDialog();
			this.statusStripMain = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabelMain = new System.Windows.Forms.ToolStripStatusLabel();
			this.labelLastUpdated = new System.Windows.Forms.Label();
			this.dateTimePickerLastUpdated = new System.Windows.Forms.DateTimePicker();
			this.labelFilter = new System.Windows.Forms.Label();
			this.textBoxFilter = new System.Windows.Forms.TextBox();
			this.labelXmlFileName = new System.Windows.Forms.Label();
			this.textBoxXmlFileName = new System.Windows.Forms.TextBox();
			this.buttonXmlFileName = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownVersion)).BeginInit();
			this.menuStripMain.SuspendLayout();
			this.statusStripMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// labelPackageName
			// 
			this.labelPackageName.AutoSize = true;
			this.labelPackageName.Location = new System.Drawing.Point(1, 38);
			this.labelPackageName.Name = "labelPackageName";
			this.labelPackageName.Size = new System.Drawing.Size(84, 13);
			this.labelPackageName.TabIndex = 0;
			this.labelPackageName.Text = "Package Name:";
			// 
			// textBoxPackageName
			// 
			this.textBoxPackageName.Location = new System.Drawing.Point(94, 35);
			this.textBoxPackageName.Name = "textBoxPackageName";
			this.textBoxPackageName.Size = new System.Drawing.Size(315, 20);
			this.textBoxPackageName.TabIndex = 1;
			// 
			// labelAuthor
			// 
			this.labelAuthor.AutoSize = true;
			this.labelAuthor.Location = new System.Drawing.Point(44, 64);
			this.labelAuthor.Name = "labelAuthor";
			this.labelAuthor.Size = new System.Drawing.Size(41, 13);
			this.labelAuthor.TabIndex = 2;
			this.labelAuthor.Text = "Author:";
			// 
			// textBoxAuthor
			// 
			this.textBoxAuthor.Location = new System.Drawing.Point(94, 61);
			this.textBoxAuthor.Name = "textBoxAuthor";
			this.textBoxAuthor.Size = new System.Drawing.Size(315, 20);
			this.textBoxAuthor.TabIndex = 3;
			// 
			// labelVersion
			// 
			this.labelVersion.AutoSize = true;
			this.labelVersion.Location = new System.Drawing.Point(40, 89);
			this.labelVersion.Name = "labelVersion";
			this.labelVersion.Size = new System.Drawing.Size(45, 13);
			this.labelVersion.TabIndex = 4;
			this.labelVersion.Text = "Version:";
			// 
			// labelPublisher
			// 
			this.labelPublisher.AutoSize = true;
			this.labelPublisher.Location = new System.Drawing.Point(32, 116);
			this.labelPublisher.Name = "labelPublisher";
			this.labelPublisher.Size = new System.Drawing.Size(53, 13);
			this.labelPublisher.TabIndex = 6;
			this.labelPublisher.Text = "Publisher:";
			// 
			// textBoxPublisher
			// 
			this.textBoxPublisher.Location = new System.Drawing.Point(94, 113);
			this.textBoxPublisher.Name = "textBoxPublisher";
			this.textBoxPublisher.Size = new System.Drawing.Size(315, 20);
			this.textBoxPublisher.TabIndex = 7;
			// 
			// numericUpDownVersion
			// 
			this.numericUpDownVersion.Location = new System.Drawing.Point(94, 87);
			this.numericUpDownVersion.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
			this.numericUpDownVersion.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numericUpDownVersion.Name = "numericUpDownVersion";
			this.numericUpDownVersion.Size = new System.Drawing.Size(59, 20);
			this.numericUpDownVersion.TabIndex = 5;
			this.numericUpDownVersion.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// labelSourceName
			// 
			this.labelSourceName.AutoSize = true;
			this.labelSourceName.Location = new System.Drawing.Point(10, 142);
			this.labelSourceName.Name = "labelSourceName";
			this.labelSourceName.Size = new System.Drawing.Size(75, 13);
			this.labelSourceName.TabIndex = 8;
			this.labelSourceName.Text = "Source Name:";
			// 
			// textBoxSourceName
			// 
			this.textBoxSourceName.Location = new System.Drawing.Point(94, 139);
			this.textBoxSourceName.Name = "textBoxSourceName";
			this.textBoxSourceName.Size = new System.Drawing.Size(315, 20);
			this.textBoxSourceName.TabIndex = 9;
			// 
			// buttonCreate
			// 
			this.buttonCreate.Location = new System.Drawing.Point(10, 293);
			this.buttonCreate.Name = "buttonCreate";
			this.buttonCreate.Size = new System.Drawing.Size(75, 23);
			this.buttonCreate.TabIndex = 12;
			this.buttonCreate.Text = "&Create";
			this.buttonCreate.UseVisualStyleBackColor = true;
			this.buttonCreate.Click += new System.EventHandler(this.ButtonCreate_Click);
			// 
			// buttonExit
			// 
			this.buttonExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonExit.Location = new System.Drawing.Point(332, 293);
			this.buttonExit.Name = "buttonExit";
			this.buttonExit.Size = new System.Drawing.Size(75, 23);
			this.buttonExit.TabIndex = 13;
			this.buttonExit.Text = "E&xit";
			this.buttonExit.UseVisualStyleBackColor = true;
			this.buttonExit.Click += new System.EventHandler(this.ButtonExit_Click);
			// 
			// menuStripMain
			// 
			this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemFile});
			this.menuStripMain.Location = new System.Drawing.Point(0, 0);
			this.menuStripMain.Name = "menuStripMain";
			this.menuStripMain.Size = new System.Drawing.Size(421, 24);
			this.menuStripMain.TabIndex = 14;
			// 
			// toolStripMenuItemFile
			// 
			this.toolStripMenuItemFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemOpen,
            this.toolStripMenuItemClose,
            this.toolStripMenuItemSave,
            this.toolStripMenuItemSaveAs,
            this.toolStripSeparator1,
            this.toolStripMenuItemRecent,
            this.toolStripSeparator2,
            this.toolStripMenuItemExit});
			this.toolStripMenuItemFile.Name = "toolStripMenuItemFile";
			this.toolStripMenuItemFile.Size = new System.Drawing.Size(37, 20);
			this.toolStripMenuItemFile.Text = "&File";
			// 
			// toolStripMenuItemOpen
			// 
			this.toolStripMenuItemOpen.Name = "toolStripMenuItemOpen";
			this.toolStripMenuItemOpen.Size = new System.Drawing.Size(114, 22);
			this.toolStripMenuItemOpen.Text = "&Open";
			this.toolStripMenuItemOpen.Click += new System.EventHandler(this.ToolStripMenuItemOpen_Click);
			// 
			// toolStripMenuItemClose
			// 
			this.toolStripMenuItemClose.Name = "toolStripMenuItemClose";
			this.toolStripMenuItemClose.Size = new System.Drawing.Size(114, 22);
			this.toolStripMenuItemClose.Text = "&Close";
			this.toolStripMenuItemClose.Click += new System.EventHandler(this.ToolStripMenuItemClose_Click);
			// 
			// toolStripMenuItemSave
			// 
			this.toolStripMenuItemSave.Name = "toolStripMenuItemSave";
			this.toolStripMenuItemSave.Size = new System.Drawing.Size(114, 22);
			this.toolStripMenuItemSave.Text = "&Save";
			this.toolStripMenuItemSave.Click += new System.EventHandler(this.ToolStripMenuItemSave_Click);
			// 
			// toolStripMenuItemSaveAs
			// 
			this.toolStripMenuItemSaveAs.Name = "toolStripMenuItemSaveAs";
			this.toolStripMenuItemSaveAs.Size = new System.Drawing.Size(114, 22);
			this.toolStripMenuItemSaveAs.Text = "Save &As";
			this.toolStripMenuItemSaveAs.Click += new System.EventHandler(this.ToolStripMenuItemSaveAs_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(111, 6);
			// 
			// toolStripMenuItemRecent
			// 
			this.toolStripMenuItemRecent.Name = "toolStripMenuItemRecent";
			this.toolStripMenuItemRecent.Size = new System.Drawing.Size(114, 22);
			this.toolStripMenuItemRecent.Text = "&Recent";
			this.toolStripMenuItemRecent.Click += new System.EventHandler(this.ToolStripMenuItemRecentItem_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(111, 6);
			// 
			// toolStripMenuItemExit
			// 
			this.toolStripMenuItemExit.Name = "toolStripMenuItemExit";
			this.toolStripMenuItemExit.Size = new System.Drawing.Size(114, 22);
			this.toolStripMenuItemExit.Text = "E&xit";
			this.toolStripMenuItemExit.Click += new System.EventHandler(this.ToolStripMenuItemExit_Click);
			// 
			// openFileDialogSettings
			// 
			this.openFileDialogSettings.Filter = "XML files|*.xml|All files|*.*";
			// 
			// saveFileDialogSettings
			// 
			this.saveFileDialogSettings.DefaultExt = "xml";
			this.saveFileDialogSettings.FileName = "FileCleaner.xml";
			this.saveFileDialogSettings.Filter = "XML files|*.xml|All files|*.*";
			// 
			// statusStripMain
			// 
			this.statusStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelMain});
			this.statusStripMain.Location = new System.Drawing.Point(0, 327);
			this.statusStripMain.Name = "statusStripMain";
			this.statusStripMain.Size = new System.Drawing.Size(421, 22);
			this.statusStripMain.SizingGrip = false;
			this.statusStripMain.TabIndex = 15;
			// 
			// toolStripStatusLabelMain
			// 
			this.toolStripStatusLabelMain.BackColor = System.Drawing.SystemColors.Control;
			this.toolStripStatusLabelMain.Name = "toolStripStatusLabelMain";
			this.toolStripStatusLabelMain.Size = new System.Drawing.Size(48, 17);
			this.toolStripStatusLabelMain.Text = "Ready...";
			this.toolStripStatusLabelMain.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// labelLastUpdated
			// 
			this.labelLastUpdated.AutoSize = true;
			this.labelLastUpdated.Location = new System.Drawing.Point(11, 171);
			this.labelLastUpdated.Name = "labelLastUpdated";
			this.labelLastUpdated.Size = new System.Drawing.Size(74, 13);
			this.labelLastUpdated.TabIndex = 10;
			this.labelLastUpdated.Text = "Last Updated:";
			// 
			// dateTimePickerLastUpdated
			// 
			this.dateTimePickerLastUpdated.CustomFormat = "MMMM dd yyyy  -  hh:mm:ss tt";
			this.dateTimePickerLastUpdated.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerLastUpdated.Location = new System.Drawing.Point(94, 165);
			this.dateTimePickerLastUpdated.Name = "dateTimePickerLastUpdated";
			this.dateTimePickerLastUpdated.Size = new System.Drawing.Size(313, 20);
			this.dateTimePickerLastUpdated.TabIndex = 11;
			// 
			// labelFilter
			// 
			this.labelFilter.AutoSize = true;
			this.labelFilter.Location = new System.Drawing.Point(53, 197);
			this.labelFilter.Name = "labelFilter";
			this.labelFilter.Size = new System.Drawing.Size(32, 13);
			this.labelFilter.TabIndex = 16;
			this.labelFilter.Text = "Filter:";
			// 
			// textBoxFilter
			// 
			this.textBoxFilter.Location = new System.Drawing.Point(94, 194);
			this.textBoxFilter.Name = "textBoxFilter";
			this.textBoxFilter.Size = new System.Drawing.Size(313, 20);
			this.textBoxFilter.TabIndex = 17;
			// 
			// labelXmlFileName
			// 
			this.labelXmlFileName.AutoSize = true;
			this.labelXmlFileName.Location = new System.Drawing.Point(3, 228);
			this.labelXmlFileName.Name = "labelXmlFileName";
			this.labelXmlFileName.Size = new System.Drawing.Size(82, 13);
			this.labelXmlFileName.TabIndex = 18;
			this.labelXmlFileName.Text = "XML File Name:";
			// 
			// textBoxXmlFileName
			// 
			this.textBoxXmlFileName.Location = new System.Drawing.Point(94, 225);
			this.textBoxXmlFileName.Multiline = true;
			this.textBoxXmlFileName.Name = "textBoxXmlFileName";
			this.textBoxXmlFileName.Size = new System.Drawing.Size(274, 45);
			this.textBoxXmlFileName.TabIndex = 19;
			// 
			// buttonXmlFileName
			// 
			this.buttonXmlFileName.Location = new System.Drawing.Point(374, 222);
			this.buttonXmlFileName.Name = "buttonXmlFileName";
			this.buttonXmlFileName.Size = new System.Drawing.Size(33, 23);
			this.buttonXmlFileName.TabIndex = 20;
			this.buttonXmlFileName.Text = "...";
			this.buttonXmlFileName.UseVisualStyleBackColor = true;
			// 
			// FormMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(421, 349);
			this.Controls.Add(this.buttonXmlFileName);
			this.Controls.Add(this.textBoxXmlFileName);
			this.Controls.Add(this.labelXmlFileName);
			this.Controls.Add(this.textBoxFilter);
			this.Controls.Add(this.labelFilter);
			this.Controls.Add(this.dateTimePickerLastUpdated);
			this.Controls.Add(this.labelLastUpdated);
			this.Controls.Add(this.statusStripMain);
			this.Controls.Add(this.menuStripMain);
			this.Controls.Add(this.buttonExit);
			this.Controls.Add(this.buttonCreate);
			this.Controls.Add(this.textBoxSourceName);
			this.Controls.Add(this.labelSourceName);
			this.Controls.Add(this.numericUpDownVersion);
			this.Controls.Add(this.textBoxPublisher);
			this.Controls.Add(this.labelPublisher);
			this.Controls.Add(this.labelVersion);
			this.Controls.Add(this.textBoxAuthor);
			this.Controls.Add(this.labelAuthor);
			this.Controls.Add(this.textBoxPackageName);
			this.Controls.Add(this.labelPackageName);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "FormMain";
			this.Text = "Create Sitecore Update Xml";
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownVersion)).EndInit();
			this.menuStripMain.ResumeLayout(false);
			this.menuStripMain.PerformLayout();
			this.statusStripMain.ResumeLayout(false);
			this.statusStripMain.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label labelPackageName;
		private System.Windows.Forms.TextBox textBoxPackageName;
		private System.Windows.Forms.Label labelAuthor;
		private System.Windows.Forms.TextBox textBoxAuthor;
		private System.Windows.Forms.Label labelVersion;
		private System.Windows.Forms.Label labelPublisher;
		private System.Windows.Forms.TextBox textBoxPublisher;
		private System.Windows.Forms.NumericUpDown numericUpDownVersion;
		private System.Windows.Forms.Label labelSourceName;
		private System.Windows.Forms.TextBox textBoxSourceName;
		private System.Windows.Forms.Button buttonCreate;
		private System.Windows.Forms.Button buttonExit;
		private System.Windows.Forms.MenuStrip menuStripMain;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemFile;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemOpen;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemClose;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSave;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSaveAs;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemRecent;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExit;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogSettings;
		private System.Windows.Forms.OpenFileDialog openFileDialogSettings;
		private System.Windows.Forms.SaveFileDialog saveFileDialogSettings;
		private System.Windows.Forms.StatusStrip statusStripMain;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelMain;
		private System.Windows.Forms.Label labelLastUpdated;
		private System.Windows.Forms.DateTimePicker dateTimePickerLastUpdated;
		private System.Windows.Forms.Label labelFilter;
		private System.Windows.Forms.TextBox textBoxFilter;
		private System.Windows.Forms.Label labelXmlFileName;
		private System.Windows.Forms.TextBox textBoxXmlFileName;
		private System.Windows.Forms.Button buttonXmlFileName;
	}
}

