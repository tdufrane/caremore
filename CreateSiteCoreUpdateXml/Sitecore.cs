﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Xml;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace CreateSiteCoreUpdateXml
{
	public class Sitecore
	{
		public string SqlServer { get; set; }
		public string Database { get; set; }

		public void Create(UpdateSetting setting)
		{
			string connection = string.Format(ConfigurationManager.ConnectionStrings["Sitecore"].ConnectionString,
				SqlServer, Database);

			SqlDatabase database = new SqlDatabase(connection);
			DataSet data = database.ExecuteDataSet(CommandType.Text,
				string.Format(Properties.Resources.SqlQuery, setting.LastUpdated));

			if ((data.Tables.Count > 0) && (data.Tables[0].Rows.Count > 0))
			{
				Assembly assembly = Assembly.GetExecutingAssembly();

				XmlDocument document = new XmlDocument();
				document.Load(assembly.GetManifestResourceStream("CreateSiteCoreUpdateXml.Template.xml"));

				CreateXml(document, data.Tables[0], setting);
			}
		}

		private void CreateXml(XmlDocument document, DataTable items, UpdateSetting setting)
		{
			XmlNode metaDataNode = document.SelectSingleNode("/project/Metadata/metadata");

			XmlNode itemNode = metaDataNode.SelectSingleNode("./PackageName");
			itemNode.InnerText = setting.PackageName;

			itemNode = metaDataNode.SelectSingleNode("./Author");
			itemNode.InnerText = setting.Author;

			itemNode = metaDataNode.SelectSingleNode("./Version");
			itemNode.InnerText = setting.Version.ToString();

			itemNode = metaDataNode.SelectSingleNode("./Publisher");
			itemNode.InnerText = setting.Publisher;

			XmlNode nameNode = metaDataNode.SelectSingleNode("/project/Sources/xitems/Name");
			nameNode.InnerText = setting.SourceName;

			XmlNode entriesNode = metaDataNode.SelectSingleNode("/project/Sources/xitems/Entries");

			XmlNode xitemNode;

			foreach (DataRow row in items.Rows)
			{
				bool useItem = true;
				string itemPath = row["ItemPath"].ToString();

				if (!string.IsNullOrWhiteSpace(setting.Filter))
				{
					useItem = (itemPath.IndexOf(setting.Filter, StringComparison.InvariantCultureIgnoreCase) > -1);
				}

				if (useItem)
				{
					xitemNode = document.CreateNode(XmlNodeType.Element, "x-item", null);
					xitemNode.InnerText = string.Format("/master/{0}/{{{1}}}/invariant/0", itemPath, row["ID"]);
					entriesNode.AppendChild(xitemNode);
				}
			}

			document.Save(setting.XmlFileName);
		}
	}
}

/*
select '        <x-item>/master/' + ItemPath + '/{' + cast(ID as varchar(60)) + '}/invariant/0</x-item>' as XmlItem
from
(
select i.ID, ItemPath =
isnull(p8.Name + '/', '') + 
isnull(p7.Name + '/', '') + 
isnull(p6.Name + '/', '') + 
isnull(p5.Name + '/', '') + 
isnull(p4.Name + '/', '') + 
isnull(p3.Name + '/', '') + 
isnull(p2.Name + '/', '') + 
isnull(p1.Name + '/', '') + 
i.Name
from Items i
left join Items p1 on p1.ID = i.ParentID
left join Items p2 on p2.ID = p1.ParentID
left join Items p3 on p3.ID = p2.ParentID
left join Items p4 on p4.ID = p3.ParentID
left join Items p5 on p5.ID = p4.ParentID
left join Items p6 on p6.ID = p5.ParentID
left join Items p7 on p7.ID = p6.ParentID
left join Items p8 on p8.ID = p7.ParentID
where i.Updated between '{0}' and getdate()
order by ItemPath
*/
