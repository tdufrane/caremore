﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace CreateSiteCoreUpdateXml
{
	public class UpdateSetting
	{
		#region Constructors

		public UpdateSetting()
		{
		}

		#endregion

		#region Properties

		[XmlIgnore()]
		public string Errors { get; private set; }

		public string Author { get; set; }

		[XmlIgnore()]
		public string FileName { get; set; }

		public string Filter { get; set; }
		public DateTime LastUpdated { get; set; }
		public string PackageName { get; set; }
		public string SourceName { get; set; }
		public string Publisher { get; set; }
		public int Version { get; set; }
		public string XmlFileName { get; set; }

		#endregion

		#region Public methods
		#endregion

		#region XML Serialization Methods

		public void Clear()
		{
			this.Author = string.Empty;
			this.Filter = string.Empty;
			this.LastUpdated = DateTime.Now;
			this.PackageName = string.Empty;
			this.SourceName = string.Empty;
			this.Publisher = string.Empty;
			this.Version = 1;
			this.XmlFileName = string.Empty;
		}

		public void Load()
		{
			if (string.IsNullOrEmpty(this.FileName))
				Load(this.FileName);
		}

		public void Load(string fileName)
		{
			this.FileName = fileName;
			TextReader reader = null;

			try
			{
				reader = new StreamReader(fileName);

				XmlRootAttribute rootAttribute = new XmlRootAttribute();
				rootAttribute.ElementName = "UpdateSetting";

				XmlSerializer serial = new XmlSerializer(typeof(UpdateSetting), rootAttribute);
				UpdateSetting setting = (UpdateSetting)serial.Deserialize(reader);

				this.Author = setting.Author;
				this.Filter = setting.Filter;
				this.LastUpdated = setting.LastUpdated;
				this.PackageName = setting.PackageName;
				this.SourceName = setting.SourceName;
				this.Publisher = setting.Publisher;
				this.Version = setting.Version;
				this.XmlFileName = setting.XmlFileName;
			}
			finally
			{
				if (reader != null)
					reader.Close();
			}
		}

		public void Save()
		{
			if (!string.IsNullOrEmpty(this.FileName))
				Save(this.FileName);
		}

		public void Save(string fileName)
		{
			this.FileName = fileName;
			XmlTextWriter writer = null;

			try
			{
				writer = new XmlTextWriter(fileName, System.Text.Encoding.ASCII);
				writer.Formatting = Formatting.Indented;
				writer.Indentation = 1;
				writer.IndentChar = '\x09';

				XmlRootAttribute rootAttribute = new XmlRootAttribute();
				rootAttribute.ElementName = "UpdateSetting";

				XmlSerializer xmlSerial = new XmlSerializer(typeof(UpdateSetting), rootAttribute);
				xmlSerial.Serialize(writer, this);
			}
			finally
			{
				if (writer != null)
					writer.Close();
			}
		}

		#endregion

	}
}
