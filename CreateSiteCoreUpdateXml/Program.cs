﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace CreateSiteCoreUpdateXml
{
	public static class Program
	{
		public static string MessageTitle = "Create Sitecore Update XML - ";

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new FormMain());
		}
	}
}
