﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CreateSiteCoreUpdateXml
{
	public partial class FormMain : Form
	{
		#region Private variables

		private bool changesPending = false;
		private string lastXmlDirectory = string.Empty;
		private List<string> recentFiles = new List<string>();
		private UpdateSetting currentSetting = new UpdateSetting();
		private string lastLogFileName = string.Empty;

		#endregion

		#region Form events

		public FormMain()
		{
			try
			{
				InitializeComponent();

				// Get recent setting files
				CheckAndAddRecent(Properties.Settings.Default.Recent1);
				CheckAndAddRecent(Properties.Settings.Default.Recent2);
				CheckAndAddRecent(Properties.Settings.Default.Recent3);
				CheckAndAddRecent(Properties.Settings.Default.Recent4);
				CheckAndAddRecent(Properties.Settings.Default.Recent5);
				CheckAndAddRecent(Properties.Settings.Default.Recent6);
				CheckAndAddRecent(Properties.Settings.Default.Recent7);
				CheckAndAddRecent(Properties.Settings.Default.Recent8);

				ToggleRecent();
			}
			catch (Exception ex)
			{
				ShowException(ex);
				Application.Exit();
			}
		}

		private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
		{
			try
			{
				// Check if changes pending
				DialogResult result = SavePending();

				// Close the settings file
				if (result == DialogResult.Cancel)
				{
					e.Cancel = true;
				}
				else
				{
					SaveRecent();
				}
			}
			catch (Exception ex)
			{
				ShowException(ex);
			}
		}

		private void ButtonExit_Click(object sender, EventArgs e)
		{
			try
			{
				this.Close();
			}
			catch (Exception ex)
			{
				ShowException(ex);
			}
		}

		private void ButtonCreate_Click(object sender, EventArgs e)
		{
			try
			{
				Sitecore sitecore = new Sitecore();
				sitecore.SqlServer = "SQLCLUST010\\SQL2K8PRD1";
				sitecore.Database = "CaremoreMaster";

				SaveSettings();
				sitecore.Create(this.currentSetting);

				this.toolStripStatusLabelMain.Text = "Update XML File created...";
			}
			catch (Exception ex)
			{
				ShowException(ex);
			}
		}

		private void ToolStripMenuItemClose_Click(object sender, EventArgs e)
		{
			try
			{
				// Check if changes pending
				DialogResult result = SavePending();

				// Close the settings file
				if (result != DialogResult.Cancel)
				{
					this.toolStripStatusLabelMain.Text = "File closed...";

					// Clear out current settings
					this.currentSetting.FileName = string.Empty;
					this.currentSetting.Clear();

					// Refresh the UI
					LoadSetting();
				}
			}
			catch (Exception ex)
			{
				ShowException(ex);
			}
		}

		private void ToolStripMenuItemExit_Click(object sender, EventArgs e)
		{
			try
			{
				this.Close();
			}
			catch (Exception ex)
			{
				ShowException(ex);
			}
		}

		private void ToolStripMenuItemOpen_Click(object sender, EventArgs e)
		{
			try
			{
				// Check if changes pending
				DialogResult result = SavePending();

				if (result != DialogResult.Cancel)
				{
					// Open a settings file
					if (!string.IsNullOrEmpty(this.lastXmlDirectory))
						this.openFileDialogSettings.InitialDirectory = this.lastXmlDirectory;

					result = this.openFileDialogSettings.ShowDialog();

					if (result == DialogResult.OK)
					{
						// Clear out current settings
						this.currentSetting.Clear();

						// Load the new settings from the file
						this.currentSetting.Load(this.openFileDialogSettings.FileName);

						// Refresh the UI
						LoadSetting();

						// Add to recent
						SetRecent(this.openFileDialogSettings.FileName);

						this.toolStripStatusLabelMain.Text = Properties.Resources.ReadyStatus;
					}
				}
			}
			catch (Exception ex)
			{
				ShowException(ex);
			}
		}

		private void ToolStripMenuItemRecentItem_Click(object sender, EventArgs e)
		{
			try
			{
				ToolStripMenuItem menuItem = ((ToolStripMenuItem)sender);

				if (File.Exists(menuItem.ToolTipText))
				{
					// Check if changes pending
					DialogResult result = SavePending();

					if (result != DialogResult.Cancel)
					{
						// Clear out current settings
						this.currentSetting.Clear();

						// Load the settings fromt the recent file
						this.currentSetting.Load(menuItem.ToolTipText);

						// Refresh the UI
						LoadSetting();

						this.toolStripStatusLabelMain.Text = Properties.Resources.ReadyStatus;
					}
				}
				else
				{
					DialogResult result = MessageBox.Show(string.Format("'{0}' does not exist.  Remove from recent list?", menuItem.Text.Substring(9)),
						Program.MessageTitle + "Warning",
						MessageBoxButtons.YesNo,
						MessageBoxIcon.Warning);

					if (result == DialogResult.Yes)
					{
						this.recentFiles.RemoveAt(int.Parse(menuItem.Text.Substring(1, 1)) - 1);
						ToggleRecent();
					}
				}
			}
			catch (Exception ex)
			{
				ShowException(ex);
			}
		}

		private void ToolStripMenuItemSave_Click(object sender, EventArgs e)
		{
			try
			{
				SaveSettings(this.currentSetting.FileName);
				this.toolStripStatusLabelMain.Text = "File saved...";
			}
			catch (Exception ex)
			{
				ShowException(ex);
			}
		}

		private void ToolStripMenuItemSaveAs_Click(object sender, EventArgs e)
		{
			try
			{
				SaveSettings(string.Empty);
				this.toolStripStatusLabelMain.Text = "File saved as...";
			}
			catch (Exception ex)
			{
				ShowException(ex);
			}
		}

		#endregion

		#region Private methods

		private void CheckAndAddRecent(string fileName)
		{
			if (!string.IsNullOrEmpty(fileName))
				this.recentFiles.Add(fileName);
		}

		private void LoadSetting()
		{
			this.textBoxAuthor.Text = this.currentSetting.Author;
			this.textBoxFilter.Text = this.currentSetting.Filter;
			this.dateTimePickerLastUpdated.Value = this.currentSetting.LastUpdated;
			this.textBoxPackageName.Text = this.currentSetting.PackageName;
			this.textBoxPublisher.Text = this.currentSetting.Publisher;
			this.textBoxSourceName.Text = this.currentSetting.SourceName;
			this.textBoxXmlFileName.Text = this.currentSetting.XmlFileName;

			if ((this.currentSetting.Version > 0) && (this.currentSetting.Version < 10000))
				this.numericUpDownVersion.Value = this.currentSetting.Version;
			else
				this.numericUpDownVersion.Value = 1;

			this.dateTimePickerLastUpdated.Value = this.currentSetting.LastUpdated;
		}

		private DialogResult SavePending()
		{
			DialogResult result = DialogResult.No;

			// Check if changes
			if (this.changesPending)
			{
				// Ask if changes not saved:  Yes / No / Cancel
				result = MessageBox.Show("Changes are pending, do you want to save?",
					Program.MessageTitle + " Question",
					MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
			}

			if (result == DialogResult.No)
			{
				// Clear changes flag
				this.changesPending = false;
			}
			else if (result == DialogResult.Yes)
			{
				result = SaveSettings(this.currentSetting.FileName);
			}

			return result;
		}

		private void SaveRecent()
		{
			if (this.recentFiles.Count > 0)
				Properties.Settings.Default.Recent1 = this.recentFiles[0];

			if (this.recentFiles.Count > 1)
				Properties.Settings.Default.Recent2 = this.recentFiles[1];

			if (this.recentFiles.Count > 2)
				Properties.Settings.Default.Recent3 = this.recentFiles[2];

			if (this.recentFiles.Count > 3)
				Properties.Settings.Default.Recent4 = this.recentFiles[3];

			if (this.recentFiles.Count > 4)
				Properties.Settings.Default.Recent5 = this.recentFiles[4];

			if (this.recentFiles.Count > 5)
				Properties.Settings.Default.Recent6 = this.recentFiles[5];

			if (this.recentFiles.Count > 6)
				Properties.Settings.Default.Recent7 = this.recentFiles[6];

			if (this.recentFiles.Count > 7)
				Properties.Settings.Default.Recent8 = this.recentFiles[7];

			Properties.Settings.Default.Save();
		}

		private void SetRecent(string fileName)
		{
			// Set the recent file list
			int recentIndex = this.recentFiles.IndexOf(fileName);
			if (recentIndex > 0)
			{
				// Move to top
				this.recentFiles.RemoveAt(recentIndex);
				this.recentFiles.Insert(0, fileName);
			}
			else if (recentIndex == -1)
			{
				// Add to top
				this.recentFiles.Insert(0, fileName);
			}
			//else is at top, no need to do anything

			// If have move than 8 in the list remove the last one
			if (this.recentFiles.Count > 8)
				this.recentFiles.RemoveAt(this.recentFiles.Count - 1);

			// Reset the recent files menu item
			ToggleRecent();
		}

		private void SaveSettings()
		{
			this.currentSetting.Author = this.textBoxAuthor.Text;
			this.currentSetting.Filter = this.textBoxFilter.Text;
			this.currentSetting.LastUpdated = this.dateTimePickerLastUpdated.Value;
			this.currentSetting.PackageName = this.textBoxPackageName.Text;
			this.currentSetting.Publisher = this.textBoxPublisher.Text;
			this.currentSetting.SourceName = this.textBoxSourceName.Text;
			this.currentSetting.Version = int.Parse(this.numericUpDownVersion.Value.ToString());
			this.currentSetting.XmlFileName = this.textBoxXmlFileName.Text;
		}

		private DialogResult SaveSettings(string fileName)
		{
			DialogResult result = DialogResult.OK;

			// Check to see if saved before
			if (string.IsNullOrEmpty(fileName))
			{
				result = SetSettingFileName(fileName);
			}

			// Verify file name was set
			if (result == DialogResult.OK)
			{
				SaveSettings();

				// Save the settings file
				this.currentSetting.Save();

				// Clear changes flag
				this.changesPending = false;

				// Add to recent
				SetRecent(this.currentSetting.FileName);
			}

			return result;
		}

		private DialogResult SetSettingFileName(string fileName)
		{
			if (string.IsNullOrEmpty(fileName))
				this.saveFileDialogSettings.FileName = fileName;
			else
				this.saveFileDialogSettings.FileName = "SitecoreUpdate.xml";

			if (!string.IsNullOrEmpty(this.currentSetting.FileName))
			{
				int lastSlashIndex = this.currentSetting.FileName.LastIndexOf('\\');
				if (lastSlashIndex > -1)
				{
					this.saveFileDialogSettings.InitialDirectory = this.currentSetting.FileName.Substring(0, lastSlashIndex);
				}
			}

			DialogResult result = this.saveFileDialogSettings.ShowDialog();

			if (result == DialogResult.OK)
			{
				this.currentSetting.FileName = this.saveFileDialogSettings.FileName;
			}

			return result;
		}

		private void ShowException(Exception ex)
		{
			this.toolStripStatusLabelMain.Text = "Error" + ex.Message;

			DialogResult result = MessageBox.Show(ex.Message + "\nClick OK for complete error, otherwise click Cancel.",
				Program.MessageTitle + "Error",
				MessageBoxButtons.OKCancel,
				MessageBoxIcon.Error);

			if (result == DialogResult.OK)
			{
				MessageBox.Show(ex.ToString().Replace(" at", "\nat"),
					Program.MessageTitle + "Error",
					MessageBoxButtons.OK,
					MessageBoxIcon.Error);
			}
		}

		private void ToggleRecent()
		{
			if (this.recentFiles.Count == 0)
			{
				this.toolStripMenuItemRecent.Visible = false;
				this.toolStripSeparator2.Visible = false;
			}
			else
			{
				this.toolStripMenuItemRecent.Visible = true;
				this.toolStripMenuItemRecent.DropDownItems.Clear();

				int index = 1;
				foreach (string fileName in this.recentFiles)
				{
					ToolStripMenuItem item = new ToolStripMenuItem();

					int lastSlashPos = fileName.LastIndexOf('\\');
					if (lastSlashPos == 0)
						item.Text = string.Format("&{0}.  {1}", index, fileName);
					else
						item.Text = string.Format("&{0}.  ...{1}", index, fileName.Substring(lastSlashPos));

					item.ToolTipText = fileName;
					item.DisplayStyle = ToolStripItemDisplayStyle.Text;
					item.Click += new EventHandler(ToolStripMenuItemRecentItem_Click);

					this.toolStripMenuItemRecent.DropDownItems.Add(item);

					index++;
				}

				this.toolStripSeparator2.Visible = true;
			}
		}

		#endregion
	}
}
