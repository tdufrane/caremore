using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using RazorGenerator.Mvc;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(Duals.SCExtensions.RazorGeneratorMvcStart), "Start")]

namespace Duals.SCExtensions {
    public static class RazorGeneratorMvcStart {
        public static void Start() {
            var engine = new PrecompiledMvcEngine(typeof(RazorGeneratorMvcStart).Assembly) {
                //UsePhysicalViewsIfNewer = HttpContext.Current.Request.IsLocal
                
            };
            //TODO: FIX HttpContext ERROR
            ViewEngines.Engines.Insert(0, engine);

            // StartPage lookups are done by WebPages. 
            VirtualPathFactoryManager.RegisterVirtualPathFactory(engine);
        }
    }
}
