// JavaScript Document

function validateForm()
{	

	console.log("form submitted");

	var fName=document.forms["contactForm"]["FirstName"].value;
	if (fName==null || fName=="" || fName=="First Name")
  	{
  		alert("First name must be filled out");
  		return false;
  	}
  	
  	var lName=document.forms["contactForm"]["LastName"].value;
	if (lName==null || lName=="" || lName=="Last Name")
  	{
  		alert("Last name must be filled out");
  		return false;
  	}
  	
  	var cName=document.forms["contactForm"]["Company"].value;
	if (cName==null || cName=="" || cName=="Company")
  	{
  		alert("Company must be filled out");
  		return false;
  	}
  	
  	var pName=document.forms["contactForm"]["Phone"].value;
	if (pName==null || pName=="" || pName=="Phone")
  	{
  		alert("Phone number must be filled out");
  		return false;
  	}
  	
  	var eName=document.forms["contactForm"]["Email"].value;
	if (eName==null || eName=="" || eName=="Email")
  	{
  		alert("Email address must be filled out");
  		return false;
  	}
}