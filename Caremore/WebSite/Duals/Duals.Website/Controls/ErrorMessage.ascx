﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ErrorMessage.ascx.cs" Inherits="Duals.Website.Controls.ErrorMessage" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<asp:PlaceHolder ID="MessagePH" runat="server">
	<div><asp:Label ID="MessageLbl" runat="server" /></div>
	<sc:Text Field="Text" ID="ProviderContact" runat="server" />
</asp:PlaceHolder>
