﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Settings;

namespace Duals.Website.Controls
{
	public class BaseSublayout : System.Web.UI.UserControl
	{
		/// <summary>
		/// Gets assigned DataSource
		/// </summary>
		private Item _dataSource = null;
		public Item DataSource
		{
			get
			{
				if (_dataSource == null)
					if (Parent is Sublayout)
						_dataSource = Sitecore.Context.Database.GetItem(((Sublayout)Parent).DataSource);

				return _dataSource;
			}
		}

		public BaseSublayout() : base() { }

		/// <summary>
		/// Get SiteSettings
		/// </summary>
		public SiteSettings SiteSettings
		{
			get
			{
				return Sitecore.Context.Database.GetItem(GlobalConsts.SiteSettingsID).ToClass<SiteSettings>();
			}
		}
	}
}