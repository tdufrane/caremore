﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Pages;

using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;

namespace Duals.Website.Controls
{
	public partial class BaseContentPage : System.Web.UI.UserControl
	{
		public string ColumnSpanCssClass { get; set;}

		protected void Page_Load(object sender, EventArgs e)
		{
			SetContent();
		}

		private void SetContent()
		{
			if (!string.IsNullOrEmpty(ColumnSpanCssClass))
				pnlContent.CssClass = ColumnSpanCssClass;

			// get current ContentPage
			ContentPage contentPage = Sitecore.Context.Item.ToClass<ContentPage>();

			// if contentPage == null log the error and do nothing else
			if (contentPage == null)
			{
				Sitecore.Diagnostics.Log.Error("Duals: Cannot load ContentPage", this);
			}
			else
			{
				// masthead image url
				string imgUrl = contentPage.MastheadImage.MediaItem != null ? MediaManager.GetMediaUrl(contentPage.MastheadImage.MediaItem) : null;

				// show PH with or without image depending if imgUrl is set
				phWithImage.Visible = !string.IsNullOrEmpty(imgUrl);
				phWithoutImage.Visible = string.IsNullOrEmpty(imgUrl);

				Item item = Sitecore.Context.Item;
				scImage.Item = item;
				scImage.Field = "Masthead Image";
			}
		}
	}
}