﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Sitecore;
using Sitecore.Data.Items;

namespace Duals.Website.Controls
{
	public partial class ErrorMessage : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack)
			{
				// Make sure previous error is hidden
				MessageLbl.Visible = false;
			}
			else
			{
				// Display contact information from wording
				ProviderContact.Item = Sitecore.Context.Database.GetItem("/sitecore/content/Global/Wording/FindADoctorContact");
			}
		}

		public void SetZipcodeCoveredMessage()
		{
			DisplayError(MessagePH, MessageLbl,
				"Congratulations! You are eligible to join.");
		}

		public void SetNotZipcodeCoveredMessage()
		{
			DisplayError(MessagePH, MessageLbl,
				"We're sorry! Your zip code is not within our service area.");
		}

		public void SetNotZipcodeCoveredMessage(Control ctl)
		{
			DisplayError(MessagePH, MessageLbl,
				"We're sorry! Your zip code is not within our service area.",
				ctl);
		}

		public void SetNoResultMessage()
		{
			DisplayError(MessagePH, MessageLbl,
				"No results have been found which match your search criteria, please try again.");
		}

		public void SetNoResultMessage(Control ctl)
		{
			DisplayError(MessagePH, MessageLbl,
				"No results have been found which match your search criteria, please try again.",
				ctl);
		}

		public void SetDownMessage(Exception ex)
		{
			Item message = Sitecore.Context.Database.GetItem("{918B4F8A-C263-423D-B10F-0E2E7119330E}");
			DisplayError(MessagePH, MessageLbl,
				message["Text"]);
			EmailException(ex);
		}

		public void SetDownMessage(Exception ex, Control ctl)
		{
			Item message = Sitecore.Context.Database.GetItem("{918B4F8A-C263-423D-B10F-0E2E7119330E}");
			DisplayError(MessagePH, MessageLbl,
				message["Text"], ctl);
			EmailException(ex);
		}

		public void SetError(Exception ex)
		{
			StringBuilder builderError = new StringBuilder();
#if DEBUG
			builderError.AppendLine("<pre>");
#else
			Sitecore.Diagnostics.Log.Error("Error: ", ex, HttpContext.Current.Handler);
			Item errorItem = Sitecore.Context.Database.GetItem("/sitecore/content/Global/Wording/Error");
			builderError.AppendLine(errorItem["Text"]);
			builderError.AppendLine("<!-- ");
#endif
			if (ex == null)  //CA1062
			{
				builderError.AppendLine("No exception was found (exception == null).");
			}
			else
			{
				builderError.AppendLine(ex.ToString());
			}
#if DEBUG
			builderError.AppendLine("</pre>");
#else
			builderError.AppendLine(ex.ToString().Replace("<!--", string.Empty).Replace("-->", "\n"));
			builderError.Replace("   at ", "\nat ");
			builderError.AppendLine(" -->");
#endif
			DisplayError(MessagePH, MessageLbl, builderError.ToString());
		}

		private void DisplayError(PlaceHolder control, string message)
		{
			DisplayError(control, message, "p");
		}

		private void DisplayError(PlaceHolder control, string message, string tag)
		{
			HtmlGenericControl paragraph = new HtmlGenericControl(tag);
			paragraph.Attributes.Add("class", "error");
			paragraph.InnerHtml = message;
			control.Controls.Add(paragraph);
			control.Visible = true;
		}

		private void DisplayError(PlaceHolder container, Label display, string message)
		{
			container.Visible = true;

			display.CssClass = "error";
			display.Text = message;
			display.Visible = true;
		}

		private void DisplayError(PlaceHolder container, Label display, string message, Control hide)
		{
			DisplayError(container, display, message);
			hide.Visible = false;
		}

		private void EmailException(Exception ex)
		{
			if (ex != null)
			{
				if (ConfigurationManager.AppSettings["ExceptionEmailTo"] != null)
				{
					try
					{
						MailMessage message = new MailMessage();
						message.To.Add(ConfigurationManager.AppSettings["ExceptionEmailTo"]);
						message.Subject = "CareMore.com - Exception";
						message.Body = (ex.ToString());

						Sitecore.MainUtil.SendMail(message);
					}
					catch
					{
						// Do nothing
					}
				}
			}
		}
	}
}