﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Recipes.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.Recipes" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="span8">
	<asp:ListView id="lvRecipeFolders" runat="server" OnItemDataBound="LvRecipeGroups_ItemDataBound">
		<LayoutTemplate>
			<div class="accordion" id="recipe-accordion">
				<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
			</div>
			<script type="text/javascript">
				function toggleChevron(e) {
					$j(e.target)
						.prev('.accordion-heading')
						.find('a')
						.toggleClass('glyphicon-chevron-right glyphicon-chevron-down');
				}
				$j('.accordion').on('hidden.bs.collapse', toggleChevron);
				$j('.accordion').on('shown.bs.collapse', toggleChevron);
			</script>
		</LayoutTemplate>
		<ItemTemplate>
			<div class="accordion-group">
				<div class="accordion-heading">
					<h3><asp:HyperLink ID="hlRecipeGroup" runat="server" CssClass="accordion-toggle collapsed" data-toggle="collapse"
						data-parent="#recipe-accordion"><sc:Text ID="txtRecipeGroup" runat="server" Field="Recipes Group Title" /></asp:HyperLink></h3>
				</div>
				<asp:Panel ID="pnlAccordionBody" runat="server" CssClass="accordion-body collapse">
					<asp:ListView ID="lvRecipes" runat="server" OnItemDataBound="LvRecipes_ItemDataBound">
						<LayoutTemplate>
							<div class="accordion-inner">
								<asp:PlaceHolder runat="server" ID="itemPlaceHolder"></asp:PlaceHolder>
							</div>
						</LayoutTemplate>
						<ItemTemplate>
							<p><sc:Link ID="linkEnglishPdf" runat="server" Field="Recipe English PDF" Target="_blank"><sc:Text ID="txtEnglishTitle" runat="server" Field="Recipe English Title" /></sc:Link> &ndash;
								<sc:Text ID="txtEnglishDescription" runat="server" Field="Recipe English Description" /><br />
							<sc:Link ID="linkSpanishPdf" runat="server" Field="Recipe Spanish PDF" Target="_blank"><sc:Text ID="txtSpanishTitle" runat="server" Field="Recipe Spanish Title" /></sc:Link> &ndash;
								<sc:Text ID="txtSpanishDescription" runat="server" Field="Recipe Spanish Description" /></p>
						</ItemTemplate>
					</asp:ListView>
				</asp:Panel>
			</div>
		</ItemTemplate>
	</asp:ListView>
</div>

<script src="/js/accordion.js"></script>
