﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Pages;

using Sitecore;
using Sitecore.Links;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;
using Sitecore.Web.UI.WebControls;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class SideRowBox : Controls.BaseSublayout
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (DataSource != null)
			{
				scTitle.Item = DataSource;
				scImage.Item = DataSource;
				scContent.Item = DataSource;

				BuildLinks();
			}
			else
			{
				// DataSource NOT set
				Sitecore.Diagnostics.Log.Error("Duals: DataSource NOT set on SideBoxRow", this);
			}
		}

		private void BuildLinks()
		{
			try
			{
				linksLV.DataSource = this.DataSource.Children.Where(item => item.TemplateName == "Side Row Box Link");
				linksLV.DataBind();
			}
			catch (Exception exc)
			{
				Sitecore.Diagnostics.Log.Error("Duals: Cannot build SideRowBox Links", exc, this);
			}
		}
	}
}