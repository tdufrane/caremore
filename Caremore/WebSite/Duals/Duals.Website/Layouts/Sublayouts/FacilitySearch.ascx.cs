﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CareMore.Web.DotCom.Models.Providers;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Content.Articles;
using Duals.Models.Pages;
using Duals.Website.Controls;

using Sitecore.Data.Items;
using Sitecore.Globalization;


namespace Duals.Website.Layouts.Sublayouts
{
	public partial class FacilitySearch : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				btnSubmit.Text = Translate.Text("Search");
				btnSubmit.ToolTip = Translate.Text("Search");

				ContentPage contentPage = Sitecore.Context.Item.ToClass<ContentPage>();
				if (contentPage != null) btnSubmit.ToolTip = contentPage.MastheadTitle;

				Item resultsPage = Sitecore.Context.Item.GetChildren().Where(i => i.Name == "Results").FirstOrDefault();
				if (resultsPage != null)
				{
					ProviderListArticle results = resultsPage.ToClass<ProviderListArticle>();
					ProviderListing listing = new ProviderListing(results.ProviderListType);

					if (listing.HideSearchForm.HasValue && listing.HideSearchForm.Value)
					{
						Session["SearchResults"] = null;
						Session["DataPagerStartRowIndex"] = null;
						Response.Redirect(Sitecore.Links.LinkManager.GetItemUrl(resultsPage));
					}

					if ((listing.ShowSubtypeFilter.HasValue) && (listing.ShowSubtypeFilter.Value))
					{
						divProviderType.Visible = true;

						List<Item> subtypes = listing.SubtypeFilter.ToList();
						ddlProviderType.Items.Add(new ListItem() { Text = "All", Value = "" });
						foreach (Item subtype in subtypes)
						{
							ddlProviderType.Items.Add(new ListItem() { Text = subtype["Name"], Value = subtype["Code"] });
						}
					}
					else
					{
						divProviderType.Visible = false;
					}
				}
			}
			catch (Exception ex)
			{
				ErrorMessage errorMessageCtl = (ErrorMessage)errorMessage;
				errorMessageCtl.SetDownMessage(ex);
				errorMessage.Visible = true;
			}
		}

		protected void btnSubmit_OnClick(object sender, EventArgs e)
		{
			if (Page.IsValid)
			{
				Item resultsPage = Sitecore.Context.Item.GetChildren().Where(i => i.Name == "Results").FirstOrDefault();

				StringBuilder url = new StringBuilder();
				url.Append(Sitecore.Links.LinkManager.GetItemUrl(resultsPage));
				url.Append("?");

				if (txtName.Text.Length > 0)
					url.AppendFormat("name={0}&", HttpUtility.UrlEncode(txtName.Text));

				if (txtZipCode.Text.Length > 0)
					url.AppendFormat("zip={0}&", txtZipCode.Text);

				if (txtCity.Text.Length > 0)
					url.AppendFormat("city={0}&", txtCity.Text);

				if (!string.IsNullOrEmpty(ddlProviderType.SelectedValue))
					url.AppendFormat("type={0}&", ddlProviderType.SelectedValue);

				Session["SearchResults"] = null;
				Session["DataPagerStartRowIndex"] = null;

				string redirectUrl = url.ToString().TrimEnd(new char[] { '?', '&' });
				Response.Redirect(redirectUrl);
			}
		}
	}
}
