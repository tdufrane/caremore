﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;
using Sitecore.Resources.Media;

using ClassySC.Data;

using Duals.Models.Pages;
using Duals.Models.Widgets;

namespace Duals.Website.Layouts.Sublayouts
{
    public partial class MemberStories : Controls.BaseSublayout
    {
        private ID _memberStoriesID = new ID("{24C8B7AB-3F4F-45A0-8700-97376953E4ED}");

        #region Properties

        public string FeatureVideoJs
        {
            get
            {
                string jsFormat =
                    "<script type=\"text/javascript\">" +
                    "playVideo(\"{0}\", \"{1}\", \"{2}\", \"{3}\", \"{4}\");" +
                    "</script>";

                return string.Format(jsFormat,
                    "feature-video",
                    FeatureVideoImagePath,
                    FeatureVideoVideoPath,
                    "420",
                    "246");
            }
        }

        public Item FeatureVideoItem
        {
            get
            {
                // video ID
                ID videoID;
                if (string.IsNullOrEmpty(Request.QueryString["vid"]))
                {
                    // return 1st member story as default
                    return Sitecore.Context.Database.GetItem(_memberStoriesID).Children[0];
                }
                else
                {
                    videoID = new ID(Request.QueryString["vid"]);
                    return Sitecore.Context.Database.GetItem(videoID);
                }
            }
        }

        public string FeatureVideoImagePath
        {
            get
            {
                if (!string.IsNullOrEmpty(FeatureVideoItem.Fields["Video Thumbnail"].Value))
                {
                    Sitecore.Data.Fields.ImageField videoThumb = FeatureVideoItem.Fields["Video Thumbnail"];

                    if (videoThumb == null) return string.Empty;

                    return MediaManager.GetMediaUrl(videoThumb.MediaItem);
                }
                else
                    return string.Empty;
            }
        }

        public string FeatureVideoVideoPath
        {
            get
            {
                if (!string.IsNullOrEmpty(FeatureVideoItem.Fields["Video Link"].Value))
                {
                    Sitecore.Data.Fields.LinkField videoUrl = FeatureVideoItem.Fields["Video Link"];

                    if (videoUrl == null) return string.Empty;
                    
                    MediaItem mediaItem = new MediaItem(videoUrl.TargetItem);

                    return "/" + MediaManager.GetMediaUrl(mediaItem).Replace(".ashx", "." + mediaItem.Extension);
                }
                else
                    return string.Empty;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ContentPage contentPage = Sitecore.Context.Item.ToClass<ContentPage>();

            var vID = Request.QueryString["vid"];
            Sitecore.Data.ID id;
            MemberStory memberStory;
            //Check to see if the querystring is a valid ID and the item is a descendant of the MemberStory template
            if (Sitecore.Data.ID.TryParse(vID, out id) && (Sitecore.Context.Database.GetItem<StandardTemplate>(id) is MemberStory))
                memberStory = Sitecore.Context.Database.GetItem(vID).ToClass<MemberStory>();
            else
                memberStory = contentPage.InnerItem.GetChildren().First().ToClass<MemberStory>();

            featureName.Item = featureDesc.Item = memberStory.InnerItem;

            //Thumbnails list
            var index = 0;
            lvLeftColumn.DataSource = contentPage.InnerItem.Children.ToList()
            .ConvertAll(item => item.ToClass<MemberStory>())
            .Where(story => story != null)
            .OrderBy(i => i.Name).Where(i => index++ % 2 == 0).ToList(); //Get even number index, starting from 0

            index = 0;
            lvRightColumn.DataSource = contentPage.InnerItem.Children.ToList()
            .ConvertAll(item => item.ToClass<MemberStory>())
            .Where(story => story != null)
            .OrderBy(i => i.Name).Where(i => index++ % 2 != 0).ToList(); //Get odd number index
            lvLeftColumn.DataBind();
            lvRightColumn.DataBind();
        }

        protected void lvColumn_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                ListViewDataItem LVDI = (ListViewDataItem)e.Item;
                MemberStory memberStory = (MemberStory)LVDI.DataItem;

                //Link
                HyperLink vidLink1 = (HyperLink)e.Item.FindControl("vidLink1");
                HyperLink vidLink2 = (HyperLink)e.Item.FindControl("vidLink2");
                vidLink1.NavigateUrl = string.Format("{0}?vid={1}", LinkManager.GetItemUrl(Sitecore.Context.Item), memberStory.ID);
                vidLink2.NavigateUrl = string.Format("{0}?vid={1}", LinkManager.GetItemUrl(Sitecore.Context.Item), memberStory.ID);

                //Thumbnail
                Sitecore.Web.UI.WebControls.Image thumbnail = e.Item.FindControl("scImage") as Sitecore.Web.UI.WebControls.Image;
                thumbnail.Field = Video.VideoThumbnail_FID;
                thumbnail.MaxWidth = 117;
                thumbnail.MaxHeight = 72;

                //Text
                Text name = e.Item.FindControl("scName") as Text;
                name.Field = MemberStory.Name_FID;

                Text desc = e.Item.FindControl("scDesc") as Text;
                desc.Field = MemberStory.Description_FID;

                thumbnail.Item = name.Item = desc.Item = memberStory.InnerItem;
            }
        }
    }
}