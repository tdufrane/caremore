﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FacilitySearch.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.FacilitySearch" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="cm" TagName="Error" Src="~/Duals/Duals.Website/Controls/ErrorMessage.ascx" %>

<!-- Begin SubLayouts/FacilitySearch -->
<div class="span8">
	<asp:Panel ID="SearchFormPnl" DefaultButton="btnSubmit" runat="server">
		<div id="searchForProviders">
			<div class="form">
				<div class="formField long">
					<label for="">Facility Name:</label>
					<div class="field">
						<asp:TextBox ID="txtName" runat="server" />
					</div>
				</div>
				<div id="divProviderType" runat="server" class="formField long">
					<label for="">Provider Type:</label>
					<div class="field">
						<asp:DropDownList ID="ddlProviderType" runat="server">
						</asp:DropDownList>
					</div>
				</div>
				<div class="formField long">
					<label for="">City:</label>
					<div class="field">
						<asp:TextBox ID="txtCity" runat="server" />
					</div>
				</div>
				<div class="formField long">
					<label for="">Zip code:</label>
					<div class="field">
						<asp:TextBox ID="txtZipCode" runat="server" />
						<asp:RangeValidator ID="rngValidate_txtZipCode" runat="server"
							ControlToValidate="txtZipCode"
							Display="Dynamic"
							ErrorMessage="Enter a number between 10000 and 99999"
							MinimumValue="10000"
							MaximumValue="99999"
							Type="Integer"
							ValidationGroup="AllValidators">
						</asp:RangeValidator>
					</div>
				</div>
				<div class="formField long">
					<label for="">&nbsp;</label>
					<div class="field">
						<asp:LinkButton ID="btnSubmit" runat="server" OnClick="btnSubmit_OnClick" BackColor="Transparent" BorderStyle="None" CssClass="btn" ValidationGroup="AllValidators" />
					</div>
				</div>
			</div>
		</div>
	</asp:Panel>

	<div><sc:Text ID="SecondaryContentFR" runat="server" Field="Secondary Page Content" /></div>

	<cm:Error ID="errorMessage" runat="server" Visible="false" />
</div>
<!-- End SubLayouts/FacilitySearch -->
