﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using CareMore.Web.DotCom.Models.Providers;
using CareMore.Web.DotCom.Provider;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Content.Articles;
using Duals.Models.Pages;
using Duals.Website.Controls;

using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Links;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class ProviderSearchResults : System.Web.UI.UserControl
	{
		#region Private variables

		private string resultsSessionName = string.Empty;
		private string resultsSessionZip = string.Empty;
		private string detailsBaseUrl = string.Empty;
		private string thisId = string.Empty;
		private string lastId = "0"; // Set so lastId at start <> thisId
		private int idAddressCount = 0;
		private string distanceText = string.Empty;
		private string searchZip = string.Empty;

		private List<ProviderSearchResult> searchResults = null;

		#endregion

		#region Page events

		protected void Page_Load(object sender, EventArgs e)
		{
			resultsSessionName = Sitecore.Context.Item.ID.ToString();
			resultsSessionZip = resultsSessionName + "Zip";
			searchZip = Request.QueryString["zip"];

			Item detailsPage = Sitecore.Context.Item.Parent.GetChildren().Where(i => i.Name == "Details").FirstOrDefault();
			detailsBaseUrl = LinkManager.GetItemUrl(detailsPage);

			NewSearchLB.Text = Translate.Text("New Search");

			litTitle.Text = Sitecore.Context.Item.Parent.ToClass<ContentPage>().MastheadTitle;

			if (IsPostBack)
			{
				errorMessage.Visible = false;
			}
			else
			{
				if (!string.IsNullOrWhiteSpace(searchZip))
				{
					if (!ProviderHelper.IsZipCodeCovered(GlobalConsts.LocalizationFolderID, GlobalConsts.CountyTemplateID, searchZip))
					{
						ErrorMessage errorMessageCtl = (ErrorMessage)errorMessage;
						errorMessageCtl.SetNotZipcodeCoveredMessage(SearchResultsPnl);
						errorMessage.Visible = true;
					}
				}

				ProviderListArticle currentPage = new ProviderListArticle(Sitecore.Context.Item);
				ProviderListing listing = new ProviderListing(currentPage.ProviderListType);

				if (listing.HideSearchForm.HasValue && listing.HideSearchForm.Value)
					NewSearchPH.Visible = false;

				bool useCache = false;
				string cacheFlag = Request.QueryString["c"];

				if (!string.IsNullOrEmpty(cacheFlag) && cacheFlag.Equals("true", StringComparison.OrdinalIgnoreCase))
					useCache = true;

				ShowResults(useCache);
			}
		}

		protected void ItemsLV_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
		{
			ItemsDP.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
			ItemsDP2.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
			Session["DataPagerStartRowIndex"] = e.StartRowIndex;
			ShowResults(true);
		}

		protected void ItemsLV_LayoutCreated(object sender, EventArgs e)
		{
			var currentDB = Sitecore.Context.Database;

			Item WordingColumnPhysicianText = currentDB.SelectSingleItem("/sitecore/content/Global/Wording/ColumnPhysicianText");
			Item WordingColumnSpecialtiesText = currentDB.SelectSingleItem("/sitecore/content/Global/Wording/ColumnSpecialtiesText");
			Item WordingColumnMedicalOfficeText = currentDB.SelectSingleItem("/sitecore/content/Global/Wording/ColumnMedicalOfficeText");
			Item WordingColumnDistanceText = currentDB.SelectSingleItem("/sitecore/content/Global/Wording/ColumnDistanceText");

			Label lblPhysician = (Label)ItemsLV.FindControl("lblPhysician");
			Label lblSpecialties = (Label)ItemsLV.FindControl("lblSpecialties");
			Label lblMedicalOffice = (Label)ItemsLV.FindControl("lblMedicalOffice");

			lblPhysician.Text = WordingColumnPhysicianText.Fields["Text"].Value;
			lblSpecialties.Text = WordingColumnSpecialtiesText.Fields["Text"].Value;
			lblMedicalOffice.Text = WordingColumnMedicalOfficeText.Fields["Text"].Value;
			distanceText = WordingColumnDistanceText.Fields["Text"].Value;
		}

		protected void ItemsLV_DataBound(object sender, EventArgs e)
		{
			var currentDB = Sitecore.Context.Database;

			Item WordingMatchesFoundText = currentDB.SelectSingleItem("/sitecore/content/Global/Wording/MatchesFoundText");

			NumResultsLbl.Text = ItemsDP.TotalRowCount.ToString() + " " + WordingMatchesFoundText.Fields["Text"].Value;
			NumResultsLbl2.Text = NumResultsLbl.Text;

			if (ItemsDP.TotalRowCount <= ItemsDP.PageSize)
			{
				ItemsDP.Visible = false;
				ItemsDP2.Visible = false;
			}
		}

		protected void ItemsLV_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				SetRowDetails(e.Item, (ProviderSearchResult)e.Item.DataItem);
			}
		}

		protected void NewSearchLB_OnClick(object sender, EventArgs e)
		{
			Item searchPage = Sitecore.Context.Item.Parent;
			Response.Redirect(Sitecore.Links.LinkManager.GetItemUrl(searchPage), true);
		}

		#endregion

		#region Private methods

		private void ShowResults(bool useCache)
		{
			try
			{
				if (useCache && Session[resultsSessionName] != null)
				{
					searchResults = (List<ProviderSearchResult>)Session[resultsSessionName];
					searchZip = (string)Session[resultsSessionZip];

					ItemsLV.DataSource = searchResults;
					ItemsLV.DataBind();

					if (Session["DataPagerStartRowIndex"] != null)
					{
						ItemsDP.SetPageProperties((int)Session["DataPagerStartRowIndex"], ItemsDP.MaximumRows, true);
						ItemsDP2.SetPageProperties((int)Session["DataPagerStartRowIndex"], ItemsDP2.MaximumRows, true);
					}
				}
				else
				{
					LoadResults();
				}
			}
			catch (Exception ex)
			{
				ErrorMessage errorMessageCtl = (ErrorMessage)errorMessage;
				errorMessage.Visible = true;
#if DEBUG
				errorMessageCtl.SetError(ex);
#else
				errorMessageCtl.SetDownMessage(ex, SearchResultsPnl);
				NewSearchPH.Visible = false;
#endif
			}
		}

		private void LoadResults()
		{
			ProviderListArticle currentPage = new ProviderListArticle(Sitecore.Context.Item);
			ProviderListing listing = new ProviderListing(currentPage.ProviderListType);

			string specialties = null;
			List<string> specialtyList = new List<string>();
			bool? isPCP = null;

			if (listing.SpecialtyFilter == null || listing.SpecialtyFilter.Count() == 0)
			{
				if (! string.IsNullOrWhiteSpace(Request.QueryString["specialty"]))
				{
					specialtyList = Request.QueryString["specialty"].Split(new char[] { ',' }).ToList();

					// Check to see if PCP flag, remove from list if so as separate filter
					if (specialtyList.Contains("PCP"))
					{
						isPCP = true;
						specialtyList.Remove("PCP");
					}
				}
			}
			else
			{
				foreach (Item specialtyItem in listing.SpecialtyFilter)
				{
					specialtyList.Add(specialtyItem["Value"].TrimEnd());
				}
			}

			// Rebuild the delimited list
			if (specialtyList.Count == 0)
				specialties = string.Empty;
			else
				specialties = string.Join(",", specialtyList.ToArray());

			string lastName = null;
			if (!string.IsNullOrWhiteSpace(Request.QueryString["lastName"]))
				lastName = HttpUtility.UrlDecode(Request.QueryString["lastName"]);

			string zip = null;
			if (!string.IsNullOrWhiteSpace(Request.QueryString["zip"]))
				zip = Request.QueryString["zip"];

			string city = null;
			if (!string.IsNullOrWhiteSpace(Request.QueryString["city"]))
				city = Request.QueryString["city"];

			string languages = null;
			if (!string.IsNullOrWhiteSpace(Request.QueryString["language"]))
				languages = Request.QueryString["language"];

			searchResults = new List<ProviderSearchResult>();

			string dataSource = listing.DataSource.Name;
			if ((dataSource.Equals("Both")) || (dataSource.Equals("Sitecore")))
			{
				List<ProviderSearchResult> sitecoreResults = LoadProvidersSitecore(listing.SitecorePath.TargetItem, lastName, zip, city, isPCP, languages);
				searchResults.AddRange(sitecoreResults);
			}

			if ((dataSource.Equals("Both")) || (dataSource.Equals("Database")))
			{
				List<ProviderSearchResult> databaseResults = LoadProvidersDatabase(lastName, zip, city, isPCP, specialties, languages);
				searchResults.AddRange(databaseResults);
			}

			List<string> providerExclusions = ProviderHelper.GetExclusions(listing.Exclusions);

			foreach (string exclusion in providerExclusions)
			{
				IEnumerable<ProviderSearchResult> filteredList;

				if (exclusion.Contains('='))
				{
					filteredList = searchResults.Where(item => (string.Format("{0}={1}", item.ProviderId, item.AddressType.Trim()) != exclusion));
				}
				else
				{
					filteredList = searchResults.Where(item => (item.ProviderId != exclusion));
				}

				searchResults = filteredList.ToList();
			}

			LoadProviders(zip);
		}

		private List<ProviderSearchResult> LoadProvidersDatabase(string lastName, string zip, string city, bool? isPCP, string specialties, string languages)
		{
			double? maxDistance;
			if (string.IsNullOrWhiteSpace(zip))
				maxDistance = null;
			else
				maxDistance = ProviderHelper.GetMaxZipDistance(GlobalConsts.SiteSettingsID);

			return ProviderFinder.GetProviderList(
				ProviderHelper.GetPortalCode(GlobalConsts.SiteSettingsID),
				lastName, zip, city, null, GlobalConsts.DefaultStateName, isPCP, specialties, languages,
				maxDistance);
		}

		private List<ProviderSearchResult> LoadProvidersSitecore(Item listRoot, string lastName, string zip, string city, bool? isPCP, string languages)
		{
			IEnumerable<Item> list = listRoot.Axes.GetDescendants().Where(i => !i.TemplateID.ToString().Equals(Common.FolderTemplateID));

			if (!string.IsNullOrWhiteSpace(lastName))
			{
				IEnumerable<Item> filteredList = from item in list
				                                 where item["Place"].IndexOf(lastName, StringComparison.OrdinalIgnoreCase) > -1 ||
				                                       item["Provider Name"].IndexOf(lastName, StringComparison.OrdinalIgnoreCase) > -1
				                                 select item;
				list = filteredList;
			}

			if (!string.IsNullOrWhiteSpace(city))
			{
				IEnumerable<Item> filteredList = from item in list
				                                 where item["City"].Equals(city, StringComparison.OrdinalIgnoreCase)
				                                 select item;
				list = filteredList;
			}

			if (isPCP != null && isPCP.Value)
			{
				IEnumerable<Item> filteredList = from item in list
				                                  where item.Fields["Is PCP"].Value.Equals("1")
				                                  select item;
				list = filteredList;
			}

			if (!string.IsNullOrWhiteSpace(languages))
			{
				string[] languageList = languages.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

				IEnumerable<Item> filteredList = from item in list
				                                 where Common.ContainsString(item["Languages"], languageList)
				                                 select item;
				list = filteredList;
			}

			List<ProviderSearchResult> zipResults = new List<ProviderSearchResult>();

			List<ZipCode> zipCodes;
			if (string.IsNullOrWhiteSpace(zip))
				zipCodes = new List<ZipCode>();
			else
				zipCodes = ProviderFinder.GetZipCodes(zip, ProviderHelper.GetMaxZipDistance(GlobalConsts.SiteSettingsID));

			foreach (Item item in list)
			{
				ProviderSearchResult zipResult = ProviderSearchDataContext.ToProviderSearchResult(item);

				if (string.IsNullOrWhiteSpace(zip))
				{
					zipResults.Add(zipResult);
				}
				else
				{
					ZipCode zipCode = zipCodes.Where(i => i.BaseZipCode == item["Postal Code"]).FirstOrDefault();
					if (zipCode != null)
					{
						zipResult.Distance = Convert.ToSingle(zipCode.DistanceMiles);
						zipResults.Add(zipResult);
					}
				}
			}

			return zipResults;
		}

		private void LoadProviders(string zip)
		{
			if (searchResults.Count() == 0)
			{
				ErrorMessage errorMessageCtl = (ErrorMessage)errorMessage;
				errorMessageCtl.SetNoResultMessage(SearchResultsPnl);
				errorMessage.Visible = true;
			}
			else
			{
				List<ProviderSearchResult> orderedList;

				if (string.IsNullOrWhiteSpace(zip))
				{
					orderedList = (from item in searchResults
					               orderby item.Name, item.City, item.Address1
					               select item).ToList();
				}
				else
				{
					orderedList = (from item in searchResults
					               orderby item.Distance, item.Name, item.City, item.Address1
					               select item).ToList();
				}

				ItemsLV.DataSource = orderedList;
				ItemsLV.DataBind();

				Session[resultsSessionName] = orderedList;
				Session[resultsSessionZip] = (zip == null) ? string.Empty : zip;

				Session["DataPagerStartRowIndex"] = null;
				SearchResultsPnl.Visible = true;
			}
		}

		private void SetRowDetails(ListViewItem control, ProviderSearchResult row)
		{
			thisId = row.ProviderId;

			if (thisId.Equals(lastId))
			{
				idAddressCount++;
			}
			else
			{
				idAddressCount = 1;

				HtmlTableRow trSeparator = (HtmlTableRow)control.FindControl("trSeparator");
				trSeparator.Visible = true;

				Label nameLbl = (Label)control.FindControl("NameLbl");
				nameLbl.Text = row.Name;

				Label idLbl = (Label)control.FindControl("IdLbl");
				ProviderHelper.SetNpi(idLbl, row.NpiId);

				Label effectiveLbl = (Label)control.FindControl("EffectiveLbl");
				ProviderHelper.SetEffectiveDate(effectiveLbl, row.EffectiveDate);

				Label languagesLbl = (Label)control.FindControl("LanguagesLbl");
				if (string.IsNullOrEmpty(row.SecondaryLanguages))
					languagesLbl.Text = string.Format("<br />Languages: {0}", row.PrimaryLanguage);
				else
					languagesLbl.Text = string.Format("<br />Languages: {0}, {1}", row.PrimaryLanguage, row.SecondaryLanguages);

				Label AcceptingNewPatientsLbl = (Label)control.FindControl("AcceptingNewPatientsLbl");

				if (string.IsNullOrWhiteSpace(row.AcceptingPatients))
					AcceptingNewPatientsLbl.Text = "<br />Accepting New Patients: No";
				else if (row.AcceptingPatients.Equals("Y"))
					AcceptingNewPatientsLbl.Text = "<br />Accepting New Patients: Yes";
				else if (!string.IsNullOrEmpty(row.PanelClosedBy) && (row.PanelClosedBy.Equals("MD") || row.PanelClosedBy.Equals("MMPM")))
					AcceptingNewPatientsLbl.Text = "<br />Accepting New Patients: Existing patients only";
				else
					AcceptingNewPatientsLbl.Text = "<br />Accepting New Patients: No";

				Literal specialtiesLit = (Literal)control.FindControl("SpecialtiesLit");
				specialtiesLit.Text = row.PrimarySpecialty;
				if (!string.IsNullOrEmpty(row.SecondarySpecialty))
				{
					specialtiesLit.Text += "<br />" + row.SecondarySpecialty;
				}
			}

			Literal addressLit = (Literal)control.FindControl("AddressLit");
			StringBuilder addressHtml = new StringBuilder();

			Literal distanceLit = (Literal)control.FindControl("DistanceLit");
			StringBuilder distanceHtml = new StringBuilder();

			if (idAddressCount < 5)
			{
				if (idAddressCount > 1)
				{
					SetRowAddressSeparator(control, false);
				}

				addressHtml.Append(LocationHelper.GetFormattedAddress(row.Address1, row.Address2,
					row.City, row.State, row.Zip));

				if (!string.IsNullOrEmpty(row.Phone))
					addressHtml.AppendFormat("<br />Ph: {0}", LocationHelper.GetFormattedPhoneNo(row.Phone));

				if (row.AddressEffectiveDate < DateTime.Now)
					ProviderHelper.SetEffectiveDate(addressHtml, row.AddressEffectiveDate);

				addressLit.Text = addressHtml.ToString();

				if (!string.IsNullOrWhiteSpace(searchZip))
					distanceHtml.AppendFormat("{0}: {1:0.00} miles<br />", distanceText, row.Distance);

				distanceHtml.AppendFormat("<a href=\"{0}\">{1}</a>",
					string.Format(ProviderHelper.DetailsUrlFormat, detailsBaseUrl, row.ProviderId, row.AddressId, row.AddressType),
					"View Provider Details");

				distanceLit.Text = distanceHtml.ToString();
			}
			else if (idAddressCount == 5)
			{
				SetRowAddressSeparator(control, true);

				addressHtml.AppendFormat("<a href=\"{0}\">{1}</a>",
					string.Format(ProviderHelper.DetailsUrlFormat, detailsBaseUrl, row.ProviderId, row.AddressId, row.AddressType),
					Translate.Text("All addresses for this provider"));
				addressLit.Text = addressHtml.ToString();
				distanceLit.Text = "&nbsp;";
			}
			else
			{
				HtmlTableRow trItemRow = (HtmlTableRow)control.FindControl("trItemRow");
				trItemRow.Visible = false;
			}

			lastId = thisId;
		}

		private void SetRowAddressSeparator(ListViewItem control, bool hideDistance)
		{
			HtmlTableCell tdAddress = (HtmlTableCell)control.FindControl("tdAddress");
			tdAddress.Attributes.Add("class", "addressSep");
		}

		#endregion
	}
}
