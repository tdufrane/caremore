﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentLinkBtn.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.ContentLinkBtn" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="span3">
	<sc:Link ID="scLink" runat="server" Field="Link"><sc:Image ID="scImage" runat="server" Field="Image" /></sc:Link>
</div>
