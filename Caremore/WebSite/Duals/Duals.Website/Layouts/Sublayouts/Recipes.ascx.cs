﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;
using Sitecore.Resources.Media;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Content;
using Duals.Models.Pages;
using Duals.Models.Widgets;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class Recipes : System.Web.UI.UserControl
	{
		#region Private variables

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
			}
		}

		protected void LvRecipeGroups_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListViewItemType.DataItem)
				{
					LoadGroup(e.Item);
				}
			}
			catch (Exception ex)
			{
			}
		}

		protected void LvRecipes_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListViewItemType.DataItem)
				{
					LoadRecipe(e.Item);
				}
			}
			catch (Exception ex)
			{
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			ID recipeGroupID = new ID(RecipeGroup.TemplateID);

			lvRecipeFolders.DataSource = Sitecore.Context.Item.Children
				.Where(i => i.TemplateID == recipeGroupID);
			lvRecipeFolders.DataBind();
		}

		private void LoadGroup(ListViewItem lvItem)
		{
			Item recipeGroup = lvItem.DataItem as Item;

			Text txtRecipeGroup = lvItem.FindControl("txtRecipeGroup") as Text;
			txtRecipeGroup.Item = recipeGroup;

			HyperLink hlRecipeGroup = lvItem.FindControl("hlRecipeGroup") as HyperLink;
			hlRecipeGroup.CssClass += lvItem.DataItemIndex == 0 ? " glyphicon-chevron-down" : " glyphicon-chevron-right";

			Panel pnlAccordionBody = lvItem.FindControl("pnlAccordionBody") as Panel;
			pnlAccordionBody.CssClass += lvItem.DataItemIndex == 0 ? " in" : string.Empty;
			hlRecipeGroup.Attributes.Add("href", "#" + pnlAccordionBody.ClientID);

			ID recipeID = new ID(Recipe.TemplateID);

			ListView lvRecipes = lvItem.FindControl("lvRecipes") as ListView;
			lvRecipes.DataSource = recipeGroup.Children
				.Where(i => i.TemplateID == recipeID);
			lvRecipes.DataBind();
		}

		private void LoadRecipe(ListViewItem lvItem)
		{
			Item recipe = lvItem.DataItem as Item;

			Link linkEnglishPdf = lvItem.FindControl("linkEnglishPdf") as Link;
			linkEnglishPdf.Item = recipe;

			Text txtEnglishTitle = lvItem.FindControl("txtEnglishTitle") as Text;
			txtEnglishTitle.Item = recipe;

			Text txtEnglishDescription = lvItem.FindControl("txtEnglishDescription") as Text;
			txtEnglishDescription.Item = recipe;

			Link linkSpanishPdf = lvItem.FindControl("linkSpanishPdf") as Link;
			linkSpanishPdf.Item = recipe;

			Text txtSpanishTitle = lvItem.FindControl("txtSpanishTitle") as Text;
			txtSpanishTitle.Item = recipe;

			Text txtSpanishDescription = lvItem.FindControl("txtSpanishDescription") as Text;
			txtSpanishDescription.Item = recipe;
		}

		#endregion
	}
}
