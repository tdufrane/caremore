﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using CareMore.Web.DotCom.Data;
using CareMore.Web.DotCom.Provider;

using Sitecore.Data.Items;
using Duals.Website.Controls;
using Duals.Models;
using Duals.Business;

using Duals.Models.Content.Articles;
using CareMore.Web.DotCom.Models.Providers;

using ClassySC.Data;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class ProviderSearch : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				SubmitBtn.Text = string.Format("<span>{0}</span>", "Find a Doctor");
				SubmitBtn.ToolTip = "Find a Doctor";

				if (!Page.IsPostBack)
				{
					Item resultsPage = Sitecore.Context.Item.GetChildren().Where(i => i.Name == "Results").FirstOrDefault();
					if (resultsPage != null)
					{
						ProviderListArticle results = resultsPage.ToClass<ProviderListArticle>();
						ProviderListing listing = new ProviderListing(results.ProviderListType);

						if (listing.HideSearchForm.HasValue && listing.HideSearchForm.Value)
						{
							Session["SearchResults"] = null;
							Session["DataPagerStartRowIndex"] = null;
							Response.Redirect(Sitecore.Links.LinkManager.GetItemUrl(resultsPage));
						}
					}

					// Cache changes at 6 am the next day after CODS refresh
					DateTime expiresOn = DateTime.Now.Date.Add(new TimeSpan(1, 6, 0, 0));

					PopulateLanguageDropDownList(LanguageDDL, expiresOn);

					if (Sitecore.Context.Item["Hide Specialty"].Equals("1"))
					{
						SpecialtyPH.Visible = false;
					}
					else
					{
						PopulateSpecialityDropDownList(SpecialtyDDL, expiresOn);
					}
				}
			}
			catch (Exception ex)
			{
				ErrorMessage errorMessageCtl = (ErrorMessage)errorMessage;
				errorMessageCtl.SetDownMessage(ex);
				errorMessage.Visible = true;
			}
		}

		private void PopulateLanguageDropDownList(ListBox ddl, DateTime expiresOn)
		{
			ListItem li = new ListItem() { Text = "All", Value = "" };
			li.Selected = true;
			ddl.Items.Add(li);

			IEnumerable<LanguageItem> langs = ProviderFinder.GetLanguageItems();

			foreach (LanguageItem row in langs)
			{
				li = new ListItem() { Text = row.Description, Value = row.Code };
				ddl.Items.Add(li);
			}
		}

		private void PopulateSpecialityDropDownList(ListBox ddl, DateTime expiresOn)
		{
			ListItem li = new ListItem() { Text = "All", Value = "" };
			li.Selected = true;
			ddl.Items.Add(li);

			li = new ListItem() { Text = "Primary Care Physician (Family, General, Internal)", Value = "PCP" };
			ddl.Items.Add(li);

			IEnumerable<SpecialtyItem> specialties = ProviderFinder.GetSpecialtyItems();

			foreach (SpecialtyItem row in specialties)
			{
				li = new ListItem() { Text = row.Description, Value = row.Code };
				ddl.Items.Add(li);
			}
		}

		protected void SubmitBtn_OnClick(object sender, EventArgs e)
		{
			if (Page.IsValid)
			{
				Item resultsPage = Sitecore.Context.Item.GetChildren().Where(i => i.Name == "Results").FirstOrDefault();

				StringBuilder url = new StringBuilder();
				url.Append(Sitecore.Links.LinkManager.GetItemUrl(resultsPage));
				url.Append("?");

				string selections;

				if (SpecialtyPH.Visible)
				{
					selections = GetSelectedValues(SpecialtyDDL);

					if (selections.Length > 0)
						url.AppendFormat("specialty={0}&", HttpUtility.UrlEncode(GetSelectedValues(SpecialtyDDL)));
				}

				if (LastNameTB.Text.Length > 0)
					url.AppendFormat("lastName={0}&", HttpUtility.UrlEncode(LastNameTB.Text));

				if (ZipCodeTB.Text.Length > 0)
					url.AppendFormat("zip={0}&", ZipCodeTB.Text);

				if (CityTB.Text.Length > 0)
					url.AppendFormat("city={0}&", CityTB.Text);

				selections = GetSelectedValues(LanguageDDL);
				if (selections.Length > 0)
					url.AppendFormat("language={0}", HttpUtility.UrlEncode(GetSelectedValues(LanguageDDL)));

				Session["SearchResults"] = null;
				Session["DataPagerStartRowIndex"] = null;

				string redirectUrl = url.ToString().TrimEnd(new char[] { '?', '&' });
				Response.Redirect(redirectUrl);
			}
		}

		private string GetSelectedValues(ListBox listBox)
		{
			StringBuilder selections = new StringBuilder();
			foreach (ListItem item in listBox.Items)
			{
				if (item.Selected && item.Value.Length > 0)
					selections.Append(item.Value + ",");
			}

			if (selections.Length > 1)
				selections.Remove(selections.Length - 1, 1);

			return selections.ToString();
		}
	}
}