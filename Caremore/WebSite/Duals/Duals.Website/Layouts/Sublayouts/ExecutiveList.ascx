﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExecutiveList.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.ExecutiveList" %>

<asp:ListView ID="lvItems" runat="server" OnItemDataBound="LvItems_ItemDataBound">
	<LayoutTemplate>
		<div class="span8">
			<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
		</div>
	</LayoutTemplate>
	<ItemTemplate>
		<div class="row execItem">
			<sc:Image ID="imgPicture" runat="server" />
			<sc:FieldRenderer ID="frName" runat="server" EnclosingTag="h3" />
			<sc:FieldRenderer ID="frTitle" runat="server" EnclosingTag="h4" />
			<sc:FieldRenderer ID="frDescription" runat="server" />
		</div>
	</ItemTemplate>
	<EmptyDataTemplate>The executive team list is coming soon.</EmptyDataTemplate>
	<ItemSeparatorTemplate>
		<hr />
	</ItemSeparatorTemplate>
</asp:ListView>
