﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MaterialRequest.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.MaterialRequest" %>
<%@ Register TagPrefix="cm" Namespace="CareMore.Web.DotCom.Controls" Assembly="CareMore.Web.DotCom"  %>
<%@ Register Src="../../Controls/BaseContentPage.ascx" TagPrefix="cm" TagName="BaseContentPage" %>

<script src="/js/jquery.maskedinput.js" type="text/javascript"></script>
<script type="text/javascript">
	$j(function () {
		$j("#ZipCode").mask("99999");
		$j("#Phone").mask("(999) 999-9999");
	});
</script>

<cm:BaseContentPage ID="ucBaseContentPage" runat="server" ColumnSpanCssClass="span12" />

<div class="span12">
	<asp:Panel ID="MaterialReqPanel" runat="server" ClientIDMode="Static">
		<div >
			<sc:Text ID="FormHeader" Field="Form Header" runat="server" />
		</div>

		<div><asp:ValidationSummary ID="ValidationSummaryText" runat="server" DisplayMode="List" /></div>
		<div style="float:none; clear:both;"><p>(*) Indicates a Required field.</p></div>
		<div><asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label></div>

		<div class="form" id="materialForm">
			<div class="formField long listGroup">
				<label for="MemberTypes">Are You*</label>
				<div class="field">
					<asp:RadioButtonList ID="MemberTypes" runat="server" RepeatDirection="Vertical" RepeatLayout="UnorderedList">
						<asp:ListItem Text="Current Member" Value="Current Member" />
						<asp:ListItem Text="Non-Member" Value="Non-Member" />
						<asp:ListItem Text="Caregiver" Value="Caregiver" />
					</asp:RadioButtonList>
					<asp:RequiredFieldValidator ID="MemberTypesVal" runat="server" ErrorMessage="Are You must be selected."
						ControlToValidate="MemberTypes" Display="None" />
				</div>
			</div>
			<div class="formField long listGroup">
				<label for="PlanMember">Are You a Current Cal MediConnect Plan Member*</label>
				<div class="field">
					<asp:RadioButtonList ID="PlanMember" runat="server" RepeatDirection="Vertical" RepeatLayout="UnorderedList">
						<asp:ListItem Text="Yes" Value="Yes" />
						<asp:ListItem Text="No" Value="No" />
					</asp:RadioButtonList>
					<asp:RequiredFieldValidator ID="PlanMemberVal" runat="server" ErrorMessage="Are You a Current Cal MediConnect Plan Member must be selected."
						ControlToValidate="PlanMember" Display="None" />
				</div>
			</div>
			<div class="formField">
				<label for="LastName">Last Name*</label>
				<div class="field">
					<asp:TextBox ID="LastName" runat="server" ClientIDMode="Static" />
					<asp:RequiredFieldValidator ID="LastNameVal" runat="server" ErrorMessage="Last Name must be filled in." ControlToValidate="LastName" Display="None" />
				</div>
			</div>
			<div class="formField">
				<label for="FirstName">First Name*</label>
				<div class="field">
					<asp:TextBox ID="FirstName" runat="server" ClientIDMode="Static" />
					<asp:RequiredFieldValidator ID="FirstNameVal" runat="server" ErrorMessage="First Name must be filled in." ControlToValidate="FirstName" Display="None" />
				</div>
			</div>
			<div class="formField short">
				<label for="MiddleInitial">Middle Initial</label>
				<div class="field">
					<asp:TextBox ID="MiddleInitial" runat="server" ClientIDMode="Static" MaxLength="1" />
				</div>
			</div>
			<div class="formField long">
				<label for="Address">Address*</label>
				<div class="field">
					<asp:TextBox ID="Address" runat="server"  ClientIDMode="Static"/>
					<asp:RequiredFieldValidator ID="AddressVal" runat="server" ErrorMessage="Address must be filled in." ControlToValidate="Address" Display="None" />
				</div>
			</div>
			<div class="formField">
				<label for="City">City*</label>
				<div class="field">
					<asp:TextBox ID="City" runat="server"  ClientIDMode="Static" />
					<asp:RequiredFieldValidator ID="CityVal" runat="server" ErrorMessage="City must be filled in." ControlToValidate="City" Display="None" />
				</div>
			</div>
			<div class="formField">
				<label for="State">State*</label>
				<div class="field">
					<asp:DropDownList ID="State" runat="server"  ClientIDMode="Static" Width="100%">
						<asp:ListItem Text="-Select One-" Value="" />
						<asp:ListItem Text="Alabama" Value="Alabama" />
						<asp:ListItem Text="Alaska" Value="Alaska" />
						<asp:ListItem Text="Arizona" Value="Arizona" />
						<asp:ListItem Text="Arkansas" Value="Arkansas" />
						<asp:ListItem Text="California" Value="California" />
						<asp:ListItem Text="Colorado" Value="Colorado" />
						<asp:ListItem Text="Connecticut" Value="Connecticut" />
						<asp:ListItem Text="Delaware" Value="Delaware" />
						<asp:ListItem Text="Florida" Value="Florida" />
						<asp:ListItem Text="Georgia" Value="Georgia" />
						<asp:ListItem Text="Hawaii" Value="Hawaii" />
						<asp:ListItem Text="Idaho" Value="Idaho" />
						<asp:ListItem Text="Illinois" Value="Illinois" />
						<asp:ListItem Text="Indiana" Value="Indiana" />
						<asp:ListItem Text="Iowa" Value="Iowa" />
						<asp:ListItem Text="Kansas" Value="Kansas" />
						<asp:ListItem Text="Kentucky" Value="Kentucky" />
						<asp:ListItem Text="Louisiana" Value="Louisiana" />
						<asp:ListItem Text="Maine" Value="Maine" />
						<asp:ListItem Text="Maryland" Value="Maryland" />
						<asp:ListItem Text="Massachusetts" Value="Massachusetts" />
						<asp:ListItem Text="Michigan" Value="Michigan" />
						<asp:ListItem Text="Minnesota" Value="Minnesota" />
						<asp:ListItem Text="Mississippi" Value="Mississippi" />
						<asp:ListItem Text="Missouri" Value="Missouri" />
						<asp:ListItem Text="Montana" Value="Montana" />
						<asp:ListItem Text="Nebraska" Value="Nebraska" />
						<asp:ListItem Text="Nevada" Value="Nevada" />
						<asp:ListItem Text="New Hampshire" Value="New Hampshire" />
						<asp:ListItem Text="New Jersey" Value="New Jersey" />
						<asp:ListItem Text="New Mexico" Value="New Mexico" />
						<asp:ListItem Text="New York" Value="New York" />
						<asp:ListItem Text="North Carolina" Value="North Carolina" />
						<asp:ListItem Text="North Dakota" Value="North Dakota" />
						<asp:ListItem Text="Ohio" Value="Ohio" />
						<asp:ListItem Text="Oklahoma" Value="Oklahoma" />
						<asp:ListItem Text="Oregon" Value="Oregon" />
						<asp:ListItem Text="Pennsylvania" Value="Pennsylvania" />
						<asp:ListItem Text="Rhode Island" Value="Rhode Island" />
						<asp:ListItem Text="South Carolina" Value="South Carolina" />
						<asp:ListItem Text="South Dakota" Value="South Dakota" />
						<asp:ListItem Text="Tennessee" Value="Tennessee" />
						<asp:ListItem Text="Texas" Value="Texas" />
						<asp:ListItem Text="Utah" Value="Utah" />
						<asp:ListItem Text="Vermont" Value="Vermont" />
						<asp:ListItem Text="Virginia" Value="Virginia" />
						<asp:ListItem Text="Washington" Value="Washington" />
						<asp:ListItem Text="West Virginia" Value="West Virginia" />
						<asp:ListItem Text="Wisconsin" Value="Wisconsin" />
						<asp:ListItem Text="Wyoming" Value="Wyoming" />
					</asp:DropDownList>
					<asp:RequiredFieldValidator ID="StateVal" runat="server" ErrorMessage="State must be selected." ControlToValidate="State" Display="None" />
				</div>
			</div>			
			<div class="formField short">
				<label for="ZipCode">Zip*</label>
				<div class="field">
					<asp:TextBox ID="ZipCode" runat="server" ClientIDMode="Static" />
					<asp:RequiredFieldValidator ID="ZipCodeVal" runat="server" ErrorMessage="Zip must be filled in." ControlToValidate="ZipCode" Display="None" />
				</div>
			</div>
			<div class="formField">
				<label for="HomePhone">Phone</label>
				<div class="field">
					<asp:TextBox ID="Phone" runat="server" ClientIDMode="Static" />
				</div>
			</div>
			<div class="formField">
				<label for="MemberID">Member ID</label>
				<div class="field">
					<asp:TextBox ID="MemberID" runat="server" ClientIDMode="Static" />
				</div>
			</div>
			<div class="formField long listGroup">
				<label for="PlanYears">Plan Year Requested</label>
				<div class="field">
					<asp:RadioButtonList ID="PlanYears" runat="server" ClientIDMode="Static" RepeatLayout="UnorderedList" />
				</div>
			</div>
			<div class="formField long listGroup">
				<label for="LanguageRequested">Language Requested*</label>
				<div class="field">
					<asp:CheckBoxList ID="LanguageRequested" runat="server" RepeatDirection="Vertical" RepeatLayout="UnorderedList">
						<asp:ListItem Text="English" Value="English" />
						<asp:ListItem Text="Arabic" Value="Arabic" />
						<asp:ListItem Text="Armenian" Value="Armenian" />
						<asp:ListItem Text="Chinese" Value="Chinese" />
						<asp:ListItem Text="Farsi" Value="Farsi" />
						<asp:ListItem Text="Khmer" Value="Khmer" />
						<asp:ListItem Text="Korean" Value="Korean" />
						<asp:ListItem Text="Russian" Value="Russian" />
						<asp:ListItem Text="Spanish" Value="Spanish" />
						<asp:ListItem Text="Tagalog" Value="Tagalog" />
						<asp:ListItem Text="Vietnamese" Value="Vietnamese" />
					</asp:CheckBoxList>
					<cm:RequiredFieldValidatorForCheckBoxLists ID="LanguageRequestedVal" runat="server" ErrorMessage="At least one Language Requested must be checked."
						ControlToValidate="LanguageRequested" Display="None" />
				</div>
			</div>
			<div class="formField long listGroup">
				<label for="AlternateFormat">Alternate Format</label>
				<div class="field">
					<asp:RadioButtonList ID="AlternateFormat" runat="server" RepeatDirection="Vertical" RepeatLayout="UnorderedList">
						<asp:ListItem Text="Audio" Value="Audio" />
						<asp:ListItem Text="Braille" Value="Braille" />
						<asp:ListItem Text="Large Print (14 point)" Value="Large Print (14 point)" />
						<asp:ListItem Text="Large Print (16 point)" Value="Large Print (16 point)" />
					</asp:RadioButtonList>
				</div>
			</div>

			<div class="formField">
				<div class="field"><asp:LinkButton ID="SubmitBtn"  BackColor="Transparent" BorderStyle="None" CssClass="btn" runat="server" OnClick="SendMessageClick" Text="SUBMIT" />
				</div>
			</div>
		</div>
	</asp:Panel>

	<asp:Panel ID="MessagePanel" runat="server" Visible="false">
		<asp:Label ID="MessageLabel" runat="server" />
	</asp:Panel>
</div>
