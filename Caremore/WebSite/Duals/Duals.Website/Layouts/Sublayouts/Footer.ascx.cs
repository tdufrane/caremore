﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Pages;
using Duals.Models.Settings;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class Footer : Controls.BaseSublayout
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			SetContent();
			BuildSiteNav();
			SetLanguageFooterItems();
		}

		private void SetContent()
		{
			if (SiteSettings != null && SiteSettings.InnerItem != null)
			{
				scNonDiscrNotice.Item = SiteSettings.InnerItem;
				scRightColumn.Item = SiteSettings.InnerItem;
			}
			else
			{
				Sitecore.Diagnostics.Log.Error("Duals: Cannot set FooterNav", this);
			}
		}

		/// <summary>
		/// Get Duals home item Childern and generate SiteNav using LandingPage items
		/// </summary>
		private void BuildSiteNav()
		{
			try
			{
				// Site Nav
				siteNavLV.DataSource = Sitecore.Context.Database.GetItem(GlobalConsts.DualsHomeID).Children.ToList()    // get all children of Duals home node
					.ConvertAll(item => item.ToClass<LandingPage>())    // convert to LandingPage class
					.Where(page => page != null && (page.ExcludeFromNavigation.HasValue ? !page.ExcludeFromNavigation.Value : true));   // exclude null and not included in nav
				siteNavLV.DataBind();

				// Middle Nav
				middleNavLV.DataSource = Sitecore.Context.Database.GetItem(GlobalConsts.DualsHomeID).Children.ToList()    // get all children of Duals home node
					.ConvertAll(item => item.ToClass<SitePage>())    // convert to SitePage class
					.Where(page => page != null && (page.ExcludeFromNavigation.HasValue ? !page.ExcludeFromNavigation.Value : true) && (page.AddToFooterMiddleColumnNav.HasValue ? page.AddToFooterMiddleColumnNav.Value : false));   // exclude null and not included in nav
				middleNavLV.DataBind();
			}
			catch (Exception exc)
			{
				Sitecore.Diagnostics.Log.Error("Duals: Cannot build Footer SiteNav", exc, this);
			}
		}


		private void SetLanguageFooterItems()
		{
			try
			{
				SiteSettings ss = Sitecore.Context.Database.GetItem(SiteSettings.ID).ToClass<SiteSettings>();

				languageFooterHL.Text = ss.FooterLanguageText;
				Sitecore.Data.Fields.LinkField field = ss.LinkToFooterLanguagePage;
				languageFooterHL.NavigateUrl = field.Url;
			}
			catch (Exception e)
			{
				Sitecore.Diagnostics.Log.Error("Duals: Cannot Build Language Footers", e, this);
			}
		}

		protected void navLV_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				ListViewDataItem LVDI = e.Item as ListViewDataItem;
				HyperLink hl = e.Item.FindControl("hl") as HyperLink;

				if (LVDI.DataItem is Duals.Models.Pages.GeneralLinkPage)
				{
					Duals.Models.Pages.GeneralLinkPage page = LVDI.DataItem as Duals.Models.Pages.GeneralLinkPage;
					hl.Target = page.Link.Target;

					if (string.IsNullOrWhiteSpace(page.Link.Text))
						hl.Text = page.NavigationTitle;
					else
						hl.Text = page.Link.Text;

					hl.NavigateUrl = page.Link.Url;
				}
				else
				{
					Duals.Models.Base.Page page = LVDI.DataItem as Duals.Models.Base.Page;
					hl.Text = page.NavigationTitle;
					hl.NavigateUrl = LinkManager.GetItemUrl(page.InnerItem);
				}
			}
		}
	}
}