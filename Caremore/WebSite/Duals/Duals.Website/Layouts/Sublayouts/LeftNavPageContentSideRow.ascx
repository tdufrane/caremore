﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftNavPageContentSideRow.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.LeftNavPageContentSideRow" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="span8">
	<div class="masthead">
<asp:PlaceHolder runat="server" ID="phWithImage" Visible="false">
		<div runat="server" id="imgIntro" class="img-intro">
			<h1><sc:Text runat="server" Field="Masthead Title" /></h1>
			<p><sc:Text runat="server" Field="Masthead Description" /></p>
		</div>
		<div class="bgImage">
			<sc:Image runat="server" ID="scImage" />
		</div>
</asp:Placeholder>
<asp:PlaceHolder runat="server" ID="phWithoutImage" Visible="false">
		<h1><sc:Text runat="server" Field="Masthead Title" /></h1>
		<p><sc:Text runat="server" Field="Masthead Description" /></p>
</asp:Placeholder>
	</div>
	<div class="row">
		<div class="side">
			<sc:Placeholder runat="server" Key="duals-siderow" />
		</div>
		<sc:Text runat="server" Field="Page Content" />
	</div>
</div>