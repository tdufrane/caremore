﻿<%@ Control Language="C#" AutoEventWireup="true" ViewStateMode="Enabled" CodeBehind="FindYourProvider.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.FindYourProvider" %>
<%@ Register TagPrefix="gm" TagName="GoogleMap" Src="~/Duals/Duals.Website/Controls/GoogleMap.ascx" %>
<%@ Register TagPrefix="cm" TagName="Error" Src="~/Duals/Duals.Website/Controls/ErrorMessage.ascx" %>

<div class="span8">
	<div id="secondaryPageContentWrap">
		<asp:Panel ID="SearchFormPnl" DefaultButton="btnSubmit" runat="server">
			<div style="margin-bottom: 10px;">
				<div class="boxNoBack" style="float: left;">
					<span class="mapKeyTitle"><asp:Label ID="ZipCodeTitle" runat="server" /></span><br />
					<div>
						<asp:TextBox ID="ZipCodeTB" runat="server" EnableViewState="true" ClientIDMode="Static" />
					</div>
				</div><!-- end boxNoBack -->

				<div class="boxNoBackRight" style="float: right;">
					<span class="mapKeyTitle"><asp:Label ID="ShowTitle" runat="server" />:</span><br />
					<div class="selector">
						<span id="spanKey" class="currentSelectSpan"></span>
						<asp:DropDownList ID="MapKeyList" runat="server" AutoPostBack="false" EnableViewState="true" CssClass="providerSelect" ClientIDMode="Static" onchange="ChangeSpanKeyText()"></asp:DropDownList>
					</div>
				</div><!-- end boxNoBack -->

				<div style="clear: both;"></div>

				<script type="text/javascript">
					$j("#spanKey").text($j("#MapKeyList option:selected").text());

					function ChangeSpanKeyText() {
						$j("#spanKey").text($j("#MapKeyList option:selected").text());
					}
				</script>
			</div>

			<div>
				<asp:RangeValidator ID="rngValidate_ZipCodeTB" runat="server"
					ControlToValidate="ZipCodeTB" Display="Dynamic"
					ErrorMessage="Enter a number between 10000 and 99999"
					MinimumValue="10000" MaximumValue="99999"
					Type="Integer" ValidationGroup="AllValidators">
				</asp:RangeValidator>
				<div style="float: right; margin-bottom: 10px;">
					<asp:LinkButton ID="btnSubmit" runat="server" OnClick="BtnSubmit_OnClick" BackColor="Transparent" BorderStyle="None" CssClass="btn" ValidationGroup="AllValidators" />
				</div>
			</div>

			<cm:Error ID="errorMessage" runat="server" Visible="false" />

			<div class="clear"></div>

			<gm:GoogleMap ID="googleMap" runat="server" Visible="false" />

			<div class="clear"></div>
		</asp:Panel>

		<div><sc:Text ID="SecondaryContentFR" runat="server" Field="Secondary Page Content" /></div>
	</div>
</div>
