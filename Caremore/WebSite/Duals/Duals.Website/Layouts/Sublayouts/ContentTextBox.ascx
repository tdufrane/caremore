﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentTextBox.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.ContentTextBox" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="span4">
	<h2><sc:Text ID="scTitle" runat="server" Field="Title" /></h2>
	<p><sc:Text ID="scContent" runat="server" Field="Content" /></p>
	<p><sc:Link ID="scLink" runat="server" Field="Link" /></p>
	<asp:Panel ID="pnlSpacer" runat="server" Visible="false"></asp:Panel>
</div>