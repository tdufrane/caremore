﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;
using Sitecore.Resources.Media;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Content;
using Duals.Models.Pages;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class ExecutiveList : Controls.BaseSublayout
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			lvItems.DataSource = Sitecore.Context.Item.Children
				.Where(i => i.TemplateName == "Executive");
			lvItems.DataBind();
		}

		protected void LvItems_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				Item item = e.Item.DataItem as Item;

				Sitecore.Web.UI.WebControls.Image picture = e.Item.FindControl("imgPicture") as Sitecore.Web.UI.WebControls.Image;
				FieldRenderer name = e.Item.FindControl("frName") as FieldRenderer;
				FieldRenderer title = e.Item.FindControl("frTitle") as FieldRenderer;
				FieldRenderer desc = e.Item.FindControl("frDescription") as FieldRenderer;

				picture.Field = Executive.Picture_FID;
				name.FieldName = Executive.Name_FID;
				title.FieldName = Executive.Title_FID;
				desc.FieldName = Executive.Description_FID;

				name.Item = title.Item = desc.Item = picture.Item = item;
 			}
		}
	}
}
