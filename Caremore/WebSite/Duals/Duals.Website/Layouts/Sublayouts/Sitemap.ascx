﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Sitemap.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.Sitemap" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>

<div class="span12">      
	<div class="masthead">
		<h1><sc:Text runat="server" Field="Masthead Title" /></h1>
	</div>
	<asp:TreeView runat="server" ID="siteMapTV" class="siteMapTV" ExpandDepth="1" ShowLines="false" />
</div> <!-- / span12 -->