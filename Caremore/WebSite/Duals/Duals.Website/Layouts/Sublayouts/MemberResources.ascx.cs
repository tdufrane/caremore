﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Pages;

using Sitecore;
using Sitecore.Links;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;
using Sitecore.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class MemberResources : Controls.BaseSublayout
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (DataSource != null)
			{
				BuildLinks();
			}
			else
			{
				// DataSource NOT set
				Sitecore.Diagnostics.Log.Error("Duals: DataSource NOT set on MemberResources", this);
			}
		}

		private void BuildLinks()
		{
			try
			{
				LanguagesLV.DataSource = this.DataSource.Children.Where(item => item.TemplateID.ToString() == GlobalConsts.MemberMaterialsFolderID);
				LanguagesLV.DataBind();
			}
			catch (Exception exc)
			{
				Sitecore.Diagnostics.Log.Error("Duals: Cannot build MemberResources Links", exc, this);
			}
		}

		protected void LanguagesLV_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				ListViewDataItem LVDI = (ListViewDataItem)e.Item;
				Item thisItem = (Item)LVDI.DataItem;

				HyperLink languageLink = e.Item.FindControl("LanguageLink") as HyperLink;
				if (languageLink != null)
				{
					languageLink.CssClass += e.Item.DataItemIndex == 0 ? " glyphicon-chevron-down" : " glyphicon-chevron-right";
					languageLink.Text = thisItem.DisplayName;

					Panel languageDiv = e.Item.FindControl("LanguageDiv") as Panel;
					if (languageDiv != null)
					{
						languageDiv.CssClass +=  e.Item.DataItemIndex==0 ? " in" : string.Empty;
						languageLink.Attributes.Add("href","#" + languageDiv.ClientID);
					}
				}

				ListView resourcesLV = e.Item.FindControl("ResourcesLV") as ListView;
				if (resourcesLV != null)
				{
					resourcesLV.DataSource = thisItem.Children;
					resourcesLV.DataBind();
				}
			}
		}
	}
}
