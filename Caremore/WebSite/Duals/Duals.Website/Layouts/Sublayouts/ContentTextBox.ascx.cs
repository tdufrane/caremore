﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Pages;

using Sitecore;
using Sitecore.Data.Items;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class ContentTextBox : Controls.BaseSublayout
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (DataSource != null)
			{
				scTitle.Item = DataSource;
				scContent.Item = DataSource;
				scLink.Item = DataSource;

				if (!string.IsNullOrEmpty(DataSource["Spacer Height"]))
				{
					pnlSpacer.Style.Add(HtmlTextWriterStyle.Height, DataSource["Spacer Height"]);
					pnlSpacer.Visible = true;
				}
			}
			else
			{
				// DataSource NOT set
				Sitecore.Diagnostics.Log.Error("Duals: DataSource NOT set on ContentTextBox", this);
			}
		}
	}
}