﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class ContentLinkBtn : Controls.BaseSublayout
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (DataSource != null)
			{
				scImage.Item = DataSource;
				scLink.Item = DataSource;
			}
			else
			{
				// DataSource NOT set
				Sitecore.Diagnostics.Log.Error("Duals: DataSource NOT set on ContentLinkBtn", this);
			}
		}
	}
}
