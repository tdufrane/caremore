﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;
using Sitecore.Resources.Media;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Pages;
using Duals.Models.Widgets;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class Carousel : Controls.BaseSublayout
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			// Set DataSource on Carousel where all items are
			lvNav.DataSource = lvContent.DataSource = Sitecore.Context.Database.GetItem(GlobalConsts.CarouseSlideFolderID).GetChildren();
			lvNav.DataBind();
			lvContent.DataBind();
		}

		protected void lvNav_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				int index = e.Item.DataItemIndex + 1;
				Item item = e.Item.DataItem as Item;

				//Does user have read access to the item?
				if (!item.Access.CanRead())
				{
					return;
				}

				HtmlGenericControl ListItem = e.Item.FindControl("listItem") as HtmlGenericControl;
				HtmlAnchor Link = e.Item.FindControl("navLink") as HtmlAnchor;

				ListItem.Attributes["class"] = "tab" + index.ToString(); //Add class to each LI sequentially
				Link.Attributes.Add("href", Request.Url.PathAndQuery + "#" + index.ToString()); //Add hash to link
				Link.Title = "Panel " + index.ToString(); //Add title to each link
			}
		}

		protected void lvContent_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				Item item = e.Item.DataItem as Item;

				//Does user have read access to the item?
				if (!item.Access.CanRead())
				{
					return;
				}

				Text title = e.Item.FindControl("scTitle") as Text;
				FieldRenderer text = e.Item.FindControl("scText") as FieldRenderer;
				Link link = e.Item.FindControl("scLink") as Link;
				Sitecore.Web.UI.WebControls.Image image = e.Item.FindControl("scImage") as Sitecore.Web.UI.WebControls.Image;

				title.Field = CarouselSlide.Title_FID;
				text.FieldName = CarouselSlide.Text_FID;
				link.Field = CarouselSlide.Link_FID;
				image.Field = CarouselSlide.Image_FID;

				title.Item = text.Item = link.Item = image.Item = item;

				if (link.Item.Fields[link.Field] == null || !link.Item.Fields[link.Field].HasValue)
					link.Visible = false;
			}
		}
	}
}