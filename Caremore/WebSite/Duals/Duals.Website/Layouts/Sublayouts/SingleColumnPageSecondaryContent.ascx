﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SingleColumnPageSecondaryContent.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.SingleColumnPageSecondaryContent" %>
<div class="span12">
	<sc:Text runat="server" Field="Secondary Page Content" />
</div><!-- / span12 -->