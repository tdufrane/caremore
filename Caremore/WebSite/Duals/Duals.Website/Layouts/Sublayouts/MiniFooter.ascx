﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MiniFooter.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.MiniFooter" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<footer class="miniFooter">
	<div><sc:Text runat="server" ID="scCode" Field="Code" /></div>
	<div><sc:Text runat="server" ID="scLastUpdatedOn" Field="Last Updated On" /> <%= LastUpdatedOnDate %></div>
	<asp:ListView id="miniFooterNavLV" runat="server" OnItemDataBound="miniFooterNavLV_ItemDataBound">
		<LayoutTemplate>
			<div>
				<ul>
					<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
				</ul>
			</div>
		</LayoutTemplate>
		<ItemTemplate>
			<li><asp:HyperLink runat="server" ID="hl" /><asp:Literal runat="server" ID="litSeparator" Text="|" Visible="false" /></li>
		</ItemTemplate>
	</asp:ListView>
</footer>