﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentLinkBtnWrapper.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.ContentLinkBtnWrapper" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="row homeButtons">
	<sc:Placeholder runat="server" Key="duals-contentbuttonwrapper" />
	<div class="clear"></div>
</div>
