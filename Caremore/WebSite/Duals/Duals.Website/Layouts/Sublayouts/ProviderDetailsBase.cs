﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using CareMore.Web.DotCom.Models.Providers;
using CareMore.Web.DotCom.Provider;

using ClassySC.Data;

using Duals.Business;
using Duals.Models;
using Duals.Models.Content.Articles;
using Duals.Website.Controls;

using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;


namespace Duals.Website.Layouts.Sublayouts
{
	public class ProviderDetailsBase : System.Web.UI.UserControl
	{
		protected string GetProviderLanguages(string providerID, string addressID, string addressType)
		{
			List<ProviderLanguage> list = ProviderFinder.GetProviderLanguages(providerID, addressID, addressType);

			List<string> languages = new List<string>();
			foreach (ProviderLanguage item in list)
			{
				if (!item.LanguageType.Equals("O")) // Exclude office language records
					languages.Add(item.Language);
			}

			return string.Join(", ", languages.ToArray());
		}

		protected void SetAccessibilityDb(List<ProviderAccessibility> accessibilities, Label accessibilityType, Repeater accessibilityList)
		{
			if ((accessibilities == null) || (accessibilities.Count == 0))
			{
				SetAccessibilityNone(accessibilityType, accessibilityList);
			}
			else
			{
				string accessType = null;

				if (accessibilities.Count(item => item.LimitedAccessMain) == 1)
					accessType = "Limited";
				else if (accessibilities.Count(item => item.BasicAccessMain) == 1)
					accessType = "Basic";

				List<ProviderAccessibility> displayAccessibilites = accessibilities
					.Where(item => !item.DoNotDisplay)
					.OrderBy(item => item.AccessibilityCode).ToList();

				if (displayAccessibilites.Count == 0)
				{
					SetAccessibilityNone(accessibilityType, accessibilityList);
				}
				else
				{
					string reportType = displayAccessibilites[0].Comments;
					string reportAs = null;

					if (reportType.Equals("Audited", StringComparison.OrdinalIgnoreCase))
						reportAs = "A";
					else if (reportType.Equals("Self Reported", StringComparison.OrdinalIgnoreCase))
						reportAs = "SR";

					if (string.IsNullOrEmpty(accessType))
					{
						if (!string.IsNullOrEmpty(reportAs))
							accessibilityType.Text = string.Format("{0}: ", reportAs);
					}
					else if (string.IsNullOrEmpty(reportAs))
					{
						accessibilityType.Text = string.Format("{0}: ", accessType);
					}
					else
					{
						accessibilityType.Text = string.Format("{0}-{1}: ", reportAs, accessType);
					}

					accessibilityList.DataSource = displayAccessibilites;
					accessibilityList.DataBind();
				}
			}
		}

		private void SetAccessibilityNone(Label accessibilityType, Repeater accessibilityList)
		{
			accessibilityType.Text = string.Empty;
			accessibilityList.Visible = false;
		}
	}
}
