﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Duals.Website.Controls;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class VideoPlayer : BaseSublayout
	{
		#region Properties

		public bool FullSize
		{
			get;
			set;
		}

		public string Path
		{
			get;
			set;
		}

		public string Still
		{
			get;
			set;
		}

		public int VideoHeight
		{
			get
			{
				if (FullSize)
				{
					return 360;
				}
				else
				{
					return 270;
				}
			}
		}

		public int VideoWidth
		{
			get
			{
				if (FullSize)
				{
					return 640;
				}
				else
				{
					return 480;
				}
			}
		}

		#endregion

		#region Page events

		protected void Page_Load(object sender, EventArgs e)
		{
			if (base.DataSource == null || string.IsNullOrEmpty(base.DataSource["Video Url"]))
			{
				pnlVideo.Visible = false;
			}
			else
			{
				pnlVideo.Visible = true;

				this.FullSize = base.DataSource["Full Size"].Equals("1");

				if (this.FullSize)
				{
					pnlSpacer.Visible = false;
					pnlVideo.CssClass = "video span12";
					Jp_container_v1.CssClass = "jp-video jp-video-360p";
				}
				else
				{
					pnlSpacer.Visible = true;
					pnlVideo.CssClass = "video span8";
					Jp_container_v1.CssClass = "jp-video jp-video-270p";
				}

				LinkField videoUrl = base.DataSource.Fields["Video Url"];
				this.Path = videoUrl.Url;

				if (!string.IsNullOrEmpty(base.DataSource["Video Image"]))
				{
					Sitecore.Data.Fields.ImageField videoThumb = base.DataSource.Fields["Video Image"];
					this.Still = MediaManager.GetMediaUrl(videoThumb.MediaItem);
				}
			}
		}

		#endregion

		#region Methods

		#endregion
	}
}
