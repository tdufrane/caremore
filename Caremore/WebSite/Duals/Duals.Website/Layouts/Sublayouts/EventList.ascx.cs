﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ClassySC.Data;

using Duals.Models;
using Duals.Models.Pages;
using Duals.Models.Content.Articles;
using Duals.Models.Content.Contact;
using Duals.Models.Content.Facilities;

using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Comparers;
using Sitecore.Links;
using Sitecore.Resources.Media;
using Sitecore.Web.UI.WebControls;

using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Models;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class EventList : Controls.BaseSublayout
	{
		public string GoogleMapID { get; set; }
		public string LocationAddress { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				BindEventTypes();
				BindEvents(Request.QueryString["selectedEventType"]);
			}
		}

		#region Bind Data

		/// <summary>
		/// Bind Event Types available
		/// </summary>
		private void BindEventTypes()
		{
			List<CareMore.Web.DotCom.Models.Events.EventType> eventTypes = new List<CareMore.Web.DotCom.Models.Events.EventType>();

			eventTypes.AddRange(DataSource.Children.ToList()
				.ConvertAll(item => item.ToClass<CareMore.Web.DotCom.Models.Events.EventType>())
				.Where(type => type != null));

			// set dropdown data source and bind
			ddlEventTypes.DataSource = eventTypes
				.ConvertAll(item => new ListItem() { Text = item.Title, Value = item.ID.ToString() });
			ddlEventTypes.DataBind();

			// Insert "All" option
			ddlEventTypes.Items.Insert(0, new ListItem() { Text = "All", Value = "" });
		}

		/// <summary>
		/// Gets all events in the "Events" folder
		/// </summary>
		/// <returns>List of EventPageBase objects</returns>
		private List<CareMore.Web.DotCom.Models.Pages.EventPage> GetAllEvents()
		{
			/*var eventPages = Sitecore.Context.Item.Children
					.Where(item => item.TemplateID == new ID(Common.FolderTemplateID)) // Common 'Folder' template
					.Single().Children.ToList()
					.ConvertAll(item => item.ToClass<CareMore.Web.DotCom.Models.Pages.EventPage>());*/

			var eventPages = Sitecore.Context.Item.Children
					.Where(item => item.TemplateID == new ID(Common.FolderTemplateID)) // Common 'Folder' template
					.Single().Axes.GetDescendants()
					.Where(item => (item.TemplateName == "Consumer Event Page" || item.TemplateName == "Stakeholder Event Page")).ToList()
					.ConvertAll(item => item.ToClass<CareMore.Web.DotCom.Models.Pages.EventPage>());

			// sort by date & exclude past events
			eventPages = (from x in eventPages
						  where calEvents.SelectedDate != DateTime.MinValue || (x.Date.HasValue && x.Date.Value >= DateTime.Now) // today or later (ignore past events). if date selected show any
						  orderby x.Date ascending
						  select x).ToList();

			return eventPages;
		}

		/// <summary>
		/// Bind events based on the selected date or month
		/// </summary>
		/// <param name="eventTypeName">Bind only this event type</param>
		private void BindEvents(string eventTypeName = "")
		{
			try
			{
				string selectedDate = Request.QueryString["selectedDate"];
				string selectedMonth = Request.QueryString["selectedMonth"];

				if (!string.IsNullOrEmpty(selectedDate))
				{
					calEvents.SelectedDate = System.Convert.ToDateTime(selectedDate);
					calEvents.VisibleDate = calEvents.SelectedDate;

					pnlSelectedDateMssg.Visible = true;
					Literal litShowingEventsForDate = pnlSelectedDateMssg.FindControl("litShowingEventsForDate") as Literal;
					litShowingEventsForDate.Text = calEvents.SelectedDate.ToLongDateString();
				}
				else
				{
					// if month selected
					if (!string.IsNullOrEmpty(selectedMonth))
					{
						// selected month
						DateTime selectedMonthDate = System.Convert.ToDateTime(selectedMonth);

						calEvents.VisibleDate = selectedMonthDate;
					}
					else
					{
						calEvents.VisibleDate = DateTime.Now;
					}
				}

				// Get all events in "Events" folder
				var eventPages = GetAllEvents();

				// filter by event type
				if (!string.IsNullOrEmpty(eventTypeName))
				{
					List<CareMore.Web.DotCom.Models.Pages.EventPage> filteredEvents = new List<CareMore.Web.DotCom.Models.Pages.EventPage>();
					foreach (var eventPage in eventPages)
					{
						MultilistField t = eventPage.InnerItem.Fields["Event Type"];
						foreach (var targetId in t.TargetIDs)
						{
							Item targetItem = Sitecore.Context.Database.GetItem(targetId);
							if (targetItem.Name == eventTypeName)
							{
								filteredEvents.Add(eventPage);
							}
						}
					}
					eventPages = filteredEvents;

					// preselect event type
					ddlEventTypes.Items.FindByValue(eventTypeName).Selected = true;
				}

				// selected date
				if (calEvents.SelectedDate != DateTime.MinValue)
				{
					List<CareMore.Web.DotCom.Models.Pages.EventPage> selectedDateEvents = new List<CareMore.Web.DotCom.Models.Pages.EventPage>();
					foreach (var eventPage in eventPages)
					{
						if(eventPage.Date.HasValue && eventPage.Date.Value.ToShortDateString() == calEvents.SelectedDate.Date.ToShortDateString())
						{
							selectedDateEvents.Add(eventPage);
						}
					}
					eventPages = selectedDateEvents;
				}

				lvEvents.DataSource = eventPages;
				lvEvents.DataBind();
			}
			catch (Exception exc)
			{
				Sitecore.Diagnostics.Log.Error("Cannot retrieve events", exc, this);
			}
		}

		#endregion

		#region Common events
		
		protected void lvEvents_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				ListViewDataItem LVDI = e.Item as ListViewDataItem;
				CareMore.Web.DotCom.Models.Pages.EventPage eventPage = LVDI.DataItem as CareMore.Web.DotCom.Models.Pages.EventPage;

				// Event Date
				DateTime? date = eventPage.Date;

				Literal litDate = (Literal)e.Item.FindControl("litDate");
				litDate.Text = date.HasValue ? date.Value.ToShortDateString() : "N/A";

				Literal litTime = (Literal)e.Item.FindControl("litTime");
				litTime.Text = date.HasValue ? date.Value.ToShortTimeString() : "";

				Literal litLocationName = (Literal)e.Item.FindControl("litLocationName");
				litLocationName.Text = eventPage.LocationName;

				Literal litLocationAddress = (Literal)e.Item.FindControl("litLocationAddress");
				litLocationAddress.Text = eventPage.LocationAddress;

				Literal litNameOfEvent = (Literal)e.Item.FindControl("litNameOfEvent");
				litNameOfEvent.Text = eventPage.Name;

				HyperLink hlDetails = (HyperLink)e.Item.FindControl("hlDetails");
				hlDetails.NavigateUrl = Sitecore.Links.LinkManager.GetItemUrl(eventPage.InnerItem);

				// Event types
				Literal litEventType = (Literal)e.Item.FindControl("litEventType");
				MultilistField eventType = eventPage.InnerItem.Fields["Event Type"];
				foreach (var targetId in eventType.TargetIDs)
				{
					if (litEventType.Text.Length > 0) litEventType.Text += ", ";
					litEventType.Text += Sitecore.Context.Database.GetItem(targetId).Name;
				}
			}
		}

		/// <summary>
		/// Event type selected changed event
		/// </summary>
		protected void ddlEventTypes_SelectedIndexChanged(object sender, EventArgs e)
		{
			string queryStringFormat = "{0}?selectedEventType={1}";

			string selectedDate = Request.QueryString["selectedDate"];
			if (!string.IsNullOrEmpty(selectedDate))
			{
				queryStringFormat += string.Format("&selectedDate={0}", selectedDate);
			}

			DropDownList ddl = sender as DropDownList;

			Response.Redirect(string.Format(queryStringFormat, Request.Url.AbsolutePath, ddl.SelectedValue));
		}

		/// <summary>
		/// Reset event date selection click event
		/// </summary>
		protected void lbEventDateReset_Click(Object sender, EventArgs e)
		{
			Response.Redirect(Request.Url.AbsolutePath);
		}

		#endregion

		#region Calendar Events

		/// <summary>
		/// Selected day rendering
		/// </summary>
		protected void calEvents_DayRender(object sender, DayRenderEventArgs e)
		{
			// Get all events in "Events" folder
			var eventPages = GetAllEvents();

			// If events found
			if (eventPages != null && eventPages.Count > 0)
			{
				foreach (var eventPage in eventPages)
				{
					// Mark event date
					if (eventPage.Date.HasValue && (eventPage.Date.Value.ToShortDateString() == e.Day.Date.ToShortDateString()))
					{
						e.Cell.Font.Bold = true;
						e.Cell.ForeColor = System.Drawing.Color.FromArgb(49, 130, 198);
					}
				}
			}
		}
		/// <summary>
		/// Selected calendar date
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void calEvents_SelectionChanged(object sender, EventArgs e)
		{
			Response.Redirect(string.Format("{0}?selectedDate={1}", Request.Url.AbsolutePath, calEvents.SelectedDate.ToShortDateString()));
		}
		/// <summary>
		/// Changed visible month on the calendar
		/// </summary>
		protected void calEvents_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
		{
			string queryStringFormat = "{0}?selectedMonth={1}";
			Response.Redirect(string.Format(queryStringFormat, Request.Url.AbsolutePath, e.NewDate.ToShortDateString()));
		}

		#endregion
	}
}