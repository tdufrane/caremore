﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MemberGrievanceForm.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.MemberGrievanceForm" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<script src="/js/jquery.maskedinput.js" type="text/javascript"></script>
<script src="/duals/duals.website/Assets/Js/colorbox.js" type="text/javascript"></script>
<link href="/duals/duals.website/Assets/Css/colorbox.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
	$j(function () {
		$j("#ZipCode").mask("99999");
		$j("#RepZipCode").mask("99999");
		$j("#HomePhone").mask("(999) 999-9999");
		$j("#WorkPhone").mask("(999) 999-9999");
		$j("#RepPhone").mask("(999) 999-9999");
		$j("#DateOfBirth").mask("99/99/9999");
		$j("#RepDate").mask("99/99/9999");
		$j("#SignatureDate").mask("99/99/9999");

		$j('.popInfo').colorbox({ inline: true, width: '950px'});
	});

	function CheckBoxRequired_ClientValidate(sender, e) {
		e.IsValid = jQuery(".AcceptedAgreement input:checkbox").is(':checked');
	}

	function Print() {
		try {
			var oIframe = $j("#ifrmPrint");
			var oDoc = (oIframe[0].contentWindow || oIframe[0].contentDocument);
			if (oDoc.document) oDoc = oDoc.document;
			oDoc.write("<html><head><title>Member Grievance Form</title>");
			oDoc.write("<link rel='stylesheet' href='/duals/duals.website/assets/css/style.css\' type='text/css' />");
			oDoc.write("</head><body onload='this.focus(); this.print();' style='font-size: 100%;'>");
			oDoc.write("<div id='grievForm' style='overflow: visible; height: auto;'>" + $j("#grievForm").html() + "</div>");
			oDoc.write("</body></html>");
			oDoc.close();
		}
		catch (e) {
			self.print();
		}
	}
</script>
<div class="span12">

		<!--START: Grievance Submission Form-->	
		<asp:Panel ID="RegPanel" runat="server" ClientIDMode="Static">
			<div class="masthead">
				<h1><sc:Text ID="Title" Field="Page Title" runat="server" /></h1>
			</div>
			<div>
				<sc:Text ID="FormHeader" Field="Form Header" runat="server" />
			</div>

			<div><asp:ValidationSummary ID="ValidationSummaryText" runat="server" DisplayMode="List" /></div>
			<div style="float:none; clear:both;">
			<p>(*) Indicates a Required field.</p></div>
			<div><asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label></div>

			<div class="form" id="grievanceForm">
				<div class="formField">
					<label for="LastName">Member Last Name*</label>
					<div class="field">
						<asp:TextBox ID="LastName" runat="server" ClientIDMode="Static" />
						<asp:RequiredFieldValidator ID="LastNameVal" runat="server" ErrorMessage="Member Last Name must be filled in." ControlToValidate="LastName" Display="None"></asp:RequiredFieldValidator>
					</div>
				</div>
				<div class="formField">
					<label for="FirstName">First*</label>
					<div class="field">
						<asp:TextBox ID="FirstName" runat="server" ClientIDMode="Static" />
						<asp:RequiredFieldValidator ID="FirstNameVal" runat="server" ErrorMessage="Member First Name must be filled in." ControlToValidate="FirstName" Display="None"></asp:RequiredFieldValidator>
					</div>
				</div>
				<div class="formField short">
					<label for="MiddleInitial">Middle Initial</label>
					<div class="field">
						<asp:TextBox ID="MiddleInitial" runat="server" ClientIDMode="Static" MaxLength="1" />
					</div>
				</div>
				<div class="formField long">
					<label for="Address">Address*</label>
					<div class="field">
						<asp:TextBox ID="Address" runat="server"  ClientIDMode="Static"/>
						<asp:RequiredFieldValidator ID="AddressVal" runat="server" ErrorMessage="Member Address must be filled in." ControlToValidate="Address" Display="None"></asp:RequiredFieldValidator>
					</div>
				</div>
				<div class="formField">
					<label for="City">City*</label>
					<div class="field">
						<asp:TextBox ID="City" runat="server"  ClientIDMode="Static" />
						<asp:RequiredFieldValidator ID="CityVal" runat="server" ErrorMessage="Member City must be filled in." ControlToValidate="City" Display="None"></asp:RequiredFieldValidator>
					</div>
				</div>
				<div class="formField">
					<label for="State">State*</label>
					<div class="field">
						<asp:TextBox ID="State" runat="server"  ClientIDMode="Static" />
						<asp:RequiredFieldValidator ID="StateVal" runat="server" ErrorMessage="Member State must be filled in." ControlToValidate="State" Display="None"></asp:RequiredFieldValidator>
					</div>
				</div>			
				<div class="formField short">
					<label for="ZipCode">Zip*</label>
					<div class="field">
						<asp:TextBox ID="ZipCode" runat="server" ClientIDMode="Static" />
						<asp:RequiredFieldValidator ID="ZipCodeVal" runat="server" ErrorMessage="Member Zip must be filled in." ControlToValidate="ZipCode" Display="None"></asp:RequiredFieldValidator>
					</div>
				</div>
				<div class="formField">
					<label for="HomePhone">Home Phone*</label>
					<div class="field">
						<asp:TextBox ID="HomePhone" runat="server" ClientIDMode="Static" />
						<asp:RequiredFieldValidator ID="HomePhoneVal" runat="server" ErrorMessage="Member Home Phone must be filled in." ControlToValidate="HomePhone" Display="None"></asp:RequiredFieldValidator>
					</div>
				</div>
				<div class="formField">
					<label for="WorkPhone">Work Phone</label>
					<div class="field">
						<asp:TextBox ID="WorkPhone" runat="server" ClientIDMode="Static" />
					</div>
				</div>
				<div class="formField">
					<label for="DateOfBirth">Date of Birth*</label>
					<div class="field">
						<asp:TextBox ID="DateOfBirth" runat="server" ClientIDMode="Static" />
						<asp:RequiredFieldValidator ID="DateOfBirthVal" runat="server" ErrorMessage="Member Date Of Birth must be filled in." ControlToValidate="DateOfBirth" Display="None"></asp:RequiredFieldValidator>
					</div>
				</div>
				<div class="formField">
					<label for="MemberID">Member ID*</label>
					<div class="field">
						<asp:TextBox ID="MemberID" runat="server" ClientIDMode="Static" />
						<asp:RequiredFieldValidator ID="MemberIDVal" runat="server" ErrorMessage="Member ID must be filled in." ControlToValidate="MemberID" Display="None"></asp:RequiredFieldValidator>
					</div>
				</div>
				<div class="clear label" ID="RepFillText" runat="server">
					<hr class="thin" />
					If the member isn't filling out this form, please fill-in the following information as the member's representative.
				</div>
				<div class="formField">
					<label for="RepName">Name</label>
					<div class="field">
						<asp:TextBox ID="RepName" runat="server" ClientIDMode="Static" />
					</div>
				</div>
				<div class="formField short">
					<label for="RepPhone">Telephone</label>
					<div class="field">
						<asp:TextBox ID="RepPhone" runat="server" ClientIDMode="Static" />
					</div>
				</div>
				<div class="formField">
					<label for="Relationship">Relationship to Member</label>
					<div class="field">
						<asp:TextBox ID="Relationship" runat="server"  ClientIDMode="Static" />
					</div>
				</div>
				<div class="formField long">
					<label for="RepAddress">Address</label>
					<div class="field">
						<asp:TextBox ID="RepAddress" runat="server"  ClientIDMode="Static" />
					</div>
				</div>
				<div class="formField">
					<label for="RepCity">City</label>
					<div class="field">
						<asp:TextBox ID="RepCity" runat="server"  ClientIDMode="Static" />
					</div>
				</div>
				<div class="formField">
					<label for="RepState">State</label>
					<div class="field">
						<asp:TextBox ID="RepState" runat="server"  ClientIDMode="Static" />
					</div>
				</div>			
				<div class="formField short">
					<label for="RepZipCode">Zip</label>
					<div class="field">
						<asp:TextBox ID="RepZipCode" runat="server" ClientIDMode="Static" />
					</div>
				</div>
				<div class="clear">
					<sc:Text ID="RepText" Field="Representative Text" runat="server" />
					<hr class="thin" />
				</div>
				<div class="formField long">
					<label for="Grievance" ID="GrievanceLabel" runat="server" class="grievanceLabel">Please share with us why you are filing this grievance or appeal.
					Please give us any information we need to know about your grievance or appeal.
					If this is a grievance, please tell us what happened, when and where it happened and who was there.</label>
					<div class="field">
						<asp:TextBox ID="Grievance" runat="server" ClientIDMode="Static" TextMode="MultiLine" Rows="3" />
					</div>
				</div>
				<div class="clear" ID="IllnessText" runat="server">
					<p>If the member’s illness is considered terminal and the denied service is considered experimental, 
					the member, treating provider, or the member’s representative has the right to contact CareMore to 
					discuss the denial.</p>
				</div>
				<div class="formField long">
					<div class="field inline">
						<asp:CheckBox ID="RequestConferenceCheck" runat="server" Text="Please check here if you would like to request a conference with CareMore to discuss the denial." ClientIDMode="Static" />
					</div>
				</div>
				<div class="formField long">
					<div class="field inline">
						<asp:CheckBox ID="ExpediteCheck" runat="server" Text="Please check here if you would like to file a fast (expedited) grievance or appeal." ClientIDMode="Static" />
					</div>
				</div>
				<div class="clear label" ID="SubmitByText" runat="server">
					<p>If submitting by mail or fax, please date and sign below. If you are not submitting by mail or fax, 
					please type in your name and the date and click on the submit button.</p>
				</div>
				<div class="formField leftLabel">
					<label for="Signature">Signature:</label>
					<div class="field">
						<asp:TextBox ID="Signature" runat="server" ClientIDMode="Static" />
					</div>
				</div>
				<div class="formField leftLabel smallField">
					<label for="SignatureDate">Date:</label>
					<div class="field">
						<asp:TextBox ID="SignatureDate" runat="server" ClientIDMode="Static" />
					</div>
				</div>
				<div class="formField leftLabel">
					<label for="Representative">Representative:</label>
					<div class="field">
						<asp:TextBox ID="Representative" runat="server" ClientIDMode="Static" />
					</div>
				</div>
				<div class="formField leftLabel smallField">
					<label for="RepDate">Date:</label>
					<div class="field">
						<asp:TextBox ID="RepDate" runat="server" ClientIDMode="Static" />
					</div>
				</div>
			
				<div class="clear"><sc:Text ID="FormFooter" Field="Submit Form Footer" runat="server" /></div>
			
				<div class="formField long">
					<div class="field inline">
						<asp:CheckBox ID="ConfirmInfoRead" ClientIDMode="Static" runat="server" CssClass="AcceptedAgreement" Text="I have reviewed this form and it is complete and accurate." />
						<asp:CustomValidator runat="server" ID="CheckBoxRequired" EnableClientScript="true" 
								OnServerValidate="CheckBoxRequired_ServerValidate"
								ClientValidationFunction="CheckBoxRequired_ClientValidate" ErrorMessage="You must acknowledge that you have reviewed the form.">You must select this box to submit the form.
						</asp:CustomValidator>
					</div>
				</div>

				<div class="clear"><sc:Text ID="SubmitDisclaimer" Field="Submit Disclaimer" runat="server" /></div>

				<div class="formField">
					<div class="field"><asp:LinkButton ID="SubmitBtn"  BackColor="Transparent" BorderStyle="None" CssClass="btn" runat="server" OnClick="SendMessageClick" Text="SUBMIT" /></div>
				</div>
			</div>
		</asp:Panel>
		<!--END: Grievance Submission Form-->
		
		<!--START: Grievance Confirm Popup-->
		<div style="display: none;">
			<asp:Panel CssClass="modalPopup" id="modalConfirmPopup" runat="server" ClientIDMode="Static" Visible="False">
				<div class="modalPopupInner" id="grievForm">
					<div class="brand"><sc:Image runat="server" ID="scLogo" Field="Logo Image" /></div>
					<div style="float:right; padding:10px;">
						<a href="#" class="link" onclick="Print()">PRINT</a>
					</div>
					<div style="clear:both; float:none; padding: 20px 0px;">
						<sc:Text ID="ConfirmMessage" Field="Confirmation Message" runat="server" />
						<strong>Confirmation #</strong> <asp:Label ID="ConfirmNumberLabel" runat="server" Text="Label"></asp:Label><br />
						<strong>Date Received</strong> <asp:Label ID="ConfirmDateLabel" runat="server" Text="Label"></asp:Label><br />
						<strong>Time Received</strong> <asp:Label ID="ConfirmTimeLabel" runat="server" Text="Label"></asp:Label>
					</div>	
					<h3><sc:Text ID="ConfirmTitle" Field="Page Title" runat="server" /></h3>
					<div><sc:Text ID="ConfHeader" Field="Form Header" runat="server" /></div>
					<div><asp:Literal ID="ConfirmFormContent" runat="server"></asp:Literal></div>
					<div><sc:Text ID="ConfFooter" Field="Confirm Form Footer" runat="server" /></div>
				</div>		
			</asp:Panel>
		</div>
		<iframe id='ifrmPrint' src="about:blank" style="width:0pt; height:0pt; border: none;"></iframe>

		<a class="button popInfo" href="#modalConfirmPopup" id="ConfirmAnchor" style="display:none">Confirm</a>  
		<!--END: Grievance Confirm Popup-->
	</div>
