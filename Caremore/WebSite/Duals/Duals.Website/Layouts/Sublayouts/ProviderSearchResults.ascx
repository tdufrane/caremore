﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProviderSearchResults.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.ProviderSearchResults" %>
<%@ Register TagPrefix="cm" TagName="Error" Src="~/Duals/Duals.Website/Controls/ErrorMessage.ascx" %>

<!-- Begin SubLayouts/ProviderSearchResults -->
<div class="span8">
	<h2 style="margin-top: 28px; float: left;"><asp:Literal ID="litTitle" runat="server"></asp:Literal></h2>

	<asp:PlaceHolder ID="NewSearchPH" runat="server">
		<div class="clearfix">
			<asp:LinkButton ID="NewSearchLB" runat="server" CssClass="btn newSearch"
				OnClick="NewSearchLB_OnClick" style="margin-top: 10px;" />
		</div>
	</asp:PlaceHolder>

	<div style="clear: both;"></div>

	<cm:Error ID="errorMessage" runat="server" Visible="false" />

	<asp:Panel ID="SearchResultsPnl" runat="server">
		<div class="searchResultsInfo clearfix">
			<div class="searchResultsNumberFound">
				<asp:Label ID="NumResultsLbl" runat="server" />
			</div>
			<div class="searchResultsPaging">
				<asp:DataPager ID="ItemsDP" runat="server" PagedControlID="ItemsLV" PageSize="25">
					<Fields>
						<asp:NextPreviousPagerField PreviousPageText="<<" ShowFirstPageButton="False" ShowPreviousPageButton="True"
							ShowNextPageButton="False" ShowLastPageButton="False"/>
						<asp:NumericPagerField />
						<asp:NextPreviousPagerField NextPageText=">>" ShowFirstPageButton="False" ShowPreviousPageButton="False"
							ShowNextPageButton="True" ShowLastPageButton="False" />
					</Fields>
				</asp:DataPager>
			</div>
		</div>

		<asp:ListView ID="ItemsLV" runat="server" OnPagePropertiesChanging="ItemsLV_PagePropertiesChanging"
			OnDataBound="ItemsLV_DataBound" OnItemDataBound="ItemsLV_ItemDataBound" OnLayoutCreated="ItemsLV_LayoutCreated">
			<LayoutTemplate>
				<table class="resultsTable hideOnSpinner">
					<tr>
						<th><asp:Label ID="lblPhysician" runat="server"></asp:Label></th>
						<th><asp:Label ID="lblSpecialties" runat="server" /></th>
						<th style="width:190px;"><asp:Label ID="lblMedicalOffice" runat="server" /></th>
					</tr>
					<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
				</table>
			</LayoutTemplate>
			<ItemTemplate>
				<tr id="trSeparator" runat="server" visible="false">
					<td colspan="3">
						<hr />
					</td>
				</tr>
				<tr id="trItemRow" runat="server">
					<td>
						<asp:Label ID="NameLbl" runat="server" />
						<asp:Label ID="IdLbl" runat="server" />
						<asp:Label ID="EffectiveLbl" runat="server" />
						<asp:Label ID="LanguagesLbl" runat="server" />
						<asp:Label ID="AcceptingNewPatientsLbl" runat="server" />
					</td>
					<td>
						<asp:Literal ID="SpecialtiesLit" runat="server"></asp:Literal>
					</td>
					<td id="tdAddress" runat="server">
						<asp:Literal ID="AddressLit" runat="server"></asp:Literal><br />
						<asp:Literal ID="DistanceLit" runat="server"></asp:Literal>
					</td>
				</tr>
			</ItemTemplate>
		</asp:ListView>

		<hr />

		<div class="searchResultsInfo clearfix">
			<div class="searchResultsNumberFound">
				<asp:Label ID="NumResultsLbl2" runat="server" />
			</div>
			<div class="searchResultsPaging">
				<asp:DataPager ID="ItemsDP2" runat="server" PagedControlID="ItemsLV" PageSize="25">
					<Fields>
						<asp:NextPreviousPagerField PreviousPageText="PREVIOUS PAGE" ShowFirstPageButton="False"
							ShowPreviousPageButton="True" ShowNextPageButton="False" ShowLastPageButton="False" />
						<asp:NumericPagerField />
						<asp:NextPreviousPagerField NextPageText="NEXT PAGE" ShowFirstPageButton="False"
							ShowPreviousPageButton="False" ShowNextPageButton="True" ShowLastPageButton="False" />
					</Fields>
				</asp:DataPager>
			</div>
		</div>

		<div class="disclaimer">
			<sc:Text ID="DisclaimerText" runat="server" Field="Disclaimer Text" />
		</div>

	</asp:Panel>
</div>
<!-- End SubLayouts/ProviderSearchResults -->
