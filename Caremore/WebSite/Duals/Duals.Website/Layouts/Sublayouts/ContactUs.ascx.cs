﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Sitecore.Links;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;

using Duals.Models.Content.Contact;

namespace Duals.Website.Layouts.Sublayouts
{
    public partial class ContactUs : System.Web.UI.UserControl
    {
        private Item currentItem = Sitecore.Context.Item;

        protected void Page_Load(object sender, EventArgs e)
        {
			FillContactDetails();
        }

		private void FillContactDetails()
		{
			Item[] contactUsItems = currentItem.Children.Where(item => item.TemplateID.ToString().Equals(ContactUsDetails.TemplateID)).ToArray();
			foreach (Item childItem in contactUsItems)
			{
				FieldRenderer textTop = ContactUsInfoTop as FieldRenderer;
				textTop.FieldName = ContactUsDetails.ContactInformationTop_FID;

				FieldRenderer textBottom = ContactUsInfoBottom as FieldRenderer;
				textBottom.FieldName = ContactUsDetails.ContactInformationBottom_FID;

				textTop.Item = textBottom.Item = childItem;
			}
		}
    }
}