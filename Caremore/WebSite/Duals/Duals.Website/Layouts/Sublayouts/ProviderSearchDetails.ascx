﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProviderSearchDetails.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.ProviderSearchDetails" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="cm" TagName="Error" Src="~/Duals/Duals.Website/Controls/ErrorMessage.ascx" %>
<%@ Register TagPrefix="gm" TagName="GoogleMap" Src="~/Duals/Duals.Website/Controls/GoogleMap.ascx" %>

<!-- Begin SubLayouts/ProviderSearchDetails -->
<div class="span8">
	<div id="mainCol" style="padding-top: 10px;">
		<div class="providerDetails">
			<div>
				<h1><asp:Label ID="PhysicianText" runat="server" /></h1>
			</div>

			<ul class="links horz">
				<li><asp:LinkButton ID="BackToResultsLB" runat="server" OnClick="BackToResultsLB_Click" CssClass="icon icoArrow icoArrowBack link">
					<sc:Text ID="BackToSearchLabel" runat="server" Field="Back To Search Label" /></asp:LinkButton></li>
				<li><a href="#" onclick="window.print()" class="link"><sc:Text ID="PrintLabel" runat="server" Field="Print Label" /></a></li>
			</ul>

			<hr />

			<asp:PlaceHolder ID="DetailsPH" runat="server">
				<div class="itemDetail">
					<div class="itemMap">
						<gm:GoogleMap ID="googleMap" runat="server" />
					</div>

					<div class="itemInfo">
						<h5><sc:Text ID="ContactInfoLabel" runat="server" Field="Contact Info Label" /></h5>

						<asp:Panel ID="PhonePnl" CssClass="info" runat="server">
							<em><sc:Text ID="PhoneLabel" runat="server" Field="Phone Label" />:</em>
							<asp:Label ID="PhoneText" runat="server" />
						</asp:Panel>
						<asp:Panel ID="FaxPnl" CssClass="info" runat="server">
							<em><sc:Text ID="FaxLabel" runat="server" Field="Fax Label" />:</em>
							<asp:Label ID="FaxText" runat="server" />
						</asp:Panel>

						<hr class="thin" />

						<h5><sc:Text ID="LocationLabel" runat="server" Field="Location Label" /></h5>

						<div class="info">
							<address style="font-style: normal;">
								<asp:Literal ID="LocationText" runat="server" />
							</address>
						</div>

						<p style="margin-top:20px;"><asp:HyperLink ID="DirectionsLink" CssClass="icon icoArrow link" runat="server" ClientIDMode="Static">
							<sc:Text ID="DirectionsLabel" runat="server" Field="Directions Label" /></asp:HyperLink></p>

						<hr class="thin" />

						<asp:Placeholder ID="BusinessHoursPH" runat="server" Visible="false">
							<h5><sc:Text ID="BusinessHoursLabel" runat="server" Field="Business Hours Label" /></h5>
							<div class="info">
								<asp:GridView ID="gvPrimaryHours" runat="server" AllowPaging="false" AllowSorting="false" AutoGenerateColumns="false" ShowHeader="false" GridLines="None" CssClass="hoursTable">
									<Columns>
										<asp:BoundField DataField="DaysOfWeek" ReadOnly="true" />
										<asp:BoundField DataField="FromHours" ReadOnly="true" />
										<asp:TemplateField>
											<ItemTemplate><%# FormatDivider(Eval("ToHours")) %></ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="ToHours" ReadOnly="true" />
										<asp:TemplateField>
											<ItemTemplate><%# Format2ndHours(Eval("FromHours2"), Eval("ToHours2")) %></ItemTemplate>
										</asp:TemplateField>
									</Columns>
								</asp:GridView>
							</div>

							<hr class="thin" />
						</asp:Placeholder>

						<div class="clear"></div>
					</div>

					<div class="providerContent">
						<asp:Panel ID="EffectivePnl" CssClass="row" runat="server" Visible="false">
							<div class="item left"><sc:Text ID="EffectiveDateLabel" runat="server" Field="Effective Date Label" /></div>
							<div class="item right">
								<asp:Label ID="EffectiveDateText" runat="server" />
							</div>
						</asp:Panel>

						<div class="row">
							<div class="item left"><sc:Text ID="AcceptLabel" runat="server" Field="Accepts New Patients Label" /></div>
							<div class="item right">
								<asp:Label ID="AcceptingNewPatientsText" runat="server" />
							</div>
						</div>

						<div class="row">
							<div class="item left"><sc:Text ID="SpecialtiesLabel" runat="server" Field="Specialties Label"/></div>
							<div class="item right">
								<asp:Label ID="SpecialtiesText" runat="server" />
							</div>
						</div>

						<div class="row">
							<div class="item left"><sc:Text ID="LanguagesLabel" runat="server" Field="Languages Label" /></div>
							<div class="item right">
								<asp:Label ID="LanguagesText" runat="server" />
							</div>
						</div>

						<div class="row">
							<div class="item left"><sc:Text ID="OnsiteInterpreterLabel" runat="server" Field="Onsite Interpreter Label" /></div>
							<div class="item right">
								<sc:Text ID="OnsiteInterpreterText" runat="server" Field="Onsite Interpreter Services" />
							</div>
						</div>

						<div class="row">
							<div class="item left"><sc:Text ID="AccessToLangLineLabel" runat="server" Field="Language Interpreters Label" /></div>
							<div class="item right">
								<sc:Text ID="AccessToLangLineText" runat="server" Field="Access to Language Line Interpreters" />
							</div>
						</div>

						<div class="row">
							<div class="item left">
								<div class="labelWithIcon"><sc:Text ID="AccessibilityLabel" runat="server" Field="Disability Access Label" /></div>
								<div class="labelIcon"><sc:Image ID="AccessibilityImg" runat="server" Field="Disability Access Icon" /></div>
								<div class="clear"></div>
							</div>
							<div class="item right">
								<asp:Label ID="AccessibilityType" runat="server" />
								<asp:Repeater ID="AccessibilityList" runat="server">
									<ItemTemplate><%# Eval("MappingCode") %></ItemTemplate><SeparatorTemplate>, </SeparatorTemplate>
								</asp:Repeater>
							</div>
						</div>

						<div class="row">
							<div class="item left">
								<div class="labelWithIcon"><sc:Text ID="PublicTransportationLabel" runat="server" Field="Public Transportation Label" /></div>
								<div class="labelIcon"><sc:Image ID="PublicTransportationImg" runat="server" Field="Public Transportation Icon"/></div>
							</div>
							<div class="item right">
								<asp:Label ID="PublicTransportationText" runat="server" />
							</div>
						</div>

						<div class="row">
							<div class="item left"><sc:Text ID="CulturalCompetencyLabel" runat="server" Field="Cultural Competency Label" /></div>
							<div class="item right">
								<sc:Text ID="CulturalCompetencyText" runat="server" Field="Cultural Competency Training" />
							</div>
						</div>

						<div class="row">
							<div class="item left"><sc:Text ID="SpecializedTrainingLabel" runat="server" Field="Specialized Training Label" /></div>
							<div class="item right">
								<asp:Label ID="SpecializedTrainingText" runat="server" />
							</div>
						</div>
 
						<div class="row">
							<div class="item left"><sc:Text ID="OtherCredsCertsLabel" runat="server" Field="Other Creds Certs Label" /></div>
							<div class="item right">
								<asp:Label ID="OtherCredsCertsText" runat="server" />
							</div>
						</div>
					</div>

					<asp:Repeater ID="MedicalOffices" runat="server" OnItemDataBound="MedicalOffices_ItemDataBound">
						<HeaderTemplate>
							<hr />
							<h5 style="margin-bottom:10px;"><sc:Text ID="MedicalOfficesLabel" runat="server" Field="Medical Offices Label" /></h5>
							<div class="providerContent">
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Panel ID="pnlState" runat="server" CssClass="state clear" Visible="false" />
							<asp:Panel ID="pnlCounty" runat="server" CssClass="county clear" Visible="false" />
							<div class="item address">
								<%# AddressHtml(Container.DataItem) %>
							</div>
						</ItemTemplate>
						<SeparatorTemplate>
							<hr />
						</SeparatorTemplate>
						<FooterTemplate>
							</div>
						</FooterTemplate>
					</asp:Repeater>

					<hr class="light" />

					<div class="legend">
						<sc:Text ID="LegendText" runat="server" Field="Legend" />
						<div class="clear"></div>
					</div>

					<div class="disclaimer">
						<sc:Text ID="DisclaimerText" runat="server" Field="Disclaimer Text" />
					</div>
				</div>
			</asp:PlaceHolder>
		</div>

		<cm:Error ID="errorMessage" runat="server" Visible="false" />
	</div>
</div>
<!-- End SubLayouts/ProviderSearchDetails -->
