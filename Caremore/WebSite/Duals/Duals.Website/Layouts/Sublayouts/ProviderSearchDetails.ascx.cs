﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using CareMore.Web.DotCom.Models.Providers;
using CareMore.Web.DotCom.Provider;

using ClassySC.Data;

using Duals.Business;
using Duals.Models;
using Duals.Models.Content.Articles;
using Duals.Website.Controls;

using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;

namespace Duals.Website.Layouts.Sublayouts
{
	public partial class ProviderSearchDetails : ProviderDetailsBase
	{
		#region Private variables

		private int addressCounter = 0;
		private string lastState = string.Empty;
		private string lastCounty = string.Empty;
		private string thisState = string.Empty;
		private string thisCounty = string.Empty;
		private string providerID = string.Empty;
		private string addressID = string.Empty;

		#endregion

		#region Page events

		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				providerID = Request.QueryString["id"];
				addressID = Request.QueryString["aid"];

				Guid sitecoreId;
				long prprId;

				if (Guid.TryParse(providerID, out sitecoreId))
				{
					GetItemFromSitecore();
				}
				else if (long.TryParse(providerID, out prprId))
				{
					GetItemFromDatabase(Request.QueryString["at"]);
				}
				else
				{
					ShowNotFound();
				}
			}
			catch (Exception ex)
			{
				ErrorMessage errorMessageCtl = (ErrorMessage)errorMessage;
				errorMessage.Visible = true;
#if DEBUG
				errorMessageCtl.SetError(ex);
#else
				errorMessageCtl.SetDownMessage(ex, DetailsPH);
#endif
			}
		}

		protected void MedicalOffices_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			switch (e.Item.ItemType)
			{
				case ListItemType.AlternatingItem:
				case ListItemType.Item:
					thisState = ((ProviderLocation)e.Item.DataItem).StateName;
					thisCounty = ((ProviderLocation)e.Item.DataItem).County;

					if (!thisState.Equals(lastState))
					{
						Panel pnlState = (Panel)e.Item.FindControl("pnlState");
						pnlState.Controls.Add(new LiteralControl(thisState));
						pnlState.Visible = true;

						Panel pnlCounty = (Panel)e.Item.FindControl("pnlCounty");
						pnlCounty.Controls.Add(new LiteralControl(thisCounty));
						pnlCounty.Visible = true;

						// Reset counters
						lastState = thisState;
						lastCounty = thisCounty;
						addressCounter = 0;
					}
					else if (!thisCounty.Equals(lastCounty))
					{
						Panel pnlCounty = (Panel)e.Item.FindControl("pnlCounty");
						pnlCounty.Controls.Add(new LiteralControl(thisCounty));
						pnlCounty.Visible = true;

						// Reset counters
						lastCounty = thisCounty;
						addressCounter = 0;
					}

					addressCounter++;
					break;

				case ListItemType.Separator:
					if ((addressCounter % 2) != 0)
						e.Item.Visible = false;
					break;

				default:
					break;
			}
		}

		protected void BackToResultsLB_Click(object sender, EventArgs e)
		{
			Item resultsPage = Sitecore.Context.Item.Parent.GetChildren().Where(i => i.Name == "Results").FirstOrDefault();
			Response.Redirect(Sitecore.Links.LinkManager.GetItemUrl(resultsPage) + "?c=true");
		}

		protected void NewSearchLB_OnClick(object sender, EventArgs e)
		{
			Item searchPage = Sitecore.Context.Item.Parent;
			Response.Redirect(Sitecore.Links.LinkManager.GetItemUrl(searchPage));
		}

		#endregion

		#region Protected methods

		protected string AddressHtml(object item)
		{
			ProviderLocation location = (ProviderLocation)item;

			StringBuilder medOfficeText = new StringBuilder();
			medOfficeText.Append(LocationHelper.GetFormattedAddress(location.Address1, location.Address2,
				location.City, location.StateName, location.Zip,
				string.Format(ProviderHelper.DetailsUrlFormat, "Details.aspx", providerID, addressID, location.AddressType.Trim()),
				"Show Details", false));

			if (!string.IsNullOrEmpty(location.Phone))
				medOfficeText.AppendFormat("<br />Ph: {0}", LocationHelper.GetFormattedPhoneNo(location.Phone));

			if (!string.IsNullOrEmpty(location.Fax))
				medOfficeText.AppendFormat("<br />Fax: {0}", LocationHelper.GetFormattedPhoneNo(location.Fax));

			return medOfficeText.ToString();
		}

		protected string FormatDivider(object toHours)
		{
			if ((toHours == null) || (string.IsNullOrWhiteSpace(toHours.ToString())))
				return string.Empty;
			else
				return "-";
		}

		protected string Format2ndHours(object fromHours, object toHours)
		{
			return LocationHelper.GetFormatted2ndHours(fromHours.ToString(), toHours.ToString());
		}

		#endregion

		#region Private methods

		private void GetItemFromDatabase(string addressType)
		{
			List<ProviderSearchDetail> details = ProviderFinder.GetProviderDetail(ProviderHelper.GetPortalCode(Duals.Models.GlobalConsts.SiteSettingsID),
				providerID, addressType);

			if (details.Count == 0)
			{
				ShowNotFound();
			}
			else
			{
				ShowDetail(details,
					ProviderFinder.GetProviderHours(providerID, addressID, addressType, details[0].AddressEffectiveDate),
					GetProviderLanguages(providerID, addressID, addressType),
					ProviderFinder.GetProviderAccessibility(providerID, addressID, addressType),
					false);

				// Additional Locations
				BindAddresses(addressType);
			}
		}

		private void GetItemFromSitecore()
		{
			Item providerItem = Sitecore.Context.Database.GetItem(providerID, Sitecore.Globalization.Language.Parse("en"));

			if (providerItem == null)
			{
				ShowNotFound();
			}
			else
			{
				ProviderSearchDetail detail = new ProviderSearchDetail();

				detail.Accessibility = !string.IsNullOrEmpty(providerItem["Accessibility"]);
				detail.Address1 = providerItem["Address Line 1"];
				detail.Address2 = providerItem["Address Line 2"];
				detail.City = providerItem["City"];
				detail.County = providerItem["County"];
				detail.Fax = providerItem["Fax"];

				if (string.IsNullOrWhiteSpace(providerItem["Place"]))
					detail.Name = providerItem["Provider Name"];
				else
					detail.Name = providerItem["Place"];

				detail.NpiId = providerItem["NPI ID"];
				detail.Phone = providerItem["Phone"];
				detail.ProviderId = providerItem["Provider ID"];
				detail.State = providerItem["State"];
				detail.Zip = providerItem["Postal Code"];

				detail.Transportation = providerItem["Transportation"];
				detail.SpecializedTraining = providerItem["Specialized Training"];
				detail.CredentialingStatus = providerItem["Other Credentials"];

				if (string.IsNullOrEmpty(providerItem["Subtype"]))
					detail.PrimarySpecialty = Sitecore.Context.Item["Facility Type"];
				else
					detail.PrimarySpecialty = providerItem["Subtype"];

				detail.AcceptingPatients = providerItem["Accepting New Patients"].Equals("1") ? "Y" : "N";
				detail.PanelClosedBy = string.Empty;

				List<ProviderSearchDetail> details = new List<ProviderSearchDetail>();
				details.Add(detail);

				ShowDetail(details,
					ProviderHelper.GetHoursOfOperation(providerItem["Hours of Operation"]),
					ProviderHelper.GetLanguages(providerItem["Languages"]),
					ProviderHelper.GetAccessibilities(providerItem["Accessibility"]),
					true);
			}
		}

		private void ShowDetail(List<ProviderSearchDetail> details, List<ProviderHour> hours, string languages, List<ProviderAccessibility> accessibilities, bool fromSitecore)
		{
			ProviderSearchDetail detail = details[0];

			// Name and ID
			if (string.IsNullOrWhiteSpace(detail.NpiId))
				PhysicianText.Text = string.Format("{0}", detail.Name);
			else
				PhysicianText.Text = string.Format("{0}<br />NPI ID: {1}", detail.Name, detail.NpiId);

			PhoneText.Text = LocationHelper.GetFormattedPhoneNo(detail.Phone);
			FaxText.Text = LocationHelper.GetFormattedPhoneNo(detail.Fax);

			// Address
			LocationText.Text = LocationHelper.GetFormattedAddress(detail.Address1, detail.Address2,
				detail.City, detail.State, detail.Zip);

			// Google Directions
			DirectionsLink.NavigateUrl = ProviderHelper.GetMapUrl(detail);
			DirectionsLink.ToolTip = "To get directions using public transportation, enter your address in Google and click the BUS icon.";
			DirectionsLink.Target = "_blank";

			// Google Map
			string locationAddresses = string.Format("\"{0} {1}, {2} {3}|{4}|{5}|{6}|{7}|{8}\"",
				detail.Address1 ?? string.Empty,
				detail.Address2 ?? string.Empty,
				detail.City ?? string.Empty,
				detail.Zip ?? string.Empty,
				detail.Name ?? string.Empty,
				Common.GetKeyImagePath(Sitecore.Context.Item),
				string.Empty,
				LocationHelper.GetFormattedPhoneNo(detail.Phone) ?? string.Empty,
				string.Empty);

			GoogleMap gMap = (GoogleMap)googleMap;
			gMap.SetLocationsArray(locationAddresses);
			gMap.SetZoomLevel(15);
			gMap.DisplayMap();

			// Business Hours
			if (hours != null && hours.Any())
			{
				gvPrimaryHours.DataSource = hours;
				gvPrimaryHours.DataBind();

				BusinessHoursPH.Visible = true;
			}

			// Effective date
			if (detail.EffectiveDate > DateTime.Now.Date)
			{
				EffectivePnl.Visible = true;
				EffectiveDateText.Text = detail.EffectiveDate.ToString("MM/dd/yyyy");
			}

			// Accessibility
			if (fromSitecore)
			{
				AccessibilityType.Visible = false;
				AccessibilityList.DataSource = accessibilities;
				AccessibilityList.DataBind();

				PublicTransportationText.Text = detail.Transportation.Equals("1") ? "Yes" : "No";

				SpecializedTrainingText.Text = detail.SpecializedTraining.Equals("1") ? "Yes" : "No";
				OtherCredsCertsText.Text = detail.CredentialingStatus;
			}
			else
			{
				SetAccessibilityDb(accessibilities, AccessibilityType, AccessibilityList);

				if (string.IsNullOrWhiteSpace(detail.Transportation))
					PublicTransportationText.Text = string.Empty;
				else if (ProviderHelper.IsInDistance(detail.Transportation, decimal.Parse(Sitecore.Context.Item["Max Transportation Distance"])))
					PublicTransportationText.Text = "Yes";
				else
					PublicTransportationText.Text = "No";

				SpecializedTrainingText.Text = detail.SpecializedTraining;

				if (detail.CredentialingStatus.Equals("C"))
					OtherCredsCertsText.Text = Sitecore.Context.Item["Certified Text"];
				else if (detail.CredentialingStatus.Equals("N"))
					OtherCredsCertsText.Text = "Other";
			}

			//Specialties
			SpecialtiesText.Text = detail.PrimarySpecialty;

			if (!string.IsNullOrEmpty(detail.SecondarySpecialty))
			{
				SpecialtiesText.Text += "<br />" + detail.SecondarySpecialty;
			}

			LanguagesText.Text = languages;

			if (detail.AcceptingPatients.Equals("Y"))
				AcceptingNewPatientsText.Text = "Yes";
			else if (detail.PanelClosedBy.Equals("MD") || detail.PanelClosedBy.Equals("MMPM"))
				AcceptingNewPatientsText.Text = "Existing patients only";
			else
				AcceptingNewPatientsText.Text = "No";
		}

		private void BindAddresses(string addressType)
		{
			List<ProviderLocation> results = ProviderFinder.GetProviderLocations(addressID, GlobalConsts.DefaultStateName).ToList();

			// Ensure excluding any addresses
			results = FilterExclusions(results);

			if (results.Count < 2)
			{
				this.MedicalOffices.Visible = false;
			}
			else
			{
				this.MedicalOffices.DataSource = results;
				this.MedicalOffices.DataBind();
			}
		}

		private List<ProviderLocation> FilterExclusions(List<ProviderLocation> list)
		{
			Item resultsItem = Sitecore.Context.Item.Parent.GetChildren().Where(i => i.Name == "Results").FirstOrDefault();
			ProviderListArticle currentPage = new ProviderListArticle(resultsItem);
			ProviderListing listing = new ProviderListing(currentPage.ProviderListType);
			List<string> providerExclusions = ProviderHelper.GetExclusions(listing.Exclusions);

			foreach (string exclusion in providerExclusions)
			{
				IEnumerable<ProviderLocation> filteredList;

				if (exclusion.Contains('='))
				{
					filteredList = list.Where(item => (string.Format("{0}={1}", providerID, item.AddressType.Trim()) != exclusion));
				}
				else
				{
					filteredList = list.Where(item => (providerID != exclusion));
				}

				list = filteredList.ToList();
			}

			return list;
		}

		private void ShowNotFound()
		{
			ErrorMessage errorMessageCtl = (ErrorMessage)errorMessage;
			errorMessageCtl.SetNoResultMessage(DetailsPH);
			errorMessage.Visible = true;
		}

		#endregion
	}
}
