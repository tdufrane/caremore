﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventList.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.EventList" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div id="secondaryPageContentWrap">

	<div class="boxNoBack" style="float:left;padding-left: 20px;margin-bottom: 10px;width:580px;">

		<asp:Panel Visible="false" runat="server" ID="pnlSelectedDateMssg">
			<div style="border:1px solid #eee;background-color:#f4f4f4;color:#333;padding:10px;margin-bottom:10px;">
				Showing events for <asp:Literal runat="server" ID="litShowingEventsForDate" />
				<span style="float:right;">
					<asp:LinkButton runat="server" ID="lbEventDateReset" Text="Reset" OnClick="lbEventDateReset_Click" />
				</span>
			</div>
		</asp:Panel>

		<div class="boxNoBack eventsList" style="float: left; margin-bottom: 10px; width: 310px;">

			<label for="dualsmaincontainer_0_dualsmaincontent_2_ddlEventTypes">Show Event Type:</label>
			<asp:DropDownList runat="server" ID="ddlEventTypes" AutoPostBack="true"
				onselectedindexchanged="ddlEventTypes_SelectedIndexChanged" style="padding:3px;font-size:1.05em;" />

			<a class="accessible" href="#main">[Skip to Content]</a>
			<asp:ListView runat="server" ID="lvEvents" OnItemDataBound="lvEvents_ItemDataBound">
	
				<LayoutTemplate>
					<table>
						<tbody>
							<asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
						</tbody>
					</table>
				</LayoutTemplate>

				<ItemTemplate>
					<tr>
						<td style="vertical-align:top;border-top:1px solid #dddddd;">
							<div style="margin-top:6px;"><asp:Literal runat="server" ID="litDate" /> <asp:Literal runat="server" ID="litTime" /></div>
							<strong><asp:Literal runat="server" ID="litNameOfEvent" /></strong><br />
							<div style="margin:6px 0;">
								<asp:Literal runat="server" ID="litEventType" />
							</div>
							<asp:Literal runat="server" ID="litLocationName" /><br />
							<asp:Literal runat="server" ID="litLocationAddress" />
							<div style="margin:12px 0 6px 0;">
								<asp:HyperLink runat="server" ID="hlDetails" Text="See Details" />
							</div>
						</td>
					</tr>
				</ItemTemplate>

				<EmptyDataTemplate>
					<div style="padding:30px;text-align:center;">
						No events found!
					</div>
				</EmptyDataTemplate>

			</asp:ListView>

		</div>

		<div class="boxNoBack" style="float: left; padding-left: 20px; margin-bottom: 10px; margin-top:26px; width: 250px;">
		
			<asp:Calendar 
				ID="calEvents" 
				ViewStateMode="Disabled"
				runat="server"
				BorderStyle="Solid" 
				CellPadding="3" 
				ShowTitle="true" 
				TitleStyle-BackColor="white"
				BorderColor="#D6E7F7" 
				BackColor="White" 
				CssClass="calendarWidget" 
				TitleFormat="Month" 
				NextPrevStyle-CssClass="calNextPrev"
				SelectedDayStyle-CssClass="calSelDay" 
				SelectedDayStyle-BackColor="#D6E7F7"
				NextPrevStyle-ForeColor="#AAD4FF" 
				TitleStyle-CssClass="calTitle" 
				DayStyle-CssClass="dayStyle" 
				OnDayRender="calEvents_DayRender" 
				onselectionchanged="calEvents_SelectionChanged" 
				onvisiblemonthchanged="calEvents_VisibleMonthChanged" 
				NextPrevFormat="FullMonth" />

		</div>


	</div>

</div>