﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentBoxWrapper.ascx.cs" Inherits="Duals.Website.Layouts.Sublayouts.ContentBoxWrapper" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="row homeFeatures">
	<sc:Placeholder runat="server" Key="duals-contentboxwrapper" />
</div>