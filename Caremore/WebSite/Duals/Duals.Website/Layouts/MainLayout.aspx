<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainLayout.aspx.cs" Inherits="Duals.Website.Layouts.MainLayout" %>
<%--<%@ Register TagPrefix="mt" Namespace="Revere.West.SC.WebControls.MetaTags" Assembly="MetaTags" %>--%>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title><%= PageTitle %></title>
	<!-- css -->
	<link href="//fonts.googleapis.com/css?family=Merriweather:700,400" rel="stylesheet" type="text/css" />
	<link href="/duals/duals.website/assets/css/glyphicons.css" rel="stylesheet" type="text/css" />
	<link href="/duals/duals.website/assets/css/coda-slider.css" rel="stylesheet" type="text/css" />
	<link href="/duals/duals.website/assets/css/style.css" rel="stylesheet" type="text/css" />
	<link href="/duals/duals.website/assets/css/style-large.css" rel="stylesheet" type="text/css" title="large" />
	<!-- js -->
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/duals/duals.website/assets/js/jquery.coda-slider-3.0.js"></script>
	<script type="text/javascript" src="/duals/duals.website/assets/js/global.js"></script>
	<script type="text/javascript" src="/duals/duals.website/assets/js/jwplayer.js"></script>
	<style type="text/css">
		<!--[if IE]>
		.content { margin-right: -1px; } /* this 1px negative margin can be placed on any of the columns in this layout with the same corrective effect. */
		.container { border-left: solid 1px #e0e0e0; border-right: solid 1px #e0e0e0; }
		ul.nav a { zoom: 1; }  /* the zoom property gives IE the hasLayout trigger it needs to correct extra whiltespace between the links */
		<![endif]-->
	</style>
	<!--[if lt IE 9]>
		<script src="/duals/duals.website/assets/js/html5shiv.js"></script>
	<![endif]-->
</head>
<body>
	<form id="MainForm" runat="server">
		<div class="container">

			<sc:Sublayout runat="server" Path="~/Duals/Duals.Website/Layouts/Sublayouts/Header.ascx" />

			<div class="mainContainer">

				<!-- begin main container -->
				<sc:Placeholder runat="server" Key="duals-maincontainer" />
				<!-- end main container -->

			</div>
		</div>
	</form>

<asp:PlaceHolder ID="plcGoogleAnalytics" runat="server">
	<script type="text/javascript">
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-50599473-1', 'caremore.com');
		ga('send', 'pageview');
	</script>
</asp:PlaceHolder>

</body>
</html>
