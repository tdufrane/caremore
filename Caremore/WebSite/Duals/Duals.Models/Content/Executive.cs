﻿using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Content {
    
    
    public partial interface IExecutive {
        
        // Field is Name, but use ExecutiveName to avoid collision with Item.Name
        string ExecutiveName {
            get;
            set;
        }
        
        string Title {
            get;
            set;
        }
        
        string Description {
            get;
            set;
        }
        
        Sitecore.Data.Fields.ImageField Picture {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Content.Executive.TemplateID)]
    public class Executive : StandardTemplate, Duals.Models.Content.IExecutive {
        
        #region Members
        public const string TemplateID = "{41A406E4-174A-4910-9FE0-EF7801F6EC5A}";
        
        public const string Name_FID = "{0DE01860-1949-4F1F-8EB5-3C3326DF4448}";
        
        public const string Title_FID = "{3B63ADA1-5E86-4694-AE4E-89871B725D62}";
        
        public const string Description_FID = "{7D7EE6A4-A595-464A-AF4D-325EAA6B4835}";
        
        public const string Picture_FID = "{A70F0FED-AE60-4082-8596-C13D8641033C}";
        
        private string _name;
        
        private string _title;
        
        private string _description;
        #endregion
        
        #region Constructors
        
        public Executive(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        
        [Field(Duals.Models.Content.Executive.Name_FID)]
        public virtual string ExecutiveName {
            get {
                if (_name == null) {
_name = this.GetString(Duals.Models.Content.Executive.Name_FID);
                }
                return _name;
            }
            set {
                _name = null;
                this.SetString(Duals.Models.Content.Executive.Name_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Executive.Title_FID)]
        public virtual string Title {
            get {
                if (_title == null) {
_title = this.GetString(Duals.Models.Content.Executive.Title_FID);
                }
                return _title;
            }
            set {
                _title = null;
                this.SetString(Duals.Models.Content.Executive.Title_FID, value);
            }
        }

        [Field(Duals.Models.Content.Executive.Description_FID)]
        public virtual string Description {
            get {
                if (_description == null) {
_description = this.GetString(Duals.Models.Content.Executive.Description_FID);
                }
                return _description;
            }
            set {
                _description = null;
                this.SetString(Duals.Models.Content.Executive.Description_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Executive.Picture_FID)]
        public virtual ImageField Picture {
            get {
                return this.GetField<ImageField>(Duals.Models.Content.Executive.Picture_FID);
            }
            set {
                this.SetField<ImageField>(Duals.Models.Content.Executive.Picture_FID, value);
            }
        }
        #endregion
    }
}
