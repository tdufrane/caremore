﻿using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using ClassySC.Data;

namespace Duals.Models.Content {
    
    
    public partial interface IRecipeGroup {
        
        string RecipesGroupTitle {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Content.RecipeGroup.TemplateID)]
    public class RecipeGroup : StandardTemplate, Duals.Models.Content.IRecipeGroup {
        
        #region Members
        public const string TemplateID = "{56A23FA0-FC48-45EB-AD52-B559052F9D8A}";
        
        public const string RecipesGroupTitle_FID = "{4000B7B4-8F00-41F6-9043-98CCC12205B4}";
        
        private string _recipesGroupTitle;
        #endregion
        
        #region Constructors
        
        public RecipeGroup(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        
        [Field(Duals.Models.Content.RecipeGroup.RecipesGroupTitle_FID)]
        public virtual string RecipesGroupTitle {
            get {
                if (_recipesGroupTitle == null) {
_recipesGroupTitle = this.GetString(Duals.Models.Content.RecipeGroup.RecipesGroupTitle_FID);
                }
                return _recipesGroupTitle;
            }
            set {
                _recipesGroupTitle = null;
                this.SetString(Duals.Models.Content.RecipeGroup.RecipesGroupTitle_FID, value);
            }
        }
        #endregion
    }
}
