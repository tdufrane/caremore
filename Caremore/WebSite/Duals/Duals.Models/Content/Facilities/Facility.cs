using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Content.Facilities {
    
    
    public partial interface IFacility {
        
        string FacilityName {
            get;
            set;
        }
        
        IEnumerable<Item> Services {
            get;
            set;
        }
        
        string Language {
            get;
            set;
        }
        
        string HoursOfOperation {
            get;
            set;
        }
        
        string HandicapAccessibility {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Content.Facilities.Facility.TemplateID)]
    public partial class Facility : StandardTemplate, Duals.Models.Content.Facilities.IFacility, Duals.Models.Content.Contact.ILocation, Duals.Models.Content.Contact.IContactInformation {
        
        #region Members
        public const string TemplateID = "{ABD4ACF1-91A9-4BA2-8BA1-2F952489A1D9}";
        
        public const string FacilityName_FID = "{549083BF-D19B-4AB0-ABCE-A202D06993E1}";
        
        public const string HoursOfOperation_FID = "{EF606BDD-0D33-4DF9-807D-C39857D0FE57}";
        
        public const string HandicapAccessibility_FID = "{716A8F3F-B24E-4A40-9752-2CF54E9D4112}";
        
        public const string Language_FID = "{C29BB150-C4FF-4A24-B125-E27C6FCB5080}";
        
        public const string Services_FID = "{0F711D9F-83B6-4A9C-A0B4-1A7314F7BBDA}";
        
        private string _facilityName;
        
        private IEnumerable<Item> _services;
        
        private string _language;
        
        private string _hoursOfOperation;
        
        private string _handicapAccessibility;
        
        private string _name;
        
        private string _addressLine1;
        
        private string _addressLine2;
        
        private string _city;
        
        private string _state;
        
        private string _postalCode;
        
        private string _county;
        
        private string _latitude;
        
        private string _longitude;
        
        private string _phone;
        
        private string _fax;
        #endregion
        
        #region Constructors
        public Facility(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Content.Facilities.Facility.FacilityName_FID)]
        public virtual string FacilityName {
            get {
                if (_facilityName == null) {
_facilityName = this.GetString(Duals.Models.Content.Facilities.Facility.FacilityName_FID);
                }
                return _facilityName;
            }
            set {
                _facilityName = null;
                this.SetString(Duals.Models.Content.Facilities.Facility.FacilityName_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Facilities.Facility.Services_FID)]
        public virtual IEnumerable<Item> Services {
            get {
                if (_services == null) {
_services = this.GetItems(Duals.Models.Content.Facilities.Facility.Services_FID);
                }
                return _services;
            }
            set {
                _services = null;
                this.SetItems(Duals.Models.Content.Facilities.Facility.Services_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Facilities.Facility.Language_FID)]
        public virtual string Language {
            get {
                if (_language == null) {
_language = this.GetString(Duals.Models.Content.Facilities.Facility.Language_FID);
                }
                return _language;
            }
            set {
                _language = null;
                this.SetString(Duals.Models.Content.Facilities.Facility.Language_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Facilities.Facility.HoursOfOperation_FID)]
        public virtual string HoursOfOperation {
            get {
                if (_hoursOfOperation == null) {
_hoursOfOperation = this.GetString(Duals.Models.Content.Facilities.Facility.HoursOfOperation_FID);
                }
                return _hoursOfOperation;
            }
            set {
                _hoursOfOperation = null;
                this.SetString(Duals.Models.Content.Facilities.Facility.HoursOfOperation_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Facilities.Facility.HandicapAccessibility_FID)]
        public virtual string HandicapAccessibility {
            get {
                if (_handicapAccessibility == null) {
_handicapAccessibility = this.GetString(Duals.Models.Content.Facilities.Facility.HandicapAccessibility_FID);
                }
                return _handicapAccessibility;
            }
            set {
                _handicapAccessibility = null;
                this.SetString(Duals.Models.Content.Facilities.Facility.HandicapAccessibility_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.Location.Name_FID)]
        public new virtual string Name {
            get {
                if (_name == null) {
_name = this.GetString(Duals.Models.Content.Contact.Location.Name_FID);
                }
                return _name;
            }
            set {
                _name = null;
                this.SetString(Duals.Models.Content.Contact.Location.Name_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.Location.AddressLine1_FID)]
        public virtual string AddressLine1 {
            get {
                if (_addressLine1 == null) {
_addressLine1 = this.GetString(Duals.Models.Content.Contact.Location.AddressLine1_FID);
                }
                return _addressLine1;
            }
            set {
                _addressLine1 = null;
                this.SetString(Duals.Models.Content.Contact.Location.AddressLine1_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.Location.AddressLine2_FID)]
        public virtual string AddressLine2 {
            get {
                if (_addressLine2 == null) {
_addressLine2 = this.GetString(Duals.Models.Content.Contact.Location.AddressLine2_FID);
                }
                return _addressLine2;
            }
            set {
                _addressLine2 = null;
                this.SetString(Duals.Models.Content.Contact.Location.AddressLine2_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.Location.City_FID)]
        public virtual string City {
            get {
                if (_city == null) {
_city = this.GetString(Duals.Models.Content.Contact.Location.City_FID);
                }
                return _city;
            }
            set {
                _city = null;
                this.SetString(Duals.Models.Content.Contact.Location.City_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.Location.State_FID)]
        public virtual string State {
            get {
                if (_state == null) {
_state = this.GetString(Duals.Models.Content.Contact.Location.State_FID);
                }
                return _state;
            }
            set {
                _state = null;
                this.SetString(Duals.Models.Content.Contact.Location.State_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.Location.PostalCode_FID)]
        public virtual string PostalCode {
            get {
                if (_postalCode == null) {
_postalCode = this.GetString(Duals.Models.Content.Contact.Location.PostalCode_FID);
                }
                return _postalCode;
            }
            set {
                _postalCode = null;
                this.SetString(Duals.Models.Content.Contact.Location.PostalCode_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.Location.County_FID)]
        public virtual string County {
            get {
                if (_county == null) {
_county = this.GetString(Duals.Models.Content.Contact.Location.County_FID);
                }
                return _county;
            }
            set {
                _county = null;
                this.SetString(Duals.Models.Content.Contact.Location.County_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.Location.Latitude_FID)]
        public virtual string Latitude {
            get {
                if (_latitude == null) {
_latitude = this.GetString(Duals.Models.Content.Contact.Location.Latitude_FID);
                }
                return _latitude;
            }
            set {
                _latitude = null;
                this.SetString(Duals.Models.Content.Contact.Location.Latitude_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.Location.Longitude_FID)]
        public virtual string Longitude {
            get {
                if (_longitude == null) {
_longitude = this.GetString(Duals.Models.Content.Contact.Location.Longitude_FID);
                }
                return _longitude;
            }
            set {
                _longitude = null;
                this.SetString(Duals.Models.Content.Contact.Location.Longitude_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.ContactInformation.Phone_FID)]
        public virtual string Phone {
            get {
                if (_phone == null) {
_phone = this.GetString(Duals.Models.Content.Contact.ContactInformation.Phone_FID);
                }
                return _phone;
            }
            set {
                _phone = null;
                this.SetString(Duals.Models.Content.Contact.ContactInformation.Phone_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.ContactInformation.Fax_FID)]
        public virtual string Fax {
            get {
                if (_fax == null) {
_fax = this.GetString(Duals.Models.Content.Contact.ContactInformation.Fax_FID);
                }
                return _fax;
            }
            set {
                _fax = null;
                this.SetString(Duals.Models.Content.Contact.ContactInformation.Fax_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.ContactInformation.Email_FID)]
        public virtual LinkField Email {
            get {
                return this.GetField<LinkField>(Duals.Models.Content.Contact.ContactInformation.Email_FID);
            }
            set {
                this.SetField<LinkField>(Duals.Models.Content.Contact.ContactInformation.Email_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.ContactInformation.Website_FID)]
        public virtual LinkField Website {
            get {
                return this.GetField<LinkField>(Duals.Models.Content.Contact.ContactInformation.Website_FID);
            }
            set {
                this.SetField<LinkField>(Duals.Models.Content.Contact.ContactInformation.Website_FID, value);
            }
        }
        #endregion
    }
}
