using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Content.Contact {
    
    
    public partial interface IContactUsDetails {
        
        string ContactInformationTop {
            get;
            set;
        }
        
        string ContactInformationBottom {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Content.Contact.ContactUsDetails.TemplateID)]
    public partial class ContactUsDetails : StandardTemplate, Duals.Models.Content.Contact.IContactUsDetails {
        
        #region Members
        public const string TemplateID = "{9F4B6611-7270-49E3-AB19-B7B233B0DB87}";
        
        public const string ContactInformationTop_FID = "{B68F5926-4E50-4CD2-8DC6-E3F1050F6ABD}";
        
        public const string ContactInformationBottom_FID = "{798033CA-D483-4E50-A77B-E48CD8D3BC13}";
        
        private string _contactInformationTop;
        
        private string _contactInformationBottom;
        #endregion
        
        #region Constructors
        public ContactUsDetails(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Content.Contact.ContactUsDetails.ContactInformationTop_FID)]
        public virtual string ContactInformationTop {
            get {
                if (_contactInformationTop == null) {
_contactInformationTop = this.GetString(Duals.Models.Content.Contact.ContactUsDetails.ContactInformationTop_FID);
                }
                return _contactInformationTop;
            }
            set {
                _contactInformationTop = null;
                this.SetString(Duals.Models.Content.Contact.ContactUsDetails.ContactInformationTop_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.ContactUsDetails.ContactInformationBottom_FID)]
        public virtual string ContactInformationBottom {
            get {
                if (_contactInformationBottom == null) {
_contactInformationBottom = this.GetString(Duals.Models.Content.Contact.ContactUsDetails.ContactInformationBottom_FID);
                }
                return _contactInformationBottom;
            }
            set {
                _contactInformationBottom = null;
                this.SetString(Duals.Models.Content.Contact.ContactUsDetails.ContactInformationBottom_FID, value);
            }
        }
        #endregion
    }
}
