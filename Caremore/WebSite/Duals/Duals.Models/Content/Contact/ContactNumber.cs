using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Content.Contact {
    
    
    public partial interface IContactNumber {
        
        string Title {
            get;
            set;
        }
        
        string Content {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Content.Contact.ContactNumber.TemplateID)]
    public partial class ContactNumber : StandardTemplate, Duals.Models.Content.Contact.IContactNumber {
        
        #region Members
        public const string TemplateID = "{FA6C1206-2F80-445B-A49D-632B881C31BC}";
        
        public const string Content_FID = "{A3E70D27-9742-44CC-A348-FF9EE4C50614}";
        
        public const string Title_FID = "{FEB9A747-660F-4105-9610-255A351E2CCD}";
        
        private string _title;
        
        private string _content;
        #endregion
        
        #region Constructors
        public ContactNumber(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Content.Contact.ContactNumber.Title_FID)]
        public virtual string Title {
            get {
                if (_title == null) {
_title = this.GetString(Duals.Models.Content.Contact.ContactNumber.Title_FID);
                }
                return _title;
            }
            set {
                _title = null;
                this.SetString(Duals.Models.Content.Contact.ContactNumber.Title_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Contact.ContactNumber.Content_FID)]
        public virtual string Content {
            get {
                if (_content == null) {
_content = this.GetString(Duals.Models.Content.Contact.ContactNumber.Content_FID);
                }
                return _content;
            }
            set {
                _content = null;
                this.SetString(Duals.Models.Content.Contact.ContactNumber.Content_FID, value);
            }
        }
        #endregion
    }
}
