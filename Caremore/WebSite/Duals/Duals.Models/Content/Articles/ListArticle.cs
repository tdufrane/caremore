using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Content.Articles {
    
    
    public partial interface IListArticle {
        
        InternalLinkField ListingSource {
            get;
            set;
        }
        
        string HeaderTitle {
            get;
            set;
        }
        
        string Header1 {
            get;
            set;
        }
        
        string Header2 {
            get;
            set;
        }
        
        string Header3 {
            get;
            set;
        }
        
        string Header4 {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Content.Articles.ListArticle.TemplateID)]
    public partial class ListArticle : StandardTemplate, Duals.Models.Content.Articles.IListArticle, Duals.Models.Base.IPage, Duals.Models.Content.Articles.IBaseArticle {
        
        #region Members
        public const string TemplateID = "{0DF813D0-A03E-4718-B10C-7597434BECEB}";
        
        public const string HeaderTitle_FID = "{8015BB2F-FDAD-4797-8E03-B570F5AB4840}";
        
        public const string Header1_FID = "{FC789B8B-FE3D-491C-92F9-8DC2A0363FA0}";
        
        public const string Header2_FID = "{1EDE600C-E000-469A-B5CA-2DD355AD393E}";
        
        public const string Header3_FID = "{6F23A201-CCA5-4593-AF8E-93AD2D159CD9}";
        
        public const string Header4_FID = "{33990E5D-9D0F-4330-8D55-7189C037183A}";
        
        public const string ListingSource_FID = "{10BF5220-0C57-40EC-A017-97B9901D5DA0}";
        
        private bool? _excludeFromNavigation;
        
        private string _pageTitle;
        
        private bool? _showInSiteMap;
        
        private string _navigationTitle;
        
        private bool? _dontCollaspeChildren;
        
        private bool? _displayLeftMenu;
        
        private string _headerTitle;
        
        private string _header1;
        
        private string _header2;
        
        private string _header3;
        
        private string _header4;
        #endregion
        
        #region Constructors
        public ListArticle(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Content.Articles.ListArticle.ListingSource_FID)]
        public virtual InternalLinkField ListingSource {
            get {
                return this.GetField<InternalLinkField>(Duals.Models.Content.Articles.ListArticle.ListingSource_FID);
            }
            set {
                this.SetField<InternalLinkField>(Duals.Models.Content.Articles.ListArticle.ListingSource_FID, value);
            }
        }
        
        [Field(Duals.Models.Base.Page.ExcludeFromNavigation_FID)]
        public virtual bool? ExcludeFromNavigation {
            get {
                if (_excludeFromNavigation == null) {
_excludeFromNavigation = this.GetBool(Duals.Models.Base.Page.ExcludeFromNavigation_FID);
                }
                return _excludeFromNavigation;
            }
            set {
                _excludeFromNavigation = null;
                this.SetBool(Duals.Models.Base.Page.ExcludeFromNavigation_FID, value);
            }
        }
        
        [Field(Duals.Models.Base.Page.PageTitle_FID)]
        public virtual string PageTitle {
            get {
                if (_pageTitle == null) {
_pageTitle = this.GetString(Duals.Models.Base.Page.PageTitle_FID);
                }
                return _pageTitle;
            }
            set {
                _pageTitle = null;
                this.SetString(Duals.Models.Base.Page.PageTitle_FID, value);
            }
        }
        
        [Field(Duals.Models.Base.Page.ShowInSiteMap_FID)]
        public virtual bool? ShowInSiteMap {
            get {
                if (_showInSiteMap == null) {
_showInSiteMap = this.GetBool(Duals.Models.Base.Page.ShowInSiteMap_FID);
                }
                return _showInSiteMap;
            }
            set {
                _showInSiteMap = null;
                this.SetBool(Duals.Models.Base.Page.ShowInSiteMap_FID, value);
            }
        }
        
        [Field(Duals.Models.Base.Page.NavigationTitle_FID)]
        public virtual string NavigationTitle {
            get {
                if (_navigationTitle == null) {
_navigationTitle = this.GetString(Duals.Models.Base.Page.NavigationTitle_FID);
                }
                return _navigationTitle;
            }
            set {
                _navigationTitle = null;
                this.SetString(Duals.Models.Base.Page.NavigationTitle_FID, value);
            }
        }
        
        [Field(Duals.Models.Base.Page.DontCollaspeChildren_FID)]
        public virtual bool? DontCollaspeChildren {
            get {
                if (_dontCollaspeChildren == null) {
_dontCollaspeChildren = this.GetBool(Duals.Models.Base.Page.DontCollaspeChildren_FID);
                }
                return _dontCollaspeChildren;
            }
            set {
                _dontCollaspeChildren = null;
                this.SetBool(Duals.Models.Base.Page.DontCollaspeChildren_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Articles.BaseArticle.DisplayLeftMenu_FID)]
        public virtual bool? DisplayLeftMenu {
            get {
                if (_displayLeftMenu == null) {
_displayLeftMenu = this.GetBool(Duals.Models.Content.Articles.BaseArticle.DisplayLeftMenu_FID);
                }
                return _displayLeftMenu;
            }
            set {
                _displayLeftMenu = null;
                this.SetBool(Duals.Models.Content.Articles.BaseArticle.DisplayLeftMenu_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Articles.BaseArticle.Url_FID)]
        public virtual LinkField Url {
            get {
                return this.GetField<LinkField>(Duals.Models.Content.Articles.BaseArticle.Url_FID);
            }
            set {
                this.SetField<LinkField>(Duals.Models.Content.Articles.BaseArticle.Url_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Articles.ListArticle.HeaderTitle_FID)]
        public virtual string HeaderTitle {
            get {
                if (_headerTitle == null) {
_headerTitle = this.GetString(Duals.Models.Content.Articles.ListArticle.HeaderTitle_FID);
                }
                return _headerTitle;
            }
            set {
                _headerTitle = null;
                this.SetString(Duals.Models.Content.Articles.ListArticle.HeaderTitle_FID, value);
            }
        }

        [Field(Duals.Models.Content.Articles.ListArticle.Header1_FID)]
        public virtual string Header1 {
            get {
                if (_header1 == null) {
_header1 = this.GetString(Duals.Models.Content.Articles.ListArticle.Header1_FID);
                }
                return _header1;
            }
            set {
                _header1 = null;
                this.SetString(Duals.Models.Content.Articles.ListArticle.Header1_FID, value);
            }
        }

        [Field(Duals.Models.Content.Articles.ListArticle.Header2_FID)]
        public virtual string Header2 {
            get {
                if (_header2 == null) {
_header2 = this.GetString(Duals.Models.Content.Articles.ListArticle.Header2_FID);
                }
                return _header2;
            }
            set {
                _header2 = null;
                this.SetString(Duals.Models.Content.Articles.ListArticle.Header2_FID, value);
            }
        }

        [Field(Duals.Models.Content.Articles.ListArticle.Header3_FID)]
        public virtual string Header3 {
            get {
                if (_header3 == null) {
_header3 = this.GetString(Duals.Models.Content.Articles.ListArticle.Header3_FID);
                }
                return _header3;
            }
            set {
                _header3 = null;
                this.SetString(Duals.Models.Content.Articles.ListArticle.Header3_FID, value);
            }
        }

        [Field(Duals.Models.Content.Articles.ListArticle.Header4_FID)]
        public virtual string Header4 {
            get {
                if (_header4 == null) {
_header4 = this.GetString(Duals.Models.Content.Articles.ListArticle.Header4_FID);
                }
                return _header4;
            }
            set {
                _header4 = null;
                this.SetString(Duals.Models.Content.Articles.ListArticle.Header4_FID, value);
            }
        }
        #endregion
    }
}
