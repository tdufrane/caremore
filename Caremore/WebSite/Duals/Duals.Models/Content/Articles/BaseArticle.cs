using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Content.Articles {
    
    
    public partial interface IBaseArticle {
        
        bool? DisplayLeftMenu {
            get;
            set;
        }
        
        LinkField Url {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Content.Articles.BaseArticle.TemplateID)]
    public partial class BaseArticle : StandardTemplate, Duals.Models.Content.Articles.IBaseArticle {
        
        #region Members
        public const string TemplateID = "{C97AB26C-8036-4487-B2B6-926AA15F761C}";
        
        public const string Url_FID = "{C43B6C87-7E90-42F5-986B-1CDC4D948968}";
        
        public const string DisplayLeftMenu_FID = "{630B2C3B-CE74-4FAC-AC2E-B77BB788CC77}";
        
        private bool? _displayLeftMenu;
        #endregion
        
        #region Constructors
        public BaseArticle(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Content.Articles.BaseArticle.DisplayLeftMenu_FID)]
        public virtual bool? DisplayLeftMenu {
            get {
                if (_displayLeftMenu == null) {
_displayLeftMenu = this.GetBool(Duals.Models.Content.Articles.BaseArticle.DisplayLeftMenu_FID);
                }
                return _displayLeftMenu;
            }
            set {
                _displayLeftMenu = null;
                this.SetBool(Duals.Models.Content.Articles.BaseArticle.DisplayLeftMenu_FID, value);
            }
        }
        
        [Field(Duals.Models.Content.Articles.BaseArticle.Url_FID)]
        public virtual LinkField Url {
            get {
                return this.GetField<LinkField>(Duals.Models.Content.Articles.BaseArticle.Url_FID);
            }
            set {
                this.SetField<LinkField>(Duals.Models.Content.Articles.BaseArticle.Url_FID, value);
            }
        }
        #endregion
    }
}
