﻿using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Pages {
    
    
    public partial interface IMaterialRequestForm {
    }
    
    [Template(Duals.Models.Pages.MaterialRequestForm.TemplateID)]
    public partial class MaterialRequestForm : Duals.Models.Pages.ContentPage, Duals.Models.Settings.IEmail, Duals.Models.Pages.IMaterialRequestForm {
        
        #region Members
        public new const string TemplateID = "{7C4D89FA-9675-4092-8290-522AFBE8FDDF}";
        
        // IContentPage
        private string _pageContent;
        
        private string _mastheadTitle;
        
        private string _mastheadDescription;
        
        // IEmail
        private string _emailTo;
        
        private string _emailFrom;
        
        private string _emailSubject;
        
        private string _emailErrorText;
         
        private string _confirmationMessage;
        #endregion
        
        #region Constructors
        public MaterialRequestForm(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region IContent Properties
        [Field(Duals.Models.Pages.ContentPage.PageContent_FID)]
        public virtual string PageContent {
            get {
                if (_pageContent == null) {
_pageContent = this.GetString(Duals.Models.Pages.ContentPage.PageContent_FID);
                }
                return _pageContent;
            }
            set {
                _pageContent = null;
                this.SetString(Duals.Models.Pages.ContentPage.PageContent_FID, value);
            }
        }
        
        [Field(Duals.Models.Pages.ContentPage.MastheadTitle_FID)]
        public virtual string MastheadTitle {
            get {
                if (_mastheadTitle == null) {
_mastheadTitle = this.GetString(Duals.Models.Pages.ContentPage.MastheadTitle_FID);
                }
                return _mastheadTitle;
            }
            set {
                _mastheadTitle = null;
                this.SetString(Duals.Models.Pages.ContentPage.MastheadTitle_FID, value);
            }
        }
        
        [Field(Duals.Models.Pages.ContentPage.MastheadDescription_FID)]
        public virtual string MastheadDescription {
            get {
                if (_mastheadDescription == null) {
_mastheadDescription = this.GetString(Duals.Models.Pages.ContentPage.MastheadDescription_FID);
                }
                return _mastheadDescription;
            }
            set {
                _mastheadDescription = null;
                this.SetString(Duals.Models.Pages.ContentPage.MastheadDescription_FID, value);
            }
        }
        
        [Field(Duals.Models.Pages.ContentPage.MastheadImage_FID)]
        public virtual ImageField MastheadImage {
            get {
                return this.GetField<ImageField>(Duals.Models.Pages.ContentPage.MastheadImage_FID);
            }
            set {
                this.SetField<ImageField>(Duals.Models.Pages.ContentPage.MastheadImage_FID, value);
            }
        }
        #endregion

        #region IEmail Properties
        [Field(Duals.Models.Settings.Email.EmailTo_FID)]
        public virtual string EmailTo {
            get {
                if (_emailTo == null) {
_emailTo = this.GetString(Duals.Models.Settings.Email.EmailTo_FID);
                }
                return _emailTo;
            }
            set {
                _emailTo = null;
                this.SetString(Duals.Models.Settings.Email.EmailTo_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.Email.EmailFrom_FID)]
        public virtual string EmailFrom {
            get {
                if (_emailFrom == null) {
_emailFrom = this.GetString(Duals.Models.Settings.Email.EmailFrom_FID);
                }
                return _emailFrom;
            }
            set {
                _emailFrom = null;
                this.SetString(Duals.Models.Settings.Email.EmailFrom_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.Email.EmailSubject_FID)]
        public virtual string EmailSubject {
            get {
                if (_emailSubject == null) {
_emailSubject = this.GetString(Duals.Models.Settings.Email.EmailSubject_FID);
                }
                return _emailSubject;
            }
            set {
                _emailSubject = null;
                this.SetString(Duals.Models.Settings.Email.EmailSubject_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.Email.EmailErrorText_FID)]
        public virtual string EmailErrorText {
            get {
                if (_emailErrorText == null) {
_emailErrorText = this.GetString(Duals.Models.Settings.Email.EmailErrorText_FID);
                }
                return _emailErrorText;
            }
            set {
                _emailErrorText = null;
                this.SetString(Duals.Models.Settings.Email.EmailErrorText_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.Email.ConfirmationMessage_FID)]
        public virtual string ConfirmationMessage {
            get {
                if (_confirmationMessage == null) {
_confirmationMessage = this.GetString(Duals.Models.Settings.Email.ConfirmationMessage_FID);
                }
                return _confirmationMessage;
            }
            set {
                _confirmationMessage = null;
                this.SetString(Duals.Models.Settings.Email.ConfirmationMessage_FID, value);
            }
        }
        #endregion
    }
}
