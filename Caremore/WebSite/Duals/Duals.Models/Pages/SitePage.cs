using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Pages {
    
    
    public partial interface ISitePage {
        
        bool? AddToGlobalNav {
            get;
            set;
        }
        
        bool? AddToMiniFooterNav {
            get;
            set;
        }
        
        bool? AddToFooterMiddleColumnNav {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Pages.SitePage.TemplateID)]
    public partial class SitePage : Duals.Models.Base.Page, Duals.Models.Pages.ISitePage {
        
        #region Members
        public new const string TemplateID = "{C0DC454C-0FB0-4813-A014-666620383BE9}";
        
        public const string AddToFooterMiddleColumnNav_FID = "{0F182F9E-E252-4AE2-8F47-1658FE65A77E}";
        
        public const string AddToMiniFooterNav_FID = "{38BCE2B7-00C7-4243-964E-8888B8E54ABB}";
        
        public const string AddToGlobalNav_FID = "{72BA972D-79FE-4223-A846-DBF4F71D8CDE}";
        
        private bool? _addToGlobalNav;
        
        private bool? _addToMiniFooterNav;
        
        private bool? _addToFooterMiddleColumnNav;
        #endregion
        
        #region Constructors
        public SitePage(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Pages.SitePage.AddToGlobalNav_FID)]
        public virtual bool? AddToGlobalNav {
            get {
                if (_addToGlobalNav == null) {
_addToGlobalNav = this.GetBool(Duals.Models.Pages.SitePage.AddToGlobalNav_FID);
                }
                return _addToGlobalNav;
            }
            set {
                _addToGlobalNav = null;
                this.SetBool(Duals.Models.Pages.SitePage.AddToGlobalNav_FID, value);
            }
        }
        
        [Field(Duals.Models.Pages.SitePage.AddToMiniFooterNav_FID)]
        public virtual bool? AddToMiniFooterNav {
            get {
                if (_addToMiniFooterNav == null) {
_addToMiniFooterNav = this.GetBool(Duals.Models.Pages.SitePage.AddToMiniFooterNav_FID);
                }
                return _addToMiniFooterNav;
            }
            set {
                _addToMiniFooterNav = null;
                this.SetBool(Duals.Models.Pages.SitePage.AddToMiniFooterNav_FID, value);
            }
        }
        
        [Field(Duals.Models.Pages.SitePage.AddToFooterMiddleColumnNav_FID)]
        public virtual bool? AddToFooterMiddleColumnNav {
            get {
                if (_addToFooterMiddleColumnNav == null) {
_addToFooterMiddleColumnNav = this.GetBool(Duals.Models.Pages.SitePage.AddToFooterMiddleColumnNav_FID);
                }
                return _addToFooterMiddleColumnNav;
            }
            set {
                _addToFooterMiddleColumnNav = null;
                this.SetBool(Duals.Models.Pages.SitePage.AddToFooterMiddleColumnNav_FID, value);
            }
        }
        #endregion
    }
}
