﻿using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;

namespace Duals.Models.Pages
{
	public partial interface IProviderSearchPage
	{
		bool? HideSpecialty { get; set; }
	}

	[Template(Duals.Models.Pages.ProviderSearchPage.TemplateID)]
	public partial class ProviderSearchPage : Duals.Models.Pages.ContentPage, Duals.Models.Pages.IProviderSearchPage
	{
		#region Members

		public new const string TemplateID = "{2B259EC7-2DE4-432F-92F3-BE79C759D23A}";

		public const string HideSpecialty_FID = "{922A53A4-8BAF-4629-8044-BC40C169CCC7}";

		private bool? _hideSpecialty;

		#endregion

		#region Constructors

		public ProviderSearchPage(Item innerItem) : base(innerItem)
		{
		}

		#endregion

		#region Properties

		[Field(Duals.Models.Pages.ProviderSearchPage.HideSpecialty_FID)]
		public virtual bool? HideSpecialty
		{
			get
			{
				if (_hideSpecialty == null)
				{
					_hideSpecialty = this.GetBool(Duals.Models.Pages.ProviderSearchPage.HideSpecialty_FID);
				}
				return _hideSpecialty;
			}
			set
			{
				_hideSpecialty = null; this.SetBool(Duals.Models.Pages.ProviderSearchPage.HideSpecialty_FID, value);
			}
		}

		#endregion
	}
}
