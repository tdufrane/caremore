using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Pages {
    
    
    public partial interface ISiteLandingPage {
    }
    
    [Template(Duals.Models.Pages.SiteLandingPage.TemplateID)]
    public partial class SiteLandingPage : Duals.Models.Pages.ContentPage, Duals.Models.Pages.ISiteLandingPage, Duals.Models.Pages.ILandingPage {
        
        #region Members
        public new const string TemplateID = "{83ADBEE9-CC6F-4F9D-8F8D-B2C2A79C8066}";
        
        private string _primaryNavCSS;
        
        private string _pageContent;
        
        private string _mastheadTitle;
        
        private string _mastheadDescription;
        #endregion
        
        #region Constructors
        public SiteLandingPage(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Pages.LandingPage.PrimaryNavCSS_FID)]
        public virtual string PrimaryNavCSS {
            get {
                if (_primaryNavCSS == null) {
_primaryNavCSS = this.GetString(Duals.Models.Pages.LandingPage.PrimaryNavCSS_FID);
                }
                return _primaryNavCSS;
            }
            set {
                _primaryNavCSS = null;
                this.SetString(Duals.Models.Pages.LandingPage.PrimaryNavCSS_FID, value);
            }
        }
        
        [Field(Duals.Models.Pages.ContentPage.PageContent_FID)]
        public new virtual string PageContent {
            get {
                if (_pageContent == null) {
_pageContent = this.GetString(Duals.Models.Pages.ContentPage.PageContent_FID);
                }
                return _pageContent;
            }
            set {
                _pageContent = null;
                this.SetString(Duals.Models.Pages.ContentPage.PageContent_FID, value);
            }
        }
        
        [Field(Duals.Models.Pages.ContentPage.MastheadTitle_FID)]
        public new virtual string MastheadTitle {
            get {
                if (_mastheadTitle == null) {
_mastheadTitle = this.GetString(Duals.Models.Pages.ContentPage.MastheadTitle_FID);
                }
                return _mastheadTitle;
            }
            set {
                _mastheadTitle = null;
                this.SetString(Duals.Models.Pages.ContentPage.MastheadTitle_FID, value);
            }
        }
        
        [Field(Duals.Models.Pages.ContentPage.MastheadDescription_FID)]
        public new virtual string MastheadDescription {
            get {
                if (_mastheadDescription == null) {
_mastheadDescription = this.GetString(Duals.Models.Pages.ContentPage.MastheadDescription_FID);
                }
                return _mastheadDescription;
            }
            set {
                _mastheadDescription = null;
                this.SetString(Duals.Models.Pages.ContentPage.MastheadDescription_FID, value);
            }
        }
        
        [Field(Duals.Models.Pages.ContentPage.MastheadImage_FID)]
        public new virtual ImageField MastheadImage {
            get {
                return this.GetField<ImageField>(Duals.Models.Pages.ContentPage.MastheadImage_FID);
            }
            set {
                this.SetField<ImageField>(Duals.Models.Pages.ContentPage.MastheadImage_FID, value);
            }
        }
        #endregion
    }
}
