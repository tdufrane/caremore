using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Pages {
    
    
    public partial interface IHomepage {
    }
    
    [Template(Duals.Models.Pages.Homepage.TemplateID)]
    public partial class Homepage : Duals.Models.Base.Page, Duals.Models.Pages.IHomepage {
        
        #region Members
        public new const string TemplateID = "{04E5B155-9BD9-4287-A9F1-D9F8F7F2B1FC}";
        #endregion
        
        #region Constructors
        public Homepage(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
    }
}
