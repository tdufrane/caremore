﻿using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Pages {
    
    
    public partial interface IConsumerEventPage {
        
        System.Collections.Generic.IEnumerable<Sitecore.Data.Items.Item> EventType {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Pages.ConsumerEventPage.TemplateID)]
    public partial class ConsumerEventPage : CareMore.Web.DotCom.Models.Pages.EventPage, Duals.Models.Pages.IConsumerEventPage {
        
        #region Members
        public new const string TemplateID = "{1FC33288-1D4A-4A73-A37A-ACDDB792AE4A}";
        
        public const string EventType_FID = "{0E9DF0C8-A033-48A2-80DC-A86B8D2E4E62}";
        
        private IEnumerable<Item> _eventType;
        #endregion
        
        #region Constructors
        public ConsumerEventPage(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Pages.ConsumerEventPage.EventType_FID)]
        public virtual System.Collections.Generic.IEnumerable<Sitecore.Data.Items.Item> EventType {
            get {
                if (_eventType == null) {
_eventType = this.GetItems(Duals.Models.Pages.ConsumerEventPage.EventType_FID);
                }
                return _eventType;
            }
            set {
                _eventType = null;
                this.SetItems(Duals.Models.Pages.ConsumerEventPage.EventType_FID, value);
            }
        }
        #endregion
    }
}