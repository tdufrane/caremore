using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Base {
    
    
    public partial interface IPage {
        
        bool? ExcludeFromNavigation {
            get;
            set;
        }
        
        string PageTitle {
            get;
            set;
        }
        
        bool? ShowInSiteMap {
            get;
            set;
        }
        
        string NavigationTitle {
            get;
            set;
        }
        
        bool? DontCollaspeChildren {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Base.Page.TemplateID)]
    public partial class Page : StandardTemplate, Duals.Models.Base.IPage {
        
        #region Members
        public const string TemplateID = "{E7A8A4D8-8E4C-41CE-8125-2B9A53C2778E}";
        
        public const string NavigationTitle_FID = "{C67C90E5-4C01-4BF8-B71B-20A61188F3E0}";
        
        public const string ShowInSiteMap_FID = "{6F056178-53A7-44D3-9B47-C4F4DC57B9F6}";
        
        public const string PageTitle_FID = "{52A288DF-20BF-46E9-8897-BE37F77CEFF7}";
        
        public const string ExcludeFromNavigation_FID = "{CA350C08-A342-462D-B6D4-ACF0C6AA8E82}";
        
        public const string DontCollaspeChildren_FID = "{4E70CBCC-E196-4EA5-85AB-B5DE71919BB7}";
        
        private bool? _excludeFromNavigation;
        
        private string _pageTitle;
        
        private bool? _showInSiteMap;
        
        private string _navigationTitle;
        
        private bool? _dontCollaspeChildren;
        #endregion
        
        #region Constructors
        public Page(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Base.Page.ExcludeFromNavigation_FID)]
        public virtual bool? ExcludeFromNavigation {
            get {
                if (_excludeFromNavigation == null) {
_excludeFromNavigation = this.GetBool(Duals.Models.Base.Page.ExcludeFromNavigation_FID);
                }
                return _excludeFromNavigation;
            }
            set {
                _excludeFromNavigation = null;
                this.SetBool(Duals.Models.Base.Page.ExcludeFromNavigation_FID, value);
            }
        }
        
        [Field(Duals.Models.Base.Page.PageTitle_FID)]
        public virtual string PageTitle {
            get {
                if (_pageTitle == null) {
_pageTitle = this.GetString(Duals.Models.Base.Page.PageTitle_FID);
                }
                return _pageTitle;
            }
            set {
                _pageTitle = null;
                this.SetString(Duals.Models.Base.Page.PageTitle_FID, value);
            }
        }
        
        [Field(Duals.Models.Base.Page.ShowInSiteMap_FID)]
        public virtual bool? ShowInSiteMap {
            get {
                if (_showInSiteMap == null) {
_showInSiteMap = this.GetBool(Duals.Models.Base.Page.ShowInSiteMap_FID);
                }
                return _showInSiteMap;
            }
            set {
                _showInSiteMap = null;
                this.SetBool(Duals.Models.Base.Page.ShowInSiteMap_FID, value);
            }
        }
        
        [Field(Duals.Models.Base.Page.NavigationTitle_FID)]
        public virtual string NavigationTitle {
            get {
                if (_navigationTitle == null) {
_navigationTitle = this.GetString(Duals.Models.Base.Page.NavigationTitle_FID);
                }
                return _navigationTitle;
            }
            set {
                _navigationTitle = null;
                this.SetString(Duals.Models.Base.Page.NavigationTitle_FID, value);
            }
        }
        
        [Field(Duals.Models.Base.Page.DontCollaspeChildren_FID)]
        public virtual bool? DontCollaspeChildren {
            get {
                if (_dontCollaspeChildren == null) {
_dontCollaspeChildren = this.GetBool(Duals.Models.Base.Page.DontCollaspeChildren_FID);
                }
                return _dontCollaspeChildren;
            }
            set {
                _dontCollaspeChildren = null;
                this.SetBool(Duals.Models.Base.Page.DontCollaspeChildren_FID, value);
            }
        }
        #endregion
    }
}
