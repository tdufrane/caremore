using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Settings {
    
    
    public partial interface ISiteSettings {
        
        ImageField LogoImage {
            get;
            set;
        }
        
        string RightColumnContent {
            get;
            set;
        }
        
        string LastUpdatedOn {
            get;
            set;
        }
        
        string SearchBarWatermark {
            get;
            set;
        }
        
        string TextSize {
            get;
            set;
        }
        
        ImageField TextSizeUpImage {
            get;
            set;
        }
        
        ImageField TextSizeDownImage {
            get;
            set;
        }
        
        IEnumerable<Item> AvailableLanguages {
            get;
            set;
        }
        
        string FooterLanguageText {
            get;
            set;
        }
        
        LinkField LinkToFooterLanguagePage {
            get;
            set;
        }
        
        string ThirdPartyLinksMessage {
            get;
            set;
        }
        
        Item PortalCode {
            get;
            set;
        }
        
        string MaxZipDistance {
            get;
            set;
        }
        
        string NonDiscriminationNotice {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Settings.SiteSettings.TemplateID)]
    public partial class SiteSettings : StandardTemplate, Duals.Models.Settings.ISiteSettings {
        
        #region Members
        public const string TemplateID = "{DE972421-3D8A-4644-97BA-A55535B9B620}";
        
        public const string LogoImage_FID = "{D9077FA2-EC96-4B96-B7C7-A63258986AE1}";
        
        public const string TextSize_FID = "{43708766-16D7-49F0-81A8-19D7CDBFD77B}";
        
        public const string RightColumnContent_FID = "{69A3BE57-AFA8-4FB7-A55D-6535B04955D2}";
        
        public const string PortalCode_FID = "{30D6952D-EEE0-4621-AE21-CBEAD851AA5F}";
        
        public const string MaxZipDistance_FID = "{A02C58DF-6E78-4331-AB4E-1CF374921626}";
        
        public const string LastUpdatedOn_FID = "{AA9354E0-9F70-410A-B758-1C8BECD2FF98}";
        
        public const string AvailableLanguages_FID = "{25463007-8EB6-4EAE-B947-B3947C47DD10}";
        
        public const string Code_FID = "{2824B870-81DB-4D83-9A19-E4ABE89A3AE5}";
        
        public const string TextSizeUpImage_FID = "{9950B05D-7F4F-4E34-A728-C661948B582F}";
        
        public const string FooterLanguageText_FID = "{4BFB062F-E05E-4403-B29A-A589588E019D}";
        
        public const string SearchBarWatermark_FID = "{F1FC0747-0248-4152-AB7F-AAD52966E958}";
        
        public const string LinkToFooterLanguagePage_FID = "{25B871F6-C230-4EF0-8F92-27E6AD8128DD}";
        
        public const string TextSizeDownImage_FID = "{2EA8EA20-EE08-449C-8905-5EC8366E2B7E}";
        
        public const string ThirdPartyLinksMessage_FID = "{A81E1142-F205-4656-9F67-4B635B228937}";
        
        public const string NonDiscriminationNotice_FID = "{E934B563-9738-4B1A-A809-91D1DCF2AF71}";
        
        private string _rightColumnContent;
        
        private string _lastUpdatedOn;
        
        private string _searchBarWatermark;
        
        private string _textSize;
        
        private IEnumerable<Item> _availableLanguages;
        
        private string _footerLanguageText;
        
        private string _thirdPartyLinksMessage;
        
        private string _nonDiscriminationNotice;
        
        private string _maxZipDistance;
        #endregion
        
        #region Constructors
        public SiteSettings(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Settings.SiteSettings.LogoImage_FID)]
        public virtual ImageField LogoImage {
            get {
                return this.GetField<ImageField>(Duals.Models.Settings.SiteSettings.LogoImage_FID);
            }
            set {
                this.SetField<ImageField>(Duals.Models.Settings.SiteSettings.LogoImage_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.SiteSettings.RightColumnContent_FID)]
        public virtual string RightColumnContent {
            get {
                if (_rightColumnContent == null) {
_rightColumnContent = this.GetString(Duals.Models.Settings.SiteSettings.RightColumnContent_FID);
                }
                return _rightColumnContent;
            }
            set {
                _rightColumnContent = null;
                this.SetString(Duals.Models.Settings.SiteSettings.RightColumnContent_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.SiteSettings.LastUpdatedOn_FID)]
        public virtual string LastUpdatedOn {
            get {
                if (_lastUpdatedOn == null) {
_lastUpdatedOn = this.GetString(Duals.Models.Settings.SiteSettings.LastUpdatedOn_FID);
                }
                return _lastUpdatedOn;
            }
            set {
                _lastUpdatedOn = null;
                this.SetString(Duals.Models.Settings.SiteSettings.LastUpdatedOn_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.SiteSettings.SearchBarWatermark_FID)]
        public virtual string SearchBarWatermark {
            get {
                if (_searchBarWatermark == null) {
_searchBarWatermark = this.GetString(Duals.Models.Settings.SiteSettings.SearchBarWatermark_FID);
                }
                return _searchBarWatermark;
            }
            set {
                _searchBarWatermark = null;
                this.SetString(Duals.Models.Settings.SiteSettings.SearchBarWatermark_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.SiteSettings.TextSize_FID)]
        public virtual string TextSize {
            get {
                if (_textSize == null) {
_textSize = this.GetString(Duals.Models.Settings.SiteSettings.TextSize_FID);
                }
                return _textSize;
            }
            set {
                _textSize = null;
                this.SetString(Duals.Models.Settings.SiteSettings.TextSize_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.SiteSettings.TextSizeUpImage_FID)]
        public virtual ImageField TextSizeUpImage {
            get {
                return this.GetField<ImageField>(Duals.Models.Settings.SiteSettings.TextSizeUpImage_FID);
            }
            set {
                this.SetField<ImageField>(Duals.Models.Settings.SiteSettings.TextSizeUpImage_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.SiteSettings.TextSizeDownImage_FID)]
        public virtual ImageField TextSizeDownImage {
            get {
                return this.GetField<ImageField>(Duals.Models.Settings.SiteSettings.TextSizeDownImage_FID);
            }
            set {
                this.SetField<ImageField>(Duals.Models.Settings.SiteSettings.TextSizeDownImage_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.SiteSettings.AvailableLanguages_FID)]
        public virtual IEnumerable<Item> AvailableLanguages {
            get {
                if (_availableLanguages == null) {
_availableLanguages = this.GetItems(Duals.Models.Settings.SiteSettings.AvailableLanguages_FID);
                }
                return _availableLanguages;
            }
            set {
                _availableLanguages = null;
                this.SetItems(Duals.Models.Settings.SiteSettings.AvailableLanguages_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.SiteSettings.FooterLanguageText_FID)]
        public virtual string FooterLanguageText {
            get {
                if (_footerLanguageText == null) {
_footerLanguageText = this.GetString(Duals.Models.Settings.SiteSettings.FooterLanguageText_FID);
                }
                return _footerLanguageText;
            }
            set {
                _footerLanguageText = null;
                this.SetString(Duals.Models.Settings.SiteSettings.FooterLanguageText_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.SiteSettings.LinkToFooterLanguagePage_FID)]
        public virtual LinkField LinkToFooterLanguagePage {
            get {
                return this.GetField<LinkField>(Duals.Models.Settings.SiteSettings.LinkToFooterLanguagePage_FID);
            }
            set {
                this.SetField<LinkField>(Duals.Models.Settings.SiteSettings.LinkToFooterLanguagePage_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.SiteSettings.ThirdPartyLinksMessage_FID)]
        public virtual string ThirdPartyLinksMessage {
            get {
                if (_thirdPartyLinksMessage == null) {
_thirdPartyLinksMessage = this.GetString(Duals.Models.Settings.SiteSettings.ThirdPartyLinksMessage_FID);
                }
                return _thirdPartyLinksMessage;
            }
            set {
                _thirdPartyLinksMessage = null;
                this.SetString(Duals.Models.Settings.SiteSettings.ThirdPartyLinksMessage_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.SiteSettings.PortalCode_FID)]
        public virtual Item PortalCode {
            get {
                return this.GetItem(Duals.Models.Settings.SiteSettings.PortalCode_FID);
            }
            set {
                this.SetItem(Duals.Models.Settings.SiteSettings.PortalCode_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.SiteSettings.MaxZipDistance_FID)]
        public virtual string MaxZipDistance {
            get {
                if (_maxZipDistance == null) {
_maxZipDistance = this.GetString(Duals.Models.Settings.SiteSettings.MaxZipDistance_FID);
                }
                return _maxZipDistance;
            }
            set {
                _maxZipDistance = null;
                this.SetString(Duals.Models.Settings.SiteSettings.MaxZipDistance_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.SiteSettings.NonDiscriminationNotice_FID)]
        public virtual string NonDiscriminationNotice {
            get {
                if (_nonDiscriminationNotice == null) {
_nonDiscriminationNotice = this.GetString(Duals.Models.Settings.SiteSettings.NonDiscriminationNotice_FID);
                }
                return _nonDiscriminationNotice;
            }
            set {
                _nonDiscriminationNotice = null;
                this.SetString(Duals.Models.Settings.SiteSettings.NonDiscriminationNotice_FID, value);
            }
        }
        #endregion
    }
}
