﻿using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Settings
{
    public partial interface IEmail {
        
        string EmailTo {
            get;
            set;
        }
        
        string EmailFrom {
            get;
            set;
        }
        
        string EmailSubject {
            get;
            set;
        }
        
        string EmailErrorText {
            get;
            set;
        }
        
        string ConfirmationMessage {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Settings.Email.TemplateID)]
    public partial class Email : StandardTemplate, Duals.Models.Settings.IEmail {
        
        #region Members
        public const string TemplateID = "{AA0FD856-2438-4003-B677-593401B72166}";
        
        public const string EmailTo_FID = "{F877B4A3-7AD0-4D55-A9C2-68FE8E99FADE}";
        
        public const string EmailFrom_FID = "{E6C7AC1C-68A0-40AA-9E9A-E2F73AF2D909}";
        
        public const string EmailSubject_FID = "{A9E77EB3-83E8-4F19-8141-C87CC34E0428}";
        
        public const string EmailErrorText_FID = "{612BAE53-78E2-4814-9077-7D62A819965E}";
        
        public const string ConfirmationMessage_FID = "{B20CD3CF-6561-42EA-8AEF-6A4C4DD5205E}";
        
        private string _emailTo;
        
        private string _emailFrom;
        
        private string _emailSubject;
        
        private string _emailErrorText;
         
        private string _confirmationMessage;
        #endregion
       
        #region Constructors
        public Email(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Settings.Email.EmailTo_FID)]
        public virtual string EmailTo {
            get {
                if (_emailTo == null) {
_emailTo = this.GetString(Duals.Models.Settings.Email.EmailTo_FID);
                }
                return _emailTo;
            }
            set {
                _emailTo = null;
                this.SetString(Duals.Models.Settings.Email.EmailTo_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.Email.EmailFrom_FID)]
        public virtual string EmailFrom {
            get {
                if (_emailFrom == null) {
_emailFrom = this.GetString(Duals.Models.Settings.Email.EmailFrom_FID);
                }
                return _emailFrom;
            }
            set {
                _emailFrom = null;
                this.SetString(Duals.Models.Settings.Email.EmailFrom_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.Email.EmailSubject_FID)]
        public virtual string EmailSubject {
            get {
                if (_emailSubject == null) {
_emailSubject = this.GetString(Duals.Models.Settings.Email.EmailSubject_FID);
                }
                return _emailSubject;
            }
            set {
                _emailSubject = null;
                this.SetString(Duals.Models.Settings.Email.EmailSubject_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.Email.EmailErrorText_FID)]
        public virtual string EmailErrorText {
            get {
                if (_emailErrorText == null) {
_emailErrorText = this.GetString(Duals.Models.Settings.Email.EmailErrorText_FID);
                }
                return _emailErrorText;
            }
            set {
                _emailErrorText = null;
                this.SetString(Duals.Models.Settings.Email.EmailErrorText_FID, value);
            }
        }
        
        [Field(Duals.Models.Settings.Email.ConfirmationMessage_FID)]
        public virtual string ConfirmationMessage {
            get {
                if (_confirmationMessage == null) {
_confirmationMessage = this.GetString(Duals.Models.Settings.Email.ConfirmationMessage_FID);
                }
                return _confirmationMessage;
            }
            set {
                _confirmationMessage = null;
                this.SetString(Duals.Models.Settings.Email.ConfirmationMessage_FID, value);
            }
        }
        #endregion
    }
}
