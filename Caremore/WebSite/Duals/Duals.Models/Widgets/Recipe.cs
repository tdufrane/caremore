﻿using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Widgets
{
//	class 
    
    
    public partial interface IRecipe {
        
        string EnglishTitle {
            get;
            set;
        }
        
        string EnglishDescription {
            get;
            set;
        }
        
        Sitecore.Data.Fields.LinkField EnglishPDF {
            get;
            set;
        }
        
        string SpanishTitle {
            get;
            set;
        }
        
        string SpanishDescription {
            get;
            set;
        }
        
        Sitecore.Data.Fields.LinkField SpanishPDF {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Widgets.Recipe.TemplateID)]
    public partial class Recipe : StandardTemplate, Duals.Models.Widgets.IRecipe {
        
        #region Members
        public const string TemplateID = "{F8A21D67-9EEE-48B5-96FF-1F0C8158C826}";
        
        public const string EnglishTitle_FID = "{C803756A-E088-4376-A4F9-245C072217F5}";
        
        public const string EnglishDescription_FID = "{55F3B67A-B7FF-4E92-87AF-BE4019DBA7F6}";
        
        public const string EnglishPDF_FID = "{8CBFAA9D-D056-4CFA-86B9-E41A739200D6}";
        
        public const string SpanishTitle_FID = "{4CDC8D69-5F07-43E2-A2EB-14FF39A8F27E}";
        
        public const string SpanishDescription_FID = "{1CB55C9A-D59A-4161-98BE-726973CA48FA}";
        
        public const string SpanishPDF_FID = "{4CDC8D69-5F07-43E2-A2EB-14FF39A8F27E}";
        
        private string _englishTitle;
        
        private string _englishDescription;
        
        private string _spanishTitle;
        
        private string _spanishDescription;
        #endregion
        
        #region Constructors
        public Recipe(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Widgets.Recipe.EnglishTitle_FID)]
        public virtual string EnglishTitle {
            get {
                if (_englishTitle == null) {
_englishTitle = this.GetString(Duals.Models.Widgets.Recipe.EnglishTitle_FID);
                }
                return _englishTitle;
            }
            set {
                _englishTitle = null;
                this.SetString(Duals.Models.Widgets.Recipe.EnglishTitle_FID, value);
            }
        }
        
        [Field(Duals.Models.Widgets.Recipe.EnglishDescription_FID)]
        public virtual string EnglishDescription {
            get {
                if (_englishDescription == null) {
_englishDescription = this.GetString(Duals.Models.Widgets.Recipe.EnglishDescription_FID);
                }
                return _englishDescription;
            }
            set {
                _englishDescription = null;
                this.SetString(Duals.Models.Widgets.Recipe.EnglishDescription_FID, value);
            }
        }
        
        [Field(Duals.Models.Widgets.Recipe.EnglishPDF_FID)]
        public virtual Sitecore.Data.Fields.LinkField EnglishPDF {
            get {
                return this.GetField<LinkField>(Duals.Models.Widgets.Recipe.EnglishPDF_FID);
            }
            set {
                this.SetField<LinkField>(Duals.Models.Widgets.Recipe.EnglishPDF_FID, value);
            }
        }
        
        [Field(Duals.Models.Widgets.Recipe.SpanishTitle_FID)]
        public virtual string SpanishTitle {
            get {
                if (_spanishTitle == null) {
_spanishTitle = this.GetString(Duals.Models.Widgets.Recipe.SpanishTitle_FID);
                }
                return _spanishTitle;
            }
            set {
                _spanishTitle = null;
                this.SetString(Duals.Models.Widgets.Recipe.SpanishTitle_FID, value);
            }
        }
        
        [Field(Duals.Models.Widgets.Recipe.SpanishDescription_FID)]
        public virtual string SpanishDescription {
            get {
                if (_spanishDescription == null) {
_spanishDescription = this.GetString(Duals.Models.Widgets.Recipe.SpanishDescription_FID);
                }
                return _spanishDescription;
            }
            set {
                _spanishDescription = null;
                this.SetString(Duals.Models.Widgets.Recipe.SpanishDescription_FID, value);
            }
        }
        
        [Field(Duals.Models.Widgets.Recipe.SpanishPDF_FID)]
        public virtual Sitecore.Data.Fields.LinkField SpanishPDF {
            get {
                return this.GetField<LinkField>(Duals.Models.Widgets.Recipe.SpanishPDF_FID);
            }
            set {
                this.SetField<LinkField>(Duals.Models.Widgets.Recipe.SpanishPDF_FID, value);
            }
        }
        
        #endregion
    }
}
