﻿using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Widgets {
    
    
    public partial interface IContentLinkBtn {
        
        ImageField Image {
            get;
            set;
        }
        
        LinkField Link {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Widgets.ContentLinkBtn.TemplateID)]
    public partial class ContentLinkBtn : StandardTemplate, Duals.Models.Widgets.IContentLinkBtn {
        
        #region Members
        public const string TemplateID = "{783D571F-6AB8-4C02-8604-17D687E5A786}";
        
        public const string Image_FID = "{8F91CB8E-2294-4C66-8B84-16FE3735A7A5}";
        
        public const string Link_FID = "{A49DE3EB-A349-4F09-813D-1C6DA1801EB0}";
        #endregion
        
        #region Constructors
        public ContentLinkBtn(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Widgets.ContentLinkBtn.Image_FID)]
        public virtual ImageField Image {
            get {
                return this.GetField<ImageField>(Duals.Models.Widgets.ContentLinkBtn.Image_FID);
            }
            set {
                this.SetField<ImageField>(Duals.Models.Widgets.ContentLinkBtn.Image_FID, value);
            }
        }
        
        [Field(Duals.Models.Widgets.ContentLinkBtn.Link_FID)]
        public virtual LinkField Link {
            get {
                return this.GetField<LinkField>(Duals.Models.Widgets.ContentLinkBtn.Link_FID);
            }
            set {
                this.SetField<LinkField>(Duals.Models.Widgets.ContentLinkBtn.Link_FID, value);
            }
        }
        #endregion
    }
}
