using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Widgets {
    
    
    public partial interface IVideo {
        
        Sitecore.Data.Fields.ImageField VideoThumbnail {
            get;
            set;
        }
        
        Sitecore.Data.Fields.LinkField VideoLink {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Widgets.Video.TemplateID)]
    public partial class Video : StandardTemplate, Duals.Models.Widgets.IVideo {
        
        #region Members
        public const string TemplateID = "{4CC97449-F140-4D05-81F9-FCA4245FAAF7}";
        
        public const string VideoLink_FID = "{E8C9EA26-9335-4DE1-AF76-8A700C959CDE}";
        
        public const string VideoThumbnail_FID = "{8BC07622-47FC-4DBF-BBB4-C787D9504916}";
        #endregion
        
        #region Constructors
        public Video(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Widgets.Video.VideoThumbnail_FID)]
        public virtual Sitecore.Data.Fields.ImageField VideoThumbnail {
            get {
                return this.GetField<ImageField>(Duals.Models.Widgets.Video.VideoThumbnail_FID);
            }
            set {
                this.SetField<ImageField>(Duals.Models.Widgets.Video.VideoThumbnail_FID, value);
            }
        }
        
        [Field(Duals.Models.Widgets.Video.VideoLink_FID)]
        public virtual Sitecore.Data.Fields.LinkField VideoLink {
            get {
                return this.GetField<LinkField>(Duals.Models.Widgets.Video.VideoLink_FID);
            }
            set {
                this.SetField<LinkField>(Duals.Models.Widgets.Video.VideoLink_FID, value);
            }
        }
        #endregion
    }
}
