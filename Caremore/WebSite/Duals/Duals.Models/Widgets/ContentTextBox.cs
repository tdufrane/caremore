using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace Duals.Models.Widgets {
    
    
    public partial interface IContentTextBox {
        
        string Title {
            get;
            set;
        }
        
        string Content {
            get;
            set;
        }
        
        LinkField Link {
            get;
            set;
        }
        
        string SpacerHeight {
            get;
            set;
        }
    }
    
    [Template(Duals.Models.Widgets.ContentTextBox.TemplateID)]
    public partial class ContentTextBox : StandardTemplate, Duals.Models.Widgets.IContentTextBox {
        
        #region Members
        public const string TemplateID = "{8FC7E092-284F-4C23-A025-6B1C46AB4664}";
        
        public const string Content_FID = "{23A22FD7-9CEA-4CAC-8F4D-102FFAC130FF}";
        
        public const string Title_FID = "{066D8B52-48C3-4E9F-8E3D-4A7500978711}";

        public const string Link_FID = "{480F6126-EAB4-4DB7-814F-569C5F48C54C}";

        public const string SpacerHeight_FID = "{FF11B3BF-32F6-4B1E-8CBF-399F2BDE4EF0}";
        
        private string _title;
        
        private string _content;
        
        private string _spacerHeight;
        #endregion
        
        #region Constructors
        public ContentTextBox(Item innerItem) : 
                base(innerItem) {
        }
        #endregion
        
        #region Properties
        [Field(Duals.Models.Widgets.ContentTextBox.Title_FID)]
        public virtual string Title {
            get {
                if (_title == null) {
_title = this.GetString(Duals.Models.Widgets.ContentTextBox.Title_FID);
                }
                return _title;
            }
            set {
                _title = null;
                this.SetString(Duals.Models.Widgets.ContentTextBox.Title_FID, value);
            }
        }
        
        [Field(Duals.Models.Widgets.ContentTextBox.Content_FID)]
        public virtual string Content {
            get {
                if (_content == null) {
_content = this.GetString(Duals.Models.Widgets.ContentTextBox.Content_FID);
                }
                return _content;
            }
            set {
                _content = null;
                this.SetString(Duals.Models.Widgets.ContentTextBox.Content_FID, value);
            }
        }
        
        [Field(Duals.Models.Widgets.ContentTextBox.Link_FID)]
        public virtual LinkField Link {
            get {
                return this.GetField<LinkField>(Duals.Models.Widgets.ContentTextBox.Link_FID);
            }
            set {
                this.SetField<LinkField>(Duals.Models.Widgets.ContentTextBox.Link_FID, value);
            }
        }

        [Field(Duals.Models.Widgets.ContentTextBox.SpacerHeight_FID)]
        public virtual string SpacerHeight {
            get {
                if (_spacerHeight == null) {
_content = this.GetString(Duals.Models.Widgets.ContentTextBox.SpacerHeight_FID);
                }
                return _spacerHeight;
            }
            set {
                _spacerHeight = null;
                this.SetString(Duals.Models.Widgets.ContentTextBox.SpacerHeight_FID, value);
            }
        }
        #endregion
    }
}
