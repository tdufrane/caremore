﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;

namespace Website.BL
{
	public class Member
	{
		#region Constructors

		public Member()
		{
			// To avoid dealing with nulls
			this.LastError = string.Empty;
		}

		public Member(string id) : this()
		{
			this.Id = id;
		}

		#endregion

		#region Constants

		private const int LOGON32_LOGON_INTERACTIVE = 2;
		private const int LOGON32_PROVIDER_DEFAULT = 0;
		private const int SECURITYIMPERSONATION = 2;

		#endregion

		#region DLL Imports

		[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern bool CloseHandle(IntPtr handle);

		//[DllImport("advapi32.dll", SetLastError=true)]
		//private static extern int ImpersonateLoggedOnUser(IntPtr hToken);

		[DllImport("advapi32.dll")]
		private static extern int LogonUser(
			string lpszUsername,
			string lpszDomain,
			string lpszPassword,
			int dwLoginType,
			int dwLoginProvider,
			ref IntPtr phToken);

		[DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private extern static bool DuplicateToken(
			IntPtr existingTokenHandle,
			int impersonationLevel,
			ref IntPtr duplicateTokenHandle);

		[DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern bool RevertToSelf();

		#endregion

		#region Fields

		/// <summary>Represents the impersonated Windows user.</summary>
		public WindowsImpersonationContext Context { get; private set; }

		/// <summary>Represents the Windows user's identity.</summary>
		public WindowsIdentity Identity { get; private set; }

		/// <summary>Represents the Windows user's principal.</summary>
		public IPrincipal Principal { get; private set; }

		#endregion

		#region Properties

		public string Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTime DateOfBirth { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }

		public string Question1 { get; set; }
		public string Question2 { get; set; }
		public string Question3 { get; set; }

		public string Answer1 { get; set; }
		public string Answer2 { get; set; }
		public string Answer3 { get; set; }

		public string LastError { get; private set; }
		public string LastRequest { get; private set; }
		public string LastResponse { get; private set; }

		#endregion

		#region Public methods

		public HttpCookie AddFormsTicket(HttpContext current, int timeout, string userData)
		{
			HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName);

			if (current != null)
			{
				current.User = this.Principal;

				DateTime timeLogin = DateTime.Now;

				FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
					this.Identity.Name,
					timeLogin, timeLogin.AddMinutes(timeout), false,
					userData, FormsAuthentication.FormsCookiePath);

				cookie.Value = FormsAuthentication.Encrypt(ticket);
			}

			return cookie;
		}

		public bool AddMember()
		{
			return PostToSailpoint(string.Format("identities/{0}", this.Id),
				BuildAddPost(), false);
		}

		public bool AddQuestionsAnswers()
		{
			return PostToSailpoint("workflows/CM_SetSQAForMembers_WF/launch",
				BuildQaPost(), true);
		}

		public bool ExistsInActiveDirectory()
		{
			this.LastError = string.Empty;

			this.LastRequest = string.Format("{0}identities/{1}/links", SailpointUrl(), this.Id);
			Uri address = new Uri(this.LastRequest);

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);
			request.Headers.Add("Authorization", SailpointAuthInfo());
			request.Method = "GET";

			ASCIIEncoding encoding = new ASCIIEncoding();

			request.ContentType = "application/json";

			using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
			{
				Stream stream = response.GetResponseStream();
				string responseData = new StreamReader(stream).ReadToEnd();
				ParseResponseData(responseData, true, false);
			}

			return (this.LastError.Length == 0);
		}

		public bool ExistsInActiveDirectory(string memberId)
		{
			this.Id = memberId;
			return ExistsInActiveDirectory();
		}

		public bool ExistsInDatabase()
		{
			bool exists = false;

			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Facets"].ToString()))
			{
				SqlCommand cmd = new SqlCommand();
				cmd.Connection = conn;
				cmd.CommandText = "CM_PT_CMC_ValidateMember";
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.Add(new SqlParameter("FirstName", this.FirstName));
				cmd.Parameters.Add(new SqlParameter("LastName", this.LastName));
				cmd.Parameters.Add(new SqlParameter("BirthDate", this.DateOfBirth));
				cmd.Parameters.Add(new SqlParameter("MemberID", this.Id));

				conn.Open();

				object obj = cmd.ExecuteScalar();
				exists = (bool)obj;
			}

			return exists;
		}

		public bool IsValid()
		{
			StringBuilder errors = new StringBuilder();

			// Member id
			if (string.IsNullOrWhiteSpace(this.Id))
			{
				errors.Append("Member ID is required.\r\n");
			}
			else if (this.Id.Length != 9)
			{
				errors.Append("Invalid Member ID length.\r\n");
			}
			else if (!Regex.IsMatch(this.Id, "(C[0-9]{8})|(CM[0-9]{7})"))
			{
				errors.Append("Member ID is not valid.\r\n");
			}

			// First name
			if (string.IsNullOrWhiteSpace(this.FirstName))
			{
				errors.Append("First Name is required.\r\n");
			}
			else if (this.FirstName.Length > 15)
			{
				errors.Append("Invalid First Name length.\r\n");
			}

			// Last name
			if (string.IsNullOrWhiteSpace(this.LastName))
			{
				errors.Append("Last Name is required.\r\n");
			}
			else if (this.LastName.Length > 35)
			{
				errors.Append("Invalid Last Name length.\r\n");
			}

			// Date of birth
			if (this.DateOfBirth.Equals(DateTime.MinValue))
			{
				errors.Append("Date of Birth is required.\r\n");
			}

			// Email address
			if (string.IsNullOrWhiteSpace(this.LastName))
			{
				errors.Append("Email address is required.\r\n");
			}
			else if (this.LastName.Length > 100)
			{
				errors.Append("Invalid Email Address length.\r\n");
			}

			// Password
			if (string.IsNullOrWhiteSpace(this.Password))
			{
				errors.Append("Password is required.\r\n");
			}
			else if ((this.Password.Length < 8) || (this.Password.Length > 20))
			{
				errors.Append("Invalid Password length.\r\n");
			}
			else
			{
				bool oneUpper = false, oneLower = false, oneNumber = false, oneOther = false;

				foreach (char character in this.Password)
				{
					if (Char.IsUpper(character))
						oneUpper = true;
					else if (Char.IsLower(character))
						oneUpper = true;
					else if (Char.IsNumber(character))
						oneNumber = true;
					else if (Char.IsPunctuation(character) || Char.IsSymbol(character))
						oneOther = true;
				}

				int criteriaCount = 0;
				if (oneUpper)
					criteriaCount += 1;

				if (oneLower)
					criteriaCount += 1;

				if (oneNumber)
					criteriaCount += 1;

				if (oneOther)
					criteriaCount += 1;

				/*
					a.	Minimum 1 uppercase
					b.	Minimum 1 lowercase
					c.	Minimum 1 numeric
					d.	Minimum 1 special character

					Must meet 3 of the 4 Minimum criteria
				*/
				if (criteriaCount < 3)
					errors.Append("Password does not meet complexity requirements.");
			}


			if (errors.Length == 0)
			{
				this.LastError = string.Empty;
				return true;
			}
			else
			{
				// Remove trailing \r\n
				errors.Length -= 2;

				this.LastError = errors.ToString();
				return false;
			}
		}

		public bool Login(string userName, string domain, string password)
		{
			bool success = false;
			IntPtr token = IntPtr.Zero;
			IntPtr tokenDuplicate = IntPtr.Zero;

			try
			{
				if (RevertToSelf())
				{
					int loggedIn = LogonUser(userName, domain, password, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, ref token);

					if (loggedIn > 0)
					{
						bool duplicated = DuplicateToken(token, SECURITYIMPERSONATION, ref tokenDuplicate);

						if (duplicated)
						{
							this.Context = WindowsIdentity.Impersonate(tokenDuplicate);
							this.Identity = new WindowsIdentity(tokenDuplicate);
						}
						else
						{
							this.Context = WindowsIdentity.Impersonate(token);
							this.Identity = new WindowsIdentity(token);
						}

						success = true;
						this.Principal = new WindowsPrincipal(this.Identity);
					}
				}
			}
			catch (Exception ex)
			{
				int win32Error = Marshal.GetLastWin32Error();

				if (win32Error == 0)
					throw ex;
				else
					throw new Win32Exception(win32Error);
			}
			finally
			{
				if (token != IntPtr.Zero)
					CloseHandle(token);

				if (tokenDuplicate != IntPtr.Zero)
					CloseHandle(tokenDuplicate);
			}

			return success;
		}

		public void Logout()
		{
			if (this.Context != null)
			{
				this.Context.Undo();
				this.Context = null;
			}
		}

		public bool SetFromDatabase(string memberId)
		{
			bool exists = false;

			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Facets"].ToString()))
			{
				SqlCommand cmd = new SqlCommand();
				cmd.Connection = conn;
				cmd.CommandText = "CM_PT_CMC_MemberDetails";
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.Add(new SqlParameter("MemberID", memberId));

				conn.Open();

				SqlDataReader reader = cmd.ExecuteReader();
				if (reader.HasRows)
				{
					exists = true;
					reader.Read();

					this.Id = memberId;
					this.FirstName = ((string)reader["MEME_FIRST_NAME"]).TrimEnd();
					this.LastName = ((string)reader["MEME_LAST_NAME"]).TrimEnd();
					this.DateOfBirth = (DateTime)reader["MEME_BIRTH_DT"];
					//this.Email = (string)reader[""];
				}
			}

			return exists;
		}

		#endregion

		#region Private methods

		private string BuildAddPost()
		{
			NameValueCollection table = new NameValueCollection();
			table.Add("UserType", "member");
			table.Add("email", this.Email);
			table.Add("name", this.Id);
			table.Add("lastname", this.LastName);
			table.Add("firstname", this.FirstName);
			table.Add("password", this.Password);

			return Json.Encode(table);
		}

		private string BuildQaPost()
		{
			// Hashtable changes sort order
			NameValueCollection inner = new NameValueCollection();
			inner.Add("identityName", this.Id);
			inner.Add("Q1", this.Question1);
			inner.Add("A1", this.Answer1);
			inner.Add("Q2", this.Question2);
			inner.Add("A2", this.Answer2);
			inner.Add("Q3", this.Question3);
			inner.Add("A3", this.Answer3);

			Hashtable outer = new Hashtable();
			outer.Add("workflowArgs", inner);

			return Json.Encode(outer);
		}

		private bool PostToSailpoint(string url, string data, bool allowNullStatus)
		{
			this.LastError = string.Empty;

			if (!IsValid())
				return false;

			Uri address = new Uri(ConfigurationManager.AppSettings["SailpointUrl"] + url);

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);
			request.Headers.Add("Authorization", SailpointAuthInfo());
			request.Method = "POST";

			this.LastRequest = data;
			ASCIIEncoding encoding = new ASCIIEncoding();
			byte[] bytes = encoding.GetBytes(this.LastRequest);

			request.ContentType = "application/x-www-form-urlencoded";
			request.ContentLength = bytes.Length;

			using (Stream requestStream = request.GetRequestStream())
			{
				requestStream.Write(bytes, 0, bytes.Length);
			}

			using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
			{
				Stream stream = response.GetResponseStream();
				string responseData = new StreamReader(stream).ReadToEnd();

				ParseResponseData(responseData, false, allowNullStatus);
			}

			return (this.LastError.Length == 0);
		}

		private void ParseResponseData(string data, bool checkObjects, bool allowNullStatus)
		{
			this.LastResponse = data;

			object decoded = Json.Decode(data);

			if (decoded == null)
			{
				this.LastError = "Unforeseen error.";
			}
			else
			{
				Hashtable table = (Hashtable)decoded;
				if (table["status"] == null)
				{
					//what to do?
					if (!allowNullStatus)
						this.LastError = "Status is null.";
				}
				else
				{
					string status = table["status"].ToString();

					if (status.Equals("success"))
					{
						if (checkObjects)
						{
							string adInfo = ParseArrayList(table, "objects");

							if (string.IsNullOrWhiteSpace(adInfo))
								this.LastError = "Login not found in Active Directory.";
						}
						else
						{
							this.LastError = string.Empty;
						}
					}
					else if (status.Equals("warning"))
					{
						this.LastError = ParseArrayList(table, "warnings");
					}
					else if (status.Equals("failure"))
					{
						this.LastError = ParseArrayList(table, "errors");
					}
				}
			}
		}

		private string ParseArrayList(Hashtable table, string fieldName)
		{
			ArrayList list = (ArrayList)table[fieldName];
			StringBuilder messages = new StringBuilder();

			foreach (var item in list)
			{
				messages.AppendLine(item.ToString());
			}

			// Remove trailing new line
			if (messages.Length > 2)
				messages.Length -= 2;

			return messages.ToString();
		}

		private string SailpointAuthInfo()
		{
			string login = ConfigurationManager.AppSettings["SailpointLogin"];
			return "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes(login));
		}

		private string SailpointUrl()
		{
			return ConfigurationManager.AppSettings["SailpointUrl"];
		}

		#endregion
	}
}
