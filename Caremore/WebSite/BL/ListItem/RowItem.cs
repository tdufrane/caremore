﻿using System;
using System.Collections.Generic;

namespace Website.BL.ListItem
{
	public class RowItem
	{
		public List<ColumnItem> Columns = new List<ColumnItem>();
		public string CssClass { get; set; }
		public bool ShowSeparator { get; set; }
	}
}
