﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;

namespace Website.BL.ListItem
{
    public class PlanItem : StandardItem
    {
        public Item PlanCountyItem { get; set; }
        public List<PlanFileItem> Files { get; set; }
        public String EnrollUrl { get; set; }
        public bool HasApplication { get; set; }
		public bool Visible { get; set; }

        public PlanItem(Item item)
            : base(item)
        {
            ;
        }
    }
}