﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.BL.ListItem
{
    public class PlanAnswerItem
    {
        public String Text { get; set; }
        public String Value { get; set; }
    }
}