﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;

namespace Website.BL.ListItem
{
	public class PlanQuestionItem : StandardItem
	{
        public enum QuestionType { CHECKLIST, RADIOLIST };

        public String QuestionText { get; set; }
        public QuestionType AnswerType { get; set; }
        public List<PlanAnswerItem> Answers = new List<PlanAnswerItem>();

        public PlanQuestionItem(Item item)
            : base(item)
        { ; }
	}
}