﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;

namespace Website.BL.ListItem
{
    public class PrimaryNavItem : StandardItem
    {
        public String Width { get; set; }
        public string CssClass { get; set; }
        public Item TargetItem { get; set; }

        public PrimaryNavItem(Item item)
            : base(item)
        {
            ;
        }
    }
}