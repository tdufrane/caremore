﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;

namespace Website.BL.ListItem
{
    public class LinkItem : StandardItem
    {
        public string Url { get; set; }
        public string Title { get; set; }

        public LinkItem(Item item)
            : base(item)
        { ; }
    }
}