﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;

namespace Website.BL.ListItem
{
    public class DetailItem : StandardItem
    {
        public bool HideRepeater { get; set; }
        public Item ColumnItem { get; set; }

        public DetailItem(Item item)
            : base(item)
        {
        }
    }
}