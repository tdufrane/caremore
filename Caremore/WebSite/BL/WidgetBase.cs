﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Data.Items;
using System.Web.UI;

namespace Website.BL
{
    public class WidgetBase : UserControl
    {
        public Item Item { get; set; }
        public Item ColumnItem { get; set; }
        public Item AppItem { get; set; }
    }
}
