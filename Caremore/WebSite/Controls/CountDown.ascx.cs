﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Website.Controls
{
    public partial class CountDown : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Item home = ItemIds.GetItem("HOME");

            CheckboxField ShowOpenEnrollmentCoundownClock = (CheckboxField)home.Fields["ShowOpenEnrollmentCoundownClock"];

            if(!ShowOpenEnrollmentCoundownClock.Checked)
            {
                this.Visible = false;
            }
            else
            {
                litCountDownClockTitle.Text = home["Count Down Clock Title"];
                litCountDownClockText.Text = home["Count Down Clock Text"];

                DateField enrollmentEndDate = (DateField)home.Fields["Enrollment End Date"];
                string enrollmentDateTimestring = enrollmentEndDate.Value;
                DateTime enrollmentDateTime = Sitecore.DateUtil.IsoDateToDateTime(enrollmentDateTimestring);

                if (enrollmentDateTime.Ticks > DateTime.Now.Ticks)
                {
                    string script = "<script type=\"text/javascript\"> $(document).ready(function()  {var endDate = new Date();endDate= new Date(" + enrollmentDateTime.ToString("yyyy, MM - 1, dd, HH, mm, ss") + "); $('#defaultCountdown').countdown({ until: endDate });});</script>";
                    this.Controls.Add(new LiteralControl(script));
                }
            }
        }
    }
}