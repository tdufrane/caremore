﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;
using Website.WebControls;

namespace Website.Controls
{
	public partial class Video : Website.Sublayouts.BaseUserControl
	{
//		#region Constructors
//
//		public Video()
//		{
//			Height = "200";
//			Width = "328";
//			ImageFieldName = "Video Image";
//		}
//
//		#endregion

		#region Properties

		public Item VideoItem { get; set; }

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected override void OnDataBinding(EventArgs e)
		{
			base.OnDataBinding(e);

			try
			{
				if (VideoItem == null)
				{
					VideoItem = WebControlUtil.GetItemFromDataBind(NamingContainer);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			if (VideoItem == null && DataSource != null && DataSource.TemplateName == "Video")
			{
				VideoItem = DataSource;
			}

			if (VideoItem != null)
			{
				Sitecore.Data.Fields.ImageField videoImage = VideoItem.Fields["Video Image"];

				if (videoImage == null || videoImage.MediaItem == null)
					vidControl.Poster = string.Empty;
				else
					vidControl.Poster = MediaManager.GetMediaUrl(videoImage.MediaItem);

				if (VideoItem["Autoplay"].Equals(CareMoreUtilities.CheckedTrueKey))
					vidControl.Attributes.Add("autoplay", "autoplay");
				else
					vidControl.Attributes.Remove("autoplay");

				LinkField videoUrl = VideoItem.Fields["Video URL"];

				if (string.IsNullOrWhiteSpace(videoUrl.Url))
					srcControl.Src = string.Empty;
				else
					srcControl.Src = videoUrl.Url;
			}
		}

		#endregion
	}
}
