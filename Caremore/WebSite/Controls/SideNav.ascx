﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SideNav.ascx.cs" Inherits="Website.Controls.SideNav" %>
<%@ Register TagPrefix="cm" TagName="Callouts" Src="~/Sublayouts/Controls/Callouts.ascx" %>

<!-- Begin Controls\SideNav -->
<cm:LeftNav ID="ucLeftNav" runat="server" />
<sc:Placeholder ID="phCustomNav" runat="server" Key="custom-nav" />
<cm:Callouts ID="ucCallouts" runat="server" WidgetCssClass="callout-lt-side" />
<!-- End Controls\SideNav -->
