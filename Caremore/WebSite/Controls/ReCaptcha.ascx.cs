﻿using System;
using System.Configuration;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore;

namespace Website.Controls
{
	public partial class ReCaptcha : System.Web.UI.UserControl
	{
		#region Protected Methods

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				string langKey;

				if (CareMoreUtilities.GetCurrentLanguage().Equals(CareMoreUtilities.SpanishKey, StringComparison.OrdinalIgnoreCase))
				{
					langKey = "?hl=es";
				}
				else
				{
					langKey = string.Empty;
				}

				ScriptManager.RegisterClientScriptInclude(Page, Page.GetType(), "reCaptcha",
					"//www.google.com/recaptcha/api.js" + langKey);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Public Methods

		public bool IsValid()
		{
			if (string.IsNullOrEmpty(Request.Form["g-recaptcha-response"]))
			{
				return false;
			}
			else
			{
				string encodedResponse = Request.Form["g-recaptcha-response"];
				string secretKey = ConfigurationManager.AppSettings["GoogleReCaptchaKey"];

				WebClient client = new WebClient();

				string googleReply = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, encodedResponse));

				return googleReply.Contains("\"success\": true");
			}
		}

		#endregion
	}
}
