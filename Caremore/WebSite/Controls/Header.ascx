﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Header.ascx.cs" Inherits="Website.Controls.Header" %>
<%@ Register TagPrefix="cm" Namespace="Website.WebControls" Assembly="Website" %>

<!-- Begin Controls\Header -->
<header>
	<div id="topBar" class="container-fluid hidden-print">
		<div class="container">
			<div class="row">
				<div class="hidden-xs col-md-7">
					<div id="topBarNav">
<asp:Repeater ID="rptTopNav" runat="server" OnItemDataBound="RptTopNav_ItemDataBound">
	<HeaderTemplate>
						<ul>
	</HeaderTemplate>
	<ItemTemplate>
							<li id="liTopNav" runat="server"><sc:Link ID="lnkTopNav" runat="server" Field="Link" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></li>
	</ItemTemplate>
	<FooterTemplate>
						</ul>
	</FooterTemplate>
</asp:Repeater>
					</div>
				</div>
				<div class="col-xs-12 col-md-5">
					<div class="row nav-top-right">
						<div id="hdrState" class="col-xs-8 col-md-5">
							<p><sc:Text ID="lblState" runat="server" Field="Name" />
								[<asp:HyperLink id="hlState" runat="server"><sc:Text ID="lblChangeState" runat="server" Field="Change Label" /></asp:HyperLink>]</p>
						</div>
						<div id="hdrLanguage" class="col-xs-4 text-xs-right col-md-3">
							<asp:LinkButton ID="lnkBtnLanguage" runat="server" CausesValidation="false" OnClick="LnkBtnLanguage_Click"><sc:Text ID="lblLanguage" runat="server" Field="Language Label" /></asp:LinkButton>
						</div>
						<div class="hidden-xs col-md-4">
							<div id="hdrTextSize" class="text-right">
								<span>
									<sc:Text ID="lblTextSize" runat="server" Field="Text Size Label" />:
									<a href="javascript:void(0);" id="smallTextTool" class="smallText">a</a>
									<a href="javascript:void(0);" id="largeTextTool" class="largeText">A</a>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<asp:Panel ID="pnlHeaderBanner" runat="server" ClientIDMode="Static" CssClass="container banner">
	<div class="row">
		<div class="col-xs-12 text-xs-center col-md-4 logo">
			<a href="/">
				<sc:Image id="imgLogo" runat="server" Field="Image" />
			</a>
		</div>
		<div class="col-xs-12 col-md-offset-4 col-md-4 pull-right search hidden-print"">
			<asp:Panel ID="pnlSearch" runat="server" CssClass="form-search" DefaultButton="lnkBtnSearch">
				<div class="input-group">
					<asp:TextBox ID="txtSearchInput" runat="server" CssClass="form-control" />
					<div class="input-group-addon">
						<asp:LinkButton ID="lnkBtnSearch" runat="server" OnClick="LnkBtnSearch_OnClick" CausesValidation="false"><span class="glyphicon glyphicon-search" style=""></span></asp:LinkButton>
					</div>
				</div>
			</asp:Panel>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 text-xs-center">
			<h1><sc:Text ID="txtPageTitle" runat="server" Field="Page Title" /></h1>
		</div>
	</div>
	<div class="row">
<asp:PlaceHolder ID="phHomeAnthem" runat="server" Visible="false">
		<div class="col-xs-12 text-xs-center home-note">
			<sc:Text ID="txtAnthemPlanText" runat="server" Field="Anthem Plan Text" />
		</div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phHomeCareMore" runat="server" Visible="false">
		<asp:Panel ID="pnlCalloutLt" runat="server" CssClass="col-xs-12 col-md-3 text-xs-center">
			<h3><sc:Text ID="txtCalloutHeadline" runat="server" Field="Callout Headline" /></h3>
			<p><cm:Link ID="lnkCallout" runat="server" CssClass="btn btn-primary btn-lg" Field="Callout Link" /></p>
		</asp:Panel>
		<asp:Panel ID="pnlCalloutRt" runat="server" CssClass="col-xs-12 col-md-3 text-xs-center">
			<h3><sc:Text ID="txtCalloutSubheadline" runat="server" Field="Callout Subheadline" /></h3>
			<p><cm:Link ID="lnkEnroll" runat="server" CssClass="btn btn-primary btn-lg" Field="Enroll Now Link" /></p>
		</asp:Panel>
</asp:PlaceHolder>
	</div>
</asp:Panel>

<nav id="navBarMain" class="navbar navbar-default cbp-af-header hidden-print" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" type="button">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div id="navbar-collapse-1" class="collapse navbar-collapse">
<asp:Repeater ID="rptMainNav" runat="server">
	<HeaderTemplate>
			<ul id="navMain" class="nav navbar-nav">
	</HeaderTemplate>
	<ItemTemplate>
				<li><sc:Link ID="lnkTopNav" runat="server" Field="Link" Item='<%# Eval("Item") %>' /></li>
	</ItemTemplate>
	<FooterTemplate>
			</ul>
	</FooterTemplate>
</asp:Repeater>
		</div>
	</div>
</nav>

<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
<!-- End Header -->
