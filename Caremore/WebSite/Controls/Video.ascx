﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Video.ascx.cs" Inherits="Website.Controls.Video" %>

<!-- Begin Controls/Video -->
<div class="embed-responsive embed-responsive-4by3">
	<video id="vidControl" runat="server" class="embed-responsive-item" controls="controls" preload="none">
		<source id="srcControl" runat="server" type="video/mp4" />
	</video>
</div>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
<!-- End Controls/Video -->
