﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using Sitecore.Collections;
using Sitecore.Data.Items;

namespace Website.Controls
{
	public partial class Footer : Website.Sublayouts.BaseUserControl
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				CareMoreUtilities.ClearControl(phError);
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			LoadNavigationLinks();
			SetCmsApproval();
			SetLastUpdateDate();

			CareMoreUtilities.SetStateItem(base.CurrentStateId, "NON_DISCRIMINATION_NOTICE", frNonDiscrimination);

			if (frNonDiscrimination.Item == null)
			{
				pnlNonDiscrimination.Visible = false;
				pnlDisclaimer.CssClass = "col-xs-12 text-center";
			}
			else
			{
				pnlNonDiscrimination.Visible = true;
				pnlDisclaimer.CssClass = "col-xs-12 text-xs-center col-md-6 text-right";
			}

			CareMoreUtilities.SetStateItem(base.CurrentStateId, "DISCLAIMER_TEXT_ID", frDisclaimer);
		}

		private void LoadNavigationLinks()
		{
			List<Item> folders = GetFooterLinks("FOOTER_ID");

			if (folders != null && folders.Count > 0)
			{
				SetFooterLinks(folders, "Left", rptFooterPrimaryLeft);
				SetFooterLinks(folders, "Middle", rptFooterPrimaryMiddle);
				SetFooterLinks(folders, "Right", rptFooterPrimaryRight);
			}

			rptFooterSecondaryLinks.DataSource = GetFooterLinks("FOOTER_HIGHLIGHT_ID");
			rptFooterSecondaryLinks.DataBind();

			rptSocial.DataSource = GetSocial();
			rptSocial.DataBind();
		}

		private List<Item> GetFooterLinks(string parentId)
		{
			Item parent = ItemIds.GetItem(parentId);
			return CareMoreUtilities.GetItemsCheckState(parent);
		}

		private List<Item> GetFooterLinks(ChildList items)
		{
			List<Item> list = new List<Item>();

			foreach (Item item in items)
			{
				if (!string.IsNullOrWhiteSpace(item["Link"]))
					list.Add(item);
			}

			return list;
		}

		private DateTime? GetLastUpdateFromProviderSearch()
		{
			// Dont let this cause rest of page to not render
			try
			{
				ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
				ProvidersInformation results = dbContext.CM_PT_CMC_ProvidersLastUpdated().FirstOrDefault();

				if (results == null)
					return null;
				else
					return results.LastUpdatedOn;
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
				return null;
			}
		}

		private ChildList GetSocial()
		{
			Item parent = ItemIds.GetItem("FOOTER_SOCIAL_ID");
			return parent.Children;
		}

		private void SetCmsApproval()
		{
			if (Sitecore.Context.Item["Override"].Equals("1"))
			{
				frCmsApproval.FieldName = "OverrideText";
				frCmsApproval.Item = Sitecore.Context.Item;
			}
			else
			{
				CareMoreUtilities.SetStateItem(base.CurrentStateId, "APPROVAL_TEXT_ID", frCmsApproval);
			}
		}

		private void SetFooterLinks(List<Item> folders, string itemName, Repeater displayRepeater)
		{
			Item folder = folders.Where(i => i.Name.Equals(itemName)).SingleOrDefault();

			if (folder == null)
			{
				displayRepeater.Visible = false;
				//TODO: Hidden/log error?
			}
			else
			{
				displayRepeater.DataSource = folder.Children;
				displayRepeater.DataBind();
			}
		}

		private void SetLastUpdateDate()
		{
			txtLastUpdatedLabel.Item = base.ConfigurationItem;

			DateTime? lastUpdateDt = null;
			String dateFormat = ItemIds.GetItemValue("DATE_FORMAT", "Text");

			if (Sitecore.Context.Item.ID == ItemIds.GetID("PROVIDER_DETAIL"))
			{
				lastUpdateDt = GetLastUpdateFromProviderSearch();
			}

			if (lastUpdateDt == null)
			{
				lastUpdateDt = Sitecore.Context.Item.Statistics.Updated;
			}

			lblLastUpdated.Text = lastUpdateDt.Value.ToString(dateFormat);
		}


		#endregion
	}
}
