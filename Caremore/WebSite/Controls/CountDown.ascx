﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CountDown.ascx.cs" Inherits="Website.Controls.CountDown" %>
<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery.countdown.js"></script>
<link href="/css/jquery.countdown.css" rel="stylesheet" type="text/css" />

<div class="bannerHeader">
    <h4><asp:Literal runat="server" ID="litCountDownClockTitle" /></h4>
    <div style="margin-top: -10px;" id="defaultCountdown"></div>
    <br />
    <br />
    <p><asp:Literal runat="server" ID="litCountDownClockText" /></p>
</div>


