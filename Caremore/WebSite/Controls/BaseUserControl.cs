﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using CareMore.Web.DotCom;

namespace Website.Controls
{
	public class XBaseUserControl : System.Web.UI.UserControl
	{
		protected Item ConfigurationItem = null;
		protected Item CurrentLocaleItem = null;

		protected string CurrentStateId = string.Empty;
		protected string CurrentLanguage = string.Empty;

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			this.ConfigurationItem = ItemIds.GetItem("MAPD_SITE_SETTINGS");
			this.CurrentLocaleItem = CareMoreUtilities.GetCurrentLocaleItem();

			this.CurrentStateId = CurrentLocaleItem.ID.ToString();
			this.CurrentLanguage = CareMoreUtilities.GetCurrentLanguage();
		}
	}
}