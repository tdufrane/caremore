﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="GoogleMap.ascx.cs" Inherits="Website.Controls.GoogleMap" %>

<!-- Begin Controls/GoogleMap -->
<script type="text/javascript" src="/js/googleMaps.min.js"></script>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=<%=GoogleMapID %>"></script>

<asp:Literal ID="litLocationsArray" runat="server" />

<script type="text/javascript">
	$(document).ready(function () {
		gMapInitialize(<%= InitialZoom %>);
	});
</script>
<div class="embed-responsive embed-responsive-16by9">
	<div id="mapCanvas" class="embed-responsive-item"></div>
</div>
<!-- End Controls/GoogleMap -->
