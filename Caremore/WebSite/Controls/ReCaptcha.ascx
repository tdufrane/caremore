﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ReCaptcha.ascx.cs" Inherits="Website.Controls.ReCaptcha" %>

<!-- Begin Controls/ReCaptcha -->
<div class="g-recaptcha" data-sitekey="6LfG4BMUAAAAAP0QYqJ2-gOvqYgkUBWkQsYkULWp"></div>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder><!-- End Controls/ReCaptcha -->
