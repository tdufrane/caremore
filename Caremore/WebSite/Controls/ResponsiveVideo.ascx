﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ResponsiveVideo.ascx.cs" Inherits="Website.Controls.ResponsiveVideo" %>
<div class="embed-responsive embed-responsive-4by3">
	<video id="vidControl" runat="server" class="embed-responsive-item" controls="controls">
		<source id="srcControl" runat="server" type="video/mp4" />
	</video>
</div>
