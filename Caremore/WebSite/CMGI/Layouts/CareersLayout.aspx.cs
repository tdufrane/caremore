﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Links;
using CMGI.BL;

namespace CMGI.Layouts
{
	public partial class CareersLayout : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Item homeItem = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath);
			Item currentItem = Sitecore.Context.Item;
			DateTime today = DateTime.Today;

			var jobs = from item in currentItem.GetChildren()
					   where item["Hide Job"] != "1" && Sitecore.DateUtil.IsoDateToDateTime(item["Date"]) <= today &&
					         item.TemplateName == "Career"
					   select item;

			if (jobs != null)
			{
				rptCareers.DataSource = jobs;
				rptCareers.DataBind();

				if (jobs.Count() == 0)
				{
					rptCareers.Visible = false;
					lblEmptyCareer.Visible = true;
				}
			}			
		}

		protected string ItemURL(object item)
		{
			return LinkManager.GetItemUrl(item as Item);
		}

	}
}