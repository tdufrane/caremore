﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventItem.aspx.cs" Inherits="CMGI.Layouts.EventItem" MasterPageFile="Base.Master" %>
<%@ MasterType VirtualPath="Base.Master" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="/CMGI/Sublayouts/LeftNavigation.ascx" %>

<asp:content runat="server" id="Content1" contentplaceholderid="head">
</asp:content>

<asp:content runat="server" id="PatientBody" contentplaceholderid="MainBodyContent">
	<div class="container">
		<!-- begin left side navigation -->
			<cg:LeftNavigation runat="server" />
		<!-- end left side navigation -->

		<div class="copy">
			<div class="story">
				<p class="big-blue"><span style="color:Black;">Event:</span> <cg:Text Field="Name" runat="server" /></p>

				<p><strong>Event Date:</strong> <cg:Date Field="Date" runat="server" /></p>

				<asp:Panel ID="pnlAddress" runat="server">
					<strong>Address:</strong><br />
					<cg:Text Field="Address 1" runat="server" /><br />
					<asp:PlaceHolder ID="plcAddress2" runat="server">
						<cg:Text Field="Address 2" runat="server" /><br />
					</asp:PlaceHolder>
					<cg:Text Field="City" runat="server" />,
					<cg:Text Field="State" runat="server" />
					<cg:Text Field="Zip Code" runat="server" />
				</asp:Panel>

				<cg:Text Field="Description" runat="server" />

				<p><a href="javascript:history.go(-1);">&lt; Go Back</a></p>
			</div>
			<div class="clear_both"></div>
		</div>
		<div class="clear_both"></div>
	</div>
</asp:content>