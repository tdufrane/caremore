﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CareerItem.aspx.cs" Inherits="CMGI.Layouts.CareerItem" MasterPageFile="Base.Master" %>
<%@ MasterType VirtualPath="Base.Master" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="/CMGI/Sublayouts/LeftNavigation.ascx" %>

<asp:content runat="server" id="Content1" contentplaceholderid="head">
<style type="text/css">
.infoLeft
{
	float: left;
	width: 15%;
	display: inline-block;
	padding: 0 0 0.5em;
	text-align: right;
}

.infoRight
{
	float: left;
	width: 84%;
	padding: 0 0 0.5em 1%;
}

.careerTop
{
	padding: 1em;
	padding-bottom: 2em;
	margin-bottom: 1em;
	background-color: #ededed;
	border-style: solid;
	border-color: #B5B5B3;
	border-width: 1px;
}

.careerMain
{
	padding: 1em;
	border-style: solid;
	border-color: #B5B5B3;
	border-width: 1px;
}
</style>
</asp:content>

<asp:content runat="server" id="CareerBody" contentplaceholderid="MainBodyContent">
	<div class="container">
		<div class="noSideMenu">
			<div class="career">
				<div class="careerTop">
					<p><strong><sc:Text ID="JobTitle" Field="Job Title" runat="server" /></strong></p>

					<div class="infoLeft"><strong>Location: </strong></div>
					<div class="infoRight"><sc:Text ID="City" Field="Location City" runat="server" />, <sc:Text ID="State" Field="Location State" runat="server" /></div>
					<div class="infoLeft"><strong>Category: </strong></div>
					<div class="infoRight"><sc:Text ID="Category" Field="Category" runat="server" /></div>
					<div class="infoLeft"><strong>Job Post Date: </strong></div>
					<div class="infoRight"><sc:Date ID="Date" Field="Date" Format="MM/dd/yyyy" runat="server" /></div>
					<div class="infoLeft"><strong>Employer: </strong></div>
					<div class="infoRight"><sc:Text ID="Employer" Field="Employer" runat="server" /></div>
					<div class="clear_both"></div>
				</div>

				<div class="careerMain">
					<p><strong>Job Description</strong></p>
					<div><sc:Text ID="Description" Field="Description" runat="server" /></div>

					<br /><br />

					<p><strong>Job Qualifications</strong></p>
					<div><sc:Text ID="Qualification" Field="Qualification" runat="server" /></div>

					<br /><br />

					<p><strong>Contact Information</strong></p>

					<!-- Contact Info -->
					<p><sc:Text ID="contactFname" Field="First Name" runat="server" /> <sc:Text ID="contactLname" Field="Last Name" runat="server" /><br />
					<sc:Text ID="address1" Field="Address 1" runat="server" /> <sc:Text ID="address2" Field="Address 2" runat="server" /><br />
					<sc:Text ID="contactCity" Field="City" runat="server" />, <sc:Text ID="contactState" Field="State" runat="server" /> <sc:Text ID="contactZip" Field="Zip Code" runat="server" /></p>
					<p><strong>Phone:</strong> <sc:Text ID="contactPhone" Field="Phone" runat="server" /><br />
					<strong>Fax:</strong> <sc:Text ID="contactFax" Field="Fax" runat="server" /><br />
					<strong>Email:</strong> <asp:HyperLink ID="emailLink" runat="server"><sc:Text ID="contactEmail" Field="Email" runat="server" /></asp:HyperLink><br /></p>
				</div>

				<p><a href="javascript:history.go(-1);">&lt; Go Back</a></p>
			</div>
			<div class="clear_both"></div>
		</div>
		<div class="clear_both"></div>
	</div>
</asp:content>
