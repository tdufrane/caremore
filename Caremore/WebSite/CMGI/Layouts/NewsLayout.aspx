﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="false" CodeBehind="NewsLayout.aspx.cs" Inherits="CMGI.Layouts.NewsLayout" MasterPageFile="Base.Master" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="/CMGI/Sublayouts/LeftNavigation.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainBodyContent" runat="server">
	<div class="container">
		<!-- begin left side navigation -->
		<cg:LeftNavigation runat="server" />
		<!-- end left side navigation -->

		<div class="copy">
			<div class="title">
				<h1><cg:Text Field="Title" runat="server" /></h1>
			</div>

			<cg:Text Field="Description" runat="server" />

			<div class="news">
				<asp:Repeater ID="rptNews" runat="server"  onitemdatabound="rptNews_ItemDataBound">
					<ItemTemplate>
						<p><strong><asp:Label ID="newsDate" runat="server" /></strong>
						-
						<asp:Label ID="newsShort" runat="server" />
						<asp:HyperLink ID="newsLink" runat="server">Full Article &gt;</asp:HyperLink></p>
					</ItemTemplate>

					<SeparatorTemplate>
						<img src="/CMGI/images/news-border.jpg" alt=" " width="100%" height="1" />
					</SeparatorTemplate>
				</asp:Repeater>
			</div>

			<p class="link"><asp:hyperlink ID="lnkArchives" runat="server">View Archives</asp:hyperlink></p>
		</div>

		<div class="clear_both"></div>
	</div>
</asp:Content>
