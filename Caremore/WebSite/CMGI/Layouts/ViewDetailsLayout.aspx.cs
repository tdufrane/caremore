﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Sitecore.Data.Items;
using Sitecore.Data.Fields;

using CMGI.BL.ListItem;

namespace CMGI.Layouts
{
	public partial class ViewDetailsLayout : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Item curr = Sitecore.Context.Item;

			string address = String.Format("{0} {1} {2} {3}", curr["Address 1"], curr["City"], curr["State"], curr["Zip Code"]);
			string url = String.Format(@"http://maps.google.com/maps?q={0}&t=m&z=16", HttpUtility.UrlEncode(address));

			lnkDirections.NavigateUrl = url;
			lnkDirections.Target = "_BLANK";

			if ((!string.IsNullOrEmpty(curr["Address 2"])) && (string.IsNullOrEmpty(curr["MultipleSuiteLocation"])))
				HTMLBreak.Text = "<br />";

			lblMoreInfo.Visible = BodyText.Visible = !(string.IsNullOrWhiteSpace(Sitecore.Context.Item["Body"]));

			var physiciansAtLocation = curr.Children;

			List<CMGI.BL.ListItem.PhysicianItem> phyItem = new List<BL.ListItem.PhysicianItem>();

			foreach (Item i in physiciansAtLocation)
			{
				phyItem.Add(new CMGI.BL.ListItem.PhysicianAtLocationItem(i));
			}

			rptPhysicians.DataSource = phyItem;
			rptPhysicians.DataBind();
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			rptPhysicians.ItemDataBound += new RepeaterItemEventHandler(rptPhysicians_ItemDataBound);
		}

		protected void rptPhysicians_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				PhysicianAtLocationItem phy = (PhysicianAtLocationItem)e.Item.DataItem;

				((Label)e.Item.FindControl("PhysicianSuite")).Text = string.IsNullOrEmpty(phy.PhysicianSuite) ? "" : phy.PhysicianSuite + "<br />";
				((Label)e.Item.FindControl("PhysFirstName")).Text = phy.FirstName;
				((Label)e.Item.FindControl("PhysLastName")).Text = phy.LastName;
				((Label)e.Item.FindControl("PhysSpec")).Text = string.Join(", ", phy.Specialty); // strBuilder.ToString();
				((HyperLink)e.Item.FindControl("PhysBio")).NavigateUrl = Sitecore.Links.LinkManager.GetItemUrl(phy.Item);

				((Literal)e.Item.FindControl("PhysHours")).Text = phy.HoursAtLocation;
				((Literal)e.Item.FindControl("PhysPhone")).Text = !string.IsNullOrEmpty(phy.PhoneAtLocation) ? string.Format("<b>Phone</b>: {0}", phy.PhoneAtLocation) : string.Empty;
				((Literal)e.Item.FindControl("PhysFax")).Text = !string.IsNullOrEmpty(phy.FaxAtLocation) ? string.Format("<b>Fax</b>: {0}", phy.FaxAtLocation) : string.Empty;
			}
		}
	}
}
