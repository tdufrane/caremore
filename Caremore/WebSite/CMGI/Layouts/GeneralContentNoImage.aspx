﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeneralContentNoImage.aspx.cs" Inherits="CMGI.Layouts.GeneralContentNoImage" MasterPageFile="Base.Master" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainBodyContent" runat="server">
	<div class="container">
		<div class="noSideMenu">
			<div class="title"><h1><sc:Text ID="TextTitle" runat="server" Field="Title" Item="<%# Sitecore.Context.Item %>" /></h1></div>

			<div class="message">
				<sc:Text ID="TextBody" runat="server" field="Body" Item="<%# Sitecore.Context.Item %>" />
				<sc:PlaceHolder ID="PlcCustom" runat="server" Key="custom" />
			</div>
		</div>
	</div>
</asp:Content>
