<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmptyLayout.aspx.cs" Inherits="CMGI.Layouts.EmptyLayout" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head id="head1" runat="server">
	<title></title>
    <!-- Static meta-tags -->
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta name="language" content="en-US" />

	<!-- CSS -->
	<link href="/css/style.css" rel="stylesheet" type="text/css" />

	<!-- JavaScript -->
    <script type="text/javascript" language="javascript" src="/js/global.js"></script>
</head>

<body ID="bodyTag" runat="server">
    <form method="post" ID="mainform" runat="server" name="mainform">
    <div id="mainContent">
		<!-- begin main content -->
		<sc:Placeholder ID="MainContent" runat="server" Key="content" />
		<!-- end main content -->
	</div>
    </form>
</body>
</html>