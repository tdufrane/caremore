﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Physicians.aspx.cs" Inherits="CMGI.Layouts.Physicians" MasterPageFile="Base.Master" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="/CMGI/Sublayouts/LeftNavigation.ascx" %>

<asp:content runat="server" id="Content1" contentplaceholderid="head">
</asp:content>

<asp:content runat="server" id="PhysicianBody" contentplaceholderid="MainBodyContent">
<div class="container">
	<!-- begin left side navigation -->
	<cg:leftnavigation runat="server" id="cmgiLeftNavigation" />
	<!-- end left side navigation -->

	<asp:Panel ID="pnlList" runat="server" CssClass="copy" DefaultButton="btnSearchPhysicians">
		<div class="title">
			<h1><cg:Text ID="PageTitle" Field="Title" runat="server" /></h1>
		</div>

		<table border="0" cellspacing="0" cellpadding="0" style="padding: 0px;width: 700px;">
			<tr style="height: 60px;">
				<td style="border-bottom:1px solid #9CC7E7;width: 90px;"><strong>Sort by:</strong></td>
				<td style="border-bottom:1px solid #9CC7E7;width: 160px;">
					<asp:DropDownList ID="sortBy" CssClass="dropdown" runat="server"></asp:DropDownList>
				</td>
				<td style="border-bottom:1px solid #9CC7E7;width: 40px;"><span style="font-weight: bold;">and</span></td>
				<td style="border-bottom:1px solid #9CC7E7;width: 120px;"><asp:TextBox CssClass="zip defaultValue" runat="server" ID="txtZip" /> </td>
				<td style="border-bottom:1px solid #9CC7E7;">&nbsp;<asp:Button ID="btnSort" OnClick="btnSubmit_Click" Text="Sort" CssClass="lgButton" runat="server" /> </td>
				<td></td>
			</tr>
			<tr>
				<td style="padding-top:10px;"><strong>Search by:</strong></td>
				<td style="padding-top:10px;"><asp:TextBox ID="txtSearch" runat="server" CssClass="search defaultValue" /></td>
				<td colspan="2" style="padding-top:10px;">&nbsp; &nbsp; <asp:Button ID="btnSearchPhysicians" OnClick="btnSubmit_Click" Text="Search" CssClass="lgButton" runat="server" /></td>
				<td>&nbsp;</td>
				<td align="right" valign="bottom">
				</td>
			</tr>
		</table>

		<div class="sort-nav" style="text-align:right;">
			<asp:Panel ID="pageGroup" runat="server">
				<asp:Hyperlink ID="lnkLast" runat="server" Visible="false">&lt; previous</asp:Hyperlink><asp:Label ID="lblLast" runat="server" Visible="false" CssClass="light-gray">&lt; previous</asp:Label> |
				<asp:Repeater ID="rptPage" runat="server" onitemdatabound="rptPage_ItemDataBound">
					<ItemTemplate><asp:Hyperlink ID="lnkPageNum" runat="server" Visible="false" /><asp:Label ID="lblPageNum" CssClass="black" runat="server" Visible="false" /> </ItemTemplate>
				</asp:Repeater>
				| <asp:HyperLink ID="lnkNext" runat="server" Visible="false">next &gt;</asp:HyperLink><asp:Label ID="lblNext" runat="server" Visible="false" CssClass="light-gray">next &gt;</asp:Label> |
				<asp:HyperLink ID="lnkAll" runat="server" Visible="false">View All</asp:HyperLink><asp:Label ID="lblAll" runat="server" Visible="false" CssClass="light-gray">View All</asp:Label>
			</asp:Panel>

			<asp:Label ID="pageArea" runat="server" />
		</div>

		<div class="phys-table">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td style="width:284px;" class="hdr left-cell">Name</td>
					<td style="width:214px;" class="hdr left-cell border">Contact</td>
					<td style="width:155px;" class="hdr left-cell border">Specialty</td>
				</tr>

			<asp:ListView ID="lvPhysicians" runat="server" onitemdatabound="lvPhysicians_ItemDataBound">
				<ItemTemplate>
					<tr>
						<td class="left-cell">
							<asp:Hyperlink ID="PicBio" runat="server"><asp:Image ID="PhysPic" runat="server" CssClass="physician" /></asp:Hyperlink>
							<p class="dr">
								<strong><asp:Label ID="PhysFirstName" runat="server" /> <asp:Label ID="PhysLastName" runat="server" /></strong>
							</p>
							<p><asp:Hyperlink ID="PhysBio" runat="server">Read full bio</asp:Hyperlink></p>
						</td>
						<td class="left-cell border">
							<div class="physblock">
								<asp:Label ID="lblAddresses" runat="server" />
							</div>
						</td>
						<td class="left-cell border">
							<div class="physblock2">
								<asp:Literal ID="PhysSpec" runat="server" />
							</div>
						</td>
					</tr>
				</ItemTemplate>

				<AlternatingItemTemplate>
					<tr class="alt">
						<td class="left-cell">
							<asp:Hyperlink ID="PicBio" runat="server"><asp:Image ID="PhysPic" runat="server" CssClass="physician" /></asp:Hyperlink>
							<p class="dr">
								<strong><asp:Label ID="PhysFirstName" runat="server" /> <asp:Label ID="PhysLastName" runat="server" /></strong>
							</p>
							<p><asp:Hyperlink ID="PhysBio" runat="server">Read full bio</asp:Hyperlink></p>
						</td>
						<td class="left-cell border">
							<div class="physblock">
								<asp:Label ID="lblAddresses" runat="server" />
							</div>
						</td>
						<td class="left-cell border">
							<div class="physblock2">
								<asp:Literal ID="PhysSpec" runat="server" />
							</div>
						</td>
					</tr>
				</AlternatingItemTemplate>

				<EmptyDataTemplate>
					<tr>
						<td colspan="5"><div class="info"><center>No results were found.</center></div></td>
					</tr>
				</EmptyDataTemplate>
			</asp:ListView>

			</table>
		</div>

		<div class="pagebottom">
			<span class="sort-nav">
				<asp:Panel ID="PageGroupBottom" runat="server">
					<asp:Hyperlink ID="lnkLast2" runat="server" Visible="false">&lt; previous</asp:Hyperlink><asp:Label ID="lblLast2" runat="server" Visible="false" CssClass="light-gray">&lt; previous</asp:Label> |
					<asp:Repeater ID="rptPage2" runat="server" onitemdatabound="rptPage_ItemDataBound">
						<ItemTemplate>
							<asp:Hyperlink ID="lnkPageNum" runat="server" Visible="false" /><asp:Label ID="lblPageNum" CssClass="black" runat="server" Visible="false" />
						</ItemTemplate>
					</asp:Repeater>
					| <asp:HyperLink ID="lnkNext2" runat="server" Visible="false">next &gt;</asp:HyperLink><asp:Label ID="lblNext2" runat="server" Visible="false" CssClass="light-gray">next &gt;</asp:Label> |
					<asp:HyperLink ID="lnkAll2" runat="server" Visible="false">View All</asp:HyperLink><asp:Label ID="lblAll2" runat="server" Visible="false" CssClass="light-gray">View All</asp:Label>
				</asp:Panel>
			</span>
		</div>
	</asp:Panel>

	<div class="clear_both"></div>
</div>
</asp:content>