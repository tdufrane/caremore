﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;

using CMGI.BL.ListItem;
using CMGI.BL;

namespace CMGI.Layouts
{
	public partial class BioLayout : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Item currItem = Sitecore.Context.Item;

			// Get physician addresses
			var physician = Sitecore.Context.Database.SelectItems(Utility.FIND_LOCATIONS_PATH + string.Format("//*[@Physician='{0}']", currItem.ID));

			rptAddresses.DataSource = physician
				.ToList().ConvertAll(item =>
					new AddressItem(item.Parent,
					item.Fields["Hours"].Value,
					item.Fields["Phone"].Value,
					item.Fields["Fax"].Value,
					item.Fields["PhysicianSuite"].Value));
			rptAddresses.DataBind();

			if (string.IsNullOrEmpty(Sitecore.Context.Item["Video Link"]))
			{
				MeetDocSection.Visible = false;
			}
			else
			{
				LinkField video = (LinkField)currItem.Fields["Video Link"];
				MeetDocTitle.Text = video.Text;

				Item videoItem = video.TargetItem;
				MeetDocImage.Item = videoItem;

				MeetDocLink.NavigateUrl = Utility.MEET_THE_DOCTORS_PATH + "?vid=" + video.TargetID.ToString();
			}
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
			Item currItem = Sitecore.Context.Item;
			Master.SetPageTitle(string.Format("{0} {1}", currItem["First Name"], currItem["Last Name"]));
		}

		protected void rptAddresses_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				AddressItem item = e.Item.DataItem as AddressItem;

				Label add = e.Item.FindControl("Address") as Label;
				HyperLink lnk = e.Item.FindControl("Link") as HyperLink;
				Label hrs = e.Item.FindControl("Hours") as Label;
				Label nmbrs = e.Item.FindControl("Numbers") as Label;

				string address = String.Format("{0} {1} {2} {3}", item.Address1, item.City, item.State, item.Zip);
				string url = String.Format(@"http://maps.google.com/maps?q={0}&t=m&z=16", HttpUtility.UrlEncode(address));

				add.Text = item.Address(true);
				hrs.Text = item.Hours;
				nmbrs.Text = item.Numbers();
				lnk.NavigateUrl = url;
			}
		}
	}
}
