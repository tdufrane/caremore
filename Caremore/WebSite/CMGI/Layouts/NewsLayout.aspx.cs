﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Links;
using CMGI.BL;
using CareMore.Web.DotCom;

namespace CMGI.Layouts
{
	public partial class NewsLayout : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			rptNews.DataSource = GetNews();
			rptNews.DataBind();

			Item newsloc = ItemIds.GetItem("NEWS_ARCHIVES_ID");
			lnkArchives.NavigateUrl = Sitecore.Links.LinkManager.GetItemUrl(newsloc);
		}

		public Item[] GetNews()
		{
			Item currItem = Sitecore.Context.Item;

			int maxPosts = int.Parse(currItem["Articles to Display"]);

			var children = currItem.Axes.GetDescendants();
			var news = from child in children
					   where
						   child.TemplateName == "News Item"
					   select child;

			var results = news.OrderByDescending(i => ((DateField)i.Fields["Date"]).DateTime);
			if (results.Count() > 0)
				return results.Take(maxPosts).ToArray();

			return new Item[] { };
		}

		protected void rptNews_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				Item i = e.Item.DataItem as Item;

				if (i != null)
				{
					Label date = e.Item.FindControl("newsDate") as Label;
					Label nshort = e.Item.FindControl("newsShort") as Label;

					DateField df = i.Fields["Date"];
					if (df != null)
						date.Text = df.DateTime.ToShortDateString();
					nshort.Text = i["Teaser"];

					HyperLink link = e.Item.FindControl("newsLink") as HyperLink;
					link.NavigateUrl = LinkManager.GetItemUrl(i);
				}
			}
		}
	}
}
