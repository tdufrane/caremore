﻿<%@ Page Title="" Language="C#" MasterPageFile="Base.Master" AutoEventWireup="true" CodeBehind="ViewDetailsLayout.aspx.cs" Inherits="CMGI.Layouts.ViewDetailsLayout" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics" %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="/CMGI/Sublayouts/LeftNavigation.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainBodyContent" runat="server">
	<div class="container">
		<div class="noSideMenu">
			<div class="title">
				<h1><cg:Text Field="Title" runat="server" /></h1>
			</div>

			<h4 style="margin-bottom:0px;">Address:</h4>

			<div>
				<div style="float:left;">
					<p><cg:Text Field="Address 1" runat="server" /><br />
						<cg:Text Field="Address 2" runat="server" />
						<asp:Literal ID="HTMLBreak" runat="server" />
						<cg:Text Field="City" runat="server" />,
						<cg:Text Field="State" runat="server" />
						<cg:Text Field="Zip Code" runat="server" /></p>
				</div>
				<div style="float:left;margin-left:40px;">
					<p><asp:HyperLink ID="lnkDirections" runat="server">Get Directions</asp:HyperLink></p>
				</div>
				<div style="clear:both;"></div>
			</div>

			<h4>Physicians Located Here:</h4>

			<div class="phys-table">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="hdr left-cell blue">Name</td>
						<td class="hdr left-cell border blue">Hours</td>
						<td class="hdr left-cell border blue">Contact</td>
					</tr>
					<asp:Repeater runat="server" ID="rptPhysicians">
						<ItemTemplate>
							<tr>
								<td class="left-cell addHeight">
									<strong>Dr.
									<asp:Label ID="PhysFirstName" runat="server" />
									<asp:Label ID="PhysLastName" runat="server" /></strong><br />
									<asp:Label ID="PhysicianSuite" runat="server" />
									<asp:Label ID="PhysSpec" runat="server" />
									<br style="margin-bottom: 8px;" />
									<asp:HyperLink ID="PhysBio" runat="server">View full bio</asp:HyperLink>
								</td>
								<td class="left-cell border addHeight">
									<asp:Literal ID="PhysHours" runat="server" />
								</td>
								<td class="left-cell border addHeight">
									<asp:Literal ID="PhysPhone" runat="server" /><br />
									<asp:Literal ID="PhysFax" runat="server" />
								</td>
							</tr>
						</ItemTemplate>
						<AlternatingItemTemplate>
							<tr class="alt">
								<td class="left-cell addHeight">
									<strong>Dr.
									<asp:Label ID="PhysFirstName" runat="server" />
									<asp:Label ID="PhysLastName" runat="server" /></strong><br />
									<asp:Label ID="PhysicianSuite" runat="server" />
									<asp:Label ID="PhysSpec" runat="server" />
									<br style="margin-bottom: 8px;" />
									<asp:HyperLink ID="PhysBio" runat="server">View full bio</asp:HyperLink>
								</td>
								<td class="left-cell border addHeight">
									<asp:Literal ID="PhysHours" runat="server" />
								</td>
								<td class="left-cell border addHeight">
									<asp:Literal ID="PhysPhone" runat="server" /><br />
									<asp:Literal ID="PhysFax" runat="server" />
								</td>
							</tr>
						</AlternatingItemTemplate>
					</asp:Repeater>
				</table>
			</div>

			<p><asp:Label ID="lblMoreInfo" runat="server"><strong>More Info:</strong></asp:Label></p>
			<p><cg:Text Field="Body" runat="server" ID="BodyText" /></p>
			<p><br /><a href="javascript:history.go(-1);">&lt; Go Back</a></p>
		</div>
	</div>
</asp:Content>
