﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="false" CodeBehind="CareersLayout.aspx.cs" Inherits="CMGI.Layouts.CareersLayout" MasterPageFile="Base.Master" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="/CMGI/Sublayouts/LeftNavigation.ascx" %>

<asp:content runat="server" id="Content1" contentplaceholderid="head">
<style type="text/css">
.titleCell
{
	width: 50%;
}

.emptyCareers
{
	display: block;
	padding: 50px 0;
}
</style>

</asp:content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainBodyContent" runat="server">
	<div class="container">
		<div class="noSideMenu">
			<div class="title">
				<h1><cg:Text Field="Title" runat="server" /></h1>
			</div>

			<div><cg:Text Field="Body" runat="server" /></div>

			<div class="phys-table">
				<asp:Repeater ID="rptCareers" runat="server">
					<HeaderTemplate>
						<table cellspacing="0" cellpadding="0" border="0" style="width:100%;">
						<tr>
							<td class="hdr left-cell">Job Title</td>
							<td class="hdr left-cell border">Location</td>
							<td class="hdr left-cell border">Category</td></tr>
					</HeaderTemplate>
					
					<ItemTemplate>
						<tr>
							<td class="left-cell titleCell"><asp:HyperLink ID="career" NavigateUrl="<%# ItemURL(Container.DataItem) %>" runat="server">
								<sc:Text ID="Title" item="<%# Container.DataItem %>" Field="Job Title" runat="server" /></asp:HyperLink></td>
							<td class="left-cell border stateCell"><sc:Text ID="City" Item="<%# Container.DataItem %>" Field="Location City" runat="server" />, 
								<sc:Text ID="State" Item="<%# Container.DataItem %>" Field="Location State" runat="server" /></td>
							<td class="left-cell border categoryCell"><sc:Text ID="Category" Item="<%# Container.DataItem %>" Field="Category" runat="server" /></td>
						</tr>
					</ItemTemplate>

					<AlternatingItemTemplate>
						<tr class="alt">
							<td class="left-cell titleCell"><asp:HyperLink ID="career2" NavigateUrl="<%# ItemURL(Container.DataItem) %>" runat="server">
								<sc:Text ID="Title2" item="<%# Container.DataItem %>" Field="Job Title" runat="server" /></asp:HyperLink></td>
							<td class="left-cell border stateCell"><sc:Text ID="City2" Item="<%# Container.DataItem %>" Field="Location City" runat="server" />, 
								<sc:Text ID="State2" Item="<%# Container.DataItem %>" Field="Location State" runat="server" /></td>
							<td class="left-cell border categoryCell"><sc:Text ID="Category2" Item="<%# Container.DataItem %>" Field="Category" runat="server" /></td>
						</tr>
					</AlternatingItemTemplate>
					
					<FooterTemplate>
						</table>
					</FooterTemplate>
					
				</asp:Repeater>

				<asp:Label ID="lblEmptyCareer" Text="There are no current openings at this time. Please check back later." runat="server" Visible="false" CssClass="emptyCareers"></asp:Label>
			</div>
		</div>

		<div class="clear_both"></div>
	</div>
</asp:Content>
