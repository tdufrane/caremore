﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Links;
using CMGI.BL;

namespace CMGI.Layouts
{
	public partial class EventsLayout : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			txtSearch.Attributes.Add("alt", "Zip Code");

			if (IsPostBack)
			{
				ClientScript.RegisterStartupScript(this.GetType(), "hash", "location.hash = '#currevents';", true);
			}
		}

		public CMGI.BL.ListItem.EventItem[] GetAllEvents(bool IgnoreSearch = false)
		{
			// Do not grab events that have already passed
			List<CMGI.BL.ListItem.EventItem> eventlist = new List<CMGI.BL.ListItem.EventItem>();

			Item currItem = Sitecore.Context.Item;

			var children = currItem.Axes.GetDescendants();
			var news = from child in children
			           where child.TemplateName == "Event" &&
			                 ((DateField)child.Fields["Date"]).DateTime.Date >= DateTime.Now.Date
			           select child;

			news = news.OrderBy(i => ((DateField)i.Fields["Date"]).DateTime);

			if (!IgnoreSearch && (txtSearch.Text != "" && txtSearch.Text != txtSearch.Attributes["alt"]))
			{

				if (!(new System.Text.RegularExpressions.Regex(@"^(\d{5})$").IsMatch(txtSearch.Text)))
				{
					lblNoResults.Text = "Error: Zip code \"" + txtSearch.Text + "\" was in an unrecognized format.";
					lblNoResults.Visible = true;
					return null;
				}

				lblSort.Text = "Showing events in proximity to " + txtSearch.Text;
				lblSort.Visible = true;
				lnkShowAll.Visible = true;

				var loc = Geocoding.CallGeoWS(txtSearch.Text + ", USA");
				if (loc.Results.Count() > 0)
				{
					double lng = loc.Results[0].Geometry.Location.Lng;
					double lat = loc.Results[0].Geometry.Location.Lat;

					ProxComparer<CMGI.BL.ListItem.EventItem> prox = new CMGI.BL.ProxComparer<CMGI.BL.ListItem.EventItem>(lat, lng);

					foreach (Item ite in news)
					{
						CMGI.BL.ListItem.EventItem evite = new BL.ListItem.EventItem(ite, lat, lng);
						eventlist.Add(evite);
					}

					eventlist = eventlist.OrderBy(i => i, prox).ToList();
				}
				else
				{
					throw new Exception("Error: Status - " + loc.Status);
				}
			}
			else
			{
				foreach (Item ite in news)
				{
					CMGI.BL.ListItem.EventItem evite = new BL.ListItem.EventItem(ite, double.NegativeInfinity, double.NegativeInfinity);
					eventlist.Add(evite);
				}
			}

			lblNoResults.Visible = false;
			if (eventlist.Count() > 0)
				return eventlist.ToArray();
			else
				lblNoResults.Visible = true;

			return new BL.ListItem.EventItem[] { };
		}

		public CMGI.BL.ListItem.EventItem[] GetEventsFor(DateTime date, bool IgnoreSearch = false)
		{
			Item currItem = Sitecore.Context.Item;

			var children = currItem.Axes.GetDescendants();
			var news = from child in children
			           where child.TemplateName == "Event" &&
			                 ((DateField)child.Fields["Date"]).DateTime.Date == date.Date
			           select child;

			List<CMGI.BL.ListItem.EventItem> eventsfor = new List<BL.ListItem.EventItem>();

			var results = news.OrderBy(i => ((DateField)i.Fields["Date"]).DateTime);

			if (!IgnoreSearch && (txtSearch.Text != "" && txtSearch.Text != txtSearch.Attributes["alt"]))
			{
				lblSort.Text = string.Format(lblSort.Text, txtSearch.Text);
				lblSort.Visible = true;
				lnkShowAll.Visible = true;

				var loc = Geocoding.CallGeoWS(txtSearch.Text + ", USA");
				if (loc.Results.Count() > 0)
				{
					double lng = loc.Results[0].Geometry.Location.Lng;
					double lat = loc.Results[0].Geometry.Location.Lat;

					ProxComparer<CMGI.BL.ListItem.EventItem> prox = new CMGI.BL.ProxComparer<CMGI.BL.ListItem.EventItem>(lat, lng);

					foreach (Item ite in news)
					{
						CMGI.BL.ListItem.EventItem evite = new BL.ListItem.EventItem(ite, lat, lng);
						eventsfor.Add(evite);
					}

					eventsfor = eventsfor.OrderBy(i => i, prox).ToList();
				}
				else
				{
					throw new Exception("Error: Status - " + loc.Status);
				}
			}
			else
			{
				foreach (Item ite in news)
				{
					CMGI.BL.ListItem.EventItem evite = new BL.ListItem.EventItem(ite, double.NegativeInfinity, double.NegativeInfinity);
					eventsfor.Add(evite);
				}
			}

			lblNoResults.Visible = false;
			if (eventsfor.Count() > 0)
				return eventsfor.ToArray();
			else
				lblNoResults.Visible = true;

			return new BL.ListItem.EventItem[] { };
		}

		protected void rptEvents_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				CMGI.BL.ListItem.EventItem i = e.Item.DataItem as CMGI.BL.ListItem.EventItem;

				if (i != null)
				{
					Label date = e.Item.FindControl("eventDate") as Label;
					Label title = e.Item.FindControl("eventName") as Label;
					Label desc = e.Item.FindControl("eventDesc") as Label;

					HyperLink lnk = e.Item.FindControl("lnkEvent") as HyperLink;

					date.Text = i.Date.ToShortDateString();
					title.Text = i.Name;
					desc.Text = i.ShortDesc;
					lnk.NavigateUrl = LinkManager.GetItemUrl(i.Item);

					Label prox = e.Item.FindControl("proximity") as Label;

					if (i.Distance != double.NegativeInfinity && i.Distance.HasValue)
					{
						prox.Text = "Proximity: " + Math.Round(Geocoding.ToMiles(i.Distance.Value), 1) + " mi";
						prox.Visible = true;
					}
					else
					{
						prox.Visible = false;
					}
				}
			}
		}

		protected void calendar_DayRender(object sender, DayRenderEventArgs e)
		{
			var ets = GetAllEvents();
			if (ets != null)
			{
				foreach (var i in GetAllEvents())
				{
					DateTime a = i.Date.Date;
					DateTime b = e.Day.Date;

					if ((i.Date.Date == e.Day.Date))
					{
						e.Cell.Font.Bold = true;
						e.Cell.ForeColor = System.Drawing.Color.FromArgb(49, 130, 198);
					}
				}
			}
		}

		protected void rptEvents_PreRender(object sender, EventArgs e)
		{
			if (calEvents.SelectedDate == DateTime.MinValue)
				rptEvents.DataSource = GetAllEvents();
			else
				rptEvents.DataSource = GetEventsFor(calEvents.SelectedDate);

			rptEvents.DataBind();
		}

		protected void lnkClear_Click(object sender, EventArgs e)
		{
			calEvents.SelectedDate = DateTime.MinValue;
		}

		protected void lnkClear2_Click(object sender, EventArgs e)
		{
			txtSearch.Text = txtSearch.Attributes["alt"];
			lblSort.Visible = false;
			lnkShowAll.Visible = false;
			calEvents.SelectedDate = DateTime.MinValue;
		}
	}
}
