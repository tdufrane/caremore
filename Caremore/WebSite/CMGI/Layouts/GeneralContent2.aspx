﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeneralContent2.aspx.cs" Inherits="CMGI.Layouts.GeneralContent2" MasterPageFile="Base.Master" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics" %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="/CMGI/Sublayouts/LeftNavigation.ascx" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainBodyContent" runat="server">
	<div class="container">
		<!-- begin left side navigation -->
		<cg:leftnavigation runat="server" id="cmgiLeftNavigation"></cg:leftnavigation>
		<!-- end left side navigation -->

		<div class="copy">
			<div class="title">
				<h1>
					<sc:Text runat="server" ID="scTitleText" Field="Title" />
				</h1>
			</div>

			<span class="v1">
				<cg:Image runat="server" ID="imgMainImage" Field="Image">
				</cg:Image>
			</span>

			<div class="message">
				<sc:Text runat="server" ID="scBodyText" Field="Body" />
			</div>

			<p class="link">
				<strong><cg:Link Field="CareMore Link" ID="lnkCareMore" runat="server"/></strong>
			</p>
		</div>
		<div class="clear_both"></div>
	</div>
</asp:Content>
