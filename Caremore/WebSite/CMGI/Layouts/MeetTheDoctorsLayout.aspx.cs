﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Links;
using CMGI.BL;
using CareMore.Web.DotCom;

namespace CMGI.Layouts
{
	public partial class MeetTheDoctorsLayout : System.Web.UI.Page
	{
		private ID _meetTheDocsID = ItemIds.GetID("CMGI_MEET_DOCTORS");

		#region Properties

		public Item FeatureVideoItem
		{
			get
			{
				// video ID
				ID videoID;
				if (string.IsNullOrEmpty(Request.QueryString["vid"]))
				{
					// return 1st doc as default
					return Sitecore.Context.Database.GetItem(_meetTheDocsID).Children[0];
				}
				else
				{
					videoID = new ID(Request.QueryString["vid"]);
					return Sitecore.Context.Database.GetItem(videoID);
				}
			}
		}

		public string FeatureVideoImagePath
		{
			get
			{
				Sitecore.Data.Fields.ImageField videoThumb = FeatureVideoItem.Fields["Video Thumb"];

				if (videoThumb == null)
					return string.Empty;
				else
					return MediaManager.GetMediaUrl(videoThumb.MediaItem);
			}
		}

		public string FeatureVideoVideoPath
		{
			get
			{
				LinkField videoUrl = FeatureVideoItem.Fields["Video Url"];

				if (videoUrl != null)
					return videoUrl.Url;
				else
					return string.Empty;
			}
		}

		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			VideoPlayerDoctor.Path = FeatureVideoVideoPath;
			VideoPlayerDoctor.Still = FeatureVideoImagePath;

			BuildSlider();

			scFeatureVideoTitle.Item = FeatureVideoItem;
			scFeatureVideoShortInfo.Item = FeatureVideoItem;
		}

		private void BuildSlider()
		{
			sliderMultiLV.DataSource = Sitecore.Context.Database.GetItem(_meetTheDocsID).Children;
			sliderMultiLV.DataBind();
		}

		protected void sliderMultiLV_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				ListViewDataItem LVDI = (ListViewDataItem)e.Item;
				Item item = (Item)LVDI.DataItem;

				// Video thumb
				Sitecore.Web.UI.WebControls.Image scImage = (Sitecore.Web.UI.WebControls.Image)e.Item.FindControl("scImage");
				scImage.Item = item;

				// link
				HyperLink hl = (HyperLink)e.Item.FindControl("hl");
				hl.NavigateUrl = string.Format("{0}?vid={1}#{2}",
					LinkManager.GetItemUrl(Sitecore.Context.Item),
					item.ID,
					Request.Url.Fragment);

				// link text
				Sitecore.Web.UI.WebControls.Text scText = (Sitecore.Web.UI.WebControls.Text)hl.FindControl("scText");
				scText.Item = item;

				// break on each 3rd
				if ((e.Item.DataItemIndex + 1) % 3 == 0)
				{
					Literal litBreak = (Literal)e.Item.FindControl("litBreak");
					litBreak.Visible = true;
				}
			}
		}
	}
}