﻿using System;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;

namespace CMGI.Layouts
{
	public partial class EventItem : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Item currItem = Sitecore.Context.Item;

			if ((string.IsNullOrWhiteSpace(currItem.Fields["Address 1"].Value)) ||
				(string.IsNullOrWhiteSpace(currItem.Fields["City"].Value)))
			{
				this.pnlAddress.Visible = false;
			}
			else if (string.IsNullOrWhiteSpace(currItem.Fields["Address 2"].Value))
			{
				this.plcAddress2.Visible = false;
			}
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
			Item currItem = Sitecore.Context.Item;
			Master.SetPageTitle(currItem["Name"]);
		}
	}
}
