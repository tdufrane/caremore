﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;

namespace CMGI.Layouts
{
	public partial class GeneralContent1 : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Sitecore.Data.Fields.ImageField imageField = Sitecore.Context.Item.Fields["Image"];

			if (imageField != null && imageField.MediaItem != null)
			{
				string backgroundImageUrl = MediaManager.GetMediaUrl(imageField.MediaItem);
				divHeroCssClass.Style.Add("background-image", backgroundImageUrl);
			}

			string overlayText = Sitecore.Context.Item.Fields["Image Overlay Text"].Value;
			if (string.IsNullOrWhiteSpace(overlayText))
				divHeroCopyCssClass.Visible = false;
		}
	}
}
