﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using CMGI.BL;

namespace CMGI.Layouts
{
    public partial class TestimonialHome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Item firstTestimonial = Sitecore.Context.Database.SelectItems(Utility.TESTIMONIALS_PATH + "//*[@@templatename='Testimonial']").Where(i => i["Type"] == "Patient").FirstOrDefault();
            string url = Sitecore.Links.LinkManager.GetItemUrl(firstTestimonial);

            Response.Redirect(url);
        }

  
    }
}