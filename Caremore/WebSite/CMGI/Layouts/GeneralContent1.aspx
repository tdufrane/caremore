﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeneralContent1.aspx.cs" Inherits="CMGI.Layouts.GeneralContent1" MasterPageFile="Base.Master" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics" %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="/CMGI/Sublayouts/LeftNavigation.ascx" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainBodyContent" runat="server">
	<div class="container">
		<!-- begin left side navigation -->
		<cg:leftnavigation runat="server" id="cmgiLeftNavigation"></cg:leftnavigation>
		<!-- end left side navigation -->

		<div class="copy">
			<div class="title">
				<h1><cg:Text runat="server" id="scTitleText" field="Title" /></h1>
			</div>

			<div runat="server" id="divHeroCssClass" class="hero">
				<div id="divHeroCopyCssClass" runat="server" class="hero-copy">
					<cg:Text runat="server" id="scImageOverlayText" field="Image Overlay Text" />
				</div>
			</div>

			<div class="message">
				<cg:Text runat="server" field="Body" />
			</div>
		</div>
		<div class="clear_both"></div>
	</div>
</asp:Content>
