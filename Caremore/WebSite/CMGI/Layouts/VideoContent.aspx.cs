﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;

namespace CMGI.Layouts
{
	public partial class VideoContent : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Item currentItem = Sitecore.Context.Item;

			Sitecore.Data.Fields.ImageField videoThumb = currentItem.Fields["Video Thumb"];
			VideoPlayerItem.Still = MediaManager.GetMediaUrl(videoThumb.MediaItem);

			LinkField videoUrl = currentItem.Fields["Video Url"];
			VideoPlayerItem.Path = videoUrl.Url;

			VideoPlayerItem.Title = currentItem["Title"];
		}
	}
}