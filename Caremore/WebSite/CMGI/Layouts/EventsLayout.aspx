﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="false" CodeBehind="EventsLayout.aspx.cs" Inherits="CMGI.Layouts.EventsLayout" MasterPageFile="Base.Master" %>

<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="/CMGI/Sublayouts/LeftNavigation.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainBodyContent" runat="server">
	<div class="container">
		<!-- begin left side navigation -->
		<cg:LeftNavigation runat="server" />
		<!-- end left side navigation -->

		<div class="copy">
			<div class="title">
				<h1><cg:Text Field="Title" runat="server" /></h1>
			</div>

			<cg:Text Field="Description" runat="server" />

			<asp:Panel ID="pnlEvents" runat="server" CssClass="current-events" DefaultButton="btnSearchEvents">
				<p class="message"><strong>Sort by:</strong> <asp:TextBox ID="txtSearch" runat="server" CssClass="search defaultValue" /> <asp:Button id="btnSearchEvents" runat="server" CssClass="lgButton" Text="Search" /></p>

				<asp:Label ID="lblSort" runat="server" Visible="false"><i style="font-size: 14px;">Sorting by proximity to {0}.</i></asp:Label>
				&nbsp; &nbsp;
				<asp:LinkButton ID="lnkShowAll" Text="(clear)" runat="server" onclick="lnkClear2_Click" Visible="false" />

				<br />

				<img src="/cmgi/images/current-border.jpg" alt=" " />

				<asp:Label ID="lblNoResults" runat="server" Visible="false"><div style="vertical-align:top; padding-bottom: 10px;"><strong><i>No results were found.</i></strong></div></asp:Label>

				<asp:Repeater ID="rptEvents" runat="server" 
					onitemdatabound="rptEvents_ItemDataBound" 
					onprerender="rptEvents_PreRender">
					<ItemTemplate>
						<div class="date"><p><asp:Label ID="eventDate" runat="server" /></p></div>
						<div class="event-description">
							<p><strong><asp:Label ID="eventName" runat="server" /></strong><br />
							<asp:Label ID="eventDesc" runat="server" /><br />
							<asp:Label runat="server" class="gray" style="font-style:italic;" ID="proximity" Visible="false">Proximity: </asp:Label><br />
							<asp:Hyperlink ID="lnkEvent" runat="server">Event Details &gt;</asp:Hyperlink></p>
						</div>
						<div class="clear_both"></div>
					</ItemTemplate>

					<SeparatorTemplate>
						<img src="/cmgi/images/current-border.jpg" alt=" " />
					</SeparatorTemplate>

				</asp:Repeater>
			</asp:Panel>

			<div class="calendar">
				<asp:Calendar id="calEvents" runat="server"
					BorderStyle="Solid" CellPadding="3" ShowTitle="true" TitleStyle-BackColor="white"
					BorderColor="#D6E7F7" BackColor="White" CssClass="calendarWidget" 
					TitleFormat="Month" NextPrevStyle-CssClass="calNextPrev"
					SelectedDayStyle-CssClass="calSelDay" SelectedDayStyle-BackColor="#D6E7F7"
					NextPrevStyle-ForeColor="#AAD4FF" TitleStyle-CssClass="calTitle" DayStyle-CssClass="dayStyle" 
					OnDayRender="calendar_DayRender" />
					<span class="smalllink"><asp:LinkButton ID="lnkClear" Text="Show All" runat="server"  onclick="lnkClear_Click" /></span>
			</div>

			<div class="clear_both"></div>
		</div>

		<div class="clear_both"></div>
	</div>
</asp:Content>
