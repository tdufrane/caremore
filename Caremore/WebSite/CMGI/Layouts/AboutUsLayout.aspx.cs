﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ImageField = Sitecore.Data.Fields.ImageField;
using Sitecore.Resources.Media;

namespace CMGI.Layouts
{
    public partial class AboutUsLayout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string backgroundImageUrl; 
            ImageField imageField = Sitecore.Context.Item.Fields["Image"];

            if (imageField != null && imageField.MediaItem != null)
            {
                backgroundImageUrl = MediaManager.GetMediaUrl(imageField.MediaItem);
                divHeroCssClass.Style.Add("background-image", backgroundImageUrl);
            }

        }

    }
}