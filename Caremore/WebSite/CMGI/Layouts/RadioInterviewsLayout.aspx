﻿<%@ Page Language="C#" MasterPageFile="Base.Master" AutoEventWireup="true" CodeBehind="RadioInterviewsLayout.aspx.cs" Inherits="CMGI.Layouts.RadioInterviewsLayout" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cg" Namespace="CMGI.BL.WebControls" Assembly="CMGI" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Analytics"  %>
<%@ Register TagPrefix="cg" TagName="LeftNavigation" Src="../Sublayouts/LeftNavigation.ascx" %>
<%@ Register Src="../Sublayouts/AudioPlayer.ascx" TagPrefix="apc" TagName="AudioPlayer" %>

<asp:Content runat="server" id="HeaderContent" contentplaceholderid="head">
	<link href="/player/jplayer.blue.monday.css" rel="stylesheet" type="text/css" />
	<script src="/js/jquery.jplayer.min.js" type="text/javascript"></script>
</asp:Content>

<asp:Content runat="server" id="BodyContent" contentplaceholderid="MainBodyContent">
	<div class="container">
		<!-- begin left side navigation -->
		<cg:LeftNavigation runat="server" />
		<!-- end left side navigation -->

		<div class="copy">
			<div class="title">
				<h1><cg:Text Field="Title" runat="server" /></h1>
			</div>

			<cg:Text Field="Body" runat="server" />

			<asp:Repeater ID="rptAudioItems" runat="server"
				onitemdatabound="rptAudioItems_ItemDataBound" 
				onprerender="rptAudioItems_PreRender">
				<ItemTemplate>
					<hr />
					<table style="width: 700px; height: 80px;">
						<tbody>
							<tr>
								<td id="tdImage" runat="server" style="width: 80px;"><img id="imgDrPicture" runat="server" alt="" width="78" height="78" src="~/media/{0}.ashx" /></td>
								<td style="width: 400px;padding-right: 15px;"><asp:Label ID="lblTitle" runat="server" /></td>
								<td>
									<apc:AudioPlayer id="apcAudioPlayerItem" runat="server" />
								</td>
							</tr>
						</tbody>
					</table>
				</ItemTemplate>
				<FooterTemplate>
					<hr />
				</FooterTemplate>
			</asp:Repeater>

			<div style="padding-bottom: 10px;"><strong><i><asp:Label ID="lblNoResults" runat="server" Text="No audio at this time." Visible="false" /></i></strong></div>
		</div>

		<div class="clear_both"></div>
	</div>
</asp:Content>
