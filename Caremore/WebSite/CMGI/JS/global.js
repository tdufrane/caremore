﻿$j = $.noConflict();

$j(document).ready(function () {
    $j('#slider').anythingSlider({autoPlay:true,delay:5000});
    adjustCalloutHeight();

    $j('.defaultValue').each(function (index, value) {
        $j(this).defaultvalue($j(this).attr('alt'));
    });
});

$j(document).ready(function () {
	var domain = document.domain;
	$j(document).on('click', 'a[href^="http"]:not([href*="' + domain + '"])[href^="http"]:not([href*=".caremore.com"])', function () {
		return confirm("You are now leaving CareMore Medical Group to enter a third party site.");
	});
});


function adjustCalloutHeight() {
    var boxHeights = new Array();

    // Gather the heights of the shopping boxes
    $j('.coltitle').each(function () {
        $j(this).attr('style', 'height:auto;');
        boxHeights.push($j(this).height());
    });

    function sortArray(valA, valB) {
        return (valB - valA);
    }
    // Sort the heights in ascending order
    boxHeights.sort(sortArray);

    // Set the height of all three shopping boxes to the value of the tallest box
    $j('.coltitle').each(function () {
        $j(this).height(boxHeights[0]);
    });
}

var players = new Array();

function playVideo(controlId, imagePath, videoPath, width, height) {
    var curPlayer = jwplayer(controlId).setup({
        'flashplayer': '/flash/player5.swf',
        'image': imagePath,
        'file': videoPath,
        'controlbar': 'over',
        'controlbar.idlehide': 'true',
        'fullscreen': 'true',
        'stretching': 'fill',
        'skin': '/flash/skewd.zip',
        'width': width,
        'height': height,
        events: {
            onPlay: function (event) {
                //stop other videos to play
                for (var i = 0; i < players.length; i++) {
                    var curPlayer = players[i];

                    if (curPlayer != this && curPlayer.getState() == "PLAYING") {
                        curPlayer.pause();
                    }
                }
            }
        }
    });

    players.push(curPlayer);
}

//*******************************************Google Map in CMGI Related Logic *START********************************************

// Google Map on Region Google Map with services
var geocoder;
var infoWindow;
var firstLocation;
var map;
var zipcode;
var pageNumber;

function initialize() {

    infoWindow = new google.maps.InfoWindow();
    var myOptions = {
        center: new google.maps.LatLng(34, -118),
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map"),
            myOptions);

    GeoCodeAddress();
}

zipcode = getUrlVars()["zip"];
if (!zipcode) { zipcode = ""; }

function GeoCodeAddress() {
    CallLocationService('/CMGI/Services/LocationCoordinates.asmx/GetCoordinates', "{ 'zipcode': '" + zipcode + "' }", SetMarker, Failed, Complete);
}

function CallLocationService(url, zipcode, successFn, errorFn, complete) {
    $j.ajax({
        url: url,
        type: "POST",
        data: zipcode,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: successFn,
        error: errorFn,
        complete: complete
    });
}

function SetMarker(result) {
    for (var i = 0; i < result.d.length; i++) {
        for (var j = 0; j < result.d[i].LocationType.length; j++) {
            SetMarkerResult(result.d[i], result.d.length, result.d[i].LocationType);
            var myLatLng = new google.maps.LatLng(result.d[0], result.d[3]);

//            var bounds = new google.maps.LatLngBounds();
//            bounds.extend(myLatLng);
//            map.fitBounds(bounds);
        }
    }
}

function SetMarkerResult(currentItem, totalItem, locationType) {

    var imageUrl;
    var viewDetailsCss;
    var locType = locationType;
    var div = document.createElement('DIV');

    if (locType == 'CMG') {
        viewDetailsCss = ""
        div.innerHTML = '<div class="bluedot"><span class="indot">' + currentItem.markerNumber + '</span></div>';
    }
    if (locType == 'Flu Shots') {
        viewDetailsCss = "style = 'display:none;'";
        div.innerHTML = '<div class="greendot"><span class="indot">' + currentItem + '</span></div>';
    }
    if (locType == 'Urgent Care Centers') {
        viewDetailsCss = "style = 'display:none;'";
        div.innerHTML = '<div class="pinkdot"><span class="indot">' + currentItem.markerNumber + '</span></div>';
    }

    var marker = new RichMarker({
        map: map,
        position: new google.maps.LatLng(currentItem.Latitude, currentItem.Longitude),
        flat: true,
        content: div
    });

    var ClickedMarkerGuid = currentItem.GUID;
    var totItem = totalItem;
    var directionsUrl = "https://maps.google.com/maps?q=" + currentItem.Address1 + " " + currentItem.Address2 + " " + currentItem.City + " " + currentItem.State + " " + currentItem.Zip + "&t=m&z=16";

    marker.setMap(map);
    google.maps.event.addListener(marker, 'click', function () {
        infoWindow.close();
        infoWindow.setContent("<div style='font-size: 12px;'><div><strong>" + locType + "</strong></div></br><div>" + currentItem.Address1 + "</div><div>" + currentItem.Address2 + "</div><div>" + currentItem.City + ", " + currentItem.State + "-" + currentItem.Zip + "</div></br><div><strong>p: </strong>" + currentItem.Phone + "</div><div><strong>f: &nbsp;</strong>" + currentItem.Fax + "</div></br><div><a class='directions' target='_blank' href='" + directionsUrl + "'>Get Directions</a></div><div class='directions'><a href='" + currentItem.ViewDetailsUrl + "'" + viewDetailsCss + "> View Details</a></div>");
        infoWindow.open(map, marker);

        for (var i = 0; i < totItem; i++) {
            $j("table[data-guid!= '" + ClickedMarkerGuid + "' ]").removeClass('clicked-marker')
        }
        $j("table[data-guid= '" + ClickedMarkerGuid + "' ]").addClass('clicked-marker')

    });

    
}

function Failed(result) {
    alert("Something is wrong");
}

function Complete(result) {
    
}


function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
//*******************************************Google Map in CMGI Related Logi *END********************************************

