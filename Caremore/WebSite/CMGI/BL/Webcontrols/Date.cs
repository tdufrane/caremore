﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CareMore.Web.DotCom;

namespace CMGI.BL.WebControls
{
    public class Date : Sitecore.Web.UI.WebControls.Date
    {
        protected override void OnLoad(EventArgs e)
        {

            Sitecore.Data.Items.Item WordingDotDateFormatText = ItemIds.GetItem("DOT_DATE_FORMAT_TEXT");

            if (WordingDotDateFormatText != null && !string.IsNullOrEmpty(WordingDotDateFormatText["Text"]))
                Format = WordingDotDateFormatText["Text"];
        }

        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);

            if (Item == null)
            {
                Item = WebControlUtil.GetItemFromDataBind(NamingContainer);
            }
        }
    }
}