﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Sitecore.Data.Items;
using System.Web.UI.WebControls;
using CMGI.BL.ListItem;

namespace CMGI.BL.WebControls
{
    public class WebControlUtil
    {
        public static Item GetItemFromDataBind(Control namingContainer)
        {
            RepeaterItem rpItem = namingContainer as RepeaterItem;

            if (rpItem == null)
                return null;

            if (rpItem.DataItem is Item)
                return rpItem.DataItem as Item;

            StandardItem navItem = rpItem.DataItem as StandardItem;

            if (navItem == null)
                return null;

            return navItem.Item;
        }
    }
}