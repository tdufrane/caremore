﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.SecurityModel;
using Sitecore.Data.Fields;
using CareMore.Web.DotCom;

namespace CMGI.BL.WebControls
{
    public class Link : Sitecore.Web.UI.WebControls.Link
    {
        public string ItemReferenceName { get; set; }
        public string DisplayText { get; set; }
        public bool AddSpanTag { get; set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!string.IsNullOrEmpty(ItemReferenceName))
                Item = ItemIds.GetItem(ItemReferenceName);
        }

        protected override void DoRender(System.Web.UI.HtmlTextWriter output)
        {
            if (Item != null && !string.IsNullOrEmpty(DisplayText))
            {
                Text = Item[DisplayText];
            }

            base.DoRender(output);
        }

        protected override void OnDataBinding(System.EventArgs e)
        {
            base.OnDataBinding(e);

            if (Item == null)
            {
                Item = WebControlUtil.GetItemFromDataBind(NamingContainer);
            }

            if (Item != null && !string.IsNullOrEmpty(DisplayText))
            {
                Text = Item[DisplayText];
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (Item == null)
            {
                Item = Sitecore.Context.Item;
            }

            if (AddSpanTag)
            {
                if (Text == null)
                {
                    LinkField linkField = Item.Fields[Field];

                    if (linkField != null)
                        Text = linkField.Text;
                }

                Text = string.Format("<span>{0}</span>", Text);
            }
        }
    }
}