﻿using System;
using System.Text;
using System.Web;
using Sitecore.Data.Items;

namespace CMGI.BL.ListItem
{
	public class AddressItem : StandardItem
	{
		public string Address1;
		public string Address2;
		public string City;
		public string State;
		public string Zip;
		public string Hours;
		public string Phone;
		public string Fax;
		public string PhysicianSuite;

		public AddressItem(Item e)
			: base(e)
		{
			this.Address1 = e["Address 1"];
			this.Address2 = e["Address 2"];
			this.City = e["City"];
			this.State = e["State"];
			this.Zip = e["Zip Code"];
			this.Hours = e["Hours"];
			this.PhysicianSuite = e["PhysicianSuite"];

			if (string.IsNullOrEmpty(e.Fields["MultipleSuiteLocation"].Value) && string.IsNullOrEmpty(PhysicianSuite))
				this.PhysicianSuite = string.Empty;
		}

		public AddressItem(Item e, string hours, string phone, string fax)
			: this(e)
		{
			this.Hours = hours;
			this.Phone = phone;
			this.Fax = fax;
		}

		public AddressItem(Item e, string hours, string phone, string fax, string physiciansuite)
			: this(e)
		{
			this.Address1 = e["Address 1"];
			this.Address2 = e["Address 2"];
			this.City = e["City"];
			this.State = e["State"];
			this.Zip = e["Zip Code"];
			this.Hours = hours;
			this.Phone = phone;
			this.Fax = fax;
			this.PhysicianSuite = physiciansuite;

			if (string.IsNullOrEmpty(e.Fields["MultipleSuiteLocation"].Value))
				this.PhysicianSuite = string.Empty;
		}

		public string Address()
		{
			return Address(true);
		}

		public string Address(bool breakSuite)
		{
			if (breakSuite)
			{
				return string.Format("{0}{1}{2} {3}, {4} {5}",
					Address1 + "<br/>\n",
					string.IsNullOrEmpty(Address2) ? string.Empty : Address2 + "<br/>\n",
					string.IsNullOrEmpty(PhysicianSuite) ? string.Empty : PhysicianSuite + "<br/>\n",
					City, State, Zip);
			}
			else
			{
				StringBuilder address = new StringBuilder();

				address.Append(Address1);

				if (!string.IsNullOrEmpty(Address2))
				{
					address.Append("<br/>\n");
					address.Append(Address2);
				}

				if (!string.IsNullOrEmpty(PhysicianSuite))
				{
					address.Append("\n");
					address.Append(PhysicianSuite);
				}

				address.Append("<br/>\n");

				address.AppendFormat("{0}, {1} {2}", City, State, Zip);

				return address.ToString();
			}
		}

		public string Numbers()
		{
			return string.Format("{0}{1}{2}",
				string.IsNullOrEmpty(Phone) ? string.Empty : string.Format("Phone: {0}", Phone),
				string.IsNullOrEmpty(Phone) || string.IsNullOrEmpty(Fax) ? string.Empty : "<br />",
				string.IsNullOrEmpty(Fax) ? string.Empty : string.Format("Fax: {0}", Fax));
		}

		public string HTMLAddress()
		{
			return HTMLAddress(true);
		}

		public string HTMLAddress(bool breakSuite)
		{
			return Address(breakSuite) + "<br/>" + Numbers();
		}
	}
}
