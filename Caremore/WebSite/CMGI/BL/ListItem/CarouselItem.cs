﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Form.Core;
using Sitecore.Data.Fields;
using System.Web.UI.WebControls;
using Sitecore.Resources.Media;

namespace CMGI.BL.ListItem
{
	public class CarouselItem
	{
		public string Title;
		public string Body;
		public string Background;
		public System.Web.UI.WebControls.HyperLink Link;
		public System.Web.UI.WebControls.HyperLink ImageLink;
		public bool FullWidthImage;
		public string Css;
		public bool Disabled;

		public CarouselItem(Item item)
		{
			if (item != null)
			{
				Title = item.Fields["Title"].GetValue(true);
				Body = item.Fields["Body"].GetValue(true);

				Sitecore.Data.Fields.ImageField img = item.Fields["Image"];
				if (img.MediaItem != null)
					Background = Sitecore.Resources.Media.MediaManager.GetMediaUrl(img.MediaItem);
				else
					Background = string.Empty;

				Link = GetLink(item.Fields["Link"]);

				ImageLink = GetLink(item.Fields["Link"]);
				ImageLink.ImageUrl = "~/" + Background;

				FullWidthImage = item.Fields["Full Width Image"].GetValue(true) == "1" ? true : false;
				Css = item.Fields["Css"].Value;
				Disabled = item.Fields["Disabled"].GetValue(true) == "1" ? true : false;
			}
		}

		private HyperLink GetLink(Sitecore.Data.Fields.LinkField LinkField)
		{
			HyperLink hyperlinkToFill = new HyperLink();

			Sitecore.Data.Database db = Sitecore.Context.Database;

			//set text and tooltip
			hyperlinkToFill.Text = LinkField.Text;
			hyperlinkToFill.ToolTip = LinkField.Title;
			hyperlinkToFill.Target = LinkField.Target;

			if (!(LinkField.Url == ""))
			{
				if (LinkField.LinkType.Equals("javascript"))
				{
					// javascript (do not fill out TargetItem, fill out Url, e,g. javascript:alert('hello').
					hyperlinkToFill.Attributes.Add("onclick", LinkField.Url + "; return false;");
				}
				else if (LinkField.LinkType.Equals("external"))
				{
					// external (do not fill out TargetItem)
					hyperlinkToFill.NavigateUrl = LinkField.Url;
				}
				else if (LinkField.LinkType.Equals("internal"))
				{
					// internal (fill out TargetItem = item in Sitecore tree)

					string strURL = LinkField.Url.Replace("/Home", string.Empty);

					if ((strURL == ".aspx"))
					{
						strURL = "/";
					}

					hyperlinkToFill.NavigateUrl = strURL;
					//sb.Append(" href=\"" + WiseBusiness.Utils.Util.ProcessLink(LinkField.Url + ".aspx") + "\"");
				}
				else if (LinkField.LinkType.Equals("media"))
				{
					/// - media (fill out TargetItem = item in media library).
					Item asset = db.Items["/sitecore/media library" + LinkField.Url];
					//this GUID refers to the File Path of the File template
					if (asset.Fields["File Path"].Value.Equals(""))
					{
						hyperlinkToFill.NavigateUrl = "/media/" + MediaManager.GetMediaUrl(asset);
					}
					else
					{
						hyperlinkToFill.NavigateUrl = asset.Fields["File Path"].Value;
					}
				}
				else if (LinkField.LinkType.Equals("mailto"))
				{
					/// - mailto (do not fill out TargetItem, fill out url).
					hyperlinkToFill.NavigateUrl = LinkField.Url;
				}
				else if (LinkField.LinkType.Equals("anchor"))
				{
					/// - anchor (do not fill out TargetItem, fill out Anchor + Url).
					hyperlinkToFill.NavigateUrl = LinkField.Url;
					if (LinkField.Anchor != "")
					{
						hyperlinkToFill.NavigateUrl += "#" + LinkField.Anchor;
					}
				}
			}
			return hyperlinkToFill;
		}
	}
}
