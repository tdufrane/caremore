﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;

namespace CMGI.BL.ListItem
{
	public class PhysicianItem : StandardItem
	{
		public string FirstName;
		public string LastName;
		public string Picture;

		public string Phone;
		public string Fax;
		public string[] Specialty;

		public AddressItem[] Addresses;

		public PhysicianItem(Item e)
			: this(e, null, null)
		{ }

		public PhysicianItem(Item e, Item[] addressItems, Item physicianLocation)
			: base(e)
		{
			ImageField picfield = e.Fields["Picture"];

			FirstName = e["First Name"];
			LastName = e["Last Name"];

			if (picfield.MediaItem != null)
				Picture = MediaManager.GetMediaUrl(picfield.MediaItem);
			else
				Picture = string.Empty;

			if (physicianLocation == null)
			{
				Phone = e["Phone"];
				Fax = e["Fax"];
			}
			else
			{
				if (string.IsNullOrEmpty(e["Phone"]))
					Phone = physicianLocation["Phone"];
				else
					Phone = e["Phone"];

				if (string.IsNullOrEmpty(e["Fax"]))
					Fax = physicianLocation["Fax"];
				else
					Fax = e["Fax"];
			}

			// Get addresses
			if (addressItems != null)
			{
				string physicianSuite;
				if (physicianLocation == null)
					physicianSuite = string.Empty;
				else
					physicianSuite = string.IsNullOrEmpty(physicianLocation["PhysicianSuite"]) ? string.Empty : physicianLocation["PhysicianSuite"];

				Addresses = addressItems
					.ToList().ConvertAll(item =>
						new AddressItem(item,
						item["Hours"], item["Phone"], item["Fax"],
						physicianSuite)).ToArray();
			}
			 
			List<string> specs = new List<string>();
			MultilistField spc = e.Fields["Specialty"];
			foreach (Item i in spc.GetItems())
			{
				specs.Add(i["Specialty"]);
			}

			Specialty = specs.ToArray();
			base.Item = e;
		}


		public PhysicianItem(Item e, AddressItem[] addressItems)
			: base(e)
		{
			ImageField picfield = e.Fields["Picture"];

			FirstName = e["First Name"];
			LastName = e["Last Name"];

			if (picfield.MediaItem != null)
				Picture = MediaManager.GetMediaUrl(picfield.MediaItem);
			else
				Picture = "";

			Phone = e["Phone"];
			Fax = e["Fax"];

			// Get addresses
			if (addressItems != null)
			{
				Addresses = addressItems;
			}
			 
			List<string> specs = new List<string>();
			MultilistField spc = e.Fields["Specialty"];
			foreach (Item i in spc.GetItems())
			{
				specs.Add(i["Specialty"]);
			}

			Specialty = specs.ToArray();
			base.Item = e;
		}

		public string Numbers()
		{
			return string.Format("{0}{1}{2}",
				string.IsNullOrEmpty(Phone) ? string.Empty : string.Format("Phone: {0}", Phone),
				string.IsNullOrEmpty(Phone) || string.IsNullOrEmpty(Fax) ? string.Empty : "<br />",
				string.IsNullOrEmpty(Fax) ? string.Empty : string.Format("Fax: {0}", Fax));
		}
	}


	public class PhysicianAtLocationItem : PhysicianItem
	{
		public string HoursAtLocation;
		public string PhoneAtLocation;
		public string FaxAtLocation;
		public string PhysicianSuite;

		public PhysicianAtLocationItem(Item item)
			: base(Sitecore.Context.Database.GetItem(item["Physician"]), null, null)
		{
			HoursAtLocation = item["Hours"];
			PhoneAtLocation = item["Phone"];
			FaxAtLocation = item["Fax"];
			PhysicianSuite = item["PhysicianSuite"];
		}
	}
}
