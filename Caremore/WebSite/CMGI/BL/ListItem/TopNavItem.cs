﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;

namespace CMGI.BL.ListItem
{
	public class TopNavItem : StandardItem
	{
		public string Width { get; set; }
		public string CssClass { get; set; }
		public Item TargetItem { get; set; }

		public TopNavItem(Item item) : base(item)
		{
			;
		}
	}
}
