﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Data;
using System.Xml;
using System.Collections;
using Sitecore.Configuration;
using Sitecore.Xml;
using System.Web.Hosting;
using Sitecore.Diagnostics;
using Sitecore.Links;

namespace CMGI.BL
{
    public class ItemIds
    {
        private static readonly string HASH_TABLE_PATH = "sitecoreIds/cmgi/id";
        private static object _settingsLock = new object();
        private static Hashtable _settings;

        public static ID GetID(string name)
        {
            
            Hashtable hash = GetHashtable();

            var itemId = hash[name] as ID;

            if (ID.IsNullOrEmpty(itemId))
            {
                Log.Fatal("Failed to get item id for: " + name, "ItemIds");
            }

            return itemId;
        }

        public static string GetString(string name)
        {
            Hashtable hash = GetHashtable();

            var itemId = hash[name] as ID;

            if (ID.IsNullOrEmpty(itemId))
            {
                Log.Fatal("Failed to get item id for: " + name, "ItemIds");
                return "";
            }

            return itemId.ToString();
        }

        public static string GetItemValue(string itemReferenceName, string fieldName, bool addSpan = false)
        {
            Item item = GetItem(itemReferenceName);

            if (item == null)
            {
                return null;
            }

            string value = item[fieldName];

            if (addSpan)
            {
                value = string.Format("<span>{0}</span>", value);
            }

            return value;
        }

        public static Item GetItem(string itemReferenceName)
        {
            if (string.IsNullOrEmpty(itemReferenceName))
            {
                Log.Fatal("itemReferenceName is empty", "ItemIds");
                return null;
            }

            ID itemId = GetID(itemReferenceName);

            if (Sitecore.Data.ID.IsNullOrEmpty(itemId))
            {
                Log.Fatal("Failed to find item id in SitecoreIds.config: " + itemReferenceName, "ItemIds");
                return null;
            }

            Item item;

            if (Sitecore.Context.ContentDatabase != null)
                item = Sitecore.Context.ContentDatabase.GetItem(itemId);
            else
                item = Sitecore.Context.Database.GetItem(itemId);

            if (item == null)
                Log.Fatal("Failed to find item in database: " + itemId, "ItemIds");

            return item;
        }

        private static Hashtable GetHashtable()
        {
            Hashtable hashtable = _settings;

            if (hashtable != null)
            {
                return hashtable;
            }

            lock (_settingsLock)
            {
                try
                {
                    hashtable = _settings;
                    if (hashtable != null)
                    {
                        return hashtable;
                    }
                    hashtable = new Hashtable();
                    XmlNodeList configNodes = Factory.GetConfigNodes(HASH_TABLE_PATH);
                    if (configNodes != null)
                    {
                        foreach (XmlNode node in configNodes)
                        {
                            string attribute = XmlUtil.GetAttribute("name", node);
                            string str2 = XmlUtil.GetAttribute("value", node);
                            if (attribute.Length > 0)
                            {
                                hashtable[attribute] = new ID(str2);
                            }
                        }
                    }
                }
                catch
                {
                    if (HostingEnvironment.IsHosted)
                    {
                        throw;
                    }
                }
                _settings = hashtable;
                return hashtable;
            }
        }

        public static bool IsDescendant(string parentReferenceName, Item descendantItem)
        {
            ID id = GetID(parentReferenceName);
            Item currentItem = descendantItem;

            while (currentItem != null)
            {
                if (currentItem.ID == id)
                {
                    return true;
                }

                currentItem = currentItem.Parent;
            }

            return false;
        }

        public static string GetUrl(string referenceName)
        {
            Item item = GetItem(referenceName);

            if (item == null)
                return null;

            return LinkManager.GetItemUrl(item);
        }

        public static bool IsTemplateBase(Item item, string referenceName)
        {
            ID referenceId = GetID(referenceName);

            return IsTemplateBase(item.Template, referenceId);
        }

        public static bool IsTemplateBase(TemplateItem templateItem, ID templateId)
        {
            if (templateItem.ID == templateId)
                return true;

            return templateItem.BaseTemplates.FirstOrDefault(p => IsTemplateBase(p, templateId)) != null;
        }
    }
}