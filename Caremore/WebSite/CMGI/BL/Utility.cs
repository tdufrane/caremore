﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using Sitecore.Data.Items;

namespace CMGI.BL
{
	public class Utility
	{
		public const string BASE_TITLE = "CareMore Medical Group";

		public const string HOME_PATH = "/sitecore/content/cmgi";
		public const string GLOBAL_PATH = "/sitecore/content/cmgi/Global";

		public const string PHYSICIANS_PATH = HOME_PATH + "/Patients/Physicians/List";
		public const string FIND_LOCATIONS_PATH = HOME_PATH + "/Locations/List";
		public const string SPECIALTY_PATH = GLOBAL_PATH + "/Specialties";

		public const string TESTIMONIALS_PATH = HOME_PATH + "/Testimonials";
		public const string LOCATION_ICON_PATH = GLOBAL_PATH + "/Location Icon";

		public const string MEET_THE_DOCTORS_PATH = "/Meet The Doctors";

		public static string MailHeader(string title)
		{
			StringBuilder builder = new StringBuilder();

			builder.AppendLine("<html>");
			builder.AppendLine("<head>");
			builder.AppendFormat("<title>{0}</title>\n", title);
			builder.AppendLine("<style type='text/css'>");
			builder.AppendLine("body, p, td { font-family: Calibri, Arial; font-size: 11pt; }");
			builder.AppendLine(".bold { font-weight: bold; }");
			builder.AppendLine(".shaded { background-color: #ccc; }");
			builder.AppendLine("</style>");
			builder.AppendLine("</head>");
			builder.AppendLine("<body>");

			return builder.ToString();
		}

		public static string MailBreak = "<br />";

		public const string MailTableOpen = "<table border='0' cellspacing='0' cellpadding='4'>";
		public const string MailTableClose = "</table>";

		public static string MailTableRow(string headerText)
		{
			StringBuilder builder = new StringBuilder();

			builder.AppendLine("<tr class='shaded'>"); // For email to have shading
			builder.AppendFormat("<td class='bold' colspan='2'>{0}</td>\n", headerText);
			builder.AppendLine("</tr>");

			return builder.ToString();
		}

		public static string MailTableRow(string headerText, string dataText)
		{
			StringBuilder builder = new StringBuilder();

			builder.AppendLine("<tr>");
			builder.AppendFormat("<td class='bold'>{0}</td>\n", headerText);
			builder.AppendFormat("<td>{0}</td>\n", dataText);
			builder.AppendLine("</tr>");

			return builder.ToString();
		}

		public static string MailFooter()
		{
			StringBuilder builder = new StringBuilder();

			builder.AppendLine("</body>");
			builder.AppendLine("</html>");

			return builder.ToString();
		}
	}

	public class DistanceComparer<T> : IComparer<T> where T : Item
	{
		private double ziplat, ziplong;

		public DistanceComparer(double loclat, double loclon)
		{
			ziplat = loclat;
			ziplong = loclon;
		}

		public int Compare(T x, T y)
		{
			double lat1 = double.Parse(((Item)x)["Lat"]);
			double lng1 = double.Parse(((Item)x)["Long"]);

			double lat2 = double.Parse(((Item)y)["Lat"]);
			double lng2 = double.Parse(((Item)y)["Long"]);

			double distx = Geocoding.Distance(ziplat, ziplong, lat1, lng1);
			double disty = Geocoding.Distance(ziplat, ziplong, lat2, lng2);

			if (distx < disty)
				return -1;
			else if (distx == disty)
				return 0;
			else
				return 1;
		}
	}

	public class ProxComparer<T> : IComparer<T> where T : CMGI.BL.ListItem.StandardItem
	{
		private double ziplat, ziplong;

		public ProxComparer(double loclat, double loclon)
		{
			ziplat = loclat;
			ziplong = loclon;
		}

		public double GetDistance(Item a)
		{
			return Geocoding.Distance(ziplat, ziplong, double.Parse(a["Lat"]), double.Parse(a["Long"]));
		}

		public int Compare(T x, T y)
		{
			CMGI.BL.ListItem.StandardItem a = x as CMGI.BL.ListItem.StandardItem;
			CMGI.BL.ListItem.StandardItem b = y as CMGI.BL.ListItem.StandardItem;

			double distx, disty;
			if (x is CMGI.BL.ListItem.EventItem)
			{
				distx = Geocoding.Distance(ziplat, ziplong, ((CMGI.BL.ListItem.EventItem)a).Lat, ((CMGI.BL.ListItem.EventItem)a).Long);
				disty = Geocoding.Distance(ziplat, ziplong, ((CMGI.BL.ListItem.EventItem)b).Lat, ((CMGI.BL.ListItem.EventItem)b).Long);
			}
			else if (x is CMGI.BL.ListItem.LocationItem)
			{
				distx = Geocoding.Distance(ziplat, ziplong, ((CMGI.BL.ListItem.LocationItem)a).Lat, ((CMGI.BL.ListItem.LocationItem)a).Long);
				disty = Geocoding.Distance(ziplat, ziplong, ((CMGI.BL.ListItem.LocationItem)b).Lat, ((CMGI.BL.ListItem.LocationItem)b).Long);
			}
			else
			{
				distx = 0.0; disty = 0.0;
			}

			if (distx < disty)
				return -1;
			else if (distx == disty)
				return 0;
			else
				return 1;
		}
	}

	public class DataComparer<T> : IComparer<T>
	{
		private string _sortExpression;
		private bool _nullsAreLess;

		public DataComparer(String sort, bool nullsLess)
		{
			_sortExpression = sort;
			_nullsAreLess = nullsLess;
		}

		public int Compare(T x, T y)
		{
			PropertyInfo pi = x.GetType().GetProperty(_sortExpression);
			if (pi == null)
				throw new NotSupportedException(String.Format("Property {0} of Type {1} does not exist and cannot be used in orderby.", _sortExpression, x.GetType().ToString()));

			IComparable a = (IComparable)pi.GetValue(x, null);
			IComparable b = (IComparable)pi.GetValue(y, null);

			if (a == null & b == null)
			{
				return 0;
			}
			else if (a == null & b != null)
			{
				return (_nullsAreLess) ? -1 : 1;
			}
			else if (a != null & b == null)
			{
				return (_nullsAreLess) ? 1 : -1;
			}
			else if (a != null & b != null)
			{
				return a.CompareTo(b);
			}
			return a.CompareTo(b);
		}
	}
}
