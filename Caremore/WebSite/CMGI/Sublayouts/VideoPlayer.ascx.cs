﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMGI.Sublayouts
{
	public partial class VideoPlayer : System.Web.UI.UserControl
	{
		public string Path
		{
			get;
			set;
		}

		public string Still
		{
			get;
			set;
		}

		public string Title
		{
			get
			{
				return this.VideoTitle.Text;
			}
			set
			{
				this.VideoTitle.Text = value;
			}
		}
	}
}
