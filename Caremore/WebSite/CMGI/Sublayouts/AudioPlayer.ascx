﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AudioPlayer.ascx.cs" Inherits="CMGI.Sublayouts.AudioPlayer" %>

<script type="text/javascript">
	$j(document).ready(function ()
	{
		$j("#<%= this.Jquery_jplayer_a1.ClientID %>").jPlayer({
			ready: function ()
			{
				$j(this).jPlayer("setMedia", {
					mp3: "<%= this.Path %>"
				});
			},
			cssSelectorAncestor: "#<%= this.Jp_container_a1.ClientID %>",
			swfPath: "/player",
			supplied: "mp3"
		});
	});
</script>

<div id="Jp_container_a1" runat="server" class="jp-audio">
	<div class="jp-type-single">
		<div id="Jquery_jplayer_a1" runat="server" class="jp-jplayer"></div>
		<div class="jp-gui jp-interface">
			<div class="jp-progress">
				<div class="jp-seek-bar">
					<div class="jp-play-bar"></div>
				</div>
			</div>
			<ul class="jp-controls">
				<li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
				<li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
				<li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
				<li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
				<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
				<li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
			</ul>
			<div class="jp-volume-bar">
				<div class="jp-volume-bar-value"></div>
			</div>
			<div class="jp-time-holder">
				<div class="jp-current-time"></div>
				<div class="jp-duration"></div>
					<ul class="jp-toggles">
						<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
						<li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
					</ul>
				</div>
			</div>
		<div class="jp-title" id="DivTitle" runat="server">
			<ul>
				<li><asp:Label ID="AudioTitle" runat="server" /></li>
			</ul>
		</div>
		<div class="jp-no-solution">
			<span>Update Required</span>
			To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
		</div>
	</div>
</div>
