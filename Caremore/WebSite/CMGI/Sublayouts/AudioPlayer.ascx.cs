﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMGI.Sublayouts
{
	public partial class AudioPlayer : System.Web.UI.UserControl
	{
		protected override void OnPreRender(EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.AudioTitle.Text))
				this.DivTitle.Visible = false;
			else
				this.DivTitle.Visible = true;

			base.OnPreRender(e);
		}

		public string Path
		{
			get;
			set;
		}

		public string Title
		{
			get
			{
				return this.AudioTitle.Text;
			}
			set
			{
				this.AudioTitle.Text = value;
			}
		}
	}
}
