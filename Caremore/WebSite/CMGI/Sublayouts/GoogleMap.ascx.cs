﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using CMGI.BL.ListItem;
using CMGI.BL;
using System.Configuration;

namespace CMGI.Sublayouts
{
    public partial class GoogleMap : System.Web.UI.UserControl
    {
        public String GoogleMapID { get; set; }
        public List<LocationItem> Locations = new List<LocationItem>();

        protected void Page_Load(object sender, EventArgs e)
        {
            GoogleMapID = ConfigurationManager.AppSettings["GoogleMapsAPIKey"];
        }
        
    }



}