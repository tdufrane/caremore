<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="PageLayout.aspx.cs" Inherits="Website.Layouts.PageLayout" MaintainScrollPositionOnPostback="true" ValidateRequest="true" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="mt" Namespace="Revere.West.SC.WebControls.MetaTags" Assembly="MetaTags" %>
<%@ Register TagPrefix="cm" Src="~/Controls/Header.ascx" TagName="Header" %>
<%@ Register TagPrefix="cm" Src="~/Controls/Footer.ascx" TagName="Footer" %>

<!doctype html>
<html>
<head id="head1" runat="server">
	<title id="pageTitle" runat="server" />
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<mt:MetaTags ID="mtMetaTags" runat="server" />

	<link href="//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,400,300,600" rel="stylesheet" type="text/css" />
	<link href="//fonts.googleapis.com/css?family=Lato:400,100,300" rel="stylesheet" type="text/css"/>
	<link href="/css/responsive/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="/css/style.min.css" rel="stylesheet" type="text/css" />
	<link id="pageStyle" runat="server" rel="stylesheet" type="text/css" />
	<link href="/css/print.min.css" rel="stylesheet" media="print" type="text/css" />
	<link rel="shortcut icon" href="/favicon.ico" />

	<!-- JavaScript -->
	<script src="/js/jquery.min.js" type="text/javascript"></script>
	<script src="/js/jquery.cookie.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/bootstrap-datepicker.min.js"></script>
	<script src="/js/jwplayer.js"></script>
	<script src="/js/global.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			var domain = document.domain;
			var subdomain;
			var periodIndex = domain.indexOf('.', 0);
			if (periodIndex == -1) {
				subdomain = domain;
			}
			else {
				subdomain = domain.substr(periodIndex);
			}

			$(document).on('click', 'a[href^="http"]:not([href*="' + subdomain + '"])', function () {
				return confirm("<asp:Literal id='litConfirmExternal' runat='server' />");
			});
		});
	</script>
	<link id="linkCanonical" runat="server" rel="canonical" />
</head>
<body>

<form id="formMain" runat="server">
	<asp:ScriptManager ID="scriptManager" runat="server" />

	<cm:Header id="ucHeader" runat="server" />

	<!-- START: Main content -->
	<div class="container top-spacer">
		<sc:Placeholder ID="placeholderMainContent" runat="server" Key="content" />
	</div>
	<!-- END: Main content -->

	<cm:Footer id="ucFooter" runat="server" />
</form>
<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js" type="text/javascript"></script>
	<script type="text/javascript">window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
<![endif]-->
	<!--[if lte IE 6]>
	<script type="text/javascript" src="/js/pngfix.js"></script>
<![endif]-->

<asp:PlaceHolder ID="phGoogleAnalytics" runat="server">
<!-- Google Analytics -->
<script type="text/javascript">
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-5795785-1', 'auto');
	ga('send', 'pageview');
</script>
<!-- End Google Analytics -->
</asp:PlaceHolder>

<asp:Literal ID="litCustomCode" runat="server" />

</body>
</html>
