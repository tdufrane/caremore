﻿using System;

namespace CMHS
{
	public class Constants
	{
		public const string CCCsKey = "CMHS_CCC_FOLDER";
		public const string CheckedTrueKey = "1";
		//public const string ClassAttribute = "class";
		public const string HiddenInNavigation = "HiddenInNavigation";
		public const string NavigationTitle = "NavigationTitle";
		public const string SiteSettingsKey = "CMHS_SITE_SETTINGS";
	}
}
