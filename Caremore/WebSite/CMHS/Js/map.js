﻿var mapWrapper = document.getElementById("mapWrapper");
var mapContainer = document.getElementById("map");
var mapImage = document.getElementById("mapImage");
var info = document.getElementById("regionInfo");
var infoWrapper = document.getElementById("regionInfoWrapper");
var regionSelector = document.getElementById("regionSelector");

regionSelector.addEventListener('change', loadRegion);
regions.forEach(function (region) {
	var option = createElement('option', region.name);
	option.value = region.name;
	regionSelector.appendChild(option);
});

var geocoder;
var map;

function loadRegion() {
	var region = _.find(regions, { name: this.options[this.selectedIndex].value });
	if (region) {
		showInfo(region);
		if ($(window).width() > 1150) {
			createMap(region);
		}
		mapWrapper.className = 'active';
		mapImage.className = 'hidden';
		mapContainer.className = '';
		infoWrapper.className = '';
	} else {
		info.innerHTML = '';
		mapWrapper.className = '';
		mapImage.className = '';
		mapContainer.className = 'hidden';
		infoWrapper.className = 'hidden';
	}
}

function showInfo(region) {
	info.innerHTML = '';
	info.appendChild(createElement('h1', region.name, 'region__name'));
	if (region.description) {
		info.appendChild(createElement('div', region.description, 'region__description'));
	}
	if (region.download) {
		var downloadLink = '<a class="region__download" href="' + region.download + '"><img class="icon" src="/CMHS/Images/PDFIcon.png" />Download Provider List</a>';
		$(info).append(downloadLink);
	} else {
		$(info).append('<p class="region__download"></p>')
	}
	if (region.locations) {
		var locations = document.createElement('div');
		locations.className = 'region__locations';
		region.locations.forEach(function (location, index) {
			locations.appendChild(createAddress(location));
			if (index < region.locations.length - 1) {
				locations.appendChild(document.createElement('hr'));
			}
		});
		info.appendChild(locations);
	}
}

function createAddress(location) {
	var address = document.createElement('div');
	address.className = 'address';
	address.appendChild(createElement('div', location.name, 'address__name'));
	address.appendChild(createElement('div', location.address));
	var phoneId = location.phone;
	//	if (location.id && location.id !== '') {
	//		phoneId += ' \u2014 ' + location.id
	//	}
	address.appendChild(createElement('div', phoneId));
	return address;
}

function createElement(tag, content, className) {
	var el = document.createElement(tag);
	if (className) {
		el.className = className;
	}
	if (content) {
		el.innerHTML = content;
	}
	return el;
}

function createMap(region) {
	initialize();
	if (region.locations.length > 0) {
		region.locations.forEach(function (location) {
			if (location.position) {
				addMarker(location.position);
			} else {
				codeAddress(location.address);
			}
		});
	}
}

function initialize() {
	geocoder = new google.maps.Geocoder();
	var latlng = new google.maps.LatLng(-34.397, 150.644);
	var mapOptions = {
		zoom: 8,
		center: latlng
	};
	map = new google.maps.Map(document.getElementById("map"), mapOptions);
}

function addMarker(position) {
	map.setCenter(position);
	var marker = new google.maps.Marker({
		map: map,
		position: position
	});
}

function codeAddress(address) {
	geocoder.geocode({ 'address': address }, function (results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			map.setCenter(results[0].geometry.location);
			console.log("Please cache this geocache result to avoid Google Maps API limits", address, { lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng() });
			var marker = new google.maps.Marker({
				map: map,
				position: results[0].geometry.location
			});
		} else {
			console.log("Geocode was not successful for the following reason: " + status, address);
		}
	});
}
