﻿$(document).ready(function () {
	var $header = $('#fixedHeader');
	var responsiveClass = 'pinned--white';

	$('#fullpage').fullpage({
		controlArrows: true,
		responsiveWidth: 1090,
		scrollingSpeed: 1000,
		onLeave: function (index, nextIndex, direction) {
			var loadedSection = $(this);

			if ($(window).width() > 1090) {
				if (nextIndex == 9) {
					$('#fixedHeader').addClass('pinned--white');
				}

				if (index == 9) {
					$('#fixedHeader').removeClass('pinned--white');
				}
			}
		},
		afterRender: function () {
			setInterval($.fn.fullpage.moveSlideRight, 5000);
		},
		afterResize: function () {
			var body = document.getElementsByTagName('body')[0];
			if ($.inArray('fp-responsive', body.classList) > -1) {
				if ($header.hasClass(responsiveClass)) $header.removeClass(responsiveClass);
			} else if (body.className.match("fp-viewing-8")) {
				if (!$header.hasClass(responsiveClass)) $header.addClass(responsiveClass);
			}
		}
	});
	$('.footer__next-section').click(function () {
		$.fn.fullpage.moveSectionDown();
	});
});
