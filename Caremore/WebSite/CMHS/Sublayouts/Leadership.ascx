﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Leadership.ascx.cs" Inherits="CMHS.Sublayouts.Leadership" %>

<!-- CMHS.Sublayouts.Leadership start -->
<asp:Repeater ID="rptLeadership" runat="server">
	<ItemTemplate>
		<p class="bio__photo"><sc:Image ID="imgPhoto" runat="server" Field="ExecutivePicture" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></p>

		<h2><sc:Text ID="txtTitle" runat="server" Field="ExecutiveTitle" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></h2>

		<h1><sc:Text ID="txtName" runat="server" Field="ExecutiveName" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></h1>

		<sc:Text ID="txtBiography" runat="server" Field="ExecutiveBiography" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />
	</ItemTemplate>
</asp:Repeater>
<asp:Panel ID="phErrors" runat="server" CssClass="error" Visible="false" />
<script type="text/javascript">
	$(document).ready(function () {
		$("p.bio__photo img").removeAttr("height width");
	});
</script>
<!-- CMHS.Sublayouts.Leadership end -->
