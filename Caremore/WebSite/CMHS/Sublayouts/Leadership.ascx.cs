﻿using System;
using CareMore.Web.DotCom;

namespace CMHS.Sublayouts
{
	public partial class Leadership : BaseSublayout
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				Utility.ShowError(phErrors, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			BindChildren(ItemIds.GetID("CMHS_EXECUTIVE_ITEM"), rptLeadership);
		}

		#endregion
	}
}
