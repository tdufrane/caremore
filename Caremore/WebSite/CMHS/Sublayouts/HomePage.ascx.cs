﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Items;
using CareMore.Web.DotCom;

namespace CMHS.Sublayouts
{
	public partial class HomePage : BaseSublayout
	{
		#region Properties

		private const string SectionCountKey = "SectionCount";

		private int SectionCount
		{
			get
			{
				object state = ViewState[SectionCountKey];
				return (state == null) ? 0 : (int)state;
			}
			set
			{
				ViewState[SectionCountKey] = value;
			}
		}

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				Utility.ShowError(phErrors, ex);
			}
		}

		protected void RptSections_DataBinding(object sender, EventArgs e)
		{
			List<Item> list = rptSections.DataSource as List<Item>;
			SectionCount = list.Count;
		}

		protected void RptSections_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
			{
				ConfigureSection(e.Item);
			}
		}

		protected void RptCollaborations_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
			{
				ConfigureTestimonial(e.Item);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			const string csUrl = "~/js/home.js";
			const string csName = "HomeScript";

			ClientScriptManager cs = Page.ClientScript;
			Type csType = this.GetType();

			if (cs.IsClientScriptIncludeRegistered(csType, csName))
			{
				cs.RegisterClientScriptInclude(csType, csName, csUrl);
			}

			ID itemID = ItemIds.GetID("CMHS_HOME_CONTENT_FOLDER");

			Item contentFolder = CurrentItem.Children
				.Where(i => i.ID == itemID)
				.FirstOrDefault();

			BindChildren(contentFolder, ItemIds.GetID("CMHS_HOME_CONTENT_ITEM"), rptSections);
		}

		private void ConfigureSection(RepeaterItem item)
		{
			Item sectionItem = item.DataItem as Item;
			CMHS.Controls.Footer ucFooter1 = item.FindControl("ucFooter1") as CMHS.Controls.Footer;

			Panel pnlSection = item.FindControl("pnlSection") as Panel;

			if (sectionItem["IsBackground"] == Constants.CheckedTrueKey)
			{
				pnlSection.CssClass += " section--background";
			}

			if (sectionItem["IsColumnsLayout"] == Constants.CheckedTrueKey)
			{
				pnlSection.CssClass += " section--columns";
			}

			if (sectionItem["IsDarkSection"] == Constants.CheckedTrueKey)
			{
				pnlSection.CssClass += " section--dark";
				ucFooter1.UseWhiteImages = true;
			}

			if (sectionItem["IsSlideOverlay"] == Constants.CheckedTrueKey)
			{
				pnlSection.CssClass += " section--overlay";
			}

			if (item.ItemIndex == SectionCount - 1)
			{
				ucFooter1.ShowNextSection = false;
			}
			else if (item.ItemIndex > 0)
			{
				ucFooter1.HideLearnMore = true;
			}

			Panel pnlSectionBackground = item.FindControl("pnlSectionBackground") as Panel;
			Image imageSectionBackground = item.FindControl("imageSectionBackground") as Image;

			string imageUrl = GetImageUrl(sectionItem, "SectionBackgroundImage");

			if (string.IsNullOrEmpty(imageUrl))
			{
				if (sectionItem["IsBackground"] != Constants.CheckedTrueKey)
				{
					pnlSectionBackground.Visible = false;
				}

				imageSectionBackground.Visible = false;
			}
			else
			{
				pnlSectionBackground.Style.Add(HtmlTextWriterStyle.BackgroundImage, string.Format("url('{0}')", imageUrl));
				imageSectionBackground.ImageUrl = imageUrl;
			}

			Repeater rptCollaborations = item.FindControl("rptCollaborations") as Repeater;
			BindChildren(sectionItem, ItemIds.GetID("CMHS_HOME_TESTIMONIAL_ITEM"), rptCollaborations);

			Panel footerPanel;
			if (sectionItem["IsBackground"] != Constants.CheckedTrueKey)
				footerPanel = item.FindControl("pnlFooter2") as Panel;
			else
				footerPanel = item.FindControl("pnlFooter1") as Panel;
			footerPanel.Visible = false;
		}

		private void ConfigureTestimonial(RepeaterItem item)
		{
			Item testimonialItem = item.DataItem as Item;

			HtmlAnchor aTestimonial = item.FindControl("aTestimonial") as HtmlAnchor;
			aTestimonial.HRef = GetFieldUrl(testimonialItem, "TestimonialLink");

			HtmlGenericControl pAttributionLogo = item.FindControl("pAttributionLogo") as HtmlGenericControl;
			if (string.IsNullOrWhiteSpace(testimonialItem["AttributionLogo"]))
			{
				pAttributionLogo.Visible = false;
			}
			else
			{
				Image imageAttribution = item.FindControl("imageAttribution") as Image;
				imageAttribution.ImageUrl = GetImageUrl(testimonialItem, "AttributionLogo");
			}
		}

		#endregion
	}
}
