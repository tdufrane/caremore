﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ClinicalModels.ascx.cs" Inherits="CMHS.Sublayouts.ClinicalModels" %>

<asp:Repeater ID="rptClinicalModels" runat="server">
	<ItemTemplate>
		<p class="post__image"><sc:Image id="imgClinicalImage" runat="server" Field="ClinicalImage" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></p>

		<h2><sc:Text id="txtClinicalNumber" runat="server" Field="ClinicalNumber" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></h2>

		<h2><sc:Text id="txtClinicalHeadline" runat="server" Field="ClinicalHeadline" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></h2>

		<p class="post__image icon icon--large"><sc:Image id="imgClinicalIcon" runat="server" Field="ClinicalIcon" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></p>

		<sc:Text id="txtContent" runat="server" Field="ClinicalContent" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />
	</ItemTemplate>
</asp:Repeater>
<asp:Panel ID="phErrors" runat="server" CssClass="error" Visible="false" />
