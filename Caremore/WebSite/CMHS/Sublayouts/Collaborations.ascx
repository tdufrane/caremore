﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Collaborations.ascx.cs" Inherits="CMHS.Sublayouts.Collaborations" %>

<asp:Repeater ID="rptCollaborations" runat="server">
	<ItemTemplate>
		<p class="post__image"><sc:Image ID="imgCollaboration" runat="server" Field="CollaborationImage" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></p>
		<sc:FieldRenderer ID="frCollaborationHeading" runat="server" FieldName="CollaborationHeading" EnclosingTag="h2" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />
		<sc:Text ID="txtCollaborationContent" runat="server" Field="CollaborationContent" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />
	</ItemTemplate>
</asp:Repeater>
<asp:Panel ID="phErrors" runat="server" CssClass="error" Visible="false" />
