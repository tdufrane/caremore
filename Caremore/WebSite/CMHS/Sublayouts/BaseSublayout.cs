﻿using System.Linq;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Data;
using Fields = Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Web.UI.WebControls;

namespace CMHS.Sublayouts
{
	public class BaseSublayout : System.Web.UI.UserControl
	{
		#region Properties

		private Item dataSource = null;
		public Item DataSource
		{
			get
			{
				if (dataSource == null && Parent is Sublayout)
					dataSource = Sitecore.Context.Database.GetItem(((Sublayout)Parent).DataSource);

				return dataSource;
			}
		}

		public Item CurrentItem
		{
			get
			{
				return Sitecore.Context.Item;
			}
		}

		#endregion

		#region Methods

		protected void BindChildren(ID childTemplateID, Repeater repeater)
		{
			BindChildren(Sitecore.Context.Item, childTemplateID, repeater);
		}

		protected void BindChildren(Item item, ID childTemplateID, Repeater repeater)
		{
			if (item != null && item.HasChildren)
			{
				var items = item.Children
					.Where(i => i.TemplateID == childTemplateID)
					.ToList();

				if (items == null || items.Count == 0)
				{
					repeater.Visible = false;
				}
				else
				{
					repeater.DataSource = items;
					repeater.DataBind();
				}
			}
			else
			{
				repeater.Visible = false;
			}
		}

		protected string GetFieldUrl(Item item, string fieldName)
		{
			string url = string.Empty;

			if (!string.IsNullOrWhiteSpace(item[fieldName]))
			{
				Fields.LinkField link = (Fields.LinkField)item.Fields[fieldName];

				if (link != null)
				{
					url = link.Url;
				}
			}

			return url;
		}

		protected string GetImageUrl(Item item, string fieldName)
		{
			string url = string.Empty;

			if (!string.IsNullOrWhiteSpace(item[fieldName]))
			{
				Fields.ImageField image = (Fields.ImageField)item.Fields[fieldName];

				if (image != null && image.MediaItem != null)
				{
					MediaItem media = image.MediaItem;
					url = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(media));
				}
			}

			return url;
		}

		#endregion
	}
}
