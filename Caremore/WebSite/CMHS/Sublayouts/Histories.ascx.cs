﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Items;
using SitecoreWebControls = Sitecore.Web.UI.WebControls;
using CareMore.Web.DotCom;

namespace CMHS.Sublayouts
{
	public partial class Histories : BaseSublayout
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				Utility.ShowError(phErrors, ex);
			}
		}

		protected void RptHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
				{
					FormatHistory(e.Item);
				}
			}
			catch (Exception ex)
			{
				Utility.ShowError(phErrors, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			BindChildren(ItemIds.GetID("CMHS_HISTORY_ITEM"), rptHistory);
		}

		private void FormatHistory(RepeaterItem repeaterItem)
		{
			HtmlGenericControl pHistoryImageTop = repeaterItem.FindControl("pHistoryImageTop") as HtmlGenericControl;
			HtmlGenericControl pHistoryImageBottom = repeaterItem.FindControl("pHistoryImageBottom") as HtmlGenericControl;
			Item historyItem = repeaterItem.DataItem as Item;

			if (string.IsNullOrWhiteSpace(historyItem["HistoryImage"]))
			{
				pHistoryImageTop.Visible = false;
				pHistoryImageBottom.Visible = false;
			}
			else
			{
				HtmlGenericControl pHistoryImage;

				if (historyItem["HistoryImageLocation"] == "Top")
				{
					pHistoryImage = pHistoryImageTop;
					pHistoryImageBottom.Visible = false;
				}
				else  //assuming (historyItem["HistoryImageLocation"] == "Bottom")
				{
					pHistoryImage = pHistoryImageBottom;
					pHistoryImageTop.Visible = false;
				}

				if (historyItem["HistoryImageIsIcon"] == "1")
				{
					pHistoryImage.Attributes["class"] += " icon icon--large";
				}

				if (!string.IsNullOrWhiteSpace(historyItem["HistoryCustomImageStyle"]))
				{
					pHistoryImage.Attributes["style"] = historyItem["HistoryCustomImageStyle"];
				}
			}
		}

		#endregion
	}
}
