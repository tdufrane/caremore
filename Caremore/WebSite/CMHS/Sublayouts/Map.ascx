﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Map.ascx.cs" Inherits="CMHS.Sublayouts.Map" %>

<!-- CMHS.Sublayouts.Map start -->
<div class="content-block content-block--blue">
	<h1>Find the CareMore Care Center nearest you:</h1>

	<div id="mapWrapper">
		<div class="map__left-flex">
			<div class="map__left">
				<select id="regionSelector" class="map__regions">
					<option>Select your state</option>
				</select>
			</div>
			<div id="mapImage">
				<sc:Image id="imgMap" runat="server" Field="CareCenterMap" />
			</div>
			<div id="map" class="hidden" style="height: 594px;"></div>
		</div>
		<div id="regionInfoWrapper" class="hidden">
			<div id="regionInfo"></div>
		</div>
	</div>
</div>

<asp:Repeater ID="rptRegions" runat="server" OnItemDataBound="RptRegions_ItemDataBound">
	<HeaderTemplate>
		<asp:Literal ID="litScriptOpen" runat="server" Text="<script type'text/javascript'>" />
		var regions = [
	</HeaderTemplate>
	<ItemTemplate>
		{
			'name': '<sc:Text id="txtRegionName" runat="server" Field="RegionName" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />',
			'description': '<sc:Text id="txtRegionDescription" runat="server" Field="RegionDescription" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />',
			'locations':

		<asp:Repeater ID="rptLocations" runat="server">
			<HeaderTemplate>
				[
			</HeaderTemplate>
			<ItemTemplate>
				{
					'name': '<sc:Text id="txtLocationName" runat="server" Field="LocationName" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />',
					'address': '<sc:Text id="txtLocationAddress" runat="server" Field="LocationAddress" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /><br /><sc:Text id="txtLocationCity" runat="server" Field="LocationCity" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />, <sc:Text id="txtLocationState" runat="server" Field="LocationState" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /> <sc:Text id="txtLocationZipCode" runat="server" Field="LocationZipCode" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />',
					'position': { lat: <sc:Text id="txtLocationLatitude" runat="server" Field="LocationLatitude" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />, lng: <sc:Text id="txtLocationLongitude" runat="server" Field="LocationLongitude" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /> },
					'phone': '<sc:Text id="txtLocationPhone" runat="server" Field="LocationPhone" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />',
					'id': '<sc:Text id="txtLocationExt" runat="server" Field="LocationPhoneExtension" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />'
				}
			</ItemTemplate>
			<SeparatorTemplate>,</SeparatorTemplate>
			<FooterTemplate>
				]
			</FooterTemplate>
		</asp:Repeater>

		}
	</ItemTemplate>
	<SeparatorTemplate>,</SeparatorTemplate>
	<FooterTemplate>
		];
		<asp:Literal ID="litScriptClose" runat="server" Text="</script>" />
	</FooterTemplate>
</asp:Repeater>

<script async="" defer="" src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js" type="text/javascript"></script>
<script async="" defer="" src="//maps.googleapis.com/maps/api/js?key=AIzaSyALr4ZmwR6hvCMrF1MaYwYSonuff-tnsVQ" type="text/javascript"></script>
<script src="/CMHS/Js/map.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$("#mapImage img").removeAttr("height width");
	});
</script>

<asp:Panel ID="phErrors" runat="server" CssClass="error" Visible="false" />
<!-- CMHS.Sublayouts.Map end -->
