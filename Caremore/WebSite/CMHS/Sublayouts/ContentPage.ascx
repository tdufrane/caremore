﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ContentPage.ascx.cs" Inherits="CMHS.Sublayouts.ContentPage" %>

<!-- CMHS.Sublayouts.ContentPage start -->
<div class="page-content">
	<article class="post">
		<div class="post__header">
			<h1 class="post-title"><sc:Text id="txtPageTitle" runat="server" Field="PageTitle" /></h1>
		</div>

		<div class="post__content">
			<p id="pImage" runat="server" class="post__image"><sc:Image id="imgPageImage" runat="server" Field="PageImage" /></p>

			<sc:Text id="txtContent" runat="server" Field="PageContent" />

			<sc:Placeholder ID="phCustomContent" runat="server" Key="custom-content" />
		</div>
	</article>
	<asp:Panel ID="phErrors" runat="server" CssClass="error" Visible="false" />
</div>
<!-- CMHS.Sublayouts.ContentPage end -->
