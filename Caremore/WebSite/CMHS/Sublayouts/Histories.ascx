﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Histories.ascx.cs" Inherits="CMHS.Sublayouts.Histories" %>

<!-- CMHS.Sublayouts.HistoryPage start -->
<asp:Repeater ID="rptHistory" runat="server" OnItemDataBound="RptHistory_ItemDataBound">
	<ItemTemplate>
		<div class="vertical"></div>

		<h2 id="section"><sc:Text id="txtYear" runat="server" Field="HistoryYear" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></h2>

		<p id="pHistoryImageTop" runat="server" class="post__image"><sc:Image id="imgHistoryImageTop" runat="server" Field="HistoryImage" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></p>

		<sc:Text id="txtContent" runat="server" Field="HistoryContent" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />

		<p id="pHistoryImageBottom" runat="server" class="post__image"><sc:Image id="imgHistoryImageBottom" runat="server" Field="HistoryImage" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></p>
	</ItemTemplate>
</asp:Repeater>
<asp:Panel ID="phErrors" runat="server" CssClass="error" Visible="false" />
<!-- CMHS.Sublayouts.HistoryPage end -->



