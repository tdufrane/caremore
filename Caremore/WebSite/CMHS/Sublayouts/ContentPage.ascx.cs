﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMHS.Sublayouts
{
	public partial class ContentPage : BaseSublayout
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				Utility.ShowError(phErrors, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			if (CurrentItem.Fields["PageImage"] == null || string.IsNullOrWhiteSpace(CurrentItem["PageImage"]))
			{
				pImage.Visible = false;
			}
		}

		#endregion
	}
}
