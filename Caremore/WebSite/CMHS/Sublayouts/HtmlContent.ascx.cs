﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;

namespace CMHS.Sublayouts
{
	public partial class HtmlContent : BaseSublayout
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				Utility.ShowError(phErrors, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			Item currentItem = Sitecore.Context.Item;

			if (currentItem.Fields[txtContent.Field] == null)
			{
				if (base.DataSource != null)
				{
					txtContent.Item = base.DataSource;
				}
			}
		}

		#endregion
	}
}
