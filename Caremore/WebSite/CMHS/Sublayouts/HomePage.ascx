﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="HomePage.ascx.cs" Inherits="CMHS.Sublayouts.HomePage" %>
<%@ Register Src="~/CMHS/Controls/Header.ascx" TagPrefix="uc1" TagName="Header" %>
<%@ Register Src="~/CMHS/Controls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>

<!-- CMHS.Sublayouts.HomePage start -->
<div id="fullpage">
	<asp:Repeater ID="rptSections" runat="server" OnDataBinding="RptSections_DataBinding" OnItemDataBound="RptSections_ItemDataBound">
		<ItemTemplate>
			<asp:Panel ID="pnlSection" runat="server" CssClass="section fp-table">
				<div class="section__container">
					<div class="invisible">
						<uc1:Header ID="ucHeader" runat="server" />
					</div>
					<div class="section__wrapper">
						<div class="section__content">
							<sc:Text id="txtSectionContent" runat="server" Field="SectionContent" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />
							<asp:Repeater ID="rptCollaborations" runat="server" OnItemDataBound="RptCollaborations_ItemDataBound">
								<ItemTemplate>
									<div class="slide">
										<a id="aTestimonial" runat="server" class="testimonial" target="_blank">
											<blockquote><sc:Text id="txtTestimonial" runat="server" Field="TestimonialContent" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></blockquote>

											<p class="attribution">
												&mdash;<sc:Text id="txtAttribution" runat="server" Field="AttributionContent" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />
											</p>

											<p id="pAttributionLogo" runat="server" class="attribution__logo">
												<asp:Image ID="imageAttribution" runat="server" />
											</p>
										</a>
									</div>
								</ItemTemplate>
							</asp:Repeater>
						</div>
						<asp:Panel ID="pnlSectionBackground" runat="server" CssClass="section__background" />
						<asp:Image id="imageSectionBackground" runat="server" class="section__image" />
						<asp:Panel ID="pnlFooter1" runat="server" class="section__footer">
							<uc1:Footer ID="ucFooter1" runat="server" ShowNextSection="true" />
						</asp:Panel>
					</div>
					<asp:Panel ID="pnlFooter2" runat="server" class="section__footer">
						<uc1:Footer ID="ucFooter2" runat="server" ShowNextSection="true" UseWhiteImages="true" />
					</asp:Panel>
				</div>
			</asp:Panel>
		</ItemTemplate>
	</asp:Repeater>
	<asp:Panel ID="phErrors" runat="server" CssClass="error" Visible="false" />
</div>
<script src="/CMHS/Js/home.js" type="text/javascript"></script>
<!-- CMHS.Sublayouts.HomePage end -->

