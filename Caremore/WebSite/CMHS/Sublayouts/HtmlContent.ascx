﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="HtmlContent.ascx.cs" Inherits="CMHS.Sublayouts.HtmlContent" %>

<!-- CMHS.Sublayouts.HtmlContent start -->
<sc:Text id="txtContent" runat="server" Field="ContentText" />
<asp:Panel ID="phErrors" runat="server" CssClass="error" Visible="false" />
<!-- CMHS.Sublayouts.HtmlContent start -->
