﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Items;
using CareMore.Web.DotCom;

namespace CMHS.Sublayouts
{
	public partial class Map : BaseSublayout
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				Utility.ShowError(phErrors, ex);
			}
		}

		protected void RptRegions_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			try
			{
				LoadRegion(e.Item);
			}
			catch (Exception ex)
			{
				Utility.ShowError(phErrors, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			Item cccFolder = ItemIds.GetItem(Constants.CCCsKey);

			if (cccFolder == null)
			{
				rptRegions.Visible = false;
			}
			else
			{
				BindChildren(cccFolder, ItemIds.GetID("CMHS_REGION_ITEM"), rptRegions);
			}
		}

		private void LoadRegion(RepeaterItem item)
		{
			Repeater rptLocations = item.FindControl("rptLocations") as Repeater;

			if (rptLocations != null)
			{
				BindChildren(item.DataItem as Item, ItemIds.GetID("CMHS_LOCATION_ITEM"), rptLocations);
			}
		}

		#endregion
	}
}
