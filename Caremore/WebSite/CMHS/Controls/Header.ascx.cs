﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Links;
using CareMore.Web.DotCom;

namespace CMHS.Controls
{
	public partial class Header : System.Web.UI.UserControl
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				Utility.ShowError(phErrors, ex);
			}
		}

		protected void RptHeaderLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
				{
					SetLink(e.Item);
				}
			}
			catch (Exception ex)
			{
				Utility.ShowError(phErrors, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			Item homeItem = Utility.HomeItem();

			if (homeItem.HasChildren)
			{
				var navItems = homeItem.Children
					.Where(i => !string.IsNullOrWhiteSpace(i[Constants.NavigationTitle]) && string.IsNullOrWhiteSpace(i[Constants.HiddenInNavigation]))
					.ToList();

				if (navItems == null || navItems.Count == 0)
				{
					rptHeaderLinks.Visible = false;
				}
				else
				{
					rptHeaderLinks.DataSource = navItems;
					rptHeaderLinks.DataBind();
				}
			}
			else
			{
				rptHeaderLinks.Visible = false;
			}

			SetLogo();
		}

		private void SetLink(RepeaterItem item)
		{
			HyperLink hlHeaderLink = item.FindControl("hlHeaderLink") as HyperLink;
			if (hlHeaderLink != null)
			{
				Item pageItem = item.DataItem as Item;
				if (pageItem != null)
				{
					if (Sitecore.Context.Item.ID == pageItem.ID)
						hlHeaderLink.CssClass += " site-nav__link--active";

					hlHeaderLink.NavigateUrl = LinkManager.GetItemUrl(pageItem);
					hlHeaderLink.Text = pageItem[Constants.NavigationTitle];
				}
			}
		}

		private void SetLogo()
		{
			Item siteSettings = Utility.SiteSettingsItem();
			imgLogoWhite.Item = siteSettings;
			imgLogo.Item = siteSettings;
		}

		#endregion
	}
}
