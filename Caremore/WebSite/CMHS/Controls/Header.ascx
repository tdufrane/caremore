﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Header.ascx.cs" Inherits="CMHS.Controls.Header" %>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.WebControls" TagPrefix="sc" %>

<!-- CMHS.Controls.Header start -->
<header class="header">
	<div class="header__logo">
		<a class="header__logo-link" href="/">
			<sc:Image id="imgLogoWhite" runat="server" CssClass="header__logo-image--white" Field="Logo White" />
			<sc:Image id="imgLogo" runat="server" CssClass="header__logo-image" Field="Logo" />
		</a>
		<div class="site-nav__toggle site-nav__toggle--exit">
			<img src="/CMHS/Images/MenuExitIcon.png" alt="">
		</div>
		<div class="site-nav__toggle site-nav__toggle--enter">
			<img src="/CMHS/Images/MenuIcon.png" alt="">
		</div>
	</div>
	<nav class="site-nav">
		<asp:Repeater ID="rptHeaderLinks" runat="server" OnItemDataBound="RptHeaderLinks_ItemDataBound">
			<ItemTemplate>
				<asp:Hyperlink ID="hlHeaderLink" runat="server" CssClass="site-nav__link"></asp:Hyperlink>
			</ItemTemplate>
		</asp:Repeater>
	</nav>
</header>
<asp:Panel ID="phErrors" runat="server" CssClass="content-block" Visible="false" />
<!-- CMHS.Controls.Header end -->
