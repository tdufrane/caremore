﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
//using SitecoreWebControls = Sitecore.Web.UI.WebControls;
using CareMore.Web.DotCom;

namespace CMHS.Controls
{
	public partial class Footer : Sublayouts.BaseSublayout
	{
		#region Properties

		private const string HideLearnMoreKey = "HideLearnMore";
		private const string ShowNextSectionKey = "ShowNextSection";
		private const string UseWhiteImagesKey = "UseWhiteImages";

		public bool HideLearnMore
		{
			get
			{
				object state = ViewState[HideLearnMoreKey];
				return (state == null) ? false : (bool)state;
			}
			set
			{
				ViewState[HideLearnMoreKey] = value;
			}
		}

		public bool ShowNextSection
		{
			get
			{
				object state = ViewState[ShowNextSectionKey];
				return (state == null) ? false : (bool)state;
			}
			set
			{
				ViewState[ShowNextSectionKey] = value;
			}
		}

		public bool UseWhiteImages
		{
			get
			{
				object state = ViewState[UseWhiteImagesKey];
				return (state == null) ? false : (bool)state;
			}
			set
			{
				ViewState[UseWhiteImagesKey] = value;
			}
		}

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				Utility.ShowError(phErrors, ex);
			}
		}

		protected void RptSocial_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
				{
					SetItem(e.Item);
				}
			}
			catch (Exception ex)
			{
				Utility.ShowError(phErrors, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			BindChildren(ItemIds.GetItem("CMHS_SOCIAL_FOLDER"),
				ItemIds.GetID("CMHS_SOCIAL_ITEM"), rptSocial);

			if (ShowNextSection)
			{
				if (HideLearnMore)
					pnlLearnMore.CssClass += " hidden";

				pnlNextSection.Visible = true;
			}
			else
			{
				pnlNextSection.Visible = false;
			}

			if (UseWhiteImages)
				imgNextSection.ImageUrl = "/CMHS/Images/DownButtonWhite.png";
			else
				imgNextSection.ImageUrl = "/CMHS/Images/DownButton.png";
		}

		private void SetItem(RepeaterItem item)
		{
			Item socialItem = item.DataItem as Item;
			if (socialItem != null)
			{
				HyperLink hlSocialLink = item.FindControl("hlSocialLink") as HyperLink;
				hlSocialLink.NavigateUrl = GetFieldUrl(socialItem, "SocialLink");

				Panel pnlIcon = item.FindControl("pnlIcon") as Panel;
				if (!string.IsNullOrWhiteSpace(socialItem["SocialCssClass"]))
				{
					pnlIcon.CssClass += socialItem["SocialCssClass"];
				}

				Image imgIcon = item.FindControl("imgIcon") as Image;
				string field;
				if (UseWhiteImages)
					field = "SocialLightImage";
				else
					field = "SocialDarkImage";

				imgIcon.ImageUrl = GetImageUrl(socialItem, field);
				imgIcon.AlternateText = socialItem.Name;
			}
		}

		#endregion
	}
}
