﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="CMHS.Controls.Footer" %>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.WebControls" TagPrefix="sc" %>

<!-- CMHS.Controls.Footer start -->
<footer class="footer">
	<asp:Repeater ID="rptSocial" runat="server" OnItemDataBound="RptSocial_ItemDataBound">
		<HeaderTemplate>
			<div class="footer__social">
				<ul class="social-list">
		</HeaderTemplate>
		<ItemTemplate>
			<li class="social-list__item">
				<asp:HyperLink ID="hlSocialLink" runat="server" Target="_blank">
					<asp:Panel id="pnlIcon" runat="server" CssClass="icon icon--">
						<asp:Image id="imgIcon" runat="server" />
					</asp:Panel>
				</asp:HyperLink>
			</li>
		</ItemTemplate>
		<FooterTemplate>
				</ul>
			</div>
		</FooterTemplate>
	</asp:Repeater>

	<asp:Panel id="pnlNextSection" runat="server" CssClass="footer__next-section">
		<asp:Panel id="pnlLearnMore" runat="server" CssClass="footer__next-section-text">Learn more</asp:Panel>
		<asp:Image ID="imgNextSection" runat="server" CssClass="footer__next-section-arrow" ImageUrl="/CMHS/Images/DownButton.png" AlternateText="next section" />
	</asp:Panel>

	<div class="footer__copyright">
		Copyright &copy; 2016 CareMore Health System
	</div>
</footer>
<asp:Panel ID="phErrors" runat="server" CssClass="content-block" Visible="false" />
<!-- CMHS.Controls.Footer end -->
