﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;

namespace CMHS.Layouts
{
	public partial class MainLayout : System.Web.UI.Page
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				Utility.ShowError(phErrors, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			if (Sitecore.Context.Item.ID == Utility.HomeItem().ID)
			{
				bodyPage.Attributes["class"] = "home";
			}

			Item siteSettings = Utility.SiteSettingsItem();

			if (siteSettings != null)
			{
				txtSiteTitle.Item = siteSettings;
				metaDescr.Attributes["content"] = siteSettings["Meta Description"];
			}
		}

		#endregion
	}
}
