﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="MainLayout.aspx.cs" Inherits="CMHS.Layouts.MainLayout" %>
<%@ Register Src="~/CMHS/Controls/Header.ascx" TagPrefix="uc1" TagName="Header" %>
<%@ Register Src="~/CMHS/Controls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.WebControls" TagPrefix="sc" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title><sc:Text id="txtSiteTitle" runat="server" Field="Site Title" /></title>
	<meta id="metaDescr" runat="server" name="description" />
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.7.6/jquery.fullPage.css" />
	<link rel="stylesheet" href="/CMHS/Css/main.css" />
	<script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.7.6/jquery.fullPage.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
</head>

<body id="bodyPage" runat="server">
	<div id="fixedHeader" class="pinned">
		<uc1:Header ID="Header" runat="server" />
	</div>

	<sc:Placeholder ID="phMainContent" runat="server" Key="content" />

	<asp:Panel ID="phErrors" runat="server" CssClass="error" Visible="false" />

	<div id="fullFooter">
		<uc1:Footer ID="ucFooter" runat="server" />
	</div>

	<script type="text/javascript">
		$(document).ready(function () {
			$('.site-nav__toggle').click(function () {
				$('#fixedHeader').toggleClass('active');
			});
			$("img").removeAttr("height width");
		});
	</script>
	<script type="text/javascript">
		(function (i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
			a = s.createElement(o),
			  m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-76495023-1', 'auto');
		ga('send', 'pageview');
	</script>

</body>
</html>
