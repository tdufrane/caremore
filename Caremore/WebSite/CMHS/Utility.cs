﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Sitecore.Data.Items;
using CareMore.Web.DotCom;

namespace CMHS
{
	public class Utility
	{
		public static Item HomeItem()
		{
			return Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath);
		}

		public static Item SiteSettingsItem()
		{
			return ItemIds.GetItem(Constants.SiteSettingsKey);
		}

		public static void ShowError(Control control, Exception ex)
		{
			ShowError(control, ex, null);
		}

		public static void ShowError(Control control, Exception ex, Control toHide)
		{
			ShowMessage(control, Common.GetErrorMessage(ex), "div", "error");

			if (toHide != null)
				toHide.Visible = false;
		}

		public static void ShowMessage(Control control, string message, string tag, string cssClass)
		{
			HtmlGenericControl paragraph = new HtmlGenericControl(tag);

			if (!string.IsNullOrWhiteSpace(cssClass))
				paragraph.Attributes.Add("class", "error");

			paragraph.InnerHtml = message;
			control.Controls.Add(paragraph);
			control.Visible = true;
		}
	}
}
