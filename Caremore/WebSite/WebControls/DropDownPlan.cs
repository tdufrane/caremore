﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;

namespace Website.WebControls
{
	public class DropDownPlan : System.Web.UI.WebControls.DropDownList
	{
		public DropDownPlan()
		{
			AutoPostBack = true;
		}

		#region Properties

		private string _selectedPlan;

		public DropDownLocation CountyDropDown { get; set; }

		public Item PlanItem
		{
			get
			{
				if (string.IsNullOrEmpty(SelectedValue) && string.IsNullOrEmpty(Page.Request.QueryString["planTypeID"]))
					return null;

				// If we have the planTypeID in query string, it means they have navigated from plan finder
				if (!string.IsNullOrEmpty(Page.Request.QueryString["planTypeID"]) && !Page.IsPostBack)
				{
					_selectedPlan = Page.Request.QueryString["planTypeID"];
				}
				else if (!string.IsNullOrEmpty(SelectedValue))
				{
					_selectedPlan = SelectedValue;
				}

				if (string.IsNullOrWhiteSpace(_selectedPlan))
					return null;
				else
					return Sitecore.Context.Database.GetItem(_selectedPlan);
			}
		}

		public Item AppItem
		{
			get
			{
				if (PlanItem == null || string.IsNullOrEmpty(PlanItem["Application"]))
					return null;

				ReferenceField applicationItem = (ReferenceField)PlanItem.Fields["Application"];

				if (applicationItem.TargetItem == null)
					return null;
				else
					return applicationItem.TargetItem;
			}
		}

		#endregion

		#region Page events

		protected override void OnPreRender(EventArgs e)
		{
			try
			{
				GetItems();
			}
			catch (Exception ex)
			{
				Common.EmailException(ex);
			}
		}

		#endregion

		#region Private Methods

		private string GetDefaultText()
		{
			string text = null;

			if (string.IsNullOrEmpty(CountyDropDown.SelectedValue))
			{
				text = ItemIds.GetItemValue("PLAN_DROPDOWN_WITH_NO_COUNTY_SELECTED_MESSAGE", "Text");
			}

			if(string.IsNullOrEmpty(text))
			{
				text = ItemIds.GetItemValue("ENROLL_SELECT_PLAN_MESSAGE", "Text");
			}

			return text;
		}

		private List<Item> GetPlans()
		{
			// Get the Selected Location
			Item selectedState = Sitecore.Context.Database.Items[CountyDropDown.SelectedValue];

			// Get All Plans under a respective Location
			string path = string.Format("{0}/{1}//*[@Region Name='{2}']//*[@@templateid='{3}']",
				CareMoreUtilities.REGIONS_PATH,
				CareMoreUtilities.GetCurrentLocale(),
				selectedState["Region Name"],
				ItemIds.GetString("PLAN_TEMPLATE_ID"));

			Item[] items = Sitecore.Context.Database.SelectItems(path);

			List<Item> plans = new List<Item>();

			foreach (Item item in items)
			{
				LookupField planType = item.Fields["Plan Type"];

				if (planType == null || planType.TargetItem == null)
					continue;

				if (planType.TargetItem["Visible"] == "1")
				{
					plans.Add(item);
				}
			}

			return plans;
		}

		private void GetItems()
		{
			List<Item> plans = new List<Item>();

			Enabled = !string.IsNullOrEmpty(CountyDropDown.SelectedValue);

			if (CountyDropDown.SelectedValue != string.Empty)
			{
				plans = GetPlans();
			}

			string oldValue = SelectedValue;

			if(string.IsNullOrEmpty(oldValue) && !Page.IsPostBack)
				oldValue = Page.Request.QueryString["planTypeID"];

			Items.Clear();

			string text = GetDefaultText();
			Items.Add(new ListItem(text, string.Empty));

			foreach (Item plan in plans)
			{
				// Display only those plans which has Application set
				// If the current item is Plan Materials
				if (Sitecore.Context.Item.ID == ItemIds.GetID("PLAN_MATERIALS"))
				{
					Item planType = Sitecore.Context.Database.Items[plan["Plan Type"]];

					ListItem item = new ListItem(planType.Fields["Title"].Value, plan.ID.ToString());

					item.Selected = (item.Value == oldValue);

					Items.Add(item);
				}
				else
				{
					if (!string.IsNullOrEmpty(plan["Application"]))
					{
						Item planType = Sitecore.Context.Database.Items[plan["Plan Type"]];

						ListItem item = new ListItem(planType.Fields["Title"].Value, plan.ID.ToString());

						item.Selected = (item.Value == oldValue);

						Items.Add(item);
					}
				}
			}
		}

		#endregion
	}
}
