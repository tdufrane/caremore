﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CareMore.Web.DotCom;
using Sitecore.Data.Items;
using System.Web.UI.WebControls;

namespace Website.WebControls
{
    public class DropDownLocation : System.Web.UI.WebControls.DropDownList
    {
        public Item CountyItem
        {
            get
            {
                if (string.IsNullOrEmpty(SelectedValue))
                    return null;

                return Sitecore.Context.Database.GetItem(SelectedValue);
            }
        }

        public DropDownLocation()
        {
            AutoPostBack = true;
        }

        protected override void OnPreRender(EventArgs e)
        {
            // Get all the Regions from Global
            string currentState = CareMoreUtilities.GetCurrentLocale();

            Item[] regions = Sitecore.Context.Database.SelectItems(CareMoreUtilities.REGIONS_PATH + "/" + currentState + "/*");
            Array.Sort<Item>(regions, delegate(Item x, Item y) { return (x.Name).CompareTo((y.Name)); });

            string selectedValue = SelectedValue;

            if (!Page.IsPostBack)
            {
                selectedValue = Page.Request.QueryString["countyID"];
            }

            Items.Clear();

            foreach (Item region in regions)
            {
                ListItem item = new ListItem(region["Region Name"], region.ID.ToString());

                item.Selected = (item.Value == selectedValue);

                Items.Add(item);
            }

            Items.Insert(0, new ListItem(ItemIds.GetItemValue("ENROLL_SELECT_COUNTY_MESSAGE", "Text"), string.Empty));
        }
    }
}