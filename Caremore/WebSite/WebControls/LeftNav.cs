﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.SessionState;
using CareMore.Web.DotCom;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Web.UI;
using Website.BL;

namespace Website.WebControls
{
	public class LeftNav : WebControl
	{
		public LeftNav()
		{
		}

		protected override void DoRender(HtmlTextWriter writer)
		{
			ListItem rootItem = GetRootItem();

			if (rootItem != null)
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Id, "sideNav");
				writer.RenderBeginTag(HtmlTextWriterTag.Div);

				writer.AddAttribute(HtmlTextWriterAttribute.Class, "nav nav-stacked");
				writer.RenderBeginTag(HtmlTextWriterTag.Ul);

				writer.AddAttribute(HtmlTextWriterAttribute.Class, "nav-stacked-hdr text-uppercase");
				writer.RenderBeginTag(HtmlTextWriterTag.Li);

				writer.RenderBeginTag(HtmlTextWriterTag.Span);
				writer.Write(CareMoreUtilities.GetNavigationTitle(rootItem.InnerItem));
				writer.RenderEndTag(); // span

				writer.RenderEndTag(); // li.nav-stacked-hdr

				rootItem.Children.ForEach(p => Render(writer, p));
				writer.RenderEndTag();  // ul

				writer.RenderEndTag(); // div#sideNav
			}
		}

		private void Render(HtmlTextWriter writer, ListItem listItem)
		{
			if (listItem.IsActive)
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Class, "active");
			}

			writer.RenderBeginTag(HtmlTextWriterTag.Li);

			if (listItem.IsCurrent)
			{
				writer.RenderBeginTag(HtmlTextWriterTag.Span);
				writer.Write(CareMoreUtilities.GetNavigationTitle(listItem.InnerItem));
				writer.RenderEndTag(); // span
			}
			else
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Href, listItem.Url);

				if (!string.IsNullOrEmpty(listItem.Target))
					writer.AddAttribute(HtmlTextWriterAttribute.Target, listItem.Target);

				writer.RenderBeginTag(HtmlTextWriterTag.A);
				writer.Write(CareMoreUtilities.GetNavigationTitle(listItem.InnerItem));
				writer.RenderEndTag(); // a
			}

			if (listItem.Children.Count > 0)
			{
				RenderChildren(writer, listItem);
			}

			writer.RenderEndTag(); // li
		}

		private static string GetTarget(Item item)
		{
			if (item.TemplateID == ItemIds.GetID("EXTERNAL_LINK_TEMPLATE_ID"))
			{
				LinkField link = item.Fields["Link"];

				if (link != null && !string.IsNullOrEmpty(link.Target))
				{
					return link.Target;
				}
			}

			return string.Empty;
		}

		private static string GetUrl(Item item)
		{
			string url = null;

			if (item.TemplateID == ItemIds.GetID("EXTERNAL_LINK_TEMPLATE_ID"))
			{
				LinkField link = item.Fields["Link"];

				if (link != null && !string.IsNullOrEmpty(link.Url))
				{
					return link.Url;
				}
			}

			if (url == null && item != null)
			{
				url = LinkManager.GetItemUrl(item);
			}

			return url;
		}

		private static string GetText(Item item)
		{
			if (item.TemplateID == ItemIds.GetID("EXTERNAL_LINK_TEMPLATE_ID"))
			{
				LinkField link = item.Fields["Link"];

				return link.Text;
			}

			return CareMoreUtilities.GetNavigationTitle(item);

		}

		private void RenderChildren(HtmlTextWriter writer, ListItem listItem)
		{
			writer.RenderBeginTag(HtmlTextWriterTag.Ul);

			foreach (ListItem childListItem in listItem.Children)
			{
				Render(writer, childListItem);
			}

			writer.RenderEndTag();
		}

		protected ListItem GetRootItem()
		{
			Item currentItem = GetContext();
			Item contextItem = GetContext();

			while (currentItem != null && currentItem.ParentID != ItemIds.GetID("HOME") && currentItem.ID != ItemIds.GetID("CONTENT_ID"))
			{
				currentItem = currentItem.Parent;
			}

			if (currentItem == null)
				return null;

			ListItem rootListItem = GetListItem(currentItem, contextItem);

			if (rootListItem != null)
				FilterDuplicates(rootListItem);

			return rootListItem;
		}

		protected Item GetContext()
		{
			Item contextItem = Sitecore.Context.Item;
			Item currentItem = Sitecore.Context.Item;

			while (currentItem != null && currentItem.ParentID != ItemIds.GetID("HOME") && currentItem.ID != ItemIds.GetID("CONTENT_ID"))
			{
				LookupField leftNavField = currentItem.Fields["Left Navigation Item"];

				if (leftNavField != null && leftNavField.TargetItem != null)
				{
					contextItem = leftNavField.TargetItem;
					currentItem = leftNavField.TargetItem;
				}
				else
				{
					currentItem = currentItem.Parent;
				}
			}

			return contextItem;
		}

		/*
		 * Remove multiple listitems when they reference same Sitecore Item.
		 * Keep the item with the lowest depth
		 */
		private void FilterDuplicates(ListItem rootListItem)
		{
			var dupResults = rootListItem.Children.Where(p => IsFiltered(p, rootListItem)).ToList();

			rootListItem.Children.RemoveAll(p => dupResults.Contains(p));
		}

		private bool IsFiltered(ListItem listItem, ListItem rootListItem)
		{
			return listItem.InnerItem.ID == rootListItem.InnerItem.ID;
		}

		private ListItem GetListItem(Item currentItem, Item contextItem)
		{
			if (!CareMoreUtilities.IsVisible(currentItem))
				return null;

			Item displayItem = CareMoreUtilities.GetLocalizedItem(currentItem);

			ListItem listItem = new ListItem();

			listItem.InnerItem = displayItem;
			listItem.IsCurrent = contextItem.ID == displayItem.ID;
			listItem.Url = GetUrl(displayItem);
			listItem.Target = GetTarget(displayItem);

			if (currentItem.Paths.IsAncestorOf(contextItem) || listItem.IsCurrent)
			{
				listItem.IsActive = true;

				foreach (Item childItem in currentItem.Children.OrderBy(item => item["Sortorder"]))
				{
					ListItem listChildItem = GetListItem(childItem, contextItem);

					if (!CareMoreUtilities.IsCorrectLocalizedNavItem(childItem, CareMoreUtilities.GetCurrentLocale()))
						listChildItem = null;

					if (listChildItem != null)
						listItem.Children.Add(listChildItem);
				}
			}

			return listItem;

		}

		protected class ListItem
		{
			public List<ListItem> Children { get; set; }
			public Item InnerItem { get; set; }
			public bool IsActive { get; set; }
			public bool IsCurrent { get; set; }
			public string Url { get; set; }
			public string Target { get; set; }

			public ListItem()
			{
				Children = new List<ListItem>();
			}
		}
	}
}