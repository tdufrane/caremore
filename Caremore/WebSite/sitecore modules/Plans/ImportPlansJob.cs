﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;

using Sitecore;
using Sitecore.Collections;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Jobs;
using Sitecore.Resources;
using Sitecore.Security.Accounts;
using Sitecore.Security.AccessControl;

namespace Website.sitecore_modules.Plans
{
	public class ImportPlansJob
	{
		#region Public variables

		public string JobName = "Import Plans";
		public string JobMessages = string.Empty;

		#endregion

		#region Private variables

		private const string English = "ENG";
		private const string Spanish = "SPA";

		private Database currentDB = Factory.GetDatabase("master");

		private Item folderItem = null;
		private Item configItem = null;
		private Item plansItem = null;

		private TemplateItem stateTemplate = null;
		private TemplateItem countyTemplate = null;
		private TemplateItem yearTemplate = null;
		private TemplateItem planTemplate = null;

		#endregion

		public Job Job
		{
			get
			{
				return JobManager.GetJob(JobName);
			}
		}

		public void ImportData(string fileName, string sheetName, string documentFolder)
		{
			int count = 0, importedCount = 0;
			StringBuilder errors = new StringBuilder();
			DateTime started = DateTime.Now;

			try
			{
				if (Job != null)
					Job.Status.State = JobState.Running;

				string connectionString = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;IMEX=1;HDR=YES;""", fileName);
				OleDbConnection objConn = new OleDbConnection(connectionString);

				//Spreadsheet column names
				string strSql = string.Format("SELECT [Year], [State], [County], [Plan], [Plan ID], [Contract], [SB EN], [SB SP], [EOC EN], [EOC SP], [Drug EN], [Drug SP], [Pharm Dir EN], [Pharm Dir SP], [Application EN], [Application SP], [Plan Chart EN], [Plan Chart SP], [ANOC EN], [ANOC SP], [How To EN], [How To SP], [Premium], [Payment Info] FROM [{0}$] WHERE [Year] IS NOT NULL ORDER BY [Year], [State], [County], [Plan]", sheetName);
				OleDbCommand objCmd = new OleDbCommand(strSql, objConn);

				objConn.Open();
				List<PlanItem> list = new List<PlanItem>();
				using (OleDbDataReader dr = objCmd.ExecuteReader())
				{
					while (dr.Read())
					{
						if (Job != null)
							Job.AddMessage("Reading row {0}", count + 1);

						if (!IsEmptyRow(dr))
						{
							PlanItem item = new PlanItem(dr);

							if (string.IsNullOrWhiteSpace(item.ReaderError))
							{
								list.Add(item);
							}
							else
							{
								errors.AppendLine(item.ReaderError);
							}

							count++;
						}
					}

					dr.Close();
				}

				objConn.Close();

				importedCount = CreateItems(list, errors, documentFolder);
			}
			catch (Exception ex)
			{
				if (Job != null)
					Job.Status.State = JobState.Unknown;

				errors.AppendLine(ex.Message);
				Log.Error("ImportProvidersJob.ImportData Exception", ex, this.GetType());
			}
			finally
			{
				if (Job != null)
					Job.Status.State = JobState.Finished;

				StringBuilder message = new StringBuilder();
				message.AppendFormat("<p><strong>{0}</strong> plans were read. <span style=\"color:Blue;\">{1}</span> plans have been successfully imported.</p>",
					count, importedCount);

				if (errors.Length > 0)
				{
					errors.Length -= 1; // Remove trailing \n

					message.AppendFormat("<p><strong>Errors:</strong><br />{0}</p>", errors.ToString().Replace("\n", "<br />"));
				}

				TimeSpan processTime = (DateTime.Now - started);
				message.AppendFormat("<p>Processing time was {0} minutes, {1} seconds.</p>",
					processTime.Minutes, processTime.Seconds);

				if (Job == null)
					JobMessages = message.ToString();
				else
					Job.AddMessage(message.ToString());
			}
		}

		private int CreateItems(List<PlanItem> list, StringBuilder errors, string documentFolder)
		{
			int itemCount = 0, importedCount = 0;

			SetCoreItemsAndTemplates();

			foreach (PlanItem item in list)
			{
				itemCount++;

				string planName = string.Format("Plan: {0} - {1} - {2} - {3}",
					item.Year, item.State, item.County, item.Plan);

				try
				{
					if (Job != null)
						Job.AddMessage(planName);

					CreatePlan(item, documentFolder);
				}
				catch (Exception ex)
				{
					string eventError = string.Format("Error '{0}' loading {1} <!-- {2} -->.\n",
						ex.Message, planName, ex.ToString().Replace("<!--", "--").Replace("-->", "--"));
					errors.AppendLine(eventError);
					Log.Error(eventError, ex, this.GetType());
				}

				if (Job != null)
					Job.Processed++;
			}

			return importedCount;
		}

		private void CreatePlan(PlanItem item, string documentFolder)
		{
			Language spanishLanguage = Language.Parse("es-mx");

			using (new Sitecore.SecurityModel.SecurityDisabler())
			{
				// See if state is under /sitecore/content/Home/Global/Plans/Regions
				Item stateItem = plansItem.Children.FirstOrDefault(i => i.Name == item.State);
				if (stateItem == null)
					stateItem = CareMoreUtilities.CreateItem(item.State, this.plansItem, this.stateTemplate, true);
				AddSpanishVersion(stateItem.ID, spanishLanguage, item.State);

				// See if county is under /sitecore/content/Home/Global/Plans/Regions/[State]
				Item countyItem = stateItem.Children.FirstOrDefault(i => i.Name == item.County);
				if (countyItem == null)
					countyItem = CareMoreUtilities.CreateItem(item.County, stateItem, this.countyTemplate, true);
				AddSpanishVersion(countyItem.ID, spanishLanguage, "Condado de " + item.County);

				// Check to see if the year item exists in /sitecore/content/Home/Global/Plans/Regions/[State]/[County]
				Item yearItem = countyItem.Children.FirstOrDefault(i => i.Name == item.Year);
				if (yearItem == null)
					yearItem = CareMoreUtilities.CreateItem(item.Year, countyItem, this.yearTemplate, true);
				AddSpanishVersion(yearItem.ID, spanishLanguage);

				// Plan Name = [Plan].Replace("CareMore ", string.Empty)
				string planName = item.Plan.Replace("CareMore ", string.Empty);

				// Check to see if the plan item exists in /sitecore/content/Home/Global/Plans/Regions/[State]/[County]/[Year]
				Item englishPlan = yearItem.Children.FirstOrDefault(i => i.Name == planName);
				if (englishPlan == null)
					englishPlan = CareMoreUtilities.CreateItem(planName, yearItem, planTemplate, true);
				Item spanishPlan = AddSpanishVersion(englishPlan.ID, spanishLanguage);

				// Begin edit
				try
				{
					englishPlan.Editing.BeginEdit();
					CreatePlanEnglish(englishPlan, item, documentFolder);
				}
				finally
				{
					// End edit
					englishPlan.Editing.EndEdit();
				}

				try
				{
					spanishPlan.Editing.BeginEdit();
					CreatePlanSpanish(spanishPlan, item, documentFolder);
				}
				finally
				{
					// End edit
					spanishPlan.Editing.EndEdit();
				}
			}
		}

		private Item AddSpanishVersion(ID itemID, Language spanishLanguage)
		{
			return AddSpanishVersion(itemID, spanishLanguage, null);
		}

		private Item AddSpanishVersion(ID itemID, Language spanishLanguage, string nameValue)
		{
			Item spanishItem = currentDB.GetItem(itemID, spanishLanguage);

			if (spanishItem.Versions.Count == 0)
				spanishItem.Versions.AddVersion();

			if (!string.IsNullOrWhiteSpace(nameValue))
			{
				try
				{
					spanishItem.Editing.BeginEdit();
					spanishItem["Name"] = nameValue;
				}
				finally
				{
					spanishItem.Editing.EndEdit();
				}
			}

			return spanishItem;
		}

		private void CreatePlanCommon(Item planItem, PlanItem item)
		{
			// Plan
			planItem["Plan ID"] = string.Format("{0}-{1}", item.Contract, item.PlanId.PadLeft(3, '0'));

			//TODO: Set Application field
			//Find matching application in /sitecore/content/Home/Global/Plans/Applications
			//Droplink or droplist?



			// Premium
			if (string.IsNullOrWhiteSpace(item.Premium))
				planItem["Premium"] = item.Premium;

			// Payment Info
			if (string.IsNullOrWhiteSpace(item.PaymentInfo))
				planItem["Payment Info"] = item.PaymentInfo;
		}

		private void CreatePlanEnglish(Item planItem, PlanItem item, string documentFolder)
		{
			CreatePlanCommon(planItem, item);

			planItem["Plan Name"] = string.Format("{0} for {1}", item.Plan, item.Year);

			LinkMediaItem(planItem, "Application File",          item.Year, English, "Application",        item.ApplicationEnglish,      @"Application\English",                 documentFolder);
			LinkMediaItem(planItem, "Drug Formulary File",       item.Year, English, "Formulary",          item.DrugEnglish,             @"Drug Formulary\English",              documentFolder);
			LinkMediaItem(planItem, "Evidence of Coverage File", item.Year, English, "EOC",                item.EvidenceCoverageEnglish, @"EOC\English",                         documentFolder);
			LinkMediaItem(planItem, "Pharmacy Directory File",   item.Year, English, "Pharmacy Directory", item.PharmDirEnglish,         @"Provider Pharmacy Directory\English", documentFolder);
			LinkMediaItem(planItem, "Plan Benefit Chart File",   item.Year, English, "Plan Benefit Chart", item.PlanChartEnglish,        @"Plan Benefit Charts\English",         documentFolder);
		}

		private void CreatePlanSpanish(Item planItem, PlanItem item, string documentFolder)
		{
			CreatePlanCommon(planItem, item);

			planItem["Plan Name"] = string.Format("{0} {1}", item.Plan, item.Year);

			LinkMediaItem(planItem, "Application File",          item.Year, Spanish, "Application",        item.ApplicationSpanish,      @"Application\Spanish",                 documentFolder);
			LinkMediaItem(planItem, "Drug Formulary File",       item.Year, Spanish, "Formulary",          item.DrugSpanish,             @"Drug Formulary\Spanish",              documentFolder);
			LinkMediaItem(planItem, "Evidence of Coverage File", item.Year, Spanish, "EOC",                item.EvidenceCoverageSpanish, @"EOC\Spanish",                         documentFolder);
			LinkMediaItem(planItem, "Pharmacy Directory File",   item.Year, Spanish, "Pharmacy Directory", item.PharmDirSpanish,         @"Provider Pharmacy Directory\Spanish", documentFolder);
			LinkMediaItem(planItem, "Plan Benefit Chart File",   item.Year, Spanish, "Plan Benefit Chart", item.PlanChartSpanish,        @"Plan Benefit Charts\Spanish",         documentFolder);
			LinkMediaItem(planItem, "Summary of Benefits File",  item.Year, Spanish, "SB",                 item.SummaryBenefitsSpanish,  @"Summary of Benefits\Spanish",         documentFolder);
		}

		private void LinkMediaItem(Item planItem, string field, string year, string language, string sitecoreSubFolder, string fieldValue, string documentFolder, string physicalSubFolder)
		{
			if (!string.IsNullOrEmpty(fieldValue))
			{
				// Find media item
				string sitecoreFolder = string.Format("/sitecore/media library/{0}/{1}/{2}/", year, language, sitecoreSubFolder);

				Item pdfItem = currentDB.GetItem(sitecoreFolder + fieldValue);
				MediaItem pdfMediaItem = null;

				if (pdfItem == null)
				{
					Item mediaRoot = currentDB.GetItem("/sitecore/media library");

					// Get or create the folder structure
					Item yearItem = CareMoreUtilities.CreateItem(year, mediaRoot, folderItem, true);
					Item langItem = CareMoreUtilities.CreateItem(language, yearItem, folderItem, true);
					Item subItem = CareMoreUtilities.CreateItem(sitecoreSubFolder, langItem, folderItem, true);

					// Build the file path
					string fileName = string.Format(@"{0}\{1}\{2}.pdf", documentFolder, physicalSubFolder, fieldValue);

					if (File.Exists(fileName))
					{
						// Set upload options
						Sitecore.Resources.Media.MediaCreatorOptions options = new Sitecore.Resources.Media.MediaCreatorOptions();
						options.Database = currentDB;
						options.Language = Sitecore.Context.Language;
						options.Versioned = Settings.Media.UploadAsVersionableByDefault;
						options.Destination = sitecoreFolder;
						options.FileBased = Settings.Media.UploadAsFiles;

						// Upload item
						Sitecore.Resources.Media.MediaCreator creator = new Sitecore.Resources.Media.MediaCreator();
						pdfMediaItem = creator.CreateFromFile(fileName, options);
					}
				}
				else
				{
					pdfMediaItem = new MediaItem(pdfItem);
				}

				if (pdfMediaItem == null)
				{
					pdfItem = currentDB.GetItem("/sitecore/media library/Files/Placeholder");
					pdfMediaItem = new MediaItem(pdfItem);
				}

				FileField file = planItem.Fields[field];
				file.MediaID = pdfMediaItem.ID;
				file.Src = Sitecore.Resources.Media.MediaManager.GetMediaUrl(pdfMediaItem);
			}
		}

		private bool IsEmptyRow(OleDbDataReader dr)
		{
			if (dr["State"].ToString().Length == 0 || dr["County"].ToString().Length == 0 || dr["Plan"].ToString().Length == 0)
				return true;
			else
				return false;
		}

		private void SetCoreItemsAndTemplates()
		{
			// Get the basic Folder template
			this.folderItem = currentDB.Templates[ItemIds.GetID("FOLDER_TEMPLATE_ID")];

			// Get configuration from /sitecore/content/Home/Global/Plans/Import
			this.plansItem = currentDB.SelectSingleItem("/sitecore/content/Home/Global/Plans/Regions");

			// Get configuration from /sitecore/content/Home/Global/Plans/Import
			this.configItem = currentDB.SelectSingleItem("/sitecore/content/Home/Global/Plans/Import");

			// Get templates
			this.stateTemplate = currentDB.Templates ["Caremore/Localization/State"];
			this.countyTemplate = currentDB.Templates["Caremore/Localization/County"];
			this.planTemplate = currentDB.Templates  ["Caremore/Plans/Plan"];
			this.yearTemplate = currentDB.Templates  ["Caremore/Plans/Year"];
		}
	}
}
