﻿<%@ Page AutoEventWireup="true" CodeBehind="ChangePwd.aspx.cs" Inherits="Website.sitecore_modules.ChangePwd" Language="C#" MasterPageFile="~/sitecore modules/Site.Master" Title="CareMore > Change Password" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentHeader" runat="server" ContentPlaceHolderID="contentPlaceHolderHeader">
	<div><a id="aLogOff" runat="server">Log Off</a></div>
</asp:Content>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<div>
		<h1 class="heading">Change Password</h1>
	</div>

	<asp:PlaceHolder ID="phMessage" runat="server" Visible="false"></asp:PlaceHolder>

<asp:PlaceHolder ID="phForm" runat="server">
	<asp:ValidationSummary id="valSumPage" runat="server" />

	<table border="0" cellpadding="0" cellspacing="0" class="appForm">
		<tr>
			<td class="label">Login</td>
			<td>
				<asp:Label id="lblLogin" runat="server" />
			</td>
		</tr>
		<tr class="alt">
			<td class="label">Current Password</td>
			<td>
				<asp:TextBox id="txtOldPassword" runat="server" maxlength="20" textmode="Password" width="140" />
				<asp:RequiredFieldValidator id="reqFldValOldPassword" runat="server"
					controltovalidate="txtOldPassword" errormessage="Old Password is required." />
			</td>
		</tr>
		<tr>
			<td class="label">New Password</td>
			<td>
				<asp:TextBox id="txtPassword" runat="server" maxlength="20" textmode="Password" width="140" />
				<asp:RequiredFieldValidator id="reqFldValPassword" runat="server"
					controltovalidate="txtPassword" errormessage="New Password is required." />
				<asp:RegularExpressionValidator id="regExpValPassword" runat="server"
					controltovalidate="txtPassword" errormessage="New Password must be at least 6 characters."
					validationexpression=".{6,}"/>
				<asp:CompareValidator id="comValOldPassword" runat="server"
					controltovalidate="txtPassword" controltocompare="txtOldPassword"
					errormessage="New Password cannot match Current Password."
					operator="NotEqual" />
			</td>
		</tr>
		<tr class="alt">
			<td class="label">Verify New Password</td>
			<td>
				<asp:TextBox id="txtVerifyPassword" runat="server" maxlength="20" textmode="Password" width="140" />
				<asp:RequiredFieldValidator id="reqFldValVerifyPassword" runat="server"
					controltovalidate="txtVerifyPassword"
					errormessage="Verify New Password is required." />
				<asp:CompareValidator id="comValNewPassword" runat="server"
					controltovalidate="txtVerifyPassword" controltocompare="txtPassword"
					errormessage="New Passwords must match."
					operator="Equal" />
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<p><asp:Button ID="btnSubmit" runat="server" OnClick="BtnSubmit_Click" CssClass="orangeSubmitButton" Text="Submit" /></p>
			</td>
		</tr>
	</table>
</asp:PlaceHolder>


</asp:Content>
