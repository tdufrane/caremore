﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security.AntiXss;
using System.Web.UI.WebControls;

namespace Website.sitecore_modules
{
	public class BaseAdminLogin : System.Web.UI.Page
	{
		protected enum AllowTypes
		{
			Broker,
			Enrollment,
			Leads,
			Order,
			Specialty
		}

		protected AllowTypes AllowType { get; set; }
		protected string RedirectPath { get; set; }
		protected string RedirectUrl { get; set; }
		protected string SessionName { get; set; }

		protected void LoadPage(TextBox txtUserName, CheckBox saveLogin)
		{
			Session.Remove(this.SessionName);

			if (Request.Cookies[this.SessionName] != null)
			{
				if (!string.IsNullOrEmpty(Request.Cookies[this.SessionName].Value))
				{
					txtUserName.Text = Request.Cookies[this.SessionName].Value;
					saveLogin.Checked = true;
				}
			}
		}

		protected void CheckLoggedIn()
		{
			object loggedIn = Session[this.AllowType.ToString()];
			if (loggedIn != null && (bool)loggedIn)
				Response.Redirect(this.RedirectUrl, false);
		}

		protected void AuthenticateLogin(string username, string password, CheckBox saveLogin, PlaceHolder phError)
		{
			CaremoreApplicationDataContext dbContext = new CaremoreApplicationDataContext();

			IEnumerable<AdminUser> users =
				from user in dbContext.AdminUsers
				where (user.Username == username)
				select user;

			if (users.Count() == 0)
			{
				CareMoreUtilities.DisplayError(phError, "The User Name you entered is not valid. Please try again.");
			}
			else
			{
				AdminUser user = users.First();

				if (user.Password != password)
				{
					CareMoreUtilities.DisplayError(phError, "The Password you entered is not valid. Please try again.");
				}
				else
				{
					bool isAllowed = false;
					switch (this.AllowType)
					{
						case AllowTypes.Broker:
							isAllowed = user.AllowBroker;
							break;

						case AllowTypes.Enrollment:
							isAllowed = user.AllowEnrollment;
							break;

						case AllowTypes.Leads:
							isAllowed = user.AllowLeads;
							break;

						case AllowTypes.Order:
							isAllowed = user.AllowBroker;
							break;

						case AllowTypes.Specialty:
							isAllowed = user.AllowSpecialty;
							break;

						default:
							break;
					}

					if (!isAllowed)
					{
						CareMoreUtilities.DisplayError(phError, "The User Name you entered does not have access to this application.");
					}
					else
					{
						if (saveLogin.Checked)
						{
							HttpCookie aCookie = new HttpCookie(this.SessionName);
							aCookie.Value = AntiXssEncoder.HtmlEncode(user.Username, false);
							aCookie.Expires = DateTime.Now.AddYears(1);
							Response.Cookies.Add(aCookie);
						}

						Session[this.SessionName] = user.Username;
						Session[this.AllowType.ToString()] = true;
						Session["RedirectPath"] = this.RedirectPath;
						Session["RedirectUrl"] = this.RedirectUrl;

						if (user.PasswordReset)
						{
							Response.Redirect(this.RedirectUrl, false);
						}
						else
						{
							Response.Redirect("~/sitecore%20modules/ChangePwd.aspx", false);
						}
					}
				}
			}
		}
	}
}
