﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Website.sitecore_modules.Brokers.Default"
	MasterPageFile="~/sitecore modules/Site.Master" Title="CareMore > Broker Admin > Home" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<h1 class="heading">Broker Administration</h1>

	<asp:PlaceHolder ID="phStatus" runat="server" Visible="false" />

	<asp:PlaceHolder ID="phContent" runat="server">
		<p>Choose from an item below:</p>

		<ul style="list-style-type:none;">
			<li><a href="BrokerList.aspx?s=p">List pending broker registrations</a>.</li>
			<li><a href="BrokerList.aspx?s=a">List active broker registrations</a>.</li>
			<li><a href="BrokerList.aspx?s=d">List disabled broker registrations</a>.</li>
			<li>Find broker registration(s) <span class="errorMsg">( coming soon )</span>.</li>
			<li>List broker messages <span class="errorMsg">( coming soon )</span>.</li>
			<li>Add a broker message <span class="errorMsg">( coming soon )</span>.</li>
		</ul>
	</asp:PlaceHolder>
</asp:Content>
