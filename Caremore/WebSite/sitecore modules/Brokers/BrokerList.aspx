﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BrokerList.aspx.cs" Inherits="Website.sitecore_modules.Brokers.BrokerList"
	MasterPageFile="~/sitecore modules/Site.Master" Title="CareMore > Broker Admin > Broker List" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentHeader" runat="server" ContentPlaceHolderID="contentPlaceHolderHeader">
	<script type="text/javascript" src="../../js/checkbox.js"></script>
</asp:Content>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<h1 class="heading">Broker List</h1>

	<asp:PlaceHolder ID="phStatus" runat="server"></asp:PlaceHolder>

<asp:PlaceHolder ID="phForm" runat="server">
	<asp:HiddenField ID="hidListType" runat="server" />
	<p><asp:ValidationSummary id="valSumPage" runat="server" HeaderText="Please correct the following fields:" /></p>

	<table border="0" cellspacing="0">
		<tr>
			<td class="listButtons">
				<asp:Button ID="btnShowAll" runat="server" OnClick="BtnShowAll_Click" Text="Show All" />
				<label> | </label>
				<asp:Button ID="btnApproved" runat="server" OnClick="BtnShowApproved_Click" Text="Approved"/>
				<label> | </label>
				<asp:Button ID="btnPending" runat="server" OnClick="BtnShowPending_Click" Text="Pending"/>
				<label> | </label>
				<asp:Button ID="btnDisabled" runat="server" OnClick="BtnShowDisabled_Click" Text="Disabled"/>
				<label> | </label>
				<input id="chkAllItems" type="button" onclick="CheckAllCheckBoxes('cbApprove', 1)"  value="Check All" />
				<label> | </label>
				<input id="unchkAllItems" type="button" onclick="CheckAllCheckBoxes('cbApprove', 0)" value="Uncheck All" />
			</td>
		</tr>
		<tr>
			<td>
				<asp:GridView ID="gvBrokers" runat="server"
					AutoGenerateColumns="false" DataKeyNames="RegistrationId"
					AllowPaging="true" AllowSorting="true" CssClass="appList" DataSourceID="ldsBrokers"
					PageSize="20">
					<AlternatingRowStyle CssClass="alt" />
					<EmptyDataRowStyle CssClass="errorMsg" />
					<FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
					<HeaderStyle CssClass="gridHeader" />
					<PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
					<Columns>
						<asp:TemplateField HeaderText="Select">
							<ItemTemplate>
								<asp:CheckBox ID="cbApprove" runat="server" />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:HyperLinkField DataNavigateUrlFields="RegistrationId" DataNavigateUrlFormatString="BrokerDetails.aspx?id={0}"
							DataTextField="CareMoreId" HeaderText="CareMore ID" SortExpression="CareMoreId" />
						<asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
						<asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
						<asp:BoundField DataField="City" HeaderText="City" />
						<asp:BoundField DataField="State" HeaderText="State" SortExpression="State" />
						<asp:BoundField DataField="Email" HeaderText="Email" />
						<asp:BoundField DataField="CreatedOn" DataFormatString="{0:MM/dd/yy HH:mm}" HeaderText="Created Date" SortExpression="CreatedOn" />
						<asp:BoundField DataField="UpdatedOn" DataFormatString="{0:MM/dd/yy HH:mm}" HeaderText="Updated Date" SortExpression="UpdatedOn" />
						<asp:TemplateField HeaderText="Status">
							<ItemTemplate>
								<%# Status((bool)Eval("Verified"), (bool)Eval("Disabled")) %>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
				<br />
			</td>
		</tr>
		<tr>
			<td class="listButtons">
				<asp:Button ID="btnApproveSelected" runat="server" Text="Approve Checked" OnClick="BtnApproveSelected_Click"></asp:Button>
				<label> | </label>
				<asp:Button ID="btnDisableSelected" runat="server" Text="Disable Checked" OnClick="BtnDisableSelected_Click"></asp:Button>
				<label> | </label>
				<asp:Button ID="btnEnableSelected" runat="server" Text="Enable Checked" OnClick="BtnEnableSelected_Click"></asp:Button>
				<label> | </label>
				<asp:Button ID="btnDeleteSelected" runat="server" Text="Delete Checked" OnClick="BtnDeleteSelected_Click"></asp:Button>
			</td>
		</tr>
	</table>

	<asp:LinqDataSource ID="ldsBrokers" runat="server" ContextTypeName="Website.CaremoreApplicationDataContext"
		Select="new (RegistrationId, CareMoreId, LastName, FirstName, City, State, Email, CreatedOn, UpdatedOn, Verified, Disabled)"
		TableName="BrokerRegistrations" AutoGenerateWhereClause="true" />

</asp:PlaceHolder>

	<p><br />Back to <a href="Default.aspx">Broker Administration Home</a></p>
</asp:Content>
