﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security.AntiXss;
using System.Web.UI;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom;
using Sitecore.Data.Items;

namespace Website.sitecore_modules.Brokers
{
	public partial class BrokerDetails : System.Web.UI.Page
	{
		#region Page events

		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				if (!Sitecore.Context.IsLoggedIn)
				{
					phForm.Visible = false;
					CareMoreUtilities.DisplayError(phStatus, Master.NotAuthorized);
				}
				else if ((!Sitecore.Context.User.IsAdministrator) && (!Sitecore.Context.User.IsInRole("sitecore\\Broker Users")))
				{
					phForm.Visible = false;
					CareMoreUtilities.DisplayError(phStatus, Master.NotAuthorized);
				}
				else if (IsPostBack)
				{
					CareMoreUtilities.ClearStatus(phStatus);
				}
				else
				{
					if (!string.IsNullOrEmpty(Request.QueryString["s"]))
					{
						hlBackTo.NavigateUrl += Request.QueryString["s"];
					}

					LoadBroker(Request.QueryString["id"]);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phStatus, ex);
				phForm.Visible = false;
			}
		}

		protected void BtnApprove_Click(object sender, EventArgs e)
		{
			try
			{
				Item brokerRegItem = ItemIds.GetItem("BROKER_REGISTRATION");
				BrokerAdminHelper.ApproveBroker(lblRegistrationId.Text, Sitecore.Context.User.Name, brokerRegItem);
				CareMoreUtilities.DisplayStatus(phStatus, "Broker registration has been Approved.");
				LoadBroker(lblRegistrationId.Text);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phStatus, ex);
			}
		}

		protected void BtnDisable_Click(object sender, EventArgs e)
		{
			try
			{
				BrokerAdminHelper.DisableBroker(lblRegistrationId.Text, Sitecore.Context.User.Name);
				CareMoreUtilities.DisplayStatus(phStatus, "Broker registration has been Disabled.");
				LoadBroker(lblRegistrationId.Text);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phStatus, ex);
			}
		}

		protected void BtnDelete_Click(object sender, EventArgs e)
		{
			try
			{
				BrokerAdminHelper.DeleteBroker(lblRegistrationId.Text);
				CareMoreUtilities.DisplayStatus(phStatus, "Broker registration has been Deleted.");
				phForm.Visible = false;
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phStatus, ex);
			}
		}

		protected void BtnEnable_Click(object sender, EventArgs e)
		{
			try
			{
				BrokerAdminHelper.EnableBroker(lblRegistrationId.Text, Sitecore.Context.User.Name);
				CareMoreUtilities.DisplayStatus(phStatus, "Broker registration has been Enabled.");
				LoadBroker(lblRegistrationId.Text);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phStatus, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadBroker(string id)
		{
			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			BrokerRegistration profile = BrokerAdminHelper.GetBroker(db, id);

			if (profile == null)
			{
				CareMoreUtilities.DisplayError(phStatus, "Broker registration id not found.");
				phForm.Visible = false;
			}
			else
			{
				LoadRegistration(profile);
			}
		}

		private void LoadRegistration(BrokerRegistration profile)
		{
			lblRegistrationId.Text = profile.RegistrationId.ToString();
			lblCareMoreId.Text = AntiXssEncoder.HtmlEncode(profile.CareMoreId, false);
			lblLastName.Text = AntiXssEncoder.HtmlEncode(profile.LastName, false);
			lblFirstName.Text = AntiXssEncoder.HtmlEncode(profile.FirstName, false);
			lblMiddleInitial.Text = AntiXssEncoder.HtmlEncode(profile.MiddleInitial, false);
			lblSalutation.Text = AntiXssEncoder.HtmlEncode(profile.Salutation, false);
			lblPhone.Text = AntiXssEncoder.HtmlEncode(profile.Phone, false);
			lblAddress.Text = AntiXssEncoder.HtmlEncode(profile.Address, false);
			lblCity.Text = AntiXssEncoder.HtmlEncode(profile.City, false);
			lblState.Text = AntiXssEncoder.HtmlEncode(profile.State, false);
			lblZip.Text = AntiXssEncoder.HtmlEncode(profile.Zip, false);
			lblEmail.Text = AntiXssEncoder.HtmlEncode(profile.Email, false);
			lblVerified.Text = profile.Verified.ToString();
			lblDisabled.Text = profile.Disabled.ToString();
			lblCreatedOn.Text = profile.CreatedOn.ToString("MM/dd/yyyy hh:mm tt");
			lblUpdatedOn.Text = profile.UpdatedOn.HasValue ? profile.UpdatedOn.Value.ToString("MM/dd/yyyy hh:mm tt") : string.Empty;
			lblUpdatedBy.Text = string.IsNullOrEmpty(profile.UpdatedBy) ? string.Empty : profile.UpdatedBy;

			if (profile.Verified)
			{
				ShowVerified(false);
				ShowDisabled(!profile.Disabled);
				ShowEnabled(profile.Disabled);
			}
			else
			{
				ShowVerified(true);
				ShowDisabled(false);
				ShowEnabled(false);
			}
		}

		private void ShowVerified(bool visible)
		{
			btnApprove.Visible = visible;
			lblSeparator1.Visible = visible;
		}

		private void ShowDisabled(bool visible)
		{
			btnDisable.Visible = visible;
			lblSeparator2.Visible = visible;
		}

		private void ShowEnabled(bool visible)
		{
			btnEnable.Visible = visible;
			lblSeparator3.Visible = visible;
		}

		#endregion
	}
}
