﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Website.sitecore_modules.Brokers
{
	public partial class Default : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Sitecore.Context.IsLoggedIn)
			{
				phContent.Visible = false;
				CareMoreUtilities.DisplayError(phStatus, Master.NotAuthorized);
			}
			else if ((!Sitecore.Context.User.IsAdministrator) && (!Sitecore.Context.User.IsInRole("sitecore\\Broker Users")))
			{
				phContent.Visible = false;
				CareMoreUtilities.DisplayError(phStatus, Master.NotAuthorized);
			}
		}
	}
}