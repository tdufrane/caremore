﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sitecore modules/Site.Master" AutoEventWireup="true" CodeBehind="MassUpdate.aspx.cs" Inherits="Website.sitecore_modules.Support.MassUpdate" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<h1>Mass Update</h1>

	<asp:PlaceHolder ID="phForm" runat="server">
		<asp:ValidationSummary id="valSumPage" runat="server" HeaderText="Please correct the following fields:" />

		<div class="form form-horizontal">
			<div class="form-group">
				<label class="col-xs-12 col-md-2 control-label">Sitecore Path</label>
				<div class="col-xs-12 col-md-7">
					<asp:TextBox ID="txtSitecorePath" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3" />
				</div>
				<div class="col-xs-12 col-md-3">
					<asp:RequiredFieldValidator ID="reqFldValSitecorePath" runat="server"
						ControlToValidate="txtSitecorePath" ErrorMessage="Sitecore Path is required."
						Display="Dynamic" Text="*" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-12 col-md-2 control-label">Matches Template Id</label>
				<div class="col-xs-12 col-md-7">
					<asp:TextBox ID="txtTemplateId" runat="server" CssClass="form-control" />
				</div>
				<div class="col-xs-12 col-md-3">
					<asp:RegularExpressionValidator ID="regExValTemplateId" runat="server"
						ControlToValidate="txtTemplateId" ErrorMessage="Matches Template Id must be a GUID"
						ValidationExpression="\{[A-Z0-9]{8}(-[A-Z0-9]{4}){3}-[A-Z0-9]{12}\}"
						Display="Dynamic" Text="*" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-12 col-md-2 control-label">Matches Field Id</label>
				<div class="col-xs-12 col-md-7">
					<asp:TextBox ID="txtFieldId" runat="server" CssClass="form-control" />
				</div>
				<div class="col-xs-12 col-md-3">
					<asp:RegularExpressionValidator ID="regExValFieldId" runat="server"
						ControlToValidate="txtFieldId" ErrorMessage="Matches Field Id must be a GUID"
						ValidationExpression="\{[A-Z0-9]{8}(-[A-Z0-9]{4}){3}-[A-Z0-9]{12}\}"
						Display="Dynamic" Text="*" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-12 col-md-2 control-label">Look At</label>
				<div class="col-xs-12 col-md-7">
					<asp:RadioButtonList ID="rblLookAt" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Table">
						<asp:ListItem Selected="True" Text="Field Value" Value="0" />
						<asp:ListItem Text="Item Name" Value="1" />
					</asp:RadioButtonList>
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-12 col-md-2 control-label">Find</label>
				<div class="col-xs-12 col-md-7">
					<div class="row">
						<div class="col-xs-12">
							<asp:RadioButtonList ID="rblFindMethod" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Table">
								<asp:ListItem Selected="True" Text="Exact Match" Value="0" />
								<asp:ListItem Text="Contains Text" Value="1" />
								<asp:ListItem Text="Regular Expression" Value="2" />
							</asp:RadioButtonList>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<asp:TextBox ID="txtFindValue" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3" />
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-3">
					<asp:RequiredFieldValidator ID="reqFldFindValue" runat="server"
						ControlToValidate="txtFindValue" ErrorMessage="Find Value is required."
						Display="Dynamic" Text="*" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-12 col-md-2 control-label">Action</label>
				<div class="col-xs-12 col-md-7">
					<asp:RadioButtonList ID="rblAction" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Table">
						<asp:ListItem Text="Delete" Value="0" />
						<asp:ListItem Text="Replace Found Value" Value="1" Selected="True" />
						<asp:ListItem Text="Replace All" Value="2" />
						<asp:ListItem Text="Copy to Spanish" Value="3" />
					</asp:RadioButtonList><br />
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-12 col-md-2 control-label">Replace With</label>
				<div class="col-xs-12 col-md-7">
					<asp:TextBox ID="txtReplaceWithValue" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-12 col-md-2 control-label">Preview</label>
				<div class="col-xs-12 col-md-7">
					<asp:CheckBox ID="chkPreview" runat="server" CssClass="radio" Checked="true" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12 col-md-offset-2 col-md-7">
					<asp:Button ID="btnProcess" runat="server" CssClass="btn btn-primary" Text="Process" OnClick="BtnProcess_Click" />
				</div>
			</div>
		</div>

	</asp:PlaceHolder>

	<asp:PlaceHolder ID="phStatus" runat="server" Visible="false" />
</asp:Content>
