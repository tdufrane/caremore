﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using Sitecore.Collections;
using Sitecore.Data;
using Sitecore.Data.Items;
using Website.sitecore_modules.Developer;

namespace Website.sitecore_modules.Support
{
	public partial class ValidateContent : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Sitecore.Context.IsLoggedIn)
			{
				phForm.Visible = false;
				CareMoreUtilities.DisplayError(phStatus, "You are not authorized to access this page.");
			}
			else if (!IsPostBack)
			{
				ChildList content = Sitecore.Context.Database.GetItem("/sitecore/content").Children;

				foreach (Item child in content)
				{
					ddlItems.Items.Add(new ListItem(child.Name, child.Paths.FullPath));
				}

				this.txtFullPath.Attributes.Add("onfocus", "this.blur();");
			}
		}

		protected void DdlItems_SelectedIndexChanged(object sender, EventArgs e)
		{
			gvValidated.Visible = false;

			if (ddlItems.SelectedIndex < 1)
			{
				plcPath.Visible = false;
			}
			else
			{
				plcPath.Visible = true;

				Item itemRoot = Sitecore.Context.Database.GetItem(ddlItems.SelectedItem.Value);
				this.aFullPath.HRef = string.Format("javascript:window.open('../PathPicker.aspx?root={0}&control={1}','Picker');void(0);",
					Server.UrlEncode(ddlItems.SelectedValue), this.txtFullPath.ClientID);
			}
		}

		protected void BtnValidate_Click(object sender, EventArgs e)
		{
			Developer.Data data = new Developer.Data();
			List<Developer.CheckDataItem> items = data.InvalidRichText(txtFullPath.Text, chkRecursive.Checked);

			gvValidated.Visible = true;
			gvValidated.DataSource = items;
			gvValidated.DataBind();
		}

		protected string GetValidatedText(object dataItem)
		{
			CheckDataItem item = (CheckDataItem)dataItem;
			StringBuilder text = new StringBuilder();

			string languageBreak = string.Empty;

			if (!string.IsNullOrEmpty(item.ErrorsEnglish))
			{
				text.Append(GetValidatedText("English", item.Value, item.ErrorsEnglish, string.Empty));
				languageBreak = "<br/><br/>";
			}

			if (!string.IsNullOrEmpty(item.ErrorsSpanish))
			{
				text.Append(GetValidatedText("Spanish", item.ValueSpanish, item.ErrorsSpanish, languageBreak));
			}

			if (text.Length == 0)
				return "Valid";
			else
				return text.ToString();
		}

		private string GetValidatedText(string language, string value, string errors, string languageBreak)
		{
			StringBuilder html = new StringBuilder();
			bool prevWasSpace = false;
			int newLineCount = 0;
			bool wasTruncated = false;

			foreach (char character in value)
			{
				switch (character)
				{
					case '\'':
						prevWasSpace = false;
						html.Append("\\'");
						break;

					case ' ':
						if (!prevWasSpace)
							html.Append(" ");

						prevWasSpace = true;
						break;

					case '\n':
						prevWasSpace = false;
						html.Append("\\n");
						newLineCount++;
						break;

					case '\r':
						prevWasSpace = false;
						break;

					default:
						prevWasSpace = false;
						html.Append(character);
						break;
				}


				// Don't display more than
				if (newLineCount > 15)
				{
					wasTruncated = true;
					break;
				}
				else if (html.Length >= 1000)
				{
					wasTruncated = true;
					break;
				}
			}

			// Don't end with a trailing slash
			if (html[html.Length - 1] == '\\')
				html.Length -= 1;

			// Add ellipse to indicate there was more
			if (wasTruncated)
			{
				html.Append("...");
			}

			return string.Format("{0}<a href=\"javascript:alert('{1}');\"><b>{2}</b>:</a><br/>&bull; {3}",
				languageBreak, Server.HtmlEncode(html.ToString()),
				language, errors.Replace("\n", "<br/>&bull; "));
		}
	}
}
