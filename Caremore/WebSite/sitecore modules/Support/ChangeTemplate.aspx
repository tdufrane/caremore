﻿<%@ Page Title="Mass Change Template" Language="C#" MasterPageFile="~/sitecore modules/Site.Master" AutoEventWireup="true" CodeBehind="ChangeTemplate.aspx.cs" Inherits="Website.sitecore_modules.Support.ChangeTemplate" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentPage" ContentPlaceHolderID="contentPlaceHolderRoot" runat="server">
	<h1>Mass Change Template</h1>

	<asp:PlaceHolder ID="phForm" runat="server">
		<asp:ValidationSummary id="valSumPage" runat="server" CssClass="text-danger" />

		<div class="form form-horizontal">
			<div class="form-group">
				<label class="col-xs-12 col-md-2 control-label">Root Path:</label>
				<div class="col-xs-12 col-md-7">
					<asp:TextBox ID="txtRootPath" runat="server" CssClass="form-control" />
				</div>
				<div class="col-xs-12 col-md-3">
					<asp:RequiredFieldValidator ID="reqFldValRootPath" runat="server" ControlToValidate="txtRootPath"
						ErrorMessage="Root Path is required." Display="Dynamic" Text="*" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-12 col-md-2 control-label">Old Template Path:</label>
				<div class="col-xs-12 col-md-7">
					<asp:TextBox ID="txtOldTemplate" runat="server" CssClass="form-control" />
				</div>
				<div class="col-xs-12 col-md-3">
					<asp:RequiredFieldValidator ID="reqFldValOldTemplate" runat="server" ControlToValidate="txtOldTemplate"
						ErrorMessage="Old Template Path is required." Display="Dynamic" Text="*" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-12 col-md-2 control-label">New Template Path:</label>
				<div class="col-xs-12 col-md-7">
					<asp:TextBox ID="txtNewTemplate" runat="server" CssClass="form-control" />
				</div>
				<div class="col-xs-12 col-md-3">
					<asp:RequiredFieldValidator ID="reqFldValNewTemplate" runat="server" ControlToValidate="txtNewTemplate"
						ErrorMessage="New Template Path is required." Display="Dynamic" Text="*" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-12 col-md-2 control-label">Preview</label>
				<div class="col-xs-12 col-md-7">
					<asp:CheckBox ID="chkPreview" runat="server" CssClass="radio" Checked="true" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12 col-md-offset-2 col-md-7">
					<asp:Button ID="btnReplace" runat="server" CssClass="btn btn-primary" OnClick="BtnReplace_Click" Text="Change" />
				</div>
			</div>
		</div>
	</asp:PlaceHolder>

	<asp:PlaceHolder ID="phStatus" runat="server" Visible="false" />
</asp:Content>
