﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Security;
using Sitecore.Security.Accounts;
using Sitecore.Collections;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;
using Sitecore.Security.AccessControl;

namespace Website.sitecore_modules.Support
{
	public partial class ChangeTemplate : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Sitecore.Context.IsLoggedIn)
			{
				phForm.Visible = false;
				CareMoreUtilities.DisplayError(phStatus, "You are not authorized to access this page as you need to logon to Sitecore first.");
			}
			else if (!Sitecore.Context.User.IsAdministrator)
			{
				phForm.Visible = false;
				CareMoreUtilities.DisplayError(phStatus, "You are not authorized to access this page as you are not assigned as an 'Administrator'.");
			}
			else if (IsPostBack)
			{
				phStatus.Controls.Clear();
				phStatus.Visible = false;
			}
		}

		protected void BtnReplace_Click(object sender, EventArgs e)
		{
			try
			{
				Database masterDB = Sitecore.Configuration.Factory.GetDatabase("master");
				Item oldTemplateItem = masterDB.SelectSingleItem(txtOldTemplate.Text);
				Item newTemplateItem = masterDB.SelectSingleItem(txtNewTemplate.Text);

				if (oldTemplateItem == null)
				{
					CareMoreUtilities.DisplayError(phStatus, "Old Template Path does not exist.");
				}
				else if (newTemplateItem == null)
				{
					CareMoreUtilities.DisplayError(phStatus, "New Template Path does not exist.");
				}
				else
				{
					MassItemProcessing itemProcessing = new MassItemProcessing();
					itemProcessing.SearchPath = txtRootPath.Text;
					itemProcessing.TemplateItem = oldTemplateItem;
					itemProcessing.NewTemplateItem = newTemplateItem;
					itemProcessing.LookAt = MassItemProcessing.LookAtTypes.Template;

					itemProcessing.Preview = chkPreview.Checked;
					itemProcessing.ProcessItems();

					CareMoreUtilities.DisplayStatus(phStatus, itemProcessing.Results(Server), Master.SuccessClass);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phStatus, ex);
			}
		}
	}
}
