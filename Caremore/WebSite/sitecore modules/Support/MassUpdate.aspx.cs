﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Collections;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Website.sitecore_modules.Support
{
	public partial class MassUpdate : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Sitecore.Context.IsLoggedIn)
			{
				phForm.Visible = false;
				CareMoreUtilities.DisplayError(phStatus, "You must login to Sitecore to access this page.");
			}
			else if (!Sitecore.Context.User.IsAdministrator)
			{
				phForm.Visible = false;
				CareMoreUtilities.DisplayError(phStatus, "You are not authorized to access this page.");
			}
			else if (IsPostBack)
			{
				phStatus.Controls.Clear();
				phStatus.Visible = false;
			}
		}

		protected void BtnProcess_Click(object sender, EventArgs e)
		{
			try
			{
				ProcessItems();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phStatus, ex);
			}
		}

		private void ProcessItems()
		{
			Database masterDB = Sitecore.Configuration.Factory.GetDatabase("master");
			MassItemProcessing itemProcessing = new MassItemProcessing();
			itemProcessing.SearchPath = txtSitecorePath.Text;

			if (!string.IsNullOrEmpty(txtTemplateId.Text))
				itemProcessing.TemplateItem = masterDB.GetItem(txtTemplateId.Text);

			if (!string.IsNullOrEmpty(txtFieldId.Text))
			{
				Guid fieldId;
				if (Guid.TryParse(txtFieldId.Text, out fieldId))
					itemProcessing.FieldItem = masterDB.GetItem(txtFieldId.Text);
				else
					itemProcessing.FieldName = txtFieldId.Text;
			}

			if (rblLookAt.SelectedValue.Equals("0"))
				itemProcessing.LookAt = MassItemProcessing.LookAtTypes.FieldValue;
			else
				itemProcessing.LookAt = MassItemProcessing.LookAtTypes.ItemName;

			if (rblFindMethod.SelectedValue.Equals("0"))
				itemProcessing.FindMethod = MassItemProcessing.FindMethods.ExactMatch;
			else if (rblFindMethod.SelectedValue.Equals("1"))
				itemProcessing.FindMethod = MassItemProcessing.FindMethods.ContainsText;
			else
				itemProcessing.FindMethod = MassItemProcessing.FindMethods.RegularExpression;

			itemProcessing.FindText = txtFindValue.Text;

			if (rblAction.SelectedValue.Equals("0"))
				itemProcessing.Action = MassItemProcessing.Actions.DeleteItem;
			else if (rblAction.SelectedValue.Equals("1"))
				itemProcessing.Action = MassItemProcessing.Actions.ReplaceFoundValue;
			else if (rblAction.SelectedValue.Equals("2"))
				itemProcessing.Action = MassItemProcessing.Actions.ReplaceWholeValue;
			else
				itemProcessing.Action = MassItemProcessing.Actions.CopyToSpanish;

			itemProcessing.ReplaceWith = txtReplaceWithValue.Text;
			itemProcessing.Preview = chkPreview.Checked;
			itemProcessing.ProcessItems();

			CareMoreUtilities.DisplayStatus(phStatus, "div", itemProcessing.Results(Server), Master.SuccessClass, false);
		}
	}
}
