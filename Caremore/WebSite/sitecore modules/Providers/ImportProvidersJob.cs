﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using CareMore.Web.DotCom;
using Sitecore;
using Sitecore.Collections;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;
using Sitecore.Jobs;
using Sitecore.Security.Accounts;
using Sitecore.Security.AccessControl;

namespace Website.sitecore_modules.Providers
{
	public class ImportProvidersJob
	{
		public string JobName = "Import Providers";
		public StringBuilder Errors = new StringBuilder();

		private Database currentDB = Factory.GetDatabase("master");
		private TemplateItem folderItem = null;

		public Job Job
		{
			get
			{
				return JobManager.GetJob(JobName);
			}
		}

		public void StartJob(string fileName, string sheetName, string facilitiesDbPath, Guid templateId)
		{
			JobOptions options = new JobOptions(JobName,
				"Admin",
				Context.Site.Name,
				this,
				"ImportData",
				new object[] { fileName, sheetName, facilitiesDbPath, templateId });

			JobManager.Start(options);
		}

		public void ImportData(string fileName, string sheetName, string facilitiesDbPath, Guid templateId)
		{
			int count = 0, importedCount = 0;
			DateTime started = DateTime.Now;

			try
			{
				if (Job != null)
					Job.Status.State = JobState.Running;

				string connectionString = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;IMEX=1;HDR=YES;""", fileName);
				OleDbConnection objConn = new OleDbConnection(connectionString);

				string strSql = string.Format("SELECT [NPI], [TYPE], [SUBTYPE], [PROVIDER FNAME], [PROVIDER LNAME], [TITLE], [PRACTICE NAME], [OFFICE ID], [OFFICE ADDRESS], [ADDRESS2], [OFFICE CITY], [COUNTY], [ST], [ZIP], [OFFICE PHONE], [FAX NUMBER], [HANDICAP ACCESS], [ACCEPTING NEW PATIENTS], [OFFICE MON HOURS], [OFFICE TUE HOURS], [OFFICE WED HOURS], [OFFICE THU HOURS], [OFFICE FRI HOURS], [OFFICE SAT HOURS], [OFFICE SUN HOURS], [OFFICE LANG 1], [OFFICE LANG 2], [OFFICE LANG 3], [OFFICE LANG 4], [EXAM ROOM HANDICAP ACCESS], [EXTERNAL BUILDING HANDICAP ACCESS], [INTERNAL BUILDING HANDICAP ACCESS], [PARKING HANDICAP ACCESS], [RESTROOM HANDICAP ACCESS], [PUBLIC TRANSPORTATION HANDICAP ACCESS], [DISABILITY ACCESS], [CULTURAL COMPETENCY], [WHEELCHAIR WEIGHT SCALE], [SPECIALIZED TRAINING] FROM [{0}$] WHERE [NPI] IS NOT NULL ORDER BY [OFFICE CITY], [PRACTICE NAME]", sheetName);
				OleDbCommand objCmd = new OleDbCommand(strSql, objConn);

				objConn.Open();
				List<ProviderItem> list = new List<ProviderItem>();
				using (OleDbDataReader dr = objCmd.ExecuteReader())
				{
					while (dr.Read())
					{
						if (Job != null)
							Job.AddMessage("Reading row {0}", count + 1);

						if (!IsEmptyRow(dr))
						{
							ProviderItem item = new ProviderItem(dr);

							if (string.IsNullOrWhiteSpace(item.ReaderError))
							{
								list.Add(item);
							}
							else
							{
								Errors.AppendLine(item.ReaderError);
							}

							count++;
						}
					}

					dr.Close();
				}

				objConn.Close();

				importedCount = CreateItems(list, Errors, facilitiesDbPath, templateId);
			}
			catch (Exception ex)
			{
				if (Job != null)
					Job.Status.State = JobState.Unknown;

				Errors.AppendLine(ex.ToString());
				Log.Error("ImportProvidersJob.ImportData Exception", ex, this.GetType());
			}
			finally
			{
				if (Job != null)
					Job.Status.State = JobState.Finished;

				StringBuilder message = new StringBuilder();
				message.AppendFormat("<p><strong>{0}</strong> providers were read. <span style=\"color:Blue;\">{1}</span> providers have been successfully imported.</p>",
					count, importedCount);

				if (Errors.Length > 0)
				{
					Errors.Length -= 1; // Remove trailing \n

					message.AppendFormat("<p><strong>Errors:</strong><br />{0}</p>", Errors.ToString().Replace("\n", "<br />"));
				}

				TimeSpan processTime = (DateTime.Now - started);
				message.AppendFormat("<p>Processing time was {0} minutes, {1} seconds.</p>",
					processTime.Minutes, processTime.Seconds);

				if (Job != null)
					Job.AddMessage(message.ToString());
			}
		}

		private int CreateItems(List<ProviderItem> list, StringBuilder errors, string facilitiesDbPath, Guid templateId)
		{
			int itemCount = 0, importedCount = 0;

			Item providerFolder = currentDB.GetItem(facilitiesDbPath);
			TemplateItem itemTemplate = currentDB.GetItem(new ID(templateId));
			folderItem = currentDB.Templates[ItemIds.GetID("FOLDER_TEMPLATE_ID")];

			if (providerFolder == null)
			{
				string providerError = string.Format("Provider Item not found '{0}'", facilitiesDbPath);
				errors.AppendLine(providerError);
				Log.Error(providerError, this.GetType());
			}
			else if (itemTemplate == null)
			{
				string templateError = string.Format("Template Item '{0}' not found", templateId);
				errors.AppendLine(templateError);
				Log.Error(templateError, this.GetType());
			}
			else
			{
				AccessRuleCollection accessRules = null;// providerRootItem.Security.GetAccessRules();
				string providerName = "start of list";

				try
				{
					foreach (ProviderItem item in list)
					{
						itemCount++;

						providerName = string.Format("Provider #{0}: {1}, {2}",
							itemCount, item.ProviderName, item.City);

						if (Job != null)
							Job.AddMessage(providerName);

						CreateProviderItem(providerFolder, itemTemplate, item, accessRules);

						importedCount++;
					}
				}
				catch (Exception ex)
				{
					string eventError = string.Format("Error '{0}' loading {1} <!-- {2} -->.\n",
						ex.Message, providerName, ex.ToString().Replace("<!--", "--").Replace("-->", "--"));
					errors.AppendLine(eventError);
					Log.Error(eventError, ex, this.GetType());
				}

				if (Job != null)
					Job.Processed++;
			}

			return importedCount;
		}

		private void CreateProviderItem(Item parent, TemplateItem itemTemplate, ProviderItem item, AccessRuleCollection accessRules)
		{
			Item cityItem = CareMoreUtilities.CreateItem(item.City, parent, folderItem, true, accessRules);
			Item providerItem;

			if (string.IsNullOrWhiteSpace(item.Location))
				providerItem = CareMoreUtilities.CreateItem(item.ProviderName, cityItem, itemTemplate, "Provider ID", item.ProviderID);
			else
				providerItem = CareMoreUtilities.CreateItem(item.Location, cityItem, itemTemplate, "Provider ID", item.ProviderID);

			using (new Sitecore.SecurityModel.SecurityDisabler())
			{
				providerItem.Editing.BeginEdit();

				try
				{
					// Contact Information
					AddToField(providerItem, "Phone", item.Phone);
					AddToField(providerItem, "Fax", item.Fax);
					AddToField(providerItem, "Place", item.Location);
					AddToField(providerItem, "Address Line 1", item.Address1);
					AddToField(providerItem, "Address Line 2", item.Address2);
					AddToField(providerItem, "City", item.City);
					providerItem.Fields["State"].Value = GetState(item.State);
					AddToField(providerItem, "Postal Code", item.ZipCode);
					AddToField(providerItem, "County", item.County);

					// Provider Data
					AddToField(providerItem, "Provider ID", item.ProviderID);
					AddToField(providerItem, "NPI ID", item.ProviderNpiID);
					AddToField(providerItem, "Provider Name", item.ProviderName);

					AddToField(providerItem, "Subtype", item.ProviderSubType);

					if (item.Accessibility.Count == 0)
						AddToField(providerItem, "Accessibility", "N/A");
					else
						AddToField(providerItem, "Accessibility", "SR-Limited: " + string.Join(", ", item.Accessibility.ToArray()));

					AddToField(providerItem, "Hours of Operation", item.HoursOfOperation);
					AddToField(providerItem, "Accepting New Patients", item.AcceptingNewPatients ? "1" : string.Empty);
					AddToField(providerItem, "Languages",  string.Join(", ", item.Languages.ToArray()));
					AddToField(providerItem, "Transportation", item.Transportation ? "1" : string.Empty);
					AddToField(providerItem, "Cultural Competency", item.CulturalCompetency ? "1" : string.Empty);
					AddToField(providerItem, "Other Credentials", string.Empty);
					AddToField(providerItem, "Specialized Training", item.SpecializedTraining ? "1" : string.Empty);

					//// Practioners only
					if (providerItem.Fields["Title"] != null)
						AddToField(providerItem, "Title", item.Title);
				}
				finally
				{
					providerItem.Editing.EndEdit();
				}
			}
		}

		private void AddToField(Item item, string fieldName, string fieldValue)
		{
			if (string.IsNullOrWhiteSpace(fieldValue))
				item.Fields[fieldName].Value = string.Empty;
			else
				item.Fields[fieldName].Value = fieldValue;
		}

		private bool IsEmptyRow(OleDbDataReader dr)
		{
			if (dr["PROVIDER FNAME"].ToString().Length == 0 || dr["PROVIDER LNAME"].ToString().Length == 0 || dr["OFFICE CITY"].ToString().Length == 0)
				return true;
			else
				return false;
		}

		private string GetState(string stateCode)
		{
			Item stateFolderItem = ItemIds.GetItem("STATE_SETTINGS_ID");

			Item stateItem = stateFolderItem.Children.Where(i => i["Code"] == stateCode).FirstOrDefault();
			//{9CC475DE-25A2-4045-94A5-711459727A5A}
			if (stateItem == null)
				return stateCode;
			else
				return stateItem["Name"];
		}
	}
}

