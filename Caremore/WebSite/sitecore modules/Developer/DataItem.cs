﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Website.sitecore_modules.Developer
{
    [Serializable]
    public class DataItem
    {
        #region Constructors

        public DataItem()
        {
        }

        public DataItem(string path, string field, string value)
        {
            Path = path;
            Field = field;
            Value = value;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Sitecore Item Path
        /// </summary>
        private string _path;
        public string Path
        {
            get { return this._path; }
            set { this._path = value; }
        }

        /// <summary>
        /// Sitecore Item Data Field
        /// </summary>
        private string _field;
        public string Field
        {
            get { return this._field; }
            set { this._field = value; }
        }

        /// <summary>
        /// Whether to 'get Value from' or 'apply Value to' all languages
        /// </summary>
        private bool _allLanguages = true;
        public bool AllLanguages
        {
            get { return this._allLanguages; }
            set { this._allLanguages = value; }
        }

        /// <summary>
        /// Sitecore Item Data Value - English
        /// </summary>
        private string _value  = string.Empty;
        public string Value
        {
            get { return this._value; }
            set
            {
                if (value == null)
                    this._value = string.Empty;
                else
                    this._value = value;
            }
        }

        /// <summary>
        /// Sitecore Item Data Value - Spanish
        /// </summary>
        private string _valueSpanish  = string.Empty;
        public string ValueSpanish
        {
            get { return this._valueSpanish; }
            set
            {
                if (value == null)
                    this._valueSpanish = string.Empty;
                else
                    this._valueSpanish = value;
            }
        }

        #endregion
    }
}
