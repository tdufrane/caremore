﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;

namespace Website.sitecore_modules.Developer
{
    public class XhtmlValidator
    {
        private StringBuilder errors;
        private const string htmlPage = "<!DOCTYPE html SYSTEM \"{0}\"><html><head><title>Template Page</title></head><body>{1}</body></html>";

        public XhtmlValidator()
        {
        }

        public string Parse(string xhtml)
        {
            this.errors = new StringBuilder();

            string xhtmlPage = string.Format(htmlPage, ConfigurationManager.AppSettings["DtdUri"], xhtml);
            using (StringReader input = new StringReader(xhtmlPage))
            {
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.CloseInput = true;
                settings.DtdProcessing = DtdProcessing.Ignore;
                settings.IgnoreWhitespace = true;
                settings.ValidationType = ValidationType.DTD;
                settings.ValidationEventHandler += new ValidationEventHandler(Settings_ValidationEventHandler);

                using (XmlReader reader = XmlReader.Create(input, settings))
                {
                    bool inBody = false;
                    string thisTag = string.Empty;
                    string lastTag = string.Empty;
                    List<string> noDuplicateTags = TagsNotAllowDuplicates();
                    Stack<string> tagHierarchy = new Stack<string>();

                    while (reader.Read())
                    {
                        thisTag = reader.Name;

                        if (reader.Name.Equals("body"))
                        {
                            if (inBody)
                                inBody = false;
                            else
                                inBody = true;
                        }
                        else if (inBody)
                        {
                            if (reader.IsStartElement())
                            {
                                if (!reader.IsEmptyElement)
                                {
                                    if ((noDuplicateTags.Contains(reader.Name)) && (tagHierarchy.Contains(reader.Name)))
                                    {
                                        string duplicateText = string.Format("Duplicate tag '{0}' exists.\n", reader.Name);

                                        if (!this.errors.ToString().Contains(duplicateText))
                                            this.errors.AppendFormat("Duplicate tag '{0}' exists.\n", reader.Name);
                                    }

                                    tagHierarchy.Push(reader.Name);
                                }
                            }
                            else if (reader.NodeType == XmlNodeType.EndElement)
                            {
                                tagHierarchy.Pop();
                            }
                        }
                    }
                }
            }

            return this.errors.ToString();
        }

        private void Settings_ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (!this.errors.ToString().Contains(e.Message))
            {
                this.errors.Append(e.Message);
                this.errors.Append("\n");
            }
        }

        private List<string> TagsNotAllowDuplicates()
        {
            List<string> tags = new List<string>();

            tags.Add("a");
            tags.Add("b");
            tags.Add("em");
            tags.Add("h1");
            tags.Add("h2");
            tags.Add("h3");
            tags.Add("h4");
            tags.Add("h5");
            tags.Add("h6");
            tags.Add("i");
            tags.Add("ol");
            tags.Add("strong");
            tags.Add("u");
            tags.Add("ul");

            return tags;
        }
    }
}
