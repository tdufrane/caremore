﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PathPicker.aspx.cs" Inherits="Website.sitecore_modules.PathPicker" MasterPageFile="~/sitecore modules/Site.Master" Title="Path Picker" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<h2>Path Picker</h2>

	<div id="tree">
		<asp:TreeView ID="tvPaths" runat="server" CssClass="tree"
			ExpandDepth="1"
			OnSelectedNodeChanged="TvPaths_SelectedNodeChanged"
			showexpandcollapse="true" showlines="true"></asp:TreeView>
	</div>

</asp:Content>
