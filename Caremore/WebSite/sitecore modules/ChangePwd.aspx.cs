﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security.AntiXss;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace Website.sitecore_modules
{
	public partial class ChangePwd : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack)
			{
				phMessage.Visible = false;
			}
			else
			{
				if ((Session["RedirectPath"] == null) || (Session["RedirectUrl"] == null))
				{
					this.aLogOff.Visible = false;
					phForm.Visible = false;
					CareMoreUtilities.DisplayError(phMessage, "You must be logged in to use this page.");
				}
				else
				{
					this.aLogOff.HRef = AntiXssEncoder.HtmlEncode((string)Session["RedirectPath"], false) + "Login.aspx";
					PageSetup();
				}
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			ChangePassword();
		}

		private void PageSetup()
		{
			if (Session["AdminUserName"] != null)
			{
				SetupForm(Session["AdminUserName"]);
			}
			else if (Session["BrokerAdminName"] != null)
			{
				SetupForm(Session["BrokerAdminName"]);
			}
			else if (Session["LeadAdminName"] != null)
			{
				SetupForm(Session["LeadAdminName"]);
			}
			else if (Session["OrderAdminName"] != null)
			{
				SetupForm(Session["OrderAdminName"]);
			}
			else if (Session["SpecialtyAdminName"] != null)
			{
				SetupForm(Session["SpecialtyAdminName"]);
			}
			else
			{
				phForm.Visible = false;
				CareMoreUtilities.DisplayError(phMessage, "You must be logged in to use this page.");
			}
		}

		private void SetupForm(object loginName)
		{
			string cleanLoginName = AntiXssEncoder.HtmlEncode((string)loginName, false);

			CaremoreApplicationDataContext dbContext = new CaremoreApplicationDataContext();

			IEnumerable<AdminUser> users = from user in dbContext.AdminUsers
			                               where (user.Username == cleanLoginName)
			                               select user;

			AdminUser adminUser = users.First();
			lblLogin.Text = adminUser.Username;
		}

		private void ChangePassword()
		{
			CaremoreApplicationDataContext dbContext = new CaremoreApplicationDataContext();

			IEnumerable<AdminUser> users = from user in dbContext.AdminUsers
			                               where (user.Username == lblLogin.Text)
			                               select user;

			AdminUser adminUser = users.First();

			if (txtOldPassword.Text == adminUser.Password)
			{
				adminUser.Password = txtPassword.Text.Trim();
				adminUser.PasswordReset = true;
				dbContext.SubmitChanges();

				Response.Redirect(AntiXssEncoder.HtmlEncode((string)Session["RedirectPath"], false) +
					AntiXssEncoder.HtmlEncode((string)Session["RedirectUrl"], false),
					false); // make sure URL is valid, only going to desired URL
			}
			else
			{
				CareMoreUtilities.DisplayError(phMessage, "Current Password is not correct.");
			}
		}
	}
}
