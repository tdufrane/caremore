﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom.Data;

namespace Website.sitecore_modules.Specialty
{
	public partial class SpecialtyDetails : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Session["SpecialtyAdminName"] == null)
			{
				Response.Redirect("Login.aspx", false);
			}
			else if (!IsPostBack)
			{
				if (string.IsNullOrEmpty(Request.QueryString["Id"]))
				{
					Response.Redirect("SpecialtyList.aspx", false);
				}
				else
				{
					LoadDetails(Request.QueryString["Id"]);
				}
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			SaveDetails();
			tblDetails.Visible = false;
		}

		private void LoadDetails(string id)
		{
			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
			List<SpecialtyMapping> item = dbContext.CM_PT_CMC_SpecMapItem(id).ToList();

			if (item.Count == 0)
			{
				tblDetails.Visible = false;
				CareMoreUtilities.DisplayError(phStatus, string.Format("A specialty with a code of <b>{0}</b> was not found.", id));
			}
			else
			{
				this.lblSpecialtyMappingId.Text = item[0].SpecialtyMappingId;
				this.lblFacetsDescription.Text = item[0].FacetsDescription;
				this.chkBoxDisplayOnWebsite.Checked = item[0].DisplayOnWebsite;
				this.txtBoxWebsiteDescription.Text = item[0].WebsiteDescription;
			}
		}

		private void SaveDetails()
		{
			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();

			string websiteDescription;
			if (string.IsNullOrEmpty(this.txtBoxWebsiteDescription.Text))
				websiteDescription = null;
			else
				websiteDescription = this.txtBoxWebsiteDescription.Text;

			dbContext.CM_PT_CMC_SpecMapUpdate(this.lblSpecialtyMappingId.Text,
				this.chkBoxDisplayOnWebsite.Checked, websiteDescription);

			string message;
			if (this.chkBoxDisplayOnWebsite.Checked)
			{
				string description;
				if (string.IsNullOrEmpty(this.txtBoxWebsiteDescription.Text))
					description = this.lblFacetsDescription.Text;
				else
					description = this.txtBoxWebsiteDescription.Text;

				message = string.Format("The <b>{0}</b> specialty with a code of was updated.<br/>Display on website: <i>Yes</i><br/>Will display as: <i></i>{1}",
					this.lblFacetsDescription.Text, description);
			}
			else
			{
				message = string.Format("The <b>{0}</b> specialty with a code of was updated.<br/>Display on website: <i>No</i>",
					this.lblFacetsDescription.Text);
			}

			if (Cache["ProviderSpecialties"] != null)
			{
				Cache.Remove("ProviderSpecialties");
			}

			CareMoreUtilities.DisplayStatus(phStatus, message);
		}
	}
}
