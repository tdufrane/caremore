﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="Login.aspx.cs" Inherits="Website.sitecore_modules.Specialty.Login" MasterPageFile="~/sitecore modules/Site.Master" Title="CareMore > Specialty Admin > Login" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<h2>Login</h2>

	<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
		<div class="form-group">
			<label for="txtUserName" class="col-xs-12 col-md-2 control-label">User Name:</label>
			<div class="col-xs-12 col-md-4">
				<asp:TextBox ID="txtUserName" runat="server" CssClass="form-control" MaxLength="100" />
			</div>
			<div class="col-xs-12 col-md-4">
				<asp:RequiredFieldValidator ID="reqValLogin" runat="server" ControlToValidate="txtUserName"
					SetFocusOnError="true" ErrorMessage="Required" />
			</div>
		</div>
		<div class="form-group">
			<label for="txtPassword" class="col-xs-12 col-md-2 control-label">Password:</label>
			<div class="col-xs-12 col-md-4">
				<asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" MaxLength="20" TextMode="Password" />
			</div>
			<div class="col-xs-12 col-md-4">
				<asp:RequiredFieldValidator ID="reqValPassword" runat="server" ControlToValidate="txtPassword"
					SetFocusOnError="true" ErrorMessage="Required" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12 col-md-offset-2 col-md-4">
				<asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_Click" Text="Login" />
			</div>
		</div>

		<div class="form-group">
			<div class="col-xs-12 col-md-offset-2">
				<p><asp:CheckBox ID="cbSaveLogin" runat="server" Text="Remember User Name" CssClass="checkbox-inline" /></p>
			</div>
		</div>
	</asp:Panel>

	<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
</asp:Content>
