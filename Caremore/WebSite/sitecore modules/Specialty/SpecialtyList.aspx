﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SpecialtyList.aspx.cs" Inherits="Website.sitecore_modules.Specialty.SpecialtyList" MasterPageFile="~/sitecore modules/Site.Master" Title="CareMore > Specialty Admin > List" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentHeader" runat="server" ContentPlaceHolderID="contentPlaceHolderHeader">
	<div>
		<a href="../ChangePwd.aspx">Change Password</a> |
		<a href="Login.aspx">Log Off</a>
	</div>
</asp:Content>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<div>
		<h1 class="heading">Specialty List</h1>
	</div>

	<br />

	<asp:GridView ID="gvSpecialties" runat="server"
		AutoGenerateColumns="false" DataKeyNames="SpecialtyMappingId"
		AllowSorting="true" CssClass="appList"
		OnSorting="GvSpecialties_Sorting">
		<AlternatingRowStyle CssClass="alt" />
		<FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
		<HeaderStyle CssClass="gridHeader" />
		<PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
		<Columns>
			<asp:HyperLinkField HeaderText="Action" ItemStyle-HorizontalAlign="Center"
				DataNavigateUrlFields="SpecialtyMappingId" DataNavigateUrlFormatString="SpecialtyDetails.aspx?Id={0}"
				Text="Edit" />
			<asp:BoundField DataField="SpecialtyMappingId" HeaderText="Code" SortExpression="SpecialtyMappingId" />
			<asp:BoundField DataField="DisplayOnWebsite" HeaderText="Display On Website"
				ItemStyle-HorizontalAlign="Center" SortExpression="DisplayOnWebsite" />
			<asp:BoundField DataField="FacetsDescription" HeaderText="Facets Desc." />
			<asp:BoundField DataField="WebsiteDescription" HeaderText="Website Desc." />
		</Columns>
	</asp:GridView>

	<p><asp:Button ID="btnSynch" runat="server" Text="Synchronize with Facets" ToolTip="This will add any new specialties and delete any not in use" OnClick="BtnSynch_Click"></asp:Button></p>

	<asp:PlaceHolder ID="phStatus" runat="server"></asp:PlaceHolder>
</asp:Content>
