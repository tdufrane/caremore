﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom.Data;

namespace Website.sitecore_modules.Specialty
{
	public partial class SpecialtyList : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Sitecore.Context.Item = Sitecore.Context.Database.GetItem(string.Empty);

			if (Session["SpecialtyAdminName"] == null)
			{
				Response.Redirect("Login.aspx", false);
			}
			else if (!IsPostBack)
			{
				LoadSpecialtyList();
			}
		}

		private void LoadSpecialtyList()
		{
			ProviderSearchDataContext db = new ProviderSearchDataContext();

			if (db.SpecialtyMappings.Count() > 0)
			{
				gvSpecialties.DataSource = db.SpecialtyMappings.OrderBy(item => item.SpecialtyMappingId);
				gvSpecialties.DataBind();

				SaveSort("SpecialtyMappingId", SortDirection.Ascending);

				CareMoreUtilities.ClearStatus(phStatus);
			}
			else
			{
				gvSpecialties.DataSource = null;
				gvSpecialties.DataBind();

				CareMoreUtilities.DisplayError(phStatus, "No Specialty Mappings were found.");
			}
		}

		protected void GvSpecialties_Sorting(object sender, GridViewSortEventArgs e)
		{
			ProviderSearchDataContext db = new ProviderSearchDataContext();

			string lastSortExpression = (string)ViewState["SortExpression"];
			SortDirection lastSortDirection = (SortDirection)ViewState["SortDirection"];

			if ((lastSortExpression == e.SortExpression) &&  (lastSortDirection == e.SortDirection))
			{
				if (e.SortDirection == SortDirection.Ascending)
					e.SortDirection = SortDirection.Descending;
				else
					e.SortDirection = SortDirection.Ascending;
			}

			if (e.SortExpression == "SpecialtyMappingId")
			{
				if (e.SortDirection == SortDirection.Ascending)
					gvSpecialties.DataSource = db.SpecialtyMappings.OrderBy(item => item.SpecialtyMappingId);
				else
					gvSpecialties.DataSource = db.SpecialtyMappings.OrderByDescending(item => item.SpecialtyMappingId);
			}
			else if (e.SortExpression == "DisplayOnWebsite")
			{
				if (e.SortDirection == SortDirection.Ascending)
					gvSpecialties.DataSource = db.SpecialtyMappings.OrderBy(item => item.DisplayOnWebsite);
				else
					gvSpecialties.DataSource = db.SpecialtyMappings.OrderByDescending(item => item.DisplayOnWebsite);
			}
			else
			{
				return;
			}

			gvSpecialties.DataBind();

			SaveSort(e.SortExpression, e.SortDirection);

			CareMoreUtilities.ClearStatus(phStatus);
		}

		protected void BtnSynch_Click(object sender, EventArgs e)
		{
		}

		private void SaveSort(string sortExpression, SortDirection direction)
		{
			ViewState["SortExpression"] = sortExpression;
			ViewState["SortDirection"] = direction;
		}
	}
}
