﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Website.sitecore_modules.Specialty
{
	public partial class Login : BaseAdminLogin
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			this.AllowType = AllowTypes.Specialty;
			this.RedirectPath = "Specialty/";
			this.RedirectUrl = "SpecialtyList.aspx";
			this.SessionName = "SpecialtyAdminName";

			CheckLoggedIn();
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				AuthenticateLogin(txtUserName.Text, txtPassword.Text, cbSaveLogin, phError);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}
	}
}
