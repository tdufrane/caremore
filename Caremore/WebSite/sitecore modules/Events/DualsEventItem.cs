﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;

namespace Website.sitecore_modules.Events
{
	public class DualsEventItem
	{
		#region Constructors

		public DualsEventItem()
		{
			EventTypeNames = new List<string>();
		}

		public DualsEventItem(OleDbDataReader dr)
		{
			EventTypeNames = new List<string>();
			FromReader(dr);
		}

		#endregion

		#region Properties

		public string Name { get; set; }
		public string Location { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string ZipCode { get; set; }
		public string Title { get; set; }
		public string State { get; set; }
		public string Content { get; set; }
		public DateTime Date { get; set; }
		public List<string> EventTypeNames { get; set; }
		public string EventGroup { get; set; }

		public string ReaderError { get; private set; }

		#endregion

		#region Private methods

		private bool FromReader(OleDbDataReader dr)
		{
			Database currentDB = Factory.GetDatabase("master");
			Item[] consumerEventTypes = currentDB.SelectItems(CareMoreUtilities.DUALS_GLOBAL_PATH + "/Event Types/Consumer Events//*");
			Item[] stakeholderEventTypes = currentDB.SelectItems(CareMoreUtilities.DUALS_GLOBAL_PATH + "/Event Types/Stakeholder Forums//*");
			string date = dr["Event Date"].ToString();
			DateTime dt = DateTime.MinValue;

			if (DateTime.TryParse(date, out dt))
			{
				date = dt.Date.ToShortDateString();
			}
			else
			{
				ReaderError = string.Format("Date not in proper format or missing for event at location {0} in {1}.",
					dr["Location Name"], dr["City"]);
				return false;
			}

			string time = dr["Event Start Time"].ToString();
			DateTime t = DateTime.MinValue;

			if (DateTime.TryParse(time, out t))
			{
				time = t.TimeOfDay.ToString();
			}
			else
			{
				ReaderError = string.Format("Time not in proper format or missing for event at location {0} in {1}.",
					dr["Location Name"], dr["City"]);
				return false;
			}

			Name = string.Format("{0} {1} {2}", dr["State"], dr["City"], dr["Location Name"]);

			Location = dr["Location Name"].ToString();
			Address = dr["Address"].ToString();
			City = dr["City"].ToString();
			ZipCode = dr["Zip Code"].ToString();
			Title = dr["Event Name"].ToString();
			State = dr["State"].ToString();
			Content = dr["Event Description"].ToString();
			Date = new DateTime(dt.Year, dt.Month, dt.Day, t.Hour, t.Minute, t.Second);

			string[] eventNames = dr["Event Type"].ToString().Split(new char[] { ',' },
				StringSplitOptions.RemoveEmptyEntries);

			foreach (Item i in consumerEventTypes)
			{
				if (eventNames[0] != null && i.Fields["Title"].Value == eventNames[0])
				{
					EventGroup = "Consumer";
					break;
				}
				EventGroup = "Stakeholder";

			}
			EventTypeNames.AddRange(eventNames);

			return true;
		}

		#endregion
	}
}