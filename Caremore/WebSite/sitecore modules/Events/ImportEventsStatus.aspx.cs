﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Jobs;
using Sitecore.Web.UI.Sheer;

namespace Website.sitecore_modules.Events
{
	public partial class ImportEventsStatus : System.Web.UI.Page
	{

		protected void Page_Load(object sender, EventArgs e)
		{
			string id = Request.QueryString["id"];

			if (string.IsNullOrEmpty(id))
			{
				Response.Redirect("ImportEvents.aspx", false);
			}
			else
			{
				hidHandle.Value = id;
				CheckStatus();
			}
		}

		protected void TimerPage_Tick(object sender, EventArgs e)
		{
			try
			{
				CheckStatus();
			}
			catch
			{
				timerPage.Enabled = false;
			}
		}

		protected void CheckStatus()
		{
			Handle jobHandle = Handle.Parse(hidHandle.Value);
			Job importJob = JobManager.GetJob(jobHandle);

			switch (importJob.Status.State)
			{
				case JobState.Finished:
					pProcessing.Visible = false;
					pStatus.Visible = true;
					pImport.Visible = true;
					pStatus.InnerHtml = importJob.Status.Messages[importJob.Status.Messages.Count - 2];
					break;

				case JobState.Running:
					pProcessing.Visible = true;
					pStatus.Visible = true;
					pImport.Visible = false;
					pStatus.InnerHtml = importJob.Status.Messages[importJob.Status.Messages.Count - 1];
					break;

				default:
					pProcessing.Visible = true;
					pStatus.Visible = false;
					pImport.Visible = false;
					break;
			}

			if (importJob.Status.State == JobState.Finished)
			{
				timerPage.Enabled = false;
			}
		}
	}
}
