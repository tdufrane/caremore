﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportEvents.aspx.cs" Inherits="Website.sitecore_modules.Events.ImportEvents"
	MasterPageFile="~/sitecore modules/Site.Master" Title="CareMore > Events > Import" %>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<h2>Import Events</h2>

	<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
		<div class="form-group">
			<label for="" class="col-xs-12 col-md-2 control-label">Event Category:</label>
			<div class="col-xs-12 col-md-10">
				<asp:RadioButton id="rbClassic" runat="server" CssClass="radio-inline" Text="Classic" Checked="True" GroupName="EventRadioGroup" />
				<asp:RadioButton id="rbDuals" runat="server" CssClass="radio-inline" Text="Duals" GroupName="EventRadioGroup" />
			</div>
		</div>
		<div class="form-group">
			<label for="" class="col-xs-12 col-md-2 control-label">Full Path of Excel File:</label>
			<div class="col-xs-12 col-md-6">
				<asp:FileUpload ID="txtExcelFile" runat="server" CssClass="form-control" />
			</div>
			<div class="col-xs-12 col-md-4">
				<asp:RequiredFieldValidator ID="reqFldValExcelFile" runat="server"
					ControlToValidate="txtExcelFile" ErrorMessage="Required" />
				<asp:RegularExpressionValidator ID="regExpValExcelFile" runat="server"
					ControlToValidate="txtExcelFile" ErrorMessage="Only an Excel 97-2003 File (.xls) can be used"
					ValidationExpression="^.+(\.xls)$" />
			</div>
		</div>
		<div class="form-group">
			<label for="" class="col-xs-12 col-md-2 control-label">Active Sheet Name:</label>
			<div class="col-xs-12 col-md-6">
				<asp:TextBox ID="txtActiveSheet" runat="server" Text="Sheet1" CssClass="form-control" />
			</div>
			<div class="col-xs-12 col-md-4">
				<asp:RequiredFieldValidator ID="reqFldValActiveSheet" runat="server"
					ControlToValidate="txtActiveSheet" ErrorMessage="Required" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12 col-md-offset-2 col-md-4">
				<asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_Click" Text="Import" />
			</div>
		</div>

		<asp:Panel id="pnlProcessing" runat="server" ClientIDMode="Static" style="display: none;">
			<img id="imgLoading" src="/images/ajax-loader.gif" alt="..." />
			<div id="divStatus">Processing please wait...</div>
		</asp:Panel>
	</asp:Panel>

	<asp:PlaceHolder ID="phStatus" runat="server" Visible="false" />
</asp:Content>
