﻿using System;
using System.Web;

using Sitecore.Configuration;
using Sitecore.Data;
using CareMore.Web.DotCom;

namespace Website.sitecore_modules.Events
{
	public partial class ImportEvents : System.Web.UI.Page
	{
		Database currentDB = Factory.GetDatabase("master");

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Sitecore.Context.IsLoggedIn)
			{
				pnlForm.Visible = false;
				CareMoreUtilities.DisplayError(phStatus, "You are not authorized to access this page.");
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				CareMoreUtilities.ClearStatus(phStatus);
				Page.Validate();

				if (Page.IsValid)
				{

					string fileName = UploadFile();
					string eventGroup;

					if (rbDuals.Checked == true)
						eventGroup = rbDuals.Text;
					else
						eventGroup = rbClassic.Text;

					LiveImport(fileName, eventGroup);
					// Comment out above and uncomment below to DEBUG
					//DebugImport(fileName, eventGroup);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phStatus, ex);
			}
		}

		private void LiveImport(string fileName, string eventGroup)
		{
			ImportEventsJob eventsJob = new ImportEventsJob();
			eventsJob.StartJob(fileName, txtActiveSheet.Text, eventGroup);
			Response.Redirect("ImportEventsStatus.aspx?id=" + eventsJob.Job.Handle.ToString(), false);
		}

		private void DebugImport(string fileName, string eventGroup)
		{
			ImportEventsJob eventsJob = new ImportEventsJob();
			eventsJob.ImportData(fileName, txtActiveSheet.Text, eventGroup);

			if (eventsJob.Errors.Length > 0)
				CareMoreUtilities.DisplayError(phStatus, eventsJob.Errors.ToString().Replace("\n", "<br />"));
		}

		private string UploadFile()
		{
			string fileName = HttpContext.Current.Server.MapPath("~/userFiles") + "\\" + Common.CleanFilename(txtExcelFile.FileName);
			txtExcelFile.PostedFile.SaveAs(fileName); // make sure validate extension, no slashes, dots
			return fileName;
		}
	}
}
