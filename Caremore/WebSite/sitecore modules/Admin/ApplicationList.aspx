﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="ApplicationList.aspx.cs" Inherits="Website.sitecore_modules.Admin.ApplicationList"
	MasterPageFile="~/sitecore modules/Site.Master" Title="CareMore > Admin > Application List" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentHeader" runat="server" ContentPlaceHolderID="contentPlaceHolderHeader">
	<div class="pull-right">
		<a href="../ChangePwd.aspx">Change Password <span class="glyphicon glyphicon-lock"></span></a> |
		<a href="Login.aspx">Log Off <span class="glyphicon glyphicon-log-out"></span></a>
	</div>
</asp:Content>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<h2>Application List</h2>

	<div class="form-group">
		<label for="rblStatusFilter" class="col-xs-1 control-label">Show:</label>
		<div class="col-xs-11">
			<asp:RadioButtonList ID="rblStatusFilter" runat="server" AutoPostBack="true" CssClass="radio-inline"
				RepeatDirection="Horizontal" RepeatLayout="Table"
				OnSelectedIndexChanged="RblStatusFilter_CheckedChanged">
				<asp:ListItem Selected="True">Unprocessed</asp:ListItem>
				<asp:ListItem>Processed</asp:ListItem>
				<asp:ListItem>Both</asp:ListItem>
			</asp:RadioButtonList>
		</div>
	</div>

	<asp:GridView ID="gvApplications" runat="server" AllowPaging="True" AllowSorting="True"
		AutoGenerateColumns="False" CssClass="table table-bordered table-striped" DataSourceID="ldsApplications"
		HeaderStyle-CssClass="info" PagerStyle-CssClass="info pagination" PagerStyle-HorizontalAlign="Center">
		<Columns>
			<asp:HyperLinkField Text="Review" DataNavigateUrlFields="ApplicationId" DataNavigateUrlFormatString="ApplicationDetails.aspx?id={0}" />
			<asp:BoundField DataField="ConfirmationNumber" HeaderText="Conf. #" ReadOnly="True" />
			<asp:BoundField DataField="FirstName" HeaderText="First Name" ReadOnly="True" SortExpression="FirstName" />
			<asp:BoundField DataField="LastName" HeaderText="Last Name" ReadOnly="True" SortExpression="LastName" />
			<asp:BoundField DataField="County" HeaderText="County" ReadOnly="True" SortExpression="County" />
			<asp:BoundField DataField="PlanAppliedFor" HeaderText="Plan Applied For" ReadOnly="True" SortExpression="PlanAppliedFor" />
			<asp:BoundField DataField="SubmittedOn" HeaderText="Date" ReadOnly="True" SortExpression="Date" DataFormatString="{0:M/dd/yyyy}" />
			<asp:TemplateField HeaderText="Status" SortExpression="Status">
				<ItemTemplate>
					<asp:Label ID="lblStatus" runat="server" Text='<%# Convert.ToBoolean(Eval("Status"))? "Processed" : "Unprocessed" %>'></asp:Label>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</asp:GridView>

	<asp:LinqDataSource ID="ldsApplications" runat="server" ContextTypeName="Website.CaremoreApplicationDataContext"
		OrderBy="SubmittedOn desc, Status desc" Select="new (ConfirmationNumber, FirstName, LastName, County, PlanAppliedFor, SubmittedOn, Status, ApplicationId)"
		TableName="MemberApplications" Where="Status == @Status" />

	<asp:PlaceHolder ID="phMessage" runat="server" Visible="false"></asp:PlaceHolder>
</asp:Content>
