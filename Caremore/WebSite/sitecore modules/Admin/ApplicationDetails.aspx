﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="ApplicationDetails.aspx.cs" Inherits="Website.sitecore_modules.Admin.ApplicationDetails"
	MasterPageFile="~/sitecore modules/Site.Master" Title="CareMore > Admin > Application Details" %>
<%@ MasterType VirtualPath="~/sitecore modules/Site.Master" %>

<asp:Content ID="contentHeader" runat="server" ContentPlaceHolderID="contentPlaceHolderHeader">
	<div class="pull-right">
		<a href="ApplicationList.aspx"><span class="glyphicon glyphicon-chevron-left"></span> Back to List</a> |
		<a href="Login.aspx">Log Off <span class="glyphicon glyphicon-log-out"></span></a>
	</div>
</asp:Content>

<asp:Content ID="contentPage" runat="server" ContentPlaceHolderID="contentPlaceHolderRoot">
	<h1 class="heading">Application Details</h1>

	<table class="table table-bordered">
		<tr>
			<td class="info col-xs-3">Confirmation Number</td>
			<td class="col-xs-9"><asp:Label ID="lblConfirmationNumber" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">County Selected</td>
			<td><asp:Label ID="lblLocation" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Plan Selected</td>
			<td><asp:Label ID="lblPlan" runat="server" /></td>
		</tr>
		<tr id="trIpaMedicalGroup" runat="server">
			<td class="info">IPA Medical Group</td>
			<td><asp:Label ID="lblIPAGroupNumber" runat="server" /></td>
		</tr>
		<tr id="trCoverageEffectiveDate" runat="server">
			<td class="info">Coverage Effective Date</td>
			<td><asp:Label ID="lblCoverageEffectiveDate" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Last Name</td>
			<td><asp:Label ID="lblLastName" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">First Name</td>
			<td><asp:Label ID="lblFirstName" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">(M.I.)</td>
			<td><asp:Label ID="lblMI" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Salutation</td>
			<td><asp:Label ID="lblSalutation" runat="server" /></td>
		</tr>
		<tr id="trSocialSecurity" runat="server">
			<td class="info">Social Security #</td>
			<td><asp:Label ID="lblSSN" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Birth Date</td>
			<td><asp:Label ID="lblBirthDate" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Sex</td>
			<td><asp:Label ID="lblSex" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Home Phone</td>
			<td><asp:Label ID="lblHomePhone" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Alternative Phone</td>
			<td><asp:Label ID="lblAlternativePhone" runat="server" /></td>
		</tr>
		<tr id="trIsCellPhone" runat="server">
			<td class="info">Is Cell Phone</td>
			<td><asp:Label ID="lblIsCellPhone" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Permanent Residence Address</td>
			<td><asp:Label ID="lblAddress" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">City</td>
			<td><asp:Label ID="lblCity" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">State</td>
			<td><asp:Label ID="lblState" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Zip Code</td>
			<td><asp:Label ID="lblZip" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Mailing address (if different from above)</td>
			<td><asp:Label ID="lblAddress2" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">City</td>
			<td><asp:Label ID="lblCity2" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">State</td>
			<td><asp:Label ID="lblState2" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Zip Code</td>
			<td><asp:Label ID="lblZip2" runat="server" /></td>
		</tr>
<asp:PlaceHolder ID="phMovedRows" runat="server">
		<tr>
			<td class="info">Moved During Calendar Year</td>
			<td><asp:Label ID="lblMoved" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Date of Moved</td>
			<td><asp:Label ID="lblDateOfMove" runat="server" /></td>
		</tr>
</asp:PlaceHolder>
		<tr>
			<td class="info">Emergency Contact</td>
			<td><asp:Label ID="lblEmergencyContact" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Emergency Phone Number</td>
			<td><asp:Label ID="lblEmergencyPhoneNumber" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Relationship to you</td>
			<td><asp:Label ID="lblEmergencyContactRelationshipToYou" runat="server" /></td>
		</tr>
		<tr id="trEmailAddress" runat="server">
			<td class="info">Email Address</td>
			<td><asp:Label ID="lblEmergencyContactEmailAddress" runat="server" /></td>
		</tr>
<asp:PlaceHolder ID="phConsentToContactRows" runat="server">
		<tr>
			<td class="info">Agrees to receive ANOC, EOC, and Formulary by Email</td>
			<td><asp:Label ID="lblReceiveDocsByEmail" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Would like to Receive Information Regarding Plan by Email</td>
			<td><asp:Label ID="lblReceiveInfoByEmail" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Email Address</td>
			<td><asp:Label ID="lblReceiveEmailAddress" runat="server" /></td>
		</tr>
</asp:PlaceHolder>
		<tr id="trPhysicianChoice" runat="server">
			<td class="info">Personal Physician Choice / Name and I.D. Number</td>
			<td><asp:Label ID="lblPhysicianChoice" runat="server" /></td>
		</tr>
		<tr id="trDentistChoice" runat="server">
			<td class="info">Dentist Choice / Name and I.D. Number</td>
			<td><asp:Label ID="lblDentistChoice" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Medicare Insurance Name</td>
			<td><asp:Label ID="lblMedicareInsuranceName" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Medicare Number</td>
			<td><asp:Label ID="lblMedicareNumber" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Medicare Sex</td>
			<td><asp:Label ID="lblMedicareSex" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Entitled to Hospital Insurance Benefits (Part A):</td>
			<td>
				<asp:Label ID="lblEntitledToHospitalBenefit" runat="server" />
				<br />
				<span class="text-info">Date:</span>
				<asp:Label ID="lblHospitalBenefitDate" runat="server" />
			</td>
		</tr>
		<tr>
			<td class="info">Entitled to Medical Insurance Benefits (Part B):</td>
			<td>
				<asp:Label ID="lblEntitledToMedicalBenefit" runat="server" />
				<br />
				<span class="text-info">Date:</span>
				<asp:Label ID="lblMedicalBenefitDate" runat="server" />
			</td>
		</tr>
		<tr id="trBenefitsVerified" runat="server">
			<td class="info">Your Benefits have been verified by:</td>
			<td><asp:Label ID="lblBenefitsVerifiedBy" runat="server" /></td>
		</tr>
		<tr id="trResidenceCounty" runat="server">
			<td class="info">Permanent Residence County:</td>
			<td><asp:Label ID="lblResidenceCounty" runat="server" /></td>
		</tr>
		<tr id="trChronicConditions" runat="server">
			<td class="info">Any applicable chronic condition(s)</td>
			<td><asp:Label ID="lblChonicConditions" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Premium payment option:</td>
			<td>
				<asp:Label ID="lblPaymentOption" runat="server" />
				<br />
				<asp:PlaceHolder ID="phEft" runat="server">
					<br />
					<span class="text-info">Bank details for Electronic funds transfer (EFT)<br />
					Account holder name:</span>
					<asp:Label ID="lblAccountHolderNameEft" runat="server" />
					<br />
					<span class="text-info">Account type:</span>
					<asp:Label ID="lblAccountType" runat="server" />
					<br />
					<span class="text-info">Routing number:</span>
					<asp:Label ID="lblRoutingNumber" runat="server" />
					<br />
					<span class="text-info"Account number:></span>
					<asp:Label ID="lblAccountNumberEft" runat="server" />
				</asp:PlaceHolder>
				<asp:PlaceHolder ID="phCreditCard" runat="server">
					<br />
					<span class="text-info">Credit Card Type:</span>
					<asp:Label ID="lblCreditCardType" runat="server" />
					<br />
					<span class="text-info">Account holder name:</span>
					<asp:Label ID="lblAccountHolderNameCC" runat="server" />
					<br />
					<span class="text-info">Account number:</span>
					<asp:Label ID="lblAccountNumberCC" runat="server" />
					<br />
					<span class="text-info">Credit Card Expiration Date:</span>
					<asp:Label ID="lblCreditCardExpirationDate" runat="server" />
				</asp:PlaceHolder>
			</td>
		</tr>
<asp:PlaceHolder ID="phChronicSNPRows" runat="server">
		<tr>
			<td class="info">On dialysis (ESRD)</td>
			<td><asp:Label ID="lblChronicESRD" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Current Dialysis Center</td>
			<td><asp:Label ID="lblCurrentDialysisCenter" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Current Dialysis Phone</td>
			<td><asp:Label ID="lblCurrentDialysisPhone" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">New Nephrologist First Name</td>
			<td><asp:Label ID="lblNephrologistFirstName" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">New Nephrologist Last Name</td>
			<td><asp:Label ID="lblNephrologistLastName" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">New Dialysis Center</td>
			<td><asp:Label ID="lblNewDialysisCenter" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Diagnosed with Asthma</td>
			<td><asp:Label ID="lblAsthma" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Diagnosed with Emphysema</td>
			<td><asp:Label ID="lblEmphysema" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Told by a Doctor Has Diabetes</td>
			<td><asp:Label ID="lblDiabetes" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Prescribed or Taking Insulin or an Oral Medication</td>
			<td><asp:Label ID="lblInsulin" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Told by a Doctor Has Coronary Artery Disease</td>
			<td><asp:Label ID="lblCoronaryArteryDisease" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Ever had a Heart Attack</td>
			<td><asp:Label ID="lblHeartAttack" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Told by a Doctor Has Heart Failure</td>
			<td><asp:Label ID="lblHeartFailure" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Problems with Fluid in Lungs and Swelling in Legs</td>
			<td><asp:Label ID="lblFluidSwelling" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Treating Specialist First Name</td>
			<td><asp:Label ID="lblTreatingFirstName" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Treating Specialist Last Name</td>
			<td><asp:Label ID="lblTreatingLastName" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Treating Specialist Specialty</td>
			<td><asp:Label ID="lblTreatingSpecialty" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Treating Specialist Phone</td>
			<td><asp:Label ID="lblTreatingPhone" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Enrollee Initials</td>
			<td><asp:Label ID="lblEnrolleeInitials" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Enrollee Date</td>
			<td><asp:Label ID="lblEnrolleeDate" runat="server" /></td>
		</tr>
</asp:PlaceHolder>
		<tr id="trMedAckAcknowledgment" runat="server">
			<td class="info">Acknowledged Responsible to Contact Existing Provider to Obtain Any Refills</td>
			<td><asp:Label ID="lblMedications" runat="server" /></td>
		</tr>
<asp:PlaceHolder ID="phContinuityCareRows" runat="server">
		<tr>
			<td class="info">Has Any Procedures or Surgeries Planned</td>
			<td><asp:Label ID="lblPlannedProcedures" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Treatment details</td>
			<td><asp:Label ID="lblTreatmentDetails" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Specialist(s) performing the procedure(s) or surgeries</td>
			<td>
				<table class="table table-striped">
					<tr>
						<td class="text-info">Name</td>
						<td class="text-info">Phone</td>
						<td class="text-info">Specialty</td>
					</tr>
					<tr>
						<td>
							<asp:Label ID="lblPlannedSpecialistFirstName1" runat="server" />&nbsp;
							<asp:Label ID="lblPlannedSpecialistLastName1" runat="server" /></td>
						<td><asp:Label ID="lblPlannedSpecialistPhone1" runat="server" /></td>
						<td><asp:Label ID="lblPlannedSpecialtyType1" runat="server" /></td>
					</tr>
					<tr>
						<td><asp:Label ID="lblPlannedSpecialistFirstName2" runat="server" />&nbsp;
							<asp:Label ID="lblPlannedSpecialistLastName2" runat="server" /></td>
						<td><asp:Label ID="lblPlannedSpecialistPhone2" runat="server" /></td>
						<td><asp:Label ID="lblPlannedSpecialtyType2" runat="server" /></td>
					</tr>
					<tr>
						<td><asp:Label ID="lblPlannedSpecialistFirstName3" runat="server" />&nbsp;
							<asp:Label ID="lblPlannedSpecialistLastName3" runat="server" /></td>
						<td><asp:Label ID="lblPlannedSpecialistPhone3" runat="server" /></td>
						<td><asp:Label ID="lblPlannedSpecialtyType3" runat="server" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="info">Specialist(s) Part of the Veterans Administration (VA)</td>
			<td><asp:Label ID="lblSpecialistPartOfVA" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Using oxygen daily to help breathe</td>
			<td><asp:Label ID="lblUsingOxygen" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Been diagnosed with Chronic Obstructive Pulmonary Disease</td>
			<td><asp:Label ID="lblDiagnosedCopd" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Currently receiving services from a Home Health Agency</td>
			<td><asp:Label ID="lblHomeHealthServices" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Home Health Provider</td>
			<td><asp:Label ID="lblHomeHealthProviderName" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Home Health Provider Phone</td>
			<td><asp:Label ID="lblHomeHealthProviderPhone" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Home Health Services Provided</td>
			<td><asp:Label ID="lblHomeHealthServicesUsed" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Dates of Upcoming Home Health Visits</td>
			<td><asp:Label ID="lblUpcomingHomeHealthVisits" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Uses the following Durable Medical Equipment</td>
			<td><asp:Label ID="lblRentedItems" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">DME Provider</td>
			<td><asp:Label ID="lblDmeProviderName" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">DME Provider Phone</td>
			<td><asp:Label ID="lblDmeProviderPhone" runat="server" /></td>
		</tr>
</asp:PlaceHolder>
		<tr id="trESRD" runat="server">
			<td class="info">Do you have End Stage Renal Disease(ESRD)?</td>
			<td><asp:Label ID="lblESRD" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Has other prescription drug coverage in addition to CareMore Health Plan?</td>
			<td>
				<div class="row">
					<div class="col-xs-12">
						<asp:Label ID="lblOtherCoverage" runat="server" />
					</div>
				</div>
				<div class="row">
					<div class="col-xs-3 text-info">Name of Other Coverage:</div>
					<div class="col-xs-9"><asp:Label ID="lblOtherCoverageName" runat="server" /></div>
				</div>
				<div class="row">
					<div class="col-xs-3 text-info">ID# for this Coverage:</div>
					<div class="col-xs-9"><asp:Label ID="lblOtherCoverageId" runat="server" /></div>
				</div>
				<div id="trOtherCoverageGroupNumber" runat="server" class="row">
					<div class="col-xs-3 text-info">Group# for this Coverage:</div>
					<div class="col-xs-9"><asp:Label ID="lblOtherCoverageGroupNumber" runat="server" /></div>
				</div>
				<div id="trOtherCoverageEndDate" runat="server" class="row">
					<div class="col-xs-3 text-info">Coverage End Date:</div>
					<div class="col-xs-9"><asp:Label ID="lblOtherCoverageDate" runat="server" /></div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="info">Are you a resident in a long-term care facility, such as a nursing home?</td>
			<td>
				<asp:Label ID="lblCareResident" runat="server" />
				<br /><br />
				<span class="text-info">Name, Address, & Phone Number of Institution (number and street):</span>
				<br />
				<asp:Label ID="lblCareResidentInfo" runat="server" />
			</td>
		</tr>
		<tr>
			<td class="info">Are you enrolled in your State Medicaid program (Medi-Cal)?</td>
			<td>
				<asp:Label ID="lblStateMedicaid" runat="server" />
				<br /><br />
				<span class="text-info">State medicaid number:</span>
				<asp:Label ID="lblStateMedicaidNumber" runat="server" />
			</td>
		</tr>
<asp:PlaceHolder ID="phCurrentMember" runat="server">
		<tr>
			<td class="info">Current CareMore Member</td>
			<td><asp:Label ID="lblCurrentMember" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Name of current medical insurance plan</td>
			<td><asp:Label ID="lblCurrentMemberPlan" runat="server" /></td>
		</tr>
</asp:PlaceHolder>
		<tr>
			<td class="info"><asp:Label id="lblSpouseText" runat="server" /></td>
			<td><asp:Label ID="lblSpouseWork" runat="server" /></td>
		</tr>
		<tr id="trSpouseGroupCoveragePlan" runat="server">
			<td class="info">Name of group coverage plan</td>
			<td><asp:Label ID="lblSpouseGroupCoveragePlan" runat="server" /></td>
		</tr>
		<tr id="trLiveInLongTermCareFacility" runat="server">
			<td class="info">Lives in a long term care facility</td>
			<td><asp:Label ID="lblLiveLongTerm" runat="server" /></td>
		</tr>
		<tr id="trChronicLungDisorder" runat="server">
			<td class="info">Has Chronic Lung Disorder</td>
			<td><asp:Label ID="lblHasChronicLungDisorder" runat="server" /></td>
		</tr>
		<tr id="trDiabetes" runat="server">
			<td class="info">Has Diabetes</td>
			<td><asp:Label ID="lblHasDiabetes" runat="server" /></td>
		</tr>
		<tr id="trChronicHeartFailure" runat="server">
			<td class="info">Has Cardiovascular Disorder or Chronic Heart Failure?</td>
			<td><asp:Label ID="lblHasCardiovascularDisorder" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Primary Care Physician Name</td>
			<td><asp:Label ID="lblPrimaryCarePhysicianName" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Print format</td>
			<td><asp:Label ID="lblPrintFormat" runat="server" /></td>
		</tr>
		<tr id="trReceivedSummary" runat="server">
			<td class="info">Was Given the CareMore Health Plan Summary of Benefits</td>
			<td><asp:Label ID="lblReceivedSummary" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Release of Information</td>
			<td>
				<div class="row">
					<div class="col-xs-3 text-info">Enrollee Initials:</div>
					<div class="col-xs-9"><asp:Label ID="lblReleaseInitials" runat="server" /></div>
				</div>
				<div class="row">
					<div class="col-xs-3 text-info">Date:</div>
					<div class="col-xs-9"><asp:Label ID="lblReleaseDate" runat="server" /></div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="info">Signature</td>
			<td><asp:Label ID="lblSignature" runat="server" /></td>
		</tr>
		<tr>
			<td class="info">Are you the authorized representative?</td>
			<td>
				<div class="row">
					<div class="col-xs-12">
						<asp:Label ID="lblAuthorizedRep" runat="server" />
					</div>
				</div>
				<div class="row">
					<div class="col-xs-3 text-info">Name:</div>
					<div class="col-xs-9"><asp:Label ID="lblRepName" runat="server" /></div>
				</div>
				<div class="row">
					<div class="col-xs-3 text-info">Address:</div>
					<div class="col-xs-9"><asp:Label ID="lblRepAddress" runat="server" /></div>
				</div>
				<div class="row">
					<div class="col-xs-3 text-info">Phone Number:</div>
					<div class="col-xs-9"><asp:Label ID="lblRepPhone" runat="server" /></div>
				</div>
				<div class="row">
					<div class="col-xs-3 text-info">Relationship to Enrollee:</div>
					<div class="col-xs-9"><asp:Label ID="lblRepRelation" runat="server" /></div>
				</div>
			</td>
		</tr>
		<tr id="trSalesAgent" runat="server">
			<td class="info">Sales agent completed this form</td>
			<td>
				<div class="row">
					<div class="col-xs-12">
						<asp:Label ID="lblSalesAgent" runat="server" />
					</div>
				</div>
				<div class="row">
					<div class="col-xs-3 text-info">Agent Code:</div>
					<div class="col-xs-9"><asp:Label ID="lblSalesAgentCode" runat="server" /></div>
				</div>
				<div class="row">
					<div class="col-xs-3 text-info">Agent/Brokers First Name:</div>
					<div class="col-xs-9"><asp:Label ID="lblSalesAgentFirstName" runat="server" /></div>
				</div>
				<div class="row">
					<div class="col-xs-3 text-info">Agent/Brokers Last Name:</div>
					<div class="col-xs-9"><asp:Label ID="lblSalesAgentLastName" runat="server" /></div>
				</div>
			</td>
		</tr>
	</table>

	<div class="row">
		<div class="col-xs-offset-3 col-xs-12">
			<asp:Button ID="btnSubmit" runat="server" OnClick="BtnSubmit_Click" CssClass="btn btn-primary" />
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<a href="ApplicationList.aspx"><span class="glyphicon glyphicon-chevron-left"></span> Back to List</a>
		</div>
	</div>

	<asp:PlaceHolder ID="phMessage" runat="server" Visible="false"></asp:PlaceHolder>
</asp:Content>
