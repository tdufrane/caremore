﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System.Web.Security.AntiXss;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using CareMore.Security;

namespace Website.sitecore_modules.Admin
{
	public partial class ApplicationDetails : System.Web.UI.Page
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				if (Session["AdminUsername"] == null)
				{
					Response.Redirect("Login.aspx", false);
				}
				else if (string.IsNullOrEmpty(Request.QueryString["Id"]))
				{
					Response.Redirect("ApplicationList.aspx", false);
				}
				else if (!IsPostBack)
				{
					LoadDetails();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phMessage, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				SaveChanges();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phMessage, ex);
			}
		}

		#endregion

		#region Private methods

		private MemberApplication GetApplication(CaremoreApplicationDataContext dbContext)
		{
			Guid applicationId = Guid.Empty;
			MemberApplication application = null;

			if (Guid.TryParse(Request.QueryString["Id"], out applicationId))
			{
				IEnumerable<MemberApplication> applicationList =
					from app in dbContext.MemberApplications
					where app.ApplicationId.Equals(applicationId)
					select app;

				if (applicationList != null && applicationList.Count() == 1)
				{
					application = applicationList.Single();
				}
			}

			return application;
		}

		private void LoadDetails()
		{
			CaremoreApplicationDataContext dbContext = new CaremoreApplicationDataContext();
			MemberApplication application = GetApplication(dbContext);

			if (application == null)
			{
				SetNotFoundMessage();
			}
			else
			{
				Database masterDB = Factory.GetDatabase("master");
				Item appItem = masterDB.GetItem(application.AppSitecoreId.ToString("B"));

				btnSubmit.Text = string.Format("Mark as {0}", application.Status ? "Unprocessed" : "Processed");

				lblConfirmationNumber.Text = AntiXssEncoder.HtmlEncode(application.ConfirmationNumber, false);
				lblLocation.Text = AntiXssEncoder.HtmlEncode(application.County, false);
				lblPlan.Text = AntiXssEncoder.HtmlEncode(application.PlanAppliedFor, false);

				trIpaMedicalGroup.Visible = appItem["IPA Group Number"].Equals("1");
				if (trIpaMedicalGroup.Visible)
					lblIPAGroupNumber.Text = AntiXssEncoder.HtmlEncode(application.IPAGroupNumber, false);

				trCoverageEffectiveDate.Visible = appItem["Coverage Effective Date"].Equals("1");
				if (trCoverageEffectiveDate.Visible)
					lblCoverageEffectiveDate.Text = CareMoreUtilities.DateFormat(application.CoverageEffectiveDate);

				lblLastName.Text = AntiXssEncoder.HtmlEncode(application.LastName, false);
				lblFirstName.Text = AntiXssEncoder.HtmlEncode(application.FirstName, false);
				lblMI.Text = AntiXssEncoder.HtmlEncode(application.MiddleInitial, false);
				lblSalutation.Text = AntiXssEncoder.HtmlEncode(application.Salutation, false);

				trSocialSecurity.Visible = appItem["Social Security"].Equals("1");
				if (trSocialSecurity.Visible)
					lblSSN.Text = Cryptography.Decrypt(application.SocialSecurityNumber);

				lblBirthDate.Text = CareMoreUtilities.DateFormat(application.BirthDate);
				lblSex.Text = AntiXssEncoder.HtmlEncode(application.Sex.ToString(), false);
				lblHomePhone.Text = AntiXssEncoder.HtmlEncode(application.HomePhone, false);
				lblAlternativePhone.Text = AntiXssEncoder.HtmlEncode(application.AltPhone, false);

				trIsCellPhone.Visible = appItem["Is Cell Phone"].Equals("1");
				if (trIsCellPhone.Visible)
					lblIsCellPhone.Text = CareMoreUtilities.YesNo(application.IsCellPhone);

				lblAddress.Text = AntiXssEncoder.HtmlEncode(application.Address, false);
				lblCity.Text = AntiXssEncoder.HtmlEncode(application.City, false);
				lblState.Text = AntiXssEncoder.HtmlEncode(application.State, false);
				lblZip.Text = AntiXssEncoder.HtmlEncode(application.Zip, false);
				lblAddress2.Text = AntiXssEncoder.HtmlEncode(application.Address2, false);
				lblCity2.Text = AntiXssEncoder.HtmlEncode(application.City2, false);
				lblState2.Text = AntiXssEncoder.HtmlEncode(application.State2, false);
				lblZip2.Text = AntiXssEncoder.HtmlEncode(application.Zip2, false);

				phMovedRows.Visible = appItem["Have You Moved"].Equals("1");
				if (phMovedRows.Visible)
				{
					lblMoved.Text = CareMoreUtilities.YesNo(application.Moved);
					lblDateOfMove.Text = CareMoreUtilities.DateFormat(application.DateOfMove);
				}

				lblEmergencyContact.Text = AntiXssEncoder.HtmlEncode(application.EmergencyContactName, false);
				lblEmergencyPhoneNumber.Text = AntiXssEncoder.HtmlEncode(application.EmergencyContactPhone, false);
				lblEmergencyContactRelationshipToYou.Text = AntiXssEncoder.HtmlEncode(application.EmergencyContactRelation, false);
				lblEmergencyContactEmailAddress.Text = AntiXssEncoder.HtmlEncode(application.EmailAddress, false);

				trEmailAddress.Visible = !appItem["Consent To Contact"].Equals("1");
				if (trEmailAddress.Visible)
					lblIsCellPhone.Text = CareMoreUtilities.YesNo(application.IsCellPhone);

				phConsentToContactRows.Visible = appItem["Consent To Contact"].Equals("1");
				if (phConsentToContactRows.Visible)
				{
					lblReceiveDocsByEmail.Text = CareMoreUtilities.YesNo(application.ReceiveDocsByEmail);
					lblReceiveInfoByEmail.Text = CareMoreUtilities.YesNo(application.ReceiveInfoByEmail);
					lblReceiveEmailAddress.Text = AntiXssEncoder.HtmlEncode(application.EmailAddress, false);
				}

				trPhysicianChoice.Visible = appItem["Personal Physician"].Equals("1");
				if (trPhysicianChoice.Visible)
					lblPhysicianChoice.Text = AntiXssEncoder.HtmlEncode(application.PhysicianChoice, false);

				trDentistChoice.Visible = appItem["Dentist Choice"].Equals("1");
				if (trDentistChoice.Visible)
					lblDentistChoice.Text = AntiXssEncoder.HtmlEncode(application.DentistChoice, false);

				lblMedicareInsuranceName.Text = AntiXssEncoder.HtmlEncode(application.MedicareName, false);
				lblMedicareNumber.Text = Cryptography.Decrypt(application.MedicareNumber);
				lblMedicareSex.Text = AntiXssEncoder.HtmlEncode(application.MedicareSex, false);
				lblEntitledToHospitalBenefit.Text = CareMoreUtilities.YesNo(application.EntitledToHospitalBenefits);
				lblHospitalBenefitDate.Text = CareMoreUtilities.DateFormat(application.HospitalBenefitsDate);
				lblEntitledToMedicalBenefit.Text = CareMoreUtilities.YesNo(application.EntitledToMedicalBenefits);
				lblMedicalBenefitDate.Text = CareMoreUtilities.DateFormat(application.MedicalBenefitsDate);

				trBenefitsVerified.Visible = appItem["Benefits Verified"].Equals("1");
				if (trBenefitsVerified.Visible)
					lblBenefitsVerifiedBy.Text = AntiXssEncoder.HtmlEncode(application.BenefitsVerifiedBy, false);

				trResidenceCounty.Visible = appItem["Permanent Residence County"].Equals("1");
				if (trResidenceCounty.Visible)
					lblResidenceCounty.Text = AntiXssEncoder.HtmlEncode(application.ResidenceCounty, false);

				trChronicConditions.Visible = appItem["Chronic Conditions"].Equals("1");
				if (trChronicConditions.Visible)
					lblChonicConditions.Text = AntiXssEncoder.HtmlEncode(application.ChronicConditions, false);

				lblPaymentOption.Text = AntiXssEncoder.HtmlEncode(application.PaymentOption, false);

				if (application.PaymentOption.Contains("(EFT)"))
				{
					lblAccountHolderNameEft.Text = AntiXssEncoder.HtmlEncode(application.AccountHolderName, false);
					lblAccountType.Text = AntiXssEncoder.HtmlEncode(application.AccountType, false);
					lblRoutingNumber.Text = Cryptography.Decrypt(application.RoutingNumber);
					lblAccountNumberEft.Text = Cryptography.Decrypt(application.AccountNumber);
					phCreditCard.Visible = false;
				}
				else if (application.PaymentOption.Equals("Credit Card"))
				{
					phEft.Visible = false;
					lblCreditCardType.Text = AntiXssEncoder.HtmlEncode(application.CreditCardType, false);
					lblAccountHolderNameCC.Text = AntiXssEncoder.HtmlEncode(application.AccountHolderName, false);
					lblAccountNumberCC.Text = Cryptography.Decrypt(application.AccountNumber);
					lblCreditCardExpirationDate.Text = AntiXssEncoder.HtmlEncode(application.CreditCardExpireDate, false);
				}
				else
				{
					phCreditCard.Visible = false;
					phEft.Visible = false;
				}

				phChronicSNPRows.Visible = appItem["Chronic SNP"].Equals("1");
				if (phChronicSNPRows.Visible)
				{
					lblChronicESRD.Text = CareMoreUtilities.YesNo(application.ChronicESRD);
					lblCurrentDialysisCenter.Text = AntiXssEncoder.HtmlEncode(application.CurrentDialysisCenter, false);
					lblCurrentDialysisPhone.Text = AntiXssEncoder.HtmlEncode(application.CurrentDialysisPhone, false);
					lblNephrologistFirstName.Text = AntiXssEncoder.HtmlEncode(application.NephrologistFirstName, false);
					lblNephrologistLastName.Text = AntiXssEncoder.HtmlEncode(application.NephrologistLastName, false);
					lblNewDialysisCenter.Text = AntiXssEncoder.HtmlEncode(application.NewDialysisCenter, false);
					lblAsthma.Text = CareMoreUtilities.YesNo(application.ChronicAsthma);
					lblEmphysema.Text = CareMoreUtilities.YesNo(application.ChronicEmphysema);
					lblDiabetes.Text = CareMoreUtilities.YesNo(application.ChronicDiabetes);
					lblInsulin.Text = CareMoreUtilities.YesNo(application.ChronicInsulin);
					lblCoronaryArteryDisease.Text = CareMoreUtilities.YesNo(application.ChronicCoronaryArteryDisease);
					lblHeartAttack.Text = CareMoreUtilities.YesNo(application.ChronicHeartAttack);
					lblHeartFailure.Text = CareMoreUtilities.YesNo(application.ChronicHeartFailure);
					lblFluidSwelling.Text = CareMoreUtilities.YesNo(application.ChronicFluidSwelling);
					lblTreatingFirstName.Text = AntiXssEncoder.HtmlEncode(application.TreatingFirstName, false);
					lblTreatingLastName.Text = AntiXssEncoder.HtmlEncode(application.TreatingLastName, false);
					lblTreatingSpecialty.Text = AntiXssEncoder.HtmlEncode(application.TreatingSpecialty, false);
					lblTreatingPhone.Text = AntiXssEncoder.HtmlEncode(application.TreatingPhone, false);
					lblEnrolleeInitials.Text = AntiXssEncoder.HtmlEncode(application.ChronicEnrolleeInitials, false);
					lblEnrolleeDate.Text = CareMoreUtilities.DateFormat(application.ChronicEnrolleeDate);
				}

				trMedAckAcknowledgment.Visible = appItem["Medications Acknowledgment"].Equals("1");
				if (trMedAckAcknowledgment.Visible)
					lblMedications.Text = CareMoreUtilities.YesNo(application.MedicationsAcknowledgment);

				phContinuityCareRows.Visible = appItem["Continuity Care Coordination"].Equals("1");
				if (phContinuityCareRows.Visible)
				{
					lblPlannedProcedures.Text = CareMoreUtilities.YesNo(application.PlannedProcedures);
					lblTreatmentDetails.Text = application.TreatmentDetails;
					lblPlannedSpecialistFirstName1.Text = AntiXssEncoder.HtmlEncode(application.PlannedSpecialistFirstName1, false);
					lblPlannedSpecialistLastName1.Text = AntiXssEncoder.HtmlEncode(application.PlannedSpecialistLastName1, false);
					lblPlannedSpecialistPhone1.Text = AntiXssEncoder.HtmlEncode(application.PlannedSpecialistPhone1, false);
					lblPlannedSpecialtyType1.Text = AntiXssEncoder.HtmlEncode(application.PlannedSpecialistType1, false);
					lblPlannedSpecialistFirstName2.Text = AntiXssEncoder.HtmlEncode(application.PlannedSpecialistFirstName2, false);
					lblPlannedSpecialistLastName2.Text = AntiXssEncoder.HtmlEncode(application.PlannedSpecialistLastName2, false);
					lblPlannedSpecialistPhone2.Text = AntiXssEncoder.HtmlEncode(application.PlannedSpecialistPhone2, false);
					lblPlannedSpecialtyType2.Text = AntiXssEncoder.HtmlEncode(application.PlannedSpecialistType2, false);
					lblPlannedSpecialistFirstName3.Text = AntiXssEncoder.HtmlEncode(application.PlannedSpecialistFirstName3, false);
					lblPlannedSpecialistLastName3.Text = AntiXssEncoder.HtmlEncode(application.PlannedSpecialistLastName3, false);
					lblPlannedSpecialistPhone3.Text = AntiXssEncoder.HtmlEncode(application.PlannedSpecialistPhone3, false);
					lblPlannedSpecialtyType3.Text = AntiXssEncoder.HtmlEncode(application.PlannedSpecialistType3, false);
					lblSpecialistPartOfVA.Text = CareMoreUtilities.YesNo(application.SpecialistPartOfVA);

					lblUsingOxygen.Text = CareMoreUtilities.YesNo(application.UsingOxygen);
					lblDiagnosedCopd.Text = CareMoreUtilities.YesNo(application.DiagnosedCopd);

					lblHomeHealthServices.Text = CareMoreUtilities.YesNo(application.HomeHealthServices);
					lblHomeHealthProviderName.Text   = AntiXssEncoder.HtmlEncode(application.HomeHealthProviderName, false);
					lblHomeHealthProviderPhone.Text  = AntiXssEncoder.HtmlEncode(application.HomeHealthProviderPhone, false);
					lblHomeHealthServicesUsed.Text   = AntiXssEncoder.HtmlEncode(application.HomeHealthServicesUsed, false);
					lblUpcomingHomeHealthVisits.Text = AntiXssEncoder.HtmlEncode(application.UpcomingHomeHealthVisits, false);

					if (string.IsNullOrEmpty(application.RentedItemsOther))
					{
						lblRentedItems.Text = AntiXssEncoder.HtmlEncode(application.RentedItems, false);
					}
					else
					{
						lblRentedItems.Text = string.Format("{0}, {1}",
							AntiXssEncoder.HtmlEncode(application.RentedItems, false),
							AntiXssEncoder.HtmlEncode(application.RentedItemsOther, false));
					}

					lblDmeProviderName.Text = AntiXssEncoder.HtmlEncode(application.DmeProviderName, false);
					lblDmeProviderPhone.Text = AntiXssEncoder.HtmlEncode(application.DmeProviderPhone, false);
				}

				lblPrintFormat.Text = AntiXssEncoder.HtmlEncode(application.PrintFormat, false);

				trESRD.Visible = appItem["ESRD"].Equals("1");
				if (trESRD.Visible)
					lblESRD.Text = CareMoreUtilities.YesNo(application.ESRD);

				lblOtherCoverage.Text = CareMoreUtilities.YesNo(application.OtherCoverage);
				lblOtherCoverageName.Text = AntiXssEncoder.HtmlEncode(application.OtherCoverageName, false);
				lblOtherCoverageId.Text = AntiXssEncoder.HtmlEncode(application.OtherCoverageId, false);

				trOtherCoverageGroupNumber.Visible = !appItem["Coverage End Date"].Equals("1");
				if (trOtherCoverageGroupNumber.Visible)
					lblOtherCoverageGroupNumber.Text = AntiXssEncoder.HtmlEncode(application.OtherCoverageGroupNumber, false);

				trOtherCoverageEndDate.Visible = appItem["Coverage End Date"].Equals("1");
				if (trOtherCoverageEndDate.Visible)
					lblOtherCoverageDate.Text = CareMoreUtilities.DateFormat(application.OtherCoverageDate);

				lblCareResident.Text = CareMoreUtilities.YesNo(application.CareResident);
				lblCareResidentInfo.Text = AntiXssEncoder.HtmlEncode(application.CareResidentInfo, false);

				lblStateMedicaid.Text = CareMoreUtilities.YesNo(application.StateMedicaid);
				lblStateMedicaidNumber.Text = Cryptography.Decrypt(application.StateMedicaidNumber);

				phCurrentMember.Visible = appItem["Current Member"].Equals("1");
				if (phCurrentMember.Visible)
				{
					lblCurrentMember.Text = CareMoreUtilities.YesNo(application.CurrentMember);
					lblCurrentMemberPlan.Text = AntiXssEncoder.HtmlEncode(application.CurrentMemberPlan, false);
				}

				lblSpouseText.Text = appItem["Spouse Label"];
				lblSpouseWork.Text = CareMoreUtilities.YesNo(application.SpouseWork);

				trSpouseGroupCoveragePlan.Visible = appItem["Spouse Group Coverage"].Equals("1");
				if (trSpouseGroupCoveragePlan.Visible)
					lblSpouseGroupCoveragePlan.Text = AntiXssEncoder.HtmlEncode(application.SpouseGroupCoveragePlan, false);

				trLiveInLongTermCareFacility.Visible = appItem["Live in Long Term Care Facility"].Equals("1");
				if (trLiveInLongTermCareFacility.Visible)
					lblLiveLongTerm.Text = CareMoreUtilities.YesNo(application.LiveinLongTermCareFacility);

				trChronicLungDisorder.Visible = appItem["Chronic Lung Disorder"].Equals("1");
				if (trChronicLungDisorder.Visible)
					lblHasChronicLungDisorder.Text = CareMoreUtilities.YesNo(application.ChronicLung);

				trDiabetes.Visible = appItem["Diabetes"].Equals("1");
				if (trDiabetes.Visible)
					lblHasDiabetes.Text = CareMoreUtilities.YesNo(application.Diabetes);

				trChronicHeartFailure.Visible = appItem["Chronic Heart Failure"].Equals("1");
				if (trChronicHeartFailure.Visible)
					lblHasCardiovascularDisorder.Text = CareMoreUtilities.YesNo(application.ChronicHeart);

				lblPrimaryCarePhysicianName.Text = AntiXssEncoder.HtmlEncode(application.PrimaryPhysicianName, false);

				trReceivedSummary.Visible = appItem["Given Summary of Benefits"].Equals("1");
				if (trReceivedSummary.Visible)
					lblReceivedSummary.Text = CareMoreUtilities.YesNo(application.ReceivedSummary);

				lblSignature.Text = AntiXssEncoder.HtmlEncode(application.Signature, false);
				lblReleaseInitials.Text = AntiXssEncoder.HtmlEncode(application.ReleaseInitials, false);
				lblReleaseDate.Text = CareMoreUtilities.DateFormat(application.ReleaseDate);

				lblAuthorizedRep.Text = CareMoreUtilities.YesNo(application.IsAuthorizedRep);
				lblRepName.Text = AntiXssEncoder.HtmlEncode(application.RepName, false);
				lblRepAddress.Text = AntiXssEncoder.HtmlEncode(application.RepAddress, false);
				lblRepPhone.Text = AntiXssEncoder.HtmlEncode(application.RepPhone, false);
				lblRepRelation.Text = AntiXssEncoder.HtmlEncode(application.RepRelation, false);

				trSalesAgent.Visible = appItem["Sales Agent"].Equals("1");
				if (trSalesAgent.Visible)
				{
					lblSalesAgent.Text = CareMoreUtilities.YesNo(application.IsSalesAgent);
					lblSalesAgentCode.Text = AntiXssEncoder.HtmlEncode(application.SalesAgentCode, false);
					lblSalesAgentFirstName.Text = AntiXssEncoder.HtmlEncode(application.SalesAgentFirstName, false);
					lblSalesAgentLastName.Text = AntiXssEncoder.HtmlEncode(application.SalesAgentLastName, false);
				}
			}
		}

		private void SaveChanges()
		{
			CaremoreApplicationDataContext dbContext = new CaremoreApplicationDataContext();
			MemberApplication application = GetApplication(dbContext);

			if (application == null)
			{
				SetNotFoundMessage();
			}
			else
			{
				application.Status = !application.Status;
				application.UpdatedBy = Session["AdminUsername"].ToString();
				application.UpdatedOn = DateTime.Now;

				dbContext.SubmitChanges();

				btnSubmit.Text = string.Format("Mark as {0}", application.Status ? "Unprocessed" : "Processed");
			}
		}

		private void SetNotFoundMessage()
		{
			string id = Request.QueryString["Id"];
			if (string.IsNullOrEmpty(id))
			{
				CareMoreUtilities.DisplayError(phMessage, "An application ID was not provided.");
			}
			else
			{
				CareMoreUtilities.DisplayError(phMessage,
					string.Format("An application with the ID of <strong>{0}</strong> was not found.", id));
			}
		}

		#endregion
	}
}
