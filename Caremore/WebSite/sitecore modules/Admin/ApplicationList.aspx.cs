﻿using System;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace Website.sitecore_modules.Admin
{
	public partial class ApplicationList : System.Web.UI.Page
	{
		private const string emptyDataText = "There are currently no {0} applications to display.";

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				if (Session["AdminUsername"] == null)
				{
					Response.Redirect("Login.aspx", false);
				}
				else if (!IsPostBack)
				{
					SetStatusFilter(0);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phMessage, ex);
			}
		}

		protected void RblStatusFilter_CheckedChanged(object sender, EventArgs e)
		{
			try
			{
				SetStatusFilter(rblStatusFilter.SelectedIndex);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phMessage, ex);
			}
		}

		#endregion

		private void SetStatusFilter(int index)
		{
			ldsApplications.WhereParameters.Clear();

			switch (index)
			{
				// Display Unprocessed Applications
				case 0:
					ldsApplications.WhereParameters.Add(new Parameter("Status", TypeCode.Boolean, "false"));
					gvApplications.EmptyDataText = string.Format(emptyDataText, "Unprocessed");
					break;

				// Display Processed Applications
				case 1:
					ldsApplications.WhereParameters.Add(new Parameter("Status", TypeCode.Boolean, "true"));
					gvApplications.EmptyDataText = string.Format(emptyDataText, "Processed");
					break;

				// Display Processed Applications
				case 2:
					gvApplications.EmptyDataText = string.Format(emptyDataText, "Processed/Unprocessed");
					break;
			}

			ldsApplications.DataBind();
		}
	}
}
