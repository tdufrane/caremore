﻿using System;
using Sitecore.Shell.Framework.Commands;

namespace Website.sitecore_modules.Admin
{
    [Serializable]
    public class EnrollmentAdmin : Command
    {
        public override void Execute(CommandContext context)
        {
            Sitecore.Layouts.PageContext pageContext = Sitecore.Context.Page;
            Uri uri = pageContext.Page.Request.Url;
            string hostName = uri.Host;

            string url = "http://" + hostName + "/sitecore modules/Admin/Login.aspx";
            Sitecore.Shell.Framework.Windows.RunUri(url, "", "Enrollment Administration");
        }
    }
}
