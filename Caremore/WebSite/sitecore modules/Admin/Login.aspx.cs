﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Website.sitecore_modules.Admin
{
	public partial class Login : BaseAdminLogin
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			this.AllowType = AllowTypes.Enrollment;
			this.RedirectPath = "Admin/";
			this.RedirectUrl = "ApplicationList.aspx";
			this.SessionName = "AdminUserName";

			CheckLoggedIn();
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				AuthenticateLogin(txtUserName.Text, txtPassword.Text, cbSaveLogin, phError);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}
	}
}
