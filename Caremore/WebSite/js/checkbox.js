﻿function CheckAllCheckBoxes(aspCheckBoxID, checkVal)
{
	re = new RegExp(aspCheckBoxID)  //generated control name starts with a colon
	for(i = 0; i < document.forms[0].elements.length; i++)
	{
		elm = document.forms[0].elements[i]
		if (elm.type == 'checkbox')
		{
			if (re.exec(elm.name))
			{
				elm.checked = checkVal
			}
		}
	}
}
