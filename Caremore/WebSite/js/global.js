﻿$(document).ready(function () {
	$('#navbar-collapse-1').on('show.bs.collapse', function () {
		if (window.innerWidth <= 768) {
			$('#navbar-collapse-1').append($('#topBarNav').html());
			$('#navbar-collapse-1 ul').last().addClass('nav navbar-nav');
		}
	});

	// Removes the top menu from main navigation when collasping mobile menu
	$('#navbar-collapse-1').on('hide.bs.collapse', function () {
		if ($('#navbar-collapse-1 ul').length > 1)
			$('#navbar-collapse-1 ul:last-child').remove();
	});

	// Fix for uncollapsed mobile menu appended top menu still showing when resized
	$(window).on('resize', function () {
		if (window.innerWidth >= 768) {
			$('#navbar-collapse-1').collapse('hide');
		}
		fontSizePersistence();
		menuPosition();
	});

	$('#largeTextTool').click(function (e) {
		e.preventDefault();
		$('body').addClass('large');
		//add a cookie for storing large font size
		$.cookie("style", "large", { expires: 365 });
	});
	$('#smallTextTool').click(function (e) {
		e.preventDefault();
		$('body').removeClass('large');
		//add a cookie for storing small font size
		$.cookie("style", "", { expires: 365 });
	});

	$(document).on('change', '.spinnerDdl', function () {
		$('.spinnerDisplay').show();
		$('.hideOnSpinner').hide();
	});

	$(document).on('click', '.spinnerBtn', function () {
		$('.spinnerDisplay').show();
		$('.hideOnSpinner').hide();
	});

	fontSizePersistence();
	menuPosition();
});
// End $(document).ready

// Movement of top menu depending on screen size
function menuPosition() {
	if (window.innerWidth < 768) {
		$('#navBarMain').insertBefore('header');
	}
	else {
		$('#navBarMain').insertAfter('#pnlHeaderBanner');
	}
}

// Set the appropriate base font size based on user history
function fontSizePersistence() {
	if (window.innerWidth < 768) {
		$('body').removeClass('large');
	}
	else {
		var cookie = $.cookie("style");

		if (cookie == "large") {
			$('body').addClass('large');
		}
		else {
			$('body').removeClass('large');
		}
	}
}

// Plan page
var enrollErrorLevel = 0;
function validateEnroll(errorLevel) {
	var result = true;

	if (errorLevel > enrollErrorLevel) {
		enrollErrorLevel = errorLevel;
	}

	if (enrollErrorLevel > 0) {
		if ($('.ddlCounty').val() == "") {
			$(".countyErrorMessage").show();
			result = false;
		}
		else {
			$(".countyErrorMessage").hide();
		}

		if ($('.ddlPlan').val() == "") {
			$(".planErrorMessage").show();
			result = false;
		}
		else {
			$(".planErrorMessage").hide();
		}
	}

	return result;
}
function loadEnroll(sender, args) {
	validateEnroll(0);
}

// Video player
var players = new Array();

function playVideo(controlId, imagePath, videoPath, width, height) {
	var curPlayer = jwplayer(controlId).setup({
		'flashplayer': '/flash/player5.swf',
		'image': imagePath,
		'file': videoPath,
		'autostart': false,
		'controlbar': 'over',
		'controlbar.idlehide': 'true',
		'fullscreen': 'true',
		'stretching': 'fill',
		'skin': '/flash/skewd.zip',
		'width': width,
		'height': height,
		events: {
			onPlay: function (event) {
				//stop other videos to play
				for (var i = 0; i < players.length; i++) {
					var curPlayer = players[i];

					if (curPlayer != this && curPlayer.getState() == "PLAYING") {
						curPlayer.pause();
					}
				}
			}
		}
	});

	players.push(curPlayer);
}


// jQuery call AJAX page method
function pageMethod(url, data, successFn, errorFn, complete) {
	//Call the page method
	$.ajax({
		type: "POST",
		url: url,
		data: data,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: successFn,
		error: errorFn,
		complete: complete
	});
}
