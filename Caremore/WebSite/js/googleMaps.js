﻿// Google Map on Region Google Map with services
var geocoder;
var map;
var infoWindow;
var firstLocation;

function gMapInitialize(initZoom) {
	geocoder = new google.maps.Geocoder();
	infoWindow = new google.maps.InfoWindow();
	markers = new Array();

	var myOptions = {
		zoom: initZoom,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}

	map = new google.maps.Map(document.getElementById("mapCanvas"), myOptions);

	for (var i = 0; i < locations.length; i++) {
		var item = locations[i].split('|');
		var address = item[0];
		var facilityName = item[1];
		var facilityTypeImage = item[2];
		var facilityTypeName = item[3];
		var facilityPhone = item[4];
		var itemUrl = item[5];

		geoCodeAddress(address, facilityName, facilityTypeImage, facilityTypeName, facilityPhone, itemUrl);
	}

	// center map after x time using first location found
	setTimeout(function () {
		map.setCenter(firstLocation);
	}, 2000);
}

function saveGeoLocation_OnSucceeded(result) {
	// geo location successfully cached
}

function saveGeoLocation_OnFailed(result) {
	// geo location failed to save
}

function getGeoLocation_OnFailed(result) {
	// cannot retrieve geo location
}

function geoCodeAddress(address, facilityName, facilityTypeImage, facilityTypeName, facilityPhone, itemUrl) {
	pageMethod('/services/geocode.asmx/GetGeoLocation',
		"{ 'address': '" + address + "' }",
		function (location) { // success
			if (location.d != undefined && location.d != 'null') {
				var lat = parseFloat(location.d.split(',')[0]);
				var lng = parseFloat(location.d.split(',')[1]);
				setMarker(address, facilityName, facilityTypeImage, facilityTypeName, facilityPhone, null, lat, lng, itemUrl);
			} else {
				geocoder.geocode({ 'address': address }, function (results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						setMarker(address, facilityName, facilityTypeImage, facilityTypeName, facilityPhone, results[0].geometry.location, null, null, itemUrl);
						pageMethod('/services/geocode.asmx/SaveGeoLocation',
							"{ 'address': '" + address + "', 'location': '" + results[0].geometry.location + "' }",
							saveGeoLocation_OnSucceeded,
							saveGeoLocation_OnFailed);
					} else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
						setTimeout(function () {
							geoCodeAddress(address, facilityName, facilityTypeImage, facilityTypeName, facilityPhone, itemUrl);
						}, 1000);
					} else {
						console.warn("Geocode was not successful for " + address + " the following reason: " + status);
					}
				});
			}
		},
		getGeoLocation_OnFailed,
		function () {
			// completed
		});
}

function setMarker(address, facilityName, facilityTypeImage, facilityTypeName, facilityPhone, loc, lat, lng, itemUrl) {
	var location;

	if (loc != undefined) {
		location = loc;
	} else {
		location = new google.maps.LatLng(lat, lng);
	}

	if (firstLocation == null) firstLocation = location;

	if (facilityTypeImage == "#") {
		var marker = new google.maps.Marker({
			map: map,
			title: address,
			animation: google.maps.Animation.DROP,
			position: location
		});
	}
	else {
		var marker = new google.maps.Marker({
			map: map,
			title: address,
			animation: google.maps.Animation.DROP,
			position: location,
			icon: facilityTypeImage
		});
	}

	marker.setMap(map);
/*
	google.maps.event.addListener(marker, 'click', function () {
		infoWindow.close();
		if (itemUrl != null && itemUrl != "") {
			infoWindow.setContent("<div style=\"margin-bottom:2px;\"><strong>" + facilityName +
				"</strong><div style=\"color:#666;\">" + facilityTypeName +
				"</div></div><div>" + address + "<br/>" + facilityPhone + "</div><div><p style='float:left;'><a href='" + itemUrl + "'>See Details</a></p><p style='float:right;'><a href=\"javascript:var r = confirm('You are now leaving CareMore.com to enter a third party site.');if (r == true) {window.open('http://maps.google.com/maps?q=to:" + address + "', '_blank');}\">Get Directions</a></div>");
		}
		else {
			infoWindow.setContent("<div style=\"margin-bottom:2px;\"><strong>" + facilityName +
				"</strong><div style=\"color:#666;\">" + facilityTypeName +
				"</div></div><div>" + address + "<br/>" + facilityPhone + "</div><div><a href=\"javascript:var r = confirm('You are now leaving CareMore.com to enter a third party site.');if (r == true) {window.open('http://maps.google.com/maps?q=to:" + address + "', '_blank');}\">Get Directions</a></div>");
		}
		infoWindow.open(map, marker);
	});*/
}
