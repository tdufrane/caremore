<%@ Page language="c#" %>
<%@ Import namespace="Sitecore.SeoToolkit.Service"%>

<script type="text/javascript">
    var linkDeleteImageUrl = '<%=Sitecore.Resources.Images.GetThemedImageSource("Network/16x16/link_delete.png", Sitecore.Web.UI.ImageDimension.id16x16)%>';
</script>

<script type="text/C#" runat="server">
  protected override void OnLoad(EventArgs e)
  {
     base.OnLoad(e);

     Response.Write(new LinksService(FormHelper.GetFormData()).Respond());
  }
</script>
