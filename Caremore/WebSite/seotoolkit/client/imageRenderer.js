function seoImageRenderer() {}

seoImageRenderer.prototype.getReportHTML = function(element) {
  var height;
  var width;  
  var image = element.domElement;
  /* if image is smaller than 48x48px, include it in original size */
  if (image.getHeight() < 48 && image.getWidth() < 48) {
    height = image.getHeight();
    width = image.getWidth();
  }
  /* otherwise scale down, keeping the aspect ratio */
  else if (image.getHeight() >= image.getWidth()) {
    height = 48;
    var ratio = 48 / image.getHeight();
    width = image.getWidth() * ratio;
  }
  else {
    width = 48;
    var ratio = 48 / image.getWidth();
    height = image.getHeight() * ratio;
  }  
  
  var template = new Template('' +
  '<div class="reportRow imageRow" onclick="seo.focus(\'#{controlID}\'); return false;">' +
      '<div class="imageContainer">' +
        '<img class="imageThumbnail" height="' + height + '" width="' + width + '" border="0" src="#{src}" />&nbsp;' +
      '</div>' +
      '<div class="reportContainer">' +
        this.getInfoHTML(element) +
      '</div><div style="clear:left; height:0"><!--ie--></div>' +      
  '</div>');

  return template.evaluate(element);
}

seoImageRenderer.prototype.getTooltipHTML = function(element) {  
  var templateString = '';
  
  if (element.isValid()) {
    templateString += '<p class="tooltipHeader">' + localizationDictionary["IMAGE"] + '</p>';
  } else {
    var errors = element.getErrors();
    templateString += '<p class="tooltipHeader errorHeader">' + 
        localizationDictionary["IMAGE"] + ': ' + errors.length + ' ' + 
        (errors.length > 1 ? localizationDictionary["ERRORS"] : localizationDictionary["ERROR"]) + '</p>';
  }
  
  templateString += '<div class="tooltipBody">' + this.getInfoHTML(element) + '</div>';  
  
  return new Template(templateString).evaluate(this);
}

seoImageRenderer.prototype.getInfoHTML = function(element) {
  var templateString = '';
  
  if (!element.isValid()) {
    templateString += '<div class="validationErrors">';

    var errorTemplate = new Template('<p class="errorInfo">#{text} <a class="help" href="#{helpLink}" target="_blank"><img src="' + 
        seo.page.baseUrl + helpButtonUrl + '" alt="' + localizationDictionary["SHOW_HELP"] + '" /></a></p>');
    element.getErrors().each(function(error) {
      templateString += errorTemplate.evaluate(error);
    });
    
    templateString += '</div>';
  }
  
  templateString += '' +
    '<p><span class="key">' + localizationDictionary["ALTERNATE_TEXT"] + ':</span><span class="value">#{alt}</span></p>' +
    '<p><span class="key">' + localizationDictionary["LOCATION"] + ':</span><span class="value">#{src}</span></p>' +
    '<p><span class="key">' + localizationDictionary["DIMENSIONS"] + 
      ':</span><span class="value">#{width} x #{height} ' + localizationDictionary["PIXELS"] + '</span></p>';
    
  if (element.hasDuplicates()) {
    templateString += '<p><span class="key">' + localizationDictionary["OCCURRENCES"] + 
      ':</span><span class="value">' + element.occurrences() + '</span></p>';
  }
    
  var template = new Template(templateString);    
  return template.evaluate(element);
}