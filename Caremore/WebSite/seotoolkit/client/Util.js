function seoUtility() {
}

seoUtility.prototype = {
  innerText: function(element) {
    var clone = element.cloneNode(true);
    clone = this.removeDots(clone);
    clone = this.removeCommands(clone);

    var result = '';

    if (clone.textContent) {
      result = clone.textContent;
    }
    else {
      result = clone.innerText;
    }

    if (!result) {
      result = localizationDictionary["NO_TEXT_DEFINED"];
    }

    return result;
  },
  
  removeCommands: function(node) {
    var commands = [];
    this.getCommandsFrom(node, commands);
    
    commands.each(function(command) {
      command.parentNode.removeChild(command);
    });
    
    return node;
   },

  removeDots: function(node) {
    var dots = [];
    this.getDotsFrom(node, dots);

    dots.each(function(dot) {
      dot.parentNode.removeChild(dot);
    });

    return node;
  },

  getCommandsFrom: function(node, result) {
    for (var i = 0; i < node.childNodes.length; i++) {
      var child = $(node.childNodes[i]);
      
      if (this.isCommandNode(child)){
        result.push(child);
      }
      else {
        this.getCommandsFrom(child, result);
      }
    }
  },

  getDotsFrom: function(node, result) {
    for (var i = 0; i < node.childNodes.length; i++) {
      var child = $(node.childNodes[i]);
      if (this.isWebEditNode(child)) {
        result.push(child);
      }
      else {
        this.getDotsFrom(child, result);
      }
    }
  },

  isCommandNode: function(node) {
    if (node.nodeName == "SPAN" && node.attributes["class"] != null && node.attributes["class"].value == "scChromeData") {
      return true;
    }

    return false;
  },

  isWebEditNode: function(node) {
    if (node.nodeName == 'LABEL' && node.getStyle('position') == 'absolute') {
      return node.getElementsBySelector("//img[src~=bullet_ball_glass]").length > 0 || node.getElementsBySelector("//img[src~=/sitecore/images/blank.gif]").length > 0
    }
  },

  isWebEditImage: function(image) {
      return image.src.indexOf('bullet_ball_glass') > 0 || image.src.indexOf('/sitecore/images/blank.gif') > 0 || image.src.indexOf('/PageEditor/') > 0;
  },

  preprocessJson: function(text) {
    if (text.indexOf('<') > 0) {
      return text.substring(0, text.indexOf('<'));
    }

    return text;
  },

  getCookie: function(c_name) {
    if (document.cookie.length > 0) {
      c_start = document.cookie.indexOf(c_name + "=");

      if (c_start != -1) {
        c_start = c_start + c_name.length + 1;
        c_end = document.cookie.indexOf(";", c_start);

        if (c_end == -1) {
          c_end = document.cookie.length;
        }

        return unescape(document.cookie.substring(c_start, c_end));
      }
    }

    return "";
  }
};