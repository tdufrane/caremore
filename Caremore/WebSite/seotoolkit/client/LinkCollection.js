var SeoLinkCollection = CollectionBase.extend({
  constructor: function() {
    this.base();

    SeoLink.prototype.renderer = new seoLinkRenderer();
    SeoLink.prototype.validators = new Array();
    SeoLink.prototype.validators.push(new linkSelfLinkingValidator());
    SeoLink.prototype.validators.push(new brokenLinkValidator());

    this.reportContainer = $('seoValidLinksPlaceholder');
    this.brokenLinksContainer = $('seoInvalidLinksPlaceholder');
    this.linkLoadingContainer = $('seoLinksLoadingPlaceholder');
  },

  getURLParam: function(param) {
    var urlParam = new String; 					// Declare the return variable as a string
    urlParam = false;
    var url = window.location.href; 					// Get the url
    if (url.indexOf("?") != -1) {						// Check if the url contains a '?'
      var str = new Array();
      str = url.split('?'); 					// Split the url...
      url = str[1]; 						// ...and store the stuff after the '?'

      if (url.indexOf(param) != -1) {					// Check if the new url contains the desired parameter
        if (url.indexOf("&") != -1) {				// Check if the url contains '&'...
          var str = new Array();
          str = url.split('&'); 			// ...then split it into an array...
          for (var i = 0; i < str.length; i++) {
            if (str[i].indexOf(param) != -1) {
              var str2 = new Array(); 	// Make a new array to test if the parameter is
              str2 = str[i].split('='); // equal to variable param and not the value
              if (str2[0] == param) {
                url = str[i]; 	// ...and omit the rest
                break;
              }
            }
          }
        }
        var str = new Array(); 				// We now have only the parameter and its attribute
        str = url.split('='); 				// Split the url at the '='...
        if (str[0] == param) {
          urlParam = str[1]; 			// ...and store just the attribute
        }
      }
    }
    return urlParam; 						// Return: (bool)false | (string)attribute
  },

  parse: function() {
//    var urlParameterValue = this.getURLParam("url");

//    if (urlParameterValue == false) {
      $$('body a').each(
        function(element) {
          if (element.up('#extender') == null) {
            this.elements.push(new SeoLink(element));
          }
        } .bind(this));
//    }
//    else {
//      $$('body a').each(
//        function(element) {
//          if (element.hostname == window.location.hostname) {

//            //            var aElement = document.createElement('a');
//            var aElement = new Object();
//            aElement.href = urlParameterValue + '/' + element.nameProp
//            aElement.text = seo.util.innerText(element);

//            this.elements.push(new SeoLink(aElement));
//          }
//          else {
//            if (element.up('#extender') == null) {
//              this.elements.push(new SeoLink(element));
//            }
//          }
//        } .bind(this));
//    }
  
    console.log('%s links parsed', this.length());

    this.serviceLookup();
  },

  serviceLookup: function() {
    console.group('links service lookup');

    var linksToLookup = this.getLinksToLookup();

    if (linksToLookup.length > 0) {
      var postData = this.getLookupData(linksToLookup, null);

      seo.registerProgress('links service');

      // Display all the links and attach overlays to each of the link
      seo.page.links.renderReport();
      seo.page.links.setInvalidWebLinksInVerifiedState();
      seo.page.links.attachOverlays();

      this.linkLoadingContainer.innerHTML =
            '<p style="height: 26px; line-height: 26px; vertical-align: middle;">' +
            '<img src="' + seo.page.baseUrl +
                '/seotoolkit/client/img/progress.gif" style="height: 26px; vertical-align: middle;" />' +
            '<b>' + localizationDictionary["LOADING_LINKS"] + '</b></p>';

      // Unregister progress for the purpose to release analyze page loading
      seo.unregisterProgress('links service');

      var jobID = -1;
      this.isFinishedCurrentJob = false;

      // First request to the server
      new Ajax.Request(seo.page.baseUrl + '/seotoolkit/service/links.aspx', {
        postBody: postData,
        onSuccess: function(transport) {
          this.onServiceSuccess(transport);
        } .bind(this),
        onException: function(transport, error) {
          throw error;
        },
        onFailure: function(transport) {
          console.error('links service failure');
          this.reportContainer.innerHTML = seo.formatError(localizationDictionary["ERROR_ANALYZING_LINKS_ON_PAGE"]);
          seo.unregisterProgress('links service');
        } .bind(this)
      });

      return true;
    }
    else {
      console.info('no links to lookup');
      this.isFinishedCurrentJob = true;
      this.renderReport();
    }

    console.groupEnd();
  },

  onServiceSuccess: function(transport) {
    console.log('links service call success');

    seo.page.links.parseServiceResponse(transport.responseText);

    if (!this.isFinishedCurrentJob) {
      postData = this.getLookupData(null, jobID);

      new Ajax.Request(seo.page.baseUrl + '/seotoolkit/service/links.aspx', {
        postBody: postData,
        onSuccess: function(transport) {
          this.onServiceSuccess(transport);
        } .bind(this),
        onException: function(transport, error) {
          throw error;
        },
        onFailure: function(transport) {
          console.error('links service failure');
          this.reportContainer.innerHTML = seo.formatError(localizationDictionary["ERROR_ANALYZING_LINKS_ON_PAGE"]);
          seo.unregisterProgress('links service');
        } .bind(this)
      });
    }
    else {
      var seoLinksInformationLine = $("seoLinksInfomationLine");

      if (seoLinksInformationLine) {
        seoLinksInformationLine.replace(seo.page.information.links.toHTML());
      }

      this.linkLoadingContainer.innerHTML = "";
    }
  },

  parseServiceResponse: function(responseText) {
    if (responseText) {
      responseText = seo.util.preprocessJson(responseText);

      try {
        eval('var evalResult = ' + responseText);

        jobID = evalResult.jobID;

        if (evalResult.isFinishedCurrentJob == "true") {
          this.isFinishedCurrentJob = true;
        }
        else {
          this.isFinishedCurrentJob = false;
        }

        var linksCollection = evalResult.links;
      }
      catch (e) {
        this.reportContainer.innerHTML = seo.formatError(localizationDictionary["UNEXPECTED_SERVICE_RESPONSE"]);
        console.error('error parsing server respone:');
        console.log(responseText);

        isFinishedCurrentJob = true;
        return false;
      }

      linksCollection.each(function(linkInfo) {
        var element = seo.findControl(linkInfo.controlID);

        if (!element.serverInfo) {
          element.serverInfo = linkInfo;
          $("row_" + linkInfo.controlID).className = "reportRow link linkProcessed kulink_#{type}";

          if (!element.isValid()) {
            var transferingElement = $("row_" + linkInfo.controlID);
            var a = transferingElement.down("a");

            var errorHtml = element.renderer.getErrorBlock(element);

            a.insert({ top: errorHtml });

            var invalidLinksDiv = $("seoInvalidLinksPlaceholder");
            invalidLinksDiv.insert(transferingElement);
          }
        }
      });

      return true;
    }
    else {
      console.error('links service response empty');
    }
  },

  getLookupData: function(links, jobID) {

    var result = '<postData>';

    if (jobID != null) {
      result += '<jobID>';
      result += jobID;
      result += '</jobID>';
    }

    if (links != null) {
      result += '<links>';
      links.each(function(link) {
        result += new Template('<link controlID="#{controlID}"><![CDATA[#{href}]]></link>').evaluate(link);
      });
      result += '</links>';
    }

    result += '</postData>';

    return result;
  },

  setInvalidWebLinksInVerifiedState: function() {
    return this.elements.findAll(function(link) {
      if (link.type == 'anchor' || !link.isWebLink()) {
        $("row_" + link.controlID).className = "reportRow link linkProcessed kulink_#{type}";
      }
    });
  },

  getLinksToLookup: function() {
    return this.elements.findAll(function(link) {
      return link.type != 'anchor' && link.isWebLink();
    });
  },

  renderReport: function() {
    var result = '';

    if (this.elements.length > 0) {
      this.elements.each(function(link) {
        result += link.renderer.getReportHTML(link);
      });
    }
    else {
      result += seo.formatNoElements(localizationDictionary["NO_LINKS_ON_PAGE"]);
    }

    this.reportContainer.innerHTML = result;
  }
});

/* validators */
function linkSelfLinkingValidator() {}
linkSelfLinkingValidator.prototype.getErrors = function(link) {
  var result = new Array();
  
  if (link.type != 'anchor' && link.href == seo.page.href) {
    result.push({ text: localizationDictionary["LINK_LEADING_TO_CURRENT_PAGE"], helpLink: 'http://sdn.sitecore.net/Products/Seo%20Toolkit/Possible%20Errors/Links/Link%20To%20Current%20Page.aspx' });
  }
  
  return result;
}

function brokenLinkValidator() {}
brokenLinkValidator.prototype.getErrors = function(link) {
  var result = new Array();

  if (link.serverInfo && link.serverInfo.broken) {
    result.push({ text: localizationDictionary["BROKEN_LINK"] + ": " + link.serverInfo.reason.gsub('-quot-', '\'') + '.', helpLink: 'http://sdn.sitecore.net/Products/Seo%20Toolkit/Possible%20Errors/Links/Broken%20Link.aspx' });
  }
  
  return result;
}