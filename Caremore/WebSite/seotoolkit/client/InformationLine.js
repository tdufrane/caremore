var SeoInformationLine = Base.extend({
  constructor: function(title, evaluator) {
    this.title = title;
    this.evaluator = evaluator;    
  },

  getTemplate: function() {

    var idString = this.id ? " id=\"" + this.id + "\"" : "";
    
    var templateString = '<div' + idString + '>';
    
    if (!this.error) {
      templateString += '<p class="information">';    
    } else {
      templateString += '<p class="information seoError">';
    }
    
    if (this.click) templateString += '<a href="#" onclick="' + this.click + '; return false">';

    templateString += '<span class="key">#{title}:</span>';
    templateString += '<span class="value">#{text}</span>';
    if (this.click) templateString += '</a>';
    if (this.helpLink) {
        templateString += '<a class="help" href="#{helpLink}" title="' + 
          localizationDictionary["OPEN_HELP_FOR_ERROR_IN_NEW_WINDOW"] + '" target="_blank"><img src="' +
          seo.page.baseUrl + helpButtonUrl + '" alt="' + localizationDictionary["SHOW_HELP"] + '" /></a>';
    }
    templateString += '</p>';
    
    return new Template(templateString);    
  },
  
  toHTML: function() {
    this.evaluator.bind(this)();
    return this.getTemplate().evaluate(this);    
  }  
});