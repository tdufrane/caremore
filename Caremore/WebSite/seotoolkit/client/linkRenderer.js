function seoLinkRenderer() {
}

seoLinkRenderer.prototype.getReportHTML = function(link) {
  var templateString = '<div id="row_#{controlID}" class="reportRow link linkProcessing kulink_#{type}">' +
                       '<a href="#" onclick="seo.focus(\'#{controlID}\'); return false;">';
  
  //templateString += this.getErrorBlock(link);
    
  templateString += '<p>#{text} [#{href}]</p>';
  templateString += '</a></div>';
  
  return new Template(templateString).evaluate(link);
}

seoLinkRenderer.prototype.getTooltipHTML = function(link) {
  var templateString = '';
  
  if (link.isValid()) {
    templateString += '<p class="tooltipHeader">' + localizationDictionary["LINK"] + '</p>';
  } else {
    var errors = link.getErrors();
    templateString += '<p class="tooltipHeader errorHeader">' + localizationDictionary["LINK"] + 
      ': ' + errors.length + ' ' + (errors.length > 1 ? localizationDictionary["ERRORS"] : localizationDictionary["ERROR"]) + '</p>';
  }
  
  templateString += '<div class="tooltipBody">'; 
  
  templateString += this.getErrorBlock(link);  
  templateString += '' +
    '<p><span class="key">' + localizationDictionary["TEXT"] + ':</span><span class="value">#{text}</span></p>' +
    '<p><span class="key">' + localizationDictionary["LOCATION"] + ':</span><span class="value">#{href}</span></p>';

  templateString += '</div>';  
  
  return new Template(templateString).evaluate(link);
}

seoLinkRenderer.prototype.getErrorBlock = function(link) {
  //if (link.isValid()) return '';

  var errorTemplate = new Template('<p class="errorInfo">#{text} <a class="help" href="#{helpLink}" target="_blank"><img src="' +
    seo.page.baseUrl + helpButtonUrl + '" alt="' + localizationDictionary["SHOW_HELP"] + '" /></a></p>');
  var result = '<div class="validationErrors">';
  
  link.getErrors().each(function(error) {
    result += errorTemplate.evaluate(error);
  });
  
  result += '</div>';
  return result;
}