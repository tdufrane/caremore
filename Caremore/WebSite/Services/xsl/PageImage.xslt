﻿<?xml version="1.0" encoding="UTF-8"?>

<!--==============================================================-->
<!-- File: PageImage.xslt                                         -->
<!-- Created by: Rob Kogan                                        -->
<!-- Created: 09/12/2008                                          -->
<!--==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  xmlns:dot="http://www.sitecore.net/dot"
  exclude-result-prefixes="dot sc">

  <!-- output directives -->
  <xsl:output method="html" indent="no" encoding="UTF-8" />

  <!-- parameters -->
  <xsl:param name="lang" select="'en'"/>
  <xsl:param name="id" select="''"/>
  <xsl:param name="sc_item"/>
  <xsl:param name="sc_currentitem"/>

  <!-- variables -->
  <xsl:variable name="home" select="/*/item[@key='content']/item[@key='home']" />

  <!-- entry point -->
  <xsl:template match="*">
    <xsl:apply-templates select="$sc_item" mode="main"/>
  </xsl:template>

  <!--==============================================================-->
  <!-- main                                                         -->
  <!--==============================================================-->
  <xsl:template match="*" mode="main">
    <xsl:if test="sc:fld('image',.,'src')!=''">
      <!-- Page Image -->
      <img id="contentImage" src="{sc:fld('image',.,'src')}" alt="{sc:fld('image',.,'alt')}" align="left" />
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>

