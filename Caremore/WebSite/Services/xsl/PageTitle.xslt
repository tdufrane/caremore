﻿<?xml version="1.0" encoding="UTF-8"?>

<!--==============================================================-->
<!-- File: PageTitle.xslt                                         -->
<!-- Created by: Admin                                            -->
<!-- Created: 07/29/2008 10:23 AM                                 -->
<!--==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  xmlns:dot="http://www.sitecore.net/dot"
  exclude-result-prefixes="dot sc">

  <!-- output directives -->
  <xsl:output method="html" indent="no" encoding="UTF-8" />

  <!-- parameters -->
  <xsl:param name="lang" select="'en'"/>
  <xsl:param name="id" select="''"/>
  <xsl:param name="sc_item"/>
  <xsl:param name="sc_currentitem"/>

  <!-- variables -->
  <xsl:variable name="home" select="/*/item[@key='content']/item[@key='home']" />

  <!-- entry point -->
  <xsl:template match="*">
    <xsl:apply-templates select="$sc_item" mode="main"/>
  </xsl:template>

  <!--==============================================================-->
  <!-- main                                                         -->
  <!--==============================================================-->
  <xsl:template match="*" mode="main">
    <h1>
      <xsl:call-template name="GetItemDisplayName">
        <xsl:with-param name="item" select="."/>
      </xsl:call-template>
    </h1>
  </xsl:template>

  <xsl:template name="GetItemDisplayName">
    <xsl:param name="item" select="." />
    <xsl:choose>
      <xsl:when test="sc:fld('title',$item) and sc:fld('title',$item)!=''">
        <xsl:value-of select="sc:fld('title',$item)" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$item/@name" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>

