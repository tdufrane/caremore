﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

namespace Website.Services
{
    /// <summary>
    /// Summary description for GeoCode
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class GeoCode : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        [ScriptMethod]
        public string GetGeoLocation(string address)
        {
            var obj = HttpContext.Current.Cache[address];

            if (obj != null)
            {
                return obj.ToString();
            }

            
            return null;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod]
        public string SaveGeoLocation(string address, string location)
        {
            MapPinLocationHelper.SaveGeoLocation(address, location);

            return location;
        }
    }
}