﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="OneColumn.ascx.cs" Inherits="Website.Sublayouts.OneColumn" %>

<!-- Begin SubLayouts/OneColumn -->
<asp:Panel ID="pnlTitle" runat="server" CssClass="row">
	<div class="col-xs-12">
		<h1><sc:Text ID="txtTitle" runat="server" Field="Title" /></h1>
	</div>
</asp:Panel>

<asp:Panel ID="pnlSubtitle" runat="server" CssClass="row">
	<div class="col-xs-12">
		<h2><sc:Text ID="txtSubtitle" runat="server" Field="Subtitle" /></h2>
	</div>
</asp:Panel>

<asp:Panel ID="pnlContentImage" runat="server" CssClass="row">
	<div class="col-xs-12">
		<sc:Image ID="imgContent" runat="server" Field="Image" />
	</div>
</asp:Panel>

<asp:Panel ID="pnlContent" runat="server" CssClass="row">
	<div class="col-xs-12">
		<sc:Text ID="txtContent" runat="server" Field="Content" />
	</div>
</asp:Panel>

<sc:Placeholder ID="phCustomLayout" runat="server" key="custom-layout"/>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
<!-- End SubLayouts/OneColumn -->
