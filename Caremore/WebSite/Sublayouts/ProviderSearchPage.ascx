﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ProviderSearchPage.ascx.cs" Inherits="Website.Sublayouts.ProviderSearchPage" %>

<!-- Begin SubLayouts/ProviderSearchPage -->
<div class="row">
	<div class="col-xs-12">
		<h2><cm:Text ID="txtSearchForProvider" runat="server" ItemReferenceName="SEARCH_FOR_PROVIDER" Field="Text"/></h2>

		<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
			<div class="form-group">
				<label for="ddlSpecialties" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtSpecialtyLabel" runat="server" Field="Specialty Label" /></label>
				<div class="col-xs-12 col-md-5">
					<asp:ListBox ID="ddlSpecialties" runat="server" CssClass="form-control" SelectionMode="Multiple" Rows="10" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtLastName" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtLastNameLabel" runat="server" Field="Last Name Label" /></label>
				<div class="col-xs-12 col-md-5">
					<asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" MaxLength="100" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtZipCode" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtZipCodeLabel" runat="server" Field="Zip Code Label" /></label>
				<div class="col-xs-12 col-md-5">
					<asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control" MaxLength="5" placeholder="#####" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtCity" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtCityLabel" runat="server" Field="City Label" /></label>
				<div class="col-xs-12 col-md-5">
					<asp:TextBox ID="txtCity" runat="server" CssClass="form-control" MaxLength="30" />
				</div>
			</div>
			<div class="form-group">
				<label for="ddlStates" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtStateLabel" runat="server" Field="State Label" /></label>
				<div class="col-xs-12 col-md-5">
					<asp:DropDownList ID="ddlStates" runat="server" AppendDataBoundItems="true" class="form-control">
						<asp:ListItem Text="All" Value=""/>
					</asp:DropDownList>
				</div>
			</div>
			<div class="form-group">
				<label for="ddlLanguages" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtLanguageLabel" runat="server" Field="Language Label" /></label>
				<div class="col-xs-12 col-md-5">
					<asp:ListBox ID="ddlLanguages" runat="server" SelectionMode="Multiple" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12 col-md-offset-3 col-md-4">
					<asp:LinkButton ID="btnSubmit" runat="server" OnClick="BtnSubmit_OnClick" CssClass="btn btn-primary" />
				</div>
			</div>
		</asp:Panel>

		<div class="row">
			<div class="col-xs-12">
				<sc:Text ID="txtProviderContent" runat="server" Field="Provider Content" />
			</div>
		</div>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End SubLayouts/ProviderSearchPage -->
