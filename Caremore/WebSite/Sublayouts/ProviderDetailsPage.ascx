﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ProviderDetailsPage.ascx.cs" Inherits="Website.Sublayouts.ProviderDetailsPage" %>
<%@ Register TagPrefix="cm" TagName="GoogleMap" Src="~/Controls/GoogleMap.ascx" %>

<!-- Begin SubLayouts/ProverDetailsPage -->
<div class="row">
	<div id="col-xs-12">
		<h2><asp:Label ID="lblPhysicianText" runat="server" /></h2>

		<div class="row">
			<div class="col-xs-6 col-md-4">
				<asp:LinkButton ID="BackToResultsLB" runat="server" OnClick="BackToResultsLB_Click"><span class="glyphicon glyphicon-chevron-left"></span>
				<sc:Text ID="lblBack" runat="server" Field="Back To Results" /></asp:LinkButton>
			</div>
			<div class="col-xs-6 col-md-2">
				<a href="#" onclick="window.print()" class="pull-right"><sc:Text ID="lblPrint" runat="server" Field="print" />
					<span class="glyphicon glyphicon-print"></span></a>
			</div>
		</div>

		<hr />

		<asp:PlaceHolder ID="phDetails" runat="server">
			<div class="row">
				<div class="col-xs-12 col-md-4">
					<h4><sc:Text ID="ContactInfoLabel" runat="server" Field="Contact Info" /></h4>

					<div class="row">
						<div class="col-xs-4"><em><sc:Text ID="PhoneLabel" runat="server" Field="Phone" />:</em></div>
						<div class="col-xs-8"><asp:Label ID="PhoneText" runat="server" /></div>
					</div>

					<asp:Panel ID="pnlFax" runat="server" CssClass="row">
						<div class="col-xs-4"><em><sc:Text ID="FaxLabel" runat="server" Field="Fax" />:</em></div>
						<div class="col-xs-8"><asp:Label ID="FaxText" runat="server" /></div>
					</asp:Panel>

					<hr />

					<h4><sc:Text ID="LocationLabel" runat="server" Field="Location" /></h4>

					<address>
						<asp:Literal ID="LocationText" runat="server" />
					</address>

					<p class="top-spacer"><asp:HyperLink ID="DirectionsLink" runat="server" Target="_blank">
						<sc:Text ID="GetDirectionsLabel" runat="server" Field="Get Directions" />
						<span class="glyphicon glyphicon-chevron-right"></span></asp:HyperLink></p>
				</div>

				<div class="col-xs-12 col-md-offset-1 col-md-6">
					<cm:GoogleMap ID="GoogleMap" runat="server" InitialZoom="13" />
				</div>
			</div>

			<hr />

			<div class="row">
				<div class="col-xs-12">
					<asp:Panel ID="EffectivePnl" CssClass="row" runat="server" Visible="false">
						<div class="row">
							<div class="col-xs-12 col-md-3"><strong><sc:Text ID="EffectiveDateLabel" runat="server" Field="Effective Date" /></strong></div>
							<div class="col-xs-12 col-md-9"><asp:Label ID="EffectiveDateText" runat="server" Font-Bold="true" /></div>
						</div>
					</asp:Panel>
					<div class="row">
						<div class="col-xs-12 col-md-3"><strong><sc:Text ID="SpecialtiesLabel" runat="server" Field="Specialties" /></strong></div>
						<div class="col-xs-12 col-md-9"><asp:Label ID="SpecialtiesText" runat="server" /></div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-3"><strong><sc:Text ID="LanguagesLabel" runat="server" Field="Languages" /></strong></div>
						<div class="col-xs-12 col-md-9">
							<asp:Label ID="LanguagesText" runat="server" />
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-3"><strong><sc:Text ID="AcceptingNewPatientsLabel" runat="server" Field="Accepting New Patients" /></strong></div>
						<div class="col-xs-12 col-md-9">
							<asp:Label ID="AcceptingNewPatientsText" runat="server" />
						</div>
					</div>
				</div>
			</div>

			<asp:Repeater ID="MedicalOffices" runat="server" OnItemDataBound="MedicalOffices_ItemDataBound">
				<HeaderTemplate>
					<hr />
					<h4 class="bottom-spacer"><sc:Text ID="MedicalOfficesLabel" runat="server" Field="Medical Offices" /></h4>
					<div id="providerAddress">
						<div class="row">
				</HeaderTemplate>
				<ItemTemplate>
						<asp:Panel ID="pnlState" runat="server" CssClass="state col-xs-12" Visible="false" />
						<asp:Panel ID="pnlCounty" runat="server" CssClass="county col-xs-12" Visible="false" />
						<div class="col-xs-12 col-md-4">
							<div class="address">
								<%# AddressHtml(Container.DataItem) %>
							</div>
						</div>
				</ItemTemplate>
				<FooterTemplate>
						</div>
					</div>
				</FooterTemplate>
			</asp:Repeater>
		</asp:PlaceHolder>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End SubLayouts/ProverDetailsPage -->
