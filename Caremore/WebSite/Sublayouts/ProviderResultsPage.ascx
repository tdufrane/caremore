﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ProviderResultsPage.ascx.cs" Inherits="Website.Sublayouts.ProviderResultsPage" %>

<!-- Begin SubLayouts/ProviderResultsPage -->
<asp:PlaceHolder ID="NewSearchPH" runat="server">
	<div class="newSearch clearfix">
		<asp:LinkButton ID="NewSearchLB" runat="server" CssClass="button btnMed newSearch"
			OnClick="NewSearchLB_OnClick" />
	</div>
</asp:PlaceHolder>

<asp:Panel ID="SearchResultsPnl" runat="server">
	<div class="searchResultsInfo clearfix">
		<div class="searchResultsNumberFound">
			<asp:Label ID="NumResultsLbl" runat="server" />
		</div>
		<div class="searchResultsPaging">
			<asp:DataPager ID="ItemsDP" runat="server" PagedControlID="ItemsLV" PageSize="25">
				<Fields>
					<asp:NextPreviousPagerField PreviousPageText="<<" ShowFirstPageButton="False" ShowPreviousPageButton="True"
						ShowNextPageButton="False" ShowLastPageButton="False"/>
					<asp:NumericPagerField />
					<asp:NextPreviousPagerField NextPageText=">>" ShowFirstPageButton="False" ShowPreviousPageButton="False"
						ShowNextPageButton="True" ShowLastPageButton="False" />
				</Fields>
			</asp:DataPager>
		</div>
	</div>

	<asp:ListView ID="ItemsLV" runat="server" OnPagePropertiesChanging="ItemsLV_PagePropertiesChanging"
		OnDataBound="ItemsLV_DataBound" OnItemDataBound="ItemsLV_ItemDataBound" OnLayoutCreated="ItemsLV_LayoutCreated">
		<LayoutTemplate>
			<table class="resultsTable hideOnSpinner" cellpadding="0" cellspacing="0" border="0">
				<tr valign="top">
					<th><asp:Label ID="lblPhysician" runat="server"></asp:Label></th>
					<th style="width:130px;"><asp:Label ID="lblSpecialties" runat="server"></asp:Label></th>
					<th><asp:Label ID="lblMedicalOffice" runat="server" /></th>
					<th style="width:85px;"><asp:Label ID="lblDistance" runat="server" /></th>
				</tr>
				<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
			</table>
		</LayoutTemplate>
		<ItemTemplate>
			<tr id="trSeparator" runat="server" visible="false">
				<td colspan="4">
					<hr />
				</td>
			</tr>
			<tr id="trItemRow" runat="server" valign="top">
				<td>
					<asp:HyperLink ID="NameHL" runat="server"></asp:HyperLink>
					<asp:Literal ID="IDLit" runat="server"></asp:Literal>
					<asp:Label ID="EffectiveLbl" runat="server" Font-Bold="true"></asp:Label>
					<asp:Label ID="AcceptingNewPatientsLbl" runat="server"></asp:Label>
				</td>
				<td>
					<asp:Literal ID="SpecialtiesLit" runat="server"></asp:Literal>
				</td>
				<td id="tdAddress" runat="server">
					<asp:Literal ID="AddressLit" runat="server"></asp:Literal>
				</td>
				<td id="tdDistance" runat="server">
					<asp:Literal ID="DistanceLit" runat="server"></asp:Literal>
				</td>
			</tr>
		</ItemTemplate>
	</asp:ListView>

	<hr />

	<div class="searchResultsInfo clearfix">
		<div class="searchResultsNumberFound">
			<asp:Label ID="NumResultsLbl2" runat="server" />
		</div>
		<div class="searchResultsPaging">
			<asp:DataPager ID="ItemsDP2" runat="server" PagedControlID="ItemsLV" PageSize="25">
				<Fields>
					<asp:NextPreviousPagerField PreviousPageText="PREVIOUS PAGE" ShowFirstPageButton="False"
						ShowPreviousPageButton="True" ShowNextPageButton="False" ShowLastPageButton="False" />
					<asp:NumericPagerField />
					<asp:NextPreviousPagerField NextPageText="NEXT PAGE" ShowFirstPageButton="False"
						ShowPreviousPageButton="False" ShowNextPageButton="True" ShowLastPageButton="False" />
				</Fields>
			</asp:DataPager>
		</div>
	</div>
</asp:Panel>

<asp:PlaceHolder ID="MessagePH" runat="server">
	<asp:Label ID="MessageLbl" runat="server" />
	<span class="red"><sc:Text Field="Text" ID="ProviderContact" runat="server" /></span>
</asp:PlaceHolder>
<!-- End SubLayouts/ProviderResultsPage -->
