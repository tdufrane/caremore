﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="VideoListPage.ascx.cs" Inherits="Website.Sublayouts.VideoListPage" %>

<!-- Begin SubLayouts/VideoListPage -->
<asp:Panel ID="pnlVideoItems" runat="server">
	<asp:Repeater ID="rptItems" runat="server" OnItemDataBound="RptItems_ItemDataBound">
		<HeaderTemplate>
			<div class="row videoRow">
		</HeaderTemplate>
		<ItemTemplate>
				<div class="col-xs-12 col-md-6 videoItem">
					<div class="row">
						<div class="col-xs-12"><sc:Text ID="txtDescription" runat="server" Field="Description" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></div>
					</div>
					<div class="row video">
						<div class="col-xs-12">
							<div class="embed-responsive embed-responsive-4by3">
								<video id="vidControl" runat="server" class="embed-responsive-item" controls="controls" preload="none">
									<source id="srcControl" runat="server" type="video/mp4" />
								</video>
							</div>
						</div>
					</div>
				</div>
		</ItemTemplate>
		<SeparatorTemplate>
			</div>
			<div class="row videoRow">
		</SeparatorTemplate>
		<FooterTemplate>
			</div>
		</FooterTemplate>
	</asp:Repeater>
</asp:Panel>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
<!-- End SubLayouts/VideoListPage -->
