﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="NewsStoryDetailPage.ascx.cs" Inherits="Website.Sublayouts.NewsStoryDetailPage" %>
<%@ Register Src="~/Controls/SideNav.ascx" TagPrefix="uc" TagName="SideNav" %>

<!-- Begin SubLayouts/NewsStoryDetailPage -->
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-3">
			<uc:SideNav id="ucSideNav" runat="server" />
		</div>
		<div class="col-xs-12 col-md-9">
			<div class="row">
				<div class="col-xs-12">
					<h1><sc:Text ID="txtContentTitle" runat="server" Field="Title" /></h1>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<h3><sc:Date ID="dtStoryDate" runat="server" Field="Story Date" Format="MM.dd.yy" />
						<sc:Text ID="txtStoryTitle" runat="server" Field="Title" /></h3>
				</div>
			</div>

			<asp:Panel ID="pnlSubtitle" runat="server" CssClass="row">
				<div class="col-xs-12">
					<h4><sc:Text ID="txtSubtitle" runat="server" Field="Subtitle" /></h4>
				</div>
			</asp:Panel>

			<div class="row">
				<div class="col-xs-12">
					<sc:Text ID="txtContent" runat="server" Field="Content" />
				</div>
			</div>
		</div>
	</div>
</div>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
<!-- End SubLayouts/NewsStoryDetailPage -->
