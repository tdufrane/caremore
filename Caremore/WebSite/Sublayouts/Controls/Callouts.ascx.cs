﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Forms;
using Sitecore.Form.Core;
using Website.BL.ListItem;
using Website.Sublayouts.Widgets;

namespace Website.Sublayouts.Controls
{
	public partial class Callouts : BaseUserControl
	{
		#region Properties

		public string WidgetCssClass { get; set; }

		#endregion

		#region Page methods

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RptItems_OnItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
				{
					BaseWidget widget = e.Item.FindControl("ucWidget") as BaseWidget;
					widget.CssClass = WidgetCssClass;
					widget.DetailItem = new DetailItem(e.Item.DataItem as Item);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			rptItems.DataSource = GetItems();
			rptItems.DataBind();
		}

		private List<Item> GetItems()
		{
			Item currentItem = Sitecore.Context.Item;

			while (currentItem != null)
			{
				MultilistField callouts = currentItem.Fields["Callouts"];
				if (callouts != null)
				{
					Item[] items = callouts.GetItems();

					if (items != null && items.Count() > 0)
					{
						return items.ToList();
					}
				}

				CheckboxField noInherit = currentItem.Fields["Callouts No Inherit"];
				if (noInherit != null && noInherit.Checked)
				{
					return new List<Item>();
				}

				currentItem = currentItem.Parent;
			}

			return new List<Item>();
		}

		#endregion
	}
}
