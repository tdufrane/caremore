﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="CalendarDetailPage.ascx.cs" Inherits="Website.Sublayouts.CalendarDetailPage" %>
<%@ Register TagPrefix="cm" TagName="GoogleMap" Src="~/Controls/GoogleMap.ascx" %>

<!-- Begin SubLayouts/CalendarDetailPage -->
<div class="row">
	<div class="col-xs-12">
<asp:PlaceHolder ID="phDetails" runat ="server">
		<div class="row">
			<div class="col-xs-6 col-md-4"><asp:HyperLink ID="hlCalendar" runat="server"><span class="glyphicon glyphicon-chevron-left"></span> <cm:Text ID="txtBackToResults" runat="server" Field="Back to Results Label" /></asp:HyperLink></div>
			<div class="col-xs-6 col-md-2"><a href="#" onclick="window.print();" class="link pull-right"><cm:Text ID="txtEventPrint" runat="server" Field="Print Button Label" />
				<span class="glyphicon glyphicon-print"></span></a></div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<hr />
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<h2><cm:Text ID="txtEventTitle" runat="server" Field="Event Title" /></h2>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="row">
					<div class="col-xs-4"><em><cm:Text ID="txtEventDay" runat="server" Field="Day Label" /></em></div>
					<div class="col-xs-8"><sc:Date ID="dtDay" runat="server" Field="Event Date" Format="dddd" /></div>
				</div>
				<div class="row">
					<div class="col-xs-4"><em><cm:Text ID="txtEventDate" Field="Date Label" runat="server" /></em></div>
					<div class="col-xs-8"><sc:Date ID="dtFull" runat="server" Field="Event Date" Format="MMM d, yyyy" /></div>
				</div>
				<div class="row">
					<div class="col-xs-4"><em><cm:Text ID="txtEventTime" runat="server" Field="Time Label" /></em></div>
					<div class="col-xs-8"><sc:Date ID="dtTime" runat="server" Field="Event Date" Format="h:mm tt" /></div>
				</div>
				<div class="row top-spacer">
					<div class="col-xs-12">
						<p><asp:Literal ID="litEventType" runat="server"></asp:Literal></p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<h4><cm:Text ID="txtEventLocation" runat="server" Field="Location Label" /></h4>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<address>
							<asp:PlaceHolder ID="phAddress" runat="server" />
						</address>
					</div>
				</div>

				<p><asp:HyperLink ID="lnkDirections" CssClass="icon icoArrow link" runat="server" Target="_blank"><span class="glyphicon glyphicon-chevron-right"></span>
					<sc:Text ID="txtGetDirections" runat="server" Field="Get Directions Label" /></asp:HyperLink></p>
			</div>
			<div class="col-xs-12 col-md-8">
				<cm:GoogleMap ID="ucGoogleMap" runat="server" InitialZoom="13" />
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<cm:Text ID="txtDisclaimer" runat="server" Field="Disclaimer" />
			</div>
		</div>

		<asp:Panel id="pnlConfirmation" runat="server" CssClass="row" Visible="false">
			<div class="col-xs-12">
				<cm:Text ID="txtConfirmation" runat="server" Field="ThankYouMessage" />
			</div>
		</asp:Panel>

		<asp:PlaceHolder ID="phRegistration" runat="server">
			<hr class="no-margin-top no-margin-bottom" />

			<div class="row">
				<div class="col-xs-12">
					<cm:Text ID="txtRegContent" runat="server" Field="Registration Content" />
				</div>
			</div>

			<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
				<div class="form-group">
					<label for="txtFirstName" class="col-xs-12 col-md-3 control-label"><cm:Text ID="txtFirstNameLabel" runat="server" Field="First Name Text" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" MaxLength="50" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtLastName" class="col-xs-12 col-md-3 control-label"><cm:Text ID="txtLastNameLabel" Field="Last Name Text" runat="server" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" MaxLength="50" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtAddress" class="col-xs-12 col-md-3 control-label"><cm:Text ID="txtAddressLabel" Field="Address Text" runat="server" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" MaxLength="100" />
					</div>
				</div>
				<div class="form-group">
					<label for="City" class="col-xs-12 col-md-3 control-label"><cm:Text ID="txtCityLabel" Field="City Text" runat="server" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtCity" runat="server" CssClass="form-control" MaxLength="50" />
					</div>
				</div>
				<div class="form-group">
					<label for="ddlState" class="col-xs-12 col-md-3 control-label"><cm:Text ID="txtStateLabel" Field="State Text" runat="server" /></label>
					<div class="col-xs-12 col-md-5">
						<asp:DropDownList ID="ddlState" runat="server" AppendDataBoundItems="true" CssClass="form-control">
						</asp:DropDownList>
					</div>
				</div>
				<div class="form-group">
					<label for="txtZipCode" class="col-xs-12 col-md-3 control-label"><cm:Text ID="txtZipLabel" Field="Zip Code Text" runat="server" /></label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control" MaxLength="5" placeholder="#####" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RegularExpressionValidator ID="regExValZip" runat="server" ControlToValidate="txtZipCode"
							SetFocusOnError="true" ErrorMessage="Format is incorrect"
							ValidationExpression="^[0-9]{5}$" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtPhone" class="col-xs-12 col-md-3 control-label"><cm:Text ID="txtPhoneLabel" Field="Phone Text" runat="server" /></label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" MaxLength="15" placeholder="###-###-####" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RegularExpressionValidator ID="regExValPhone" runat="server" ControlToValidate="txtPhone"
							ErrorMessage="Phone is invalid or incorrectly formatted" SetFocusOnError="true"
							ValidationExpression="^(\(?[2-9]\d{2}\)?)[- ]?([2-9]\d{2})[- ]?\d{4}$" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtEmail" class="col-xs-12 col-md-3 control-label"><cm:Text ID="txtEmailLabel" Field="Email Text" runat="server" /></label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="100" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RegularExpressionValidator ID="regExValEmail" runat="server" ControlToValidate="txtEmail"
							SetFocusOnError="true" ErrorMessage="Invalid Email"
							ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
					</div>
				</div>
				<div class="form-group">
					<label for="rblBringGuest" class="col-xs-12 col-md-3 control-label"><cm:Text ID="txtBringGuestLabel" Field="Bring Guest Text" runat="server" /></label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblBringGuest" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Text="Yes" />
							<asp:ListItem Text="No" />
						</asp:RadioButtonList>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<cm:Text ID="txtRegFooter" runat="server" Field="Registration Footer" />
					</div>
				</div>
				<div class="col-xs-12 col-md-offset-3 col-md-4 top-spacer">
					<asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_Click" Text="SUBMIT" />
				</div>
			</asp:Panel>
		</asp:PlaceHolder>
</asp:PlaceHolder>

		<div class="row">
			<div class="col-xs-12">
				<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
			</div>
		</div>
	</div>
</div>
<!-- End SubLayouts/CalendarDetailPage -->
