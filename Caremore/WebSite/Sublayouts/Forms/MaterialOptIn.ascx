﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="MaterialOptIn.ascx.cs" Inherits="Website.Sublayouts.Forms.MaterialOptIn" %>
<%@ Register src="~/Controls/ReCaptcha.ascx" tagname="ReCaptcha" tagprefix="uc1" %>

<!-- Begin Sublayouts/Forms/MaterialOptIn -->
<div class="row">
	<div class="col-xs-12">
		<asp:PlaceHolder ID="phConfirmation" runat="server" Visible="false">
			<sc:Text ID="txtThankYouMessage" runat="server" Field="ThankYouMessage" />
		</asp:PlaceHolder>

		<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
			<sc:Text ID="txtFormHeader" Field="Form Header" runat="server" />
			<div class="form-group">
				<label for="txtLastName" class="col-xs-12 col-md-2 control-label"><sc:Text ID="lblLastName" runat="server" Field="Last Name Label" /> <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:TextBox ID="txtLastName" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="25" />
				</div>
				<div class="col-xs-4 col-md-6">
					<asp:RequiredFieldValidator ID="reqValLastName" runat="server" ControlToValidate="txtLastName"
						SetFocusOnError="true" ValidationGroup="MaterialOptIn" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtFirstName" class="col-xs-12 col-md-2 control-label"><sc:Text ID="lblFirstName" runat="server" Field="First Name Label" /> <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:TextBox ID="txtFirstName" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="25" />
				</div>
				<div class="col-xs-4 col-md-6">
					<asp:RequiredFieldValidator ID="reqValFirstName" runat="server" ControlToValidate="txtFirstName"
						SetFocusOnError="true" ValidationGroup="MaterialOptIn" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtEmail" class="col-xs-12 col-md-2 control-label"><sc:Text ID="lblEmail" runat="server" Field="Email Label" /> <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:TextBox ID="txtEmail" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="100" />
				</div>
				<div class="col-xs-4 col-md-6">
					<asp:RequiredFieldValidator ID="reqValEmail" runat="server" ControlToValidate="txtEmail"
						SetFocusOnError="true" ValidationGroup="MaterialOptIn" />
					<asp:RegularExpressionValidator id="regExValEmail" runat="server" ControlToValidate="txtEmail"
						SetFocusOnError="true" ValidationGroup="MaterialOptIn"
						ValidationExpression="^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtZipCode" class="col-xs-12 col-md-2 control-label"><sc:Text ID="lblZipCode" runat="server" Field="Zip Code Label" /> <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:TextBox ID="txtZipCode" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="5" />
				</div>
				<div class="col-xs-4 col-md-6">
					<asp:RequiredFieldValidator ID="reqValZipCode" runat="server" ControlToValidate="txtZipCode"
						SetFocusOnError="true" ValidationGroup="MaterialOptIn" />
					<asp:RegularExpressionValidator ID="regExValZipCode" runat="server" ControlToValidate="txtZipCode"
						SetFocusOnError="true" ValidationGroup="MaterialOptIn"
						ValidationExpression="^[0-9]{5}$" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtMemberId" class="col-xs-12 col-md-2 control-label"><sc:Text ID="lblMemberId" runat="server" Field="Member ID Label" /> <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:TextBox ID="txtMemberId" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="9" />
				</div>
				<div class="col-xs-4 col-md-6">
					<asp:RequiredFieldValidator ID="reqValMemberId" runat="server" ControlToValidate="txtMemberId"
						SetFocusOnError="true" ValidationGroup="MaterialOptIn" />
					<asp:RegularExpressionValidator ID="regExValMemberId" runat="server" ControlToValidate="txtMemberId"
						SetFocusOnError="true" ValidationGroup="MaterialOptIn"
						ValidationExpression="^[a-zA-Z0-9]{9}$" />
				</div>
			</div>
			<div class="form-group">
				<label for="rblReceiveMaterials" class="col-xs-12"><sc:Text ID="lblReceiveMaterials" runat="server" Field="Receive Materials Label" /></label>
				<div class="col-xs-12 col-md-offset-2 col-md-5">
					<asp:RadioButtonList ID="rblReceiveMaterials" runat="server" ClientIDMode="Static" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
						<asp:ListItem Value="Yes" Selected="True" />
						<asp:ListItem Value="No" />
					</asp:RadioButtonList>
				</div>
			</div>
			<div class="form-group">
				<label for="rblReceiveEmails" class="col-xs-12"><sc:Text ID="lblReceiveEmails" runat="server" Field="Receive Emails Label" /></label>
				<div class="col-xs-12 col-md-offset-2 col-md-5">
					<asp:RadioButtonList ID="rblReceiveEmails" runat="server" ClientIDMode="Static" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
						<asp:ListItem Value="Yes" Selected="True" />
						<asp:ListItem Value="No" />
					</asp:RadioButtonList>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<sc:Text ID="txtAgreement" runat="server" Field="Agreement" />
				</div>
			</div>
			<div class="form-group">
				<label for="ucReCaptcha" class="col-xs-12 col-md-2 control-label"><sc:Text ID="lblReCaptcha" runat="server" Field="Verify Label" /> <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<uc1:ReCaptcha ID="ucReCaptcha" runat="server" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12 col-md-offset-2 col-md-4">
					<asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_Click" ValidationGroup="MaterialOptIn" />
				</div>
			</div>
		</asp:Panel>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End Sublayouts/Forms/MaterialOptIn -->
