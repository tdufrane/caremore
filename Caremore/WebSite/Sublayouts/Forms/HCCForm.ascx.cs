﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Text;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace Website.Sublayouts.Forms
{
	public partial class HCCForm : BaseUserControl
	{
		#region Private variables

		private const string PostBackSessionName = "HCCForm";

		#endregion

		#region Protected properties

		protected Item FormItem
		{
			get
			{
				return this.DataSource;
			}
		}

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();

				if (IsPostBack)
				{
					CareMoreUtilities.ClearControl(phError);
				}
				else
				{
					Session[PostBackSessionName] = null;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				if (Session[PostBackSessionName] == null)
				{
					Page.Validate("HCCForm");

					if (Page.IsValid && ucReCaptcha.IsValid())
					{
						SendEmail();

						// Show thank you message
						phConfirmation.Visible = true;
						pnlForm.Visible = false;

						// Set session to prevent refresh posts
						Session[PostBackSessionName] = DateTime.Now;
					}
					else
					{
						CareMoreUtilities.DisplayInvalidSubmission(phError, Page.Validators, pnlForm);
					}
				}
				else
				{
					CareMoreUtilities.DisplayNoMultiSubmit(phError);
					phConfirmation.Visible = false;
					pnlForm.Visible = false;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			if (this.DataSource == null)
			{
				CareMoreUtilities.DisplayConfigurationIssue(phError);
			}
			else
			{
				Page.DataBind();

				reqValEmail.ErrorMessage = FormItem["Required Validation"];
				regExValEmail.ErrorMessage = FormItem["Email Validation"];
				regExValPhone.ErrorMessage = FormItem["Phone Validation"];
				btnSubmit.Text = FormItem["Submit Label"];
			}
		}

		private void SendEmail()
		{
			if (this.DataSource == null)
			{
				CareMoreUtilities.DisplayConfigurationIssue(phError);
			}
			else
			{
				string body = string.Format(FormItem["MailContent"],
					string.IsNullOrEmpty(txtFirstName.Text) ? CareMoreUtilities.NoValue : txtFirstName.Text,
					string.IsNullOrEmpty(txtLastName.Text) ? CareMoreUtilities.NoValue : txtLastName.Text,
					string.IsNullOrEmpty(txtPhone.Text) ? CareMoreUtilities.NoValue : txtPhone.Text,
					rblTimeToCall.SelectedItem == null ? CareMoreUtilities.NoValue : rblTimeToCall.SelectedItem.Text,
					string.IsNullOrEmpty(txtComments.Text) ? CareMoreUtilities.NoValue : txtComments.Text);

				MailMessage message = new MailMessage();

				if (string.IsNullOrEmpty(txtEmail.Text))
					message.From = new MailAddress(FormItem["From"]);
				else
					message.From = new MailAddress(txtEmail.Text);

				message.To.Add(new MailAddress(FormItem["To"]));
				message.Subject = FormItem["Subject"];
				message.Body = body;
				message.IsBodyHtml = true;

				// Send email
				CareMoreUtilities.SendEmail(message);
			}
		}

		#endregion
	}
}
