﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="MaterialRequest.ascx.cs" Inherits="Website.Sublayouts.Forms.MaterialRequest" %>
<%@ Register TagPrefix="cm" Namespace="CareMore.Web.DotCom.Controls" Assembly="CareMore.Web.DotCom"  %>
<%@ Register src="~/Controls/ReCaptcha.ascx" tagname="ReCaptcha" tagprefix="uc1" %>

<!-- Begin Sublayouts/Forms/MaterialRequest -->
<div class="row">
	<div class="col-xs-12">
		<asp:PlaceHolder ID="phConfirmation" runat="server" Visible="false">
			<sc:Text ID="txtThankYouMessage" runat="server" Field="ThankYouMessage" />
		</asp:PlaceHolder>

		<asp:PlaceHolder ID="phHeader" runat="server">
			<sc:Text ID="txtHeader" runat="server" Field="Header Text" />
		</asp:PlaceHolder>

		<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
			<div class="form-group">
				<label for="rblMemberTypes" class="col-xs-12 col-md-3 control-label"><asp:Label ID="lblMemberTypes" runat="server" /> <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:RadioButtonList ID="rblMemberTypes" runat="server" CssClass="radio-inline" RepeatDirection="Vertical" RepeatLayout="Flow">
					</asp:RadioButtonList>
				</div>
				<div class="col-xs-4 col-md-5">
					<asp:RequiredFieldValidator ID="reqValMemberTypes" runat="server" ControlToValidate="rblMemberTypes"
						SetFocusOnError="true" ValidationGroup="MaterialRequest" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtLastName" class="col-xs-12 col-md-3 control-label"><asp:Label ID="lblLastName" runat="server" /> <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" MaxLength="100" />
				</div>
				<div class="col-xs-4 col-md-5">
					<asp:RequiredFieldValidator ID="reqValLastName" runat="server" ControlToValidate="txtLastName"
						SetFocusOnError="true" ValidationGroup="MaterialRequest" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtFirstName" class="col-xs-12 col-md-3 control-label"><asp:Label ID="lblFirstName" runat="server" /> <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" MaxLength="100" />
				</div>
				<div class="col-xs-4 col-md-5">
					<asp:RequiredFieldValidator ID="reqValFirstName" runat="server" ControlToValidate="txtFirstName"
						SetFocusOnError="true" ValidationGroup="MaterialRequest" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtMiddleInitial" class="col-xs-12 col-md-3 control-label"><asp:Label ID="lblMiddleInitial" runat="server" /></label>
				<div class="col-xs-2 col-md-1">
					<asp:TextBox ID="txtMiddleInitial" runat="server" CssClass="form-control" MaxLength="1" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtAddress" class="col-xs-12 col-md-3 control-label"><asp:Label ID="lblAddress" runat="server" /></label>
				<div class="col-xs-8 col-md-4">
					<asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" />
				</div>
				<div class="col-xs-4 col-md-5">
					<asp:RequiredFieldValidator ID="reqValAddress" runat="server" ControlToValidate="txtAddress"
						SetFocusOnError="true" ValidationGroup="MaterialRequest" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtCity" class="col-xs-12 col-md-3 control-label"><asp:Label ID="lblCity" runat="server" /> <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:TextBox ID="txtCity" runat="server" CssClass="form-control" MaxLength="100" />
				</div>
				<div class="col-xs-4 col-md-5">
					<asp:RequiredFieldValidator ID="reqValCity" ControlToValidate="txtCity" runat="server"
						SetFocusOnError="true" ValidationGroup="MaterialRequest" />
				</div>
			</div>
			<div class="form-group">
				<label for="ddlStates" class="col-xs-12 col-md-3 control-label"><asp:Label ID="lblState" runat="server" /> <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:DropDownList ID="ddlStates" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CssClass="form-control"
						OnSelectedIndexChanged="DdlStates_SelectedIndexChanged">
					</asp:DropDownList>
				</div>
				<div class="col-xs-4 col-md-5">
					<asp:RequiredFieldValidator ID="reqValState" ControlToValidate="ddlStates" runat="server"
						SetFocusOnError="true" ValidationGroup="MaterialRequest"
						InitialValue="" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtZipCode" class="col-xs-12 col-md-3 control-label"><asp:Label ID="lblZipCode" runat="server" /> <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control" MaxLength="10" placeholder="#####" />
				</div>
				<div class="col-xs-4 col-md-5">
					<asp:RequiredFieldValidator ID="reqValZipCode" ControlToValidate="txtZipCode" runat="server"
						SetFocusOnError="true" ValidationGroup="MaterialRequest" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtPhone" class="col-xs-12 col-md-3 control-label"><asp:Label ID="lblPhone" runat="server" /></label>
				<div class="col-xs-8 col-md-4">
					<asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" MaxLength="100" placeholder="###-###-####" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtMemberId" class="col-xs-12 col-md-3 control-label"><asp:Label ID="lblMemberId" runat="server" /></label>
				<div class="col-xs-8 col-md-4">
					<asp:TextBox ID="txtMemberId" runat="server" CssClass="form-control" MaxLength="9" />
				</div>
				<div class="col-xs-4 col-md-5">
					<asp:RegularExpressionValidator id="regExValMemberId" runat="server" ControlToValidate="txtMemberId"
						SetFocusOnError="true" ValidationGroup="MaterialRequest"
						ValidationExpression="^[a-zA-Z0-9]{9}$" />
				</div>
			</div>
			<div class="form-group">
				<label for="cblLanguageRequested" class="col-xs-12 col-md-3 control-label"><asp:Label ID="lblLanguageRequested" runat="server" /> <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:CheckBoxList ID="cblLanguageRequested" runat="server" CssClass="checkbox-inline" RepeatDirection="Vertical" RepeatLayout="Flow"></asp:CheckBoxList>
				</div>
				<div class="col-xs-4 col-md-5">
					<cm:RequiredFieldValidatorForCheckBoxLists ID="reqValLanguageRequested" runat="server" ControlToValidate="cblLanguageRequested"
						CssClass="text-danger" Display="Dynamic" SetFocusOnError="true" ValidationGroup="MaterialRequest" />
				</div>
			</div>
			<div class="form-group">
				<label for="ddlCounties" class="col-xs-12 col-md-3 control-label"><asp:Label ID="lblChooseCounty" runat="server" /> <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:DropDownList ID="ddlCounties" runat="server" AutoPostBack="true" CssClass="form-control"
						OnSelectedIndexChanged="DdlCounties_SelectedIndexChanged"></asp:DropDownList>
				</div>
				<div class="col-xs-4 col-md-5">
					<asp:RequiredFieldValidator ID="reqValCounties" runat="server" ControlToValidate="ddlCounties"
						SetFocusOnError="true" ValidationGroup="MaterialRequest"
						InitialValue="" />
				</div>
			</div>
			<div class="form-group">
				<label for="ddlPlans" class="col-xs-12 col-md-3 control-label"><asp:Label ID="lblChoosePlan" runat="server" /> <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:DropDownList ID="ddlPlans" runat="server" CssClass="form-control"></asp:DropDownList>
				</div>
				<div class="col-xs-4 col-md-5">
					<asp:RequiredFieldValidator ID="reqValPlans" runat="server" ControlToValidate="ddlPlans"
						SetFocusOnError="true" ValidationGroup="MaterialRequest"
						InitialValue="" />
				</div>
			</div>
			<div class="form-group">
				<label for="cblMaterialsRequested" class="col-xs-12 col-md-3 control-label"><asp:Label ID="lblMaterialsRequested" runat="server" /> <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:CheckBoxList ID="cblMaterialsRequested" runat="server" CssClass="checkbox-inline" RepeatDirection="Vertical" RepeatLayout="Flow"></asp:CheckBoxList>
				</div>
				<div class="col-xs-4 col-md-5">
					<cm:RequiredFieldValidatorForCheckBoxLists ID="reqValMaterialsRequested" runat="server" ControlToValidate="cblMaterialsRequested"
						CssClass="text-danger" Display="Dynamic" SetFocusOnError="true" ValidationGroup="MaterialRequest" />
				</div>
			</div>
			<div class="form-group">
				<label for="ucReCaptcha" class="col-xs-12 col-md-3 control-label"><asp:Label ID="lblVerify" runat="server" /> <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<uc1:ReCaptcha ID="ucReCaptcha" runat="server" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12 col-md-offset-3 col-md-4">
					<asp:LinkButton ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_Click" ValidationGroup="MaterialRequest" />
				</div>
			</div>
		</asp:Panel>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End Sublayouts/Forms/MaterialRequest -->
