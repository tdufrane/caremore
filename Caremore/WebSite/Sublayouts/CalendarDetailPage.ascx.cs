﻿using System;
using System.Linq;
using CareMore.Web.DotCom;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Globalization;
using Sitecore.Links;
using Website.Controls;
using Website.Sublayouts.Widgets;
using Website.BL.ListItem;
using System.Text;
using System.Web.UI.WebControls;
using System.Net.Mail;

namespace Website.Sublayouts
{
	public partial class CalendarDetailPage : BaseUserControl
	{
		#region Private variables

		private const string PostBackSessionName = "CalendarDetailPage";

		private string registrationItemKey { get; set; }
		private Item calendarItem = null;

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				if (IsPostBack)
				{
					CareMoreUtilities.ClearControl(phError);
				}
				else
				{
					Session[PostBackSessionName] = null;
				}

				LoadPage(Request.QueryString["id"]);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				if (Session[PostBackSessionName] == null)
				{
					SendEmail();

					// Set session to prevent refresh posts
					Session[PostBackSessionName] = DateTime.Now;

					pnlConfirmation.Visible = true;
					phRegistration.Visible = false;
				}
				else
				{
					CareMoreUtilities.DisplayNoMultiSubmit(phError);
					pnlConfirmation.Visible = false;
					phRegistration.Visible = false;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage(string guidString)
		{
			if (!string.IsNullOrEmpty(guidString))
			{
				Guid eventId;

				if (Guid.TryParse(guidString, out eventId))
					calendarItem = CurrentDb.GetItem(guidString, Language.Parse(CareMoreUtilities.EnglishKey));
			}

			if (calendarItem == null)
			{
				//TODO CareMoreUtilities.DisplayNotFound(phError);
				phDetails.Visible = false;
			}
			else
			{
				LoadPage();
			}
		}

		private void LoadPage()
		{
			string state = CareMoreUtilities.GetCurrentLocale();

			if (state.Equals("Virginia", StringComparison.OrdinalIgnoreCase))
				registrationItemKey = "EVENTS_VA_REGISTRATION";
			else
				registrationItemKey = "EVENTS_REGISTRATION";

			SetDetailsItems();
			SetFormItems();

			ucGoogleMap.Locations.Add(new LocationItem(calendarItem, "Place"));
			ucGoogleMap.DisplayMap();
		}

		private void SetDetailsItems()
		{
			hlCalendar.NavigateUrl = LinkManager.GetItemUrl(CurrentItem.Parent);

			txtEventTitle.Item = calendarItem;
			dtDay.Item = calendarItem;
			dtFull.Item = calendarItem;
			dtTime.Item = calendarItem;

			RenderEventType(calendarItem);

			phAddress.Controls.Add(CareMoreUtilities.GetInlineEditableAddress(calendarItem));

			lnkDirections.NavigateUrl = LocationHelper.GetMapQuery(calendarItem["Address Line 1"], calendarItem["Address Line 2"],
				calendarItem["City"], calendarItem["State"], calendarItem["Postal Code"]);
		}

		private void RenderEventType(Item calendarItem)
		{
			MultilistField EventType = calendarItem.Fields["Event Type"];
			Item[] eventTypes = EventType.GetItems();

			StringBuilder eventTypeNames = new StringBuilder();

			foreach (Item eventType in eventTypes)
			{
				eventTypeNames.Append(eventType.DisplayName);
				eventTypeNames.AppendLine("<br />");
			}

			if (eventTypeNames.Length > 0)
				litEventType.Text = eventTypeNames.ToString();
		}

		private void SendEmail()
		{
			Item regItem = Sitecore.Context.Database.GetItem(ItemIds.GetID(registrationItemKey), Sitecore.Context.Language);

			StringBuilder body = new StringBuilder();

			body.AppendFormat("<div>{0}</div><br /><br />", regItem.Fields["MailContent"].Value);
			body.AppendFormat("<strong>First Name:</strong> {0}<br />", txtFirstName.Text);
			body.AppendFormat("<strong>Last Name:</strong> {0}<br />", txtLastName.Text);
			body.AppendFormat("<strong>Address:</strong> {0}<br />", txtAddress.Text);
			body.AppendFormat("<strong>City:</strong> {0}<br />", txtCity.Text);
			body.AppendFormat("<strong>State:</strong> {0}<br />", ddlState.SelectedItem == null ? string.Empty : ddlState.SelectedValue);
			body.AppendFormat("<strong>Zip Code:</strong> {0}<br />", txtZipCode.Text);
			body.AppendFormat("<strong>Phone:</strong> {0}<br />", txtPhone.Text);
			body.AppendFormat("<strong>Email:</strong> {0}<br />", txtEmail.Text);
			body.AppendFormat("<strong>Extra guest attending:</strong> {0}<br />", rblBringGuest.SelectedItem == null ? string.Empty : rblBringGuest.SelectedItem.Text);

			DateTime eventDate = Sitecore.DateUtil.IsoDateToDateTime(calendarItem.Fields["Event Date"].Value);
			body.AppendFormat("<br /><br /><u>Event Information</u>:<br />");
			body.AppendFormat("<strong>Event Title:</strong> {0}<br />", txtEventTitle.RenderAsText());
			body.AppendFormat("<strong>Event Type:</strong> {0}<br />", litEventType.Text.Replace("<br /><br />", ", "));
			body.AppendFormat("<strong>Day:</strong> {0}<br />", eventDate.ToString("dddd"));
			body.AppendFormat("<strong>Date:</strong> {0}<br />", eventDate.ToShortDateString());
			body.AppendFormat("<strong>Time:</strong> {0}<br /><br />", eventDate.ToShortTimeString());
			body.AppendFormat("<strong>Location:</strong> <br />");

			if (CareMoreUtilities.HasFieldValue(calendarItem.Fields["Place"]))
			{
				body.AppendFormat("{0}<br />", calendarItem.Fields["Place"].Value);
			}

			if (CareMoreUtilities.HasFieldValue(calendarItem.Fields["Address Line 1"]))
			{
				body.AppendFormat("{0}<br />", calendarItem.Fields["Address Line 1"].Value);
			}

			if (CareMoreUtilities.HasFieldValue(calendarItem.Fields["AddressLine2"]))
			{
				body.AppendFormat("{0}<br />", calendarItem.Fields["Address Line 2"].Value);
			}

			if (CareMoreUtilities.HasFieldValue(calendarItem.Fields["City"]))
			{
				body.AppendFormat("{0}", calendarItem.Fields["City"].Value);
			}

			if (CareMoreUtilities.HasFieldValue(calendarItem.Fields["State"]))
			{
				body.AppendFormat(", {0}", calendarItem.Fields["State"].Value);
			}

			if (CareMoreUtilities.HasFieldValue(calendarItem.Fields["Postal Code"]))
			{
				body.AppendFormat(" {0}", calendarItem.Fields["Postal Code"].Value);
			}

			body.AppendFormat("<br /><br /><strong>Registration Date:</strong> {0}<br />", DateTime.Now.ToShortDateString());
			body.AppendFormat("<strong>Registration Time:</strong> {0}<br />", DateTime.Now.ToShortTimeString());

			MailMessage message = new MailMessage();
			Common.SetEmail(message.To, regItem["To"]);
			message.From = new MailAddress(txtEmail.Text.Trim().Length > 0 ? txtEmail.Text.Trim() : regItem["From"]);
			message.Subject = regItem["Subject"];
			message.Body = body.ToString();
			message.IsBodyHtml = true;

			//Send email
			CareMoreUtilities.SendEmail(message);
		}

		private void FillStateList(string state)
		{
			Item[] states = Sitecore.Context.Database.SelectItems(CareMoreUtilities.LOCALIZATION_PATH + "//*[@@templatename='State']");

			if (states.Count() > 0)
			{
				foreach (Item stateItem in states)
				{
					ddlState.Items.Add(new ListItem(stateItem["Name"], stateItem["Code"]));
				}

				if (state != null)
				{
					ddlState.Items.FindByText(state).Selected = true;
				}
			}
		}

		private void SetFormItems()
		{
			Item regItem = Sitecore.Context.Database.GetItem(ItemIds.GetID(registrationItemKey), Sitecore.Context.Language);

			if (regItem != null)
			{
				txtConfirmation.Item = regItem;
				txtRegContent.Item = regItem;
				txtFirstNameLabel.Item = regItem;
				txtLastNameLabel.Item = regItem;
				txtAddressLabel.Item = regItem;
				txtCityLabel.Item = regItem;
				txtStateLabel.Item = regItem;
				txtZipLabel.Item = regItem;
				txtPhoneLabel.Item = regItem;
				txtEmailLabel.Item = regItem;
				txtBringGuestLabel.Item = regItem;
				txtRegFooter.Item = regItem;
				btnSubmit.Text = regItem["Submit Button Text"];

				rblBringGuest.Items[0].Text = rblBringGuest.Items[0].Value = regItem.Fields["Yes Text"].Value;
				rblBringGuest.Items[1].Text = rblBringGuest.Items[1].Value = regItem.Fields["No Text"].Value;

				FillStateList((string)Session[CareMoreUtilities.LocalStateKey]);
			}
		}

		#endregion
	}
}
