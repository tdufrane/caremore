﻿using System;
using Sitecore.Data.Items;

namespace Website.Sublayouts
{
    public partial class ChineseApplicationPage : ApplicationBase
    {
        protected override string CheckLocale(bool isChinese)
        {
            string redirectUrl = string.Empty;

            if (Sitecore.Context.Language.Name.Equals("en", StringComparison.OrdinalIgnoreCase) && isChinese == false)
            {
                Item applicationItem = base.currentDB.Items[CareMoreUtilities.HOME_PATH + "/Plans/Application"];
                redirectUrl = Sitecore.Links.LinkManager.GetItemUrl(applicationItem);
            }
            
            if (Sitecore.Context.Language.Name.Equals("es-mx", StringComparison.OrdinalIgnoreCase) && isChinese == false)
            {
                Item applicationItem = base.currentDB.Items[CareMoreUtilities.HOME_PATH + "/Plans/Spanish Application"];
                redirectUrl = Sitecore.Links.LinkManager.GetItemUrl(applicationItem);
            }


            CountyName.Text = Session["LastCountySelected"] != null ? (string)Session["LastCountySelected"] : string.Empty;

            return redirectUrl;
        }
    }
}
