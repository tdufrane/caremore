﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;

namespace Website.Sublayouts
{
	public partial class NewsStoryListPage : BaseUserControl
	{
		#region Private variables

		private ID FolderTemplateID = new ID("{3CFED6C6-114F-4513-88FB-1B6B48DF8022}");
		private ID ItemTemplateID = new ID("{C14A5D63-D8FF-4BFD-B7D3-25AF927D9B7E}");

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				if (!Page.IsPostBack)
				{
					LoadPage();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void LvAllYears_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			try
			{
				LoadYearNewsItems(e.Item);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void LvAllStories_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListViewItemType.DataItem)
				{
					LoadNewsItem(e.Item);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			Item englishItem = base.GetEnglishItem(base.CurrentItem);

			List<Item> itemList = englishItem.Axes.GetDescendants()
				.Where(i => i.TemplateID == ItemTemplateID &&
				            (i["Display For All"] == "1" || (string.IsNullOrEmpty(i["IsNotLocalized"]) && i["State"].Equals(CurrentState))))
				.OrderByDescending(i => i["Story Date"])
				.Take(4).ToList();

			if (itemList != null && itemList.Count > 0)
			{
				int currentYear = DateTime.Now.Year;

				if (itemList.Count == 1)
				{
					BindList(lvFeaturedItems, itemList.GetRange(0, 1));
					lvCurrentItems.Visible = false;
				}
				else
				{
					BindList(lvFeaturedItems, itemList.GetRange(0, 1));
					BindList(lvCurrentItems, itemList.GetRange(1, itemList.Count - 1));
				}

				List<Item> allYears = englishItem.GetChildren()
					.Where(i => i.TemplateID == FolderTemplateID)
					.OrderByDescending(i => i.Name).ToList();

				BindList(lvAllYears, allYears);
			}
		}

		private List<Item> GetNewsItems(Item parent)
		{
			return parent.Children
				.Where(i => i.TemplateID == ItemTemplateID &&
				           (i["Display For All"] == "1" || (string.IsNullOrEmpty(i["IsNotLocalized"]) && i["State"].Equals(CurrentState))))
				.OrderByDescending(i => i["Story Date"]).ToList();
		}

		private void LoadYearNewsItems(ListViewItem viewItem)
		{
			BindList((ListView)viewItem.FindControl("lvAllStories"),
				GetNewsItems((Item)viewItem.DataItem));
		}

		private void LoadNewsItem(ListViewItem viewItem)
		{
			Item newsItem = (Item)viewItem.DataItem;
			HyperLink hlName = (HyperLink)viewItem.FindControl("hlName");
			hlName.NavigateUrl = Sitecore.Links.LinkManager.GetItemUrl(newsItem);
		}

		#endregion
	}
}
