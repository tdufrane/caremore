﻿using System;
using CareMore.Web.DotCom;
using Sitecore.Data.Fields;
using Sitecore.Links;

namespace Website.Sublayouts
{
	public partial class Enroll : BaseUserControl
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnEnroll_OnClick(object sender, EventArgs e)
		{
			try
			{
				Session["PlanId"] = ddlPlan.SelectedValue;
				Session["CountyId"] = ddlCounty.SelectedValue;
				Session["LastCountySelected"] = CurrentDb.GetItem(ddlCounty.SelectedValue)["County"];

				InternalLinkField link = (InternalLinkField)CurrentItem.Fields["Enroll Button Url"];

				Response.Redirect(LinkManager.GetItemUrl(link.TargetItem), false);
				Context.ApplicationInstance.CompleteRequest();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			gridEnroll.AppItem = ddlPlan.AppItem;
			ddlPlan.CountyDropDown = ddlCounty;
			reqValCountyRequired.ErrorMessage = CurrentItem["County Required Message"];
			reqValPlanRequired.ErrorMessage = CurrentItem["Plan Required Message"];
			btnEnroll.Text = CurrentItem["Enroll Online Button"];
		}

		#endregion
	}
}
