﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Website.Controls;

namespace Website.Sublayouts
{
	public partial class HomeCategoryPage : BaseUserControl
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				if (!IsPostBack)
					LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			Header ucHeader = this.Page.FindControl("ucHeader") as Header;
			if (ucHeader != null)
			{
				ucHeader.BannerPanel.CssClass = ucHeader.BannerPanel.CssClass.Replace("banner", "banner-home");

				if (CurrentItem["Show CareMore Plan"].Equals(CareMoreUtilities.CheckedTrueKey))
				{
					ucHeader.HomePlaceHolderAnthem.Visible = false;
					ucHeader.HomePlaceHolderCareMore.Visible = true;
				}
				else if (CurrentItem["Show Anthem Plan"].Equals(CareMoreUtilities.CheckedTrueKey))
				{
					ucHeader.HomePlaceHolderAnthem.Visible = true;
					ucHeader.HomePlaceHolderCareMore.Visible = false;
				}
				else
				{
					ucHeader.HomePlaceHolderAnthem.Visible = false;
					ucHeader.HomePlaceHolderCareMore.Visible = false;
				}
			}
		}

		#endregion
	}
}
