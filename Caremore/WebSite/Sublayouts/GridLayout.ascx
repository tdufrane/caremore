﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="GridLayout.ascx.cs" Inherits="Website.Sublayouts.GridLayout" %>
<%@ Register TagPrefix="cm" TagName="BaseWidget" Src="~/Sublayouts/Widgets/BaseWidget.ascx" %>

<asp:Repeater ID="rptRows" runat="server" OnItemDataBound="RptRows_OnItemDataBound">
	<ItemTemplate>
		<asp:Panel ID="pnlRow" runat="server">
			<hr id="hrSeparator" runat="server" />

			<asp:Repeater ID="rptColumns" runat="server" OnItemDataBound="RptColumns_OnItemDataBound">
				<ItemTemplate>
					<asp:Panel ID="pnlColumn" runat="server">
						<asp:Repeater ID="rptItems" runat="server" OnItemDataBound="RptItems_OnItemDataBound">
							<ItemTemplate>
								<cm:BaseWidget ID="ucWidget" runat="server" />
							</ItemTemplate>
						</asp:Repeater>
					</asp:Panel>
				</ItemTemplate>
			</asp:Repeater>
		</asp:Panel>
	</ItemTemplate>
</asp:Repeater>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
