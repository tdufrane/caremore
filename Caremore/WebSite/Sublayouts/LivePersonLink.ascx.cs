﻿using System;

namespace Website.Sublayouts
{
    public partial class LivePersonLink : System.Web.UI.UserControl
    {
        protected string GetImagePath()
        {
            if (Sitecore.Context.Language.Name.Equals("es-mx", StringComparison.OrdinalIgnoreCase))
            {
                return "LivePerson/spanish";
            }
            else
            {
                return "LivePerson";
            }
        }
    }
}
