﻿using System;

namespace Website.Sublayouts
{
	public partial class OneColumn : BaseUserControl
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			if (!IsPostBack)
			{
				if (string.IsNullOrEmpty(base.CurrentItem["Title"]))
					pnlTitle.Visible = false;

				if (string.IsNullOrEmpty(base.CurrentItem["Subtitle"]))
					pnlSubtitle.Visible = false;

				if (string.IsNullOrEmpty(base.CurrentItem["Image"]))
					pnlContentImage.Visible = false;

				if (string.IsNullOrEmpty(base.CurrentItem["Content"]))
					pnlContent.Visible = false;
			}
		}

		#endregion
	}
}
