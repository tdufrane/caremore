﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SiteMap.ascx.cs" Inherits="Website.Sublayouts.SiteMap" %>

<!-- Begin SubLayouts/SiteMap -->
<div class="row">
	<div class="col-xs-12">
		<asp:TreeView ID="tvSiteMap" runat="server" ExpandDepth="1" ShowLines="false" />
	</div>
</div>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
<!-- End SubLayouts/SiteMap -->
