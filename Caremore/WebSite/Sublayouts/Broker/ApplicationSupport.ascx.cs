﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace Website.Sublayouts.Broker
{
	public partial class ApplicationSupport : BrokerBaseUserControl
	{
		private const string PostBackSessionName = "ApplicationSupportmm";

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				if (IsPostBack)
				{
					CareMoreUtilities.ClearStatus(phError);
				}
				else
				{
					Session[PostBackSessionName] = null;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnGenerate_Click(object sender, EventArgs e)
		{
			try
			{
				if (Session[PostBackSessionName] == null)
				{
					Page.Validate("ApplicationSupport");

					if (Page.IsValid)
					{
						if (VerifyApplication(txtAppConfNumber.Text))
						{
							//Set session to prevent refresh posts
							if (SendDocuments(txtAppConfNumber.Text))
								Session[PostBackSessionName] = DateTime.Now;
						}
						else
						{
							CareMoreUtilities.DisplayError(phError,
								string.Format(CurrentItem["NotFoundMessage"], txtAppConfNumber.Text));
						}
					}
					else
					{
						CareMoreUtilities.DisplayInvalidSubmission(phError, Page.Validators, pnlForm);
					}
				}
				else
				{
					CareMoreUtilities.DisplayNoMultiSubmit(phError);
					phConfirmation.Visible = false;
					pnlForm.Visible = false;
				}

			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
				return;
			}
		}

		#endregion

		#region Private methods

		private bool VerifyApplication(string confirmationNumber)
		{
			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			IEnumerable<MemberApplication> memberApp = db.MemberApplications.Where(item =>
				item.ConfirmationNumber.ToLower() == confirmationNumber.ToLower());

			if ((memberApp == null) || (memberApp.Count() == 0))
				return false;
			else
				return true;
		}

		private bool SendDocuments(string confirmationNumber)
		{
			int maxFileSize = int.Parse(CurrentItem["Max File Size"]);
			bool success = false;

			List<Attachment> files = new List<Attachment>();
			Attachment file = null;

			file = UploadFile(filUpDocument1, maxFileSize);
			if (file != null)
				files.Add(file);

			file = UploadFile(filUpDocument2, maxFileSize);
			if (file != null)
				files.Add(file);

			file = UploadFile(filUpDocument3, maxFileSize);
			if (file != null)
				files.Add(file);

			if (files.Count > 0)
			{
				string body = string.Format(CurrentItem["MailContent"],
					txtAppConfNumber.Text, txtFirstName.Text, txtLastName.Text,
					BrokerProfile.FirstName, BrokerProfile.LastName,
					BrokerProfile.CareMoreId);

				string subject = string.Format(CurrentItem["Subject"], confirmationNumber);
				BrokerHelper.SendEmailNotification(body, subject, files);

				success = true;

				pnlForm.Visible = false;
				phConfirmation.Visible = true;
			}
			else if (phError.Controls.Count == 0)
			{
				CareMoreUtilities.DisplayError(phError, "At least one file must be provided.");
			}

			return success;
		}

		private Attachment UploadFile(FileUpload filUpDocument, int maxFileSize)
		{
			string fileName = null;
			Attachment attachFile = null;

			if (filUpDocument.HasFile)
			{
				fileName = Path.GetFileName(filUpDocument.PostedFile.FileName);
				attachFile = new Attachment(filUpDocument.PostedFile.InputStream, fileName);

				int fileSize = filUpDocument.PostedFile.ContentLength;
				if (fileSize > maxFileSize)
				{
					attachFile = null;

					CareMoreUtilities.DisplayError(phError,
						string.Format("Size of '{0}' file is {1:0.0}MB, it cannot exceed {2:0.0}MB.",
							filUpDocument.PostedFile.FileName, fileSize / 1000000, maxFileSize / 1000000));
				}
			}

			return attachFile;
		}

		private string ExtractFileName(string fileName)
		{
			StringBuilder newItemName = new StringBuilder();

			foreach (char c in fileName)
			{
				if (Char.IsLetterOrDigit(c))
					newItemName.Append(c);
				else if (c.Equals('.'))
					newItemName.Append(c);
				else
					newItemName.Append('_');
			}

			return newItemName.ToString();
		}

		#endregion
	}
}
