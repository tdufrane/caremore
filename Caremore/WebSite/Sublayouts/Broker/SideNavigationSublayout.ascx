﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SideNavigationSublayout.ascx.cs" Inherits="Website.Sublayouts.Broker.SideNavigationSublayout" %>

<!-- Begin SubLayouts/Broker/SideNavigationSublayout -->
<asp:PlaceHolder ID="phBrokerSideNav" runat="server">
	<div id="sideNavBroker">
		<asp:Repeater ID="rptBrokerSideMenu" runat="server" OnItemDataBound="RptBrokerSideMenu_ItemDataBound">
			<HeaderTemplate>
				<ul class="nav nav-stacked">
					<li id="h2SideNavTitle" runat="server" class="nav-stacked-hdr"><asp:HyperLink ID="hlSideNavTitle" runat="server">Broker Portal Home</asp:HyperLink></li>
			</HeaderTemplate>
			<ItemTemplate>
					<li id="liBrokerSideMenu" runat="server"><asp:HyperLink ID="hlBrokerSideMenu" runat="server" /></li>
			</ItemTemplate>
			<FooterTemplate>
					<li><asp:LinkButton ID="lbLogout" runat="server" Text="LOGOUT" OnClick="LbLogout_Click" CausesValidation="false" /></li>
				</ul>
			</FooterTemplate>
		</asp:Repeater>
	</div>
</asp:PlaceHolder>
<!-- End SubLayouts/Broker/SideNavigationSublayout -->
