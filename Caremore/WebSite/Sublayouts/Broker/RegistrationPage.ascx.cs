﻿using System;
using System.Web.UI;

namespace Website.Sublayouts.Broker
{
	public partial class RegistrationPage : System.Web.UI.UserControl
	{
		private const string PostBackSessionName = "ContactUsPage";

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				if (IsPostBack)
				{
					CareMoreUtilities.ClearControl(phError);
				}
				else
				{
					Session[PostBackSessionName] = null;
					ucProfileForm.LoadEmptyRegistration();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				if (Session[PostBackSessionName] == null)
				{
					Page.Validate("BrokerProfileForm");

					if (Page.IsValid && ucReCaptcha.IsValid())
					{
						if (ucProfileForm.AddRegistration(phError))
						{
							//Set session to prevent refresh posts
							Session[PostBackSessionName] = DateTime.Now;
							phConfirmation.Visible = true;
							pnlForm.Visible = false;
						}
					}
					else
					{
						CareMoreUtilities.DisplayInvalidSubmission(phError, Page.Validators, pnlForm);
					}
				}
				else
				{
					CareMoreUtilities.DisplayNoMultiSubmit(phError);
					phConfirmation.Visible = false;
					pnlForm.Visible = false;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}
	}
}
