﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace Website.Sublayouts.Broker
{
	public partial class SideNavigationSublayout : System.Web.UI.UserControl
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if (!IsPostBack)
			{
				if (Session[CareMoreUtilities.BROKER_SESSION] == null)
				{
					if (Sitecore.Context.Item["Requires Login"] == "1")
						RedirectToBrokerHome();
				}
				else
				{
					GenerateMenu();
				}
			}
		}

		protected void LbLogout_Click(object sender, EventArgs e)
		{
			Session[CareMoreUtilities.BROKER_SESSION] = null;
			RedirectToBrokerHome();
		}

		protected void RptBrokerSideMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Header)
			{
				Item brokerHome = ItemIds.GetItem("BROKERS_LOGGED_IN_HOME");

				HyperLink hlSideNavTitle = (HyperLink)e.Item.FindControl("hlSideNavTitle");
				hlSideNavTitle.Text = LinkText(brokerHome);

				if (Sitecore.Context.Item.ID.Guid == brokerHome.ID.Guid)
				{
					//HtmlGenericControl h2 = (HtmlGenericControl)e.Item.FindControl("h2SideNavTitle");
					//h2.Attributes.Add("class", " active");
				}
				else
				{
					hlSideNavTitle.NavigateUrl = LinkManager.GetItemUrl(brokerHome);
				}
			}
			else if ((e.Item.ItemType == ListItemType.AlternatingItem) || (e.Item.ItemType == ListItemType.Item))
			{
				Item menuItem = (Item)e.Item.DataItem;
				HyperLink hlBrokerSideMenu = (HyperLink)e.Item.FindControl("hlBrokerSideMenu");
				HtmlGenericControl li = (HtmlGenericControl)e.Item.FindControl("liBrokerSideMenu");

				Item currentItem = Sitecore.Context.Item;

				if (currentItem.ID.Guid == menuItem.ID.Guid)
				{
					// For CSS styling dont want a hyperlink for active item, just text
					hlBrokerSideMenu.Visible = false;
					li.Controls.Add(new LiteralControl(LinkText(menuItem)));

					RenderChildren(li, menuItem, currentItem);
				}
				else if (menuItem.Children.Where(item => item.ID.Guid == currentItem.ID.Guid).Count() > 0)
				{
					SetLink(hlBrokerSideMenu, menuItem);
					RenderChildren(li, menuItem, currentItem);
				}
				else
				{
					SetLink(hlBrokerSideMenu, menuItem);
				}
			}
		}

		private void GenerateMenu()
		{
			Item brokerHomeItem = ItemIds.GetItem("BROKERS_LOGGED_IN_HOME");
			List<Item> itemList = brokerHomeItem.Children.Where(item => item["Hide from navigation"] != "1").ToList();

			if (itemList.Count() == 0)
			{
				rptBrokerSideMenu.Visible = false;
			}
			else
			{
				rptBrokerSideMenu.Visible = true;
				rptBrokerSideMenu.DataSource = itemList;
				rptBrokerSideMenu.DataBind();
			}
		}

		private void RenderChildren(HtmlGenericControl li, Item menuItem, Item currentItem)
		{
			li.Attributes.Add("class", "expanded");

			List<Item> children = menuItem.Children.Where(item => ((item["Hide from navigation"] != "1") && (item.TemplateName != "BrokerContentFolder"))).ToList();

			if (children.Count > 0)
			{
				HtmlGenericControl ulChildren = new HtmlGenericControl("ul");
				foreach (Item childItem in children)
				{
					HtmlGenericControl liChild = new HtmlGenericControl("li");

					if (currentItem.ID.Guid == childItem.ID.Guid)
					{
						liChild.Attributes.Add("class", "expanded");
						liChild.InnerText = CareMoreUtilities.GetNavigationTitle(childItem);
					}
					else
					{
						HtmlAnchor aChild = new HtmlAnchor();
						aChild.HRef = CareMoreUtilities.GetItemUrl(childItem);
						aChild.InnerText = CareMoreUtilities.GetNavigationTitle(childItem);

						if (!string.IsNullOrEmpty(childItem["Link"]))
						{
							LinkField link = childItem.Fields["Link"];

							if (!string.IsNullOrEmpty(link.Target))
								aChild.Target = link.Target;
						}

						liChild.Controls.Add(aChild);
					}

					ulChildren.Controls.Add(liChild);
				}

				li.Controls.Add(ulChildren);
			}
		}

		private void SetLink(HyperLink link, Item menuItem)
		{
			link.Text = LinkText(menuItem);
			link.NavigateUrl = LinkManager.GetItemUrl(menuItem);
		}

		private string LinkText(Item menuItem)
		{
			string text;

			if (!string.IsNullOrEmpty(menuItem["Navigation Title"]))
			{
				text = menuItem["Navigation Title"];
			}
			else if (!string.IsNullOrEmpty(menuItem["Title"]))
			{
				text = menuItem["Title"];
			}
			else
			{
				text = menuItem.Name;
			}

			return text;
		}

		private void RedirectToBrokerHome()
		{
			Response.Redirect(LinkManager.GetItemUrl(ItemIds.GetItem("BROKERS")), false);
		}
	}
}
