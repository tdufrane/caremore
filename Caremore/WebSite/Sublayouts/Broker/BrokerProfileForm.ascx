﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BrokerProfileForm.ascx.cs" Inherits="Website.Sublayouts.Broker.BrokerProfileForm" %>

<p>Required fields are indicated with an asterisk (<span class="text-danger">*</span>)</p>

<asp:HiddenField ID="hidRegistrationId" runat="server" />

<div class="form-group">
	<label for="txtCareMoreId" class="col-xs-12 col-md-3 control-label">CareMore Id <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtCareMoreId" runat="server" CssClass="form-control" MaxLength="50" />
	</div>
	<div class="col-xs-12 col-md-5">
		<asp:RequiredFieldValidator ID="reqValCareMoreId" runat="server" ControlToValidate="txtCareMoreId"
			ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="BrokerProfileForm" />
		<asp:RegularExpressionValidator ID="regValCareMoreId" runat="server" ControlToValidate="txtCareMoreId"
			ErrorMessage="Invalid id or incorrect format" SetFocusOnError="true" ValidationGroup="BrokerProfileForm"
			ValidationExpression="BKR[0-9]{5}" />
	</div>
</div>
<div class="form-group">
	<label for="txtLastName" class="col-xs-12 col-md-3 control-label">Last Name <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" MaxLength="50" />
	</div>
	<div class="col-xs-12 col-md-5">
		<asp:RequiredFieldValidator ID="reqValLastName" runat="server" ControlToValidate="txtLastName"
			ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="BrokerProfileForm" />
	</div>
</div>
<div class="form-group">
	<label for="txtFirstName" class="col-xs-12 col-md-3 control-label">First Name <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" MaxLength="50" />
	</div>
	<div class="col-xs-12 col-md-5">
		<asp:RequiredFieldValidator ID="reqValFirstName" runat="server" ControlToValidate="txtFirstName"
			ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="BrokerProfileForm" />
	</div>
</div>
<div class="form-group">
	<label for="txtMiddleInitial" class="col-xs-12 col-md-3 control-label">Middle Initial</label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtMiddleInitial" runat="server" CssClass="form-control" MaxLength="1" />
	</div>
</div>
<div class="form-group">
	<label for="rbLstSalutation" class="col-xs-12 col-md-3 control-label">Salutation</label>
	<div class="col-xs-12 col-md-4" style="margin-top:4px;">
		<asp:RadioButtonList ID="rbLstSalutation" runat="server" CssClass="radio-inline"
			RepeatDirection="Horizontal" RepeatLayout="Flow">
			<asp:ListItem>Mr.</asp:ListItem>
			<asp:ListItem>Mrs.</asp:ListItem>
			<asp:ListItem>Ms.</asp:ListItem>
		</asp:RadioButtonList>
	</div>
</div>
<div class="form-group">
	<label for="txtHomePhone" class="col-xs-12 col-md-3 control-label">Phone Number <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtHomePhone" runat="server" CssClass="form-control" MaxLength="12" placeholder="###-###-####" />
	</div>
	<div class="col-xs-12 col-md-5">
		<asp:RequiredFieldValidator ID="reqValHomePhone" runat="server" ControlToValidate="txtHomePhone"
			ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="BrokerProfileForm" />
		<asp:RegularExpressionValidator ID="regValHomePhone" runat="server" ControlToValidate="txtHomePhone"
			ErrorMessage="Invalid phone or incorrect format" SetFocusOnError="true" ValidationGroup="BrokerProfileForm"
			ValidationExpression="^([2-9]{1}[0-9]{2}-{0,1}){2}[0-9]{4}[ ]*$" />
	</div>
</div>
<div class="form-group">
	<label for="txtAddress" class="col-xs-12 col-md-3 control-label">Address <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" MaxLength="100" />
	</div>
	<div class="col-xs-12 col-md-5">
		<asp:RequiredFieldValidator ID="reqValAddress" runat="server" ControlToValidate="txtAddress"
			ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="BrokerProfileForm" />
	</div>
</div>
<div class="form-group">
	<label for="txtCity" class="col-xs-12 col-md-3 control-label">City <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtCity" runat="server" CssClass="form-control" MaxLength="50" />
	</div>
	<div class="col-xs-12 col-md-5">
		<asp:RequiredFieldValidator ID="reqValCity" runat="server" ControlToValidate="txtCity"
			ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="BrokerProfileForm" />
	</div>
</div>
<div class="form-group">
	<label for="ddlState" class="col-xs-12 col-md-3 control-label">State <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:DropDownList ID="ddlState" runat="server" CssClass="form-control">
			<asp:ListItem Text="-select one-" Value="" />
			<asp:ListItem Text="Alabama" Value="Alabama" />
			<asp:ListItem Text="Alaska" Value="Alaska" />
			<asp:ListItem Text="Arizona" Value="Arizona" />
			<asp:ListItem Text="Arkansas" Value="Arkansas" />
			<asp:ListItem Text="California" Value="California" />
			<asp:ListItem Text="Colorado" Value="Colorado" />
			<asp:ListItem Text="Connecticut" Value="Connecticut" />
			<asp:ListItem Text="Delaware" Value="Delaware" />
			<asp:ListItem Text="Florida" Value="Florida" />
			<asp:ListItem Text="Georgia" Value="Georgia" />
			<asp:ListItem Text="Hawaii" Value="Hawaii" />
			<asp:ListItem Text="Idaho" Value="Idaho" />
			<asp:ListItem Text="Illinois" Value="Illinois" />
			<asp:ListItem Text="Indiana" Value="Indiana" />
			<asp:ListItem Text="Iowa" Value="Iowa" />
			<asp:ListItem Text="Kansas" Value="Kansas" />
			<asp:ListItem Text="Kentucky" Value="Kentucky" />
			<asp:ListItem Text="Louisiana" Value="Louisiana" />
			<asp:ListItem Text="Maine" Value="Maine" />
			<asp:ListItem Text="Maryland" Value="Maryland" />
			<asp:ListItem Text="Massachusetts" Value="Massachusetts" />
			<asp:ListItem Text="Michigan" Value="Michigan" />
			<asp:ListItem Text="Minnesota" Value="Minnesota" />
			<asp:ListItem Text="Mississippi" Value="Mississippi" />
			<asp:ListItem Text="Missouri" Value="Missouri" />
			<asp:ListItem Text="Montana" Value="Montana" />
			<asp:ListItem Text="Nebraska" Value="Nebraska" />
			<asp:ListItem Text="Nevada" Value="Nevada" />
			<asp:ListItem Text="New Hampshire" Value="New Hampshire" />
			<asp:ListItem Text="New Jersey" Value="New Jersey" />
			<asp:ListItem Text="New Mexico" Value="New Mexico" />
			<asp:ListItem Text="New York" Value="New York" />
			<asp:ListItem Text="North Carolina" Value="North Carolina" />
			<asp:ListItem Text="North Dakota" Value="North Dakota" />
			<asp:ListItem Text="Ohio" Value="Ohio" />
			<asp:ListItem Text="Oklahoma" Value="Oklahoma" />
			<asp:ListItem Text="Oregon" Value="Oregon" />
			<asp:ListItem Text="Pennsylvania" Value="Pennsylvania" />
			<asp:ListItem Text="Rhode Island" Value="Rhode Island" />
			<asp:ListItem Text="South Carolina" Value="South Carolina" />
			<asp:ListItem Text="South Dakota" Value="South Dakota" />
			<asp:ListItem Text="Tennessee" Value="Tennessee" />
			<asp:ListItem Text="Texas" Value="Texas" />
			<asp:ListItem Text="Utah" Value="Utah" />
			<asp:ListItem Text="Vermont" Value="Vermont" />
			<asp:ListItem Text="Virginia" Value="Virginia" />
			<asp:ListItem Text="Washington" Value="Washington" />
			<asp:ListItem Text="West Virginia" Value="West Virginia" />
			<asp:ListItem Text="Wisconsin" Value="Wisconsin" />
			<asp:ListItem Text="Wyoming" Value="Wyoming" />
		</asp:DropDownList>
	</div>
	<div class="col-xs-4 col-md-5">
		<asp:RequiredFieldValidator ID="reqValState" runat="server" ControlToValidate="ddlState"
			ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="BrokerProfileForm" />
	</div>
</div>
<div class="form-group">
	<label for="txtZip" class="col-xs-12 col-md-3 control-label">Zip Code <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtZip" runat="server" CssClass="form-control" MaxLength="5" placeholder="#####" />
	</div>
	<div class="col-xs-12 col-md-5">
		<asp:RequiredFieldValidator ID="reqValZip" runat="server" ControlToValidate="txtZip"
			ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="BrokerProfileForm" />
		<asp:RegularExpressionValidator ID="regValZip" runat="server" ControlToValidate="txtZip"
			ErrorMessage="Invalid zip or incorrect format" SetFocusOnError="true" ValidationGroup="BrokerProfileForm"
			ValidationExpression="^[0-9]{5}$" />
	</div>
</div>
<div class="form-group">
	<label for="txtEmail" class="col-xs-12 col-md-3 control-label">Email <span class="text-danger">*</span></label>
	<div class="col-xs-12 col-md-4">
		<asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="100" />
		<div class="text-info">(This will be your Login Name)</div>
	</div>
	<div class="col-xs-12 col-md-5">
		<asp:RequiredFieldValidator ID="reqValEmail" ControlToValidate="txtEmail" runat="server"
			ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="BrokerProfileForm" />
		<asp:RegularExpressionValidator ID="regValEmail" runat="server" ControlToValidate="txtEmail"
			ErrorMessage="Invalid email or incorrect format" SetFocusOnError="true" ValidationGroup="BrokerProfileForm"
			ValidationExpression="^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*[ ]*$" />
	</div>
</div>
<asp:PlaceHolder ID="phUpdateOnly" runat="server">
	<div class="form-group">
		<label for="txtVerifyPassword" class="col-xs-12 col-md-3 control-label">Verify Password <span class="text-danger">*</span></label>
		<div class="col-xs-12 col-md-4">
			<asp:TextBox ID="txtVerifyPassword" runat="server" CssClass="form-control" MaxLength="20" TextMode="Password" />
		</div>
		<div class="col-xs-12 col-md-5">
			<asp:RequiredFieldValidator ID="reqValVerifyPassword" runat="server" ControlToValidate="txtVerifyPassword"
				ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="BrokerProfileForm" />
			<asp:RegularExpressionValidator ID="regValVerifyPassword" runat="server" ControlToValidate="txtVerifyPassword"
				ErrorMessage="Must be at least 6 characters" SetFocusOnError="true" ValidationGroup="BrokerProfileForm"
				ValidationExpression=".{6,20}" />
		</div>
	</div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phRegisterOnly" runat="server">
	<div class="form-group">
		<label for="txtPassword" class="col-xs-12 col-md-3 control-label">Password <span class="text-danger">*</span></label>
		<div class="col-xs-12 col-md-4">
			<asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" MaxLength="20" TextMode="Password" />
		</div>
		<div class="col-xs-12 col-md-5">
			<asp:RequiredFieldValidator ID="reqValPassword" runat="server" ControlToValidate="txtPassword"
				ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="BrokerProfileForm" />
			<asp:RegularExpressionValidator ID="regValPassword" runat="server" ControlToValidate="txtPassword"
				ErrorMessage="Must be at least 6 characters" SetFocusOnError="true" ValidationGroup="BrokerProfileForm"
				ValidationExpression=".{6,20}" />
		</div>
	</div>
	<div class="form-group">
		<label for="txtPassword2" class="col-xs-12 col-md-3 control-label">Repeat Password <span class="text-danger">*</span></label>
		<div class="col-xs-12 col-md-4">
			<asp:TextBox ID="txtPassword2" runat="server" CssClass="form-control" MaxLength="20" TextMode="Password" />
		</div>
		<div class="col-xs-12 col-md-5">
			<asp:RequiredFieldValidator ID="reqValRepeatPassword" runat="server" ControlToValidate="txtPassword2"
				ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="BrokerProfileForm" />
			<asp:CompareValidator ID="comValPassword" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtPassword2"
				ErrorMessage="Passwords do not match" SetFocusOnError="true" ValidationGroup="BrokerProfileForm" />
		</div>
	</div>
</asp:PlaceHolder>
