﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BrokerLoginWidget.ascx.cs" Inherits="Website.Sublayouts.Broker.BrokerLoginWidget" %>

<!-- Begin Sublayouts/Broker/BrokerLoginWidget -->
<div class="row">
	<div class="col-xs-12">
		<h2>Broker Login</h2>

		<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
			<div class="form-group">
				<label for="txtEmail" class="col-xs-12 col-md-2">Email <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="100" />
				</div>
				<div class="col-xs-4 col-md-6">
					<asp:RequiredFieldValidator ID="reqValEmail" runat="server" ControlToValidate="txtEmail"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="BrokerLoginWidget" />
					<asp:RegularExpressionValidator id="regExValEmail" runat="server" ControlToValidate="txtEmail"
						ErrorMessage="Invalid email or incorrect format" SetFocusOnError="true" ValidationGroup="BrokerLoginWidget"
						ValidationExpression="^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtPassword" class="col-xs-12 col-md-2">Password <span class="text-danger">*</span></label>
				<div class="col-xs-8 col-md-4">
					<asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" MaxLength="20" TextMode="Password" />
				</div>
				<div class="col-xs-4 col-md-6">
					<asp:RequiredFieldValidator ID="reqValPassword" runat="server" ControlToValidate="txtPassword"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="BrokerLoginWidget" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-4 col-md-offset-2">
					<asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_Click" Text="Login" ValidationGroup="BrokerLoginWidget" />
				</div>
			</div>
		</asp:Panel>

		<div class="row">
			<div class="col-xs-12">
				<p>First time user? <cm:Link ID="lnkRegister" CssClass="registerBtn" ItemReferenceName="BROKER_LOGIN_SETTINGS_ID" Field="Register Link" runat="server" /></p>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<p><cm:Link ID="lnkForgotPassword" CssClass="registerBtn" ItemReferenceName="BROKER_LOGIN_SETTINGS_ID" Field="Forgot Password Link" runat="server" />?</p>
			</div>
		</div>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End Sublayouts/Broker/BrokerLoginWidget -->
