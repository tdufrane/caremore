﻿using System;
using System.Linq;
using System.Collections.Generic;
using CareMore.Web.DotCom;

namespace Website.Sublayouts.Broker
{
	public partial class BrokerPasswordChange : BrokerBaseUserControl
	{
		#region Private variables

		private const string PostBackSessionName = "BrokerPasswordChange";

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				if (IsPostBack)
				{
					CareMoreUtilities.ClearControl(phError);
				}
				else
				{
					base.VerifyLogin();

					Session[PostBackSessionName] = null;

					LoadPage();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				if (Session[PostBackSessionName] == null)
				{
					Page.Validate("BrokerPasswordChange");

					if (Page.IsValid)
					{
						if (ChangePassword())
						{
							// Set session to prevent refresh posts
							Session[PostBackSessionName] = DateTime.Now;
							phConfirmation.Visible = true;
							pnlForm.Visible = false;
						}
					}
					else
					{
						CareMoreUtilities.DisplayInvalidSubmission(phError, Page.Validators, pnlForm);
					}
				}
				else
				{
					CareMoreUtilities.DisplayNoMultiSubmit(phError);
					phConfirmation.Visible = false;
					pnlForm.Visible = false;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			if (!BrokerProfile.PasswordUpdated)
			{
				//TODO: How to hide broker menu??
				lnkCancel.Attributes.Add("disabled", "disabled");
			}
		}

		private bool ChangePassword()
		{
			bool changed = false;

			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			IEnumerable<BrokerRegistration> profiles = db.BrokerRegistrations.Where(item =>
				item.Email.ToLower() == BrokerProfile.Email.ToLower());

			if ((profiles == null) || (profiles.Count() == 0))
			{
				CareMoreUtilities.DisplayError(phError, CurrentItem["NotFoundMessage"]);
			}
			else
			{
				BrokerRegistration profile = profiles.FirstOrDefault();

				if (!profile.Verified)
				{
					CareMoreUtilities.DisplayError(phError,
						string.Format(CurrentItem["RegistrationNotVerified"], "Change Your Password"));
					phConfirmation.Visible = false;
					pnlForm.Visible = false;
				}
				else if (profile.Disabled)
				{
					CareMoreUtilities.DisplayError(phError,
						string.Format(CurrentItem["RegistrationDisabled"], "Change Your Password"));
					phConfirmation.Visible = false;
					pnlForm.Visible = false;
				}
				else if (profile.Password != Common.Encrypt(txtOldPassword.Text))
				{
					CareMoreUtilities.DisplayError(phError,
						string.Format(CurrentItem["InvalidOldPassword"], "Change Your Password"));
					phConfirmation.Visible = false;
					pnlForm.Visible = true;
				}
				else
				{
					profile.Password = Common.Encrypt(txtPassword.Text);
					profile.PasswordUpdated = true;
					profile.UpdatedOn = DateTime.Now;
					profile.UpdatedBy = profile.FirstName + " " + profile.LastName;

					db.SubmitChanges();

					BrokerProfile = profile;
					changed = true;
				}
			}

			return changed;
		}

		#endregion
	}
}
