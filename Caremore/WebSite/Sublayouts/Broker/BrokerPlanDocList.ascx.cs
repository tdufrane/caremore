﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom;
using Sitecore.Collections;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Links;
using Sitecore.Resources.Media;
using Website.BL.ListItem;
using Website.BL.Plans;

namespace Website.Sublayouts.Broker
{
	public partial class BrokerPlanDocList : BrokerBaseUserControl
	{
		#region Variables

		private Item regionsItem = null;
		private Item planTypesItem = null;

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				regionsItem = CurrentDb.GetItem(CareMoreUtilities.REGIONS_PATH);
				planTypesItem = ItemIds.GetItem("PLAN_TYPES");

				if (!IsPostBack)
				{
					LoadForm();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RptDocuments_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			switch (e.Item.ItemType)
			{
				case ListItemType.AlternatingItem:
				case ListItemType.Item:
					Repeater rptPlanTypeLinks = (Repeater)e.Item.FindControl("rptPlanTypeLinks");
					rptPlanTypeLinks.DataSource = ((BrokerPlanItem)e.Item.DataItem).Files;
					rptPlanTypeLinks.DataBind();
					break;

				default:
					break;
			}

		}

		protected void BtnList_Click(object sender, EventArgs e)
		{
			try
			{
				lblYear.Text = ddlYear.SelectedValue;
				lblLanguage.Text = ddlLanguage.SelectedItem.Text;

				ShowDocuments(ddlYear.SelectedValue, ddlLanguage.SelectedValue, ddlStates.SelectedValue, ddlCounties.SelectedValue, ddlPlans.SelectedValue);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void DdlYear_SelectedIndexChanged(object sender, EventArgs e)
		{
			ListItem currentState = ddlStates.SelectedItem;

			ClearList(ddlStates);
			LoadStates(ddlYear.SelectedValue);
			ResetState(currentState);

			DdlStates_SelectedIndexChanged(sender, e);
			DdlCounties_SelectedIndexChanged(sender, e);
		}

		protected void DdlStates_SelectedIndexChanged(object sender, EventArgs e)
		{
			ListItem currentCounty = ddlCounties.SelectedItem;
			ClearList(ddlCounties);

			if (ddlStates.SelectedValue != string.Empty)
			{
				LoadCounties(ddlYear.SelectedValue, ddlStates.SelectedValue);
			}

			ResetCounty(currentCounty);
			DdlCounties_SelectedIndexChanged(sender, e);
		}

		protected void DdlCounties_SelectedIndexChanged(object sender, EventArgs e)
		{
			ListItem currentPlan = ddlPlans.SelectedItem;
			ClearList(ddlPlans);

			LoadPlans(ddlYear.SelectedValue, ddlStates.SelectedValue, ddlCounties.SelectedValue);
			ResetPlan(currentPlan);
			phResults.Visible = false;
		}

		#endregion

		#region Private methods

		private void LoadForm()
		{
			LoadYears();
			LoadStates(ddlYear.Items[0].Value);
			LoadPlans(ddlYear.Items[0].Value, null, null);
		}

		private void LoadYears()
		{
			Item[] years = regionsItem.Axes.GetDescendants()
				.Where(item => item.TemplateName == "Year")
				.GroupBy(item => item.Name)
				.Select(group => group.First())
				.OrderByDescending(item => item.Name)
				.ToArray<Item>();

			foreach (Item year in years)
			{
				ddlYear.Items.Add(new ListItem(year.Name, year.Name));
			}
		}

		private void LoadStates(string year)
		{
			Item[] states = regionsItem.Children.Where(item => item.TemplateName == "Region State").ToArray<Item>();

			foreach (Item state in states)
			{
				if (state.Axes.GetDescendants().Where(item => ((item.TemplateName == "Year") && (item.Name == year) && (item["Hidden"] != "1"))).Count() > 0)
					ddlStates.Items.Add(new ListItem(state.Name, state.Name));
			}
		}

		private void LoadCounties(string year, string state)
		{
			Item stateItem = regionsItem.Children.Where(item => ((item.TemplateName == "Region State") && (item.Name == state))).FirstOrDefault();

			if (stateItem != null)
			{
				Item[] counties = stateItem.Children.Where(item => item.TemplateName == "Region").ToArray<Item>();

				foreach (Item county in counties)
				{
					if (county.Children.Where(item => ((item.TemplateName == "Year") && (item.Name == year) && (item["Hidden"] != "1"))).Count() > 0)
						ddlCounties.Items.Add(new ListItem(county["County"], county.Name));
				}
			}
		}

		private void LoadPlans(string year, string state, string county)
		{
			List<Item> planTypes;

			if (string.IsNullOrEmpty(state))
			{
				planTypes = planTypesItem.Children.Where(item => ((item.TemplateName == "Plan Type") && (item.Name.EndsWith(year)))).ToList<Item>();
			}
			else
			{
				Item[] plans = GetPlans(year, state, county, null);

				planTypes = new List<Item>();

				foreach (Item plan in plans)
				{
					LookupField planType = plan.Fields["Plan Type"];

					if (planType.TargetItem != null)
					{
						if (!planTypes.Any(item => item.ID.Guid.ToString() == planType.TargetItem.ID.Guid.ToString()))
							planTypes.Add(planType.TargetItem);
					}
				}
			}

			if (planTypes.Count > 0)
			{
				foreach (Item plan in planTypes)
				{
					ddlPlans.Items.Add(new ListItem(plan.Name, plan.ID.Guid.ToString("B").ToUpper()));
				}
			}
		}

		private void ClearList(DropDownList list)
		{
			if (list.Items.Count > 0)
				list.Items.Clear();

			list.Items.Add(new ListItem("-all-", string.Empty));
			list.Items[0].Selected = true;
		}

		private Item[] GetPlans(string year, string state, string county, string planId)
		{
			// Search path is: sitecore/content/Global/Regions/<State>/<County/<Year>/[@Plan Type='<Guid>']
			StringBuilder searchPath = new StringBuilder();

			searchPath.Append("./");
			if (string.IsNullOrEmpty(state))
			{
				searchPath.Append("*");
			}
			else
			{
				searchPath.Append(state);
			}

			searchPath.Append("/");
			if (string.IsNullOrEmpty(county))
			{
				searchPath.Append("*");
			}
			else
			{
				searchPath.Append(county);
			}

			searchPath.AppendFormat("/{0}/", year);
			if (string.IsNullOrEmpty(planId))
			{
				searchPath.Append("*[@@templatename='Plan']");
			}
			else
			{
				searchPath.AppendFormat("*[@Plan Type='{0}']", planId);
			}

			return regionsItem.Axes.SelectItems(searchPath.ToString());
		}

		private void ResetState(ListItem lastState)
		{
			int stateIndex = ddlStates.Items.IndexOf(lastState);
			if (stateIndex > -1)
				ddlStates.SelectedIndex = stateIndex;
		}

		private void ResetCounty(ListItem lastCounty)
		{
			int countyIndex = ddlStates.Items.IndexOf(lastCounty);
			if (countyIndex > -1)
				ddlCounties.SelectedIndex = countyIndex;
		}

		private void ResetPlan(ListItem lastPlan)
		{
			int planIndex = ddlPlans.Items.IndexOf(lastPlan);
			if (planIndex > -1)
				ddlPlans.SelectedIndex = planIndex;
		}

		private void ShowDocuments(string year, string language, string state, string county, string planId)
		{
			phResults.Visible = true;

			Item[] plans = GetPlans(year, state, county, planId);

			if ((plans == null) || (plans.Length == 0))
			{
				rptDocuments.Visible = false;
				pnlNoRecords.Visible = true;
			}
			else
			{
				rptDocuments.Visible = true;
				pnlNoRecords.Visible = false;

				List<BrokerPlanItem> docList = new List<BrokerPlanItem>();

				foreach (Item plan in plans)
				{
					LookupField planType = plan.Fields["Plan Type"];

					if (planType == null || planType.TargetItem == null)
						continue;

					// Get the matching plan type for
					string planTypePath = string.Format("./{0}/{1}/*[@Region Name='{2}']", planType.TargetItem.Name,
						plan.Parent.Parent["State"],
						plan.Parent.Parent["Region Name"]);

					Item planTypeItem = planTypesItem.Axes.SelectSingleItem(planTypePath);

					if (planTypeItem == null)
						continue;

					//TODO Spanish!!
					Item planLang;
					Item planTypeLang;
					Item planTypeItemLang;

					if (language == "en")
					{
						planLang = plan;
						planTypeLang = planType.TargetItem;
						planTypeItemLang = planTypeItem;
					}
					else
					{
						Language itemLanguage = Language.Parse(language);
						planLang = CurrentDb.GetItem(plan.ID, itemLanguage);
						planTypeLang = CurrentDb.GetItem(planType.TargetItem.ID, itemLanguage);
						planTypeItemLang = CurrentDb.GetItem(planTypeItem.ID, itemLanguage);
					}

					BrokerPlanItem planItem = new BrokerPlanItem(planTypeLang, planTypeItemLang);
					planItem.Files = PlanManager.GetPlanFiles(planLang, planLang.Parent.Parent);

					Item[] additionalLinks = CurrentItem.Axes.SelectItems("./Content/*[@@templatename='General Link']");
					if ((additionalLinks != null) && (additionalLinks.Length > 0))
					{
						foreach (Item linkItem in additionalLinks)
						{
							PlanFileItem fileItem = SetAdditionalLink(plan, linkItem);

							if (!string.IsNullOrWhiteSpace(fileItem.Url))
								planItem.Files.Add(fileItem);
						}
					}

/*
http://dev.caremore.com/Locate-Services/Care-Centers.aspx?s=California&c=Orange
kLocate-Services/Hospitals.aspx
Provider Search (Doctor Search)
Hospital Search
Urgent Care Center Search
CareMore Care Center Search
*/
					docList.Add(planItem);
				}

				rptDocuments.DataSource = docList;
				rptDocuments.DataBind();
			}
		}

		private PlanFileItem SetAdditionalLink(Item plan, Item linkItem)
		{
			LinkField field = (LinkField)linkItem.Fields["Link"];
			PlanFileItem fileItem = new PlanFileItem();

			//if (!string.IsNullOrWhiteSpace(field.Class))
			//	fileItem.CssClass = field.Class;

			if (!string.IsNullOrWhiteSpace(field.Target))
				fileItem.Target = field.Target;

			if (!string.IsNullOrWhiteSpace(field.Text))
			{
				fileItem.Text = field.Text;
			}
			else
			{
				fileItem.Text = linkItem.Name;
			}

			if (field.TargetItem == null)
			{
				fileItem.Url = field.Url;
			}
			else if (field.IsInternal)
			{
				fileItem.Url = string.Format("{0}?s={1}&c={2}", LinkManager.GetItemUrl(field.TargetItem), plan.Parent.Parent["State"], plan.Parent.Parent["County"]);
			}
			else if (field.IsMediaLink)
			{
				fileItem.Url = MediaManager.GetMediaUrl(field.TargetItem);
			}

			return fileItem;
		}

		#endregion
	}
}
