﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CareMore.Web.DotCom;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace Website.Sublayouts.Broker
{
	public partial class BrokerPasswordChallenge : System.Web.UI.UserControl
	{
		private const string PostBackSessionName = "BrokerPasswordChallenge";

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				if (IsPostBack)
				{
					phError.Controls.Clear();
					phError.Visible = false;
				}
				else
				{
					Session[PostBackSessionName] = null;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnGenerate_Click(object sender, EventArgs e)
		{
			try
			{
				if (Session[PostBackSessionName] == null)
				{
					Page.Validate("BrokerPasswordChallenge");

					if (Page.IsValid && ucReCaptcha.IsValid())
					{
						if (SetTempPassword())
						{
							// Set session to prevent refresh posts
							Session[PostBackSessionName] = DateTime.Now;
						}
					}
					else
					{
						CareMoreUtilities.DisplayInvalidSubmission(phError, Page.Validators, pnlForm);
					}
				}
				else
				{
					phConfirmation.Visible = false;
					pnlForm.Visible = false;

					CareMoreUtilities.DisplayNoMultiSubmit(phError);
				}
				
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
				return;
			}
		}

		#endregion

		#region Private methods

		private bool SetTempPassword()
		{
			bool changed = false;

			Item currentItem = Sitecore.Context.Item;

			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			IEnumerable<BrokerRegistration> profiles = db.BrokerRegistrations.Where(item =>
				item.Email.ToLower() == txtEmail.Text.ToLower());

			if ((profiles == null) || (profiles.Count() == 0))
			{
				CareMoreUtilities.DisplayError(phError, currentItem["NotFoundMessage"]);
			}
			else
			{
				BrokerRegistration profile = profiles.FirstOrDefault();

				if (!profile.Verified)
				{
					phConfirmation.Visible = false;
					pnlForm.Visible = false;

					CareMoreUtilities.DisplayError(phError,
						string.Format(currentItem["RegistrationNotVerified"], "Reset Your Password"));
				}
				else if (profile.Disabled)
				{
					phConfirmation.Visible = false;
					pnlForm.Visible = false;

					CareMoreUtilities.DisplayError(phError,
						string.Format(currentItem["RegistrationDisabled"], "Reset Your Password"));
				}
				else
				{
					string newPassword = GeneratePassword(profile);

					profile.Password = Common.Encrypt(newPassword);
					profile.PasswordUpdated = false;
					db.SubmitChanges();

					InternalLinkField link = (InternalLinkField)currentItem.Fields["NavigateTo"];

					string body = string.Format(currentItem["MailContent"], newPassword, LinkManager.GetItemUrl(link.TargetItem));
					BrokerHelper.SendEmailNotification(profile.Email, body);

					pnlForm.Visible = false;
					phConfirmation.Visible = true;
					changed = true;
				}
			}

			return changed;
		}

		private string GeneratePassword(BrokerRegistration profile)
		{
			StringBuilder password = new StringBuilder();
			password.Append(profile.FirstName.Substring(0, 1));
			password.Append(profile.LastName.Substring(0, 1));
			password.Append(DateTime.Now.ToString("yyMMddhhmm"));
			password.Append("!");
			return password.ToString();
		}

		#endregion
	}
}
