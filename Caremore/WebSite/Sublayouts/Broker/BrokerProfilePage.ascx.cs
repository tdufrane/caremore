﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Collections;
using System.Text;
using System.Net.Mail;

namespace Website.Sublayouts.Broker
{
	public partial class BrokerProfilePage : BrokerBaseUserControl
	{
		#region Private variables

		private const string PostBackSessionName = "BrokerProfilePage";

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				if (IsPostBack)
				{
					CareMoreUtilities.ClearControl(phError);
				}
				else
				{
					Session[PostBackSessionName] = null;
					LoadPage();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				if (Session[PostBackSessionName] == null)
				{
					Page.Validate("BrokerProfileForm");

					if (Page.IsValid)
					{
						if (ucProfileForm.ChangeRegistration())
						{
							//Set session to prevent refresh posts
							Session[PostBackSessionName] = DateTime.Now;
							phConfirmation.Visible = true;
							pnlForm.Visible = false;
						}
						else
						{
							CareMoreUtilities.DisplayError(phError,
								string.Format(CurrentItem["InvalidPassword"], "Change Your Profile"));
						}
					}
					else
					{
						CareMoreUtilities.DisplayInvalidSubmission(phError, Page.Validators, pnlForm);
					}
				}
				else
				{
					phConfirmation.Visible = false;
					pnlForm.Visible = false;
					CareMoreUtilities.DisplayNoMultiSubmit(phError);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			string brokerId = string.Empty;
			if (brokerId == null) return;

			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			IEnumerable<BrokerRegistration> profiles = db.BrokerRegistrations.Where(item =>
				item.Email.ToLower() == BrokerProfile.Email.ToLower());

			if ((profiles == null) || (profiles.Count() == 0))
			{
				CareMoreUtilities.DisplayError(phError, CurrentItem["NotFoundMessage"]);
			}
			else
			{
				BrokerRegistration profile = profiles.FirstOrDefault();

				if (!profile.Verified)
				{
					CareMoreUtilities.DisplayError(phError,
						string.Format(CurrentItem[RegistrationNotVerifiedKey], "Change Your Password"));
					phConfirmation.Visible = false;
					pnlForm.Visible = false;
				}
				else if (profile.Disabled)
				{
					CareMoreUtilities.DisplayError(phError,
						string.Format(CurrentItem[RegistrationDisabledKey], "Change Your Password"));
					phConfirmation.Visible = false;
					pnlForm.Visible = false;
				}
				else
				{
					ucProfileForm.LoadRegistration(profile);
				}
			}
		}

		#endregion
	}
}
