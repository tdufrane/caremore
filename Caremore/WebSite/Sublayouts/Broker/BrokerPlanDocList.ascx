﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BrokerPlanDocList.ascx.cs" Inherits="Website.Sublayouts.Broker.BrokerPlanDocList" %>

<asp:PlaceHolder ID="phError" runat="server" Visible="false" />

<asp:PlaceHolder ID="phForm" runat="server">
	<asp:Panel ID="brokerPlan" runat="server" ClientIDMode="Static" DefaultButton="btnSubmit">
		<div class="form">
			<p>Required fields are indicated with an asterisk (<span class="red">*</span>)</p>

			<asp:ValidationSummary ID="valSumPage" runat="server" CssClass="errorMsg" HeaderText="Please correct the following fields:" />

			<div class="formField">
				<label for="ddlYear">Year<span class="red">*</span></label>
				<div class="field">
					<asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="true"
						OnSelectedIndexChanged="DdlYear_SelectedIndexChanged"></asp:DropDownList>
				</div>
			</div>
			<div class="formField">
				<label for="ddlLanguage">Language<span class="red">*</span></label>
				<div class="field">
					<asp:DropDownList ID="ddlLanguage" runat="server">
						<asp:ListItem Text="English" Value="en" Selected="True" />
						<asp:ListItem Text="Espanol" Value="es-mx" />
					</asp:DropDownList>
				</div>
			</div>
			<div class="formField">
				<label for="ddlState">State</label>
				<div class="field">
					<asp:DropDownList ID="ddlStates" runat="server" AppendDataBoundItems="true"
						AutoPostBack="true" OnSelectedIndexChanged="DdlStates_SelectedIndexChanged">
						<asp:ListItem Text="-all-" Value="" />
					</asp:DropDownList>
				</div>
			</div>
			<div class="formField">
				<label for="ddlCounty">County</label>
				<div class="field">
					<asp:DropDownList ID="ddlCounties" runat="server" AppendDataBoundItems="true"
						AutoPostBack="true" OnSelectedIndexChanged="DdlCounties_SelectedIndexChanged">
						<asp:ListItem Text="-all-" Value="" />
					</asp:DropDownList>
				</div>
			</div>
			<div class="formField">
				<label for="ddlPlan">Plan</label>
				<div class="field">
					<asp:DropDownList ID="ddlPlans" runat="server" AppendDataBoundItems="true">
						<asp:ListItem Text="-all-" Value="" />
					</asp:DropDownList>
				</div>
			</div>
			<div class="formField">
				<div class="label">&nbsp;</div>
				<div class="field">
					<asp:LinkButton ID="btnSubmit" runat="server" CssClass="button btnMed" OnClick="BtnList_Click"><span>List</span></asp:LinkButton>
				</div>
			</div>
		</div>
	</asp:Panel>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phResults" runat="server" Visible="false">
	<hr />
	<h2><asp:Label ID="lblYear" runat="server" /> - <asp:Label ID="lblLanguage" runat="server" /></h2>
	<div><sc:Text ID="txtAcrobat" runat="server" Field="Acrobat Text" /></div>
	<div><sc:Text ID="txtDrugList" runat="server" Field="Drug List Text" /></div>
	<hr />
	<asp:Repeater ID="rptDocuments" runat="server" OnItemDataBound="RptDocuments_ItemDataBound">
		<ItemTemplate>
			<h4><%# Eval("StateName") %> - <%# Eval("CountyName") %></h4>
			<p><strong><%# Eval("PlanName") %></strong></p>

			<asp:Repeater ID="rptPlanTypeLinks" runat="server">
				<HeaderTemplate>
					<ul>
				</HeaderTemplate>
				<ItemTemplate>
						<li><a href="<%# Eval("Url") %>" target="<%# Eval("Target") %>"><%# Eval("Text") %></a></li>
				</ItemTemplate>
				<FooterTemplate>
					</ul>
				</FooterTemplate>
			</asp:Repeater>
			<div><%# Eval("PlanDescription")%></div>
		</ItemTemplate>
		<SeparatorTemplate>
			<hr />
		</SeparatorTemplate>
	</asp:Repeater>
	<asp:Panel ID="pnlNoRecords" runat="server" Visible="false"><p>No plans found, please revise your search.</p></asp:Panel>
</asp:PlaceHolder>

