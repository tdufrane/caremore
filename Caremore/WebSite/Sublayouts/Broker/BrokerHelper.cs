﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using Sitecore.Data.Items;

namespace Website.Sublayouts.Broker
{
	public static class BrokerHelper
	{
		public static void SendEmailNotification(string body)
		{
			SendEmailNotification(null, null, body, null);
		}

		public static void SendEmailNotification(string toAddress, string body)
		{
			SendEmailNotification(toAddress, null, body, null);
		}

		public static void SendEmailNotification(string body, string subject, List<Attachment> attachments)
		{
			SendEmailNotification(null, subject, body, attachments);
		}

		public static void SendEmailNotification(string toAddress, string subject, string body, List<Attachment> attachments)
		{
			Item currentItem = Sitecore.Context.Item;
			string toEmail = string.IsNullOrEmpty(toAddress) ? currentItem["To"] : toAddress;

			if (toEmail.Length > 0)
			{
				MailMessage message = new MailMessage();

				string[] destinationAddresses = toEmail.Split(';');
				foreach (string address in destinationAddresses)
				{
					message.To.Add(new MailAddress(address));
				}

				if (!string.IsNullOrEmpty(currentItem["From"]))
					message.From = new MailAddress(currentItem["From"]);

				if (string.IsNullOrEmpty(subject))
					message.Body = currentItem["Subject"];
				else
					message.Subject = subject;

				if (string.IsNullOrEmpty(body))
					message.Body = currentItem["MailContent"];
				else
					message.Body = body;

				message.IsBodyHtml = true;

				if (attachments != null && attachments.Count > 0)
				{
					foreach (Attachment attachment in attachments)
						message.Attachments.Add(attachment);
				}

				// Send email
				CareMoreUtilities.SendEmail(message);
			}
		}
	}
}
