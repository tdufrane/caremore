﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ApplicationSupport.ascx.cs" Inherits="Website.Sublayouts.Broker.ApplicationSupport" %>

<!-- Begin SubLayouts/Broker/ApplicationSupport -->
<div class="row">
	<div class="col-xs-12">
<asp:PlaceHolder ID="phConfirmation" runat="server" Visible="false">
		<div class="row">
			<div class="col-xs-12">
				<sc:Text ID="txtThankYouMessage" runat="server" Field="ThankYouMessage" />
			</div>
		</div>
</asp:PlaceHolder>

		<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
			<p>Required fields are indicated with an asterisk (<span class="text-danger">*</span>)</p>

			<div class="form-group">
				<label for="txtAppConfNumber" class="col-xs-12 col-md-3 control-label">App. Confirmation # <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtAppConfNumber" runat="server" CssClass="form-control" MaxLength="50" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValAppConfNumber" runat="server" ControlToValidate="txtAppConfNumber"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ApplicationSupport" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtLastName" class="col-xs-12 col-md-3 control-label">Enrollee Last Name <span class="text-danger">*</span>:</label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" MaxLength="50" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValLastName" runat="server" ControlToValidate="txtLastName"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ApplicationSupport" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtFirstName" class="col-xs-12 col-md-3 control-label">Enrollee First Name <span class="text-danger">*</span>:</label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" MaxLength="50" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValFirstName" runat="server" ControlToValidate="txtFirstName"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ApplicationSupport" />
				</div>
			</div>
			<div class="form-group">
				<label for="filUpDocument1" class="col-xs-12 col-md-3 control-label">File(s) to Upload <span class="text-danger">*</span>:</label>
				<div class="col-xs-12 col-md-9">
					<div class="row">
						<div class="col-xs-12 col-md-4">
							<asp:FileUpload ID="filUpDocument1" runat="server" CssClass="form-control" />
						</div>
						<div class="col-xs-12 col-md-5">
							<asp:RequiredFieldValidator ID="reqFldValExcelFile1" runat="server" ControlToValidate="filUpDocument1"
								ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ApplicationSupport" />
							<asp:RegularExpressionValidator ID="regExpValExcelFile1" runat="server" ControlToValidate="filUpDocument1" 
								ErrorMessage="Only Adobe PDFs or images (GIF, JPG, PNG) are allowed"
								SetFocusOnError="true" ValidationGroup="ApplicationSupport"
								ValidationExpression="^.+((\.pdf)|(\.gif)|(\.jpg)|(\.png))$" />
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-4">
							<asp:FileUpload ID="filUpDocument2" runat="server" CssClass="form-control" />
						</div>
						<div class="col-xs-12 col-md-5">
							<asp:RegularExpressionValidator ID="regExpValExcelFile2" runat="server" ControlToValidate="filUpDocument2"
								ErrorMessage="Only Adobe PDFs or images (GIF, JPG, PNG) are allowed"
								SetFocusOnError="true" ValidationGroup="ApplicationSupport"
								ValidationExpression="^.+((\.pdf)|(\.gif)|(\.jpg)|(\.png))$" />
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-4">
							<asp:FileUpload ID="filUpDocument3" runat="server" CssClass="form-control" />
						</div>
						<div class="col-xs-12 col-md-5">
							<asp:RegularExpressionValidator ID="regExpValExcelFile" runat="server" ControlToValidate="filUpDocument3"
								ErrorMessage="Only Adobe PDFs or images (GIF, JPG, PNG) are allowed"
								SetFocusOnError="true" ValidationGroup="ApplicationSupport"
								ValidationExpression="^.+((\.pdf)|(\.gif)|(\.jpg)|(\.png))$" />
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12 col-md-offset-3 col-md-4">
					<asp:LinkButton ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnGenerate_Click" ValidationGroup="ApplicationSupport">Submit</asp:LinkButton>
				</div>
			</div>
		</asp:Panel>


		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End SubLayouts/Broker/ApplicationSupport -->
