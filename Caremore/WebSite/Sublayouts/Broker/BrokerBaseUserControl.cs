﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Links;
using CareMore.Web.DotCom;

namespace Website.Sublayouts.Broker
{
	public class BrokerBaseUserControl : BaseUserControl
	{
		#region Constants

		public const string BrokerRegistrationKey = "BrokerRegistration";
		public const string RegistrationDisabledKey = "RegistrationDisabled";
		public const string RegistrationNotVerifiedKey = "RegistrationNotVerified";

		#endregion

		#region Properties

		public Guid BrokerId
		{
			get
			{
				return BrokerProfile == null ? Guid.Empty : BrokerProfile.RegistrationId;
			}
		}

		public BrokerRegistration BrokerProfile
		{
			get
			{
				object profile = Page.Session[CareMoreUtilities.BROKER_SESSION];
				return profile == null ? null : (BrokerRegistration)profile;
			}
			set
			{
				Session[CareMoreUtilities.BROKER_SESSION] = value;
			}
		}

		#endregion

		#region Methods

		public void VerifyLogin()
		{
			if (Page.Session[CareMoreUtilities.BROKER_SESSION] == null)
			{
				Page.Response.Redirect(LinkManager.GetItemUrl(ItemIds.GetItem("BROKERS")), false);
			}
		}

		#endregion
	}
}
