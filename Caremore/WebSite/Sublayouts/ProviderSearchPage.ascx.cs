﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Security.AntiXss;
using System.Web.UI;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using Sitecore.Collections;
using Sitecore.Data.Items;

namespace Website.Sublayouts
{
	public partial class ProviderSearchPage : BaseUserControl
	{
		#region Variables

		private string pageKey = string.Empty;
		private string pageLastStateKey = string.Empty;

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				pageKey = CurrentItem.TemplateID.ToString();
				pageLastStateKey = pageKey + "_State";

				// if not posack and nto from details page, then load saved
				if (Page.IsPostBack)
				{
					CareMoreUtilities.ClearControl(phError);
				}
				else
				{
					LoadForm();
					Session[pageKey] = base.CurrentState;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayDownMessage(phError, ex, pnlForm);
			}
		}

		protected void BtnSubmit_OnClick(object sender, EventArgs e)
		{
			try
			{
				SetupSearch();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayDownMessage(phError, ex, pnlForm);
			}
		}

		#endregion

		#region Private methods

		private void LoadForm()
		{
			// Cache changes at 6 am the next day after CODS refresh
			DateTime expiresOn = DateTime.Now.Date.Add(new TimeSpan(1, 6, 0, 0));

			PopulateLanguageDropDownList(ddlLanguages, expiresOn);
			PopulateSpecialityDropDownList(ddlSpecialties, expiresOn);

			SetState(Request.QueryString["s"]);

			btnSubmit.Text = CurrentItem["Find Button Label"];
			btnSubmit.ToolTip = btnSubmit.Text;
		}

		private void PopulateLanguageDropDownList(ListBox list, DateTime expiresOn)
		{
			ListItem li = new ListItem() { Text = "All", Value = string.Empty };
			li.Selected = true;
			list.Items.Add(li);

			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
			IEnumerable<LanguageItem> languages;
			languages = from lang in dbContext.LanguageItems
			            orderby lang.Description
			            select lang;

			foreach (LanguageItem row in languages)
			{
				li = new ListItem() { Text = row.Description, Value = row.Code };
				list.Items.Add(li);
			}
		}

		private void PopulateSpecialityDropDownList(ListBox list, DateTime expiresOn)
		{
			ListItem li = new ListItem() { Text = "All", Value = string.Empty };
			li.Selected = true;
			list.Items.Add(li);

			li = new ListItem() { Text = ItemIds.GetItem("PCP_SELECT_ID")["Text"], Value = "PCP" };
			list.Items.Add(li);

			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
			IEnumerable<SpecialtyItem> specialties;
			specialties = from specialty in dbContext.SpecialtyItems
			              orderby specialty.Description
			              select specialty;

			foreach (SpecialtyItem row in specialties)
			{
				li = new ListItem() { Text = row.Description, Value = row.Code };
				list.Items.Add(li);
			}
		}

		private void FillStateList(string state)
		{
			var states = ItemIds.GetItem("STATE_SETTINGS_ID").Children.Where(item => item["Exclude From Provider Lists"] != "1");

			if (states.Count() > 0)
			{
				foreach (Item stateItem in states)
				{
					ddlStates.Items.Add(new ListItem(stateItem["Name"], stateItem["Code"]));
				}

				if (state != null)
				{
					ListItem item = ddlStates.Items.FindByText(state);

					if (item == null)
						item = ddlStates.Items.FindByValue(state);

					if (item != null)
						item.Selected = true;
				}
			}
		}

		private void SetState(string queryState)
		{
			ChildList stateItems = ItemIds.GetItem("STATE_SETTINGS_ID").Children;

			// Figure out the last used State for this page
			string state;
			if (string.IsNullOrEmpty(queryState))
			{
				state = SetState(base.CurrentState, true);
			}
			else
			{
				var states = stateItems.Where(item =>
					(item["Exclude From Provider Lists"] != "1") &&
					(item["Name"].ToLower() == queryState.ToLower()));

				if (states.Count() == 1)
					state = SetState(queryState, false);
				else
					state = SetState(base.CurrentState, true);
			}

			// Load the dropdowns
			FillStateList(state);
		}

		private string SetState(string state, bool useLastState)
		{
			// Figure out the last used State for this page
			if (useLastState)
			{
				string lastSiteState;
				if (Session[pageKey] == null)
					lastSiteState = state;
				else
					lastSiteState = (string)Session[pageKey];

				string lastState;
				if (Session[pageLastStateKey] == null)
					lastState = state;
				else
					lastState = (string)Session[pageLastStateKey];

				if ((state.Equals(lastSiteState)) && (!lastState.Equals(state)))
				{
					state = lastState;
				}
			}

			return state;
		}

		private void SetupSearch()
		{
			StringBuilder url = new StringBuilder();

			Item resultsPage = CurrentItem.Children["Results"];
			url.Append(Sitecore.Links.LinkManager.GetItemUrl(resultsPage));
			url.Append("?");

			string selections = CareMoreUtilities.GetSelectedItems(ddlSpecialties.Items, ",");
			if (selections.Length > 0)
				url.AppendFormat("specialty={0}&", AntiXssEncoder.UrlEncode(selections));

			if (txtLastName.Text.Length > 0)
				url.AppendFormat("lastName={0}&", AntiXssEncoder.UrlEncode(txtLastName.Text));

			if (txtZipCode.Text.Length > 0)
				url.AppendFormat("zip={0}&", AntiXssEncoder.UrlEncode(txtZipCode.Text));

			if (txtCity.Text.Length > 0)
				url.AppendFormat("city={0}&",AntiXssEncoder.UrlEncode(txtCity.Text));

			if (!string.IsNullOrEmpty(ddlStates.SelectedValue))
				url.AppendFormat("state={0}&", AntiXssEncoder.UrlEncode(ddlStates.SelectedValue));

			selections = CareMoreUtilities.GetSelectedItems(ddlLanguages.Items, ",");
			if (selections.Length > 0)
				url.AppendFormat("language={0}", AntiXssEncoder.UrlEncode(selections));

			Session[pageLastStateKey] = AntiXssEncoder.HtmlEncode(ddlStates.SelectedValue, false);
			Session["SearchResults"] = null;
			Session["DataPagerStartRowIndex"] = null;

			string redirectUrl = url.ToString().TrimEnd(new char[] { '?', '&' });
			Response.Redirect(redirectUrl, false);
		}

		#endregion
	}
}
