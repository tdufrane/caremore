﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Linq;

using Sitecore.Data.Items;

namespace Website.Sublayouts
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class CountyServices : System.Web.Services.WebService
    {
        [WebMethod]
        public string[] Find(string prefixText, int count)
        {
            string state = string.Empty;
            string county = string.Empty;

            if (Session["ServiceState"] != null)
            {
                state = (string)Session["ServiceState"];
            }

            if (Session["ServiceCounty"] != null)
            {
                county = (string)Session["ServiceCounty"];
            }

            XDocument zipCodes = GetZipCodes();

            bool isNumeric = Regex.IsMatch(prefixText, @"^\d+$");
            IEnumerable<string> matches;

            if (isNumeric)
            {
                matches = FindByZipCode(state, county, prefixText, zipCodes);
            }
            else
            {
                matches = FindByCity(state, county, prefixText, zipCodes);
            }

            return matches.ToArray<string>();
        }

        private IEnumerable<string> FindByCity(string state, string county, string cityPrefix, XDocument zipCodes)
        {
            var matches = from item in zipCodes.Descendants("ZipCode")
                          where item.Element("City").Value.StartsWith(cityPrefix)
                          select item;

            if (!string.IsNullOrEmpty(state))
            {
                matches = matches.Where(item => item.Element("State").Value.Equals(state));
            }

            if (!string.IsNullOrEmpty(county))
            {
               matches = matches.Where(item => item.Element("County").Value.Equals(county));
            }

            var orderedCities = from item in matches
                                orderby item.Element("City").Value
                                select item.Element("City").Value;

            return orderedCities;
        }

        private IEnumerable<string> FindByZipCode(string state, string county, string zipCodePrefix, XDocument zipCodes)
        {
            var matches = from item in zipCodes.Descendants("ZipCode")
                          where item.Element("Number").Value.StartsWith(zipCodePrefix)
                          select item;

            if (!string.IsNullOrEmpty(state))
            {
                matches = matches.Where(item => item.Element("State").Value.Equals(state));
            }

            if (!string.IsNullOrEmpty(county))
            {
                matches = matches.Where(item => item.Element("County").Value.Equals(county));
            }

            var orderedNumbers = from item in matches
                                 orderby item.Element("Number").Value
                                 select item.Element("Number").Value;

            return orderedNumbers;
        }

        public static XDocument GetZipCodes()
        {
            XDocument xmlDoc = null;

            if (HttpContext.Current.Cache["ZipCodeCache"] == null)
            {
                string sitecorePath = string.Format("{0}/ZipCodes.xml", CareMoreUtilities.MEDIA_PATH);
                Item zipCodeItem = Sitecore.Context.Database.Items[sitecorePath];
                MediaItem mediaItem = new MediaItem(zipCodeItem);

                StreamReader reader = null;
                try
                {
                    reader = new StreamReader(mediaItem.GetMediaStream());
                    xmlDoc = XDocument.Load(reader, LoadOptions.None);
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }

                HttpContext.Current.Cache.Insert("ZipCodeCache", xmlDoc);
            }
            else
            {
                xmlDoc = (XDocument)HttpContext.Current.Cache["ZipCodeCache"];
            }

            return xmlDoc;
        }
    }
}
