﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using CareMore.Web.DotCom.Provider;

using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;

namespace Website.Sublayouts
{
	public partial class DocumentList : BaseUserControl
	{
		#region Public

		public enum ListTypes
		{
			List = 0,
			Collapsible = 1,
			Dropdown = 2,
			Indented = 3
		}

		public ListTypes ListType
		{ 
			get 
			{
				if (ViewState["ListType"] == null)
				{
					return ListTypes.List;
				}
				else
				{
					return (ListTypes)ViewState["ListType"];
				}
			}
			set 
			{
				ViewState["ListType"] = value;
			}
		}

		#endregion

		#region Variables

		private Item[] categories;
		private Item[] documents;
		private Item[] localizationList;
		private Item listItem = null;
		private string state = null;
		private string county = null;
		private string categoryDropdownLabel = null;
		private string subCategoryDropdownLabel = null;
		private string categoryDropdownSelectionLabel = null;
		private string subCategoryDropdownSelectionLabel = null;
		//private bool expandCategory;
		private string showAs;
		
		#endregion

		#region Page Events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			state = CareMoreUtilities.GetCurrentLocale();

			if (Session["County"] != null && (string)Session["County"] != string.Empty)
			{
				county = (string)Session["County"];
			}

			if (DataSource != null && DataSource.TemplateID == ItemIds.GetID("DOCUMENT_LIST_TEMPLATE_ID"))
			{
				listItem = DataSource;
			}

			GetDocumentListData();

			if (!Page.IsPostBack)
			{
				if (string.IsNullOrEmpty(listItem["Text Before"]))
					pnlDocumentListTextBefore.Visible = false;
				else
					txtBefore.Item = listItem;

				if (string.IsNullOrEmpty(listItem["Text After"]))
					pnlDocumentListTextAfter.Visible = false;
				else
					txtAfter.Item = listItem;

				ListItemBind(showAs);
			}
		}

		protected void ListItemBind(string showAs)
		{
			if (showAs == "Collapsible")
			{
				this.ListType = ListTypes.Collapsible;
				DataBind(lvCollapsibleTop, documents);
				DataBind(lvCollapsible, categories);
			}
			else if (showAs == "Indented")
			{
				this.ListType = ListTypes.Indented;
				DataBind(lvIndent, categories);
			}
			else if (showAs == "Dropdown")
			{
				if (categories != null && categories.Count() > 0)
				{
					this.ListType = ListTypes.Dropdown;
					pnlUpdate.Visible = true;
					FillCategoryList();
				}
			}
			else
			{
				//Default to List
				documents = listItem.Axes.GetDescendants().Where(x => x.TemplateID == ItemIds.GetID("GENERAL_LINK_TEMPLATE_ID")).ToArray<Item>();
				if (documents != null && documents.Count() > 0)
				{
					DataBind(lvList, documents);
				}
			}
		}

		protected void OnCategorySelectionChange(object sender, System.EventArgs e)
		{
			ddlSubCategoryList.Items.Clear();

			Item selectedItem = listItem.Children[ddlCategoryList.SelectedValue];
			Item[] subCategory = ddlCategoryList.SelectedValue == string.Empty ? null : GetChildrenItems(selectedItem, "DOCUMENT_CATEGORY_TEMPLATE_ID");

			if (subCategory != null && subCategory.Count() > 0)
			{
				ClearDropDownListView();

				ddlSubCategoryList.Visible = true;
				ddlSubCategoryList.Enabled = true;
				ddlSubCategoryList.DataTextField = "Name";
				ddlSubCategoryList.DataValueField = "Name";
				DataBind(ddlSubCategoryList, subCategory);
				ddlSubCategoryList.Items.Insert(0, new ListItem(string.Format("- {0} -", listItem["Sub Category Dropdown Default Text"]), string.Empty));
			}
			else
			{
				Item[] documentList = ddlCategoryList.SelectedValue == string.Empty ? null : GetChildrenItems(selectedItem, "GENERAL_LINK_TEMPLATE_ID");
				ddlSubCategoryList.Enabled = false;
				ddlSubCategoryList.Items.Insert(0, new ListItem(string.Format("- {0} -", listItem["Sub Category Dropdown Default Text"]), string.Empty));

				if (documentList != null && documentList.Count() > 0)
				{
					DataBind(DropDownLV, documentList);
					((Label)DropDownLV.FindControl("CategoryName")).Text = selectedItem.Name;
				}
				else
				{
					ClearDropDownListView();
				}
			}
			FindMatchingControls(categories, subCategory);
		}

		protected void OnSubCategorySelectionChange(object sender, System.EventArgs e)
		{
			Item selectedCategoryItem = listItem.Children[ddlCategoryList.SelectedValue];
			Item[] subCategories = ddlCategoryList.SelectedValue == string.Empty ? null : GetChildrenItems(selectedCategoryItem, "DOCUMENT_CATEGORY_TEMPLATE_ID");
			Item selectedItem = listItem.Children[ddlCategoryList.SelectedValue];
			selectedItem = selectedItem.Children[ddlSubCategoryList.SelectedValue];
			Item[] documentList = ddlSubCategoryList.SelectedValue == string.Empty ? null : GetChildrenItems(selectedItem, "GENERAL_LINK_TEMPLATE_ID"); 

			if (documentList != null && documentList.Count() > 0)
			{
				DataBind(DropDownLV, documentList);
				((Label)DropDownLV.FindControl("CategoryName")).Text = selectedItem.Name;
			}
			else
			{
				ClearDropDownListView();
			}
			FindMatchingControls(categories, subCategories);
		}

		protected void LvCollapsibleTop_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				Item thisItem = (Item)e.Item.DataItem;
				HyperLink link = (HyperLink)e.Item.FindControl("hlDocument");

				SetHyperLink(thisItem, link);
			}
		}

		protected void LvCollapsible_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				Item thisItem = (Item)e.Item.DataItem;
				//string name = thisItem.Name.Replace(" ", "_");

				ListView lvCollapsibleInner = (ListView)e.Item.FindControl("lvCollapsibleInner");
				Item[] categoryItems = GetChildrenItems(thisItem, "GENERAL_LINK_TEMPLATE_ID");

				//Literal ltlDiv = (Literal)e.Item.FindControl("ltlDiv");
				//if (expandCategory)
				//{
				//	ltlDiv.Text = string.Format("<div id=\"{0}\" class=\"accordion-body in collapse\">", name);
				//	expandCategory = false;
				//}
				//else
				//{
				//	ltlDiv.Text = string.Format("<div id=\"{0}\" class=\"accordion-body collapse\">", name);
				//}

				DataBind(lvCollapsibleInner, categoryItems);
			}
		}

		protected void LvCollapsibleInner_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				Item thisItem = (Item)e.Item.DataItem;
				HyperLink link = (HyperLink)e.Item.FindControl("hlDocument");
				SetHyperLink(thisItem, link);
			}
		}

		protected void LvIndent_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				Item thisItem = (Item)e.Item.DataItem;
				Label categoryName = (Label)e.Item.FindControl("CategoryName");
				ListView subCategoryView = (ListView)e.Item.FindControl("lvIndent2");
				ListView documentView = (ListView)e.Item.FindControl("lvIndent3");
				Item[] categoryItems = GetChildrenItems(thisItem, "DOCUMENT_CATEGORY_TEMPLATE_ID");
				Item[] documentItems = GetChildrenItems(thisItem, "GENERAL_LINK_TEMPLATE_ID");

				categoryName.Text = thisItem.Name;
				categoryName.Attributes.CssStyle.Add("text-decoration", "underline");

				DataBind(documentView, documentItems);
				DataBind(subCategoryView, categoryItems);
			}
		}

		protected void LvIndent2_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				Item thisItem = (Item)e.Item.DataItem;
				Label subCategoryName = (Label)e.Item.FindControl("SubCategoryName");
				ListView documentView = (ListView)e.Item.FindControl("lvIndent3");
				Item[] documentItems = GetChildrenItems(thisItem, "GENERAL_LINK_TEMPLATE_ID");

				subCategoryName.Text = thisItem.Name;
				DataBind(documentView, documentItems);
			}
		}

		protected void LvIndent3_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				Item thisItem = (Item)e.Item.DataItem;
				HyperLink link = (HyperLink)e.Item.FindControl("hlDocument");

				SetHyperLink(thisItem, link);
			}
		}
		
		protected void DropdownLV_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				Item thisItem = (Item)e.Item.DataItem;
				HyperLink link = (HyperLink)e.Item.FindControl("hlDocument");

				SetHyperLink(thisItem, link);
				link.CssClass = "icon icoArrow";
			}
		}

		protected void LvList_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				Item thisItem = (Item)e.Item.DataItem;
				Item parentItem = null;
				Item grandParentItem = null;

				string linkText;
				LinkField mediaField = (LinkField)thisItem.Fields["Link"];
				if (string.IsNullOrWhiteSpace(mediaField.Text))
				{
					linkText = thisItem.Name;
				}
				else
				{
					linkText = mediaField.Text;
				}

				HyperLink link = (HyperLink)e.Item.FindControl("hlDocument");

				if (thisItem.Parent.TemplateID == ItemIds.GetID("DOCUMENT_CATEGORY_TEMPLATE_ID"))
				{
					parentItem = thisItem.Parent;
					linkText = parentItem.Name + " - " + linkText;

					if (parentItem.Parent.TemplateID == ItemIds.GetID("DOCUMENT_CATEGORY_TEMPLATE_ID"))
					{
						grandParentItem = parentItem.Parent;
						linkText = grandParentItem.Name + " - " + linkText;
					}
				}

				SetHyperLink(thisItem, linkText, link);
			}
		}

		#endregion

		#region Private Methods

		private void GetDocumentListData()
		{
			MultilistField localizationTree = listItem.Fields["Localized For"];
			localizationList = localizationTree.GetItems();
			categories = GetChildrenItems(listItem, "DOCUMENT_CATEGORY_TEMPLATE_ID");
			documents = GetChildrenItems(listItem, "GENERAL_LINK_TEMPLATE_ID");
			showAs = listItem["Show As"];
			categoryDropdownLabel = listItem["Category Dropdown Label"];
			subCategoryDropdownLabel = listItem["Sub Category Dropdown Label"];
			categoryDropdownSelectionLabel = listItem["Category Dropdown Default Text"];
			subCategoryDropdownSelectionLabel = listItem["Sub Category Dropdown Default Text"];

			if (string.IsNullOrEmpty(listItem["Name"]))
			{
				h3ListName.Visible = false;
			}
			else
			{
				h3ListName.Visible = true;
				h3ListName.Controls.Add(new LiteralControl(listItem["Name"]));
			}

			//if (listItem["Expand First Category"] == "1")
			//{
			//	expandCategory = true;
			//}
			//else
			//{
			//	expandCategory = false;
			//}
		}

		private void FillCategoryList()
		{
			if (categories != null && categories.Count() > 0)
			{
				ddlCategoryList.DataTextField = "Name";
				ddlCategoryList.DataValueField = "Name";
				DataBind(ddlCategoryList, categories);
				ddlCategoryList.Items.Insert(0, new ListItem("- " + categoryDropdownSelectionLabel + " -", string.Empty));
				ddlSubCategoryList.Items.Insert(0, new ListItem("- " + subCategoryDropdownSelectionLabel + " -", string.Empty));
			}

			if (string.IsNullOrWhiteSpace(subCategoryDropdownLabel))
			{
				ddlSubCategoryList.Visible = false;
				ddlSubCategoryLabel.Visible = false;
			}
			else
			{
				subCategoryDropDownPanel.Visible = true;
				ddlSubCategoryLabel.Text = subCategoryDropdownLabel + ":";
			}

			ddlCategoryLabel.Text = categoryDropdownLabel + ":";
		}

		private void ClearDropDownListView()
		{
			DataBind(DropDownLV, null);
		}

		private Item[] GetChildrenItems(Item selectedItem, string id)
		{
			if (id == "DOCUMENT_CATEGORY_TEMPLATE_ID")
			{
				return selectedItem.Children.Where(x => x.TemplateID == ItemIds.GetID(id) && IsLocalized(x)).ToArray<Item>();
			}

			return selectedItem.Children.Where(x => x.TemplateID == ItemIds.GetID(id)).ToArray<Item>();			
		}

		private bool IsLocalized(Item categoryItem)
		{
			MultilistField fieldItems = categoryItem.Fields["Localized For"];
			Item[] localizationList = fieldItems.GetItems();

			if (localizationList.Count() == 0 || localizationList.Any(x => x.Name == state || x.Name == county))
			{
				return true;
			}

			return false;
		}

		private void DataBind(ListView view, Item[] itemList)
		{
			if (itemList != null && itemList.Count() > 0)
			{
				view.Visible = true;
				view.DataSource = itemList;
				view.DataBind();
			}
			else
			{
				view.Visible = false;
				view.DataSource = null;
				view.DataBind();
			}
		}

		private void DataBind(DropDownList dropDown, Item[] itemList)
		{
			if (itemList != null && itemList.Count() > 0)
			{
				dropDown.Visible = true;
				dropDown.DataSource = itemList;
				dropDown.DataBind();
			}			
		}

		private void SetHyperLink(Item documentItem, HyperLink link)
		{
			SetHyperLink(documentItem, documentItem.Name, link);
		}

		private void SetHyperLink(Item documentItem, string defaultText, HyperLink link)
		{
			LinkField mediaField = (LinkField)documentItem.Fields["Link"];

			if (!string.IsNullOrWhiteSpace(mediaField.Class))
				link.CssClass = mediaField.Class;

			if (!string.IsNullOrWhiteSpace(mediaField.Target))
				link.Target = mediaField.Target;

			if (!string.IsNullOrWhiteSpace(mediaField.Text))
			{
				link.Text = mediaField.Text;
			}
			else if (!string.IsNullOrWhiteSpace(defaultText))
			{
				link.Text = defaultText;
			}
			else
			{
				link.Text = documentItem.Name;
			}

			if (!string.IsNullOrWhiteSpace(mediaField.Title))
				link.ToolTip = mediaField.Title;

			if (mediaField.TargetItem == null)
				link.NavigateUrl = mediaField.Url;
			else
				link.NavigateUrl = MediaManager.GetMediaUrl(mediaField.TargetItem);
		}

		private void FindMatchingControls(Item[] thisCategories, Item[] thisSubCategories)
		{
			ChangeMatchingControls(this.Page.Controls, thisCategories, thisSubCategories);
		}

		private void ChangeMatchingControls(ControlCollection controls, Item[] thisCategories, Item[] thisSubCategories)
		{
			Type listType = typeof(DocumentList);

			foreach (Control control in controls)
			{
				if (control.GetType().BaseType == listType && control.ClientID != this.ClientID)
				{
					DocumentList controlListItem = (DocumentList)control;

					if ((this.ListType == ListTypes.Dropdown) && (thisCategories.Length == controlListItem.categories.Length))
					{
						Item[] controlListItemSubCat = controlListItem.ddlCategoryList.SelectedValue == string.Empty ? null :
							GetChildrenItems(controlListItem.listItem.Children[controlListItem.ddlCategoryList.SelectedValue], "DOCUMENT_CATEGORY_TEMPLATE_ID");
						bool isMatch = MatchCategories(thisCategories, controlListItem.categories);

						if (isMatch && this.ddlCategoryList.SelectedValue != controlListItem.ddlCategoryList.SelectedValue)
						{
							ChangeDropdownSelection(controlListItem, this.ddlCategoryList, controlListItem.ddlCategoryList, true);
						}
						else if (isMatch && thisSubCategories.Length == controlListItemSubCat.Length)
						{
							bool subCategoryMatch = MatchCategories(thisSubCategories, controlListItemSubCat);

							if (subCategoryMatch && this.ddlSubCategoryList.SelectedValue != controlListItem.ddlSubCategoryList.SelectedValue)
							{
								ChangeDropdownSelection(controlListItem, this.ddlSubCategoryList, controlListItem.ddlSubCategoryList, false);
							}
						}
					}
				}

				ChangeMatchingControls(control.Controls, thisCategories, thisSubCategories);
			}
		}

		private bool MatchCategories(Item[] thisCategories, Item[] listItemCategories)
		{
			// have to compare category list to ensure its the same
			bool match = false;

			for (int index = 0; index < thisCategories.Length; index++)
			{
				Item thisItem = thisCategories[index];
				match = false;

				foreach (Item item in listItemCategories)
				{
					if (thisItem.Name == item.Name)
					{
						match = true;
						break;
					}
				}

				if (!match)
				{
					return false;
				}
			}

			return true;
		}

		private void ChangeDropdownSelection(DocumentList controlListItem, DropDownList thisDropdownList, DropDownList controlDropdownList, bool isCategory)
		{
			ListItem matchedItem = controlDropdownList.Items.FindByValue(thisDropdownList.SelectedValue);
			controlDropdownList.SelectedIndex = controlDropdownList.Items.IndexOf(matchedItem);

			if (isCategory)
			{
				controlListItem.OnCategorySelectionChange(null, EventArgs.Empty);
			}
			else
			{
				controlListItem.OnSubCategorySelectionChange(null, EventArgs.Empty);
			}		
		}

		#endregion
	}
}
