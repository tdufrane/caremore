﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security.AntiXss;
using Sitecore.Data.Items;

namespace Website.Sublayouts
{
	public class ProviderTrainingBasePage : BaseUserControl
	{
		#region Constructor

		protected ProviderTrainingBasePage()
		{
			this.ProviderRegistration = new ProviderTrainingRegistration();
		}

		#endregion

		#region Variables / Constants

		private const string cookieName = "ProviderTrainingRegistrationPage";

		protected enum RegistrationStatuses
		{
			None,
			Expired,
			Registered
		}

		#endregion

		#region Properties

		protected ProviderTrainingRegistration ProviderRegistration { get; private set; }

		#endregion

		#region Protected methods

		protected void RecordAccess(int registrationId)
		{
			CaremoreApplicationDataContext dbContext = new CaremoreApplicationDataContext();
			dbContext.ProviderTrainingResourceAccessAdd(registrationId,
				CurrentItem.ID.Guid, CurrentItem.Name, CurrentItem["Training Category"]);
		}

		protected RegistrationStatuses RegistrationStatus(Item trainingItem)
		{
			int registrationId = GetSavedRegistrationId();
			return RegistrationStatus(trainingItem, registrationId);
		}

		protected RegistrationStatuses RegistrationStatus(Item trainingItem, int registrationId)
		{
			RegistrationStatuses regStatus;

			if (registrationId == 0)
			{
				regStatus = RegistrationStatuses.None;
			}
			else
			{
				this.ProviderRegistration = GetProviderRegistration(registrationId);

				if (this.ProviderRegistration == null)
				{
					regStatus = RegistrationStatuses.None;
				}
				else
				{
					DateTime lastRegistered;

					if (this.ProviderRegistration.ModifiedOn.HasValue)
						lastRegistered = this.ProviderRegistration.ModifiedOn.Value;
					else
						lastRegistered = this.ProviderRegistration.CreatedOn;

					if (lastRegistered.AddYears(1) >= DateTime.Now)
					{
						CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
						IEnumerable<ProviderTrainingAccess> results = db.ProviderTrainingAccesses
							.Where(item => (item.RegistrationID == registrationId && item.ResourceID == trainingItem.ID.Guid));

						regStatus = RegistrationStatuses.Registered;
					}
					else
					{
						regStatus = RegistrationStatuses.Expired;
					}
				}
			}

			return regStatus;
		}

		protected void SetCookie(string registrationID)
		{
			HttpCookie regCookie = Request.Cookies[cookieName];
			if (regCookie == null)
			{
				regCookie = new HttpCookie(cookieName);
			}

			string regId = regCookie.Values["RegistrationID"];
			if (string.IsNullOrEmpty(regId))
			{
				regCookie.Values.Add("RegistrationID", AntiXssEncoder.HtmlEncode(registrationID, false)); // Neutralization of CRLF
			}

			regCookie.Expires = DateTime.Now.AddYears(50);
			Response.Cookies.Add(regCookie);
		}

		#endregion

		#region Private methods

		private ProviderTrainingRegistration GetProviderRegistration(int registrationId)
		{
			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			IEnumerable<ProviderTrainingRegistration> results = db.ProviderTrainingRegistrations.Where(item => (item.RegistrationID == registrationId));

			if (results.Count() > 0)
				return results.First();
			else
				return null;
		}

		private int GetSavedRegistrationId()
		{
			int providerRegistrationId;

			HttpCookie regCookie = Request.Cookies[cookieName];

			if (regCookie == null)
			{
				providerRegistrationId = 0;
			}
			else
			{
				string regId = regCookie.Values["RegistrationID"];

				if (string.IsNullOrWhiteSpace(regId))
				{
					providerRegistrationId = 0;
				}
				else
				{
					int regIdValue = 0;

					if (int.TryParse(regId, out regIdValue))
						providerRegistrationId = regIdValue;
					else
						providerRegistrationId = 0;
				}
			}

			return providerRegistrationId;
		}

		#endregion
	}
}
