﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Links;

namespace Website.Sublayouts
{
	public partial class SelectState : BaseUserControl
	{
		#region Variables

		private const string ReferrerKey = "SelectStateReferrer";

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				if (!IsPostBack)
					LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnCancel_OnClick(object sender, EventArgs e)
		{
			try
			{
				if (ViewState[ReferrerKey] == null)
					Response.Redirect(LinkManager.GetItemUrl(CareMoreUtilities.HomeItem()), false);
				else
					Response.Redirect(ViewState[ReferrerKey].ToString(), false);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_OnClick(object sender, EventArgs e)
		{
			try
			{
				SetLocalization();

				Response.Redirect(LinkManager.GetItemUrl(CareMoreUtilities.HomeItem(rblState.SelectedItem.Text)), false);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		public List<ListItem> GetStates()
		{
			InternalLinkField stateFolder = (InternalLinkField)base.CurrentItem.Fields["States"];
			List<ListItem> list = new List<ListItem>();

			foreach (Item childItem in stateFolder.TargetItem.Children)
			{
				ListItem listItem = new ListItem(childItem["Name"], childItem.ID.ToString());
				list.Add(listItem);
			}

			return list;
		}

		public List<ListItem> GetLanguages()
		{
			List<ListItem> list = new List<ListItem>();

			list.Add(new ListItem("English", "en"));
			list.Add(new ListItem("Español", "es-MX"));

			return list;
		}

		private void LoadPage()
		{
			// Save for redirect - to home if not referred or referred by an external URL
			if (Request.UrlReferrer == null || !Request.UrlReferrer.Host.Equals(Request.Url.Host))
				ViewState.Add(ReferrerKey, LinkManager.GetItemUrl(CareMoreUtilities.HomeItem()));
			else
				ViewState.Add(ReferrerKey, Request.UrlReferrer.PathAndQuery);

			rblState.DataSource = GetStates();
			rblState.DataBind();

			for (int i = 0; i < rblState.Items.Count; i++)
			{
				if (rblState.Items[i].Text.Equals(CurrentState))
				{
					rblState.SelectedIndex = i;
					break;
				}
			}

			rblLanguage.DataSource = GetLanguages();
			rblLanguage.DataBind();

			if (CurrentLanguage.Equals("es-MX", StringComparison.OrdinalIgnoreCase))
				rblLanguage.SelectedIndex = 1;
			else
				rblLanguage.SelectedIndex = 0;

			btnRegionSubmit.Text = base.CurrentItem["Submit Label"];
			btnRegionCancel.Text = base.CurrentItem["Cancel Label"];
		}

		private void SetLocalization()
		{
			CareMoreUtilities.SetState(rblState.SelectedItem.Text, chkRememberMySettings.Checked);
			CareMoreUtilities.SetLanguage(rblLanguage.SelectedItem.Value, chkRememberMySettings.Checked);

			HttpCookie  aCookie = new HttpCookie(CareMoreUtilities.LocalizationStateSelectedKey);
			aCookie.Value = "True";
			Response.Cookies.Add(aCookie);
			Session[CareMoreUtilities.LocalizationStateSelectedKey] = true;
		}

		#endregion
	}
}
