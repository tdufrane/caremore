﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SpanishApplicationPage.ascx.cs" Inherits="Website.Sublayouts.SpanishApplicationPage" %>
<%@ Register TagPrefix="cm" Namespace="CareMore.Web.DotCom.Controls" Assembly="CareMore.Web.DotCom"  %>
<%@ Register src="~/Sublayouts/Controls/EnrollmentAssistance.ascx" tagname="EnrollAssist" tagprefix="uc2" %>

<!-- Begin SubLayouts/SpanishApplicationPage -->
<div class="row">
	<div class="col-xs-12 appPage">
		<asp:HiddenField ID="hidCountyId" runat="server" />
		<asp:HiddenField ID="hidPlanId" runat="server" />
		<asp:HiddenField ID="hidSitecoreId" runat="server" />

<asp:PlaceHolder ID="phApplicationDisclaimer" runat="server">
	<sc:Text ID="txtDisclaimer" runat="server" Field="Disclaimer" />
	<sc:Text ID="txtLocationPlan" runat="server" Field="Location and Plan" />
</asp:PlaceHolder>

<asp:PlaceHolder ID="phPlanHeader" runat="server" Visible="false">
		<div class="row">
			<div class="col-xs-4">
				<h3><asp:Label ID="lblPlanName" runat="server" /></h3>
				<p><strong>Condado de <asp:Label ID="lblCountyName" runat="server" /></strong></p>
				<p><strong><asp:Label ID="lblPlanID" runat="server" /></strong></p>
				<p><strong><asp:Label ID="lblPlanPremium" runat="server" /></strong>
					<asp:PlaceHolder ID="phAdditionalPayment" runat="server">
						<br /><asp:Label ID="lblAdditionalPayment" runat="server" />
					</asp:PlaceHolder>
				</p>
			</div>
			<div class="col-xs-8">
				<p><uc2:EnrollAssist ID="ucEnrollAssist" runat="server" /></p>
				<asp:Literal ID="litNeedHelp" runat="server" />
			</div>
		</div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phConfirmation" runat="server" Visible="false">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>Gracias, su aplicación ha sido recibida.</strong></div>
		<div class="panel-body">
			<p>Note por favor su número de confirmación <asp:Label ID="lblConfNum" runat="server" Font-Bold="true" />  para futura referencia.
				Nosotros le contactaremos por teléfono con preguntas o actualizaciones acerca de su aplicación.
				Esperamos tenerle como una parte de la familia de CareMore.</p>
			<p>Guarde por favor esta confirmación para sus registros. Haga clic en el botón debajo de imprimir.</p>
			<p><a href="#" class="btn btn-info" onclick="javascript:window.print();" role="button">Imprimir <span class="glyphicon glyphicon-print"></span></a></p>
		</div>
	</div>
	<asp:Literal ID="litCustomCodeSubmit" runat="server" />
</asp:PlaceHolder>

<asp:PlaceHolder ID="phStatement" runat="server" Visible="false">
	<asp:Literal ID="litStatementOfUnderstanding" runat="server" />
	<p><asp:LinkButton ID="lnkBtnAgreedSOU" runat="server" OnClick="LnkBtn_StatementAgreedClick" CssClass="btn btn-primary">Concuerdo</asp:LinkButton></p>
	<asp:Literal ID="litCustomCodeLoad" runat="server" />
</asp:PlaceHolder>

<asp:PlaceHolder ID="phApplicationForm" runat="server" Visible="false">
	<hr class="no-padding-top" />

	<asp:Literal ID="litBeginEnrollment" runat="server" />

	<p class="text-danger">* <asp:Label ID="lblRequired" runat="server" /></p>

	<div class="form-horizontal">
<asp:UpdatePanel ID="updPnlForm" runat="server">
	<ContentTemplate>
		<div class="panel panel-default">
			<div class="panel-heading">
				Para inscribirse en CareMore Health Plan, proporcione la siguiente información
			</div>
			<div class="panel-body">
				<asp:Panel id="pnlCoverageEffectiveDate" runat="server" CssClass="form-group">
					<label for="txtCoverageEffectiveDate" class="col-xs-12 col-md-3 control-label">
						Fecha de entrada en vigencia de la cobertura <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-2">
						<asp:DropDownList ID="ddlCoverageEffectiveDate" runat="server" CssClass="form-control" />
					</div>
					<div class="col-xs-12 col-md-offset-3 col-md-4">
						<asp:RequiredFieldValidator ID="reqValCoverageEffectiveDate" runat="server" ControlToValidate="ddlCoverageEffectiveDate"
							ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</asp:Panel>
				<div class="form-group">
					<label for="txtLastName" class="col-xs-12 col-md-3 control-label">
						Apellido <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" MaxLength="50" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValLastName" runat="server" ControlToValidate="txtLastName"
							ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtFirstName" class="col-xs-12 col-md-3 control-label">
						Nombre <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" MaxLength="50" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValFirstName" runat="server" ControlToValidate="txtFirstName"
							ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtMiddleInitial" class="col-xs-12 col-md-3 control-label">
						Inicial
					</label>
					<div class="col-xs-2 col-md-1">
						<asp:TextBox ID="txtMiddleInitial" runat="server" CssClass="form-control" MaxLength="1" />
					</div>
				</div>
				<div class="form-group">
					<label for="rblSalutation" class="col-xs-12 col-md-3 control-label">
						Salutation
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblSalutation" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
					<asp:ListItem Value="Mr">Sr.</asp:ListItem>
					<asp:ListItem Value="Mrs">Sra.</asp:ListItem>
					<asp:ListItem Value="Ms">Srta.</asp:ListItem>
						</asp:RadioButtonList>
					</div>
				</div>
				<div class="form-group">
					<label for="txtBirthDate" class="col-xs-12 col-md-3 control-label">
						Fecha de nacimiento <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-2">
						<asp:TextBox ID="txtBirthDate" runat="server" CssClass="form-control" MaxLength="10" placeholder="mm/dd/yyyy" />
					</div>
					<div class="col-xs-12 col-md-offset-3 col-md-4">
						<asp:RequiredFieldValidator ID="reqValBirthDate" runat="server" ControlToValidate="txtBirthDate"
							ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
						<asp:CompareValidator ID="cmpValBirthDate" runat="server" ControlToValidate="txtBirthDate"
							ErrorMessage="Fecha incorrecta o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
							Operator="DataTypeCheck" Type="Date" />
					</div>
				</div>
				<div class="form-group">
					<label for="rblSex" class="col-xs-12 col-md-3 control-label">
						Sexo <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:RadioButtonList ID="rblSex" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem>M</asp:ListItem>
							<asp:ListItem>F</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValSex" runat="server" ControlToValidate="rblSex"
							ErrorMessage="Se requiere" SetFocusOnError="true"  ValidationGroup="ApplicationPage"/>
					</div>
				</div>
				<div class="form-group">
					<label for="txtHomePhone" class="col-xs-12 col-md-3 control-label">
						Numero de teléfono <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtHomePhone" runat="server" CssClass="form-control" MaxLength="15" placeholder="###-###-####" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValHomePhone" runat="server" ControlToValidate="txtHomePhone"
							ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
						<asp:RegularExpressionValidator ID="regExValHomePhone" runat="server" ControlToValidate="txtHomePhone"
							ErrorMessage="Teléfono incorrecto o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
							ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$" />
					</div>
				</div>
				<asp:Panel id="pnlAlternativePhone" runat="server" class="form-group">
					<label for="txtAltPhone" class="col-xs-12 col-md-3 control-label">
						Número de teléfono alternativo
					</label>
					<div class="col-xs-12 col-md-5">
						<div class="row">
							<div class="col-xs-12">
								<asp:TextBox ID="txtAltPhone" runat="server" CssClass="form-control" MaxLength="15" placeholder="###-###-####" />
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RegularExpressionValidator ID="regExValAltPhone" runat="server" ControlToValidate="txtAltPhone"
							ErrorMessage="Teléfono incorrecto o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
							ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$" />
					</div>
				</asp:Panel>
				<asp:Panel ID="pnlIsCellPhone" runat="server" CssClass="form-group">
					<div class="col-xs-12 col-md-offset-3 col-md-6">
						<div class="row">
							<div class="col-xs-6 col-md-4">
								¿Es un número de teléfono móvil?
							</div>
							<div class="col-xs-6 col-md-8">
								<asp:RadioButtonList ID="rblIsCellPhone" runat="server" CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
						</div>
					</div>
				</asp:Panel>
				<div class="form-group">
					<label for="txtAddress" class="col-xs-12 col-md-3 control-label">
						Direccion de residencia permanente <br class="hidden-xs" />(no se permite el uso de un apartado postal) <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" MaxLength="80" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValAddress" runat="server" ControlToValidate="txtAddress"
							ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtCity" class="col-xs-12 col-md-3 control-label">
						Ciudad <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtCity" runat="server" CssClass="form-control" MaxLength="50" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValCity" runat="server" ControlToValidate="txtCity"
							ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="form-group">
					<label for="ddlState" class="col-xs-12 col-md-3 control-label">
						Estado <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:DropDownList ID="ddlState" runat="server" CssClass="form-control" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValState" runat="server" ControlToValidate="ddlState"
							ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtZip" class="col-xs-12 col-md-3 control-label">
						Código postal <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtZip" CssClass="form-control" runat="server" MaxLength="10" placeholder="#####" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValZip" runat="server" ControlToValidate="txtZip"
							ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
						<asp:RegularExpressionValidator ID="regExValZip" runat="server" ControlToValidate="txtZip"
							ErrorMessage="Código postal no válido o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
							ValidationExpression="^[0-9]{5}$" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtAddress2" class="col-xs-12 col-md-3 control-label">
						Domicilio postal <br class="hidden-xs" />(si es diferente al de arriba)
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control" MaxLength="80" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtCity2" class="col-xs-12 col-md-3 control-label">
						Ciudad
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtCity2" runat="server" CssClass="form-control" MaxLength="50" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtState2" class="col-xs-12 col-md-3 control-label">
						Estado
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtState2" runat="server" CssClass="form-control" MaxLength="50" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtZip2" class="col-xs-12 col-md-3 control-label">
						Código postal
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtZip2" runat="server" CssClass="form-control" MaxLength="10" placeholder="#####" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RegularExpressionValidator ID="regExValZip2" runat="server" ControlToValidate="txtZip2"
							ErrorMessage="Código postal no válido o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
							ValidationExpression="^[0-9]{5}$" />
					</div>
				</div>
				<asp:Panel id="pnlMoved" runat="server" CssClass="form-group">
					<label for="rblMoved" class="col-xs-12 col-md-3 control-label">
						¿Se ha mudado durante este año calendario? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<div class="form-group">
							<div class="col-xs-12">
								<asp:RadioButtonList ID="rblMoved" runat="server" AutoPostBack="true" CssClass="radio-inline"
									CausesValidation="false" RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblMoved_SelectedIndexChanged">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<asp:Label id="lblDateOfMove" runat="server" Text="Fecha de la mudanza" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:TextBox ID="txtDateOfMove" runat="server" CssClass="form-control" MaxLength="10" Enabled="false" placeholder="mm/dd/yyyy" />
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValDateOfMove" runat="server" ControlToValidate="txtDateOfMove"
									ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
								<asp:CompareValidator ID="cmpValDateOfMove" runat="server" ControlToValidate="txtDateOfMove"
									ErrorMessage="Fecha incorrecta o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
									Enabled="false" />
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValMoved" ControlToValidate="rblMoved" runat="server"
							ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</asp:Panel>
				<div class="form-group">
					<label for="txtEmergencyContact" class="col-xs-12 col-md-3 control-label">
						Contacto de Emergencia <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtEmergencyContact" runat="server" CssClass="form-control" MaxLength="80" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValEmergencyContact" runat="server" ControlToValidate="txtEmergencyContact"
							ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtEmergencyPhone" class="col-xs-12 col-md-3 control-label">
						Teléfono de Emergencia <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtEmergencyPhone" runat="server" CssClass="form-control" MaxLength="15" placeholder="###-###-####" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValEmergencyPhone" runat="server" ControlToValidate="txtEmergencyPhone"
							ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
						<asp:RegularExpressionValidator ID="regExValEmergencyPhone" runat="server" ControlToValidate="txtEmergencyPhone"
							ErrorMessage="Teléfono incorrecto o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
							ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtRelation" class="col-xs-12 col-md-3 control-label">
						Relación con usted <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtRelation" runat="server" CssClass="form-control" MaxLength="80" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValRelation" runat="server" ControlToValidate="txtRelation"
							ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<asp:Panel ID="pnlEmail" runat="server" CssClass="form-group">
					<label for="txtEmailAddress" class="col-xs-12 col-md-3 control-label">
						Dirección de correo electrónico <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-5">
						<asp:TextBox ID="txtEmailAddress" runat="server" CssClass="form-control" MaxLength="100" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValEmailAddress" runat="server" ControlToValidate="txtEmailAddress"
							ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
						<asp:RegularExpressionValidator ID="regExValEmailAddress" runat="server" ControlToValidate="txtEmailAddress"
							ErrorMessage="Correo electrónico no válido o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
							ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
					</div>
				</asp:Panel>
			</div>
		</div>

<asp:PlaceHolder ID="phConsentToContact" runat="server">
		<div class="panel panel-default">
			<div class="panel-heading">
				Acepto recibir notificaciones por correo electrónico
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						<asp:Literal ID="litConsentToContactText" runat="server"></asp:Literal>
					</div>
				</div>
				<div class="form-group">
					<label for="cbReceiveDocsByEmail" class="col-xs-12 col-md-3 control-label">
						Marque una opción o ambas
					</label>
					<div class="col-xs-12 col-md-8 top-spacer">
						<div class="row">
							<div class="col-xs-12">
								<asp:CheckBox ID="cbReceiveDocsByEmail" runat="server" />
								Acepto recibir mi Aviso anual de cambios, Evidencia de cobertura y mi Lista de medicamentos cubiertos (formulario) por correo electrónico.
							</div>
						</div>
						<div class="row top-spacer">
							<div class="col-xs-12">
								<asp:CheckBox ID="cbReceiveInfoByEmail" runat="server" />
								Deseo recibir información relacionada con los beneficios del plan, programas de salud, consejos, boletines, encuestas, información general y otros servicios del plan por correo electrónico.
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="txtReceiveEmailAddress" class="col-xs-12 col-md-3 control-label">
						Dirección de correo electrónico
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtReceiveEmailAddress" runat="server" CssClass="form-control" MaxLength="100" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RegularExpressionValidator ID="regExValReceiveEmailAddress" runat="server" ControlToValidate="txtReceiveEmailAddress"
							ErrorMessage="Correo electrónico no válido o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
							ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
					</div>
				</div>
				<div class="row no-top-margin">
					<div class="col-xs-12">
						<hr />
						Si decide no participar en el programa de envíos por correo electrónico, CareMore le enviará la información por correo postal.
					</div>
				</div>
			</div>
		</div>
</asp:PlaceHolder>

		<div class="panel panel-default">
			<div class="panel-heading">
				Proporcione la información del seguro de Medicare a continuación
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						<asp:Literal ID="litMedicareText" runat="server" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtMedicareName" class="col-xs-12 col-md-3 control-label">
						Nombre <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtMedicareName" runat="server" CssClass="form-control" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValMedicareName" runat="server" ControlToValidate="txtMedicareName"
							ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtMedicareClaimNumber" class="col-xs-12 col-md-3 control-label">
						Número de la demanda de Medicare <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtMedicareClaimNumber" runat="server" CssClass="form-control" MaxLength="14" placeholder="###-##-####-a"/>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValMedicareClaimNumber" runat="server" ControlToValidate="txtMedicareClaimNumber"
							ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="form-group">
					<label for="rblMedicareSex" class="col-xs-12 col-md-3 control-label">
						Sexo
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblMedicareSex" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem>M</asp:ListItem>
							<asp:ListItem>F</asp:ListItem>
						</asp:RadioButtonList>
					</div>
				</div>
				<div class="form-group">
					<label for="txtHospitalBenefitDate" class="col-xs-12 col-md-3 control-label">
						Tiene derecho a - COB. HOSPITALARIA (Parte A) <br class="hidden-xs" />Fecha de entrada en vigencia
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtHospitalBenefitDate" runat="server" CssClass="form-control" MaxLength="10" placeholder="mm/dd/yyyy" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:CompareValidator ID="cmpValHospitalBenefitDate" runat="server" ControlToValidate="txtHospitalBenefitDate"
							ErrorMessage="Fecha incorrecta o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
							Operator="DataTypeCheck" Type="Date" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtMedicalBenefitDate" class="col-xs-12 col-md-3 control-label">
						Tiene derecho a - COB. MÉDICA (Parte B) <br class="hidden-xs" />Fecha de entrada en vigencia
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtMedicalBenefitDate" runat="server" CssClass="form-control"  MaxLength="10" placeholder="mm/dd/yyyy" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:CompareValidator ID="cmpValMedicalBenefitDate" runat="server" ControlToValidate="txtMedicalBenefitDate"
							ErrorMessage="Fecha incorrecta o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
							Operator="DataTypeCheck" Type="Date" />
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				Cómo pagar la prima del plan
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						<asp:Label ID="lblPaymentPlan" runat="server" />
					</div>
				</div>
				<asp:PlaceHolder id="phPaymentPlans" runat="server">
					<div class="form-group top-spacer">
						<label for="rblPaymentOption" class="col-xs-12 col-md-3 control-label">
							Por favor seleccione una <br class="hidden-xs" />opción para pagar su prima (en su caso)
						</label>
						<div class="col-xs-12 col-md-6">
							<asp:RadioButtonList ID="rblPaymentOption" runat="server" AutoPostBack="True" CausesValidation="false"
								CssClass="radio-inline" RepeatDirection="Vertical" RepeatLayout="Flow"
								OnSelectedIndexChanged="RblPaymentOption_SelectedIndexChanged">
								<asp:ListItem Value="Receive a bill">Recibe una factura</asp:ListItem>
								<asp:ListItem Value="Electronic funds transfer(EFT)">Transferencia electrónica de fondos (EFT, por sus siglas en ingles) de su cuenta bancaria cada mes</asp:ListItem>
								<asp:ListItem Value="Credit Card">Tarjeta de crédito</asp:ListItem>
								<asp:ListItem Value="Automatic deduction">Deducción automática de su Seguro Social o Junta de Retiro Ferroviario beneficio (RRB) cheque. (El Seguro Social / RRB deducción puede tomar dos o más meses en comenzar después de la Seguridad Social o RRB aprueba la deducción. En la mayoría de los casos, si el Seguro Social o RRB acepta su solicitud de deducción automática, la primera deducción de su beneficio de Seguro Social o RRB comprobar incluirá todas las primas adeudadas desde la fecha efectiva de inscripción hasta el descuento comience. Si el Seguro Social o RRB no aprueba su solicitud de deducción automática, le enviaremos una factura por sus primas mensuales.)</asp:ListItem>
							</asp:RadioButtonList>
						</div>
					</div>
					<asp:Panel ID="pnlPaymentPlanDetails" runat="server" CssClass="row" Visible="false">
						<div class="col-xs-12 col-md-offset-3 col-md-8">
							<asp:PlaceHolder ID="phBankAccount" runat="server" Visible="false">
								<p class="text-info">Provea lo siguiente:</p>

								<div class="form-group">
									<label for="txtAccountHolderName" class="col-xs-12 col-md-3 control-label">
										Nombre de titular de la cuenta
									</label>
									<div class="col-xs-12 col-md-4">
										<asp:TextBox ID="txtAccountHolderName" runat="server" MaxLength="50" CssClass="form-control" Enabled="false" />
									</div>
									<div class="col-xs-12 col-md-4">
										<asp:RequiredFieldValidator ID="reqValAccountHolderName" runat="server" ControlToValidate="txtAccountHolderName"
											ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
											Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<label for="rblAccountType" class="col-xs-12 col-md-3 control-label">
										Tipo de cuenta
									</label>
									<div class="col-xs-12 col-md-4">
										<asp:RadioButtonList ID="rblAccountType" runat="server" CssClass="radio-inline"
											RepeatDirection="Horizontal" RepeatLayout="Flow" Enabled="false">
											<asp:ListItem>Checking</asp:ListItem>
											<asp:ListItem>Saving</asp:ListItem>
										</asp:RadioButtonList>
									</div>
									<div class="col-xs-12 col-md-4">
										<asp:RequiredFieldValidator ID="reqValAccountType" runat="server" ControlToValidate="rblAccountType"
											ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
											Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<label for="txtRoutingNumber" class="col-xs-12 col-md-3 control-label">
										Número de ruta ABA
									</label>
									<div class="col-xs-12 col-md-4">
										<asp:TextBox ID="txtRoutingNumber" runat="server" MaxLength="25" CssClass="form-control" Enabled="false" />
									</div>
									<div class="col-xs-12 col-md-4">
										<asp:RequiredFieldValidator ID="reqValRoutingNumber" runat="server" ControlToValidate="txtRoutingNumber"
											ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
											Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<label for="txtAccountNumber" class="col-xs-12 col-md-3 control-label">
										Numero de cuenta bancaria
									</label>
									<div class="col-xs-12 col-md-4">
										<asp:TextBox ID="txtAccountNumber" runat="server" MaxLength="25" CssClass="form-control" Enabled="false"/>
									</div>
									<div class="col-xs-12 col-md-4">
										<asp:RequiredFieldValidator ID="reqValAccountNumber" runat="server" ControlToValidate="txtAccountNumber"
											ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
											Enabled="false" />
									</div>
								</div>
							</asp:PlaceHolder>

							<asp:PlaceHolder ID="phCreditCardInfo" runat="server" Visible="false">
								<p class="text-info">Por favor provea la siguiente información:</p>

								<div class="form-group">
									<label for="ddlCreditCardType" class="col-xs-12 col-md-3 control-label">Tipo de tarjeta</label>
									<div class="col-xs-12 col-md-4">
										<asp:DropDownList ID="ddlCreditCardType" runat="server" CssClass="form-control">
											<asp:ListItem>Visa</asp:ListItem>
											<asp:ListItem>Mastercard</asp:ListItem>
											<asp:ListItem>Discover</asp:ListItem>
											<asp:ListItem>American Express</asp:ListItem>
										</asp:DropDownList>
									</div>
								</div>
								<div class="form-group">
									<label for="txtCreditCardAccountHolderName" class="col-xs-12 col-md-3 control-label">Nombre del dueño de la cuenta como aparece en la tarjeta</label>
									<div class="col-xs-12 col-md-4">
										<asp:TextBox ID="txtCreditCardAccountHolderName" runat="server" MaxLength="50" CssClass="form-control" />
									</div>
									<div class="col-xs-12 col-md-4">
										<asp:RequiredFieldValidator ID="reqValCCAccountName" runat="server" ControlToValidate="txtCreditCardAccountHolderName"
											ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
											Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<label for="txtCreditCardAccountNumber" class="col-xs-12 col-md-3 control-label">
										Número de la cuenta
									</label>
									<div class="col-xs-12 col-md-4">
										<asp:TextBox ID="txtCreditCardAccountNumber" runat="server" MaxLength="25" CssClass="form-control" />
									</div>
									<div class="col-xs-12 col-md-4">
										<asp:RequiredFieldValidator ID="reqValCCAccountNumber" runat="server" ControlToValidate="txtCreditCardAccountNumber"
											ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
											Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<label for="ddlCCExpirationMonth" class="col-xs-12 col-md-3 control-label">Fecha de expiración (MM/YYYY)</label>
									<div class="col-xs-12 col-md-2">
										<asp:DropDownList ID="ddlCCExpirationMonth" runat="server" CssClass="form-control">
											<asp:ListItem>01</asp:ListItem>
											<asp:ListItem>02</asp:ListItem>
											<asp:ListItem>03</asp:ListItem>
											<asp:ListItem>04</asp:ListItem>
											<asp:ListItem>05</asp:ListItem>
											<asp:ListItem>06</asp:ListItem>
											<asp:ListItem>07</asp:ListItem>
											<asp:ListItem>08</asp:ListItem>
											<asp:ListItem>09</asp:ListItem>
											<asp:ListItem>10</asp:ListItem>
											<asp:ListItem>11</asp:ListItem>
											<asp:ListItem>12</asp:ListItem>
										</asp:DropDownList>
									</div>
									<div class="col-xs-12 col-md-2">
										<asp:DropDownList ID="ddlCCExpirationYear" runat="server" CssClass="form-control"></asp:DropDownList>
									</div>
								</div>
							</asp:PlaceHolder>
						</div>
					</asp:Panel>
				</asp:PlaceHolder>
			</div>
		</div>

<asp:PlaceHolder ID="phChronicSNP" runat="server">
		<div class="panel panel-default">
			<div class="panel-heading">
				EVALUACIÓN DE ELEGIBILIDAD ANTES DE LA INSCRIPCIÓN A UN PLAN DE NECESIDADES ESPECIALES PARA ENFERMEDADES CRÓNICAS
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						Complete si tiene una afección crónica. Si la respuesta es “Sí” a, por lo menos, una de las preguntas, usted reúne los requisitos previos correspondientes a la afección o las afecciones.
					</div>
				</div>
				<div class="row">
					<label for="rblSNP_ESRD" class="col-xs-12 col-md-3">
						<div class="row">
							<div class="col-xs-12 control-label">
								¿Se realiza diálisis? <span class="text-danger">*</span><br />
								(enfermedad renal terminal, ESRD)
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 control-text">
								Si recibió un trasplante de riñón con éxito o ya no necesita diálisis de forma regular, adjunte una nota o los registros
								del médico que indiquen que usted recibió un trasplante exitoso de riñón o que ya no necesita diálisis; de lo contrario,
								tal vez sea necesario que nos contactemos con usted para obtener información adicional.
							</div>
						</div>
					</label>
					<div class="col-xs-12 col-md-8">
						<div class="form-group">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblSNP_ESRD" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValSNP_ESRD" runat="server" ControlToValidate="rblSNP_ESRD"
									ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
							</div>
						</div>
						<div class="form-group top-spacer">
							<div class="col-xs-12 col-md-3">
								Centro de diálisis al que acude actualmente
							</div>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtCurrentDialysisCenter" runat="server" CssClass="form-control" MaxLength="100" /></div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-3">
								Teléfono (no se admiten números 800)
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:TextBox ID="txtCurrentDialysisPhone" runat="server" CssClass="form-control" MaxLength="15" placeholder="###-###-####" />
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RegularExpressionValidator ID="regExValCurrentDialysisPhone" runat="server" ControlToValidate="txtCurrentDialysisPhone"
									ErrorMessage="Teléfono incorrecto o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
									ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-3">
								Nombre del nuevo nefrólogo
							</div>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtNephrologistFirstName" runat="server" CssClass="form-control" MaxLength="50" /></div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-3">
								Apellido del nuevo nefrólogo
							</div>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtNephrologistLastName" runat="server" CssClass="form-control" MaxLength="50" /></div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-3">
								Nuevo centro de diálisis
							</div>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtNewDialysisCenter" runat="server" CssClass="form-control" MaxLength="100" /></div>
						</div>
					</div>
				</div>
				<div class="row">
					<label class="col-xs-12 col-md-3 control-label">
						¿Se le ha diagnosticado alguna de las siguientes afecciones? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-8">
						<div class="form-group">
							<label class="col-xs-12 col-md-2 control-text">
								Asma
							</label>
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblSNP_Asthma" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValSNP_Asthma" runat="server" ControlToValidate="rblSNP_Asthma"
									ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-12 col-md-2 control-text">
								Enfisema
							</label>
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblSNP_Emphysema" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValSNP_Emphysema" runat="server" ControlToValidate="rblSNP_Emphysema"
									ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3 control-label">
						¿Le dijo algún médico que tiene diabetes (niveles elevados de azúcar en la sangre o la orina)? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblSNP_Diabetes" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="True">Si</asp:ListItem>
							<asp:ListItem Value="False">No</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValSNP_Diabetes" runat="server" ControlToValidate="RblSNP_Diabetes"
							ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3 control-label">
						¿Alguna vez le recetaron o está utilizando insulina o algún medicamento por vía oral que se supone que disminuye el nivel de azúcar en la sangre? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblSNP_Insulin" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="True">Si</asp:ListItem>
							<asp:ListItem Value="False">No</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValSNP_Insulin" runat="server" ControlToValidate="rblSNP_Insulin"
							ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3 control-label">
						¿Alguna vez le dijo un médico que tiene enfermedad de las arterias coronarias, mala circulación debido al endurecimiento de las arterias o venas "malas"? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblSNP_CoronaryArteryDisease" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="True">Si</asp:ListItem>
							<asp:ListItem Value="False">No</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValSNP_CoronaryArteryDisease" runat="server" ControlToValidate="rblSNP_CoronaryArteryDisease"
							ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3 control-label">
						¿Alguna vez tuvo un ataque al corazón o ingresó en el hospital debido a angina de pecho (dolor en el pecho)? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblSNP_HeartAttack" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="True">Si</asp:ListItem>
							<asp:ListItem Value="False">No</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValSNP_HeartAttack" runat="server" ControlToValidate="rblSNP_HeartAttack"
							ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3 control-label">
						¿Alguna vez le dijo un médico que tiene insuficiencia cardíaca (corazón débil)? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblSNP_HeartFailure" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="True">Si</asp:ListItem>
							<asp:ListItem Value="False">No</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValSNP_HeartFailure" runat="server" ControlToValidate="rblSNP_HeartFailure"
							ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="row">
					<label class="col-xs-12 col-md-3 control-label">
						¿Alguna vez tuvo líquido en los pulmones e inflamación en las piernas, acompañados por falta de aliento, debido a un problema cardíaco? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblSNP_FluidSwelling" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="True">Si</asp:ListItem>
							<asp:ListItem Value="False">No</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValSNP_FluidSwelling" runat="server" ControlToValidate="rblSNP_FluidSwelling"
							ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="row">
					<label class="col-xs-12 col-md-3 control-label">
						<div class="row">
							<div class="col-xs-12 control-label">
								Especialista que trata la enfermedad que reúne los requisitos en virtud del plan de necesidades especiales (SNP)
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 control-text">
								Si usted está viendo a un especialista para una condición crónica, por favor proporcione su información para que podamos contactar con ellos en su nombre.
							</div>
						</div>
					</label>
					<div class="col-xs-12 col-md-8">
						<div class="form-group">
							<label class="col-xs-12 col-md-2 control-text">
								Nombre
							</label>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtTreatingFirstName" runat="server" CssClass="form-control" MaxLength="50" /></div>
						</div>
						<div class="form-group">
							<label class="col-xs-12 col-md-2 control-text">
								Apellido
							</label>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtTreatingLastName" runat="server" CssClass="form-control" MaxLength="50" /></div>
						</div>
						<div class="form-group">
							<label class="col-xs-12 col-md-2 control-text">
								Especialidad
							</label>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtTreatingSpecialty" runat="server" CssClass="form-control" MaxLength="50" /></div>
						</div>
						<div class="form-group">
							<label class="col-xs-12 col-md-2 control-text">
								Teléfono
							</label>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtTreatingPhone" runat="server" CssClass="form-control" MaxLength="15" placeholder="###-###-####" /></div>
							<div class="col-xs-12 col-md-4">
								<asp:RegularExpressionValidator ID="regExptTreatingPhone" runat="server" ControlToValidate="txtTreatingPhone"
									ErrorMessage="Teléfono incorrecto o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
									ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				Reconocimiento de las necesidades especiales para enfermedades crónicas
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						<asp:Literal ID="litChronicSpecialNeedsAcknowledgment" runat="server" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3">
						Firma de la persona inscrita <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:TextBox ID="txtEnrolleeInitials" runat="server" CssClass="form-control" MaxLength="3" />
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValEnrolleeInitials" runat="server" ControlToValidate="txtEnrolleeInitials"
							ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3">
						Fecha <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:Label ID="lblEnrolleeDate" runat="server" CssClass="form-control readonly" />
					</div>
				</div>
			</div>
		</div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phMedicationsAcknowledgment" runat="server">
		<div class="panel panel-default">
			<div class="panel-heading">
				Reconocimiento de medicamentos <span class="text-danger">*</span>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12 col-md-8">
						<asp:CheckBoxList ID="cblMedications" runat="server" CssClass="checkbox-inline">
							<asp:ListItem>
								Reconozco que soy responsable de contactarme con mis proveedores actuales para resurtir mis medicamentos dentro de los próximos 30 días anteriores a la fecha de entrada en vigencia de la cobertura de CareMore Health Plan.
							</asp:ListItem>
						</asp:CheckBoxList>
					</div>
					<div class="col-xs-12 col-md-4">
						<cm:RequiredFieldValidatorForCheckBoxLists ID="reqValMedications" runat="server" ControlToValidate="cblMedications"
							ErrorMessage="Por favor, mencione" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
			</div>
		</div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phContinuityCareCoordination" runat="server">
		<div class="panel panel-default">
			<div class="panel-heading">
				Coordinación de la continuidad de la atención
			</div>
			<div class="panel-body">
				<div class="form-group">
					<label class="col-xs-12 col-md-3">
						<span class="underline">Procedimientos o cirugías programados</span><br />
						¿Tiene procedimientos o cirugías programados dentro de los 30 días de entrada en vigencia de la cobertura? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-8">
						<div class="form-group">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblPlannedProcedures" runat="server" AutoPostBack="true"
									CausesValidation="false" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblPlannedProcedures_SelectedIndexChanged">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValPlannedProcedures" runat="server" ControlToValidate="rblPlannedProcedures"
									ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
							</div>
						</div>
						<div class="row">
							<label class="col-xs-12">
								Detalles del tratamiento
							</label>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-8">
								<asp:TextBox ID="txtTreatmentDetails" runat="server" CssClass="form-control" TextMode="MultiLine" Columns="50" Rows="4" MaxLength="500" Enabled="false" />
							</div>
						</div>
						<div class="row">
							<label class="col-xs-12">
								Especialistas que realizarán los procedimientos o las cirugías
							</label>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-8">
								<div class="form-group">
									<div class="col-xs-12 col-md-2">
										Nombre
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialistFirstName1" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" /></div>
									<div class="col-xs-12 col-md-2">
										Apellido
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialistLastName1" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" /></div>
								</div>
								<div class="form-group">
									<div class="col-xs-12 col-md-2">
										Teléfono
									</div>
									<div class="col-xs-12 col-md-4">
										<asp:TextBox ID="txtPlannedSpecialistPhone1" runat="server" CssClass="form-control" MaxLength="15" Enabled="false" placeholder="###-###-####" /><br />
										<asp:RegularExpressionValidator ID="regExpPlannedSpecialistPhone1" runat="server" ControlToValidate="txtPlannedSpecialistPhone1"
											ErrorMessage="Teléfono incorrecto o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
											ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$" />
									</div>
									<div class="col-xs-12 col-md-2">
										Especialidad
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialtyType1" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" /></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-8">
								<div class="form-group">
									<div class="col-xs-12 col-md-2">
										Nombre
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialistFirstName2" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" /></div>
									<div class="col-xs-12 col-md-2">
										Apellido
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialistLastName2" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" /></div>
								</div>
								<div class="form-group">
									<div class="col-xs-12 col-md-2">
										Phone
									</div>
									<div class="col-xs-12 col-md-4">
										<asp:TextBox ID="txtPlannedSpecialistPhone2" runat="server" CssClass="form-control" MaxLength="15" Enabled="false" placeholder="###-###-####" /><br />
										<asp:RegularExpressionValidator ID="regExpPlannedSpecialistPhone2" runat="server" ControlToValidate="txtPlannedSpecialistPhone2"
											ErrorMessage="Teléfono incorrecto o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
											ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$" />
									</div>
									<div class="col-xs-12 col-md-2">
										Especialidad
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialtyType2" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" /></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-8">
								<div class="form-group">
									<div class="col-xs-12 col-md-2">
										Nombre
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialistFirstName3" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" /></div>
									<div class="col-xs-12 col-md-2">
										Apellido
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialistLastName3" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" /></div>
								</div>
								<div class="form-group">
									<div class="col-xs-12 col-md-2">
										Phone
									</div>
									<div class="col-xs-12 col-md-4">
										<asp:TextBox ID="txtPlannedSpecialistPhone3" runat="server" CssClass="form-control" MaxLength="15" Enabled="false" placeholder="###-###-####" /><br />
										<asp:RegularExpressionValidator ID="regExpPlannedSpecialistPhone3" runat="server" ControlToValidate="txtPlannedSpecialistPhone3"
											ErrorMessage="Teléfono incorrecto o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
											ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$" />
									</div>
									<div class="col-xs-12 col-md-2">
										Especialidad
									</div>
									<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtPlannedSpecialtyType3" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" /></div>
								</div>
							</div>
						</div>
						<div class="row">
							<label class="col-xs-12">
								¿El especialista forma parte de la Administración de veteranos (VA)?
							</label>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<asp:RadioButtonList ID="rblSpecialistVA" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow" Enabled="false">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3">
						¿Utiliza oxígeno a diario para que le ayude a respirar? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblUsingOxygen" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="True">Si</asp:ListItem>
							<asp:ListItem Value="False">No</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValUsingOxygen" runat="server" ControlToValidate="rblUsingOxygen"
							ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3">
						¿Le han diagnosticado Enfermedad pulmonar obstructiva crónica (EPOC)? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-4">
						<asp:RadioButtonList ID="rblDiagnosedCopd" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem Value="True">Si</asp:ListItem>
							<asp:ListItem Value="False">No</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<div class="col-xs-12 col-md-4">
						<asp:RequiredFieldValidator ID="reqValDiagnosedCopd" runat="server" ControlToValidate="rblDiagnosedCopd"
							ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3">
						<span class="underline">Servicios de salud a domicilio</span><br />
						¿Recibe actualmente servicios de una agencia de atención de la salud a domicilio? <span class="text-danger">*</span><br />
						<span class="control-text">(las empleadas domésticas/los cuidadores no constituyen servicios de salud a domicilio)</span>
					</label>
					<div class="col-xs-12 col-md-8">
						<div class="form-group">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblHomeHealthServices" runat="server" AutoPostBack="true"
									CausesValidation="false" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblHomeHealthServices_SelectedIndexChanged">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValHomeHealthServices" runat="server" ControlToValidate="rblHomeHealthServices"
									ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-3">
								Proveedor de atención de la salud a domicilio
							</div>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtHomeHealthProviderName" runat="server" CssClass="form-control" MaxLength="100" Enabled="false" /></div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-3">
								Teléfono
							</div>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtHomeHealthProviderPhone" runat="server" CssClass="form-control" MaxLength="15" Enabled="false" placeholder="###-###-####" /></div>
							<div class="col-xs-12 col-md-4">
								<asp:RegularExpressionValidator ID="regExpHomeHealthProviderPhone" runat="server" ControlToValidate="txtHomeHealthProviderPhone"
									ErrorMessage="Teléfono incorrecto o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
									ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$" />
							</div>
						</div>
						<div class="row">
							<label class="col-xs-12">
								Servicios proporcionados. Seleccione todos los que correspondan:
							</label>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-8">
								<asp:CheckBoxList ID="cblHomeHealthServices" runat="server" CssClass="checkbox-inline" RepeatDirection="Vertical" RepeatLayout="Flow" Enabled="false">
									<asp:ListItem Value="Skilled nursing">Enfermería especializada</asp:ListItem>
									<asp:ListItem Value="In-home physical therapy">Fisioterapia en el hogar</asp:ListItem>
									<asp:ListItem Value="In-home speech therapy / language therapy">Terapia del habla y del lenguaje en el hogar</asp:ListItem>
									<asp:ListItem Value="In-home occupational therapy">Terapia ocupacional en el hogar</asp:ListItem>
								</asp:CheckBoxList>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-8">
								Fechas de las próximas visitas de atención médica a domicilio
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-6">
								<asp:TextBox ID="txtUpcomingHomeHealthVisits" runat="server" CssClass="form-control" MaxLength="100" Enabled="false" />
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-12 col-md-3">
						<span class="underline">Únicamente artículos alquilados</span><br />
						En la actualidad, ¿usa alguno de los siguientes equipos médicos duraderos (DME)?
					</label>
					<div class="col-xs-12 col-md-4">
						<div>
							<asp:CheckBoxList ID="cblRentedItems" runat="server" CssClass="checkbox-inline" RepeatDirection="Vertical" RepeatLayout="Flow">
								<asp:ListItem Value="Wheelchair">Silla de ruedas</asp:ListItem>
								<asp:ListItem Value="Scooter">Scooter</asp:ListItem>
								<asp:ListItem Value="Oxygen">Oxígeno</asp:ListItem>
								<asp:ListItem Value="Oversized Wheelchair">Silla de ruedas de gran tamaño</asp:ListItem>
								<asp:ListItem Value="Hospital Bed">Cama de hospital</asp:ListItem>
								<asp:ListItem Value="Nebulizer">Nebulizador</asp:ListItem>
								<asp:ListItem Value="Electric Wheelchair">Silla de ruedas eléctrica</asp:ListItem>
								<asp:ListItem Value="Lift">Elevador</asp:ListItem>
							</asp:CheckBoxList>
						</div>
						<div>
							<asp:CheckBox ID="cbOtherDme" runat="server" CssClass="checkbox-inline" Text="Otro" />
						</div>
						<div>
							<asp:TextBox ID="txtOtherDme" runat="server" CssClass="form-control" MaxLength="200" />
						</div>
						<hr />
						<div class="form-group">
							<div class="col-xs-12 col-md-2">
								Proveedor de DME
							</div>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtDmeProviderName" runat="server" CssClass="form-control" MaxLength="50" /></div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-2">
								Teléfono
							</div>
							<div class="col-xs-12 col-md-4"><asp:TextBox ID="txtDmeProviderPhone" runat="server" CssClass="form-control" MaxLength="15" placeholder="###-###-####" /></div>
							<div class="col-xs-12 col-md-4">
								<asp:RegularExpressionValidator ID="regExpDmeProviderPhone" runat="server" ControlToValidate="txtDmeProviderPhone"
									ErrorMessage="Teléfono incorrecto o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
									ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</asp:PlaceHolder>

		<div class="panel panel-default">
			<div class="panel-heading">
				Lea y responda estas preguntas importantes
			</div>
			<div class="panel-body">
				<asp:Panel id="pnlESRD" runat="server" CssClass="form-group">
					<label for="rblESRD" class="col-xs-12 col-md-3 control-label">
						¿Tiene la Enfermedad Renal en su Fase Final (ESRD por sus siglas en ingles)? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblESRD" runat="server" CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValESRD" runat="server" ControlToValidate="rblESRD"
									ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
							</div>
						</div>
						<div class="row top-spacer">
							<div class="col-xs-12">
								<p>Si usted ha tenido un exitoso trasplante de riñón y / o que no necesita diálisis regular, por favor adjunte una nota o los registros
									de su médico que demuestren que han tenido un trasplante de riñón exitoso, o que ya no necesita diálisis, de lo contrario es posible
									que necesitemos en contacto con usted para obtener información adicional.</p>
							</div>
						</div>
					</div>
				</asp:Panel>

				<div class="form-group">
					<label for="rblOtherCoverage" class="col-xs-12 col-md-3 control-label">
						Algunos individuos podrían tener alguna otra cobertura de medicamentos, incluyendo seguro privado, TRICARE,
						cobertura de beneficios de salud federales, beneficios de VA, o con algún programa de ayuda farmacéutica del estado.
						¿Tendrá usted alguna otra cobertura de medicamentos recetados aparte de la cobertura
						con CareMore Health Plan? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<div class="col-xs-12 col-md-4 no-padding-top">
								<asp:RadioButtonList ID="rblOtherCoverage" runat="server" AutoPostBack="True"
									CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblOtherCoverage_SelectedIndexChanged">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValOtherCoverage" runat="server" ControlToValidate="rblOtherCoverage"
									ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<p>Si usted contesto "si" por favor indique la cobertura y su número de identificación para esta cobertura:</p>
							</div>
						</div>
						<div class="form-group">
							<label for="txtOtherCoverageName" class="col-xs-12 col-md-5 control-label">
								Nombre de su otra cobertura
							</label>
							<div class="col-xs-12 col-md-4">
								<asp:TextBox ID="txtOtherCoverageName" runat="server" MaxLength="50" CssClass="form-control" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-3">
								<asp:RequiredFieldValidator ID="reqValOtherCoverageName" runat="server" ControlToValidate="txtOtherCoverageName"
									ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
									Enabled="false" />
							</div>
						</div>
						<div class="form-group">
							<label for="txtOtherCoverageId" class="col-xs-12 col-md-5 control-label">
								Numero de identificación
							</label>
							<div class="col-xs-12 col-md-4">
								<asp:TextBox ID="txtOtherCoverageId" runat="server" MaxLength="50" CssClass="form-control" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-3">
								<asp:RequiredFieldValidator ID="reqValOtherCoverageId" runat="server" ControlToValidate="txtOtherCoverageId"
									ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
									Enabled="false" />
							</div>
						</div>
						<asp:Panel ID="pnlOtherCoverageGroup" runat="server" CssClass="form-group">
							<label for="txtOtherCoverageGroupNumber" class="col-xs-12 col-md-5 control-label">
								Numero de grupo
							</label>
							<div class="col-xs-12 col-md-4">
								<asp:TextBox ID="txtOtherCoverageGroupNumber" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-3">
								<asp:RequiredFieldValidator ID="reqValOtherCoverageGroupNumber" runat="server" ControlToValidate="txtOtherCoverageGroupNumber"
									ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
									Enabled="false" />
							</div>
						</asp:Panel>
						<asp:Panel id="pnlCoverageEndDate" runat="server" CssClass="form-group">
							<label for="txtOtherCoverageDate" class="col-xs-12 col-md-5 control-label">
								Fecha del fin de la cobertura
							</label>
							<div class="col-xs-12 col-md-4">
								<asp:TextBox ID="txtOtherCoverageDate" runat="server" CssClass="form-control" MaxLength="10" Enabled="false" placeholder="mm/dd/yyyy" />
							</div>
							<div class="col-xs-12 col-md-3">
								<asp:RequiredFieldValidator ID="reqValOtherCoverageDate" runat="server" ControlToValidate="txtOtherCoverageDate"
									ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
									Enabled="false" />
								<asp:CompareValidator ID="cmpValOtherCoverageDate" runat="server" ControlToValidate="txtOtherCoverageDate"
									ErrorMessage="Fecha incorrecta o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
									Operator="DataTypeCheck" Type="Date" Enabled="false" />
							</div>
						</asp:Panel>
					</div>
				</div>

				<div class="form-group">
					<label for="rblCareResident" class="col-xs-12 col-md-3 control-label">
						¿Reside en un centro de atención a largo plazo, como un hogar de convalecencia? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblCareResident" runat="server" AutoPostBack="True"
									CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblCareResident_SelectedIndexChanged">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValCareResident" runat="server" ControlToValidate="rblCareResident"
									ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
							</div>
						</div>
						<div class="row top-spacer">
							<div class="col-xs-12">
								<p>Si usted contesto "si", por favor proporcione la siguiente información:<br />
								Nombre y Dirección y Numero de Teléfono de la Institución (numero y calle)</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-9">
								<asp:TextBox ID="txtCareResident" runat="server" MaxLength="200" Rows="4" TextMode="MultiLine"
									CssClass="form-control" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-3">
								<asp:RequiredFieldValidator ID="reqValCareResidentInfo" runat="server" ControlToValidate="txtCareResident"
									ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
									Enabled="false" />
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="rblStateMedicaid" class="col-xs-12 col-md-3 control-label">
						¿Esta inscrito usted en su programa de Medicaid Estatal? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblStateMedicaid" runat="server" AutoPostBack="True"
									CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblStateMedicaid_SelectedIndexChanged">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValStateMedicaid" runat="server" ControlToValidate="rblStateMedicaid"
									ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<p>Si "sí", por favor proporcione su número del programa de Medicaid:</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-8">
								<asp:TextBox ID="txtStateMedicaidNumber" runat="server" MaxLength="50" CssClass="form-control" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValStateMedicaidNumber" runat="server" ControlToValidate="txtStateMedicaidNumber"
									ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
									Enabled="false" />
							</div>
						</div>
					</div>
				</div>

				<asp:Panel ID="pnlCurrentMember" runat="server" CssClass="form-group">
					<label for="rblCurrentMember" class="col-xs-12 col-md-3 control-label">
						¿Es actualmente miembro de CareMore? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblCurrentMember" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValCurrentMember" runat="server" ControlToValidate="rblCurrentMember"
									ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								Nombre de su plan de seguro médico actual
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-8">
								<asp:TextBox ID="txtCurrentMemberPlan" runat="server" CssClass="form-control" MaxLength="100" />
							</div>
						</div>
					</div>
				</asp:Panel>

				<div class="form-group">
					<label for="rblSpouseWork" class="col-xs-12 col-md-3 control-label">
						<asp:Literal id="litSpouseText" runat="server"></asp:Literal> <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblSpouseWork" runat="server" AutoPostBack="true"
									CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblSpouseWork_SelectedIndexChanged">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValSpouseWork" runat="server" ControlToValidate="rblSpouseWork"
									ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
							</div>
						</div>
				<asp:Placeholder ID="phSpouseGroupCoveragePlan" runat="server">
						<div class="row">
							<div class="col-xs-12">
								<p>Si la respuesta es “sí”, indique el plan de cobertura grupals</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-8">
								<asp:TextBox ID="txtSpouseGroupCoveragePlan" runat="server" CssClass="form-control" MaxLength="100" Enabled="false" />
							</div>
						</div>
				</asp:Placeholder>
					</div>
				</div>

				<asp:Panel id="pnlLiveLongTerm" runat="server" CssClass="form-group">
					<label for="rblLiveLongTerm" class="col-xs-12 col-md-3 control-label">
						¿Reside en un centro de atención a largo plazo, como un hogar de convalecencia? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblLiveLongTerm" runat="server" CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValRdbLiveLongTerm" runat="server" ControlToValidate="rblLiveLongTerm"
									ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
							</div>
						</div>
					</div>
				</asp:Panel>

				<asp:Panel id="pnlChronicLung" runat="server" CssClass="form-group">
					<label for="rblChronicLung" class="col-xs-12 col-md-3 control-label">
						¿Sufre usted de una Enfermedad de Pulmón Crónica? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblChronicLung" runat="server" CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValChronicLung" runat="server" ControlToValidate="rblChronicLung"
									ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
							</div>
						</div>
					</div>
				</asp:Panel>

				<asp:Panel id="pnlDiabetes" runat="server" CssClass="form-group">
					<label for="rblDiabetes" class="col-xs-12 col-md-3 control-label">
						¿Sufre usted de Diabetes? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblDiabetes" runat="server" CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValDiabetes" runat="server" ControlToValidate="rblDiabetes"
									ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
							</div>
						</div>
					</div>
				</asp:Panel>

				<asp:Panel id="pnlChronicHeart" runat="server" CssClass="form-group">
					<label for="rblChronicHeart" class="col-xs-12 col-md-3 control-label">
						¿Sufre usted de una Enfermedad Cardiovascular o Insuficiencia Crónica del Corazón? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblChronicHeart" runat="server" CssClass="radio-inline no-padding-top" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValChronicHeart" ControlToValidate="rblChronicHeart" runat="server"
									ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
							</div>
						</div>
					</div>
				</asp:Panel>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
				<label for="txtPrimaryPhysicianName" class="col-xs-12 col-md-3">
					Elija el nombre de un médico de atención primaria (PCP)
				</label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtPrimaryPhysicianName" runat="server" CssClass="form-control" MaxLength="100" />
				</div>
				<div class="col-xs-12 col-md-4">
					<asp:HyperLink ID="hlProviderSearch" runat="server" CssClass="btn btn-default" Target="_blank">Search for a Doctor</asp:HyperLink>
				</div>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<label for="rblPrintFormat" class="col-xs-12 col-md-3">
						Por favor marque una de las siguientes casillas si prefiere que le enviemos información
						en otro idioma diferente al Inglés, o en otro formato
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<div class="col-xs-12">
								<asp:RadioButtonList ID="rblPrintFormat" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
									<asp:ListItem Value="" Selected="True">None</asp:ListItem>
									<asp:ListItem>Español</asp:ListItem>
									<asp:ListItem>Braille</asp:ListItem>
									<asp:ListItem>Letra grande</asp:ListItem>
								</asp:RadioButtonList>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<p>Si necesita información en otro formato o idioma que los listados anteriormente, por favor comuníquese con CareMore
								Health Plan al <strong>(800) 499-2793</strong>. Estamos disponibles el domingo, lunes, martes, miércoles, jueves, viernes y sábado, de
								8:00 a.m. - 8:00 p.m. hora del Pacífico. Los usuarios de TTY deben llamar al (800) 577-5586.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				Por Favor Lea Esta Información Importante
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						<asp:Label ID="lblPleaseReadInfo" runat="server" />
					</div>
				</div>
				<div class="row">
					<label class="col-xs-12 col-md-3 control-label">
						Liberación de Información <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-8">
						<div class="row">
							<div class="col-xs-12">
								<asp:Label ID="lblReleaseofInfo" runat="server" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-2">
								Iniciales para afiliados
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:TextBox ID="txtReleaseInitials" runat="server" CssClass="form-control" MaxLength="3" />
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RequiredFieldValidator ID="reqValReleaseInitials" runat="server" ControlToValidate="txtReleaseInitials"
									ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-2">
								Fecha
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:Label ID="lblReleaseDate" runat="server" CssClass="form-control readonly" />
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="cblignature" class="col-xs-12 col-md-3 control-label">
						Firma del miembro <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<asp:CheckBoxList ID="cblSignature" runat="server" CssClass="radio" RepeatDirection="Horizontal" RepeatLayout="Flow">
							<asp:ListItem>Seleccione esta casilla será su firma electrónica</asp:ListItem>
						</asp:CheckBoxList>
					</div>
					<div class="col-xs-12 col-md-3">
						<cm:RequiredFieldValidatorForCheckBoxLists ID="reqValCblSignature" runat="server" ControlToValidate="cblSignature"
							ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
					</div>
				</div>
				<div class="form-group">
					<label for="rblAuthorizedRep" class="col-xs-12 col-md-3 control-label">
						¿Es usted un representante <br class="hidden-xs" />autorizado? <span class="text-danger">*</span>
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblAuthorizedRep" runat="server" AutoPostBack="True" CssClass="radio-inline"
									RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblAuthorizedRep_SelectedIndexChanged">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
							<div class="col-xs-12 col-md-8">
								<asp:RequiredFieldValidator ID="reqValAuthorizedRep" runat="server" ControlToValidate="rblAuthorizedRep"
									ErrorMessage="Se requiere respuesta" SetFocusOnError="true" ValidationGroup="ApplicationPage" />
							</div>
						</div>
						<div class="row top-spacer">
							<div class="col-xs-12">
								<p>Si "sí", debe firmar arriba y proporcionar la siguiente información:</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
									<label for="txtRepName" class="col-xs-12 col-md-3 control-label">Nombre</label>
									<div class="col-xs-12 col-md-5">
										<asp:TextBox ID="txtRepName" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
									</div>
									<div class="col-xs-12 col-md-3">
										<asp:RequiredFieldValidator ID="reqValRepName" runat="server" ControlToValidate="txtRepName"
											ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
											Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<label for="txtRepAddress" class="col-xs-12 col-md-3 control-label">Domicilio</label>
									<div class="col-xs-12 col-md-5">
										<asp:TextBox ID="txtRepAddress" runat="server" CssClass="form-control" MaxLength="100" Enabled="false" />
									</div>
									<div class="col-xs-12 col-md-3">
										<asp:RequiredFieldValidator ID="reqValRepAddress" runat="server" ControlToValidate="txtRepAddress"
											ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
											Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<label for="txtRepPhone" class="col-xs-12 col-md-3 control-label">Numero de Teléfono</label>
									<div class="col-xs-12 col-md-5">
										<asp:TextBox ID="txtRepPhone" runat="server" CssClass="form-control" MaxLength="15" Enabled="false" placeholder="###-###-####" />
									</div>
									<div class="col-xs-12 col-md-3">
										<asp:RequiredFieldValidator ID="reqValRepPhone" ControlToValidate="txtRepPhone" runat="server"
											ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
											Enabled="false" />
										<asp:RegularExpressionValidator ID="regExValRepPhone" runat="server" ControlToValidate="txtRepPhone"
											ErrorMessage="Teléfono incorrecto o formato incorrecto" SetFocusOnError="true" ValidationGroup="ApplicationPage"
											ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$"
											Enabled="false" />
									</div>
								</div>
								<div class="form-group">
									<label for="txtRepRelation" class="col-xs-12 col-md-3 control-label">La relación al Enrollee</label>
									<div class="col-xs-12 col-md-5">
										<asp:TextBox ID="txtRepRelation" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
									</div>
									<div class="col-xs-12 col-md-3">
										<asp:RequiredFieldValidator ID="reqValRepRelation" runat="server" ControlToValidate="txtRepRelation"
											ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
											Enabled="false" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<asp:Panel ID="pnlSalesAgent" runat="server" CssClass="row">
					<label class="col-xs-12 col-md-3 control-label">
						Si un agente de ventas completó este formulario, por favor envíe la siguiente información
					</label>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<div class="col-xs-12 col-md-8">
								<span class="control-text">Ayudé al solicitante a llenar esta solicitud</span>
							</div>
							<div class="col-xs-12 col-md-4">
								<asp:RadioButtonList ID="rblSalesAgent" runat="server" AutoPostBack="true"
									CausesValidation="false" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow"
									OnSelectedIndexChanged="RblSalesAgent_SelectedIndexChanged">
									<asp:ListItem Value="True">Si</asp:ListItem>
									<asp:ListItem Value="False">No</asp:ListItem>
								</asp:RadioButtonList>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-12 col-md-3 control-label">
								Código del agente
							</label>
							<div class="col-xs-12 col-md-5">
								<asp:TextBox ID="txtSalesAgentCode" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-3">
								<asp:RequiredFieldValidator ID="reqValSalesAgentCode" ControlToValidate="txtSalesAgentCode" runat="server"
									ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
									Enabled="false" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-12 col-md-3 control-label">
								Nombre del agente/corredor
							</label>
							<div class="col-xs-12 col-md-5">
								<asp:TextBox ID="txtSalesAgentFirstName" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-3">
								<asp:RequiredFieldValidator ID="reqValSalesAgentFirstName" ControlToValidate="TxtSalesAgentFirstName" runat="server"
									ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
									Enabled="false" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-12 col-md-3 control-label">
								Apellido del agente/corredor
							</label>
							<div class="col-xs-12 col-md-5">
								<asp:TextBox ID="txtSalesAgentLastName" runat="server" CssClass="form-control" MaxLength="50" Enabled="false" />
							</div>
							<div class="col-xs-12 col-md-3">
								<asp:RequiredFieldValidator ID="reqValSalesAgentLastName" ControlToValidate="TxtSalesAgentLastName" runat="server"
									ErrorMessage="Se requiere" SetFocusOnError="true" ValidationGroup="ApplicationPage"
									Enabled="false" />
							</div>
						</div>
					</div>
				</asp:Panel>
			</div>
		</div>
	</ContentTemplate>
</asp:UpdatePanel>

		<div class="form-group">
			<div class="col-xs-12 col-md-offset-3 col-md-6">
				<asp:LinkButton ID="lnkSubmit" runat="server" CssClass="btn btn-primary" OnClick="LnkBtnSubmit_Click" ValidationGroup="ApplicationPage">ENVIAR</asp:LinkButton>
			</div>
		</div>
	</div>

</asp:PlaceHolder>

<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End SubLayouts/SpanishApplicationPage -->
