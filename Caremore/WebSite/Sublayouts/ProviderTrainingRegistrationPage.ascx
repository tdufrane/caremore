﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ProviderTrainingRegistrationPage.ascx.cs" Inherits="Website.Sublayouts.ProviderTrainingRegistrationPage" %>
<%@ Register src="~/Controls/ReCaptcha.ascx" tagname="ReCaptcha" tagprefix="uc1" %>

<!-- Begin SubLayouts/ProviderTrainingRegistrationPage -->
<div class="row">
	<div class="col-xs-12">
		<asp:Panel id="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
			<script type="text/javascript">
				function OneChosen(source, args) {
					if (($("#<%= txtLicense.ClientID %>").val() == "") && ($("#<%= txtNpi.ClientID %>").val() == ""))
						args.IsValid = false;
					else
						args.IsValid = true;
				}
			</script>

			<div>
				<sc:Text ID="txtRegister" runat="server" Field="Register Content" />
				<sc:Text ID="txtAlready" runat="server" Field="Already Registered Content" Visible="false" />
				<sc:Text ID="txtMoreInfo" runat="server" Field="More Register Info Content" Visible="false" />
				<sc:Text ID="txtReattest" runat="server" Field="ReAttest Content" Visible="false" />
			</div>

			<p><asp:LinkButton ID="lnkBtnAlreadyRegistered" runat="server" CssClass="btn btn-danger" OnClick="BtnAlreadyRegistered_Click" CausesValidation="false" /></p>

		<asp:PlaceHolder ID="phRegister1" runat="server">
			<div class="form-group">
				<label for="txtFirstName" class="col-xs-12 col-md-3 control-label">First Name <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" MaxLength="25" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValFirstName" runat="server" ControlToValidate="txtFirstName"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtLastName" class="col-xs-12 col-md-3 control-label">Last Name <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" MaxLength="25" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValLastName" runat="server" ControlToValidate="txtLastName"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration" />
				</div>
			</div>
			<div class="form-group">
				<label for="ddlTitle" class="col-xs-12 col-md-3 control-label">Title<asp:PlaceHolder ID="phTitleReq" runat="server">*</asp:PlaceHolder></label>
				<div class="col-xs-12 col-md-4">
					<asp:DropDownList ID="ddlTitle" runat="server" CssClass="form-control" AppendDataBoundItems="true">
						<asp:ListItem Text="" Value=""/>
					</asp:DropDownList>
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValTitle" runat="server" ControlToValidate="ddlTitle"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration"
						InitialValue="" />
				</div>
			</div>
		</asp:PlaceHolder>
			<div class="form-group">
				<label for="txtEmail" class="col-xs-12 col-md-3 control-label">Email Address <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="100" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValEmail" runat="server" ControlToValidate="txtEmail"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration" />
					<asp:RegularExpressionValidator ID="regExValEmail" runat="server" ControlToValidate="txtEmail"
						ErrorMessage="Incorrect format or invalid email" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration"
						ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
				</div>
			</div>
		<asp:PlaceHolder ID="phRegister2" runat="server">
			<div class="form-group">
				<label for="txtAddress" class="col-xs-12 col-md-3 control-label">Address <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" MaxLength="100" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValAddress" runat="server" ControlToValidate="txtAddress"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtCity" class="col-xs-12 col-md-3 control-label">City <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtCity" runat="server" CssClass="form-control" MaxLength="50" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValCity" runat="server" ControlToValidate="txtCity"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration" />
				</div>
			</div>
			<div class="form-group">
				<label for="ddlState" class="col-xs-12 col-md-3 control-label">State <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:DropDownList ID="ddlState" runat="server" CssClass="form-control">
						<asp:ListItem Text="-select one-" Value="" />
						<asp:ListItem Text="Alabama" Value="Alabama" />
						<asp:ListItem Text="Alaska" Value="Alaska" />
						<asp:ListItem Text="Arizona" Value="Arizona" />
						<asp:ListItem Text="Arkansas" Value="Arkansas" />
						<asp:ListItem Text="California" Value="California" />
						<asp:ListItem Text="Colorado" Value="Colorado" />
						<asp:ListItem Text="Connecticut" Value="Connecticut" />
						<asp:ListItem Text="Delaware" Value="Delaware" />
						<asp:ListItem Text="Florida" Value="Florida" />
						<asp:ListItem Text="Georgia" Value="Georgia" />
						<asp:ListItem Text="Hawaii" Value="Hawaii" />
						<asp:ListItem Text="Idaho" Value="Idaho" />
						<asp:ListItem Text="Illinois" Value="Illinois" />
						<asp:ListItem Text="Indiana" Value="Indiana" />
						<asp:ListItem Text="Iowa" Value="Iowa" />
						<asp:ListItem Text="Kansas" Value="Kansas" />
						<asp:ListItem Text="Kentucky" Value="Kentucky" />
						<asp:ListItem Text="Louisiana" Value="Louisiana" />
						<asp:ListItem Text="Maine" Value="Maine" />
						<asp:ListItem Text="Maryland" Value="Maryland" />
						<asp:ListItem Text="Massachusetts" Value="Massachusetts" />
						<asp:ListItem Text="Michigan" Value="Michigan" />
						<asp:ListItem Text="Minnesota" Value="Minnesota" />
						<asp:ListItem Text="Mississippi" Value="Mississippi" />
						<asp:ListItem Text="Missouri" Value="Missouri" />
						<asp:ListItem Text="Montana" Value="Montana" />
						<asp:ListItem Text="Nebraska" Value="Nebraska" />
						<asp:ListItem Text="Nevada" Value="Nevada" />
						<asp:ListItem Text="New Hampshire" Value="New Hampshire" />
						<asp:ListItem Text="New Jersey" Value="New Jersey" />
						<asp:ListItem Text="New Mexico" Value="New Mexico" />
						<asp:ListItem Text="New York" Value="New York" />
						<asp:ListItem Text="North Carolina" Value="North Carolina" />
						<asp:ListItem Text="North Dakota" Value="North Dakota" />
						<asp:ListItem Text="Ohio" Value="Ohio" />
						<asp:ListItem Text="Oklahoma" Value="Oklahoma" />
						<asp:ListItem Text="Oregon" Value="Oregon" />
						<asp:ListItem Text="Pennsylvania" Value="Pennsylvania" />
						<asp:ListItem Text="Rhode Island" Value="Rhode Island" />
						<asp:ListItem Text="South Carolina" Value="South Carolina" />
						<asp:ListItem Text="South Dakota" Value="South Dakota" />
						<asp:ListItem Text="Tennessee" Value="Tennessee" />
						<asp:ListItem Text="Texas" Value="Texas" />
						<asp:ListItem Text="Utah" Value="Utah" />
						<asp:ListItem Text="Vermont" Value="Vermont" />
						<asp:ListItem Text="Virginia" Value="Virginia" />
						<asp:ListItem Text="Washington" Value="Washington" />
						<asp:ListItem Text="West Virginia" Value="West Virginia" />
						<asp:ListItem Text="Wisconsin" Value="Wisconsin" />
						<asp:ListItem Text="Wyoming" Value="Wyoming" />
					</asp:DropDownList>
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValState" runat="server" ControlToValidate="ddlState"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration"
						InitialValue="" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtZipCode" class="col-xs-12 col-md-3 control-label">Zip Code <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control" MaxLength="5" placeholder="#####" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValZipCode" runat="server" ControlToValidate="txtZipCode"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration" />
					<asp:RegularExpressionValidator ID="regExValZipCode" runat="server" ControlToValidate="txtZipCode"
						ErrorMessage="Incorrect format" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration"
						ValidationExpression="^\d{5}$" />
				</div>
			</div>
			<div class="form-group">
				<label for="txtPhone" class="col-xs-12 col-md-3 control-label">Phone <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" MaxLength="14" placeholder="###-###-####" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValPhone" runat="server" ControlToValidate="txtPhone"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration" />
					<asp:RegularExpressionValidator ID="regExValPhone" runat="server" ControlToValidate="txtPhone"
						ErrorMessage="Incorrect format or invalid #" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration"
						ValidationExpression="^(\(?[2-9]\d{2}\)?)[- ]?([2-9]\d{2})[- ]?\d{4}$" />
				</div>
			</div>
		</asp:PlaceHolder>
		<asp:PlaceHolder ID="phRegister3" runat="server">
			<asp:Panel ID="pnlSpecialty" runat="server" CssClass="form-group">
				<label for="ddlSpecialty" class="col-xs-12 col-md-3 control-label">Specialty <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:DropDownList ID="ddlSpecialty" runat="server" AppendDataBoundItems="true" CssClass="form-control">
						<asp:ListItem Text="" Value=""/>
					</asp:DropDownList>
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValSpecialty" runat="server" ControlToValidate="ddlSpecialty"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration"
						InitialValue="" />
				</div>
			</asp:Panel>
			<asp:Panel ID="pnlJobTitle" runat="server" CssClass="form-group">
				<label for="txtJobTitle" class="col-xs-12 col-md-3 control-label">Job Title <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtJobTitle" runat="server" CssClass="form-control" MaxLength="50" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValJobTitle" runat="server" ControlToValidate="txtJobTitle"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration" />
				</div>
			</asp:Panel>
			<asp:Panel ID="pnlProviderName" runat="server" CssClass="form-group">
				<label for="txtProviderName" class="col-xs-12 col-md-3 control-label">Provider Name <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtProviderName" runat="server" CssClass="form-control" MaxLength="50" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValProviderName" runat="server" ControlToValidate="txtProviderName"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration" />
				</div>
			</asp:Panel>
			<asp:Panel ID="pnlOrganization" runat="server" CssClass="form-group">
				<label for="txtOrganization" class="col-xs-12 col-md-3 control-label">Organization <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtOrganization" runat="server" CssClass="form-control" MaxLength="50" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:RequiredFieldValidator ID="reqValOrganization" runat="server" ControlToValidate="txtOrganization"
						ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration" />
				</div>
			</asp:Panel>
			<asp:Panel ID="pnlOneNumber" runat="server" CssClass="form-group">
				<div class="row">
					<div class="col-xs-12 col-md-12">
						Please enter either a Practitioner License Number or National Provider Identification or both if applicable: <span class="text-danger">*</span><br />
					</div>
				</div>
			</asp:Panel>
			<asp:Panel ID="pnlLicense" runat="server" CssClass="form-group">
				<label for="txtLicense" class="col-xs-12 col-md-3 control-label">Practitioner License Number</label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtLicense" runat="server" CssClass="form-control" MaxLength="20" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:CustomValidator ID="cusValOneNumber1" runat="server" ClientValidationFunction="OneChosen"
						ErrorMessage="This or NPI required" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration" />
				</div>
			</asp:Panel>
			<asp:Panel ID="pnlNpi" runat="server" CssClass="form-group">
				<label for="txtNpi" class="col-xs-12 col-md-3 control-label">National Provider Identification (NPI)</label>
				<div class="col-xs-12 col-md-4">
					<asp:TextBox ID="txtNpi" runat="server" CssClass="form-control" MaxLength="10" />
				</div>
				<div class="col-xs-12 col-md-5">
					<asp:CustomValidator ID="cusValOneNumber2" runat="server" ClientValidationFunction="OneChosen"
						ErrorMessage="This or Practitioner License Number required" SetFocusOnError="true" ValidationGroup="ProvTrainRegistration" />
				</div>
			</asp:Panel>
		</asp:PlaceHolder>
			<asp:Panel ID="pnlAcceptance" runat="server" CssClass="row">
				<div class="col-xs-12">
					<sc:Text ID="txtAcceptance" runat="server" CssClass="form-control" Field="Acceptance Content" />
				</div>
			</asp:Panel>
			<asp:Panel ID="pnlReCaptcha" runat="server" CssClass="form-group">
				<label for="ucReCaptcha" class="col-xs-12 col-md-3 control-label">Verify <span class="text-danger">*</span></label>
				<div class="col-xs-12 col-md-4">
					<uc1:ReCaptcha ID="ucReCaptcha" runat="server" />
				</div>
			</asp:Panel>
			<div class="form-group">
				<div class="col-xs-12 col-md-offset-3 col-md-4 text-xs-center">
					<asp:LinkButton ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_Click" ClientValidationFunction="OneChosen" ValidationGroup="ProvTrainRegistration">Register</asp:LinkButton>
					<asp:LinkButton ID="btnRegistered" runat="server" CssClass="btn btn-info" OnClick="BtnRegistered_Click" Visible="false" ClientValidationFunction="OneChosen" ValidationGroup="ProvTrainRegistration">Submit</asp:LinkButton>
				</div>
			</div>
		</asp:Panel>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
<!-- End SubLayouts/ProviderTrainingRegistrationPage -->
