﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using CareMore.Web.DotCom;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using Sitecore.Globalization;

namespace Website.Sublayouts
{
	/// <summary>
	/// Acts as the base class for most of the usercontrols (sublayouts) containing common functionalities.
	/// 
	/// </summary>
	public class BaseUserControl : System.Web.UI.UserControl
	{
		#region Variables

		protected Database CurrentDb = Sitecore.Context.Database;

		private Item dataSource = null;
		private Item configItem = null;
		private Item currItem = null;
		private string currLanguage = null;
		private Item currLocaleItem = null;
		private string currState =  null;
		private string currStateCode = null;
		private string currStateId = null;

		#endregion

		#region Properties

		protected Item ConfigurationItem
		{
			get
			{
				if (configItem == null)
					configItem = ItemIds.GetItem("MAPD_SITE_SETTINGS");

				return configItem;
			}
		}

		protected Item CurrentItem
		{
			get
			{
				if (currItem == null)
					currItem = CareMoreUtilities.GetLanguageItem(Sitecore.Context.Item);

				return currItem;
			}
		}

		protected string CurrentLanguage
		{
			get
			{
				if (string.IsNullOrWhiteSpace(currLanguage))
					currLanguage = CareMoreUtilities.GetCurrentLanguage();

				return currLanguage;
			}
		}

		protected Item CurrentLocaleItem
		{
			get
			{
				if (currLocaleItem == null)
					currLocaleItem = CareMoreUtilities.GetCurrentLocaleItem();

				return currLocaleItem;
			}
		}

		protected string CurrentState
		{
			get
			{
				if (string.IsNullOrWhiteSpace(currState))
					currState = CareMoreUtilities.GetCurrentLocale();

				return currState;
			}
		}

		protected string CurrentStateCode
		{
			get
			{
				if (string.IsNullOrWhiteSpace(currStateCode))
					currStateCode = CareMoreUtilities.GetCurrentLocaleCode();

				return currState;
			}
		}

		protected string CurrentStateId
		{
			get
			{
				if (string.IsNullOrWhiteSpace(currStateId))
					currStateId = CurrentLocaleItem.ID.ToString();

				return currStateId;
			}
		}

		public Item DataSource
		{
			get
			{
				if (dataSource == null && Parent is Sublayout)
					dataSource = Sitecore.Context.Database.GetItem(((Sublayout)Parent).DataSource);

				return dataSource;
			}
		}

		#endregion

		#region Protected methods

		protected void BindList(ListView view, List<Item> items)
		{
			if (items == null || items.Count == 0)
			{
				view.Visible = false;
			}
			else
			{
				view.DataSource = items;
				view.DataBind();
			}
		}

		protected string GetMediaItemUrl(MediaItem mediaItem)
		{
			if (mediaItem != null)
			{
				string repRoot = System.Web.HttpContext.Current.Request.Url.Host.ToLower();
				string repItemPath = "/~/media" + mediaItem.MediaPath + ".ashx";
				return repItemPath;
			}

			return string.Empty;
		}

		protected string GetComparisonValueCounty(string county)
		{
			string itemPath = string.Format("{0}//*[@@templatename='County' and @Name='{1}']",
				CareMoreUtilities.LOCALIZATION_PATH, county);

			Item countyItem = this.CurrentDb.SelectSingleItem(itemPath);
			return county;
		}

		protected string GetComparisonValueState(string state)
		{
			string itemPath = string.Format("{0}//*[@@templatename='State' and (@Name='{1}' or @Code='{1}' or @@ID='{1}')]",
				CareMoreUtilities.LOCALIZATION_PATH, state);

			Item stateItem = this.CurrentDb.SelectSingleItem(itemPath);
			if (stateItem != null)
			{
				state = stateItem.Fields["Name"].Value;
			}

			return state;
		}

		protected virtual Item GetSourceItem(Item thisItem)
		{
			Item sourceItem = CurrentItem;

			if (Request.QueryString["Source"] == null)
			{
				sourceItem = thisItem;
			}
			else
			{
				sourceItem = this.CurrentDb.Items[Request.QueryString["Source"]];
			}

			return sourceItem;
		}

		protected virtual Item GetEnglishItem(Item item)
		{
			Item englishItem;

			if (CurrentLanguage.Equals(CareMoreUtilities.SpanishKey, StringComparison.OrdinalIgnoreCase))
				englishItem = CurrentDb.GetItem(item.ID, Language.Parse(CareMoreUtilities.EnglishKey));
			else
				englishItem = item;

			return englishItem;
		}

		protected virtual void SetToEnglishItem()
		{
			if (CurrentLanguage.Equals(CareMoreUtilities.SpanishKey, StringComparison.OrdinalIgnoreCase))
			{
				Sitecore.Context.Item = CurrentDb.GetItem(this.CurrentItem.ID, Language.Parse(CareMoreUtilities.EnglishKey));
				currItem = Sitecore.Context.Item;
			}
		}


		#endregion

		#region Private methods

		protected void SetDownMessage(Exception ex, PlaceHolder messagePlaceholder, Control controlToHide)
		{
			Item message = ItemIds.GetItem("FIND_A_DOCTOR_DOWN_MESSAGE");

			CareMoreUtilities.DisplayError(messagePlaceholder, message["Text"], controlToHide);
		}

		#endregion
	}
}
