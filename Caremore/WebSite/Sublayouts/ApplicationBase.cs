﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security.AntiXss;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;
using System.Text;
using System.Net.Mail;
using Sitecore.Data.Fields;
using CareMore.Security;
using CareMore.Web.DotCom;

namespace Website.Sublayouts
{
	public abstract partial class ApplicationBase : BaseUserControl
	{
		private const string PostBackSessionName = "ApplicationBase";
		private const string CountyIdKey = "CountyId";
		private const string PlanIdKey = "PlanId";

		#region Properties

		public string County
		{
			get
			{
				if (string.IsNullOrEmpty(hidCountyId.Value))
					return Session[CountyIdKey] == null ? null : AntiXssEncoder.HtmlEncode((string)Session[CountyIdKey], false);
				else
					return hidCountyId.Value;
			}
		}

		public string Plan
		{
			get
			{
				if (string.IsNullOrEmpty(hidPlanId.Value))
					return Session[PlanIdKey] == null ? null : AntiXssEncoder.HtmlEncode((string)Session[PlanIdKey], false);
				else
					return hidPlanId.Value;
			}
		}

		public Item CountyItem
		{
			get
			{
				if (County == null)
					return null;
				else
					return Sitecore.Context.Database.GetItem(County);
			}
		}

		public Item PlanItem
		{
			get
			{
				if (Plan == null)
					return null;
				else
					return Sitecore.Context.Database.GetItem(Plan);
			}
		}

		public Item AppItem
		{
			get
			{
				if (PlanItem == null || string.IsNullOrEmpty(PlanItem["Application"]))
					return null;

				ReferenceField applicationItem = (ReferenceField)PlanItem.Fields["Application"];

				if (applicationItem.TargetItem == null)
					return null;
				else
					return applicationItem.TargetItem;
			}
		}

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			try
			{
				base.Page.MaintainScrollPositionOnPostBack = false;
				base.OnLoad(e);

				if (IsPostBack)
				{
					phError.Controls.Clear();
					phError.Visible = false;
				}
				else
				{
					Session[PostBackSessionName] = null;
					LoadPage();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void LnkBtn_StatementAgreedClick(object sender, EventArgs e)
		{
			try
			{
				phStatement.Visible = false;
				ShowForm();
				ScrollToTop();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void LnkBtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				if (Session[PostBackSessionName] == null)
				{
					Page.Validate();

					if (Page.IsValid)
					{
						SubmitApplication();

						//Set session to prevent refresh posts
						Session[PostBackSessionName] = DateTime.Now;
					}
				}
				else
				{
					HideForm(false);
					CareMoreUtilities.DisplayNoMultiSubmit(phError);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RblMoved_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				ChangeMoved();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RblPaymentOption_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				ChangePayment();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RblPlannedProcedures_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				ChangePlannedProcedures();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RblHomeHealthServices_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				ChangeHomeHealthServices();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RblOtherCoverage_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				ChangeCoverage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RblCareResident_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				ChangeResident();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RblStateMedicaid_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				ChangeStateMedicaid();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RblSpouseWork_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				ChangeSpouseWork();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RblAuthorizedRep_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				ChangeAuthRep();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RblSalesAgent_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				ChangeSalesAgent();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private Methods

		private void LoadPage()
		{
			// Ensiure on correct language item
			string redirectUrl = CheckLocale();
			if (!string.IsNullOrEmpty(redirectUrl))
			{
				Response.Redirect(redirectUrl, false);
				return;
			}

			if (County == null && Plan == null)
			{
				Response.Redirect(ItemIds.GetUrl("ENROLL_ID"), false);
			}
			else
			{
				hidCountyId.Value = County;
				hidPlanId.Value = Plan;

				lblEnrolleeDate.Text = CareMoreUtilities.DateFormat(DateTime.Now);
				lblReleaseDate.Text = lblEnrolleeDate.Text;
				lblCountyName.Text = CountyItem["Region Name"];

				phApplicationDisclaimer.Visible = false;
				phApplicationForm.Visible = false;

				ShowStatementOfUnderstanding();
				ScrollToTop();
			}
		}

		private void ChangeMoved()
		{
			if (Boolean.Parse(rblMoved.SelectedValue))
			{
				lblDateOfMove.Enabled = true;
				txtDateOfMove.Enabled = true;
				reqValDateOfMove.Visible = true;
				cmpValDateOfMove.Visible = true;
			}
			else
			{
				lblDateOfMove.Enabled = false;
				txtDateOfMove.Text = string.Empty;
				txtDateOfMove.Enabled = false;
				reqValDateOfMove.Visible = false;
				cmpValDateOfMove.Visible = false;
			}
		}

		private void ChangePayment()
		{
			if (rblPaymentOption.SelectedValue.StartsWith("Electronic funds transfer"))
			{
				pnlPaymentPlanDetails.Visible = true;
				phBankAccount.Visible = true;

				txtAccountHolderName.Enabled = true;
				reqValAccountHolderName.Enabled = true;

				rblAccountType.Enabled = true;
				reqValAccountType.Enabled = true;

				txtRoutingNumber.Enabled = true;
				reqValRoutingNumber.Enabled = true;

				txtAccountNumber.Enabled = true;
				reqValAccountNumber.Enabled = true;

				phCreditCardInfo.Visible = false;
			}
			else
			{
				if (rblPaymentOption.SelectedValue.Equals("Credit Card"))
				{
					pnlPaymentPlanDetails.Visible = true;
					phCreditCardInfo.Visible = true;
				}
				else
				{
					pnlPaymentPlanDetails.Visible = false;
					phCreditCardInfo.Visible = false;
				}

				phBankAccount.Visible = false;

				txtAccountHolderName.Text = string.Empty;
				txtAccountHolderName.Enabled = false;
				reqValAccountHolderName.Enabled = false;

				rblAccountType.ClearSelection();
				rblAccountType.Enabled = false;
				reqValAccountType.Enabled = false;

				txtRoutingNumber.Text = string.Empty;
				txtRoutingNumber.Enabled = false;
				reqValRoutingNumber.Enabled = false;

				txtAccountNumber.Text = string.Empty;
				txtAccountNumber.Enabled = false;
				reqValAccountNumber.Enabled = false;
			}
		}

		private void ChangePlannedProcedures()
		{
			if (Boolean.Parse(rblPlannedProcedures.SelectedValue))
			{
				txtTreatmentDetails.Enabled = true;

				txtPlannedSpecialistFirstName1.Enabled = true;
				txtPlannedSpecialistLastName1.Enabled = true;
				txtPlannedSpecialistPhone1.Enabled = true;
				txtPlannedSpecialtyType1.Enabled = true;

				txtPlannedSpecialistFirstName2.Enabled = true;
				txtPlannedSpecialistLastName2.Enabled = true;
				txtPlannedSpecialistPhone2.Enabled = true;
				txtPlannedSpecialtyType2.Enabled = true;

				txtPlannedSpecialistFirstName3.Enabled = true;
				txtPlannedSpecialistLastName3.Enabled = true;
				txtPlannedSpecialistPhone3.Enabled = true;
				txtPlannedSpecialtyType3.Enabled = true;

				rblSpecialistVA.Enabled = true;
			}
			else
			{
				txtTreatmentDetails.Text = string.Empty;
				txtTreatmentDetails.Enabled = false;

				txtPlannedSpecialistFirstName1.Text = string.Empty;
				txtPlannedSpecialistFirstName1.Enabled = false;
				txtPlannedSpecialistLastName1.Text = string.Empty;
				txtPlannedSpecialistLastName1.Enabled = false;
				txtPlannedSpecialistPhone1.Text = string.Empty;
				txtPlannedSpecialistPhone1.Enabled = false;
				txtPlannedSpecialtyType1.Text = string.Empty;
				txtPlannedSpecialtyType1.Enabled = false;

				txtPlannedSpecialistFirstName2.Text = string.Empty;
				txtPlannedSpecialistFirstName2.Enabled = false;
				txtPlannedSpecialistLastName2.Text = string.Empty;
				txtPlannedSpecialistLastName2.Enabled = false;
				txtPlannedSpecialistPhone2.Text = string.Empty;
				txtPlannedSpecialistPhone2.Enabled = false;
				txtPlannedSpecialtyType2.Text = string.Empty;
				txtPlannedSpecialtyType2.Enabled = false;

				txtPlannedSpecialistFirstName3.Text = string.Empty;
				txtPlannedSpecialistFirstName3.Enabled = false;
				txtPlannedSpecialistLastName3.Text = string.Empty;
				txtPlannedSpecialistLastName3.Enabled = false;
				txtPlannedSpecialistPhone3.Text = string.Empty;
				txtPlannedSpecialistPhone3.Enabled = false;
				txtPlannedSpecialtyType3.Text = string.Empty;
				txtPlannedSpecialtyType3.Enabled = false;

				rblSpecialistVA.ClearSelection();
				rblSpecialistVA.Enabled = false;
			}
		}

		private void ChangeHomeHealthServices()
		{
			if (Boolean.Parse(rblHomeHealthServices.SelectedValue))
			{
				txtHomeHealthProviderName.Enabled = true;
				txtHomeHealthProviderPhone.Enabled = true;
				cblHomeHealthServices.Enabled = true;
				txtUpcomingHomeHealthVisits.Enabled = true;
			}
			else
			{
				txtHomeHealthProviderName.Text = string.Empty;
				txtHomeHealthProviderName.Enabled = false;

				txtHomeHealthProviderPhone.Text = string.Empty;
				txtHomeHealthProviderPhone.Enabled = false;

				cblHomeHealthServices.ClearSelection();
				cblHomeHealthServices.Enabled = false;

				txtUpcomingHomeHealthVisits.Text = string.Empty;
				txtUpcomingHomeHealthVisits.Enabled = false;
			}
		}

		private void ChangeCoverage()
		{
			if (Boolean.Parse(rblOtherCoverage.SelectedValue))
			{
				txtOtherCoverageName.Enabled = true;
				reqValOtherCoverageName.Enabled = true;

				txtOtherCoverageId.Enabled = true;
				reqValOtherCoverageId.Enabled = true;

				txtOtherCoverageGroupNumber.Enabled = true;
				reqValOtherCoverageGroupNumber.Enabled = true;

				txtOtherCoverageDate.Enabled = true;
				reqValOtherCoverageDate.Enabled = true;
				cmpValOtherCoverageDate.Enabled = true;
			}
			else
			{
				txtOtherCoverageName.Text = string.Empty;
				txtOtherCoverageName.Enabled = false;
				reqValOtherCoverageName.Enabled = false;

				txtOtherCoverageId.Text = string.Empty;
				txtOtherCoverageId.Enabled = false;
				reqValOtherCoverageId.Enabled = false;

				txtOtherCoverageGroupNumber.Text = string.Empty;
				txtOtherCoverageGroupNumber.Enabled = false;
				reqValOtherCoverageGroupNumber.Enabled = false;

				txtOtherCoverageDate.Text = string.Empty;
				txtOtherCoverageDate.Enabled = false;
				reqValOtherCoverageDate.Enabled = false;
				cmpValOtherCoverageDate.Enabled = false;
			}
		}

		private void ChangeResident()
		{
			if (Boolean.Parse(rblCareResident.SelectedValue))
			{
				txtCareResident.Enabled = true;
				reqValCareResidentInfo.Enabled = true;
			}
			else
			{
				txtCareResident.Text = string.Empty;
				txtCareResident.Enabled = false;
				reqValCareResidentInfo.Enabled = false;
			}
		}

		private void ChangeStateMedicaid()
		{
			if (Boolean.Parse(rblStateMedicaid.SelectedValue))
			{
				txtStateMedicaidNumber.Enabled = true;
				reqValStateMedicaidNumber.Enabled = true;
			}
			else
			{
				txtStateMedicaidNumber.Text = string.Empty;
				txtStateMedicaidNumber.Enabled = false;
				reqValStateMedicaidNumber.Enabled = false;
			}
		}

		private void ChangeSpouseWork()
		{
			if (Boolean.Parse(rblSpouseWork.SelectedValue))
			{
				txtSpouseGroupCoveragePlan.Enabled = true;
			}
			else
			{
				txtSpouseGroupCoveragePlan.Text = string.Empty;
				txtSpouseGroupCoveragePlan.Enabled = false;
			}
		}

		private void ChangeAuthRep()
		{
			if (Boolean.Parse(rblAuthorizedRep.SelectedValue))
			{
				txtRepName.Enabled = true;
				reqValRepName.Enabled = true;

				txtRepAddress.Enabled = true;
				reqValRepAddress.Enabled = true;

				txtRepPhone.Enabled = true;
				reqValRepPhone.Enabled = true;
				regExValRepPhone.Enabled = true;

				txtRepRelation.Enabled = true;
				reqValRepRelation.Enabled = true;
			}
			else
			{
				txtRepName.Text = string.Empty;
				txtRepName.Enabled = false;
				reqValRepName.Enabled = false;

				txtRepAddress.Text = string.Empty;
				txtRepAddress.Enabled = false;
				reqValRepAddress.Enabled = false;

				txtRepPhone.Text = string.Empty;
				txtRepPhone.Enabled = false;
				reqValRepPhone.Enabled = false;
				regExValRepPhone.Enabled = false;

				txtRepRelation.Text = string.Empty;
				txtRepRelation.Enabled = false;
				reqValRepRelation.Enabled = false;
			}
		}

		private void ChangeSalesAgent()
		{
			if (Boolean.Parse(rblSalesAgent.SelectedValue))
			{
				txtSalesAgentCode.Enabled = true;
				reqValSalesAgentCode.Enabled = true;

				txtSalesAgentFirstName.Enabled = true;
				reqValSalesAgentFirstName.Enabled = true;

				txtSalesAgentLastName.Enabled = true;
				reqValSalesAgentLastName.Enabled = true;
			}
			else
			{
				txtSalesAgentCode.Text = string.Empty;
				txtSalesAgentCode.Enabled = false;
				reqValSalesAgentCode.Enabled = false;

				txtSalesAgentFirstName.Text = string.Empty;
				txtSalesAgentFirstName.Enabled = false;
				reqValSalesAgentFirstName.Enabled = false;

				txtSalesAgentLastName.Text = string.Empty;
				txtSalesAgentLastName.Enabled = false;
				reqValSalesAgentLastName.Enabled = false;
			}
		}

		private string CheckLocale()
		{
			string redirectUrl = string.Empty;
			Item applicationItem;

			if (Sitecore.Context.Language.Name.Equals(CareMoreUtilities.SpanishKey, StringComparison.OrdinalIgnoreCase))
				applicationItem = CurrentDb.Items[CareMoreUtilities.HOME_PATH + "/Plans/Spanish Application"];
			else
				applicationItem = CurrentDb.Items[CareMoreUtilities.HOME_PATH + "/Plans/Application"];

			if (!applicationItem.Paths.FullPath.Equals(CurrentItem.Paths.FullPath))
				redirectUrl = LinkManager.GetItemUrl(applicationItem);

			return redirectUrl;
		}

		private void ShowConfirmation(string confNumber)
		{
			HideForm(true);
			lblConfNum.Text = AntiXssEncoder.HtmlEncode(confNumber, false);
			litCustomCodeSubmit.Text = Common.GetCustomCode(CurrentItem, Request, true, true);
		}

		private void HideForm(bool showConfirmation)
		{
			phApplicationDisclaimer.Visible = false;
			phStatement.Visible = false;
			phApplicationForm.Visible = false;
			phConfirmation.Visible = showConfirmation;
			phPlanHeader.Visible = false;
		}

		private void ScrollToTop()
		{
			string jScript;
			jScript = "<script type=\"text/javascript\" language=\"javascript\">\n";
			jScript += "scroll(0,0);\n";
			jScript += "</script>\n";

			Type cstype = this.GetType();

			// Get a ClientScriptManager reference from the Page class.
			ClientScriptManager cs = Page.ClientScript;

			// Check to see if the startup script is already registered.
			if (!cs.IsStartupScriptRegistered(cstype, "regScrollTop"))
			{
				cs.RegisterStartupScript(cstype, "regScrollTop", jScript);
			}
		}

		private void SendEmailNotification()
		{
			MailMessage message = new MailMessage();
			Item CurrentItem = Sitecore.Context.Item;

			string toAddress = CurrentItem["To"];
			if (toAddress.Length > 0)
			{
				string[] destinationAddresses = toAddress.Split(';');
				foreach (string address in destinationAddresses)
				{
					message.To.Add(new MailAddress(address));
				}

				if (CurrentItem["From"] != null && CurrentItem["From"].Length > 0)
					message.From = new MailAddress(CurrentItem["From"]);

				message.Subject = CurrentItem["Subject"];

				message.Body = CurrentItem["MailContent"];
				message.IsBodyHtml = true;

				//Send email
				CareMoreUtilities.SendEmail(message);
			}
		}

		private void ShowForm()
		{
			Item appItem = AppItem;
			if (appItem == null)
				return;

			phApplicationForm.Visible = true;
			phPlanHeader.Visible = true;

			// Defaults to the first day of the next calendar month and only allow the first day of the next two calendar months
			DateTime startNextMonth = DateTime.Now.AddDays(1 - DateTime.Now.Day).AddMonths(1);
			ddlCoverageEffectiveDate.Items.Add(startNextMonth.ToString("MM/dd/yyyy"));
			ddlCoverageEffectiveDate.Items.Add(startNextMonth.AddMonths(1).ToString("MM/dd/yyyy"));
			ddlCoverageEffectiveDate.Items.Add(startNextMonth.AddMonths(2).ToString("MM/dd/yyyy"));

			// Get states
			var states = ItemIds.GetItem("STATE_SETTINGS_ID").Children.Where(item => item["Exclude From Application"] != CareMoreUtilities.CheckedTrueKey);
			if (states != null && states.Count() > 0)
			{
				List<ListItem> stateListItems = states.ToList().ConvertAll(item => new ListItem()
				{
					Value = item.Fields["Code"].Value,
					Text = item.Fields["Name"].Value
				});

				ddlState.DataSource = stateListItems;
				ddlState.DataBind();

				// select current state
				ddlState.Items.FindByText(CareMoreUtilities.GetCurrentLocale()).Selected = true;
			}

			// set CC Exp Year
			List<ListItem> ccExpYears = new List<ListItem>();
			int expYear = DateTime.Now.Year;
			for (int x = 0; x < 10; x++)
			{
				ccExpYears.Add(new ListItem() { Text = expYear.ToString() });
				expYear++;
			}

			ddlCCExpirationYear.DataSource = ccExpYears;
			ddlCCExpirationYear.DataBind();

			lblRequired.Text = CurrentDb.SelectSingleItem(CareMoreUtilities.WORDING_PATH + "/Required")["Text"];

			hidSitecoreId.Value = appItem.ID.Guid.ToString("B");
			lblPleaseReadInfo.Text = appItem["Read Information"];
			lblReleaseofInfo.Text = appItem["Release of Information"];
			lblPaymentPlan.Text = appItem["Payment Plan"];
			litMedicareText.Text = appItem["Medicare Text"];
			litNeedHelp.Text = appItem["Need Help"];
			litBeginEnrollment.Text = appItem["Begin Enrollment"];
			litChronicSpecialNeedsAcknowledgment.Text = appItem["Chronic Special Needs Acknowledgment"];
			litSpouseText.Text = appItem["Spouse Label"];
			reqValSpouseWork.ErrorMessage = appItem["Spouse Error Message"];
			lblPlanName.Text = CurrentDb.GetItem(PlanItem["Plan Type"])["Title"];
			pnlCoverageEffectiveDate.Visible = appItem["Coverage Effective Date"].Equals("1");
			pnlIsCellPhone.Visible = appItem["Is Cell Phone"].Equals("1");
			pnlMoved.Visible = appItem["Have You Moved"].Equals("1");
			pnlEmail.Visible = !appItem["Consent To Contact"].Equals("1");
			phConsentToContact.Visible = appItem["Consent To Contact"].Equals("1");

			ListItem listItem = rblPaymentOption.Items.FindByValue("Credit Card");
			if (appItem["Payment By Credit Card"].Equals(CareMoreUtilities.CheckedTrueKey))
			{
				if (listItem == null)
				{
					if (CareMoreUtilities.GetCurrentLanguage().Equals("en", StringComparison.OrdinalIgnoreCase))
						rblPaymentOption.Items.Insert(2, new ListItem("Credit Card", "Credit Card"));
					else
						rblPaymentOption.Items.Insert(2, new ListItem("Tarjeta de crédito.", "Credit Card"));
				}
			}
			else if (listItem != null)
			{
				rblPaymentOption.Items.Remove(listItem);
			}

			phChronicSNP.Visible = appItem["Chronic SNP"].Equals("1");
			phMedicationsAcknowledgment.Visible = appItem["Medications Acknowledgment"].Equals("1");
			phContinuityCareCoordination.Visible = appItem["Continuity Care Coordination"].Equals("1");
			pnlESRD.Visible = appItem["ESRD"].Equals("1");
			pnlOtherCoverageGroup.Visible = !appItem["Coverage End Date"].Equals("1");
			pnlCoverageEndDate.Visible = appItem["Coverage End Date"].Equals("1");
			pnlCurrentMember.Visible = appItem["Current Member"].Equals("1");
			phSpouseGroupCoveragePlan.Visible = appItem["Spouse Group Coverage"].Equals("1");
			pnlLiveLongTerm.Visible = appItem["Live in Long Term Care Facility"].Equals("1");
			pnlChronicLung.Visible = appItem["Chronic Lung Disorder"].Equals("1");
			pnlDiabetes.Visible = appItem["Diabetes"].Equals("1");
			pnlChronicHeart.Visible = appItem["Chronic Heart Failure"].Equals("1");
			pnlSalesAgent.Visible = appItem["Sales Agent"].Equals("1");

			hlProviderSearch.NavigateUrl = ItemIds.GetUrl("FIND_A_DOCTOR");

			ShowPlanDetail();
		}

		private void ShowPlanDetail()
		{
			lblPlanID.Text = PlanItem["Plan ID"];
			lblPlanPremium.Text = PlanItem["Premium"];

			if (string.IsNullOrEmpty(PlanItem["Payment Info"]))
			{
				phAdditionalPayment.Visible = false;
				lblAdditionalPayment.Text = PlanItem["Payment Info"];
			}
			else
			{
				phAdditionalPayment.Visible = true;
			}
		}

		private void ShowStatementOfUnderstanding()
		{
			phStatement.Visible = true;

			Item souItem = AppItem;
			if (souItem != null)
			{
				litStatementOfUnderstanding.Text = souItem["Statement of Understanding"];
			}

			Item atlas = CurrentDb.GetItem("/sitecore/content/Home/Global/Custom Code/Atlas");
			litCustomCodeLoad.Text = string.Format(atlas["Code"], "haccar_ApplicationPage_1");
		}

		private void ResetForm()
		{
			ddlCoverageEffectiveDate.SelectedIndex = 0;
			txtLastName.Text = string.Empty;
			txtFirstName.Text = string.Empty;
			txtMiddleInitial.Text = string.Empty;
			rblSalutation.ClearSelection();
			txtBirthDate.Text = string.Empty;
			rblSex.ClearSelection();
			txtHomePhone.Text = string.Empty;
			txtAltPhone.Text = string.Empty;
			rblIsCellPhone.ClearSelection();
			txtAddress.Text = string.Empty;
			txtCity.Text = string.Empty;
			ddlState.Items.FindByText(CareMoreUtilities.GetCurrentLocale()).Selected = true;
			txtZip.Text = string.Empty;
			txtAddress2.Text = string.Empty;
			txtCity2.Text = string.Empty;
			txtState2.Text = string.Empty;
			txtZip2.Text = string.Empty;

			rblMoved.ClearSelection();
			txtDateOfMove.Text = string.Empty;

			txtEmergencyContact.Text = string.Empty;
			txtEmergencyPhone.Text = string.Empty;
			txtRelation.Text = string.Empty;
			txtEmailAddress.Text = string.Empty;

			cbReceiveDocsByEmail.Checked = false;
			cbReceiveInfoByEmail.Checked = false;
			txtReceiveEmailAddress.Text = string.Empty;

			txtMedicareName.Text = string.Empty;
			txtMedicareClaimNumber.Text = string.Empty;
			rblMedicareSex.ClearSelection();
			txtHospitalBenefitDate.Text = string.Empty;
			txtMedicalBenefitDate.Text = string.Empty;

			rblPaymentOption.ClearSelection();
			txtAccountHolderName.Text = string.Empty;
			rblAccountType.ClearSelection();
			txtRoutingNumber.Text = string.Empty;
			txtAccountNumber.Text = string.Empty;
			ddlCreditCardType.ClearSelection();
			txtCreditCardAccountHolderName.Text = string.Empty;
			txtCreditCardAccountNumber.Text = string.Empty;
			ddlCCExpirationMonth.ClearSelection();
			ddlCCExpirationYear.ClearSelection();

			rblESRD.ClearSelection();
			txtCurrentDialysisCenter.Text = string.Empty;
			txtCurrentDialysisPhone.Text = string.Empty;
			txtNephrologistFirstName.Text = string.Empty;
			txtNephrologistLastName.Text = string.Empty;
			txtNewDialysisCenter.Text = string.Empty;

			rblSNP_Asthma.ClearSelection();
			rblSNP_Emphysema.ClearSelection();
			rblSNP_Diabetes.ClearSelection();
			rblSNP_Insulin.ClearSelection();
			rblSNP_CoronaryArteryDisease.ClearSelection();
			rblSNP_HeartAttack.ClearSelection();
			rblSNP_HeartFailure.ClearSelection();
			rblSNP_FluidSwelling.ClearSelection();
			txtTreatingFirstName.Text = string.Empty;
			txtTreatingLastName.Text = string.Empty;
			txtTreatingSpecialty.Text = string.Empty;
			txtTreatingPhone.Text = string.Empty;
			txtEnrolleeInitials.Text = string.Empty;

			cblMedications.ClearSelection();

			rblPlannedProcedures.ClearSelection();
			txtTreatmentDetails.Text = string.Empty;
			txtPlannedSpecialistFirstName1.Text = string.Empty;
			txtPlannedSpecialistLastName1.Text = string.Empty;
			txtPlannedSpecialistPhone1.Text = string.Empty;
			txtPlannedSpecialtyType1.Text = string.Empty;
			txtPlannedSpecialistFirstName2.Text = string.Empty;
			txtPlannedSpecialistLastName2.Text = string.Empty;
			txtPlannedSpecialistPhone2.Text = string.Empty;
			txtPlannedSpecialtyType2.Text = string.Empty;
			txtPlannedSpecialistFirstName3.Text = string.Empty;
			txtPlannedSpecialistLastName3.Text = string.Empty;
			txtPlannedSpecialistPhone3.Text = string.Empty;
			txtPlannedSpecialtyType3.Text = string.Empty;
			rblSpecialistVA.ClearSelection();

			rblUsingOxygen.ClearSelection();
			rblDiagnosedCopd.ClearSelection();

			rblHomeHealthServices.ClearSelection();
			txtHomeHealthProviderName.Text = string.Empty;
			txtHomeHealthProviderPhone.Text = string.Empty;
			cblHomeHealthServices.ClearSelection();
			txtUpcomingHomeHealthVisits.Text = string.Empty;

			cblRentedItems.ClearSelection();
			cbOtherDme.Checked = false;
			txtOtherDme.Text = string.Empty;
			txtDmeProviderName.Text = string.Empty;
			txtDmeProviderPhone.Text = string.Empty;

			rblESRD.ClearSelection();

			rblOtherCoverage.ClearSelection();
			txtOtherCoverageName.Text = string.Empty;
			txtOtherCoverageId.Text = string.Empty;
			txtOtherCoverageGroupNumber.Text = string.Empty;
			txtOtherCoverageDate.Text = string.Empty;

			rblCareResident.ClearSelection();
			txtCareResident.Text = string.Empty;

			rblStateMedicaid.ClearSelection();
			txtStateMedicaidNumber.Text = string.Empty;

			rblCurrentMember.ClearSelection();
			txtCurrentMemberPlan.Text = string.Empty;

			rblSpouseWork.ClearSelection();
			txtSpouseGroupCoveragePlan.Text = string.Empty;

			rblLiveLongTerm.ClearSelection();
			rblChronicLung.ClearSelection();
			rblDiabetes.ClearSelection();
			rblChronicHeart.ClearSelection();

			txtPrimaryPhysicianName.Text = string.Empty;
			rblPrintFormat.Items[0].Selected = true;

			txtReleaseInitials.Text = string.Empty;
			cblSignature.ClearSelection();

			rblAuthorizedRep.ClearSelection();
			txtRepName.Text = string.Empty;
			txtRepAddress.Text = string.Empty;
			txtRepPhone.Text = string.Empty;
			txtRepRelation.Text = string.Empty;

			rblSalesAgent.ClearSelection();
			txtSalesAgentCode.Text = string.Empty;
			txtSalesAgentFirstName.Text = string.Empty;
			txtSalesAgentLastName.Text = string.Empty;
		}

		private void SubmitApplication()
		{
			string confirmationNumber = txtFirstName.Text.Substring(0, 1) +
				txtFirstName.Text.Substring(txtFirstName.Text.Length - 1, 1) +
				txtHomePhone.Text.Substring(txtHomePhone.Text.Length - 4, 4) +
				txtLastName.Text.Substring(0, 1) +
				txtLastName.Text.Substring(txtLastName.Text.Length - 1, 1) +
				"-" + DateTime.Now.ToString("yyyyMMddhhmmss");
			confirmationNumber = confirmationNumber.ToUpper();

			MemberApplication newMember = new MemberApplication()
			{
				ApplicationId = Guid.NewGuid(),
				AppSitecoreId = Guid.Parse(hidSitecoreId.Value),
				SubmittedOn = DateTime.Now,
				County = CountyItem["County"],
				PlanAppliedFor = lblPlanName.Text,
				SupplPackage = string.Empty, // No longer used
				IPAGroupNumber = string.Empty, // No longer used
				CoverageEffectiveDate = DateTime.Parse(ddlCoverageEffectiveDate.SelectedItem.Value),
				FirstName = txtFirstName.Text,
				LastName = txtLastName.Text,
				MiddleInitial = txtMiddleInitial.Text,
				Salutation = rblSalutation.SelectedValue,
				SocialSecurityNumber = string.Empty, // No longer used
				BirthDate = GetDate(txtBirthDate),
				Sex = Convert.ToChar(rblSex.SelectedValue),
				HomePhone = txtHomePhone.Text,
				AltPhone = txtAltPhone.Text,
				IsCellPhone = GetTrueFalse(rblIsCellPhone),
				Address = txtAddress.Text,
				City = txtCity.Text,
				State = ddlState.SelectedValue,
				Zip = txtZip.Text,
				Address2 = txtAddress2.Text,
				City2 = txtCity2.Text,
				State2 = txtState2.Text,
				Zip2 = txtZip2.Text,

				Moved = GetTrueFalse(rblMoved),
				DateOfMove = CareMoreUtilities.GetDateNull(txtDateOfMove),

				EmergencyContactName = txtEmergencyContact.Text,
				EmergencyContactPhone = txtEmergencyPhone.Text,
				EmergencyContactRelation = txtRelation.Text,
				EmailAddress = pnlEmail.Visible ? txtEmailAddress.Text : txtReceiveEmailAddress.Text,
				ReceiveDocsByEmail = cbReceiveDocsByEmail.Checked,
				ReceiveInfoByEmail = cbReceiveInfoByEmail.Checked,

				PhysicianChoice = string.Empty, // No longer used
				DentistChoice = string.Empty, // No longer used

				MedicareName = txtMedicareName.Text,
				MedicareNumber = Cryptography.Encrypt(txtMedicareClaimNumber.Text),
				MedicareSex = rblMedicareSex.SelectedValue,
				EntitledToHospitalBenefits = txtHospitalBenefitDate.Text.Length > 0 ? true : false,
				HospitalBenefitsDate = CareMoreUtilities.GetDateNull(txtHospitalBenefitDate),
				EntitledToMedicalBenefits = txtMedicalBenefitDate.Text.Length > 0 ? true : false,
				MedicalBenefitsDate = CareMoreUtilities.GetDateNull(txtMedicalBenefitDate),

				BenefitsVerifiedBy = string.Empty, // No longer used
				ResidenceCounty = string.Empty, // No longer used
				ChronicConditions = string.Empty, // No longer used

				PaymentOption = rblPaymentOption.SelectedValue,
				AccountHolderName = phCreditCardInfo.Visible ? txtCreditCardAccountHolderName.Text : txtAccountHolderName.Text,
				AccountType = phCreditCardInfo.Visible ? string.Empty : rblAccountType.SelectedValue,
				RoutingNumber = phCreditCardInfo.Visible ? string.Empty : Cryptography.Encrypt(txtRoutingNumber.Text),
				AccountNumber = phCreditCardInfo.Visible ? Cryptography.Encrypt(txtCreditCardAccountNumber.Text) : Cryptography.Encrypt(txtAccountNumber.Text),
				CreditCardType = phCreditCardInfo.Visible ? ddlCreditCardType.SelectedValue : string.Empty,
				CreditCardExpireDate = phCreditCardInfo.Visible ? string.Format("{0}/{1}", ddlCCExpirationMonth.SelectedValue, ddlCCExpirationYear.SelectedValue) : string.Empty,

				ChronicESRD = GetTrueFalse(rblSNP_ESRD),
				CurrentDialysisCenter = txtCurrentDialysisCenter.Text,
				CurrentDialysisPhone = txtCurrentDialysisPhone.Text,
				NephrologistFirstName = txtNephrologistFirstName.Text,
				NephrologistLastName = txtNephrologistLastName.Text,
				NewDialysisCenter = txtNewDialysisCenter.Text,

				ChronicAsthma = GetTrueFalse(rblSNP_Asthma),
				ChronicEmphysema = GetTrueFalse(rblSNP_Emphysema),
				ChronicDiabetes = GetTrueFalse(rblSNP_Diabetes),
				ChronicInsulin = GetTrueFalse(rblSNP_Insulin),
				ChronicCoronaryArteryDisease = GetTrueFalse(rblSNP_CoronaryArteryDisease),
				ChronicHeartAttack = GetTrueFalse(rblSNP_HeartAttack),
				ChronicHeartFailure = GetTrueFalse(rblSNP_HeartFailure),
				ChronicFluidSwelling = GetTrueFalse(rblSNP_FluidSwelling),

				TreatingFirstName = txtTreatingFirstName.Text,
				TreatingLastName = txtTreatingLastName.Text,
				TreatingSpecialty = txtTreatingSpecialty.Text,
				TreatingPhone = txtTreatingPhone.Text,

				ChronicEnrolleeInitials = txtEnrolleeInitials.Text,
				ChronicEnrolleeDate = GetDate(lblEnrolleeDate),
				MedicationsAcknowledgment = cblMedications.SelectedIndex != -1,

				PlannedProcedures = GetTrueFalse(rblPlannedProcedures),
				TreatmentDetails = txtTreatmentDetails.Text,
				PlannedSpecialistFirstName1 = txtPlannedSpecialistFirstName1.Text,
				PlannedSpecialistLastName1 = txtPlannedSpecialistLastName1.Text,
				PlannedSpecialistPhone1 = txtPlannedSpecialistPhone1.Text,
				PlannedSpecialistType1 = txtPlannedSpecialtyType1.Text,
				PlannedSpecialistFirstName2 = txtPlannedSpecialistFirstName2.Text,
				PlannedSpecialistLastName2 = txtPlannedSpecialistLastName2.Text,
				PlannedSpecialistPhone2 = txtPlannedSpecialistPhone2.Text,
				PlannedSpecialistType2 = txtPlannedSpecialtyType2.Text,
				PlannedSpecialistFirstName3 = txtPlannedSpecialistFirstName3.Text,
				PlannedSpecialistLastName3 = txtPlannedSpecialistLastName3.Text,
				PlannedSpecialistPhone3 = txtPlannedSpecialistPhone3.Text,
				PlannedSpecialistType3 = txtPlannedSpecialtyType3.Text,
				SpecialistPartOfVA = GetTrueFalse(rblSpecialistVA),

				UsingOxygen = GetTrueFalse(rblUsingOxygen),
				DiagnosedCopd = GetTrueFalse(rblDiagnosedCopd),

				HomeHealthServices = GetTrueFalse(rblHomeHealthServices),
				HomeHealthProviderName = txtHomeHealthProviderName.Text,
				HomeHealthProviderPhone = txtHomeHealthProviderPhone.Text,
				HomeHealthServicesUsed = CareMoreUtilities.GetSelectedItems(cblHomeHealthServices.Items),
				UpcomingHomeHealthVisits = txtUpcomingHomeHealthVisits.Text,

				RentedItems = CareMoreUtilities.GetSelectedItems(cblRentedItems.Items),
				RentedItemsOther = txtOtherDme.Text,
				DmeProviderName = txtDmeProviderName.Text,
				DmeProviderPhone = txtDmeProviderPhone.Text,

				ESRD = GetTrueFalse(rblESRD),

				OtherCoverage = GetTrueFalse(rblOtherCoverage),
				OtherCoverageName = txtOtherCoverageName.Text,
				OtherCoverageId = txtOtherCoverageId.Text,
				OtherCoverageGroupNumber = txtOtherCoverageGroupNumber.Text,
				OtherCoverageDate = GetDate(txtOtherCoverageDate),

				CareResident = GetTrueFalse(rblCareResident),
				CareResidentInfo = txtCareResident.Text,

				CurrentMember = GetTrueFalse(rblCurrentMember),
				CurrentMemberPlan = txtCurrentMemberPlan.Text,

				StateMedicaid = GetTrueFalse(rblStateMedicaid),
				StateMedicaidNumber = Cryptography.Encrypt(txtStateMedicaidNumber.Text),

				SpouseWork = GetTrueFalse(rblSpouseWork),
				SpouseGroupCoveragePlan = txtSpouseGroupCoveragePlan.Text,

				LiveinLongTermCareFacility = GetTrueFalse(rblLiveLongTerm),
				ChronicLung = GetTrueFalse(rblChronicLung),
				Diabetes = GetTrueFalse(rblDiabetes),
				ChronicHeart = GetTrueFalse(rblChronicHeart),
				ExistingPatient = false,  // No longer used

				PrimaryPhysicianName = txtPrimaryPhysicianName.Text,
				PrintFormat = CareMoreUtilities.GetSelectedItems(rblPrintFormat.Items),
				ReceivedSummary = false,
				ReleaseInitials = txtReleaseInitials.Text,
				ReleaseDate = GetDate(lblReleaseDate),
				Signature = "I Agree",

				IsAuthorizedRep = GetTrueFalse(rblAuthorizedRep),
				RepName = txtRepName.Text,
				RepAddress = txtRepAddress.Text,
				RepPhone = txtRepPhone.Text,
				RepRelation = txtRepRelation.Text,

				IsSalesAgent = GetTrueFalse(rblSalesAgent),
				SalesAgentCode = txtSalesAgentCode.Text,
				SalesAgentFirstName = txtSalesAgentFirstName.Text,
				SalesAgentLastName = txtSalesAgentLastName.Text,

				ConfirmationNumber = confirmationNumber,
				IPAddress = CareMoreUtilities.UserIpAddress(System.Web.HttpContext.Current.Request),
				Status = false // Status false = Unprocessed, true = Precessed
			};

			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			db.MemberApplications.InsertOnSubmit(newMember);
			db.SubmitChanges();

			SendEmailNotification();
			ShowConfirmation(confirmationNumber);
			ResetForm();
		}

		private DateTime GetDate(Label label)
		{
			DateTime? dateNull = CareMoreUtilities.GetDateNull(label.Text);

			if (dateNull.HasValue)
				return dateNull.Value;
			else
				return DateTime.MinValue;
		}

		private DateTime GetDate(TextBox textBox)
		{
			DateTime? dateNull = CareMoreUtilities.GetDateNull(textBox);

			if (dateNull.HasValue)
				return dateNull.Value;
			else
				return DateTime.MinValue;
		}

		private bool GetTrueFalse(RadioButtonList list)
		{
			if (list.SelectedIndex == -1)
				return false;
			else
				return bool.Parse(list.SelectedValue);
		}

		#endregion
	}
}
