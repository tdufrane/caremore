﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Web.UI.WebControls;

namespace Website.Sublayouts
{
	public partial class VideoListPage : BaseUserControl
	{
		#region Private variables

		private int itemsPerRow = 0;
		private int shownCount = 0;

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RptItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
				{
					shownCount++;
					LoadVideo(e.Item);
				}
				else if (e.Item.ItemType == ListItemType.Separator)
				{
					if (itemsPerRow > 0)
					{
						if ((shownCount % itemsPerRow) != 0)
							e.Item.Visible = false;
					}
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			ID templateID = new ID("{7F5B38FA-4F35-4562-A47D-A7B17C81E9CF}");
			List<Item> list = base.CurrentItem.Children.
				Where(i => i.TemplateID == templateID && !string.IsNullOrEmpty(i["Video URL"]) && i["Hidden"] != "1").ToList();

			if (list == null || list.Count == 0)
			{
				pnlVideoItems.Visible = false;
			}
			else
			{
				string itemValue = CurrentItem["Items Per Row"];
				int.TryParse(itemValue, out itemsPerRow);

				itemValue = CurrentItem["Custom CSS Class"];
				if (!string.IsNullOrEmpty(itemValue))
					pnlVideoItems.CssClass = itemValue;

				rptItems.DataSource = list;
				rptItems.DataBind();
			}
		}

		private void LoadVideo(RepeaterItem rptItem)
		{
			Item videoItem = (Item)rptItem.DataItem;

			HtmlVideo vidControl = (HtmlVideo)rptItem.FindControl("vidControl");
			Sitecore.Data.Fields.ImageField videoImage = videoItem.Fields["Video Image"];

			if (videoImage == null || videoImage.MediaItem == null)
				vidControl.Poster = string.Empty;
			else
				vidControl.Poster = MediaManager.GetMediaUrl(videoImage.MediaItem);

			HtmlSource srcControl = (HtmlSource)rptItem.FindControl("srcControl");
			LinkField videoUrl = videoItem.Fields["Video URL"];

			if (string.IsNullOrWhiteSpace(videoUrl.Url))
				srcControl.Src = string.Empty;
			else
				srcControl.Src = videoUrl.Url;
		}

		#endregion
	}
}
