﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Enroll.ascx.cs" Inherits="Website.Sublayouts.Enroll" %>
<%@ Register TagPrefix="cm" TagName="Grid" Src="~/Sublayouts/GridLayout.ascx" %>

<!-- Begin SubLayouts/Widgets/Enroll -->
<asp:UpdatePanel ID="pnlUpdate" runat="server">
	<ContentTemplate>
		<div class="row">
			<div class="col-xs-12 col-md-10 panel-hero">
				<div class="row">
					<div class="col-xs-12 col-md-4">
						<div class="row">
							<div class="col-xs-12">
								<h2><sc:Text ID="txtHeadline" runat="server" Field="Headline" /></h2>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<h4><sc:Text ID="txtSubHeadline" runat="server" Field="SubHeadline" /></h4>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-5">
						<div class="row">
							<div class="col-xs-12">&nbsp;
							</div>
						</div>
						<div class="row top-spacer">
							<div class="col-xs-12">
								<div class="form-horizontal">
									<div class="form-group">
										<div class="col-xs-12 col-md-9">
											<cm:DropDownLocation ID="ddlCounty" runat="server" ClientIDMode="Static" CssClass="form-control" />
										</div>
										<div class="col-xs-12 col-md-3">
											<asp:RequiredFieldValidator ID="reqValCountyRequired" runat="server" ControlToValidate="ddlCounty"
												InitialValue="" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-xs-12 col-md-9">
											<cm:DropDownPlan ID="ddlPlan" runat="server" ClientIDMode="Static" CssClass="form-control" />
										</div>
										<div class="col-xs-12 col-md-3">
											<asp:RequiredFieldValidator ID="reqValPlanRequired" runat="server" ControlToValidate="ddlPlan"
												InitialValue="" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-3">
						<div class="row">
							<div class="col-xs-12 top-spacer-double">
								<sc:Text ID="txtEnrollRightText" runat="server" Field="Enroll Right Text" />
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 top-spacer-double">
								<asp:Button ID="btnEnroll" runat="server" CssClass="btn btn-primary"
									OnClientClick="if(!validateEnroll(1)) return false;"
									OnClick="BtnEnroll_OnClick" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-10 text-center">
				<h3><cm:Text ID="txtEnrollBelowText" runat="server" Field="Enroll Below Text" /></h3>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-10">
				<cm:Grid ID="gridEnroll" runat="server" />
			</div>
		</div>
		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</ContentTemplate>
</asp:UpdatePanel>
<script type="text/javascript" >
	var prm = Sys.WebForms.PageRequestManager.getInstance();
	prm.add_pageLoaded(loadEnroll);
</script>
<!-- End SubLayouts/Widgets/Enroll -->
