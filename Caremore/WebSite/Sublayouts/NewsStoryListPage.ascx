﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="NewsStoryListPage.ascx.cs" Inherits="Website.Sublayouts.NewsStoryListPage" %>

<!-- Begin SubLayouts/NewsStoryListPage -->
<div class="row">
	<div class="col-xs-12">
<asp:ListView ID="lvFeaturedItems" runat="server">
	<LayoutTemplate>
		<div id="topNews">
			<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
		</div>
	</LayoutTemplate>
	<ItemTemplate>
		<h3>
			<sc:Date ID="dtNewsDate" runat="server" Field="Story Date" Format="MM.dd.yy" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />
			<sc:Text ID="txtNewsTitle" runat="server" Field="Title" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />
		</h3>
		<h4><sc:Text ID="txtSubTitle" runat="server" Field="Subtitle" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></h4>
		<p><asp:HyperLink ID="hlDetails" runat="server" NavigateUrl="<%# Sitecore.Links.LinkManager.GetItemUrl(Container.DataItem as Sitecore.Data.Items.Item) %>">
			<span class="glyphicon glyphicon-chevron-right"></span>
			<sc:Text ID="txtDetailsLinkLabel" runat="server" Field="Details Label" /></asp:HyperLink></p>
		<hr />
	</ItemTemplate>
</asp:ListView>

<asp:ListView ID="lvCurrentItems" runat="server" GroupItemCount="1">
	<LayoutTemplate>
		<div id="currentNews">
			<asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
		</div>
	</LayoutTemplate>
	<ItemTemplate>
		<h3><sc:Text ID="txtNewsTitle" runat="server" Field="Title" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></h3>
		<h4><strong><sc:Date ID="dtNewsDate" runat="server" Field="Story Date" Format="MM.dd.yy" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></strong>
			<sc:Text ID="txtSubTitle" runat="server" Field="Subtitle" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />
		</h4>
		<p><asp:HyperLink ID="hlDetails" runat="server" NavigateUrl="<%# Sitecore.Links.LinkManager.GetItemUrl(Container.DataItem as Sitecore.Data.Items.Item) %>">
			<span class="glyphicon glyphicon-chevron-right"></span>
			<sc:Text ID="txtDetailsLinkLabel" runat="server" Field="Details Label" /></asp:HyperLink></p>
		<hr />
	</ItemTemplate>
</asp:ListView>

<asp:ListView ID="lvAllYears" runat="server" GroupItemCount="1" OnItemDataBound="LvAllYears_ItemDataBound">
	<LayoutTemplate>
		<div class="panel-group" id="newsAccordian" role="tablist" aria-multiselectable="true">
			<asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
		</div>
	</LayoutTemplate>
	<ItemTemplate>
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="heading<%# Container.DataItemIndex %>">
				<a href="#collapse<%# Container.DataItemIndex %>" aria-controls="collapse<%# Container.DataItemIndex %>"
					aria-expanded="false" data-toggle="collapse" data-parent="#newsAaccordion" role="button">
					<div class="row">
						<div class="col-xs-6 col-md-9">
							<h3 class="panel-title"><%# ((Sitecore.Data.Items.Item)Container.DataItem).Name %></h3>
						</div>
						<div class="col-xs-6 col-md-2 col-md-offset-1 text-center"><span class="badge"><%# ((Sitecore.Data.Items.Item)Container.DataItem).Children.Count %></span></div>
					</div>
				</a>
			</div>
			<div id="collapse<%# Container.DataItemIndex %>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<%# Container.DataItemIndex %>">
				<asp:ListView ID="lvAllStories" runat="server" OnItemDataBound="LvAllStories_ItemDataBound">
					<LayoutTemplate>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-6 col-md-9 bg-primary item-spacer"><sc:Text ID="txtTitleLabel" runat="server" Field="Title Label" /></div>
								<div class="col-xs-6 col-md-2 col-md-offset-1 text-center bg-primary item-spacer"><sc:Text ID="txtDateLabel" runat="server" Field="Date Label" /></div>
							</div>
							<asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
						</div>
					</LayoutTemplate>
					<ItemTemplate>
						<div class="row">
							<div class="col-xs-6 col-md-9">
								<div class="item-spacer"><asp:HyperLink ID="hlName" runat="server">
									<sc:Text ID="txtNewsTitle" runat="server" Field="Title" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" />
								</asp:HyperLink></div>
							</div>
							<div class="col-xs-6 col-md-2 col-md-offset-1 text-center">
								<div class="item-spacer"><sc:Date ID="dtNewsDate" runat="server" Field="Story Date" Format="MM.dd.yy" Item="<%# Container.DataItem as Sitecore.Data.Items.Item %>" /></div>
							</div>
						</div>
					</ItemTemplate>
				</asp:ListView>
			</div>
		</div>
	</ItemTemplate>
</asp:ListView>

	</div>
</div>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
<!-- End SubLayouts/NewsStoryListPage -->
