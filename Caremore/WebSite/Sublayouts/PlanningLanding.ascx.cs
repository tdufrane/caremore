﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace Website.Sublayouts
{
	public partial class PlanningLanding : BaseUserControl
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			Item contentItem = base.CurrentItem.Children[CareMoreUtilities.ContentItemKey];

			if (contentItem == null)
			{
				rptPlanningGrid.Visible = false;
			}
			else
			{
				ID templateID = new ID("{5F6B9868-E92F-4821-92FE-6338FCC5A692}");
				List<Item> list = contentItem.Axes.GetDescendants()
					.Where(i => i.TemplateID == templateID).ToList();

				if (list == null || list.Count == 0)
				{
					rptPlanningGrid.Visible = false;
				}
				else
				{
					rptPlanningGrid.DataSource = list;
					rptPlanningGrid.DataBind();
				}
			}
		}

		#endregion
	}
}
