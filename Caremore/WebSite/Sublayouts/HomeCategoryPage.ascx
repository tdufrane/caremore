﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="HomeCategoryPage.ascx.cs" Inherits="Website.Sublayouts.HomeCategoryPage" %>
<%@ Register TagPrefix="cm" TagName="Grid" Src="~/Sublayouts/GridLayout.ascx" %>

<div class="row">
	<div class="col-xs-12 text-center">
		<h1><sc:Text ID="txtTitle" Field="Title" runat="server" /></h1>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 text-center">
		<sc:Text ID="txtContent" Field="Content" runat="server" />
	</div>
</div>
<div id="panel-home-grid">
	<cm:Grid ID="gridPromos" runat="server" HideItemBreak="true" />
</div>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
