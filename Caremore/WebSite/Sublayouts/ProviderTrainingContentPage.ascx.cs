﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;

namespace Website.Sublayouts
{
	public partial class ProviderTrainingContentPage : ProviderTrainingBasePage
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			if (RegistrationStatus(CurrentItem.Parent) == RegistrationStatuses.Registered)
			{
				string itemId = CurrentItem.Parent.ID.Guid.ToString();

				if ((Session[itemId] != null) && ((bool)Session[itemId]))
				{
					HtmlGenericControl div = NamingContainer.FindControl("divMainContent") as HtmlGenericControl;

					if (div == null)
					{
						this.Controls.Add(new LiteralControl(CurrentItem["Registered Message"]));
					}
					else
					{
						div.Controls.AddAt(1, new LiteralControl(CurrentItem["Registered Message"]));
					}

					Session[itemId] = false;
				}
			}
			else
			{
				Response.Redirect(CareMoreUtilities.GetItemUrl(CurrentItem.Parent), false);
			}
		}

		#endregion
	}
}
