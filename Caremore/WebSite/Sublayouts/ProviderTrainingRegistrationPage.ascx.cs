﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using System.Data.Linq;
using Sitecore.Data.Fields;

namespace Website.Sublayouts
{
	public partial class ProviderTrainingRegistrationPage : ProviderTrainingBasePage
	{
		#region Private variables

		private const string PostBackSessionName = "ProviderTrainingRegistrationPage";

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				if (Page.IsPostBack)
				{
					phError.Controls.Clear();
					phError.Visible = false;
				}
				else
				{
					LoadPage();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnAlreadyRegistered_Click(object sender, EventArgs e)
		{
			SetAlreadyRegistered();
		}

		protected void BtnRegistered_Click(object sender, EventArgs e)
		{
			try
			{
				Page.Validate("ProvTrainRegistration");

				if (Page.IsValid)
				{
					CheckEmailRegistered(txtEmail.Text);
				}
				else
				{
					CareMoreUtilities.DisplayInvalidSubmission(phError, Page.Validators, pnlForm);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				if (Session[PostBackSessionName] == null)
				{
					Page.Validate("ProvTrainRegistration");

					if (Page.IsValid && ucReCaptcha.IsValid())
					{
						SaveRequest();
						RedirectToContent();
					}
					else
					{
						CareMoreUtilities.DisplayInvalidSubmission(phError, Page.Validators, pnlForm);
					}
				}
				else
				{
					pnlForm.Visible = false;
					CareMoreUtilities.DisplayNoMultiSubmit(phError);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			Session[PostBackSessionName] = null;

			LoadForm();

			switch (RegistrationStatus(CurrentItem))
			{
				case RegistrationStatuses.Expired:
					txtReattest.Visible = true;
					SetNormalForm();
					break;

				case RegistrationStatuses.Registered:
					LoadRegistration(true);
					break;

				default:
					break;
			}
		}

		private void LoadForm()
		{
			lnkBtnAlreadyRegistered.Text = CurrentItem["Already Registered Link Text"];

			bool titleRequired = CurrentItem["Title Required"].Equals("1");
			phTitleReq.Visible = titleRequired;
			FillTitlesList();

			pnlSpecialty.Visible = CurrentItem["Specialty Visible"].Equals("1");
			FillSpecialtyList();

			pnlJobTitle.Visible = CurrentItem["Job Title Visible"].Equals("1");
			pnlProviderName.Visible = CurrentItem["Provider Name Visible"].Equals("1");
			pnlOrganization.Visible = CurrentItem["Organization Visible"].Equals("1");
			pnlLicense.Visible = CurrentItem["Practitioner License Visible"].Equals("1");

			pnlOneNumber.Visible = pnlLicense.Visible;
		}

		private void LoadRegistration(bool isFullRegisterForm)
		{
			bool infoFilled = true;

			txtFirstName.Text = base.ProviderRegistration.FirstName;
			txtLastName.Text = base.ProviderRegistration.LastName;
			ddlTitle.Text = base.ProviderRegistration.Title;
			txtEmail.Text = base.ProviderRegistration.Email;
			txtAddress.Text = base.ProviderRegistration.Address;
			txtCity.Text = base.ProviderRegistration.City;
			ddlState.SelectedValue = base.ProviderRegistration.State;
			txtZipCode.Text = base.ProviderRegistration.ZipCode;
			txtPhone.Text = base.ProviderRegistration.Phone;

			// Check each of the required; show only if originally visible and has no db value
			ddlSpecialty.SelectedValue = NullToEmpty(base.ProviderRegistration.Specialty);
			if (CurrentItem["Specialty Visible"].Equals("1"))
			{
				if (string.IsNullOrWhiteSpace(ddlSpecialty.SelectedValue))
					infoFilled = false;
				else
					pnlSpecialty.Visible = false;
			}

			txtJobTitle.Text = NullToEmpty(base.ProviderRegistration.JobTitle);
			if (CurrentItem["Job Title Visible"].Equals("1"))
			{
				if (string.IsNullOrWhiteSpace(txtJobTitle.Text))
					infoFilled = false;
				else
					pnlJobTitle.Visible = false;
			}

			txtProviderName.Text = NullToEmpty(base.ProviderRegistration.ProviderName);
			if (CurrentItem["Provider Name Visible"].Equals("1"))
			{
				if (string.IsNullOrWhiteSpace(txtProviderName.Text))
					infoFilled = false;
				else
					pnlProviderName.Visible = false;
			}

			txtOrganization.Text = NullToEmpty(base.ProviderRegistration.Organization);
			if (CurrentItem["Organization Visible"].Equals("1"))
			{
				if (string.IsNullOrWhiteSpace(txtOrganization.Text))
					infoFilled = false;
				else
					pnlOrganization.Visible = false;
			}

			txtLicense.Text = NullToEmpty(base.ProviderRegistration.PractitionerLicense);
			txtNpi.Text = NullToEmpty(base.ProviderRegistration.NPI);

			if (CurrentItem["Practitioner License Visible"].Equals("1"))
			{
				if (!string.IsNullOrWhiteSpace(txtLicense.Text))
					pnlLicense.Visible = false;

				if (string.IsNullOrWhiteSpace(txtLicense.Text) && string.IsNullOrWhiteSpace(txtNpi.Text))
					infoFilled = false;
			}
			else
			{
				pnlLicense.Visible = false;
			}

			if (!string.IsNullOrWhiteSpace(txtNpi.Text))
			{
				pnlNpi.Visible = false;
			}

			if (infoFilled)
			{
				RecordAccess(base.ProviderRegistration.RegistrationID);
				SetCookie(base.ProviderRegistration.RegistrationID.ToString());
				RedirectToContent();
			}
			else
			{
				if (isFullRegisterForm)
				{
					SetAlreadyRegistered();
				}

				txtAlready.Visible = false;
				txtMoreInfo.Visible = true;
				phRegister3.Visible = true;
			}
		}

		private void FillSpecialtyList()
		{
			Item[] items = CurrentItem.Axes.SelectItems(CareMoreUtilities.WORDING_PATH + "/Specialties//*[@@templatename='Text']");

			if (items != null && items.Length > 0)
			{
				foreach (Item item in items)
				{
					ddlSpecialty.Items.Add(new ListItem(item["Text"]));
				}
			}
		}

		private void FillTitlesList()
		{
			Item[] items = CurrentDb.SelectItems(CareMoreUtilities.WORDING_PATH + "/Medical Titles//*[@@templatename='Text']");

			if (items != null && items.Length > 0)
			{
				foreach (Item item in items)
				{
					ddlTitle.Items.Add(new ListItem(item["Text"]));
				}
			}
		}

		private void RedirectToContent()
		{
			Item article = CurrentItem.Children.Where(item => item.TemplateName == "Provider Training Article").FirstOrDefault();

			if (article == null)
			{
				CareMoreUtilities.DisplayError(phError, "No training content to display.");
			}
			else
			{
				Response.Redirect(CareMoreUtilities.GetItemUrl(article), false);
			}
		}

		private void CheckEmailRegistered(string email)
		{
			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();
			IEnumerable<ProviderTrainingRegistration> results = db.ProviderTrainingRegistrations.Where(item => (item.Email.ToLower() == email.ToLower()));

			if (results.Count() == 0)
			{
				CareMoreUtilities.DisplayError(phError, "Unable to match email entered, please check your entry and try again.");
			}
			else
			{
				RegistrationStatuses regStatus = RegistrationStatus(CurrentItem, results.First().RegistrationID);

				if (regStatus == RegistrationStatuses.Expired)
				{
					txtReattest.Visible = true;
					SetNormalForm();
				}
				else if (phRegister3.Visible)
				{
					BtnSubmit_Click(this, EventArgs.Empty);
				}
				else
				{
					LoadRegistration(false);
				}
			}
		}

		private void SaveRequest()
		{
			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();

			ProviderTrainingRegistration registration;

			IEnumerable<ProviderTrainingRegistration> results = db.ProviderTrainingRegistrations.Where(item => (item.Email.ToLower() == txtEmail.Text.ToLower()));

			if (results.Count() == 0)
			{
				registration = new ProviderTrainingRegistration();
				registration.CreatedOn = DateTime.Now;
			}
			else
			{
				registration = results.First();
				registration.ModifiedOn = DateTime.Now;
			}

			registration.FirstName = txtFirstName.Text;
			registration.LastName = txtLastName.Text;
			registration.Title = ddlTitle.Text;
			registration.Email = txtEmail.Text;
			registration.Address = txtAddress.Text;
			registration.City = txtCity.Text;
			registration.State = ddlState.SelectedValue;
			registration.ZipCode = txtZipCode.Text;
			registration.Phone = CareMoreUtilities.FormattedPhone(txtPhone.Text);
			registration.NPI = txtNpi.Text;

			if (pnlJobTitle.Visible)
				registration.JobTitle = EmptyToNull(txtJobTitle.Text);

			if (pnlProviderName.Visible)
				registration.ProviderName = EmptyToNull(txtProviderName.Text);

			if (pnlOrganization.Visible)
				registration.Organization = EmptyToNull(txtOrganization.Text);

			if (pnlSpecialty.Visible)
				registration.Specialty = EmptyToNull(ddlSpecialty.SelectedValue);

			if (pnlLicense.Visible)
				registration.PractitionerLicense = EmptyToNull(txtLicense.Text);

			registration.IPAddress = CareMoreUtilities.UserIpAddress(Request);

			if (results.Count() == 0)
				db.ProviderTrainingRegistrations.InsertOnSubmit(registration);

			db.SubmitChanges();

			RecordAccess(registration.RegistrationID);
			SetCookie(registration.RegistrationID.ToString());
		}

		private void SetAlreadyRegistered()
		{
			lnkBtnAlreadyRegistered.Visible = false;
			pnlForm.DefaultButton = btnRegistered.ID;
			txtRegister.Visible = false;
			txtAlready.Visible = true;
			txtMoreInfo.Visible = false;
			txtReattest.Visible = false;
			phRegister1.Visible = false;
			phRegister2.Visible = false;
			phRegister3.Visible = false;
			pnlAcceptance.Visible = false;
			pnlReCaptcha.Visible = false;
			btnSubmit.Visible = false;
			btnRegistered.Visible = true;
		}

		private void SetNormalForm()
		{
			pnlForm.DefaultButton = btnSubmit.ID;
			txtRegister.Visible = true;
			txtAlready.Visible = false;
			txtMoreInfo.Visible = false;
			phRegister1.Visible = true;
			phRegister2.Visible = true;
			phRegister3.Visible = true;
			pnlAcceptance.Visible = true;
			pnlReCaptcha.Visible = true;
			btnSubmit.Visible = true;
			btnRegistered.Visible = false;
		}

		private string NullToEmpty(string value)
		{
			if (value == null)
				return string.Empty;
			else
				return value;
		}

		private string EmptyToNull(string value)
		{
			if (string.IsNullOrEmpty(value))
				return null;
			else
				return value;
		}

		#endregion
	}
}
