﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using CareMore.Web.DotCom.Provider;

using Sitecore.Collections;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;

namespace Website.Sublayouts
{
	public partial class ItemListPage : BaseUserControl
	{
		#region Variables

		private string detailsBaseUrl = string.Empty;
		private string pageKey = string.Empty;
		private string pageLastStateKey = string.Empty;
		private string pageLastCountyKey = string.Empty;

		private string lastProvider = string.Empty;
		private string lastId = string.Empty;
		private string thisProvider = string.Empty;
		private string thisId = string.Empty;

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				Item detailsPage = CurrentItem.GetChildren().Where(i => i.Name == "Details").FirstOrDefault();
				detailsBaseUrl = LinkManager.GetItemUrl(detailsPage);

				pageKey = CurrentItem.TemplateID.ToString();
				pageLastStateKey = pageKey + "_State";
				pageLastCountyKey = pageKey + "_County";

				if (IsPostBack)
				{
					// Make sure previous error is hidden
					CareMoreUtilities.ClearControl(phError);
				}
				else
				{
					SetStateAndCounty(Request.QueryString["s"], Request.QueryString["c"]);
					ShowResults();
					Session[pageKey] = base.CurrentState;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayDownMessage(phError, ex, phSearchResults);
			}
		}

		protected void DdlStateList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				FillCountyList(ddlStateList.SelectedItem.Text);
				ShowResults();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayDownMessage(phError, ex, phSearchResults);
			}
		}

		protected void DdlCountyList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				ShowResults();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayDownMessage(phError, ex, phSearchResults);
			}
		}

		protected void LvItems_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListViewItemType.DataItem)
				{
					SetRowDetails(e.Item, (FacilitySearchResult)e.Item.DataItem);
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex, phSearchResults);
			}
		}

		#endregion

		#region Private methods

		private void SetStateAndCounty(string queryState, string queryCounty)
		{
			ChildList stateItems = ItemIds.GetItem("STATE_SETTINGS_ID").Children;

			// Figure out the last used State for this page
			string state;
			if (string.IsNullOrEmpty(queryState))
			{
				state = SetState(base.CurrentState, true);
			}
			else
			{
				var states = stateItems.Where(item =>
					(item["Exclude From Provider Lists"] != "1") &&
					(item["Name"].ToLower() == queryState.ToLower()));

				if (states.Count() == 1)
					state = SetState(queryState, false);
				else
					state = SetState(base.CurrentState, true);
			}

			// Load the dropdowns
			FillStateList(state);
			FillCountyList(state);

			// Figure out the last used County for this page
			if (string.IsNullOrEmpty(queryCounty))
			{
				SetCounty(null);
			}
			else
			{
				var counties = stateItems[state].Children.Where(item => item["Name"].ToLower() == queryCounty.ToLower());

				if (counties.Count() == 1)
					SetCounty(queryCounty);
				else
					SetCounty(null);
			}
		}

		private string SetState(string state, bool useLastState)
		{
			// Figure out the last used State for this page
			if (useLastState)
			{
				string lastSiteState;
				if (Session[pageKey] == null)
					lastSiteState = state;
				else
					lastSiteState = (string)Session[pageKey];

				string lastState;
				if (Session[pageLastStateKey] == null)
					lastState = state;
				else
					lastState = (string)Session[pageLastStateKey];

				if ((state.Equals(lastSiteState)) && (!lastState.Equals(state)))
				{
					state = lastState;
				}
			}

			return state;
		}

		private void SetCounty(string county)
		{
			// Try to see if can use the last County for this Page & State
			string lastCounty = string.Empty;

			if (string.IsNullOrEmpty(county))
			{
				if (Session[pageLastCountyKey] != null)
				{
					lastCounty = (string)Session[pageLastCountyKey];
				}
			}
			else
			{
				lastCounty = county;
			}

			if (ddlCountyList.Items.FindByValue(lastCounty) != null)
				ddlCountyList.SelectedValue = lastCounty;
		}

		private void FillStateList(string state)
		{
			var states = ItemIds.GetItem("STATE_SETTINGS_ID").Children.Where(item => item["Exclude From Provider Lists"] != "1");

			if (states.Count() > 0)
			{
				foreach (Item stateItem in states)
				{
					ddlStateList.Items.Add(new ListItem(stateItem["Name"], stateItem["Code"]));
				}

				if (state != null)
				{
					ddlStateList.Items.FindByText(state).Selected = true;
				}
			}
		}

		private void FillCountyList(string state)
		{
			ddlCountyList.Items.Clear();
			Item[] counties = base.CurrentDb.SelectItems(CareMoreUtilities.LOCALIZATION_PATH + "/" + state + "//*[@@templatename='County']");

			if (counties.Count() > 0)
			{
				ddlCountyList.DataSource = counties;
				ddlCountyList.DataTextField = "Name"; // "Display Text";
				ddlCountyList.DataValueField = "Name";
				ddlCountyList.DataBind();
			}
		}

		private void ShowResults()
		{
			string state = ddlStateList.SelectedItem.Text;
			string stateCode = ddlStateList.SelectedItem.Value;
			Session[pageLastStateKey] = state;

			string county = ddlCountyList.SelectedValue;
			Session[pageLastCountyKey] = county;

			List<FacilitySearchResult> searchResults = new List<FacilitySearchResult>();

			Item listing = ((InternalLinkField)CurrentItem.Fields["Provider List Type"]).TargetItem;
			string dataSource = CurrentDb.GetItem(listing["Data Source"]).Name;

			if ((dataSource.Equals("Both")) || (dataSource.Equals("Sitecore")))
			{
				List<FacilitySearchResult> sitecoreResults = LoadProvidersSitecore(CurrentDb.GetItem(listing["Sitecore Path"]), state, county);
				searchResults.AddRange(sitecoreResults);
			}

			if ((dataSource.Equals("Both")) || (dataSource.Equals("Database")))
			{
				List<FacilitySearchResult> databaseResults = LoadProvidersDatabase(listing["Class Code"], stateCode, county);
				searchResults.AddRange(databaseResults);
			}

			List<string> providerExclusions = ProviderHelper.GetExclusions(listing["Exclusions"]);

			foreach (string exclusion in providerExclusions)
			{
				IEnumerable<FacilitySearchResult> filteredList;

				if (exclusion.Contains('='))
				{
					filteredList = searchResults.Where(item => (string.Format("{0}={1}", item.ProviderId, item.AddressType.Trim()) != exclusion));
				}
				else
				{
					filteredList = searchResults.Where(item => (item.ProviderId != exclusion));
				}

				searchResults = filteredList.ToList();
			}

			LoadProviders(searchResults);
		}

		private List<FacilitySearchResult> LoadProvidersDatabase(string code, string state, string county)
		{
			return ProviderFinder.GetFacilityList(ProviderHelper.GetPortalCode(BL.SiteSettings.SiteSettingPath),
				code, null, null, null, county, state, null);
		}

		private List<FacilitySearchResult> LoadProvidersSitecore(Item listRoot, string state, string county)
		{
			Item englishItem = base.CurrentDb.GetItem(listRoot.ID, Sitecore.Globalization.Language.Parse("en"));

			IEnumerable<Item> itemList = englishItem.Axes.GetDescendants();

			IEnumerable<Item> list = from item in itemList
			                         where item["State"].Equals(state, StringComparison.OrdinalIgnoreCase) &&
			                               item["County"].Equals(county, StringComparison.OrdinalIgnoreCase)
			                         select item;

			List<FacilitySearchResult> searchResults = new List<FacilitySearchResult>();

			foreach (Item item in list)
			{
				FacilitySearchResult facility = ProviderSearchDataContext.ToFacilitySearchResult(item);
				searchResults.Add(facility);
			}

			return searchResults;
		}

		private void LoadProviders(List<FacilitySearchResult> searchResults)
		{
			if (searchResults.Count() == 0)
			{
				lvItems.DataSource = searchResults;
			}
			else
			{
				List<FacilitySearchResult> orderedList = (from item in searchResults
				                                          orderby item.City, item.Name, item.Address1
				                                          select item).ToList();

				lvItems.DataSource = orderedList;
			}

			lvItems.DataBind();
			phSearchResults.Visible = true;
		}

		private string GetFormattedMultilist(Sitecore.Data.Fields.MultilistField mf)
		{
			string list = string.Empty;
			int counter = 0;

			foreach (Item listItem in mf.GetItems())
			{
				list += listItem["Title"];

				if (counter != mf.Count - 1)
				{
					list += ", ";
					counter++;
				}
			}

			return list;
		}

		private void SetSeparator(ListViewItem control, FacilitySearchResult row)
		{
			HtmlTableCell tdEmpty = (HtmlTableCell)control.FindControl("tdEmpty");
			HtmlTableCell tdHr = (HtmlTableCell)control.FindControl("tdHr");

			if ((lastProvider.Equals(string.Empty)) || (!thisProvider.Equals(lastProvider)))
			{
				tdEmpty.Visible = false;
				tdHr.ColSpan = 4;

				lastProvider = thisProvider;
			}
			else
			{
				tdEmpty.Visible = true;
			}
		}

		private void SetRowDetails(ListViewItem control, FacilitySearchResult row)
		{
			HyperLink nameHL = (HyperLink)control.FindControl("NameHL");
			PlaceHolder addressPH = (PlaceHolder)control.FindControl("AddressPH");
			Label countyTxt = (Label)control.FindControl("CountyTxt");
			PlaceHolder detailsPH = (PlaceHolder)control.FindControl("DetailsPH");
			Label idLbl = (Label)control.FindControl("IdLbl");
			Label effectiveLbl = (Label)control.FindControl("EffectiveLbl");

			string url = string.Format(ProviderHelper.DetailsUrlFormat,
				detailsBaseUrl, row.ProviderId, row.AddressId, row.AddressType);

			thisId = row.ProviderId;
			thisProvider = row.Name;

			if ((lastProvider.Equals(string.Empty)) || (!thisProvider.Equals(lastProvider)))
			{
				nameHL.Text = row.Name;
				nameHL.NavigateUrl = url;
				nameHL.Visible = true;

				lastProvider = thisProvider;
			}
			else
			{
				nameHL.Visible = false;
			}

			Guid sitecoreId;
			if (Guid.TryParse(thisId, out sitecoreId))
			{
				idLbl.Visible = false;
			}
			else if (lastId.Equals(string.Empty) || !thisId.Equals(lastId))
			{
				idLbl.Text = "<br />ID: " + row.ProviderId;
				lastId = thisId;
			}

			if (nameHL.Visible)
			{
				ProviderHelper.SetEffectiveDate(effectiveLbl, CurrentItem["Effective Date"], row.EffectiveDate);
			}

			detailsPH.Controls.Add(new HyperLink() { Text = ItemIds.GetItemValue("VIEW_MAP", "Text"), NavigateUrl = url });

			StringBuilder address = new StringBuilder();

			address.Append(LocationHelper.GetFormattedAddress(row.Address1, row.Address2,
					row.City, row.State, row.Zip));

			if (!string.IsNullOrWhiteSpace(row.Phone))
				address.AppendFormat("<br />Ph: {0}", LocationHelper.GetFormattedPhoneNo(row.Phone));

			if (row.EffectiveDate < DateTime.Now)
				ProviderHelper.SetEffectiveDate(address, CurrentItem["Effective Date"], row.AddressEffectiveDate);

			Literal addressControl = new Literal();
			addressControl.Text = address.ToString();

			addressPH.Controls.Add(addressControl);

			countyTxt.Text = row.County;
		}

		#endregion
	}
}
