﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using Sitecore.Globalization;
using Sitecore.Data.Fields;
using Sitecore.Links;
using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using CareMore.Web.DotCom.Models.Events;

namespace Website.Sublayouts
{
	public partial class CalendarListPage : BaseUserControl
	{
		#region Private variables

		private int currentPage = 0;
		private DateTime currentTime = DateTime.Now;

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void LvItems_Load(object sender, EventArgs e)
		{
			try
			{
				if (Page.IsPostBack)
					ShowResults(0);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void LvItems_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
		{
			try
			{
				dpItems.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
				currentPage = Convert.ToInt32(e.StartRowIndex / e.MaximumRows) + 1;
				lvItems.DataBind();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				CareMoreUtilities.SetCurrentZipCode(txtZipCode.Text.Trim());
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void LvItems_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListViewItemType.DataItem)
					SetItem(e.Item);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void DpItems_PreRender(object sender, EventArgs e)
		{
			try
			{
				if (dpItems.Controls.Count > 0 )
				{
					LinkButton link = (LinkButton)dpItems.Controls[0].Controls[0];
					link.Text = "&lt;&lt;";
					link.Visible = (currentPage > 1);

					link = (LinkButton)dpItems.Controls[2].Controls[0];
					link.Visible = (currentPage * dpItems.PageSize) < dpItems.TotalRowCount;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			if (CurrentState.Equals("Virginia", StringComparison.OrdinalIgnoreCase))
				txtEventsDisclaimer.Item = Sitecore.Context.Database.GetItem(ItemIds.GetID("EVENTS_VA_DISCLAIMER"), Sitecore.Context.Language);
			else
				txtEventsDisclaimer.Item = Sitecore.Context.Database.GetItem(ItemIds.GetID("EVENTS_DISCLAIMER"), Sitecore.Context.Language);

			if (!Page.IsPostBack)
			{
				btnSubmit.Text = CurrentItem["Button Label"];
				SetMileLabels(ddlDistance);

				CareMoreUtilities.PopulateDropDownList(ddlEventType, CurrentDb.SelectSingleItem(CareMoreUtilities.EVENTS_SORTBY_PATH));

				if (Request.QueryString["evnt"] != null && Request.QueryString["evnt"].Length > 0)
				{
					int eventIndex;
					if (int.TryParse(Request.QueryString["evnt"], out eventIndex) && eventIndex < ddlEventType.Items.Count)
						ddlEventType.SelectedIndex = eventIndex;
				}

				if (Request.QueryString["dist"] != null && Request.QueryString["dist"].Length > 0)
				{
					int distIndex;
					if (int.TryParse(Request.QueryString["dist"], out distIndex) && distIndex < ddlDistance.Items.Count)
						ddlDistance.SelectedIndex = distIndex;
				}

				string currentzip = CareMoreUtilities.GetCurrentZipCode();

				if (Request.QueryString["city"] != null && Request.QueryString["city"].Length > 0)
					txtCity.Text = Server.UrlDecode(Request.QueryString["city"]);

				if (Request.QueryString["time"] != null && Request.QueryString["time"].Length == 4
					&& DateTime.Now.ToString("HHmm").CompareTo(Request.QueryString["time"]) > 0
					&& !DateTime.TryParse(DateTime.Now.ToShortDateString() + " " + Request.QueryString["time"].Substring(0, 2) + " " + Request.QueryString["time"].Substring(2, 2), out currentTime))
					currentTime = DateTime.Now;

				string submitText=Sitecore.Context.Database.GetItem(ItemIds.GetID("EVENTS_SEARCH"), Sitecore.Context.Language).Fields["Text"].Value;
				btnSubmit.Text = submitText;

				if (Request.QueryString["page"] != null && Request.QueryString["page"].Length > 0)
					int.TryParse(Request.QueryString["page"], out currentPage);

				ShowResults(currentPage);
			}
		}

		private void SetItem(ListViewItem viewItem)
		{
			ListViewDataItem lvItem = (ListViewDataItem)viewItem;
			Item thisItem = (Item)lvItem.DataItem;

			Date txtDateDt = (Date)viewItem.FindControl("txtDateDt");
			txtDateDt.Item = thisItem;

			Date txtTime = (Date)viewItem.FindControl("txtTime");
			txtTime.Item = thisItem;

			Text txtCountyTxt = (Text)viewItem.FindControl("txtCounty");
			txtCountyTxt.Item = thisItem;
			txtCountyTxt.RenderAs = Sitecore.Web.UI.RenderAs.WebControl;

			Literal litEventType = (Literal)viewItem.FindControl("litEventType");
			litEventType.Text = EventList.RenderEventType(thisItem);

			Item detailsPage = CurrentItem.Children["Details"];
			HyperLink hlName = (HyperLink)viewItem.FindControl("hlName");
			hlName.NavigateUrl = string.Format("{0}?id={1:}", LinkManager.GetItemUrl(detailsPage), thisItem.ID.Guid);
			hlName.Text = thisItem["Event Title"];

			Text txtCity = (Text)viewItem.FindControl("txtCity");
			txtCity.Item = thisItem;
			txtCity.RenderAs = Sitecore.Web.UI.RenderAs.WebControl;

			Text txtSummary = (Text)viewItem.FindControl("txtSummary");
			txtSummary.Item = thisItem;

			HyperLink hlLearnMore = (HyperLink)viewItem.FindControl("hlLearnMore");
			hlLearnMore.NavigateUrl = hlName.NavigateUrl;
		}

		private void SetMileLabels(DropDownList list)
		{
			string mileLabel = CurrentItem["Mile Label"];

			foreach (ListItem item in ddlDistance.Items)
			{
				item.Text = item.Text.Replace("mile", mileLabel);
			}
		}

		private void ShowResults(int page)
		{
			List<Item> itemList = EventList.SearchEvents(
				(ddlEventType.SelectedIndex == -1) ? null : ddlEventType.SelectedValue,
				txtZipCode.Text, txtCity.Text, CareMoreUtilities.GetCurrentLocaleCode(),
				ddlDistance.SelectedValue, null, null, 0, 0);

			lvItems.DataSource = itemList;

			if (itemList == null)
			{
				lvItems.DataBind();
			}
			else
			{
				if (page > 0 && dpItems.PageSize <= itemList.Count())
				{
					int maxPage = Convert.ToInt32(itemList.Count() / dpItems.PageSize) + 1;
					if (page > maxPage)
						page = maxPage;

					dpItems.SetPageProperties(page * dpItems.PageSize - dpItems.PageSize, dpItems.PageSize, false);
				}
				currentPage = page;

				lvItems.DataBind();

				if (itemList.Count() > 0)
				{
					if (dpItems.PageSize >= dpItems.TotalRowCount)
						dpItems.Visible = false;
					else
						dpItems.Visible = true;
				}
			}
		}

		#endregion
	}
}
