﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Sitecore.Data.Items;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Globalization;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;

using Website.BL;
using Website.BL.ListItem;

using CareMore.Web.DotCom;
using CareMore.Web.DotCom.Data;
using CareMore.Web.DotCom.Models.Events;

namespace Website.Sublayouts.Widgets
{
	public partial class EventsSearchWidget : WidgetBase
	{
		#region Private variables

		private Database currentDB = Sitecore.Context.Database;
		private string queryString = string.Empty;
		
		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void LvItems_LayoutCreated(object sender, EventArgs e)
		{
			try
			{
				SetHeader();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void LvItems_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListViewItemType.DataItem)
					SetItem(e.Item);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void LvItems_ItemCreated(object sender, ListViewItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListViewItemType.EmptyItem)
				{
					Text txtNoEventsMsg = (Text)e.Item.FindControl("txtNoEventsMsg");
					txtNoEventsMsg.Item = Item;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				CareMoreUtilities.SetCurrentZipCode(txtZipCode.Text.Trim());
				if (string.IsNullOrEmpty(txtZipCode.Text))
					ShowResults(5, 0);
				else
					ShowResults(0, 0);
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			CareMoreUtilities.PopulateDropDownList(ddlEventType, currentDB.SelectSingleItem(CareMoreUtilities.EVENTS_SORTBY_PATH));

			if (Request.QueryString["evnt"] != null && Request.QueryString["evnt"].Length > 0)
			{
				int eventIndex;
				if (int.TryParse(Request.QueryString["evnt"], out eventIndex) && eventIndex < ddlEventType.Items.Count)
					ddlEventType.SelectedIndex = eventIndex;
			}

			if (Request.QueryString["dist"] != null && Request.QueryString["dist"].Length > 0)
			{
				int distIndex;
				if (int.TryParse(Request.QueryString["dist"], out distIndex) && distIndex < ddlDistance.Items.Count)
					ddlDistance.SelectedIndex = distIndex;
			}

			txtBody.Item = Item;
			txtTitle.Item = Item;

			lnkMain.Item = Item;
			lnkMain.Text = Item["More Events Link Text"];

			txtZipCodeLabel.Item = Item;
			txtDistanceLabel.Item = Item;
			txtEventTypeLabel.Item = Item;

			string state = CareMoreUtilities.GetCurrentLocale();
			if (state.Equals("Virginia", StringComparison.OrdinalIgnoreCase))
				txtEventsDisclaimer.Item = Sitecore.Context.Database.GetItem(ItemIds.GetID("EVENTS_VA_DISCLAIMER"), Sitecore.Context.Language);
			else
				txtEventsDisclaimer.Item = Sitecore.Context.Database.GetItem(ItemIds.GetID("EVENTS_DISCLAIMER"), Sitecore.Context.Language);

			btnSubmit.Text = Item["Submit Button"];

			string currentzip = CareMoreUtilities.GetCurrentZipCode();

			if (!IsPostBack)
			{
				Page.ClientScript.RegisterClientScriptBlock(GetType(), "EventsSearchWidget",
					string.Format("$(document).ready(function () {{ __doPostBack('{0}',''); }});", btnSubmit.UniqueID),
					true);
			}
		}

		private void SetHeader()
		{
			Text txtEventDateHeader = (Text)lvItems.FindControl("txtEventDateHeader");
			Text txtEventNameHeader = (Text)lvItems.FindControl("txtEventNameHeader");
			Text txtEventCityHeader = (Text)lvItems.FindControl("txtEventCityHeader");
			Text txtEventTypeHeader = (Text)lvItems.FindControl("txtEventTypeHeader");

			txtEventDateHeader.Item = Item;
			txtEventNameHeader.Item = Item;
			txtEventCityHeader.Item = Item;
			txtEventTypeHeader.Item = Item;
		}

		private void SetItem(ListViewItem viewItem)
		{
			Item eventItem = viewItem.DataItem as Item;

			Date txtEventDate = (Date)viewItem.FindControl("txtEventDate");
			txtEventDate.Item = eventItem;

			HyperLink hlEventName = (HyperLink)viewItem.FindControl("hlEventName");
			InternalLinkField detailsPage = Item.Fields["Details Page"];
			hlEventName.NavigateUrl = string.Format("{0}?id={1:}", LinkManager.GetItemUrl(detailsPage.TargetItem), eventItem.ID.Guid);
			hlEventName.Text = eventItem["Event Title"];

			Text txtEventCity = (Text)viewItem.FindControl("txtEventCity");
			txtEventCity.Item = eventItem;

			Literal litEventType = (Literal)viewItem.FindControl("litEventType");
			litEventType.Text = EventList.RenderEventType(eventItem);
		}

		private void ShowResults(int displayCount, int pageNumber)
		{
			queryString = "?evnt=" + ddlEventType.SelectedIndex.ToString() + "&dist=" + ddlDistance.SelectedIndex.ToString();

			lvItems.DataSource = EventList.SearchEvents(
				(ddlEventType.SelectedIndex == -1) ? null : ddlEventType.SelectedValue,
				txtZipCode.Text, null, CareMoreUtilities.GetCurrentLocaleCode(),
				ddlDistance.SelectedValue, null, null,
				displayCount, pageNumber);

			lvItems.DataBind();
		}

		#endregion
	}
}
