﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="StandardWidget.ascx.cs" Inherits="Website.Sublayouts.Widgets.StandardWidget" %>

<!-- Begin Sublayouts/Widgets/StandardWidget -->
<div class="panel panel-default">
	<asp:Panel ID="pnlHeading" runat="server" CssClass="panel-heading">
		<h3><a id="aTitle" runat="server" /><cm:Text ID="txtTitleLink" runat="server" Field="Title" /></h3>
	</asp:Panel>
	<asp:Panel ID="pnlImage" runat="server" CssClass="panel-image">
		<asp:Image ID="imgWidget" runat="server" CssClass="widget-image img-responsive" />
	</asp:Panel>
	<div class="panel-body">
		<div class="widget-text">
			<cm:Text ID="ucBody" runat="server" Field="Body" />
		</div>

		<asp:Panel ID="pnlLink" runat="server" CssClass="widget-link">
			<p><asp:HyperLink ID="hlWidgetLink" runat="server"><span class="glyphicon glyphicon-chevron-right"></span> <asp:Label ID="lblWidgetLinkText" runat="server" /></asp:HyperLink></p>
		</asp:Panel>
	</div>
</div>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
<!-- End Sublayouts/Widgets/StandardWidget -->