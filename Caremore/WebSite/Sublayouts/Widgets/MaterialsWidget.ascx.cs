﻿using System;
using System.Globalization;
using System.Linq;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace Website.Sublayouts.Widgets
{
	public partial class MaterialsWidget : BaseUserControl
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			if (string.IsNullOrEmpty(CurrentItem["Redirect To"]))
			{
				ShowMaterials();
			}
			else
			{
				InternalLinkField link = (InternalLinkField)CurrentItem.Fields["Redirect To"];
				Response.Redirect(LinkManager.GetItemUrl(link.TargetItem), false);
			}
		}

		private void ShowMaterials()
		{
			ReferenceField plan = CurrentItem.Fields["Plan"];

			if (plan == null || plan.TargetItem == null)
			{
				ShowNotAvailable();
			}
			else
			{
				Item planItem = plan.TargetItem;
				LookupField planType = planItem.Fields["Plan Type"];

				if (planType == null || planItem == null)
				{
					ShowNotAvailable();
				}
				else
				{
					// /sitecore/content/Global/Regions/Arizona/Pima/2016/Breathe
					//     plan.Parent = year
					//     plan.Parent.Parent = county
					//     plan.Parent.Parent.Parent = state
					// /sitecore/content/Global/Plan Types/CareMore Breathe 2016/Arizona/Pima County

					Item planTypeItem = planType.TargetItem.Axes.GetDescendants()
						.Where(i => i["Region Name"] == planItem.Parent.Parent["Region Name"])
						.FirstOrDefault();

					if (planTypeItem == null)
					{
						ShowNotAvailable();
					}
					else
					{
						ShowMaterial(planItem, planType.TargetItem["Title"], planTypeItem);
					}
				}
			}
		}

		private void ShowMaterial(Item planItem, string planTypeName, Item planTypeItem)
		{
			string body = CurrentItem.Parent["Body"];

			if (string.IsNullOrEmpty(body))
			{
				ShowNotAvailable();
			}
			else
			{
				DateTime startDate = ((DateField)CurrentItem.Parent.Fields["Start Date"]).DateTime;
				Item formularyPdf = planTypeItem.Children.Where(i => i.Name.EndsWith("Drug Formulary PDF")).FirstOrDefault();
				Item formularyUrl = planTypeItem.Children.Where(i => i.Name.EndsWith("Drug Formulary Search")).FirstOrDefault();

				string monthDayYear;
				string monthYear;
				if (CurrentLanguage.Equals("es-mx", StringComparison.OrdinalIgnoreCase))
				{
					CultureInfo spanishInfo = new CultureInfo("es-MX");
					monthDayYear = startDate.ToString("MMMM d, yyyy", spanishInfo);
					monthYear = startDate.ToString("MMMM yyyy", spanishInfo);
				}
				else
				{
					monthDayYear = startDate.ToString("MMMM d, yyyy");
					monthYear = startDate.ToString("MMMM yyyy");
				}

				litMaterial.Text = string.Format(body,
					planTypeName, startDate.Year, monthDayYear, monthYear,
					((FileField)planItem.Fields["ANOC PDF File"]).Src,
					((FileField)planItem.Fields["Evidence PDF File"]).Src,
					CareMoreUtilities.GetItemUrl(formularyPdf, "Link"),
					CareMoreUtilities.GetItemUrl(formularyUrl, "Link"));;
			}
		}

		private void ShowNotAvailable()
		{
			CareMoreUtilities.DisplayError(phError, CurrentItem.Parent["Not Available Message"]);
		}

		#endregion
	}
}
