﻿using System;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Website.BL;

namespace Website.Sublayouts.Widgets
{
	public partial class AdobeReaderWidget : WidgetBase
	{
		#region Private variables

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			Item readerItem = null;

			if (base.Item.ID == Sitecore.Context.Item.ID)
			{
				readerItem = Sitecore.Context.Database.GetItem("{706CD812-F45F-4D3E-9BC2-CDB84DBDE6B5}");
			}
			else
			{
				readerItem = base.Item;
			}

			txtTitle.Item = readerItem;
			txtBody.Item = readerItem;
			lnkGetReader.Item = readerItem;
		}

		#endregion
	}
}
