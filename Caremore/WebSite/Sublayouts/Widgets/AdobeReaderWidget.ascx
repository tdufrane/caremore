﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="AdobeReaderWidget.ascx.cs" Inherits="Website.Sublayouts.Widgets.AdobeReaderWidget" %>

<!-- Begin Sublayouts/Widgets/AdobeReaderWidget -->
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<div class="col-xs-3">
				<img src="/images/adobe-icon.png" class="img-responsive pull-left" />
			</div>
			<h4 class="col-xs-9 no-margin-top"><sc:Text ID="txtTitle" runat="server" Field="Title" /></h4>
		</div>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-12">
				<sc:Text ID="txtBody" runat="server" Field="Body" />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 top-spacer">
				<p><cm:Link ID="lnkGetReader" runat="server" Field="Link" AddSpanTag="true" SpanCssClass="glyphicon glyphicon-chevron-right" /></p>
			</div>
		</div>
	</div>
</div>
<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
<!-- End Sublayouts/Widgets/AdobeReaderWidget -->
