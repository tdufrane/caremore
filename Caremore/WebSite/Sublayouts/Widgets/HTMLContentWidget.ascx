﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="HTMLContentWidget.ascx.cs" Inherits="Website.Sublayouts.Widgets.HTMLContentWidget" %>

<!-- Begin Sublayouts/Widgets/HTMLContentWidget -->
<asp:PlaceHolder ID="phContentWidget" runat="server">
	<sc:Text ID="txtContent" runat="server" Field="Text" />
</asp:PlaceHolder>
<!-- End Sublayouts/Widgets/HTMLContentWidget -->