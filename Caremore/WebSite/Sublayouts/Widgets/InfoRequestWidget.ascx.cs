﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Website.BL;
using CareMore.Web.DotCom;

namespace Website.Sublayouts.Widgets
{
	public partial class InfoRequestWidget : WidgetBase
	{
		#region Private variables

		private const string PostBackSessionName = "InfoRequestWidget";

		#region Properties

		#endregion

		private Item formItem = null;
		public Item FormItem
		{
			get
			{
				if (formItem == null)
				{
					LookupField form = Item.Fields["Web Form"];

					if (form != null && form.TargetItem != null)
						formItem = form.TargetItem;
				}

				return formItem;
			}
		}

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();

				if (IsPostBack)
				{
					phError.Controls.Clear();
					phError.Visible = false;
				}
				else
				{
					Session[PostBackSessionName] = null;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				if (Session[PostBackSessionName] == null)
				{
					Page.Validate("InfoRequestWidget");

					if (Page.IsValid && ucReCaptcha.IsValid())
					{
						SaveForm();
						EmailForm();
						ClearForm();

						//Set session to prevent refresh posts
						Session[PostBackSessionName] = DateTime.Now;

						phThankYouMessage.Visible = true;
						phSubmit.Visible = false;
					}
					else
					{
						CareMoreUtilities.DisplayInvalidSubmission(phError, Page.Validators);
					}
				}
				// In this case due to it being a popup, not alerting user about multiple posts
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			string required = FormItem["Required Validation"];

			cmpValEventDate.ErrorMessage = FormItem["Date Validation"];
			reqValLastName.ErrorMessage = required;
			reqValFirstName.ErrorMessage = required;
			reqValEmail.ErrorMessage = required;
			regExValEmail.ErrorMessage = FormItem["Email Validation"];
			regExValPhone.ErrorMessage = FormItem["Phone Validation"];
			regValZipCode.ErrorMessage = FormItem["Zip Code Validation"];
			regExValComments1.ErrorMessage = FormItem["Valid Text Validation"];
			regExValComments2.ErrorMessage = FormItem["Valid Text Validation"];
		}

		private void SaveForm()
		{
			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();

			InformationRequest infoRequest = new InformationRequest()
			{
				InformationRequestId = Guid.NewGuid(),
				CreatedOn = DateTime.Now,
				EventDate = CareMoreUtilities.GetDateNull(txtEventDate),
				EventName = txtEventName.Text,
				HearAboutEvent = CareMoreUtilities.GetSelectedItems(cblHearAboutEvent.Items),
				CurrentDoctor = txtCurrentDoctor.Text,
				FirstName = txtFirstName.Text,
				LastName = txtLastName.Text,
				Email = txtEmail.Text,
				Phone = CareMoreUtilities.FormattedPhone(txtPhone.Text),
				Address = txtAddress.Text,
				City = txtCity.Text,
				State = ddlState.SelectedValue,
				ZipCode = txtZipCode.Text,
				ReceiveMedicare = (rblReceiveMedicare.SelectedIndex == 0),
				CurrentMember = (rblCareMoreMember.SelectedIndex == 0),
				CurrentPlan = txtCurrentHealthPlan.Text,
				SpecialConditions = CareMoreUtilities.GetSelectedItems(cblCondition.Items),
				Comments = txtComments.Text,
				IPAddress = CareMoreUtilities.UserIpAddress(Request)
			};

			db.InformationRequests.InsertOnSubmit(infoRequest);
			db.SubmitChanges();

			// Fix for AJAX issue with modal not closing all the way
			ScriptManager.RegisterStartupScript(Page, Page.GetType(), "infoReqModal",
				"$('#infoReqModal').modal('hide');$('body').removeClass('modal-open');$('.modal-backdrop').remove();", true);
		}

		private void EmailForm()
		{
			MailMessage message = new MailMessage();
			message.From = new MailAddress(FormItem["From"]);

			Common.SetEmail(message.To, FormItem["To"]);
			Common.SetEmail(message.CC, FormItem["CC"]);

			message.Subject = FormItem["Subject"];
			message.Body = string.Format(FormItem["MailContent"],
				DateTime.Now, txtEventDate.Text, txtEventName.Text, CareMoreUtilities.GetSelectedItems(cblHearAboutEvent.Items),
				txtCurrentDoctor.Text, txtFirstName.Text, txtLastName.Text, txtEmail.Text,
				txtPhone.Text, txtAddress.Text, txtCity.Text, ddlState.SelectedValue, txtZipCode.Text,
				CareMoreUtilities.GetSelectedItems(rblReceiveMedicare.Items),
				CareMoreUtilities.GetSelectedItems(rblCareMoreMember.Items),
				txtCurrentHealthPlan.Text, CareMoreUtilities.GetSelectedItems(cblCondition.Items),
				txtComments.Text);
				
			message.IsBodyHtml = true;

			// Send email
			CareMoreUtilities.SendEmail(message);
		}

		private void ClearForm()
		{
			txtEventDate.Text = string.Empty;
			txtEventName.Text = string.Empty;
			cblHearAboutEvent.ClearSelection();
			txtCurrentDoctor.Text = string.Empty;
			txtLastName.Text = string.Empty;
			txtFirstName.Text = string.Empty;
			txtEmail.Text = string.Empty;
			txtPhone.Text = string.Empty;
			txtAddress.Text = string.Empty;
			txtCity.Text = string.Empty;
			ddlState.ClearSelection();
			txtZipCode.Text = string.Empty;
			rblReceiveMedicare.ClearSelection();
			rblCareMoreMember.ClearSelection();
			cblCondition.ClearSelection();
			txtComments.Text = string.Empty;
		}

		#endregion
	}
}
