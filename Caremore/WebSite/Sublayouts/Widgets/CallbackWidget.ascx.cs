﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Website.BL;
using CareMore.Web.DotCom;

namespace Website.Sublayouts.Widgets
{
	public partial class CallbackWidget : WidgetBase
	{
		#region Private variables

		private const string PostBackSessionName = "CallbackWidget";

		#endregion

		#region Properties

		private Item formItem = null;
		public Item FormItem
		{
			get
			{
				if (formItem == null)
				{
					LookupField form = Item.Fields["Web Form"];

					if (form != null && form.TargetItem != null)
						formItem = form.TargetItem;
				}

				return formItem;
			}
		}

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();

				if (IsPostBack)
				{
					phError.Controls.Clear();
					phError.Visible = false;
				}
				else
				{
					Session[PostBackSessionName] = null;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				if (Session[PostBackSessionName] == null)
				{
					Page.Validate("CallbackWidget");

					if (Page.IsValid && ucReCaptcha.IsValid())
					{
						SaveForm();
						EmailForm();
						ClearForm();

						//Set session to prevent refresh posts
						Session[PostBackSessionName] = DateTime.Now;

						phThankYouMessage.Visible = true;
						phSubmit.Visible = false;
					}
					else
					{
						CareMoreUtilities.DisplayInvalidSubmission(phError, Page.Validators);
					}
				}
				// In this case due to it being a popup, not alerting user about multiple posts
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			string required = FormItem["Required Validation"];
			reqValFirstName.ErrorMessage = required;
			reqValLastName.ErrorMessage = required;
			reqValZipCode.ErrorMessage = required;
			regValZipCode.ErrorMessage = FormItem["Zip Code Validation"];
			regExValPhone.ErrorMessage = FormItem["Phone Validation"];
		}

		private void SaveForm()
		{
			CaremoreApplicationDataContext db = new CaremoreApplicationDataContext();

			Contact contact = new Contact()
			{
				ContactId = Guid.NewGuid(),
				CreatedOn = DateTime.Now,
				LastName = txtLastName.Text,
				FirstName = txtFirstName.Text,
				ZipCode = txtZipCode.Text,
				Phone = CareMoreUtilities.FormattedPhone(txtPhone.Text),
				IPAddress = CareMoreUtilities.UserIpAddress(Request)
			};

			db.Contacts.InsertOnSubmit(contact);
			db.SubmitChanges();

			// Fix for AJAX issue with modal not closing all the way
			ScriptManager.RegisterStartupScript(Page, Page.GetType(), "callbackModal",
				"$('#callbackModal').modal('hide');$('body').removeClass('modal-open');$('.modal-backdrop').remove();", true);
		}

		private void EmailForm()
		{
			MailMessage message = new MailMessage();
			message.From = new MailAddress(FormItem["From"]);

			Common.SetEmail(message.To, FormItem["To"]);
			Common.SetEmail(message.CC, FormItem["CC"]);

			message.Subject = FormItem["Subject"];
			message.Body = string.Format(FormItem["MailContent"],
				txtFirstName.Text, txtLastName.Text, txtZipCode.Text, txtPhone.Text);
			message.IsBodyHtml = true;

			// Send email
			CareMoreUtilities.SendEmail(message);
		}

		private void ClearForm()
		{
			txtLastName.Text = string.Empty;
			txtFirstName.Text = string.Empty;
			txtZipCode.Text = string.Empty;
			txtPhone.Text = string.Empty;
		}

		#endregion
	}
}
