﻿using System;
using Sitecore.Data;

namespace Website.Sublayouts.Widgets
{
	public partial class HTMLContentWidget : BaseUserControl
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			ID templateID = new ID("{CD70E2DF-DDCE-4485-AF28-932A858AF468}");

			if (DataSource == null || DataSource.TemplateID != templateID || string.IsNullOrEmpty(DataSource["Text"]))
			{
				phContentWidget.Visible = false;
			}
			else
			{
				phContentWidget.Visible = true;
				txtContent.Item = DataSource;
			}
		}
	}
}