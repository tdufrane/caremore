﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="CallbackWidget.ascx.cs" Inherits="Website.Sublayouts.Widgets.CallbackWidget" %>
<%@ Register src="~/Controls/ReCaptcha.ascx" tagname="ReCaptcha" tagprefix="uc1" %>
<%@ Register TagPrefix="cm" Namespace="Website.WebControls" Assembly="Website" %>

<!-- Begin Sublayouts/Widgets/CallbackWidget -->
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<div class="col-xs-3">
				<img src="/images/phone-icon.png" class="img-responsive pull-left" />
			</div>
			<h3 class="col-xs-9 no-margin-top"><%# Item["Body"] %></h3>
		</div>
	</div>
	<div class="panel-body no-padding-top">
		<div class="row bg-primary">
			<div class="col-xs-12">
				<h4><%--<cm:Text ID="phoneNum" runat="server" Field="Phone" />--%></h4>
			</div>
		</div>
		<div class="row top-spacer">
			<div class="col-xs-12">
				<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
				<asp:PlaceHolder ID="phThankYouMessage" runat="server" Visible="false">
					<%# FormItem["ThankYouMessage"] %>
				</asp:PlaceHolder>
				<asp:PlaceHolder id="phSubmit" runat="server">
					<a href="#" data-toggle="modal" data-target="#callbackModal">
						<span class="glyphicon glyphicon-chevron-right"></span>
						<%# Item["Link Text"] %></a>
					<%# Item["Footer Text"] %>
				</asp:PlaceHolder>
			</div>
		</div>
	</div>
</div>
<div id="callbackModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><%# FormItem["Title"] %></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12">
						<%# FormItem["Header Text"] %>
					</div>
				</div>
				<div class="form-horizontal">
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<div class="form-group">
								<div class="col-xs-12 col-md-8">
									<label for="txtFirstName"><%# FormItem["First Name Label"] %></label> <span class="text-danger">*</span><br />
									<asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" MaxLength="25" />
								</div>
								<div class="col-xs-12 col-md-4">
									<br class="hidden-xs" />
									<asp:RequiredFieldValidator ID="reqValFirstName" runat="server" ControlToValidate="txtFirstName"
										SetFocusOnError="true" ValidationGroup="CallbackWidget" />
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6">
							<div class="form-group">
								<div class="col-xs-12 col-md-8">
									<label for="txtLastName"><%# FormItem["Last Name Label"] %></label> <span class="text-danger">*</span><br />
									<asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" MaxLength="25" />
								</div>
								<div class="col-xs-12 col-md-4">
									<br class="hidden-xs" />
									<asp:RequiredFieldValidator ID="reqValLastName" runat="server" ControlToValidate="txtLastName"
										SetFocusOnError="true" ValidationGroup="CallbackWidget" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<div class="form-group">
								<div class="col-xs-12 col-md-8">
									<label for="txtZipCode"><%# FormItem["Zip Code Label"] %></label> <span class="text-danger">*</span><br />
									<asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control" MaxLength="5" placeholder="#####" />
								</div>
								<div class="col-xs-12 col-md-4">
									<br class="hidden-xs" />
									<asp:RequiredFieldValidator ID="reqValZipCode" runat="server" ControlToValidate="txtZipCode"
										SetFocusOnError="true" ValidationGroup="CallbackWidget" />
									<asp:RegularExpressionValidator ID="regValZipCode" runat="server" ControlToValidate="txtZipCode"
										SetFocusOnError="true" ValidationGroup="CallbackWidget"
										ValidationExpression="^[0-9]{5}$" />
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6">
							<div class="form-group">
								<div class="col-xs-12 col-md-8">
									<label for="txtPhone"><%# FormItem["Phone Number Label"] %></label> <span class="text-danger">*</span><br />
									<asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" MaxLength="14" placeholder="###-###-####" />
								</div>
								<div class="col-xs-12 col-md-4">
									<br class="hidden-xs" />
									<asp:RequiredFieldValidator ID="reqValPhone" runat="server" ControlToValidate="txtPhone"
										SetFocusOnError="true" ValidationGroup="CallbackWidget" />
									<asp:RegularExpressionValidator ID="regExValPhone" runat="server" ControlToValidate="txtPhone"
										SetFocusOnError="true" ValidationGroup="CallbackWidget"
										ValidationExpression="^(\(?[2-9]\d{2}\)?)[- ]?([2-9]\d{2})[- ]?\d{4}$" />
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<label for="ucReCaptcha"><%# FormItem["Verify Label"] %></label> <span class="text-danger">*</span><br />
							<uc1:ReCaptcha ID="ucReCaptcha" runat="server" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<%# FormItem["Footer Text"] %>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"><%# FormItem["Cancel Label"] %></button>
				<asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_Click" Text='<%# FormItem["Submit Label"] %>' ValidationGroup="CallbackWidget" />
			</div>
		</div>
	</div>
</div>
<!-- End Sublayouts/Widgets/CallbackWidget -->
