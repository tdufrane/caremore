﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Website.BL.ListItem;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Website.BL;

namespace Website.Sublayouts.Widgets
{
	public partial class EnrollWidget : WidgetBase
	{
		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			if (string.IsNullOrEmpty(Item["Top Right Image"]))
				imgWidget.Visible = false;

			string hyperLink;
			string hyperLinkText;

			if (AppItem != null && AppItem.Fields["File"] != null)
			{
				FileField fileField = AppItem.Fields["File"];

				hyperLink = fileField.Src;
				hyperLinkText = Item["Plan Selected Button Text"];
			}
			else
			{
				hyperLink = "#";
				hyperLinkText = Item["No Plan Selected Button Text"];
			}

			string script = GetScript(Item);
			txtLnk.NavigateUrl = hyperLink;
			txtLnk.Text = hyperLinkText;
			txtLnk.Attributes.Add("OnClick", script);

			if (string.IsNullOrEmpty(Item["Link"]) && string.IsNullOrEmpty(hyperLinkText))
				pnlEnroll.Visible = false;
		}

		private string GetScript(Item item)
		{
			string script;

			if (AppItem != null && AppItem.Fields["File"] != null)
			{
				script = "return validateEnroll(1);";
			}
			else
			{
				script = "validateEnroll(1); return false;";
			}
			return script;
		}

		#endregion
	}
}