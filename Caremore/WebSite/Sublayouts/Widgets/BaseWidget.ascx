﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BaseWidget.ascx.cs" Inherits="Website.Sublayouts.Widgets.BaseWidget" %>

<!-- Begin Sublayouts/Widgets/BaseWidget -->
<asp:Panel ID="pnlWidget" runat="server">
	<div class="row widget-row">
		<div class="col-xs-12 widget-content">
			<sc:Placeholder ID="phItem" runat="server" />
			<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
		</div>
	</div>
</asp:Panel>
<!-- End Sublayouts/Widgets/BaseWidget -->
