﻿<%@ Control Language="C#" CodeBehind="InfoRequestWidget.ascx.cs" Inherits="Website.Sublayouts.Widgets.InfoRequestWidget" %>
<%@ Register src="~/Controls/ReCaptcha.ascx" tagname="ReCaptcha" tagprefix="uc1" %>
<%@ Register TagPrefix="cm" Namespace="Website.WebControls" Assembly="Website" %>

<!-- Begin Sublayouts/Forms/InformationRequest -->
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".input-group.date").datepicker({ orientation: "top" });
	});
</script>
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<div class="col-xs-2 col-md-3">
				<img src="/images/i-icon.png" class="img-responsive pull-left" />
			</div>
			<h3 class="col-xs-10 col-md-9 no-margin-top"><cm:Text ID="txtTitleLink" runat="server" Field="Title" /></h3>
		</div>
	</div>
	<div class="panel-body">
		<div class="col-xs-12">
			<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
			<asp:PlaceHolder ID="phThankYouMessage" runat="server" Visible="false">
				<%# FormItem["ThankYouMessage"] %>
			</asp:PlaceHolder>
			<asp:PlaceHolder ID="phSubmit" runat="server">
				<cm:Text ID="ucBody" runat="server" Field="Body" />
				<p><a href="#" data-toggle="modal" data-target="#infoReqModal">
					<span class="glyphicon glyphicon-chevron-right"></span>
					<%# Item["Link Text"] %></a></p>
			</asp:PlaceHolder>
		</div>
	</div>
</div>
<div id="infoReqModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><%# FormItem["Title"] %></h4>
			</div>
			<div class="modal-body">
				<asp:Panel ID="pnlForm" runat="server" CssClass="form-horizontal" DefaultButton="btnSubmit">
					<div class="form-group">
						<label for="txtDate" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtDateLabel" runat="server" Field="Date Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-4">
							<div class="input-group date">
								<asp:TextBox ID="txtEventDate" runat="server" CssClass="form-control" MaxLength="50" TextMode="SingleLine" />
								<span class="input-group-addon" id="date-addon1"><i class="glyphicon glyphicon-th"></i></span>
							</div>
							<div class="col-xs-12 col-md-offset-2 col-md-3">
								<asp:CompareValidator ID="cmpValEventDate" runat="server" ControlToValidate="txtEventDate"
									ErrorMessage="Format is incorrect" SetFocusOnError="true" ValidationGroup="InfoRequestWidget"
									Operator="DataTypeCheck" Type="Date" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="txtEvent" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtEventLabel" runat="server" Field="Event Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtEventName" runat="server" CssClass="form-control" MaxLength="100" TextMode="SingleLine" ValidationGroup="InfoRequestWidget" />
						</div>
					</div>
					<div class="form-group">
						<label for="cblHearAboutEvent" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtHearAboutEventLabel" runat="server" Field="Hear About Event Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-9">
							<asp:CheckBoxList ID="cblHearAboutEvent" runat="server" CssClass="radio" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="2" ValidationGroup="InfoRequestWidget">
								<asp:ListItem Text="Direct Mail" Value="Direct Mail" />
								<asp:ListItem Text="Website" Value="Website" />
								<asp:ListItem Text="Friend" Value="Friend" />
								<asp:ListItem Text="CareMore Care Center" Value="CareMore Care Center" />
								<asp:ListItem Text="Flyer" Value="Flyer" />
								<asp:ListItem Text="DoctorTV" Value="DoctorTV" />
								<asp:ListItem Text="Sales " Value="Sales " />
								<asp:ListItem Text="Flyer" Value="Flyer" />
								<asp:ListItem Text="Newspaper" Value="Newspaper" />
								<asp:ListItem Text="Radio" Value="Radio" />
							</asp:CheckBoxList>
						</div>
					</div>
					<div class="form-group">
						<label for="txtCurrentDoctor" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtCurrentDoctorLabel" runat="server" Field="Current Doctor Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtCurrentDoctor" runat="server" CssClass="form-control" MaxLength="50" TextMode="SingleLine" />
						</div>
					</div>
					<div class="form-group">
						<label for="txtFirstName" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtFirstNameLabel" runat="server" Field="First Name Label" Item="<%# FormItem %>" /> <span class="text-danger">*</span></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" MaxLength="25" TextMode="SingleLine" />
						</div>
						<div class="col-xs-12 col-md-3">
							<asp:RequiredFieldValidator id="reqValFirstName" runat="server" ControlToValidate="txtFirstName"
								SetFocusOnError="true" ValidationGroup="InfoRequestWidget" />
						</div>
					</div>
					<div class="form-group">
						<label for="txtLastName" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtLastNameLabel" runat="server" Field="Last Name Label" Item="<%# FormItem %>" /> <span class="text-danger">*</span></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" MaxLength="25" TextMode="SingleLine" />
						</div>
						<div class="col-xs-12 col-md-3">
							<asp:RequiredFieldValidator id="reqValLastName" runat="server" ControlToValidate="txtLastName"
								SetFocusOnError="true" ValidationGroup="InfoRequestWidget" />
						</div>
					</div>
					<div class="form-group">
						<label for="txtEmail" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtEmailLabel" runat="server" Field="Email Label" Item="<%# FormItem %>" /> <span class="text-danger">*</span></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="100" ValidationGroup="InfoRequestWidget" />
						</div>
						<div class="col-xs-12 col-md-3">
							<asp:RequiredFieldValidator id="reqValEmail" runat="server" ControlToValidate="txtEmail" OnServerValidate="ReqValEmail_OnServerValidate"
								SetFocusOnError="true" ValidationGroup="InfoRequestWidget" />
							<asp:RegularExpressionValidator ID="regExValEmail" runat="server" ControlToValidate="txtEmail"
								SetFocusOnError="true" ValidationGroup="InfoRequestWidget"
								ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
						</div>
					</div>
					<div class="form-group">
						<label for="txtPhone" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtPhoneNumberLabel" runat="server" Field="Phone Number Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" MaxLength="14" TextMode="SingleLine" placeholder="###-###-####" />
						</div>
						<div class="col-xs-12 col-md-3">
							<asp:RegularExpressionValidator ID="regExValPhone" runat="server" ControlToValidate="txtPhone"
								SetFocusOnError="true" ValidationGroup="InfoRequestWidget"
								ValidationExpression="^(\(?[2-9]\d{2}\)?)[- ]?([2-9]\d{2})[- ]?\d{4}$" />
						</div>
					</div>
					<div class="form-group">
						<label for="txtAddress" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtAddressLabel" runat="server" Field="Address Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" MaxLength="100" />
						</div>
					</div>
					<div class="form-group">
						<label for="txtCity" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtCityLabel" runat="server" Field="City Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtCity" runat="server" CssClass="form-control" MaxLength="50" />
						</div>
					</div>
					<div class="form-group">
						<label for="ddlState" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtStateLabel" runat="server" Field="State Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:DropDownList ID="ddlState" runat="server" CssClass="form-control">
								<asp:ListItem Text="-select one-" Value="" />
								<asp:ListItem Text="Arizona" Value="Arizona" />
								<asp:ListItem Text="California" Value="California" />
								<asp:ListItem Text="Nevada" Value="Nevada" />
							</asp:DropDownList>
						</div>
					</div>
					<div class="form-group">
						<label for="txtZipCode" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtZipCodeLabel" runat="server" Field="Zip Code Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-4">
							<asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control" MaxLength="5" placeholder="#####" />
						</div>
						<div class="col-xs-12 col-md-offset-2 col-md-3">
							<asp:RegularExpressionValidator ID="regValZipCode" runat="server" ControlToValidate="txtZipCode"
								SetFocusOnError="true" ValidationGroup="InfoRequestWidget"
								ValidationExpression="^[0-9]{5}$" />
						</div>
					</div>
					<div class="form-group">
						<label for="rblReceiveMedicare" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtReceiveMedicareLabel" runat="server" Field="Receive Medicare Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:RadioButtonList ID="rblReceiveMedicare" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
								<asp:ListItem Text="Yes" Value="Yes" />
								<asp:ListItem Text="No" Value="No" Selected="True" />
							</asp:RadioButtonList>
						</div>
					</div>
					<div class="form-group">
						<label for="rblCareMoreMember" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtCareMoreMemberLabel" runat="server" Field="Current Member Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:RadioButtonList ID="rblCareMoreMember" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
								<asp:ListItem Text="Yes" Value="Yes" />
								<asp:ListItem Text="No" Value="No" Selected="True" />
							</asp:RadioButtonList>
						</div>
					</div>
					<div class="form-group">
						<label for="txtCurrentHealthPlan" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtCurrentHealthPlanLabel" runat="server" Field="Current Health Plan Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtCurrentHealthPlan" runat="server" CssClass="form-control" MaxLength="100" TextMode="SingleLine" />
						</div>
					</div>
					<div class="form-group">
						<label for="cblCondition" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtConditionLabel" runat="server" Field="Condition Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-9">
							<asp:CheckBoxList ID="cblCondition" runat="server" CssClass="radio" RepeatDirection="Vertical" RepeatLayout="Flow">
								<asp:ListItem Text="Diabetes" Value="Diabetes" />
								<asp:ListItem Text="Cardiovascular Disorder / Chronic Heart Failure" Value="Cardiovascular Disorder / Chronic Heart Failure" />
								<asp:ListItem Text="Chronic Lung Disorder" Value="Chronic Lung Disorder" />
								<asp:ListItem Text="ESRD" Value="ESRD" />
							</asp:CheckBoxList>
						</div>
					</div>
					<div class="form-group">
						<label for="txtComments" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtCommentLabel" runat="server" Field="Comments Label" Item="<%# FormItem %>" /></label>
						<div class="col-xs-12 col-md-6">
							<asp:TextBox ID="txtComments" runat="server" CssClass="form-control" Rows="6" TextMode="MultiLine" />
						</div>
						<div class="col-xs-12 col-md-3">
							<asp:RegularExpressionValidator ID="regExValComments1" runat="server" ControlToValidate="txtComments"
								SetFocusOnError="true" ValidationGroup="InfoRequestWidget"
								ValidationExpression="^[^<>]+$" />
							<asp:RegularExpressionValidator ID="regExValComments2" runat="server" ControlToValidate="txtComments"
								SetFocusOnError="true" ValidationGroup="InfoRequestWidget"
								ValidationExpression="(?!(^(ht|f)tp$)).*" />
						</div>
					</div>
					<div class="row">
						<label for="txtComments" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtVerifyLabel" runat="server" Field="Verify Label" Item="<%# FormItem %>" /> <span class="text-danger">*</span></label>
						<div class="col-xs-12 col-md-6">
							<%--<uc1:ReCaptcha ID="ucReCaptcha" runat="server" />--%>
						</div>
					</div>
				</asp:Panel>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"><%# FormItem["Cancel Label"] %></button>
				<asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="BtnSubmit_Click" Text='<%# FormItem["Submit Label"] %>' ValidationGroup="InfoRequestWidget" />
			</div>
		</div>
	</div>
</div>
<!-- End Sublayouts/Forms/InformationRequest -->
