﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="PlanMaterials.ascx.cs" Inherits="Website.Sublayouts.PlanMaterials" %>
<%@ Register src="Widgets/AdobeReaderWidget.ascx" tagname="AdobeReaderWidget" tagprefix="uc1" %>

<!-- Begin SubLayouts/PlanMaterials -->
<div class="row">
	<div class="col-xs-12">
		<div id="row">
			<div class="form-horizontal">
				<div class="form-group">
					<label for="drpCounty" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtChooseCounty" runat="server" Field="Choose County Label" /></label>
					<div class="col-xs-12 col-md-5">
						<cm:DropDownLocation ID="ddlCounty" runat="server" ClientIDMode="Static" CssClass="form-control" OnSelectedIndexChanged="DdlCounty_SelectedIndexChanged" />
					</div>
					<div class="col-xs-12 col-md-3">
						<asp:RequiredFieldValidator ID="reqValCountyRequired" runat="server" ControlToValidate="ddlCounty" InitialValue="" />
					</div>
				</div>
				<div class="form-group">
					<label for="drpPlan" class="col-xs-12 col-md-3 control-label"><sc:Text ID="txtChoosePlan" runat="server" Field="Choose Plan Label" /></label>
					<div class="col-xs-12 col-md-5">
						<cm:DropDownPlan ID="ddlPlan" runat="server" ClientIDMode="Static" CssClass="form-control" OnSelectedIndexChanged="DdlPlan_SelectedIndexChanged" />
					</div>
					<div class="col-xs-12 col-md-3">
						<asp:RequiredFieldValidator ID="reqValPlanRequired" runat="server" ControlToValidate="ddlPlan" InitialValue="" />
					</div>
				</div>
			</div>
		</div>

		<hr />

		<asp:PlaceHolder ID="phPlanInfo" runat="server" Visible="false">
			<div class="row">
				<h2 class="col-xs-12"><cm:Text ID="txtPlanTitle" Field="Title" runat="server" /></h2>
			</div>

			<div class="row">
				<asp:Repeater ID="rptLinks" runat="server">
					<HeaderTemplate>
						<ul class="list-group col-xs-12 col-md-7">
					</HeaderTemplate>
					<ItemTemplate>
							<li class="list-group-item"><a href="<%# Eval("Url") %>" target="<%# Eval("Target") %>"><span class="glyphicon glyphicon-chevron-right"></span> <%# Eval("Text") %></a></li>
					</ItemTemplate>
					<FooterTemplate>
						</ul>
					</FooterTemplate>
				</asp:Repeater>

				<div class="col-xs-12 col-md-offset-1 col-md-4">
					<uc1:AdobeReaderWidget ID="ucAdobeReaderWidget" runat="server" />
				</div>
			</div>

			<hr />

			<div class="row">
				<div class="col-xs-12">
					<cm:Text ID="txtDescription" Field="Description" runat="server" />
				</div>
			</div>

			<hr />
		</asp:PlaceHolder>

		<div class="row">
			<div class="col-xs-12">
				<sc:Text ID="txtPharmacyNote" runat="server" Field="Pharmacy Note" />
			</div>
		</div>

		<asp:PlaceHolder ID="phError" runat="server" Visible="false"></asp:PlaceHolder>
	</div>
</div>
