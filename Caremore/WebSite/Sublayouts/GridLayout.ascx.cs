﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CareMore.Web.DotCom;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;
using System.Web.UI.WebControls;
using ImageField = Sitecore.Data.Fields.ImageField;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Website.BL.ListItem;
using Website.Sublayouts.Widgets;

namespace Website.Sublayouts
{
	public partial class GridLayout : BaseUserControl
	{
		#region Private variables

		public Item AppItem { get; set; }
		public bool HideItemBreak { get; set; }

		#endregion

		#region Page events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			try
			{
				LoadPage();
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RptRows_OnItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
				{
					RowItem currentRow = e.Item.DataItem as RowItem;
					Repeater rptColumns = e.Item.FindControl("rptColumns") as Repeater;

					Panel pnlRow = e.Item.FindControl("pnlRow") as Panel;
					if (string.IsNullOrEmpty(currentRow.CssClass))
						pnlRow.CssClass = "row";
					else
						pnlRow.CssClass = currentRow.CssClass;

					HtmlGenericControl hrSeparator = e.Item.FindControl("hrSeparator") as HtmlGenericControl;
					if (currentRow.ShowSeparator)
						hrSeparator.Visible = true;
					else
						hrSeparator.Visible = false;

					rptColumns.DataSource = currentRow.Columns;
					rptColumns.DataBind();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RptColumns_OnItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
				{
					ColumnItem currentCol = e.Item.DataItem as ColumnItem;
					Panel pnlColumn = e.Item.FindControl("pnlColumn") as Panel;

					if (string.IsNullOrEmpty(currentCol.CssClass))
						pnlColumn.CssClass = "col-xs-12";
					else
						pnlColumn.CssClass = currentCol.CssClass;

					Repeater rptItems = e.Item.FindControl("rptItems") as Repeater;
					rptItems.DataSource = currentCol.Items;
					rptItems.DataBind();
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		protected void RptItems_OnItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			try
			{
				if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
				{
					DetailItem detailItem = e.Item.DataItem as DetailItem;
					BaseWidget widget = e.Item.FindControl("ucWidget") as BaseWidget;

					widget.DetailItem = detailItem;
					widget.AppItem = AppItem;
				}
			}
			catch (Exception ex)
			{
				CareMoreUtilities.DisplayError(phError, ex);
			}
		}

		#endregion

		#region Private methods

		private void LoadPage()
		{
			rptRows.DataSource = GetRows();
			rptRows.DataBind();
		}

		private List<RowItem> GetRows()
		{
			List<RowItem> rows = new List<RowItem>();

			Item contentItem = CurrentItem.Children[CareMoreUtilities.ContentItemKey];

			if (contentItem != null)
			{
				foreach (Item rowItem in contentItem.Children)
				{
					rows.Add(GetRow(rowItem));
				}
			}

			return rows;
		}

		private RowItem GetRow(Item row)
		{
			RowItem currentRow = new RowItem();
			currentRow.CssClass = row["CSS Class"];
			currentRow.ShowSeparator = (row["Show Separator"] == CareMoreUtilities.CheckedTrueKey);

			foreach (Item column in row.Children)
			{
				ColumnItem currentColumn = new ColumnItem();
				currentColumn.CssClass = column["CSS Class"];

				if (ItemIds.IsTemplateBase(column, "BASE_WIDGET_TEMPLATE_ID"))
				{
					currentColumn.Items.Add(GetDetailItem(column, column));
				}
				else
				{
					currentColumn.Items = GetDetailItems(column);
				}

				currentRow.Columns.Add(currentColumn);
			}

			return currentRow;
		}

		private List<DetailItem> GetDetailItems(Item column)
		{
			List<DetailItem> list = new List<DetailItem>();

			foreach (Item child in column.Children)
			{
				list.Add(GetDetailItem(child, column));
			}

			if (list.Count() > 0)
			{
				list.Last().HideRepeater = true;
			}

			return list;
		}

		private DetailItem GetDetailItem(Item item, Item column)
		{
			DetailItem detailItem = new DetailItem(item);

			detailItem.ColumnItem = column;
			detailItem.HideRepeater = HideItemBreak;

			return detailItem;
		}

		#endregion
	}
}
