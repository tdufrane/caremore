﻿using System;
using System.Text;
using System.Collections.Generic;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Shell.Applications.ContentEditor;
using Sitecore.Web.UI.Sheer;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Web.UI.HtmlControls.Data;

namespace Sitecore.Shell.Applications.ContentEditor 
{
    public class FieldValueLookup : Lookup 
    {
        protected override void OnLoad(EventArgs args) 
        { 
            if (!Sitecore.Context.ClientPage.IsEvent) 
            {
                Item contextItem = Sitecore.Context.ContentDatabase.Items[this.ItemID];
                foreach (TemplateFieldItem tfItem in contextItem.Template.OwnFields)
                {
                    if (tfItem.Source == this.Source)
                    {
                        FieldName = tfItem.InnerItem.Fields["Field Name"].Value;
                    }
                }

            }
            base.OnLoad(args);

        }

        protected override string GetItemValue(Item item)
        {
            return Sitecore.Context.ContentDatabase.GetItem(item.ID)[this.FieldName];
        }

        protected override Item[] GetItems(Item current)
        {
            Item[] items = base.GetItems(current);
            List<Item> filteredItems = new List<Item>();
            List<string> filteredValues = new List<string>();
            string value = null;

            foreach (Item item in items)
            {
                value = this.GetItemValue(item);
                if (!filteredValues.Contains(value))
                {
                    filteredValues.Add(value);
                    filteredItems.Add(item);
                }
            }

            return filteredItems.ToArray();
            //return base.GetItems(current);
        }
    }
}