﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security.AntiXss;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using CareMore.Web.DotCom;

using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Data.Managers;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Links;
using Sitecore.Resources.Media;
using Sitecore.Security.Accounts;
using Sitecore.Security.AccessControl;
using Sitecore.Web.UI.WebControls;

namespace Website
{
/// <summary>
/// All the common functionalities, definitions are defined in this class that are usd throughout the site
/// </summary>
	public static class CareMoreUtilities
	{
		#region Key Constants

		public const string LocalLanguageKey = "LocalLanguage";
		public const string LocalizationStateSelectedKey = "LocalizationStateSelected";
		public const string LocalStateKey = "LocalState";
		public const string LocalStateCodeKey = "LocalStateCode";

		public const string ContentItemKey = "Content";
		public const string LanguageQuery = "sc_lang";
		public const string EnglishKey = "en";
		public const string SpanishKey = "es-mx";

		public const string LastCountySelectedKey = "LastCountySelected";
		public const string LastStateSelectedKey = "LastStateSelected";
		public const string LastZipEnteredKey = "LastZipEntered";

		public const string IsNotLocalizedKey = "IsNotLocalized";
		public const string LocalizedForStateKey = "LocalizedForState";

		public const string CheckedTrueKey = "1";

		public const string NoValue = "-";

		#endregion

		#region Sitecore Path Constants

		public const string HOME_PATH = "/sitecore/content/Home";
		public const string GLOBAL_PATH = "/sitecore/content/Global";
		public const string MEDIA_PATH = "/sitecore/Media Library";
		public const string DUALS_GLOBAL_PATH = "/sitecore/content/Duals/Global";

		public const string EVENTS_SORTBY_PATH = GLOBAL_PATH + "/Events/Sort By";
		public const string REGIONS_PATH = GLOBAL_PATH + "/Regions";
		public const string WORDING_PATH = GLOBAL_PATH + "/Wording";
		public const string LOCALIZATION_PATH = GLOBAL_PATH + "/LocalizationStates";
		public const string BROKER_SESSION = "BrokerRegistration";
		public const string PLANS_FOLDER = HOME_PATH + "/Plans";

		#endregion

		#region Locale Methods

		public static string GetCurrentLocale()
		{
			HttpSessionState session = HttpContext.Current.Session;
			HttpRequest request = HttpContext.Current.Request;

			string currentLocale = (session[LocalStateKey] == null) ? "California" : session[LocalStateKey].ToString();

			if (session[LocalStateKey] == null)
			{
				if (request.Cookies[LocalStateKey] != null)
				{
					string cookieValue = request.Cookies[LocalStateKey].Value;
					if ((!string.IsNullOrEmpty(cookieValue)) &&
						(!currentLocale.Equals(cookieValue, StringComparison.OrdinalIgnoreCase)))
					{
						SetState(cookieValue, true);
					}
				}
			}

			string localeSetting;
			if (session[LocalStateKey] == null)
			{
				localeSetting = currentLocale;
				SetState(currentLocale, true);
			}
			else
			{
				localeSetting = session[LocalStateKey].ToString();
			}

			return localeSetting;
		}

		public static Item GetCurrentLocaleItem()
		{
			string path = string.Format("{0}//*[@@templatename='State' and @Name='{1}']",
				CareMoreUtilities.LOCALIZATION_PATH, CareMoreUtilities.GetCurrentLocale());

			return Sitecore.Context.Database.SelectSingleItem(path);
		}

		public static string GetCurrentLocaleCode()
		{
			HttpSessionState session = HttpContext.Current.Session;
			if (session[LocalStateCodeKey] == null)
			{
				GetCurrentLocale();
			}

			return (string)session[LocalStateCodeKey];
		}

//		public static void SetCurrentLocale(HttpSessionState session, string locale)
//		{
//			Item newLocale = Sitecore.Context.Database.SelectSingleItem(LOCALIZATION_PATH + "/" + locale);
//
//			if (newLocale != null)
//			{
//				session[LocalStateKey] = locale;
//				session[LocalStateCodeKey] = newLocale["Code"];
//			}
//		}

		public static void SetState(string newState, bool permanent)
		{
			HttpSessionState session = HttpContext.Current.Session;
			HttpRequest request = HttpContext.Current.Request;
			HttpResponse response = HttpContext.Current.Response;

			// Clear state & county from plan finder page
			string oldState = (string)session[LocalStateKey];
			Item newLocale = Sitecore.Context.Database.SelectSingleItem(LOCALIZATION_PATH + "/" + newState);

			if (newLocale != null && !newState.Equals(oldState, StringComparison.OrdinalIgnoreCase))
			{
				session.Remove(LastCountySelectedKey);
				session.Remove(LastStateSelectedKey);
				session.Remove(LastZipEnteredKey);
				session.Remove("LastPlanFilter");

				if (request.Cookies[LocalStateKey] != null)
				{
					request.Cookies.Remove(LocalStateKey);
					response.Cookies.Remove(LocalStateKey);
				}

				session[LocalStateKey] = newState;
				session[LocalStateCodeKey] = newLocale["Code"];
				session[LastStateSelectedKey] = newState;
			}

			HttpCookie  aCookie = new HttpCookie(LocalStateKey);
			aCookie.Value = AntiXssEncoder.HtmlEncode(newState, false); // Neutralization of CRLF
			if (permanent)
				aCookie.Expires = DateTime.Now.AddYears(100);
			response.Cookies.Add(aCookie);
		}

		#endregion

		#region Language methods

		public static string GetCurrentLanguage()
		{
			Language currentLanguage = Sitecore.Context.Language;

			HttpSessionState session = HttpContext.Current.Session;
			HttpRequest request = HttpContext.Current.Request;

			if (!string.IsNullOrEmpty(request.QueryString[LanguageQuery]))
			{
				string queryLanguage = request.QueryString[LanguageQuery];
				if (!queryLanguage.Equals(currentLanguage.Name, StringComparison.OrdinalIgnoreCase))
				{
					SetLanguage(queryLanguage, true);
				}
				else if ((session["LocalLanguage"] != null) && 
						 (!queryLanguage.Equals(session[LocalLanguageKey].ToString(), StringComparison.OrdinalIgnoreCase)))
				{
					SetLanguage(queryLanguage, true);
				}
			}

			if (session[LocalLanguageKey] == null)
			{
				if (request.Cookies[LocalLanguageKey] != null)
				{
					string cookieValue = request.Cookies[LocalLanguageKey].Value;
					if ((!string.IsNullOrEmpty(cookieValue)) &&
						(!currentLanguage.Name.Equals(cookieValue, StringComparison.OrdinalIgnoreCase)))
					{
						SetLanguage(cookieValue, true);
					}
				}
			}

			string localLanguage;
			if (session[LocalLanguageKey] == null)
			{
				localLanguage = currentLanguage.Name;
				session[LocalLanguageKey] = currentLanguage.Name;
			}
			else
			{
				localLanguage = session[LocalLanguageKey].ToString();
			}

			return localLanguage;
		}

		public static Item GetEnglishItem(Item item)
		{
			Item englishItem;

			if (GetCurrentLanguage().Equals(CareMoreUtilities.SpanishKey, StringComparison.OrdinalIgnoreCase))
				englishItem = Sitecore.Context.Database.GetItem(item.ID, Language.Parse(CareMoreUtilities.EnglishKey));
			else
				englishItem = item;

			return englishItem;
		}

		public static Item GetLanguageItem(Item item)
		{
			if (GetCurrentLanguage().Equals(EnglishKey, StringComparison.OrdinalIgnoreCase))
				return item;
			else
				return Sitecore.Context.Database.GetItem(item.ID, Sitecore.Context.Language);
		}

		public static void SetLanguage(string newLanguage, bool permanent)
		{
			HttpSessionState session = HttpContext.Current.Session;
			HttpRequest request = HttpContext.Current.Request;
			HttpResponse response = HttpContext.Current.Response;

			string oldLanguage = (string)session[LocalLanguageKey];

			if (!newLanguage.Equals(oldLanguage, StringComparison.OrdinalIgnoreCase))
			{
				session.Remove(LocalLanguageKey);

				if (request.Cookies[LocalLanguageKey] != null)
				{
					request.Cookies.Remove(LocalLanguageKey);
					response.Cookies.Remove(LocalLanguageKey);
				}

				session[LocalLanguageKey] = newLanguage;
				Sitecore.Context.SetLanguage(LanguageManager.GetLanguage(newLanguage), true);
			}

			HttpCookie aCookie = new HttpCookie(LocalLanguageKey);
			aCookie.Value = AntiXssEncoder.HtmlEncode(newLanguage, false); // Neutralization of CRLF
			if (permanent)
				aCookie.Expires = DateTime.Now.AddYears(100);
			response.Cookies.Add(aCookie);

		}

		#endregion

		#region Methods

		public static string GetCurrentZipCode()
		{
			HttpSessionState session = HttpContext.Current.Session;
			HttpRequest request = HttpContext.Current.Request;

			string currentZipCode = (session["ZipCode"] == null) ? string.Empty : session["ZipCode"].ToString();

			if (session["ZipCode"] == null)
			{
				if (request.Cookies["ZipCode"] != null)
				{
					string cookieValue = request.Cookies["ZipCode"].Value;
					if ((!string.IsNullOrEmpty(cookieValue)) &&
						(!currentZipCode.Equals(cookieValue, StringComparison.OrdinalIgnoreCase)))
					{
						SetCurrentZipCode(cookieValue);
					}
				}
			}

			string zipCodeSetting;
			if (session["ZipCode"] == null)
			{
				zipCodeSetting = currentZipCode;
				SetCurrentZipCode(currentZipCode);
			}
			else
			{
				zipCodeSetting = session["ZipCode"].ToString();
			}

			return zipCodeSetting;
		}

		public static void SetCurrentZipCode(string zipCode)
		{
			HttpRequest request = HttpContext.Current.Request;
			HttpResponse response = HttpContext.Current.Response;

			HttpContext.Current.Session["ZipCode"] = zipCode;

			if (request.Cookies["ZipCode"] != null)
			{
				request.Cookies.Remove("ZipCode");
				response.Cookies.Remove("ZipCode");
			}

			if (!string.IsNullOrEmpty(zipCode) &&  request.Cookies["LocalState"] != null)
			{
				HttpCookie aCookie = new HttpCookie("ZipCode");
				aCookie.Value = AntiXssEncoder.HtmlEncode(zipCode, false); // Neutralization of CRLF
				aCookie.Expires = DateTime.Now.AddYears(50);
				response.Cookies.Add(aCookie);
			}
		}


		public static string GetItemDisplayName(Item thisItem)
		{
			if (!string.IsNullOrEmpty(thisItem["Navigation Title"]))
				return thisItem["Navigation Title"].Trim();
			else if (!string.IsNullOrEmpty(thisItem["Title"]))
				return thisItem["Title"].Trim();
			else if (!string.IsNullOrEmpty(thisItem["Header Title"]))
				return thisItem["Header Title"].Trim();
			else
				return thisItem.Name;
		}

		public static string GetItemDisplayName(Item thisItem, string fieldName)
		{
			if (!string.IsNullOrEmpty(thisItem[fieldName]))
				return thisItem[fieldName].Trim();
			else
				return thisItem.Name;
		}

		public static string GetItemDisplayName(Item thisItem, bool htmlEncode)
		{
			if (htmlEncode)
				return AntiXssEncoder.HtmlEncode(GetItemDisplayName(thisItem), false);
			else
				return GetItemDisplayName(thisItem);
		}


		public static string GetItemUrl(Item item)
		{
			if (item == null)
			{
				return string.Empty;
			}
			else if (string.IsNullOrEmpty(item["Url"]))
			{
				return Sitecore.Links.LinkManager.GetItemUrl(item);
			}
			else
			{
				return GetItemUrl(item, "Url");
			}
		}

		public static string GetItemUrl(Item item, string fieldName)
		{
			string url = string.Empty;

			if (item != null && !string.IsNullOrWhiteSpace(item[fieldName]))
			{
				Field field = item.Fields[fieldName];

				if (field.Type == "General Link")
				{
					LinkField link = (LinkField)field;

					if (link != null)
					{
						switch (link.LinkType)
						{
							case "internal":
							case "external":
							case "mailto":
							case "anchor":
							case "javascript":
								url = link.Url;
								break;

							case "media":
								if (link.TargetItem == null)
								{
									url = link.Url;
								}
								else
								{
									MediaItem media = new MediaItem(link.TargetItem);
									url = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(media));
								}
								break;

							default:
								break;
						}
					}
				}
				else
				{
					url = LinkManager.GetItemUrl(item);
				}
			}

			return url;
		}

		public static Item HomeItem()
		{
			return Sitecore.Context.Database.SelectSingleItem(CareMoreUtilities.HOME_PATH);
		}

		public static Item HomeItem(string state)
		{
			return GetLocalizedItem(HomeItem(), state);
		}

		#region Localized Items

		public static List<Item> GetItemsCheckState(Item parent)
		{
			ID stateTemplateId = ItemIds.GetID("STATE_TEMPLATE_ID");
			List<Item> list = new List<Item>();
			Item parentForState = null;

			foreach (Item item in parent.Children)
			{
				if (item.TemplateID.Equals(stateTemplateId))
				{
					if (item.Name == GetCurrentLocale())
					{
						parentForState = item;
					}
				}
			}

			if (parentForState == null)
			{
				list = parent.Children.Where(i => !i.TemplateID.Equals(stateTemplateId)).ToList();
			}
			else
			{
				list = parentForState.Children.ToList();
			}

			return list;
		}

		public static Item GetLocalizedItem(Item item)
		{
			string state = CareMoreUtilities.GetCurrentLocale();

			Item localizedItem = GetLocalizedItem(item, state);

			return localizedItem;
		}

		// Get the localized item based on the user state
		private static Item GetLocalizedItem(Item item, string state)
		{
			Item result = null;

			if (IsCorrectLocalizedItem(item, state, item.TemplateID))
			{
				result = item;
			}
			else if (item.Parent != null && IsCorrectLocalizedItem(item.Parent, state, item.TemplateID)) //Check parent
			{
				result = item.Parent;
			}
			else
			{
				// Check children
				result = item.Children.FirstOrDefault(p => IsCorrectLocalizedItem(p, state, item.TemplateID));

				if (result == null)
				{
					// Check siblings as long as parent isn't home
					Item parent = item.Parent;
					if (parent.ID != ItemIds.GetID("HOME"))
					{
						result = parent.Children.FirstOrDefault(p => IsCorrectLocalizedItem(p, state, item.TemplateID));
					}
				}
			}

			if (result == null)
				result = HomeItem(state);

			return result;
		}

		private static bool IsCorrectLocalizedItem(Item item, string state, ID templateId)
		{
			bool localizedForState = false;

			if (item.TemplateID == templateId)
			{
				if ((item[IsNotLocalizedKey].Equals(CheckedTrueKey)) || (string.IsNullOrWhiteSpace(item[LocalizedForStateKey])))
				{
					localizedForState = true;
				}
				else
				{
					MultilistField mlfLocalizedForState = (MultilistField)item.Fields[LocalizedForStateKey];

					foreach (Item fieldItem in mlfLocalizedForState.GetItems())
					{
						if (fieldItem["Name"] == state)
						{
							localizedForState = true;
							break;
						}
					}
				}
			}

			return localizedForState;
		}

		public static bool IsCorrectLocalizedNavItem(Item item, string state)
		{
			bool localizedForState = false;

			if ((item[IsNotLocalizedKey].Equals(CheckedTrueKey)) || (string.IsNullOrWhiteSpace(item[LocalizedForStateKey])))
			{
				localizedForState = true;
			}
			else
			{
				MultilistField mlfLocalizedForState = (MultilistField)item.Fields[LocalizedForStateKey];

				foreach (Item subItem in mlfLocalizedForState.GetItems())
				{
					if (subItem["Name"] == state)
					{
						localizedForState = true;
					}
				}
			}

			return localizedForState;
		}

		#endregion

		public static void SetListViewLink(ListViewItemEventArgs e, string linkID)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				ListViewDataItem dataItem = (ListViewDataItem)e.Item;
				Item thisItem = (Item)dataItem.DataItem;

				Link link = (Link)e.Item.FindControl(linkID);
				link.Item = thisItem;
			}
		}

		public static void SetStateItem(string stateId, string itemId, Sitecore.Web.UI.WebControls.FieldRenderer stateItem)
		{
			Item folderItem = ItemIds.GetItem(itemId);
			Item foundItem = folderItem.Children.SingleOrDefault(item => item["State"].Contains(stateId));

			if (foundItem != null)
				stateItem.Item = foundItem;
		}


		public static bool HasFieldValue(Field itemField)
		{
			bool hasValue = false;
			if (itemField != null)
			{
				if (!string.IsNullOrEmpty(itemField.Value))
					hasValue = true;
			}
			return hasValue;
		}

		public static bool HasFieldValue(Field itemField, string value)
		{
			bool matched = false;

			if (itemField != null)
			{
				if (!string.IsNullOrEmpty(itemField.Value))
				{
					if (itemField.Value.Equals(value))
						matched = true;
				}
			}

			return matched;
		}

		public static bool HasNonEmptyValue(string str)
		{
			if (str != null && str != string.Empty)
			{
				return true;
			}
			return false;
		}

		public static string GetFieldValue(Field itemField)
		{
			if (itemField != null)
			{
				return itemField.Value;
			}
			return null;
		}


		#region Address Information Rendering

		public static Placeholder GetInlineEditableAddress(Item thisItem)
		{
			Placeholder scPlaceholder = new Placeholder();

			if (CareMoreUtilities.HasFieldValue(thisItem.Fields["Place"]))
			{
				scPlaceholder.Controls.Add(new Text() { Item = thisItem, Field = "Place" });
				scPlaceholder.Controls.Add(new Literal() { Text = "<br />" });
			}

			if (CareMoreUtilities.HasFieldValue(thisItem.Fields["Address Line 1"]))
			{
				scPlaceholder.Controls.Add(new Text() { Item = thisItem, Field = "Address Line 1" });
				scPlaceholder.Controls.Add(new Literal() { Text = "<br />" });
			}

			if (CareMoreUtilities.HasFieldValue(thisItem.Fields["Address Line 2"]))
			{
				scPlaceholder.Controls.Add(new Text() { Item = thisItem, Field = "Address Line 2" });
				scPlaceholder.Controls.Add(new Literal() { Text = "<br />" });
			}

			if (HasFieldValue(thisItem.Fields["City"]) || HasFieldValue(thisItem.Fields["State"]) || HasFieldValue(thisItem.Fields["Postal Code"]))
			{
				if (HasFieldValue(thisItem.Fields["City"]))
				{
					scPlaceholder.Controls.Add(new Text() { Item = thisItem, Field = "City" });
					scPlaceholder.Controls.Add(new Literal() { Text = ", " });
				}

				if (HasFieldValue(thisItem.Fields["State"]))
				{
					scPlaceholder.Controls.Add(new Text() { Item = thisItem, Field = "State" });
					scPlaceholder.Controls.Add(new Literal() { Text = " " });
				}

				if (HasFieldValue(thisItem.Fields["Postal Code"]))
				{
					scPlaceholder.Controls.Add(new Text() { Item = thisItem, Field = "Postal Code" });
				}

				scPlaceholder.Controls.Add(new Literal() { Text = "<br />" });
			}

			return scPlaceholder;
		}


		#endregion

		public static string GetCapitalizedString(string str)
		{
			return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
		}

		public static DateTime? GetDateNull(string text)
		{
			if (string.IsNullOrEmpty(text))
			{
				return null;
			}
			else
			{
				DateTime tempDate;

				if (DateTime.TryParse(text, out tempDate))
					return tempDate;
				else
					return null;
			}
		}

		public static DateTime? GetDateNull(TextBox textBox)
		{
			return GetDateNull(textBox.Text);
		}

		public static string GetFormattedMultilist(Sitecore.Data.Fields.MultilistField mf)
		{
			string list = string.Empty;
			int counter = 0;
			foreach (Item listItem in mf.GetItems())
			{
				list += listItem.Name;
				if (counter != mf.Count - 1)
				{
					list += ", ";
					counter++;
				}
			}
			return list;
		}

		public static void PopulateDropDownList(DropDownList ddl, Item sourceFolder)
		{
			if (sourceFolder != null)
			{
				foreach (Item child in sourceFolder.Children)
				{
					if (CareMoreUtilities.HasFieldValue(child.Fields["Text"]) && child.Fields["Value"] != null)
					{
						ListItem li = new ListItem() { Text = child.Fields["Text"].Value, Value = child.Fields["Value"].Value };
						if (CareMoreUtilities.HasFieldValue(child.Fields["Selected"]) && child.Fields["Selected"].Value == CheckedTrueKey)
							li.Selected = true;
						ddl.Items.Add(li);
					}
				}
			}
		}

		public static Control FindControlRecursive(Control root, string id)
		{
			if (root.ID == id)
			{
				return root;
			}

			foreach (Control c in root.Controls)
			{
				Control t = FindControlRecursive(c, id);
				if (t != null)
				{
					return t;
				}
			}

			return null;
		}

		public static Item GetItemFromPath(string path)
		{
			return Sitecore.Context.Database.SelectSingleItem(path);
		}

		public static int CompareDates(DateTime x, DateTime y)
		{
			if (x == null)
			{
				if (y == null)
				{
					return 0;
				}
				else
				{
					return -1;
				}
			}
			else
			{
				if (y == null)
				{
					return 1;
				}
				else
				{
					int retval = x.CompareTo(y);

					return retval;
				}
			}
		}

		public static Item CreateItem(string itemName, Item parent, TemplateItem itemType, bool updateExisting)
		{
			return CreateItem(itemName, parent, itemType, updateExisting, null);
		}

		public static Item CreateItem(string itemName, Item parent, TemplateItem itemType, string findField, string findValue)
		{
			Item findItem = parent.Children.Where(i => i[findField] == findValue).FirstOrDefault();

			if (findItem == null)
				return CreateItem(itemName, parent, itemType, false, null);
			else
				return findItem;
		}

		public static Item CreateItem(string itemName, Item parent, TemplateItem itemType, bool updateExisting, AccessRuleCollection accessRules)
		{
			itemName = ExtractValidSitecoreItemName(itemName, ' ');

			if (updateExisting && parent.Children[itemName] != null)
			{
				//Updating Item
				return parent.Children[itemName];
			}
			else
			{
				//Creating Item
				Item newItem;

				using (new Sitecore.SecurityModel.SecurityDisabler())
				{
					newItem = parent.Add(itemName, itemType);

					if (accessRules != null)
						newItem.Security.SetAccessRules(accessRules);
				}

				return newItem;
			}
		}

		public static string GetFileExtension(string filename)
		{
			int index = filename.LastIndexOf('.');
			return filename.Substring(index + 1);
		}

		public static string RemoveQueryStringParameter(string originalQueryString, string parameter)
		{
			int findIndex;
			if (parameter.EndsWith("="))
				findIndex = originalQueryString.IndexOf(parameter);
			else
				findIndex = originalQueryString.IndexOf(parameter + "=");

			string queryString;
			if (findIndex == -1)
			{
				queryString = originalQueryString;  //Source={12A34C77-8215-485B-B16A-068954163207}&test=1
			}
			else
			{
				int ampersandIndex = originalQueryString.IndexOf('&', findIndex);

				if (ampersandIndex == -1)
				{
					if (findIndex == 0)
					{
						queryString = string.Empty; //sc_lang=en
					}
					else
					{
						queryString = originalQueryString.Substring(0, findIndex - 1); //Source={12A34C77-8215-485B-B16A-068954163207}&test=1&sc_lang=en
					}
				}
				else if (findIndex == 0)
				{
					queryString = originalQueryString.Substring(ampersandIndex + 1); //sc_lang=en&Source={12A34C77-8215-485B-B16A-068954163207}&test=1
				}
				else
				{
					queryString = originalQueryString.Remove(findIndex, ampersandIndex - findIndex + 1); //Source={12A34C77-8215-485B-B16A-068954163207}&sc_lang=en&test=1
				}
			}

			return queryString;
		}


		public static string ExtractValidSitecoreItemName(string itemName, char replacedChar)
		{
			StringBuilder newItemName = new StringBuilder();
			char[] invalidChars = Sitecore.Configuration.Settings.InvalidItemNameChars;

			foreach (char c in itemName)
			{
				if (invalidChars.Contains(c))
					newItemName.Append(replacedChar);
				else if ((Char.IsLetterOrDigit(c)) || (c.Equals(' ')))
					newItemName.Append(c);
				else if (c.Equals('-'))
					newItemName.Append(replacedChar);
			}

			return newItemName.ToString();
		}


		public static string GetFormattedString(object col)
		{
			if (col != null)
				return CareMoreUtilities.GetCapitalizedString(col.ToString());
			else
				return string.Empty;
		}

		public static bool HasNonEmptyValue(object rowData)
		{
			if (rowData != null && rowData.ToString() != string.Empty)
				return true;
			else
				return false;
		}


		public static void SetLastUpdateDates(Item item)
		{
			if (item != null)
				SetLastUpdateDates(new Item[] { item });
		}

		public static void SetLastUpdateDates(Item[] items)
		{
			if ((items != null) && (items.Length > 0))
			{
				HttpSessionState session = HttpContext.Current.Session;

				List<DateTime> otherDates;
				if (session["LastUpdated"] == null)
				{
					otherDates = new List<DateTime>();
				}
				else
				{
					otherDates = (List<DateTime>)session["LastUpdated"];
				}

				foreach (Item item in items)
				{
					otherDates.Add(((DateField)item.Fields["__Updated"]).DateTime);
				}

				session["LastUpdated"] = otherDates;
			}
		}

		public static Item GetContentItem(string folderName)
		{
			return Sitecore.Context.Item.Paths.GetSubItem("content/" + folderName);
		}

		public static void SendEmail(MailMessage message)
		{
			CareMore.Web.DotCom.Common.SendEmail(message);
		}


		public static string GetSelectedItems(ListItemCollection items)
		{
			return GetSelectedItems(items, ", ");
		}

		public static string GetSelectedItems(ListItemCollection items, string separator)
		{
			StringBuilder selectedValues = new StringBuilder();

			foreach (ListItem item in items)
			{
				if (item.Selected)
				{
					if (!string.IsNullOrEmpty(item.Value))
					{
						selectedValues.Append(item.Value);
						selectedValues.Append(separator);
					}
				}
			}

			if (selectedValues.Length > 0 && separator.Length > 0)
				selectedValues.Remove(selectedValues.Length - separator.Length, separator.Length);

			return selectedValues.ToString();
		}

		public static void SetSelectedItem(ListControl listControl, string valueToSelect)
		{
			SetSelectedItem(listControl, valueToSelect, StringComparison.OrdinalIgnoreCase);
		}

		public static void SetSelectedItem(ListControl listControl, string valueToSelect, StringComparison comparisonType)
		{
			foreach (ListItem item in listControl.Items)
			{
				if (item.Value.Equals(valueToSelect, comparisonType))
				{
					item.Selected = true;
					break;
				}
			}
		}


		public static string FormattedPhone(string originalNumber)
		{
			string newNumber = string.Empty;

			if (!string.IsNullOrEmpty(originalNumber))
			{
				string[] phone = originalNumber.Replace("(", string.Empty).Replace(")", string.Empty).Split(
					new char[] { ' ', '.', '-' }, StringSplitOptions.RemoveEmptyEntries);

				if (phone.Length == 3) // 111-222-3333
				{
					newNumber = string.Format("({0}) {1}-{2}", phone[0], phone[1], phone[2]);
				}
				else if (phone.Length == 4) // 1-222-333-4444
				{
					newNumber = string.Format("({0}) {1}-{2}", phone[1], phone[2], phone[3]);
				}
				else // unknown
				{
					newNumber = string.Join("-", phone);
				}
			}

			return newNumber;
		}


		//public static void ErrorMessageIsRequired(BaseValidator validator, string label, bool isEnglish)
		//{
		//	if (isEnglish)
		//		validator.ErrorMessage = string.Format("{0} is required.", label);
		//	else
		//		validator.ErrorMessage = string.Format("{0} se requiere.", label);
		//}

		//public static void ErrorMessageIsRequired(BaseValidator validator, Text label, string field, bool isEnglish)
		//{
		//	if (isEnglish)
		//		validator.ErrorMessage = string.Format("{0} is required.", label.Item[field]);
		//	else
		//		validator.ErrorMessage = string.Format("{0} se requiere.", label.Item[field]);
		//}

		//public static void ErrorMessageWrongFormat(BaseValidator validator, string label, bool isEnglish)
		//{
		//	if (isEnglish)
		//		validator.ErrorMessage = string.Format("{0} is not in the correct format.", label);
		//	else
		//		validator.ErrorMessage = string.Format("{0} no está en el formato correcto.", label);
		//}

		//public static void ErrorMessageWrongFormat(BaseValidator validator, Text label, string field, bool isEnglish)
		//{
		//	if (isEnglish)
		//		validator.ErrorMessage = string.Format("{0} is not in the correct format.", label.Item[field]);
		//	else
		//		validator.ErrorMessage = string.Format("{0} no está en el formato correcto.", label.Item[field]);
		//}

		//public static void ValidationSummaryHeaderText(ValidationSummary summary, bool isEnglish)
		//{
		//	if (isEnglish)
		//		summary.HeaderText = "Please correct the following fields:";
		//	else
		//		summary.HeaderText = "Corrija por favor los campos siguientes:";
		//}


		public static string DateFormat(DateTime? value)
		{
			if (value.HasValue)
				return value.Value.ToString("MM/dd/yyyy");
			else
				return string.Empty;
		}

		public static string DateFormat(DateTime value)
		{
			if (value > DateTime.MinValue)
				return value.ToString("MM/dd/yyyy");
			else
				return string.Empty;
		}

		public static string DateTimeFormat(DateTime? value)
		{
			if (value.HasValue)
				return value.Value.ToString("MM/dd/yyyy hh:mm tt");
			else
				return string.Empty;
		}

		public static string NotApplicable(string value)
		{
			if (string.IsNullOrEmpty(value))
				return string.Empty;
			else
				return value;
		}

		public static string YesNo(bool? value)
		{
			if (value.HasValue)
			{
				if (value.Value)
					return "Yes";
				else
					return "No";
			}
			else
			{
				return string.Empty;
			}
		}

		public static string YesNo(bool value)
		{
			if (value)
				return "Yes";
			else
				return "No";
		}

		#endregion

		#region Messaging

		public static void DisplayConfigurationIssue(PlaceHolder container)
		{
			Item wording = Sitecore.Context.Database.SelectSingleItem("{E2FA034C-55AC-4525-AD8E-D8F3E4FC1D1F}");
			CareMoreUtilities.DisplayError(container, wording["Text"]);
		}

		public static void DisplayDownMessage(PlaceHolder control, Exception ex, Control hide)
		{
			Item message;

			if (ex.GetType() == typeof(System.Data.SqlClient.SqlException) && ex.Message.Contains("Timeout expired"))
			{
				message = ItemIds.GetItem("FIND_A_DOCTOR_TIMEOUT");
			}
			else
			{
				message = ItemIds.GetItem("FIND_A_DOCTOR_DOWN_MESSAGE");
				Common.EmailException(ex);
			}

			DisplayError(control, message["Text"], hide);
		}

		public static void DisplayError(PlaceHolder control, Exception ex)
		{
			Sitecore.Diagnostics.Log.Error("Error: ", ex, HttpContext.Current.Handler);

			StringBuilder builderError = new StringBuilder();

			if (ex == null)  //CA1062
			{
				builderError.AppendLine("No exception was found (exception == null).");
			}
			else
			{
				bool duplicate = false;
				string exString = ex.ToString();

				foreach (Control item in control.Controls)
				{
					HtmlGenericControl generic = (HtmlGenericControl)item;
					if (generic.InnerText.Contains(exString))
					{
						duplicate = true;
						break;
					}
				}

				if (duplicate)
				{
					return;
				}
				else
				{
					HttpContext context = HttpContext.Current;

					builderError.AppendFormat("UserHostAddress: {0}{1}", CareMoreUtilities.UserIpAddress(context.Request), Environment.NewLine);
					builderError.AppendFormat("Request        : {0}{1}", context.Request.Url.AbsoluteUri, Environment.NewLine);
					if (context.Request.UrlReferrer != null)
						builderError.AppendFormat("Referrer       : {0}{1}", context.Request.UrlReferrer.AbsoluteUri, Environment.NewLine);

					builderError.AppendLine();
					builderError.AppendLine(ex.ToString());

					if (context.Request.Form != null)
					{
						builderError.AppendLine();
						builderError.AppendLine("Form:");

						foreach (string key in HttpContext.Current.Request.Form.Keys)
						{
							if ((key.StartsWith("__") && (key.Equals("__EVENTTARGET")) || key.Equals("__EVENTARGUMENT")) || !key.StartsWith("__"))
								builderError.AppendFormat("    {0}: {1}{2}", key, context.Request.Form[key], Environment.NewLine);
						}
					}
				}
			}
#if DEBUG
			DisplayError(control, builderError.ToString(), "pre");
#else
			Item wording = Sitecore.Context.Database.SelectSingleItem("{AE53A20F-E91A-42C9-B524-1B3C1660A5B7}");
			DisplayError(control, wording["Text"]);
			Common.EmailException(builderError.ToString());
#endif
		}

		public static void DisplayError(PlaceHolder control, Exception ex, Control hide)
		{
			DisplayError(control, ex);
			if (hide != null)
				hide.Visible = false;
		}

		public static void DisplayError(PlaceHolder control, string message)
		{
			DisplayError(control, message, "p");
		}

		public static void DisplayError(PlaceHolder control, string message, Control hide)
		{
			DisplayError(control, message, "p");
			if (hide != null)
				hide.Visible = false;
		}

		public static void DisplayError(PlaceHolder control, string message, string tag)
		{
			HtmlGenericControl paragraph = new HtmlGenericControl(tag);
			paragraph.Attributes.Add("class", "bg-danger");
			paragraph.InnerHtml = message;
			control.Controls.Add(paragraph);
			control.Visible = true;
		}

		public static void DisplayError(PlaceHolder container, Label display, string message)
		{
			container.Visible = true;

			display.CssClass = "bg-danger";
			display.Text = message;
			display.Visible = true;
		}

		public static void DisplayError(PlaceHolder container, Label display, string message, Control hide)
		{
			DisplayError(container, display, message);
			if (hide != null)
				hide.Visible = false;
		}

		public static void DisplayInvalidSubmission(PlaceHolder control, ValidatorCollection validators)
		{
			DisplayInvalidSubmission(control, validators, null);
		}

		public static void DisplayInvalidSubmission(PlaceHolder control, ValidatorCollection validators, Control hide)
		{
			Item userMsg = ItemIds.GetItem("INVALID_SUBMISSION");

			StringBuilder message = new StringBuilder();
			message.AppendLine(userMsg["Text"]);
#if DEBUG
			message.AppendLine("<!--");
			if (validators == null)
			{
				message.AppendLine("No validators");
			}
			else
			{
				foreach (BaseValidator validator in validators)
				{
					if (validator.Enabled && !validator.IsValid)
					{
						message.AppendLine(validator.ClientID);
					}
				}
			}
			message.AppendLine("-->");
#endif
			DisplayError(control, message.ToString(), hide);
		}

		public static void DisplayNoMultiSubmit(PlaceHolder container)
		{
			Item wording = Sitecore.Context.Database.SelectSingleItem("{88F8663E-DE75-4AEB-9F79-F557813F31B8}");
			CareMoreUtilities.DisplayError(container, wording["Text"]);
		}

		public static void DisplayStatus(PlaceHolder control, string message)
		{
			DisplayStatus(control, "p", message, null, true);
		}

		public static void DisplayStatus(PlaceHolder control, string message, string cssClass)
		{
			DisplayStatus(control, "p", message, cssClass, true);
		}

		public static void DisplayStatus(PlaceHolder control, string containerTag, string message, string cssClass, bool encode)
		{
			HtmlGenericControl tag = new HtmlGenericControl(containerTag);

			if (!string.IsNullOrWhiteSpace(cssClass))
				tag.Attributes.Add("class", cssClass);

			if (encode)
				tag.InnerHtml = AntiXssEncoder.HtmlEncode(message, false);
			else
				tag.InnerHtml = message;

			control.Controls.Add(tag);
			control.Visible = true;
		}

		public static void ClearStatus(PlaceHolder control)
		{
			if (control.Controls.Count > 0)
				control.Controls.Clear();

			control.Visible = false;
		}

		#endregion

		#region Other

		public static void ClearControl(Control control)
		{
			control.Controls.Clear();
			control.Visible = false;
		}

		public static string GetNavigationTitle(Item item)
		{
			string title = null;

			if (item.TemplateID == ItemIds.GetID("EXTERNAL_LINK_TEMPLATE_ID"))
			{
				LinkField link = item.Fields["Link"];
				title = link.Text;
			}

			if (string.IsNullOrEmpty(title))
			{
				if (string.IsNullOrEmpty(item["Navigation Title"]))
					title = item.Name;
				else
					title = item["Navigation Title"];
			}

			return title;
		}

		public static string GetNavigationTitle(Item item, string fieldName)
		{
			string title = string.Empty;

			LinkField link = (LinkField)item.Fields[fieldName];

			if (link != null)
			{
				if (string.IsNullOrEmpty(link.Text))
					title = link.Title;
				else
					title = link.Text;
			}

			return title;
		}

		public static bool IsVisible(Item item)
		{
			//Check hide from navigation
			CheckboxField hideSitemap = item.Fields["Hide from navigation"];

			if (hideSitemap != null && hideSitemap.Checked)
				return false;

			if (item.TemplateID == ItemIds.GetID("EXTERNAL_LINK_TEMPLATE_ID"))
				return true;

			return IsLayoutAssigned(item);
		}

		/**
		 * Checks to see if a item has a presentation assigned. Usefully to 
		 * see if a item is a setting item and hide in navigation.
		 */
		private static bool IsLayoutAssigned(Item item)
		{
			//Check to see if default device is populated
			DeviceItem defaultDevice = ItemIds.GetItem("DEFAULT_DEVICE");

			if (item.Visualization == null)
				return false;

			LayoutItem layoutItem = null;

			if (item["__Renderings"].Trim().Length > 0)
			{
				layoutItem = item.Visualization.GetLayout(defaultDevice);
			}

			return (layoutItem != null);
		}

		public static string GetCssClass(Item column, string fieldName)
		{
			LookupField columnStyleField = column.Fields[fieldName];

			if (columnStyleField == null || columnStyleField.TargetItem == null)
				return null;

			return columnStyleField.TargetItem["Style"];
		}

		public static string UserIpAddress(HttpRequest request)
		{
			if (request == null)
				return "127.0.0.1";
			else if (string.IsNullOrEmpty(request.ServerVariables["HTTP_X_FORWARDED_FOR"]))
				return request.UserHostAddress;
			else
				return request.ServerVariables["HTTP_X_FORWARDED_FOR"];
		}

		#endregion
	}
}
