﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;
using System.IO;
using Sitecore;
using System.Xml;

namespace Website.SiteMap
{
    /// <summary>
    /// Handles the incoming request for "/sitemap.xml" to generate the google sitemap xml 
    /// from the SitemapManager
    /// </summary>
    public class SitemapHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {

            //Attempt to find result in Cache
            string _sitemap = StringUtil.GetString(context.Cache.Get("sitemap"));
            
            //if new map is required
            if (string.IsNullOrEmpty(_sitemap))
            {
                //generate xml sitemap
                SitemapManager _mgr = new SitemapManager(true);
                XDocument _map = _mgr.GenerateXmlSiteMap();



                MemoryStream _ms = new MemoryStream();
                StreamReader _sr = new StreamReader(_ms);
                using (XmlWriter _xw = new XmlTextWriter(_ms, Encoding.UTF8))
                {
                    _map.Save(_xw);
                    _xw.Flush();
                    _ms.Seek(0, SeekOrigin.Begin);
                    _sitemap = _sr.ReadToEnd();
                }
                
                _sr.Close();
                _ms.Close();

                //store in cache for 30 mins
                context.Cache.Insert("sitemap", _sitemap, null, DateTime.Now.AddMinutes(30), TimeSpan.Zero);
            }


            //output result
            context.Response.Buffer = true;
            context.Response.ClearContent();
            context.Response.ClearHeaders();
            context.Response.ContentType = "text/xml";
            context.Response.Write(_sitemap);
            context.Response.End();
        }

        #endregion
    }
}
