﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using CareMore.Web.DotCom.Data;

namespace CareMore.Web.DotCom.Provider
{
	// <notes>
	// Provider Portal Codes are currently specified as follows:
	// <notes>
	// <portal code="0" NWNW_IDs="1000 to 3035">CareMore</portal>
	// <portal code="1" NWNW_IDs="4000 to 4020">Empire MediBlue (Kings County)</portal>
	// <portal code="2" NWNW_IDs="5000 to 5035">Anthem MediBlue (Richmond City)</portal>
	// <portal code="3" NWNW_IDs="6000 to 6001">MMP-Medicare (Los Angeles County)</portal>
	// <portal code="4" NWNW_IDs="6300 to 6301">MMP-Medicare (Santa Clara County)</portal>
	// <portal code="5" NWNW_IDs="7000 to 7001">MMP-Medicare (Richmond City)</portal>

	public static class ProviderFinder
	{
		/// <summary>
		/// Get a list of providers according to the specified filters
		/// </summary>
		/// <param name="portalCode">Code of provider website to filter for</param>
		/// <param name="lastName">Provider last name</param>
		/// <param name="zip">Zip code to search</param>
		/// <param name="city">Name of city</param>
		/// <param name="county">Name of county</param>
		/// <param name="stateName">Name of state</param>
		/// <param name="isPCP">Boolean value to see if specialties list containcs PCP</param>
		/// <param name="specialties">Provider specialites</param>
		/// <param name="languages">Spoken languages</param>
		/// <returns>A list of ProviderSearchResult</returns>
		public static List<ProviderSearchResult> GetProviderList(byte portalCode, string lastName, string zip, string city, string county, string stateCode, bool? isPCP, string specialties, string languages, double? maxDistance)
		{
			List<ProviderSearchResult> list = new List<ProviderSearchResult>();

			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
			IEnumerable<ProviderSearchResult> results =
				from result in dbContext.CM_PT_CMC_ProviderSearch(portalCode, lastName, zip, city, county, stateCode, isPCP, specialties, languages, maxDistance)
				select result;
			list = results.ToList();

			return list;
		}

		/// <summary>
		/// Get a list of facilities according to the specified filters
		/// </summary>
		/// <param name="portalCode">Code of provider website to filter for</param>
		/// <param name="name">Name of facility</param>
		/// <param name="zip">Zip code to search</param>
		/// <param name="city">Name of city</param>
		/// <param name="stateName">Name of state</param>
		/// <returns>A list of FacilitySearchResult</returns>
		public static List<FacilitySearchResult> GetFacilityList(byte portalCode, string facilityCode, string name, string zip, string city, string county, string stateCode, double? maxDistance)
		{
			List<FacilitySearchResult> list = new List<FacilitySearchResult>();

			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
			IEnumerable<FacilitySearchResult> results =
				from result in dbContext.CM_PT_CMC_FacilitySearch(portalCode, facilityCode, name, zip, city, county, stateCode, maxDistance)
				select result;
			list = results.ToList();

			return list;
		}

		/// <summary>
		/// Get provider details information
		/// </summary>
		/// <param name="portalCode">Code of provider website to filter for</param>
		/// <param name="providerID">ID of provider obtained from Provider List search</param>
		/// <param name="addressType">Address Type of provider obtained from provider details</param>
		/// <returns>A list of ProviderSearchDetail</returns>
		public static List<ProviderSearchDetail> GetProviderDetail(byte portalCode, string providerID, string addressType)
		{
			List<ProviderSearchDetail> list;

			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
			IEnumerable<ProviderSearchDetail> results =
				from result in dbContext.CM_PT_CMC_ProviderDetail(portalCode, providerID, addressType)
				select result;
			list = results.ToList();

			return list;
		}

		/// <summary>
		/// Get facility details information
		/// </summary>
		/// <param name="portalCode">Code of provider website to filter for</param>
		/// <param name="providerID">ID of facility obtained from Facility List search</param>
		/// <param name="addressType">Address Type of provider obtained from provider details</param>
		/// <returns>A list of FacilitySearchDetail</returns>
		public static List<FacilitySearchDetail> GetFacilityDetail(byte portalCode, string providerID, string addressType)
		{
			List<FacilitySearchDetail> list;
			
			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
			IEnumerable<FacilitySearchDetail> results =
				from result in dbContext.CM_PT_CMC_FacilityDetail(portalCode, providerID, addressType)
				select result;
			list = results.ToList();

			return list;
		}

		/// <summary>
		/// Get provider accessibility
		/// </summary>
		/// <param name="providerID">ID of provider obtained from provider list search</param>
		/// <param name="addressID">Address ID of provider obtained from provider details</param>
		/// <param name="addressType">Address Type of provider obtained from provider details</param>
		/// <returns>A list of ProviderAccessibility</returns>
		public static List<ProviderAccessibility> GetProviderAccessibility(string providerID, string addressID, string addressType)
		{
			List<ProviderAccessibility> list = new List<ProviderAccessibility>();

			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
			IEnumerable<ProviderAccessibility> results =
				from result in dbContext.CM_PT_CMC_ProviderAccessibility(providerID, addressID, addressType)
				select result;
			list = results.ToList();

			return list;
		}

		/// <summary>
		/// Get provider hours of operation
		/// </summary>
		/// <param name="providerID">ID of provider obtained from provider list search</param>
		/// <param name="addressID">Address ID of provider obtained from provider details</param>
		/// <param name="addressType">Address Type of provider obtained from provider details</param>
		/// <returns>A list of ProviderHours</returns>
		public static List<ProviderHour> GetProviderHours(string providerID, string addressID, string addressType, DateTime addressDate)
		{
			List<ProviderHour> list = new List<ProviderHour>();

			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
			IEnumerable<ProviderHour> results =
				from result in dbContext.CM_PT_CMC_ProviderHours(providerID, addressID, addressType, addressDate)
				select result;
			list = results.ToList();

			return list;
		}

		/// <summary>
		/// Get provider languages
		/// </summary>
		/// <param name="providerID">ID of provider obtained from provider list search</param>
		/// <param name="addressID">Address ID of provider obtained from provider details</param>
		/// <param name="addressType">Address Type of provider obtained from provider details</param>
		/// <returns>A list of ProviderLanguage</returns>
		public static List<ProviderLanguage> GetProviderLanguages(string providerID, string addressID, string addressType)
		{
			List<ProviderLanguage> list = new List<ProviderLanguage>();

			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
			IEnumerable<ProviderLanguage> results =
				from result in dbContext.CM_PT_CMC_ProviderLanguages(providerID, addressID, addressType)
				select result;
			list = results.ToList();

			return list;
		}

		/// <summary>
		/// Get all provider locations
		/// </summary>
		/// <param name="addressID">Address ID of facility obtained from facility details</param>
		/// <param name="state">State of current location</param>
		/// <returns>ProviderLocation</returns>
		public static IEnumerable<ProviderLocation> GetProviderLocations(string addressID, string state)
		{
			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
			IEnumerable<ProviderLocation> results =
				from result in dbContext.CM_PT_CMC_ProviderAddresses(addressID, state)
				select result;

			return results;
		}

		/// <summary>
		/// Get provider services
		/// </summary>
		/// <param name="providerID">ID of provider obtained from provider list search</param>
		/// <param name="addressID">Address ID of provider obtained from provider details</param>
		/// <param name="addressType">Address Type of provider obtained from provider details</param>
		/// <returns>A list of ProviderService</returns>
		public static List<ProviderService> GetProviderServices(string providerID, string addressID, string addressType)
		{
			List<ProviderService> list = new List<ProviderService>();

			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
			IEnumerable<ProviderService> results =
				from result in dbContext.CM_PT_CMC_ProviderServices(providerID, addressID, addressType)
				select result;
			list = results.ToList();

			return list;
		}

		public static List<ZipCode> GetZipCodes(string baseZipCode, double maxDistance)
		{
			List<ZipCode> list = new List<ZipCode>();

			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
			IEnumerable<ZipCode> results =
				from result in dbContext.CM_PT_CMC_ZipsWithinDistance(baseZipCode, maxDistance)
				select result;
			list = results.ToList();

			return list;
		}

		public static IEnumerable<SpecialtyItem> GetSpecialtyItems()
		{
			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
			IEnumerable<SpecialtyItem> results =
				from specialty in dbContext.SpecialtyItems
				orderby specialty.Description
				select specialty;

			return results;
		}

		public static IEnumerable<LanguageItem> GetLanguageItems()
		{
			ProviderSearchDataContext dbContext = new ProviderSearchDataContext();
			IEnumerable<LanguageItem> results =
				from lang in dbContext.LanguageItems
				orderby lang.Description
				select lang;

			return results;
		}
	}
}
