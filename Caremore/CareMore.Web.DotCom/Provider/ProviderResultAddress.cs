﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CareMore.Web.DotCom.Data;

namespace CareMore.Web.DotCom.Provider
{
	public class ProviderResultAddress
	{
		#region Constructors

		public ProviderResultAddress()
		{
		}

		public ProviderResultAddress(ProviderSearchResult item)
		{
			this.AddressType = item.AddressType;
			this.AddressEffectiveDate = item.AddressEffectiveDate;
			this.Address1 = item.Address1;
			this.Address2 = item.Address2;
			this.City = item.City;
			this.State = item.State;
			this.Zip = item.Zip;
			this.Phone = item.Phone;
		}

		#endregion

		#region Properties

		public string AddressType { get; set; }
		public DateTime AddressEffectiveDate { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Zip { get; set; }
		public string Phone { get; set; }

		#endregion

		#region Methods

		#endregion
	}
}
