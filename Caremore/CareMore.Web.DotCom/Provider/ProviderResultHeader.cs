﻿using System;
using System.Collections.Generic;
using CareMore.Web.DotCom.Data;
using Sitecore.Data.Items;

namespace CareMore.Web.DotCom.Provider
{
	public class ProviderResultHeader
	{
		public ProviderResultHeader(ProviderSearchResult item)
		{
			this.SearchResult = item;

			this.ProviderId = item.ProviderId;
			this.NpiId = item.NpiId;
			this.AddressId = item.AddressId;
			this.Name = item.Name;
			this.ClassDescription = item.ClassDescription;
			this.PrimarySpecialty = item.PrimarySpecialty;
			this.SecondarySpecialty = item.SecondarySpecialty;
			this.Distance = item.Distance;
			this.EffectiveDate = item.EffectiveDate;

			this.Addresses = new List<ProviderResultAddress>();

			ProviderResultAddress address = new ProviderResultAddress(item);
			this.Addresses.Add(address);
		}

		public string ProviderId { get; set; }
		public string NpiId { get; set; }
		public string AddressId { get; set; }
		public string Name { get; set; }
		public string ClassDescription { get; set; }
		public string PrimarySpecialty { get; set; }
		public string SecondarySpecialty { get; set; }
		public float Distance { get; set; }
		public DateTime EffectiveDate { get; set; }

		public ProviderSearchResult SearchResult { get; private set; }
		public List<ProviderResultAddress> Addresses { get; private set; }
	}
}
