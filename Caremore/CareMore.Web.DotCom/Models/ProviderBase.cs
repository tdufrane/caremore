﻿using System;
using System.Collections.Generic;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using ClassySC.Data;

namespace CareMore.Web.DotCom.Models
{
	public partial interface IProviderBase
	{
		DateTime? EffectiveDate { get; set; }
		string HandicapAccessibility { get; set; }
		string HoursOfOperation { get; set; }
		string MappingID { get; set; }
		string ProviderID { get; set; }
		string ProviderName { get; set; }
		string SubType { get; set; }
	}

	[Template(CareMore.Web.DotCom.Models.ProviderBase.TemplateID)]
	public partial class ProviderBase : StandardTemplate, CareMore.Web.DotCom.Models.IProviderBase, CareMore.Web.DotCom.Models.ILocation, CareMore.Web.DotCom.Models.IContactInformation
	{
		#region Members

		public const string TemplateID = "{BE301E6E-9D9F-49D6-9A98-BDC4B7B56406}";

		public const string EffectiveDate_FID = "{18680302-CC07-4B0A-A5F5-5F157FDDAEE2}";
		public const string HandicapAccessibility_FID = "{020EA504-98C0-4BD1-9744-3DB8DAD90D15}";
		public const string HoursOfOperation_FID = "{28D2A70D-2D1F-4C1C-B0CB-04EC010412AC}";
		public const string MappingID_FID = "52B923A4-95E5-4C49-AE08-D6851D3D47A3";
		public const string ProviderID_FID = "{4746E160-B1C5-4264-8D29-D286F51B463C}";
		public const string ProviderName_FID = "{DB82A18D-0F8E-485F-9B42-CA8C08966496}";
		public const string SubType_FID = "{82306118-40E2-48C9-B1CB-69C19CDA5398}";

		// IProviderBase
		private DateTime? _effectiveDate;
		private string _handicapAccessibility;
		private string _hoursOfOperation;
		private string _mappingID;
		private string _providerID;
		private string _providerName;
		private string _subType;

		// ILocation
		private string _locationName;
		private string _addressLine1;
		private string _addressLine2;
		private string _city;
		private string _county;
		private string _state;
		private string _postalCode;
		private string _latitude;
		private string _longitude;

		// IContactInformation
		private string _phone;
		private string _fax;

		#endregion

		#region Constructors

		public ProviderBase(Item innerItem) : base(innerItem)
		{
		}

		#endregion

		#region IProviderBase Properties

		[Field(CareMore.Web.DotCom.Models.ProviderBase.EffectiveDate_FID)]
		public virtual DateTime? EffectiveDate
		{
			get
			{
				if (_effectiveDate == null)
				{
					_effectiveDate = this.GetDateTime(CareMore.Web.DotCom.Models.ProviderBase.EffectiveDate_FID);
				}
				return _effectiveDate;
			}
			set
			{
				_effectiveDate = null;
				this.SetDateTime(CareMore.Web.DotCom.Models.ProviderBase.EffectiveDate_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ProviderBase.HandicapAccessibility_FID)]
		public virtual string HandicapAccessibility
		{
			get
			{
				if (_handicapAccessibility == null)
				{
					_handicapAccessibility = this.GetString(CareMore.Web.DotCom.Models.ProviderBase.HandicapAccessibility_FID);
				}
				return _handicapAccessibility;
			}
			set
			{
				_handicapAccessibility = null;
				this.SetString(CareMore.Web.DotCom.Models.ProviderBase.HandicapAccessibility_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ProviderBase.HoursOfOperation_FID)]
		public virtual string HoursOfOperation
		{
			get
			{
				if (_hoursOfOperation == null)
				{
					_hoursOfOperation = this.GetString(CareMore.Web.DotCom.Models.ProviderBase.HoursOfOperation_FID);
				}
				return _hoursOfOperation;
			}
			set
			{
				_hoursOfOperation = null;
				this.SetString(CareMore.Web.DotCom.Models.ProviderBase.HoursOfOperation_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ProviderBase.MappingID_FID)]
		public virtual string MappingID
		{
			get
			{
				if (_mappingID == null)
				{
					_mappingID = this.GetString(CareMore.Web.DotCom.Models.ProviderBase.MappingID_FID);
				}
				return _mappingID;
			}
			set
			{
				_mappingID = null;
				this.SetString(CareMore.Web.DotCom.Models.ProviderBase.MappingID_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ProviderBase.ProviderID_FID)]
		public virtual string ProviderID
		{
			get
			{
				if (_providerID == null)
				{
					_providerID = this.GetString(CareMore.Web.DotCom.Models.ProviderBase.ProviderID_FID);
				}
				return _providerID;
			}
			set
			{
				_providerID = null;
				this.SetString(CareMore.Web.DotCom.Models.ProviderBase.ProviderID_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ProviderBase.ProviderName_FID)]
		public virtual string ProviderName
		{
			get
			{
				if (_providerName == null)
				{
					_providerName = this.GetString(CareMore.Web.DotCom.Models.ProviderBase.ProviderName_FID);
				}
				return _providerName;
			}
			set
			{
				_providerName = null;
				this.SetString(CareMore.Web.DotCom.Models.ProviderBase.ProviderName_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ProviderBase.SubType_FID)]
		public virtual string SubType
		{
			get
			{
				if (_subType == null)
				{
					_subType = this.GetString(CareMore.Web.DotCom.Models.ProviderBase.SubType_FID);
				}
				return _subType;
			}
			set
			{
				_subType = null;
				this.SetString(CareMore.Web.DotCom.Models.ProviderBase.SubType_FID, value);
			}
		}

		#endregion

		#region ILocation Properties

		[Field(CareMore.Web.DotCom.Models.Location.LocationName_FID)]
		public virtual string LocationName
		{
			get
			{
				if (_locationName == null)
				{
					_locationName = this.GetString(CareMore.Web.DotCom.Models.Location.LocationName_FID);
				}
				return _locationName;
			}
			set
			{
				_locationName = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.LocationName_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Location.AddressLine1_FID)]
		public virtual string AddressLine1
		{
			get
			{
				if (_addressLine1 == null)
				{
					_addressLine1 = this.GetString(CareMore.Web.DotCom.Models.Location.AddressLine1_FID);
				}
				return _locationName;
			}
			set
			{
				_addressLine1 = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.AddressLine1_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Location.AddressLine2_FID)]
		public virtual string AddressLine2
		{
			get
			{
				if (_addressLine2 == null)
				{
					_addressLine2 = this.GetString(CareMore.Web.DotCom.Models.Location.AddressLine2_FID);
				}
				return _locationName;
			}
			set
			{
				_addressLine2 = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.AddressLine2_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Location.City_FID)]
		public virtual string City
		{
			get
			{
				if (_city == null)
				{
					_city = this.GetString(CareMore.Web.DotCom.Models.Location.City_FID);
				}
				return _locationName;
			}
			set
			{
				_city = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.City_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Location.County_FID)]
		public virtual string County
		{
			get
			{
				if (_county == null)
				{
					_county = this.GetString(CareMore.Web.DotCom.Models.Location.County_FID);
				}
				return _locationName;
			}
			set
			{
				_county = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.County_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Location.State_FID)]
		public virtual string State
		{
			get
			{
				if (_state == null)
				{
					_state = this.GetString(CareMore.Web.DotCom.Models.Location.State_FID);
				}
				return _locationName;
			}
			set
			{
				_state = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.State_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Location.PostalCode_FID)]
		public virtual string PostalCode
		{
			get
			{
				if (_postalCode == null)
				{
					_postalCode = this.GetString(CareMore.Web.DotCom.Models.Location.PostalCode_FID);
				}
				return _locationName;
			}
			set
			{
				_postalCode = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.PostalCode_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Location.Latitude_FID)]
		public virtual string Latitude
		{
			get
			{
				if (_latitude == null)
				{
					_latitude = this.GetString(CareMore.Web.DotCom.Models.Location.Latitude_FID);
				}
				return _locationName;
			}
			set
			{
				_latitude = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.Latitude_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Location.Longitude_FID)]
		public virtual string Longitude
		{
			get
			{
				if (_longitude == null)
				{
					_longitude = this.GetString(CareMore.Web.DotCom.Models.Location.Longitude_FID);
				}
				return _locationName;
			}
			set
			{
				_longitude = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.Longitude_FID, value);
			}
		}

		#endregion

		#region IContactInformation Properties

		[Field(CareMore.Web.DotCom.Models.ContactInformation.Phone_FID)]
		public virtual string Phone
		{
			get
			{
				if (_phone == null)
				{
					_phone = this.GetString(CareMore.Web.DotCom.Models.ContactInformation.Phone_FID);
				}
				return _phone;
			}
			set
			{
				_phone = null;
				this.SetString(CareMore.Web.DotCom.Models.ContactInformation.Phone_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ContactInformation.Fax_FID)]
		public virtual string Fax
		{
			get
			{
				if (_fax == null)
				{
					_fax = this.GetString(CareMore.Web.DotCom.Models.ContactInformation.Fax_FID);
				}
				return _fax;
			}
			set
			{
				_fax = null;
				this.SetString(CareMore.Web.DotCom.Models.ContactInformation.Fax_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ContactInformation.Email_FID)]
		public virtual Sitecore.Data.Fields.LinkField Email
		{
			get
			{
				return this.GetField<LinkField>(CareMore.Web.DotCom.Models.ContactInformation.Email_FID);
			}
			set
			{
				this.SetField<LinkField>(CareMore.Web.DotCom.Models.ContactInformation.Email_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.ContactInformation.Website_FID)]
		public virtual Sitecore.Data.Fields.LinkField Website
		{
			get
			{
				return this.GetField<LinkField>(CareMore.Web.DotCom.Models.ContactInformation.Website_FID);
			}
			set
			{
				this.SetField<LinkField>(CareMore.Web.DotCom.Models.ContactInformation.Website_FID, value);
			}
		}

		#endregion
	}
}
