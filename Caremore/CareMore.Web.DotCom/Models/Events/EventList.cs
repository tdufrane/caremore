﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Globalization;

namespace CareMore.Web.DotCom.Models.Events
{
	public class EventList
	{
		public static List<Item> SearchEvents(string eventType, string zipCode, string city, string stateCode, string distance, string sortDirection, string orderBy, int displayCount, int pageNumber)
		{
			Item EventsDB = Sitecore.Context.Database.GetItem(ItemIds.GetID("EVENTS_DB_ID"), Language.Parse("en"));
			Item eventTypeItem = null;
			List<Item> orderedItems = null;

			if (!string.IsNullOrEmpty(eventType))
			{
				Item sortByItem = Sitecore.Context.Database.GetItem(ItemIds.GetID("EVENT_SORT_BY_ID"), Language.Parse("en"));
				Item[] eventTypeChoices = sortByItem.Axes.GetDescendants();

				eventTypeItem = eventTypeChoices.Where(i => i["Value"] == eventType).FirstOrDefault();
			}

			string[] allowedEventGuids = eventTypeItem == null ? null : eventTypeItem["Event Type"].Split('|');

			Item[] itemList = null;
			DateTime now = DateTime.Now;

			if (zipCode.Equals(string.Empty))
			{
				if (string.IsNullOrEmpty(city))
				{
					itemList = (
						from i in EventsDB.Axes.GetDescendants()
						where
							i.TemplateName == "Event" &&
							i["Status"] == "Active" &&
							((DateField)i.Fields["Event Date"]).DateTime >= now &&
							i["State Code"].Equals(stateCode, StringComparison.OrdinalIgnoreCase) &&
							(allowedEventGuids == null || i["Event Type"].Split('|').Any(e => allowedEventGuids.Contains(e)))
						select i).ToArray();
				}
				else
				{
					itemList = (
						from i in EventsDB.Axes.GetDescendants()
						where
							i.TemplateName == "Event" &&
							i["Status"] == "Active" &&
							((DateField)i.Fields["Event Date"]).DateTime >= now &&
							i["State Code"].Equals(stateCode, StringComparison.OrdinalIgnoreCase) &&
							(i.Fields["City"].Value.StartsWith(city, StringComparison.InvariantCultureIgnoreCase)) &&
							(allowedEventGuids == null || i["Event Type"].Split('|').Any(e => allowedEventGuids.Contains(e)))
						select i).ToArray();
				}
			}
			else
			{
				List<Data.ZipCode> zipCodes = Provider.ProviderFinder.GetZipCodes(zipCode, double.Parse(distance));

				if (zipCodes.Count > 0)
				{
					itemList = (
						from i in EventsDB.Axes.GetDescendants()
						join zip in zipCodes on i["Postal Code"] equals zip.BaseZipCode
						where
							i.TemplateName == "Event" &&
							i["Status"] == "Active" &&
							((DateField)i.Fields["Event Date"]).DateTime >= now &&
							i["State Code"].Equals(stateCode, StringComparison.OrdinalIgnoreCase) &&
							(allowedEventGuids == null || i["Event Type"].Split('|').Any(e => allowedEventGuids.Contains(e)))
						select i).ToArray();
				}
			}

			if (itemList != null  && itemList.Length > 0)
			{
				List<Item> eventItems;

				if (string.IsNullOrEmpty(orderBy))
					eventItems = itemList.OrderBy(i => i.Fields["Event Date"].Value).ToList();
				else  if (sortDirection.Equals("ASC"))
					eventItems = itemList.OrderBy(i => i.Fields[orderBy].Value).ToList();
				else
					eventItems = itemList.OrderByDescending(i => i.Fields[orderBy].Value).ToList();

				if (displayCount == 0)
					orderedItems = eventItems;
				else if (pageNumber == 0)
					orderedItems = eventItems.Take(displayCount).ToList();
				else
					orderedItems = eventItems.Skip(displayCount * pageNumber).Take(displayCount).ToList();
			}

			return orderedItems;
		}

		public static string RenderEventType(Item item)
		{
			StringBuilder events = new StringBuilder();

			MultilistField eventTypeItem = item.Fields["Event Type"];
			Item[] eventTypes = eventTypeItem.GetItems();

			if (eventTypes.Length > 0)
			{
				foreach (Item eventType in eventTypes)
				{
					events.Append(eventType.DisplayName);
					events.Append("<br />");
				}

				events.Length -= 6; //remove last <br />
			}

			return events.ToString();
		}

	}
}
