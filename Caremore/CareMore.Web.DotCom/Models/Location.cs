﻿using System;
using System.Collections.Generic;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using ClassySC.Data;

namespace CareMore.Web.DotCom.Models
{
	public partial interface ILocation
	{
		string LocationName
		{
			get;
			set;
		}

		string AddressLine1
		{
			get;
			set;
		}

		string AddressLine2
		{
			get;
			set;
		}

		string City
		{
			get;
			set;
		}

		string County
		{
			get;
			set;
		}

		string State
		{
			get;
			set;
		}

		string PostalCode
		{
			get;
			set;
		}

		string Latitude
		{
			get;
			set;
		}

		string Longitude
		{
			get;
			set;
		}
	}

	[Template(CareMore.Web.DotCom.Models.Location.TemplateID)]
	public partial class Location : StandardTemplate, CareMore.Web.DotCom.Models.ILocation
	{
		#region Members

		public const string TemplateID = "{23F50D62-9C8D-4CBC-80BC-05AB21038CBA}";

		public const string LocationName_FID = "A67C77F6-23D9-41CD-BDFE-5C82AA860923";
		public const string AddressLine1_FID = "CAFC012F-D70C-4AF7-A3E4-F42450563D5F";
		public const string AddressLine2_FID = "C8556E0D-23A3-4450-9A88-2BC48747B2F5";
		public const string City_FID = "6EA54482-ED0E-49A7-8C5D-855437BE1980";
		public const string County_FID = "E9E40A5A-6B93-43AD-8245-849584E19F6D";
		public const string State_FID = "D2DD2010-10BC-4B5D-9E58-3971B8629781";
		public const string PostalCode_FID = "D800657A-269F-46E0-9A48-A797E6F76A06";
		public const string Latitude_FID = "8E9CF757-5ECF-4E06-883B-C1C74867B90A";
		public const string Longitude_FID = "C93F78FC-A6EE-4DDE-B3A9-4E6C1DC191E9";

		private string _locationName;
		private string _addressLine1;
		private string _addressLine2;
		private string _city;
		private string _county;
		private string _state;
		private string _postalCode;
		private string _latitude;
		private string _longitude;

		#endregion

		#region Constructors

		public Location(Item innerItem) : base(innerItem)
		{
		}

		#endregion

		#region Properties

		[Field(CareMore.Web.DotCom.Models.Location.LocationName_FID)]
		public virtual string LocationName
		{
			get
			{
				if (_locationName == null)
				{
					_locationName = this.GetString(CareMore.Web.DotCom.Models.Location.LocationName_FID);
				}
				return _locationName;
			}
			set
			{
				_locationName = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.LocationName_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Location.AddressLine1_FID)]
		public virtual string AddressLine1
		{
			get
			{
				if (_addressLine1 == null)
				{
					_addressLine1 = this.GetString(CareMore.Web.DotCom.Models.Location.AddressLine1_FID);
				}
				return _locationName;
			}
			set
			{
				_addressLine1 = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.AddressLine1_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Location.AddressLine2_FID)]
		public virtual string AddressLine2
		{
			get
			{
				if (_addressLine2 == null)
				{
					_addressLine2 = this.GetString(CareMore.Web.DotCom.Models.Location.AddressLine2_FID);
				}
				return _locationName;
			}
			set
			{
				_addressLine2 = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.AddressLine2_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Location.City_FID)]
		public virtual string City
		{
			get
			{
				if (_city == null)
				{
					_city = this.GetString(CareMore.Web.DotCom.Models.Location.City_FID);
				}
				return _locationName;
			}
			set
			{
				_city = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.City_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Location.County_FID)]
		public virtual string County
		{
			get
			{
				if (_county == null)
				{
					_county = this.GetString(CareMore.Web.DotCom.Models.Location.County_FID);
				}
				return _locationName;
			}
			set
			{
				_county = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.County_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Location.State_FID)]
		public virtual string State
		{
			get
			{
				if (_state == null)
				{
					_state = this.GetString(CareMore.Web.DotCom.Models.Location.State_FID);
				}
				return _locationName;
			}
			set
			{
				_state = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.State_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Location.PostalCode_FID)]
		public virtual string PostalCode
		{
			get
			{
				if (_postalCode == null)
				{
					_postalCode = this.GetString(CareMore.Web.DotCom.Models.Location.PostalCode_FID);
				}
				return _locationName;
			}
			set
			{
				_postalCode = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.PostalCode_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Location.Latitude_FID)]
		public virtual string Latitude
		{
			get
			{
				if (_latitude == null)
				{
					_latitude = this.GetString(CareMore.Web.DotCom.Models.Location.Latitude_FID);
				}
				return _locationName;
			}
			set
			{
				_latitude = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.Latitude_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Location.Longitude_FID)]
		public virtual string Longitude
		{
			get
			{
				if (_longitude == null)
				{
					_longitude = this.GetString(CareMore.Web.DotCom.Models.Location.Longitude_FID);
				}
				return _locationName;
			}
			set
			{
				_longitude = null;
				this.SetString(CareMore.Web.DotCom.Models.Location.Longitude_FID, value);
			}
		}

		#endregion
	}
}
