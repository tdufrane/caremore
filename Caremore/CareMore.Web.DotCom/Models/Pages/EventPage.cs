﻿using System;
using System.IO;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Collections.Generic;
using ClassySC.Data;
namespace CareMore.Web.DotCom.Models.Pages
{


	public partial interface IEventPage
	{

		string Name
		{
			get;
			set;
		}

		DateTime? Date
		{
			get;
			set;
		}

		string Description
		{
			get;
			set;
		}

		string LocationName
		{
			get;
			set;
		}

		string LocationAddress
		{
			get;
			set;
		}
	}

	[Template(CareMore.Web.DotCom.Models.Pages.EventPage.TemplateID)]
	public partial class EventPage : CareMore.Web.DotCom.Models.Pages.BasePage, CareMore.Web.DotCom.Models.Pages.IEventPage
	{

		#region Members
		public const string TemplateID = "{42415F0F-0595-4E40-9588-D715C5E99C73}";

		public const string Date_FID = "{B51A65B0-5732-4003-965C-9B11EC2DFD5D}";

		public const string LocationAddress_FID = "{1522E97F-7D1F-41F2-827D-66220117D2FF}";

		public const string LocationName_FID = "{560ABB51-12B0-4418-8410-30AC8F470707}";

		public const string Description_FID = "{81F1BC1A-8A70-4D92-81B0-088355FCC698}";

		public const string Name_FID = "{E5EE9EA2-E1FA-4D6C-93AD-34B98C896D05}";

		private string _name;

		private DateTime? _date;

		private string _description;

		private string _locationName;

		private string _locationAddress;
		#endregion

		#region Constructors
		public EventPage(Item innerItem) :
			base(innerItem)
		{
		}
		#endregion

		#region Properties
		[Field(CareMore.Web.DotCom.Models.Pages.EventPage.Name_FID)]
		public virtual string Name
		{
			get
			{
				if (_name == null)
				{
					_name = this.GetString(CareMore.Web.DotCom.Models.Pages.EventPage.Name_FID);
				}
				return _name;
			}
			set
			{
				_name = null;
				this.SetString(CareMore.Web.DotCom.Models.Pages.EventPage.Name_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Pages.EventPage.Date_FID)]
		public virtual DateTime? Date
		{
			get
			{
				if (_date == null)
				{
					_date = this.GetDateTime(CareMore.Web.DotCom.Models.Pages.EventPage.Date_FID);
				}
				return _date;
			}
			set
			{
				_date = null;
				this.SetDateTime(CareMore.Web.DotCom.Models.Pages.EventPage.Date_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Pages.EventPage.Description_FID)]
		public virtual string Description
		{
			get
			{
				if (_description == null)
				{
					_description = this.GetString(CareMore.Web.DotCom.Models.Pages.EventPage.Description_FID);
				}
				return _description;
			}
			set
			{
				_description = null;
				this.SetString(CareMore.Web.DotCom.Models.Pages.EventPage.Description_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Pages.EventPage.LocationName_FID)]
		public virtual string LocationName
		{
			get
			{
				if (_locationName == null)
				{
					_locationName = this.GetString(CareMore.Web.DotCom.Models.Pages.EventPage.LocationName_FID);
				}
				return _locationName;
			}
			set
			{
				_locationName = null;
				this.SetString(CareMore.Web.DotCom.Models.Pages.EventPage.LocationName_FID, value);
			}
		}

		[Field(CareMore.Web.DotCom.Models.Pages.EventPage.LocationAddress_FID)]
		public virtual string LocationAddress
		{
			get
			{
				if (_locationAddress == null)
				{
					_locationAddress = this.GetString(CareMore.Web.DotCom.Models.Pages.EventPage.LocationAddress_FID);
				}
				return _locationAddress;
			}
			set
			{
				_locationAddress = null;
				this.SetString(CareMore.Web.DotCom.Models.Pages.EventPage.LocationAddress_FID, value);
			}
		}
		#endregion
	}
}