﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.Hosting;
using System.Xml;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using Sitecore.Xml;

namespace CareMore.Web.DotCom
{
	public class ItemIds
	{
		#region Private variables

		private static object _settingsLock = new object();
		private static Hashtable _settings;

		#endregion

		#region Public methods

		public static ID GetID(string name)
		{
			Hashtable hash = GetHashtable();

			ID itemId = hash[name] as ID;

			if (ID.IsNullOrEmpty(itemId))
			{
				Log.Error("Failed to get item id for: " + name, "ItemIds");
			}

			return itemId;
		}

		public static string GetString(string name)
		{
			Hashtable hash = GetHashtable();

			var itemId = hash[name] as ID;

			if (ID.IsNullOrEmpty(itemId))
			{
				Log.Error("Failed to get item id for: " + name, "ItemIds");
				return string.Empty;
			}

			return itemId.ToString();
		}

		public static string GetItemValue(string itemReferenceName, string fieldName)
		{
			return GetItemValue(itemReferenceName, fieldName, false);
		}

		public static string GetItemValue(string itemReferenceName, string fieldName, bool addSpan)
		{
			Item item = GetItem(itemReferenceName);

			if (item == null)
			{
				return string.Empty;
			}

			string value = item[fieldName];

			if (addSpan)
			{
				value = string.Format("<span>{0}</span>", value);
			}

			return value;
		}

		public static Item GetItem(string itemReferenceName)
		{
			if (string.IsNullOrEmpty(itemReferenceName))
			{
				Log.Error("itemReferenceName is empty", "ItemIds");
				return null;
			}

			ID itemId = GetID(itemReferenceName);

			if (Sitecore.Data.ID.IsNullOrEmpty(itemId))
			{
				Log.Error("Failed to find item id in SitecoreIds.config: " + itemReferenceName, "ItemIds");
				return null;
			}

			Item item;

			if (Sitecore.Context.ContentDatabase != null)
				item = Sitecore.Context.ContentDatabase.GetItem(itemId);
			else
				item = Sitecore.Context.Database.GetItem(itemId);

			if (item == null)
				Log.Error("Failed to find item in database: " + itemId, "ItemIds");

			return item;
		}

		public static bool IsDescendant(string parentReferenceName, Item descendantItem)
		{
			bool descendant = false;

			ID id = GetID(parentReferenceName);
			Item currentItem = descendantItem;

			while (currentItem != null)
			{
				if (currentItem.ID == id)
				{
					descendant = true;
					break;
				}

				currentItem = currentItem.Parent;
			}

			return descendant;
		}

		public static string GetUrl(string referenceName)
		{
			Item item = GetItem(referenceName);

			if (item == null)
				return null;
			else
				return LinkManager.GetItemUrl(item);
		}

		public static bool IsTemplateBase(Item item, string referenceName)
		{
			ID referenceId = GetID(referenceName);

			if (ID.IsNullOrEmpty(referenceId))
				return false;
			else
				return IsTemplateBase(item.Template, referenceId);
		}

		public static bool IsTemplateBase(TemplateItem templateItem, ID templateId)
		{
			if (templateItem.ID == templateId)
				return true;
			else
				return templateItem.BaseTemplates.FirstOrDefault(p => IsTemplateBase(p, templateId)) != null;
		}

		#endregion

		#region Private methods

		private static Hashtable GetHashtable()
		{
			Hashtable hashtable = _settings;

			if (hashtable == null)
			{
				lock (_settingsLock)
				{
					hashtable = _settings;
					if (hashtable == null)
					{
						hashtable = new Hashtable();
                        //TODO: made a code change - if statement was incorrect need to review with Cameron
						string HASH_TABLE_PATH = "sitecoreIds/id";

						XmlNodeList configNodes = Factory.GetConfigNodes(HASH_TABLE_PATH);
						if (configNodes != null)
						{
							foreach (XmlNode node in configNodes)
							{
								string attribute = XmlUtil.GetAttribute("name", node);
								string str2 = XmlUtil.GetAttribute("value", node);
								if (attribute.Length > 0)
								{
									hashtable[attribute] = new ID(str2);
								}
							}
						}
					}

					_settings = hashtable;
				}
			}

			return hashtable;
		}

		#endregion
	}
}
