using System;
using System.Configuration;
using Sitecore.Data.Items;

namespace CareMore.Web.DotCom.Data
{
	partial class ProviderSearchDataContext
	{
		partial void OnCreated()
		{
			this.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["Facets"].ConnectionString;
		}

		public static FacilitySearchResult ToFacilitySearchResult(Item item)
		{
			FacilitySearchResult facility = new FacilitySearchResult();

			if (string.IsNullOrWhiteSpace(item["Place"]))
				facility.Name = item["Provider Name"];
			else
				facility.Name = item["Place"];

			facility.AddressType = "P";
			facility.Address1 = item["Address Line 1"];
			facility.Address2 = item["Address Line 2"];
			facility.City = item["City"];
			facility.County = item["County"];

			DateTime effectiveDate;
			if (DateTime.TryParse(item["Effective Date"], out effectiveDate))
				facility.EffectiveDate = effectiveDate;
			else
				facility.EffectiveDate = DateTime.MinValue;

			facility.NpiId = item["NPI ID"];
			facility.Phone = item["Phone"];
			facility.ProviderId = item.ID.ToString();
			facility.State = item["State"];
			facility.Zip = item["Postal Code"];

			return facility;
		}

		public static ProviderSearchResult ToProviderSearchResult(Item item)
		{
			ProviderSearchResult provider = new ProviderSearchResult();

			if (string.IsNullOrWhiteSpace(item["Location Name"]))
				provider.Name = item["Provider Name"];
			else
				provider.Name = item["Location Name"];

			provider.AddressType = "P";
			provider.Address1 = item["Address Line 1"];
			provider.Address2 = item["Address Line 2"];
			provider.City = item["City"];
			provider.County = item["County"];

			DateTime effectiveDate;
			if (DateTime.TryParse(item["Effective Date"], out effectiveDate))
				provider.EffectiveDate = effectiveDate;
			else
				provider.EffectiveDate = DateTime.MinValue;

			provider.NpiId = item["NPI ID"];
			provider.Phone = item["Phone"];
			provider.ProviderId = item.ID.ToString();
			provider.State = item["State"];
			provider.Zip = item["Postal Code"];
			provider.AcceptingPatients = item["Accepting New Patients"].Equals("1") ? "Y" : "N";
			provider.PrimaryLanguage = item["Languages"];

			return provider;
		}
	}
}
