﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;

namespace CareMore.Web.DotCom
{
	public enum DataSources
	{
		Database = 0,
		Sitecore = 1
	}

	public static class Common
	{
		public const string FolderTemplateID = "{A87A00B1-E6DB-45AB-8B54-636FEC3B5523}";

		public static string CleanFilename(string fileName)
		{
			char[] invalidChars = Path.GetInvalidFileNameChars();

			foreach (char item in invalidChars)
			{
				fileName = fileName.Replace(item.ToString(), string.Empty);
			}

			return fileName;
		}

		public static bool ContainsString(string toFind, string[] list)
		{
			bool found = false;

			foreach (string item in list)
			{
				if (toFind.IndexOf(item, StringComparison.OrdinalIgnoreCase) > -1)
				{
					found = true;
					break;
				}
			}

			return found;
		}

		public static string GetCustomCode(Item item, HttpRequest request, bool isPostback)
		{
			return GetCustomCode(item, request, isPostback, false);
		}

		public static string GetCustomCode(Item item, HttpRequest request, bool isPostback, bool isSpecialImplementation)
		{
			StringBuilder code = new StringBuilder();

			if (item != null && !string.IsNullOrWhiteSpace(item["Custom Code Items"]))
			{
				MultilistField customCodeList = (MultilistField)item.Fields["Custom Code Items"];
				string customCodeParams = item["Custom Code Parameters"];
				bool customCodeSpecialImplementation = item["Custom Code Special Implementation"].Equals("1");
				string showsOn = item["Custom Code Shows On"];

				if (((customCodeList != null) && (customCodeList.Items.Length > 0)) &&
					((!customCodeSpecialImplementation) || (customCodeSpecialImplementation && isSpecialImplementation)))
				{
					string[] codeParams = customCodeParams.Split(new char[] { '\n', '\r', '|' }, StringSplitOptions.RemoveEmptyEntries);

					foreach (Item customCode in customCodeList.GetItems())
					{
						bool hasParameters = customCode["Has Parameters"].Equals("1");
						bool hideForCareMore = customCode["Hide for CareMore IPs"].Equals("1");

						if (((!hideForCareMore) || (hideForCareMore && !IsInternalIpAddress(request))) &&
							((!isPostback && (string.IsNullOrEmpty(showsOn) || showsOn.Equals("Both") || showsOn.Equals("Load"))) ||
							 (isPostback && (string.IsNullOrEmpty(showsOn) || showsOn.Equals("Both") || showsOn.Equals("Postback")))))
						{
							string parameterValue = null;
							if (hasParameters)
							{
								foreach (string codeParam in codeParams)
								{
									if (codeParam.StartsWith(customCode.Name))
									{
										parameterValue = codeParam.Substring(customCode.Name.Length + 1); // This removes the name=
										break;
									}
								}
							}

							if (string.IsNullOrWhiteSpace(parameterValue))
							{
								code.AppendLine(customCode["Code"]);
							}
							else
							{
								code.AppendLine(string.Format(customCode["Code"], parameterValue));
							}
						}
					}
				}
			}

			return code.ToString();
		}

		public static string GetErrorMessage(Exception ex)
		{
			string errorPrefix;
#if DEBUG
			errorPrefix = string.Empty;
#else
			Item errorItem = Sitecore.Context.Database.GetItem("/sitecore/content/Global/Wording/Error");
			errorPrefix = errorItem["Text"];
#endif
			return GetErrorMessage(ex, errorPrefix);
		}

		public static string GetErrorMessage(Exception ex, string errorPrefix)
		{
			Sitecore.Diagnostics.Log.Error("Error: ", ex, HttpContext.Current.Handler);

			StringBuilder builderError = new StringBuilder();

#if !DEBUG
			builderError.AppendLine(errorPrefix);
			builderError.AppendLine("<!-- ");
#endif
			if (ex == null)  //CA1062
			{
				builderError.AppendLine("No exception was found (exception == null).");
			}
			else
			{
				builderError.AppendLine(ex.ToString());
			}
#if !DEBUG
			builderError.AppendLine(ex.ToString().Replace("<!--", string.Empty).Replace("-->", "\n"));
			builderError.Replace("   at ", "\nat ");
#endif
			return builderError.ToString();
		}

		public static string GetNoMultiSubmit()
		{
			Item wording = Sitecore.Context.Database.SelectSingleItem("{88F8663E-DE75-4AEB-9F79-F557813F31B8}");
			if (wording == null)
				return string.Empty;
			else
				return wording["Text"];
		}

		public static string GetKeyImagePath(Item item)
		{
			Item currentItem = item;

			while (currentItem != null)
			{
				ImageField keyIconField = currentItem.Fields["Google Map Icon"];

				if (keyIconField != null && keyIconField.MediaItem != null)
				{
					return MediaManager.GetMediaUrl(keyIconField.MediaItem);
				}

				currentItem = currentItem.Parent;
			}

			return "#";
		}

		public static bool IsInternalIpAddress(HttpRequest request)
		{
			bool isInternal = false;

			string ipAddress;

			if (string.IsNullOrEmpty(request.ServerVariables["HTTP_X_FORWARDED_FOR"]))
				ipAddress = request.UserHostAddress;
			else
				ipAddress = request.ServerVariables["HTTP_X_FORWARDED_FOR"];

			if (ipAddress.Length > 0)
			{
				string[] careMoreIps = ConfigurationManager.AppSettings["CareMoreIPs"].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

				foreach (string careMoreIp in careMoreIps)
				{
					if (careMoreIp.Contains("*") && ipAddress.StartsWith(careMoreIp.Replace("*", string.Empty)))
					{
						isInternal = true;
					}
					else if (ipAddress.Equals(careMoreIp))
					{
						isInternal = true;
					}
				}
			}

			return isInternal;
		}

		public static string Encrypt(string text)
		{
			StringBuilder encrypted = new StringBuilder();

			using (SHA256Managed shaHash = new SHA256Managed())
			{
				byte[] data = shaHash.ComputeHash(Encoding.UTF8.GetBytes(text));

				foreach (byte item in data)
				{
					encrypted.Append(item.ToString("x2"));
				}
			}

			return encrypted.ToString();
		}

		#region Emails

		public const string MailBreak = "<br />";
		public const string MailRule = "<hr />";

		public const string MailTableOpen = "<table border='0' cellspacing='0' cellpadding='4'>";
		public const string MailTableRowOpen = "<tr>";
		public const string MailTableCellOpen = "<td>";
		public const string MailTableCellOpen2Col = "<td colspan='2'>";
		public const string MailTableCellClose = "</td>";
		public const string MailTableRowClose = "</tr>";
		public const string MailTableClose = "</table>";

		public static string MailHeader(string title)
		{
			StringBuilder builder = new StringBuilder();

			builder.AppendLine("<html>");
			builder.AppendLine("<head>");
			builder.AppendFormat("<title>{0}</title>\n", title);
			builder.AppendLine("<style type='text/css'>");
			builder.AppendLine("body, p, td { font-family: Calibri, Arial; font-size: 11pt; }");
			builder.AppendLine(".bold { font-weight: bold; }");
			builder.AppendLine(".shaded { background-color: #ccc; }");
			builder.AppendLine("</style>");
			builder.AppendLine("</head>");
			builder.AppendLine("<body>");

			return builder.ToString();
		}

		public static string MailTableRow(string headerText)
		{
			StringBuilder builder = new StringBuilder();

			builder.AppendLine("<tr class='shaded'>"); // For email to have shading
			builder.AppendFormat("<td class='bold' colspan='2'>{0}</td>\n", headerText);
			builder.AppendLine("</tr>");

			return builder.ToString();
		}

		public static string MailTableRow(string headerText, string dataText)
		{
			StringBuilder builder = new StringBuilder();

			builder.AppendLine("<tr>");
			builder.AppendFormat("<td class='bold'>{0}</td>\n", headerText);
			builder.AppendFormat("<td>{0}</td>\n", dataText);
			builder.AppendLine("</tr>");

			return builder.ToString();
		}

		public static string MailTableRow(string headerText1, string dataText1, string dataText2)
		{
			StringBuilder builder = new StringBuilder();

			builder.AppendLine("<tr>");
			builder.AppendFormat("<td class='bold'>{0}</td>\n", headerText1);
			builder.AppendFormat("<td>{0}</td>\n", dataText1);
			builder.AppendFormat("<td>{0}</td>\n", dataText2);
			builder.AppendLine("</tr>");

			return builder.ToString();
		}

		public static string MailFooter()
		{
			StringBuilder builder = new StringBuilder();

			builder.AppendLine("</body>");
			builder.AppendLine("</html>");

			return builder.ToString();
		}

		public static void EmailException(Exception ex)
		{
			EmailException(ex.ToString());
		}

		public static void EmailException(string exception)
		{
			if (!string.IsNullOrEmpty(exception) && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["ExceptionEmailTo"]))
			{
				MailMessage message = new MailMessage();
				message.From = new MailAddress("no-reply@caremore.com");
				message.To.Add(ConfigurationManager.AppSettings["ExceptionEmailTo"]);
				message.Subject = string.Format("{0} - Exception - {1}", HttpContext.Current.Request.Url.Host, HttpContext.Current.Request.Path);
				message.Body = (exception);

				SendEmail(message, true);
			}
		}

		public static void SendEmail(MailMessage message)
		{
			SendEmail(message, false);
		}

		private static void SendEmail(MailMessage message, bool isException)
		{
			if (HttpContext.Current.Request.UserHostName.Equals("127.0.0.1"))
			{
				// Development machines have no access to email
				string path = HttpContext.Current.Server.MapPath("~/userFiles/");

				foreach (MailAddress address in message.To)
				{
					string mailPath = path + CleanFilename(address.Address);

					if (! Directory.Exists(mailPath))
						Directory.CreateDirectory(mailPath);

					StreamWriter writer = new StreamWriter(string.Format("{0}\\{1:yyyyMMddhhmmssss}.htm", mailPath, DateTime.Now));
					writer.Write(message.Body);
					writer.Close();
				}
			}
			else
			{
				string toEmail = ConfigurationManager.AppSettings["RedirectEmailsTo"];

				if (!string.IsNullOrEmpty(toEmail) && !isException)
				{
					StringBuilder originalToEmails = new StringBuilder();

					if (message.IsBodyHtml)
						originalToEmails.Append("<p style='border-bottom:solid 1px gray;'>");

					originalToEmails.Append("Email would have been sent to the following addresses:");
					originalToEmails.Append(EmailAddresses(message.To, "To", message.IsBodyHtml));
					originalToEmails.Append(EmailAddresses(message.CC, "CC", message.IsBodyHtml));
					originalToEmails.Append(EmailAddresses(message.Bcc, "BCC", message.IsBodyHtml));

					if (message.IsBodyHtml)
						originalToEmails.Append("</p>");
					else
						originalToEmails.Append("\n\n");

					message.To.Add(toEmail);
					message.Body = message.Body.Insert(0, originalToEmails.ToString());
				}

				Sitecore.MainUtil.SendMail(message);
			}
		}

		private static string EmailAddresses(MailAddressCollection mailAddresses, string addressType, bool isBodyHtml)
		{
			StringBuilder addresses = new StringBuilder();

			if (mailAddresses.Count > 0)
			{
				if (isBodyHtml)
					addresses.AppendFormat("<br />\n &nbsp; {0}: ", addressType);
				else
					addresses.AppendFormat("\n\t{0}: ", addressType);

				foreach (MailAddress address in mailAddresses)
				{
					addresses.Append(address.Address);
					addresses.Append(", ");
				}

				addresses.Length -= 2;  // Remove trailing comma + space
				mailAddresses.Clear();
			}

			return addresses.ToString();
		}

		public static void SetEmail(MailAddressCollection mailAddresses, string emailAddresses)
		{
			if (!string.IsNullOrEmpty(emailAddresses))
			{
				string[] emails = emailAddresses.Split(new char[] { ';', ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
				foreach (string address in emails)
				{
					mailAddresses.Add(new MailAddress(address));
				}
			}
		}

		#endregion
	}
}
