﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CareMore.Web.DotCom.Data;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Data.Fields;

namespace CareMore.Web.DotCom
{
	public class LocationItem
	{
		#region Properties

		public string Address1 { get; set; }
		public string AddressType { get; set; }
		public string City { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string IconImage { get; set; }
		public string ItemID { get; set; }
		public string ItemURL { get; set; }
		public string Phone { get; set; }
		public string ZipCode { get; set; }

		#endregion

		#region Constructors

		public LocationItem() { ; }

		public LocationItem(Item item, string nameField)
		{
			FromItem(item, nameField);
			IconImage = Common.GetKeyImagePath(item);
			Description = item.Parent["Description"];
			AddressType = "P";
		}

		public LocationItem(Item item, string nameField, string iconImage, string typeDescription, string url)
		{
			FromItem(item, nameField);
			IconImage = iconImage;
			Description = typeDescription;
			ItemURL = url;
			AddressType = "P";
		}

		public LocationItem(string address1, string city, string zip, string name, string iconImage, string typeDescription, string phone, string id, string addressType, string url)
		{
			Address1 = address1;
			AddressType = addressType;
			City = city;
			ZipCode = zip;
			Name = name;
			IconImage = iconImage;
			Description = typeDescription;
			Phone = phone;
			ItemID = id;
			ItemURL = url;
		}

		#endregion

		#region Methods

		private void FromItem(Item item, string nameField)
		{
			Address1 = item["Address Line 1"];
			City = item["City"];
			ZipCode = item["Postal Code"];
			Name = item[nameField];
			Phone = item["Phone"];
			ItemID = item.ID.ToString();
		}

		#endregion
	}
}
