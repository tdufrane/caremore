﻿using System;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace CareMore.Web.DotCom
{
	public static class SitecoreHelper
	{
		public static bool HasFieldValue(Field itemField)
		{
			bool hasValue = false;

			if (itemField != null)
			{
				if (!string.IsNullOrEmpty(itemField.Value))
					hasValue = true;
			}

			return hasValue;
		}

		public static bool HasFieldValue(Field itemField, string value)
		{
			bool matched = false;

			if (itemField != null)
			{
				if (!string.IsNullOrEmpty(itemField.Value))
				{
					if (itemField.Value.Equals(value))
						matched = true;
				}
			}

			return matched;
		}
	}
}
