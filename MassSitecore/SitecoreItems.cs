﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace MassSitecore
{
	public class SitecoreItems : BindingList<SitecoreItem>
	{
		#region Methods

		public void FromXml(string fileName)
		{
			TextReader reader = null;

			try
			{
				reader = new StreamReader(fileName);

				XmlRootAttribute rootAttribute = new XmlRootAttribute();
				rootAttribute.ElementName = "SitecoreItems";

				XmlSerializer serial = new XmlSerializer(typeof(SitecoreItems), rootAttribute);
				SitecoreItems items = (SitecoreItems)serial.Deserialize(reader);

				foreach (SitecoreItem item in items)
				{
					Add(item);
				}
			}
			finally
			{
				if (reader != null)
					reader.Close();
			}
		}

		public SitecoreData.DataItem[] ToArray()
		{
			SitecoreData.DataItem[] array = new SitecoreData.DataItem[this.Count];

			for (int index = 0; index < this.Count; index++)
			{
				array[index] = new MassSitecore.SitecoreData.DataItem();
				array[index].Path = this[index].Path;
				array[index].Field = this[index].Field;
				array[index].AllLanguages = this[index].AllLanguages;
				array[index].Value = this[index].Value;
				array[index].ValueSpanish = this[index].ValueSpanish;
			}

			return array;
		}

		public void ToXml(string fileName)
		{
			XmlTextWriter writer = null;

			try
			{
				if (File.Exists(fileName))
				{
					File.SetAttributes(fileName, FileAttributes.Normal);  // In case file is read-only from Source Control push
				}

				writer = new XmlTextWriter(fileName, System.Text.Encoding.ASCII);
				writer.Formatting = Formatting.Indented;
				writer.Indentation = 1;
				writer.IndentChar = '\x09';

				XmlRootAttribute rootAttribute = new XmlRootAttribute();
				rootAttribute.ElementName = "SitecoreItems";

				XmlSerializer xmlSerial = new XmlSerializer(typeof(SitecoreItems), rootAttribute);
				xmlSerial.Serialize(writer, this);
			}
			finally
			{
				if (writer != null)
					writer.Close();
			}
		}

		#endregion
	}
}
