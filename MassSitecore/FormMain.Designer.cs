﻿namespace MassSitecore
{
	partial class FormMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
			this.labelXmlFile = new System.Windows.Forms.Label();
			this.dataGridViewItems = new System.Windows.Forms.DataGridView();
			this.ColumnPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ColumnField = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ColumnAllLanguage = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.ColumnValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ColumnSpanish = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.buttonGo = new System.Windows.Forms.Button();
			this.labelDirection = new System.Windows.Forms.Label();
			this.panelDirection = new System.Windows.Forms.Panel();
			this.radioButtonExportFields = new System.Windows.Forms.RadioButton();
			this.radioButtonImport = new System.Windows.Forms.RadioButton();
			this.radioButtonExportValues = new System.Windows.Forms.RadioButton();
			this.panelServer = new System.Windows.Forms.Panel();
			this.radioButtonProd = new System.Windows.Forms.RadioButton();
			this.radioButtonStage = new System.Windows.Forms.RadioButton();
			this.radioButtonDev = new System.Windows.Forms.RadioButton();
			this.labelServer = new System.Windows.Forms.Label();
			this.openFileDialogXmlFileName = new System.Windows.Forms.OpenFileDialog();
			this.textBoxConfiguration = new System.Windows.Forms.TextBox();
			this.buttonConfiguration = new System.Windows.Forms.Button();
			this.buttonSave = new System.Windows.Forms.Button();
			this.toolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.radioButtonExportItems = new System.Windows.Forms.RadioButton();
			this.buttonRefresh = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewItems)).BeginInit();
			this.panelDirection.SuspendLayout();
			this.panelServer.SuspendLayout();
			this.SuspendLayout();
			// 
			// labelXmlFile
			// 
			this.labelXmlFile.AutoSize = true;
			this.labelXmlFile.Location = new System.Drawing.Point(12, 9);
			this.labelXmlFile.Name = "labelXmlFile";
			this.labelXmlFile.Size = new System.Drawing.Size(51, 13);
			this.labelXmlFile.TabIndex = 0;
			this.labelXmlFile.Text = "XML &File:";
			// 
			// dataGridViewItems
			// 
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
			this.dataGridViewItems.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
			this.dataGridViewItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.dataGridViewItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnPath,
            this.ColumnField,
            this.ColumnAllLanguage,
            this.ColumnValue,
            this.ColumnSpanish});
			this.dataGridViewItems.Location = new System.Drawing.Point(12, 95);
			this.dataGridViewItems.Name = "dataGridViewItems";
			this.dataGridViewItems.Size = new System.Drawing.Size(835, 473);
			this.dataGridViewItems.TabIndex = 8;
			this.toolTipMain.SetToolTip(this.dataGridViewItems, "List of all Sitecore items, fields, and values");
			this.dataGridViewItems.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridViewItems_ColumnHeaderMouseDoubleClick);
			this.dataGridViewItems.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.DataGridViewItems_UserDeletingRow);
			this.dataGridViewItems.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.DataGridViewItems_UserRowChanged);
			this.dataGridViewItems.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.DataGridViewItems_UserRowChanged);
			// 
			// ColumnPath
			// 
			this.ColumnPath.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.ColumnPath.DataPropertyName = "Path";
			this.ColumnPath.FillWeight = 300F;
			this.ColumnPath.HeaderText = "Path";
			this.ColumnPath.MinimumWidth = 300;
			this.ColumnPath.Name = "ColumnPath";
			// 
			// ColumnField
			// 
			this.ColumnField.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.ColumnField.DataPropertyName = "Field";
			this.ColumnField.FillWeight = 150F;
			this.ColumnField.HeaderText = "Field";
			this.ColumnField.MinimumWidth = 150;
			this.ColumnField.Name = "ColumnField";
			// 
			// ColumnAllLanguage
			// 
			this.ColumnAllLanguage.DataPropertyName = "AllLanguages";
			this.ColumnAllLanguage.HeaderText = "All Language";
			this.ColumnAllLanguage.MinimumWidth = 85;
			this.ColumnAllLanguage.Name = "ColumnAllLanguage";
			this.ColumnAllLanguage.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.ColumnAllLanguage.ToolTipText = "Check if data in Value column applies to all languages";
			this.ColumnAllLanguage.Width = 85;
			// 
			// ColumnValue
			// 
			this.ColumnValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.ColumnValue.DataPropertyName = "Value";
			this.ColumnValue.FillWeight = 120F;
			this.ColumnValue.HeaderText = "English";
			this.ColumnValue.MinimumWidth = 50;
			this.ColumnValue.Name = "ColumnValue";
			this.ColumnValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			// 
			// ColumnSpanish
			// 
			this.ColumnSpanish.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.ColumnSpanish.DataPropertyName = "ValueSpanish";
			this.ColumnSpanish.FillWeight = 120F;
			this.ColumnSpanish.HeaderText = "Spanish";
			this.ColumnSpanish.MinimumWidth = 50;
			this.ColumnSpanish.Name = "ColumnSpanish";
			this.ColumnSpanish.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			// 
			// buttonGo
			// 
			this.buttonGo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonGo.Enabled = false;
			this.buttonGo.Location = new System.Drawing.Point(774, 582);
			this.buttonGo.Name = "buttonGo";
			this.buttonGo.Size = new System.Drawing.Size(75, 23);
			this.buttonGo.TabIndex = 10;
			this.buttonGo.Text = "G&O !";
			this.toolTipMain.SetToolTip(this.buttonGo, "Click to Export or Import");
			this.buttonGo.UseVisualStyleBackColor = true;
			this.buttonGo.Click += new System.EventHandler(this.ButtonGo_Click);
			// 
			// labelDirection
			// 
			this.labelDirection.AutoSize = true;
			this.labelDirection.Location = new System.Drawing.Point(12, 38);
			this.labelDirection.Name = "labelDirection";
			this.labelDirection.Size = new System.Drawing.Size(52, 13);
			this.labelDirection.TabIndex = 4;
			this.labelDirection.Text = "&Direction:";
			// 
			// panelDirection
			// 
			this.panelDirection.Controls.Add(this.radioButtonExportItems);
			this.panelDirection.Controls.Add(this.radioButtonExportFields);
			this.panelDirection.Controls.Add(this.radioButtonImport);
			this.panelDirection.Controls.Add(this.radioButtonExportValues);
			this.panelDirection.Location = new System.Drawing.Point(74, 33);
			this.panelDirection.Name = "panelDirection";
			this.panelDirection.Size = new System.Drawing.Size(500, 25);
			this.panelDirection.TabIndex = 5;
			// 
			// radioButtonExportFields
			// 
			this.radioButtonExportFields.AutoSize = true;
			this.radioButtonExportFields.Location = new System.Drawing.Point(111, 3);
			this.radioButtonExportFields.Name = "radioButtonExportFields";
			this.radioButtonExportFields.Size = new System.Drawing.Size(85, 17);
			this.radioButtonExportFields.TabIndex = 1;
			this.radioButtonExportFields.TabStop = true;
			this.radioButtonExportFields.Text = "Export Fields";
			this.radioButtonExportFields.UseVisualStyleBackColor = true;
			// 
			// radioButtonImport
			// 
			this.radioButtonImport.AutoSize = true;
			this.radioButtonImport.Location = new System.Drawing.Point(202, 3);
			this.radioButtonImport.Name = "radioButtonImport";
			this.radioButtonImport.Size = new System.Drawing.Size(89, 17);
			this.radioButtonImport.TabIndex = 2;
			this.radioButtonImport.Text = "Import Values";
			this.toolTipMain.SetToolTip(this.radioButtonImport, "Select if you want to write the values into Sitecore");
			this.radioButtonImport.UseVisualStyleBackColor = true;
			// 
			// radioButtonExportValues
			// 
			this.radioButtonExportValues.AutoSize = true;
			this.radioButtonExportValues.Location = new System.Drawing.Point(3, 3);
			this.radioButtonExportValues.Name = "radioButtonExportValues";
			this.radioButtonExportValues.Size = new System.Drawing.Size(90, 17);
			this.radioButtonExportValues.TabIndex = 0;
			this.radioButtonExportValues.Text = "Export Values";
			this.toolTipMain.SetToolTip(this.radioButtonExportValues, "Select if you want to read the values from Sitecore");
			this.radioButtonExportValues.UseVisualStyleBackColor = true;
			// 
			// panelServer
			// 
			this.panelServer.Controls.Add(this.radioButtonProd);
			this.panelServer.Controls.Add(this.radioButtonStage);
			this.panelServer.Controls.Add(this.radioButtonDev);
			this.panelServer.Location = new System.Drawing.Point(74, 64);
			this.panelServer.Name = "panelServer";
			this.panelServer.Size = new System.Drawing.Size(500, 25);
			this.panelServer.TabIndex = 7;
			// 
			// radioButtonProd
			// 
			this.radioButtonProd.AutoSize = true;
			this.radioButtonProd.Location = new System.Drawing.Point(202, 5);
			this.radioButtonProd.Name = "radioButtonProd";
			this.radioButtonProd.Size = new System.Drawing.Size(76, 17);
			this.radioButtonProd.TabIndex = 2;
			this.radioButtonProd.Text = "Production";
			this.toolTipMain.SetToolTip(this.radioButtonProd, "Select if want to update the production server");
			this.radioButtonProd.UseVisualStyleBackColor = true;
			// 
			// radioButtonStage
			// 
			this.radioButtonStage.AutoSize = true;
			this.radioButtonStage.Location = new System.Drawing.Point(111, 6);
			this.radioButtonStage.Name = "radioButtonStage";
			this.radioButtonStage.Size = new System.Drawing.Size(61, 17);
			this.radioButtonStage.TabIndex = 1;
			this.radioButtonStage.Text = "Staging";
			this.toolTipMain.SetToolTip(this.radioButtonStage, "Select if want to update the staging server");
			this.radioButtonStage.UseVisualStyleBackColor = true;
			// 
			// radioButtonDev
			// 
			this.radioButtonDev.AutoSize = true;
			this.radioButtonDev.Location = new System.Drawing.Point(3, 6);
			this.radioButtonDev.Name = "radioButtonDev";
			this.radioButtonDev.Size = new System.Drawing.Size(88, 17);
			this.radioButtonDev.TabIndex = 0;
			this.radioButtonDev.Text = "Development";
			this.toolTipMain.SetToolTip(this.radioButtonDev, "Select if want to update the development server");
			this.radioButtonDev.UseVisualStyleBackColor = true;
			// 
			// labelServer
			// 
			this.labelServer.AutoSize = true;
			this.labelServer.Location = new System.Drawing.Point(12, 72);
			this.labelServer.Name = "labelServer";
			this.labelServer.Size = new System.Drawing.Size(41, 13);
			this.labelServer.TabIndex = 6;
			this.labelServer.Text = "&Server:";
			// 
			// openFileDialogXmlFileName
			// 
			this.openFileDialogXmlFileName.CheckFileExists = false;
			this.openFileDialogXmlFileName.DefaultExt = "xml";
			this.openFileDialogXmlFileName.Filter = "XML file|*.xml";
			// 
			// textBoxConfiguration
			// 
			this.textBoxConfiguration.Location = new System.Drawing.Point(74, 7);
			this.textBoxConfiguration.Name = "textBoxConfiguration";
			this.textBoxConfiguration.Size = new System.Drawing.Size(705, 20);
			this.textBoxConfiguration.TabIndex = 1;
			this.toolTipMain.SetToolTip(this.textBoxConfiguration, "Enter a path and name of XML file to read or save to");
			this.textBoxConfiguration.TextChanged += new System.EventHandler(this.TextBoxConfiguration_TextChanged);
			// 
			// buttonConfiguration
			// 
			this.buttonConfiguration.Location = new System.Drawing.Point(785, 4);
			this.buttonConfiguration.Name = "buttonConfiguration";
			this.buttonConfiguration.Size = new System.Drawing.Size(27, 23);
			this.buttonConfiguration.TabIndex = 2;
			this.buttonConfiguration.Text = "...";
			this.toolTipMain.SetToolTip(this.buttonConfiguration, "Click to find a XML file");
			this.buttonConfiguration.UseVisualStyleBackColor = true;
			this.buttonConfiguration.Click += new System.EventHandler(this.ButtonConfiguration_Click);
			// 
			// buttonSave
			// 
			this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonSave.Enabled = false;
			this.buttonSave.Location = new System.Drawing.Point(12, 582);
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.Size = new System.Drawing.Size(75, 23);
			this.buttonSave.TabIndex = 9;
			this.buttonSave.Text = "Sa&ve";
			this.toolTipMain.SetToolTip(this.buttonSave, "Click to save grid to XML file");
			this.buttonSave.UseVisualStyleBackColor = true;
			this.buttonSave.Click += new System.EventHandler(this.ButtonSave_Click);
			// 
			// radioButtonExportItems
			// 
			this.radioButtonExportItems.AutoSize = true;
			this.radioButtonExportItems.Location = new System.Drawing.Point(297, 3);
			this.radioButtonExportItems.Name = "radioButtonExportItems";
			this.radioButtonExportItems.Size = new System.Drawing.Size(83, 17);
			this.radioButtonExportItems.TabIndex = 3;
			this.radioButtonExportItems.TabStop = true;
			this.radioButtonExportItems.Text = "Export Items";
			this.radioButtonExportItems.UseVisualStyleBackColor = true;
			// 
			// buttonRefresh
			// 
			this.buttonRefresh.BackgroundImage = global::MassSitecore.Properties.Resources.Refresh;
			this.buttonRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.buttonRefresh.Location = new System.Drawing.Point(818, 4);
			this.buttonRefresh.Name = "buttonRefresh";
			this.buttonRefresh.Size = new System.Drawing.Size(29, 23);
			this.buttonRefresh.TabIndex = 3;
			this.toolTipMain.SetToolTip(this.buttonRefresh, "Click to re-read the current XML file");
			this.buttonRefresh.UseVisualStyleBackColor = true;
			this.buttonRefresh.Click += new System.EventHandler(this.ButtonRefresh_Click);
			// 
			// FormMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(861, 611);
			this.Controls.Add(this.buttonRefresh);
			this.Controls.Add(this.buttonSave);
			this.Controls.Add(this.buttonConfiguration);
			this.Controls.Add(this.textBoxConfiguration);
			this.Controls.Add(this.labelServer);
			this.Controls.Add(this.panelServer);
			this.Controls.Add(this.panelDirection);
			this.Controls.Add(this.labelDirection);
			this.Controls.Add(this.buttonGo);
			this.Controls.Add(this.dataGridViewItems);
			this.Controls.Add(this.labelXmlFile);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(877, 649);
			this.Name = "FormMain";
			this.Text = "Mass Sitecore Item Data";
			this.Load += new System.EventHandler(this.FormMain_Load);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewItems)).EndInit();
			this.panelDirection.ResumeLayout(false);
			this.panelDirection.PerformLayout();
			this.panelServer.ResumeLayout(false);
			this.panelServer.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label labelXmlFile;
		private System.Windows.Forms.DataGridView dataGridViewItems;
		private System.Windows.Forms.Button buttonGo;
		private System.Windows.Forms.DataGridViewTextBoxColumn pathDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn fieldDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn valueDataGridViewTextBoxColumn;
		private System.Windows.Forms.Label labelDirection;
		private System.Windows.Forms.Panel panelDirection;
		private System.Windows.Forms.RadioButton radioButtonImport;
		private System.Windows.Forms.RadioButton radioButtonExportValues;
		private System.Windows.Forms.Panel panelServer;
		private System.Windows.Forms.RadioButton radioButtonProd;
		private System.Windows.Forms.RadioButton radioButtonStage;
		private System.Windows.Forms.RadioButton radioButtonDev;
		private System.Windows.Forms.Label labelServer;
		private System.Windows.Forms.OpenFileDialog openFileDialogXmlFileName;
		private System.Windows.Forms.TextBox textBoxConfiguration;
		private System.Windows.Forms.Button buttonConfiguration;
		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Button buttonRefresh;
		private System.Windows.Forms.ToolTip toolTipMain;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPath;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnField;
		private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnAllLanguage;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnValue;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSpanish;
		private System.Windows.Forms.RadioButton radioButtonExportFields;
        private System.Windows.Forms.RadioButton radioButtonExportItems;
	}
}

