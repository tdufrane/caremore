﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace MassSitecore
{
	public class SitecoreItem : INotifyPropertyChanged
	{
		#region Constructors

		public SitecoreItem()
		{
		}

		public SitecoreItem(SitecoreData.DataItem item)
		{
			this._path = item.Path;
			this._field = item.Field;
			this._allLanguages = item.AllLanguages;
			this._value = item.Value;
			this._valueSpanish = item.ValueSpanish;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Sitecore Item Path
		/// </summary>
		private string _path;
		public string Path
		{
			get { return this._path; }
			set
			{
				this._path = value;
				this.NotifyPropertyChanged("Path");
			}
		}

		/// <summary>
		/// Sitecore Item Data Field
		/// </summary>
		private string _field;
		public string Field
		{
			get { return this._field; }
			set
			{
				this._field = value;
				this.NotifyPropertyChanged("Field");
			}
		}

		/// <summary>
		/// Whether to 'get Value from' or 'apply Value to' all languages
		/// </summary>
		private bool _allLanguages = true;
		public bool AllLanguages
		{
			get { return this._allLanguages; }
			set
			{
				this._allLanguages = value;
				this.NotifyPropertyChanged("AllLanguages");
			}
		}

		/// <summary>
		/// Sitecore Item Data Value - English
		/// </summary>
		private string _value = string.Empty;
		public string Value
		{
			get { return this._value; }
			set
			{
				this._value = value;
				this.NotifyPropertyChanged("Value");
			}
		}

		/// <summary>
		/// Sitecore Item Data Value - Spanish
		/// </summary>
		private string _valueSpanish = string.Empty;
		public string ValueSpanish
		{
			get { return this._valueSpanish; }
			set
			{
				this._valueSpanish = value;
				this.NotifyPropertyChanged("ValueSpanish");
			}
		}

		#endregion

		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged(string name)
		{
			if (PropertyChanged != null)
				PropertyChanged(this, new PropertyChangedEventArgs(name));
		}

		#endregion
	}
}
