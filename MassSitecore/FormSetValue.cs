﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MassSitecore
{
	public partial class FormSetValue : Form
	{
		#region Propeties

		public string ColumnTitle { get; set; }
		public string ColumnValue { get; set; }
		public bool OkPressed { get; set; }

		#endregion

		#region Form events

		public FormSetValue()
		{
			InitializeComponent();
		}

		private void FormSetValue_Shown(object sender, EventArgs e)
		{
			this.labelColumnName.Text = this.ColumnTitle + ":";
		}

		#endregion

		#region Control events

		private void ButtonOk_Click(object sender, EventArgs e)
		{
			this.ColumnValue = this.textBox1.Text;
			this.OkPressed = true;
			this.Close();
		}

		private void ButtonCancel_Click(object sender, EventArgs e)
		{
			this.ColumnValue = string.Empty;
			this.OkPressed = false;
			this.Close();
		}

		#endregion
	}
}
