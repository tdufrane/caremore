﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace MassSitecore
{
	public partial class FormMain : Form
	{
		#region Private variables

		private SitecoreItems items = new SitecoreItems();

		#endregion

		#region Form events

		public FormMain()
		{
			InitializeComponent();
			ColumnAllLanguage.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
		}

		private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
		{
			FormData.Default.LastXmlFileName = this.textBoxConfiguration.Text;

			if (this.radioButtonExportValues.Checked)
				FormData.Default.LastDirection = 0;
			else if (this.radioButtonExportFields.Checked)
				FormData.Default.LastDirection = 1;
			else
				FormData.Default.LastDirection = 2;

			if (this.radioButtonDev.Checked)
				FormData.Default.LastServer = 0;
			else if (this.radioButtonStage.Checked)
				FormData.Default.LastServer = 1;
			else
				FormData.Default.LastServer = 2;

			FormData.Default.Save();
		}

		private void FormMain_Load(object sender, EventArgs e)
		{
			this.textBoxConfiguration.Text = FormData.Default.LastXmlFileName;

			if (FormData.Default.LastDirection == 2)
				this.radioButtonImport.Checked = true;
			else if (FormData.Default.LastDirection == 1)
				this.radioButtonExportFields.Checked = true;
			else
				this.radioButtonExportValues.Checked = true;

			if (FormData.Default.LastServer == 1)
				this.radioButtonStage.Checked = true;
			else if (FormData.Default.LastServer == 2)
				this.radioButtonProd.Checked = true;
			else
				this.radioButtonDev.Checked = true;

			this.dataGridViewItems.DataSource = this.items;

			VerifyForm();
		}

		#endregion

		#region Control events

		private void ButtonConfiguration_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(this.textBoxConfiguration.Text))
			{
				SetDefaultPath();
			}
			else
			{
				// Parse directory
				int lastSlashPosition = this.textBoxConfiguration.Text.LastIndexOf('\\');
				if (lastSlashPosition == -1)
				{
					SetDefaultPath();
				}
				else
				{
					string path = this.textBoxConfiguration.Text.Substring(0, lastSlashPosition);

					if (Directory.Exists(path))
						openFileDialogXmlFileName.InitialDirectory = path;
					else
						SetDefaultPath();
				}
			}

			DialogResult result = openFileDialogXmlFileName.ShowDialog();

			if (result == DialogResult.OK)
			{
				this.textBoxConfiguration.Text = openFileDialogXmlFileName.FileName;
			}
		}

		private void ButtonGo_Click(object sender, EventArgs e)
		{
			string message = string.Empty;
			this.Cursor = Cursors.WaitCursor;

			try
			{
				if (this.items.Count == 0)
				{
					MessageBox.Show("No items to process, please add at least one item",
						"Missing Data", MessageBoxButtons.OK,
						MessageBoxIcon.Exclamation);
				}
				else
				{
					ContactSitecore();

					if (this.radioButtonExportValues.Checked)
						message = "Values exported";
					else if (this.radioButtonExportFields.Checked)
						message = "Fields exported";
					else
						message = "Values imported, please verify in Sitecore";

					MessageBox.Show(message,
						"Completed", MessageBoxButtons.OK,
						MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				if (this.radioButtonExportValues.Checked)
					message = string.Format("Export of values failed:\n{0}", ex);
				else if (this.radioButtonExportFields.Checked)
					message = string.Format("Export of fields failed:\n{0}", ex);
				else
					message = string.Format("Import of values failed:\n{0}", ex);

				MessageBox.Show(message,
					"Error", MessageBoxButtons.OK,
					MessageBoxIcon.Error);
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}
		}

		private void ButtonRefresh_Click(object sender, EventArgs e)
		{
			items.Clear();
			LoadGrid();
			VerifyForm();
		}

		private void ButtonSave_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(this.textBoxConfiguration.Text))
			{
				MessageBox.Show("Please enter or select a XML file in Configuration.",
					"Input Required", MessageBoxButtons.OK,
					MessageBoxIcon.Information);
			}
			else
			{
				items.ToXml(this.textBoxConfiguration.Text);
			}
		}

		private void DataGridViewItems_ColumnHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
		{
			string columnName = this.dataGridViewItems.Columns[e.ColumnIndex].HeaderText;

			FormSetValue setValueForm = new FormSetValue();
			setValueForm.ColumnTitle = columnName + ":";
			setValueForm.ShowDialog();

			if (setValueForm.OkPressed)
			{
				string newValue = setValueForm.ColumnValue;
				foreach (SitecoreItem item in this.items)
				{
					switch (columnName)
					{
						case "Path":
							item.Path = newValue;
							break;
						case "Field":
							item.Field = newValue;
							break;
						case "English":
							item.Value = newValue;
							break;
						case "Spanish":
							item.ValueSpanish = newValue;
							break;
						default:
							break;
					}
				}
			}

			setValueForm = null;
		}

		private void DataGridViewItems_UserRowChanged(object sender, DataGridViewRowEventArgs e)
		{
			VerifyForm();
			this.dataGridViewItems.AllowUserToAddRows = true;  // Reset for bug fix
		}

		private void DataGridViewItems_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
		{
			// For bug where only one row left, UserDeletedRow does not fire unless the following
			this.dataGridViewItems.AllowUserToAddRows = false;
		}

		private void TextBoxConfiguration_TextChanged(object sender, EventArgs e)
		{
			try
			{
				items.Clear();

				if (File.Exists(this.textBoxConfiguration.Text))
				{
					LoadGrid();
				}
			}
			finally
			{
				VerifyForm();
			}
		}

		#endregion

		#region Methods

		private void LoadGrid()
		{
			if (File.Exists(this.textBoxConfiguration.Text))
				items.FromXml(this.textBoxConfiguration.Text);
		}

		private void SetDefaultPath()
		{
			openFileDialogXmlFileName.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
		}

		private void ContactSitecore()
		{
			string server;

			if (radioButtonDev.Checked)
				server = ConfigurationManager.AppSettings["DevServiceUrl"];
			else if (radioButtonStage.Checked)
				server = ConfigurationManager.AppSettings["StageServiceUrl"];
			else
				server = ConfigurationManager.AppSettings["ProdServiceUrl"];

			SitecoreData.Data data = new MassSitecore.SitecoreData.Data();
			data.Url = server;

			if (this.radioButtonExportValues.Checked)
			{
				MassSitecore.SitecoreData.DataItem[] exportedItems = data.ExportValues(this.items.ToArray());

				this.items.Clear();

				foreach (MassSitecore.SitecoreData.DataItem item in exportedItems)
				{
					this.items.Add(new SitecoreItem(item));
				}
			}
			else if (this.radioButtonExportFields.Checked)
			{
				MassSitecore.SitecoreData.DataItem[] exportedItems = data.ExportFields(this.items[0].Path);

				this.items.Clear();

				foreach (MassSitecore.SitecoreData.DataItem item in exportedItems)
				{
					this.items.Add(new SitecoreItem(item));
				}
			}
			else if (this.radioButtonExportItems.Checked)
			{
				MassSitecore.SitecoreData.DataItem[] exportedItems = data.ExportItems(this.items[0].Path);

				foreach (MassSitecore.SitecoreData.DataItem item in exportedItems)
				{
					this.items.Add(new SitecoreItem(item));
				}
			}
			else
			{
				data.ImportValues(this.items.ToArray());
			}
		}

		private void VerifyForm()
		{
			if (string.IsNullOrEmpty(this.textBoxConfiguration.Text))
				this.buttonSave.Enabled = false;
			else
				this.buttonSave.Enabled = true;

			if (this.dataGridViewItems.Rows.Count == 0)
				this.buttonGo.Enabled = false;
			else
				this.buttonGo.Enabled = true;
		}

		#endregion
	}
}
