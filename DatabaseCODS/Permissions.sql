﻿grant execute on [dbo].[CM_PT_CMC_AllProvidersList] to [extapp_RO]
go
grant execute on [dbo].[CM_PT_CMC_AllProvidersList] to [extapp_RW]
go
grant execute on [dbo].[CM_PT_CMC_FacilityDetail] to [extapp_RO]
go
grant execute on [dbo].[CM_PT_CMC_FacilityDetail] to [extapp_RW]
go
grant execute on [dbo].[CM_PT_CMC_FacilitySearch] to [extapp_RO]
go
grant execute on [dbo].[CM_PT_CMC_FacilitySearch] to [extapp_RW]
go
grant execute on [dbo].[CM_PT_CMC_ProviderAccessibility] to [extapp_RO]
go
grant execute on [dbo].[CM_PT_CMC_ProviderAccessibility] to [extapp_RW]
go
grant execute on [dbo].[CM_PT_CMC_ProviderAddresses] to [extapp_RO]
go
grant execute on [dbo].[CM_PT_CMC_ProviderAddresses] to [extapp_RW]
go
grant execute on [dbo].[CM_PT_CMC_ProviderDetail] to [extapp_RO]
go
grant execute on [dbo].[CM_PT_CMC_ProviderDetail] to [extapp_RW]
go
grant execute on [dbo].[CM_PT_CMC_ProviderHours] to [extapp_RO]
go
grant execute on [dbo].[CM_PT_CMC_ProviderHours] to [extapp_RW]
go
grant execute on [dbo].[CM_PT_CMC_ProviderLanguages] to [extapp_RO]
go
grant execute on [dbo].[CM_PT_CMC_ProviderLanguages] to [extapp_RW]
go
grant execute on [dbo].[CM_PT_CMC_ProviderSearch] to [extapp_RO]
go
grant execute on [dbo].[CM_PT_CMC_ProviderSearch] to [extapp_RW]
go
grant exec on [dbo].[CM_PT_CMC_ProviderServices] to [extapp_RO]
go
grant exec on [dbo].[CM_PT_CMC_ProviderServices] to [extapp_RW]
go
grant execute on [dbo].[CM_PT_CMC_ProvidersLastUpdated] to [extapp_RO]
go
grant execute on [dbo].[CM_PT_CMC_ProvidersLastUpdated] to [extapp_RW]
go
grant execute on [dbo].[CM_PT_CMC_SpecMapItem] to [extapp_RO]
go
grant execute on [dbo].[CM_PT_CMC_SpecMapItem] to [extapp_RW]
go
grant execute on [dbo].[CM_PT_CMC_SpecMapSynch] to [extapp_RO]
go
grant execute on [dbo].[CM_PT_CMC_SpecMapSynch] to [extapp_RW]
go
grant execute on [dbo].[CM_PT_CMC_SpecMapUpdate] to [extapp_RO]
go
grant execute on [dbo].[CM_PT_CMC_SpecMapUpdate] to [extapp_RW]
go
grant execute on [dbo].[CM_PT_CMC_ZipsWithinDistance] to [extapp_RO]
go
grant execute on [dbo].[CM_PT_CMC_ZipsWithinDistance] to [extapp_RW]
go
