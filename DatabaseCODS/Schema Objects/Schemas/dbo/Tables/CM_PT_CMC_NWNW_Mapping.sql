﻿-- PORTAL 0 = CareMore
--        1 = Empire MediBlue (Kings County)
--        2 = Anthem MediBlue (Richmond City)
--        3 = MMP-Medicare (Los Angeles County)
--        4 = MMP-Medicare (Santa Clara County)
--        5 = MMP-Medicaid (Richmond City)
CREATE TABLE [dbo].[CM_PT_CMC_NWNW_Mapping]
(
	[NWNW_ID] CHAR(12)  NOT NULL,
	[PORTAL]  TINYINT   NOT NULL,
	CONSTRAINT [PK_CM_PT_CMC_GRGR_Mapping] PRIMARY KEY CLUSTERED ([NWNW_ID] ASC, [PORTAL] ASC)
)
