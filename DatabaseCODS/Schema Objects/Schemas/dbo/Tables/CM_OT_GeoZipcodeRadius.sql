﻿CREATE TABLE [dbo].[CM_OT_GeoZipcodeRadius] (
    [BaseZipCode]   CHAR (5)   NOT NULL,
    [ToZipCode]     CHAR (5)   NOT NULL,
    [DistanceMiles] FLOAT (53) NOT NULL,
    CONSTRAINT [PK_GeoZipcodeRadius] PRIMARY KEY CLUSTERED ([BaseZipCode] ASC, [ToZipCode] ASC)
);

