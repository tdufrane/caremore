﻿CREATE TABLE [dbo].[CMC_PROF_OFF_HRS] (
    [PRPR_ID]         CHAR (12) NOT NULL,
    [PRAD_ID]         CHAR (12) NOT NULL,
    [PRAD_TYPE]       CHAR (3)  NOT NULL,
    [PRAD_EFF_DT]     DATETIME  NOT NULL,
    [PROF_SEQ_NO]     SMALLINT  NOT NULL,
    [PROF_DAYS]       CHAR (12) NOT NULL,
    [PROF_FROM_HR1]   CHAR (8)  NOT NULL,
    [PROF_TO_HR1]     CHAR (8)  NOT NULL,
    [PROF_FROM_HR2]   CHAR (8)  NOT NULL,
    [PROF_TO_HR2]     CHAR (8)  NOT NULL,
    [PROF_LOCK_TOKEN] SMALLINT  NOT NULL,
    [ATXR_SOURCE_ID]  DATETIME  NOT NULL,
    CONSTRAINT [CMCX_PROF_PRIMARY] PRIMARY KEY CLUSTERED ([PRPR_ID] ASC, [PRAD_ID] ASC, [PRAD_TYPE] ASC, [PRAD_EFF_DT] ASC, [PROF_SEQ_NO] ASC)
);

