﻿CREATE TABLE [dbo].[CM_PT_CMC_MaterialMapping]
(
	[PDPD_ID]           CHAR(8) NOT NULL,
	[MaterialAvailable] BIT     NOT NULL,
	CONSTRAINT [PK_CM_PT_CMC_MaterialMapping] PRIMARY KEY CLUSTERED ([PDPD_ID] ASC)
)
