﻿CREATE TABLE [dbo].[CM_PT_DUALS_Accessibility]
(
	[AccessibilityId]    CHAR(4)  NOT NULL,
	[LimitedAccessMain]  BIT      NOT NULL,
	[BasicAccessMain]    BIT      NOT NULL,
	[BasicAccessSub]     BIT      NOT NULL,
	[DoNotDisplay]       BIT      NOT NULL,
	[MappingCode]        CHAR(4)  NOT NULL,
	CONSTRAINT [PK_CM_PT_DUALS_Accessibility] PRIMARY KEY CLUSTERED ([AccessibilityId] ASC)
)
