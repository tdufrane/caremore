﻿CREATE TABLE [dbo].[XREF_CSPI_HPCODE] (
    [CSPI_ID]         VARCHAR (8)   NOT NULL,
    [PDDS_DESC]       VARCHAR (70)  NOT NULL,
    [LOBD_ID]         VARCHAR (4)   NOT NULL,
    [LOBD_NAME]       VARCHAR (50)  NOT NULL,
    [PYPY_ID]         VARCHAR (8)   NOT NULL,
    [PYPY_PAYOR_NAME] VARCHAR (50)  NOT NULL,
    [PYPY_TAX_ID]     VARCHAR (9)   NOT NULL,
    [ContractID]      VARCHAR (5)   NOT NULL,
    [PBPNumber]       VARCHAR (3)   NOT NULL,
    [CompanyID]       VARCHAR (4)   NOT NULL,
    [HPCode]          VARCHAR (4)   NOT NULL,
    [ActiveHPCode]    BIT           NOT NULL,
    [PlanPhoneNumber] VARCHAR (20)  NULL,
    [County]          VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_XREF_CSPI_HPCODE] PRIMARY KEY CLUSTERED ([CSPI_ID] ASC, [HPCode] ASC) WITH (FILLFACTOR = 90)
);


GO

-- Add trigger
create trigger trXREF_CSPI_HPCODE
on XREF_CSPI_HPCODE
	for insert, update
as
	declare @inCSPI_ID varchar(8), @inActiveHPCode bit

	select @inCSPI_ID = CSPI_ID, @inActiveHPCode = ActiveHPCode
	from inserted

	if (@inActiveHPCode = 1)
	begin
		if exists (select CSPI_ID
		           from XREF_CSPI_HPCODE
		           where (CSPI_ID = @inCSPI_ID) and (ActiveHPCode = 1)
		           group by CSPI_ID
		           having count(HPCode) > 1)
		begin
			raiserror ('Duplicatation of CSPI_ID where ActiveHPCode = 1 is not allowed', 16, 1)
			rollback transaction
		end
	end