CREATE TABLE [dbo].[CM_PT_CMC_SpecialtyMapping]
(
	SpecialtyMappingId  varchar(4)    NOT NULL,
	FacetsDescription   varchar(255)  NOT NULL,
	DisplayOnWebsite    bit           NOT NULL,
	WebsiteDescription  varchar(255)  NULL,
	CONSTRAINT [PK_CM_PT_CMC_SpecialtyMapping] PRIMARY KEY CLUSTERED (SpecialtyMappingId ASC)
)
