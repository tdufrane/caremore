create view [dbo].[vwCMC_Specialties]
as

select distinct
	SpecialtyMappingId as Code,
	isnull(WebsiteDescription, FacetsDescription) as Description
from
	CM_PT_CMC_SpecialtyMapping
where
	(DisplayOnWebsite = 1)
