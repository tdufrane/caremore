﻿create view [dbo].[vwCMC_Facilities]
as

select distinct
	prpr.PRPR_ID as ProviderId,
	rtrim(prpr.PRPR_NAME) as Name,
	rtrim(prpr.PRPR_NPI) as NpiId,
	rtrim(prpr.PRCP_ID) as CredentialingId,
	rtrim(prpr.PRPR_MCTR_TYPE) as ClassCode,
	rtrim(mctr_class.MCTR_DESC) as ClassDescription,
	prpr.PRAD_ID as AddressId,
	prad.PRAD_TYPE as AddressType,
	prad.PRAD_EFF_DT as AddressEffectiveDate,
	rtrim(prad.PRAD_ADDR1) as Address1,
	rtrim(prad.PRAD_ADDR2) as Address2,
	rtrim(prad.PRAD_ADDR3) as Address3,
	rtrim(prad.PRAD_CITY) as City,
	rtrim(prad.PRAD_COUNTY) as County,
	rtrim(prad.PRAD_STATE) as State,
	rtrim(mctr_state.MCTR_DESC) as StateName,
	rtrim(prad.PRAD_ZIP) as Zip,
	rtrim(prad.PRAD_PHONE) as Phone,
	rtrim(prad.PRAD_FAX) as Fax,
	cast(case when prad.PRAD_HD_IND = 'Y'
	          then 1
	          else 0 end as bit) as Accessibility,
	rtrim(prpr.PRCF_MCTR_SPEC) as PrimarySpecialtyCode,
	rtrim(prpr.PRPR_MCTR_LANG) as PrimaryLanguageCode
from
	CMC_PRPR_PROV prpr
	join CMC_MCTR_CD_TRANS mctr_class
		on prpr.PRPR_MCTR_TYPE = mctr_class.MCTR_VALUE and
			((mctr_class.MCTR_ENTITY = 'FAC' and rtrim(prpr.PRPR_MCTR_TYPE) not in ('CCC', 'PRO')) or
			 (mctr_class.MCTR_ENTITY = 'PRAC' and rtrim(prpr.PRPR_MCTR_TYPE) in ('CCC', 'PRO'))) and
			mctr_class.MCTR_TYPE = 'TYPE'
	join CMC_PRAD_ADDRESS prad
		on prpr.PRAD_ID = prad.PRAD_ID and
		   prad.PRAD_DIRECTORY_IND = 'I' and
		   prad.PRAD_TERM_DT > getdate()
	join CMC_MCTR_CD_TRANS mctr_state
		on prad.PRAD_STATE = mctr_state.MCTR_VALUE and
			mctr_state.MCTR_ENTITY = '#ALL' and
			mctr_state.MCTR_TYPE   = 'ST  '
where
	prpr.PRPR_ENTITY in ('F', 'G') and
	prpr.PRPR_TERM_DT > getdate() and
	prpr.PRPR_ID in (select nwpr.PRPR_ID
	                 from CMC_NWPR_RELATION nwpr
	                 where nwpr.NWPR_TERM_DT > getdate())
