create view [dbo].[vwCMC_Languages]
as

select distinct
	rtrim(prpr.PRPR_MCTR_LANG) as Code,
	rtrim(mctr.MCTR_DESC) as Description
from
	CMC_PRPR_PROV prpr
	join CMC_MCTR_CD_TRANS mctr
		on prpr.PRPR_MCTR_LANG = mctr.MCTR_VALUE and
		   mctr.MCTR_ENTITY = 'ALL' and
		   mctr.MCTR_TYPE = 'LANG'

union

select distinct
	rtrim(prla.PRLA_MCTR_LANG) as Code,
	rtrim(mctr.MCTR_DESC) as Description
from
	CMC_PRPR_PROV prpr
	join CMC_PRLA_LANG prla
		on prpr.PRCR_ID = prla.PRCR_ID
	join CMC_MCTR_CD_TRANS mctr
		on prla.PRLA_MCTR_LANG = mctr.MCTR_VALUE and
		   mctr.MCTR_ENTITY = 'ALL' and
		   mctr.MCTR_TYPE = 'LANG'
