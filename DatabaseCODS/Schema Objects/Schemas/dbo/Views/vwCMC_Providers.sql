create view [dbo].[vwCMC_Providers]
as

select distinct
	prpr.PRPR_ID as ProviderId,
	rtrim(prpr.PRPR_NAME) as Name,
	rtrim(prpr.PRPR_NPI) as NpiId,
	rtrim(prpr.PRCP_ID) as CredentialingId,
	rtrim(isnull(prcp.PRCP_TITLE, '')) as Title,
	rtrim(prpr.PRPR_MCTR_TYPE) as ClassCode,
	rtrim(mctr_class.MCTR_DESC) as ClassDescription,
	prpr.PRAD_ID as AddressId,
	prad.PRAD_TYPE as AddressType,
	prad.PRAD_EFF_DT as AddressEffectiveDate,
	rtrim(prad.PRAD_ADDR1) as Address1,
	rtrim(prad.PRAD_ADDR2) as Address2,
	rtrim(prad.PRAD_ADDR3) as Address3,
	rtrim(prad.PRAD_CITY) as City,
	rtrim(prad.PRAD_COUNTY) as County,
	rtrim(prad.PRAD_STATE) as State,
	rtrim(mctr_state.MCTR_DESC) as StateName,
	rtrim(prad.PRAD_ZIP) as Zip,
	rtrim(prad.PRAD_PHONE) as Phone,
	rtrim(prad.PRAD_FAX) as Fax,
	cast(case when prad.PRAD_HD_IND = 'Y'
	          then 1
	          else 0 end as bit) as Accessibility,
	rtrim(prpr.PRCF_MCTR_SPEC) as PrimarySpecialtyCode,
	rtrim(mctr_pspec.MCTR_DESC) as PrimarySpecialty,
	rtrim(prpr.PRPR_MCTR_LANG) as PrimaryLanguageCode,
	rtrim(mctr_plang.MCTR_DESC) as PrimaryLanguage,
	rtrim(prpr.PRCF_MCTR_SPEC2) as SecondarySpecialtyCode,
	rtrim(mctr_sspec.MCTR_DESC) as SecondarySpecialty,
	rtrim(prpr.PRPR_MCTR_VAL2) as PanelClosedBy
from
	CMC_PRPR_PROV prpr
	left join CMC_PRCP_COMM_PRAC prcp
		on prpr.PRCP_ID = prcp.PRCP_ID
	join CMC_MCTR_CD_TRANS mctr_class
		on prpr.PRPR_MCTR_TYPE = mctr_class.MCTR_VALUE and
		   mctr_class.MCTR_ENTITY = 'PRAC' and
		   mctr_class.MCTR_TYPE = 'TYPE'
	join CMC_PRAD_ADDRESS prad
		on prpr.PRAD_ID = prad.PRAD_ID and
		   prad.PRAD_DIRECTORY_IND = 'I' and
		   prad.PRAD_TERM_DT > getdate()
	join CMC_MCTR_CD_TRANS mctr_plang
		on prpr.PRPR_MCTR_LANG = mctr_plang.MCTR_VALUE and
		   mctr_plang.MCTR_ENTITY = 'ALL' and
		   mctr_plang.MCTR_TYPE = 'LANG'
	join CMC_MCTR_CD_TRANS mctr_pspec
		on prpr.PRCF_MCTR_SPEC = mctr_pspec.MCTR_VALUE and
		   mctr_pspec.MCTR_ENTITY = 'PRAC' and
		   mctr_pspec.MCTR_TYPE = 'SPEC'
	join CMC_MCTR_CD_TRANS mctr_sspec
		on prpr.PRCF_MCTR_SPEC2 = mctr_sspec.MCTR_VALUE and
		   mctr_sspec.MCTR_ENTITY = 'PRAC' and
		   mctr_sspec.MCTR_TYPE = 'SPEC'
	join CMC_MCTR_CD_TRANS mctr_state
		on prad.PRAD_STATE = mctr_state.MCTR_VALUE and
			mctr_state.MCTR_ENTITY = '#ALL' and
			mctr_state.MCTR_TYPE   = 'ST  '
where
	prpr.PRPR_ENTITY = 'P' and
	prpr.PRPR_TERM_DT > getdate() and
	prpr.PRPR_ID in (select nwpr.PRPR_ID
	                 from CMC_NWPR_RELATION nwpr
	                 where nwpr.NWPR_TERM_DT > getdate())
