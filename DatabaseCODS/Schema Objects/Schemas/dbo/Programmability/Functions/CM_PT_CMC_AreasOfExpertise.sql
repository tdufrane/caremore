﻿create function [dbo].[CM_PT_CMC_AreasOfExpertise]
(
	@ProviderId       char(12),
	@AddressId        char(12),
	@AddressType      char(3),
	@CredentialingId  char(12),
	@ServicesOnly     bit
)
returns varchar(max)
as
begin
	declare @Expertise varchar(max)

	if (@ServicesOnly = 1)
	begin
		select
			@Expertise = isnull(@Expertise + ', ', '') + (mctr.MCTR_DESC)
		from
			CMC_PRSR_LOC_SVCS prsr
			join CMC_MCTR_CD_TRANS mctr
				on prsr.PRSR_MCTR_SVCS = mctr.MCTR_VALUE and
				   mctr.MCTR_ENTITY = 'PRAC' and
				   mctr.MCTR_TYPE = 'SVCS'
		where
			(PRPR_ID   = @ProviderId) and
			(PRAD_ID   = @AddressId) and
			(PRAD_TYPE = @AddressType) and
			(PRAD_EFF_DT <= getdate())
		order by
			mctr.MCTR_DESC
	end

	else
	begin
		select
			@Expertise = isnull(@Expertise + ', ', '') + (prae.PRAE_DESC)
		from
			CM_PRCP_AREA_OF_EXPERTISE prcp
			join CM_PRAE_CATEGORY prae
				on prcp.PRAE_VALUE = prae.PRAE_VALUE and
				   prcp.PRAE_ENTITY = prae.PRAE_ENTITY and
				   prcp.PRAE_TYPE = prae.PRAE_TYPE
		where
			(prcp.PRCP_ID = @CredentialingId)
	end

	return isnull(@Expertise, '')
end
