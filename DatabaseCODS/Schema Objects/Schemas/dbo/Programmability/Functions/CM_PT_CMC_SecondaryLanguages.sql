﻿create function [dbo].[CM_PT_CMC_SecondaryLanguages]
(
	@ProviderId    char(12),
	@AddressId     char(12)
)
returns varchar(max)
as
begin
	declare @Languages varchar(max)

	select
		@Languages = isnull(@Languages + ', ', '') + (rtrim(MCTR_DESC))
	from
	(
		select
			mctr.MCTR_DESC
		from
			CMC_PRPR_PROV prpr
			join CMC_PRLA_LANG prla
				on prpr.PRCR_ID = prla.PRCR_ID
			join CMC_MCTR_CD_TRANS mctr
				on prla.PRLA_MCTR_LANG = mctr.MCTR_VALUE and
				   mctr.MCTR_ENTITY = 'ALL' and
				   mctr.MCTR_TYPE = 'LANG'
		where
			(prpr.PRPR_ID   = @ProviderId) and
			(prpr.PRAD_ID   = @AddressId) and
			(prpr.PRPR_MCTR_LANG <> prla.PRLA_MCTR_LANG)
	) lang

	return isnull(@Languages, '')
end
