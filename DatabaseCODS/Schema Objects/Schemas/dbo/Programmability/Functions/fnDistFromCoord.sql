-- Returns the distance between 2 latitude/longitude pairs
create function fnDistFromCoord
(
	@Latitude_1   float,
	@Longitude_1  float,
	@Latitude_2   float,
	@Longitude_2  float
)
returns float
as
begin
	declare @Distance float

	if (@Latitude_1 = @Latitude_2) and (@Longitude_1 = @Longitude_2)
	  begin
		set @Distance = 0
	  end
	else
	  begin
		set @Distance = (sin(radians(@Latitude_1)) *
		                 sin(radians(@Latitude_2)) +
		                 cos(radians(@Latitude_1)) *
		                 cos(radians(@Latitude_2)) *
		                 cos(radians(@Longitude_1 - @Longitude_2))
		                )

		set @Distance = (degrees(acos(@Distance))) * 69.09
	  end

	return @Distance
end
