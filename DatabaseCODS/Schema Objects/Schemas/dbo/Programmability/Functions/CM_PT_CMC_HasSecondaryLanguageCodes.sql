﻿create function [dbo].[CM_PT_CMC_HasSecondaryLanguageCodes]
(
	@ProviderId     char(12),
	@AddressId      char(12),
	@LanguageCodes  varchar(100)
)
returns bit
as
begin
	declare @CodeCount int

	select @CodeCount = count(PRLA_MCTR_LANG)
	from
		CMC_PRPR_PROV prpr
		join CMC_PRLA_LANG prla
			on prpr.PRCR_ID = prla.PRCR_ID
	where
		(prpr.PRPR_ID   = @ProviderId) and
		(prpr.PRAD_ID   = @AddressId) and
		(prpr.PRPR_MCTR_LANG <> prla.PRLA_MCTR_LANG) and
		(',' + @LanguageCodes + ',' like '%,' + rtrim(PRLA_MCTR_LANG) + ',%')

	return @CodeCount
end
