-- Returns the details for a single provider
create procedure [dbo].[CM_PT_CMC_ProviderDetail]
(
	@Portal      tinyint,
	@ProviderId  char(12),
	@AddressType char(3)
)
as
	set nocount on;

	select top 1
		ProviderId,
		NpiId,
		AddressId,
		AddressEffectiveDate,
		Name,
		Title,
		Address1,
		Address2,
		Address3,
		City,
		County,
		State,
		Zip,
		Phone,
		Fax,
		isnull(sm1.WebsiteDescription, sm1.FacetsDescription) as PrimarySpecialty,
		isnull(sm2.WebsiteDescription, sm2.FacetsDescription) as SecondarySpecialty,
		(
			select min(NWPR_EFF_DT)
			from CM_PT_CMC_NWNW_Mapping nwm
			join CMC_NWPR_RELATION nwpr
				on nwm.NWNW_ID = nwpr.NWNW_ID and
				   nwpr.PRPR_ID = prov.ProviderId
			where
				nwm.PORTAL = @Portal
		) as EffectiveDate,
		Accessibility,
		pcp.IsPCP,
		(
			select case when count(*) > 0 then 'Y' else 'N' end
			from CM_PT_CMC_NWNW_Mapping nwm
			join CMC_NWPR_RELATION nwpr
				on nwm.NWNW_ID = nwpr.NWNW_ID and
				   nwpr.PRPR_ID = prov.ProviderId
			where
				nwpr.NWPR_PCP_IND = case when prov.PrimarySpecialtyCode in ('FP','FPG','GP','IM','IMG') then 'Y' else 'N' end and
				nwpr.NWPR_TERM_DT > getdate() and
				nwpr.NWPR_ACC_PAT_IND = 'Y' and
				nwm.PORTAL = @Portal
		) as AcceptingPatients,
		rtrim(prer_prpr.PRPR_NAME) as GroupProviderName,
		prov.PanelClosedBy,
		isnull(prsq.PRSQ_COMMENT, '') as Transportation,
		[CM_PT_CMC_AreasOfExpertise]
		(
			prov.ProviderId,
			prov.AddressId,
			prov.AddressType,
			prov.CredentialingId,
			case when prov.ClassCode = 'CCC' then 1 else 0 end
		) as SpecializedTraining,
		isnull(prcf.PRCF_STS, 'U') as CredentialingStatus
	from
		vwCMC_Providers prov
		cross apply
		(
			select cast(case when count(*) > 0 then 1 else 0 end as bit) as IsPCP
			from CM_PT_CMC_NWNW_Mapping nwm
			join CMC_NWPR_RELATION nwpr
				on nwm.NWNW_ID = nwpr.NWNW_ID and
				   nwpr.PRPR_ID = prov.ProviderId
			where
				prov.PrimarySpecialtyCode in ('FP','FPG','GP','IM','IMG') and
				nwpr.NWPR_TERM_DT > getdate() and
				nwpr.NWPR_PCP_IND = 'Y' and
				nwm.PORTAL = @Portal
		) pcp
		inner join CM_PT_CMC_SpecialtyMapping sm1
			on prov.PrimarySpecialtyCode = sm1.SpecialtyMappingId
		left join CM_PT_CMC_SpecialtyMapping sm2
			on prov.SecondarySpecialtyCode = sm2.SpecialtyMappingId
		left join CMC_PRER_RELATION prer
			on prov.ProviderId = prer.PRPR_ID and
			   prer.PRER_TERM_DT > getdate()
		left join CMC_PRPR_PROV prer_prpr
			on prer.PRER_PRPR_ID = prer_prpr.PRPR_ID
		left join CMC_PRSQ_SITE_QA prsq
			on prov.ProviderId = prsq.PRPR_ID and
			   prov.AddressId = prsq.PRAD_ID and
			   prov.AddressType = prsq.PRAD_TYPE and
			   prsq.PRSQ_MCTR_ITEM = 'PT'
		left join CMC_PRCF_CERT prcf
			on prov.CredentialingId = prcf.PRCR_ID and
			   prov.PrimarySpecialtyCode = prcf.PRCF_MCTR_SPEC
	where
		(ProviderId = @ProviderId) and
		(AddressType = isnull(@AddressType, AddressType)) and
		(@Portal in
			(
			select PORTAL
			from CM_PT_CMC_NWNW_Mapping nwm
			join CMC_NWPR_RELATION nwpr
				on nwm.NWNW_ID = nwpr.NWNW_ID and
				   nwpr.PRPR_ID = prov.ProviderId
			where
				nwpr.NWPR_TERM_DT > getdate()
			)
		)
	order by
		AddressType
