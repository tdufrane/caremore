﻿-- Returns information for a member who matches ID and available materials
create procedure [dbo].[CM_PT_CMC_MemberIdValidation]
	@MemberId char(9)
as
	select
		mepe.PDPD_ID as ProductPlanId,
		mm.MaterialAvailable
	from
		CMC_MEME_MEMBER meme
		inner join CMC_SBSB_SUBSC sbsb
			on meme.SBSB_CK = sbsb.SBSB_CK
		inner join CMC_MEPE_PRCS_ELIG mepe
			on meme.MEME_CK = mepe.MEME_CK
		inner join CM_PT_CMC_MaterialMapping mm
			on mepe.PDPD_ID = mm.PDPD_ID
	where
		(sbsb.SBSB_ID = @MemberId) and
		(getdate() between mepe.MEPE_EFF_DT and mepe.MEPE_TERM_DT) and
		(mepe.CSPD_CAT = 'M') and
		(mepe.MEPE_ELIG_IND = 'Y')
