﻿create procedure [dbo].[CM_PT_CMC_ZipsWithinDistance]
	@Zip         varchar(10),
	@MaxDistance float(53)
as
	select
		BaseZipCode, DistanceMiles
	from
		CM_OT_GeoZipcodeRadius
	where
		ToZipCode = @Zip and
		DistanceMiles between 0.0 and isnull(@MaxDistance, 25.0)
