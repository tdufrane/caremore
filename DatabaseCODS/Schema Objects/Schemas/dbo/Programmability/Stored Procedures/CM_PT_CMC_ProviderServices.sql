﻿-- Returns the services for a single provider
create procedure [dbo].[CM_PT_CMC_ProviderServices]
(
	@ProviderId    char(12),
	@AddressId     char(12),
	@AddressType   char(3)
)
as
	select
		MCTR_VALUE as ServiceCode,
		MCTR_DESC as [Service]
	from
		CMC_PRSR_LOC_SVCS prsr
		join CMC_MCTR_CD_TRANS mctr
			 on prsr.PRSR_MCTR_SVCS = mctr.MCTR_VALUE and
			    mctr.MCTR_ENTITY = 'PRAC' and
			    mctr.MCTR_TYPE = 'SVCS'
	where
		(PRPR_ID   = @ProviderId) and
		(PRAD_ID   = @AddressId) and
		(PRAD_TYPE = @AddressType) and
		(PRAD_EFF_DT <= getdate())
	order by
		mctr.MCTR_DESC
