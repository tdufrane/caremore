-- Updates a single SpecialtyMapping DisplayOnWebsite and WebsiteDescription columns for given Id
create procedure CM_PT_CMC_SpecMapUpdate
(
	@SpecialtyMappingId  varchar(4),
	@DisplayOnWebsite    bit,
	@WebsiteDescription  varchar(100)
)
as
	update
		CM_PT_CMC_SpecialtyMapping
	set
		DisplayOnWebsite   = @DisplayOnWebsite,
		WebsiteDescription = @WebsiteDescription
	where
		(SpecialtyMappingId = @SpecialtyMappingId)
