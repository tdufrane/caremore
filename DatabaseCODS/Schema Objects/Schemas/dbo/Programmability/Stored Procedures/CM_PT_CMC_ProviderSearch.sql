-- Returns a list of providers matching one or more filters
create procedure [dbo].[CM_PT_CMC_ProviderSearch]
(
	@Portal      tinyint,
	@LastName    varchar(103),
	@Zip         varchar(10),
	@City        varchar(30),
	@County      varchar(20),
	@State       varchar(2),
	@IsPCP       bit,
	@Specialties varchar(100),
	@Languages   varchar(100),
	@MaxDistance float(53)
)
as
	declare @SearchName varchar(105)

	set nocount on;

	if (@LastName is not null)
	begin
		select @LastName = replace(@LastName, '*', '%')

		if (charindex(@LastName, '%', 1) > 0)
			select @SearchName = @LastName
		else
			select @SearchName = @LastName + '%'
	end

	select distinct
		ProviderId,
		NpiId,
		AddressId,
		AddressType,
		AddressEffectiveDate,
		Name,
		Title,
		ClassDescription,
		Address1,
		Address2,
		Address3,
		City,
		County,
		State,
		Zip,
		Phone,
		isnull(sm1.WebsiteDescription, sm1.FacetsDescription) as PrimarySpecialty,
		isnull(sm2.WebsiteDescription, sm2.FacetsDescription) as SecondarySpecialty,
		isnull(DistanceMiles, 0.0) as Distance,
		(
			select min(NWPR_EFF_DT)
			from CM_PT_CMC_NWNW_Mapping nwm
			join CMC_NWPR_RELATION nwpr
				on nwm.NWNW_ID = nwpr.NWNW_ID and
				   nwpr.PRPR_ID = prov.ProviderId
			where
				nwm.PORTAL = @Portal
		) as EffectiveDate,
		pcp.IsPCP,
		(
			select case when count(*) > 0 then 'Y' else 'N' end
			from CM_PT_CMC_NWNW_Mapping nwm
			join CMC_NWPR_RELATION nwpr
				on nwm.NWNW_ID = nwpr.NWNW_ID and
				   nwpr.PRPR_ID = prov.ProviderId
			where
				nwpr.NWPR_PCP_IND = case when prov.PrimarySpecialtyCode in ('FP','FPG','GP','IM','IMG') then 'Y' else 'N' end and
				nwpr.NWPR_TERM_DT > getdate() and
				nwpr.NWPR_ACC_PAT_IND = 'Y' and
				nwm.PORTAL = @Portal
		) as AcceptingPatients,
		prov.PanelClosedBy,
		PrimaryLanguage,
		SecondaryLanguages = [CM_PT_CMC_SecondaryLanguages](prov.ProviderId, prov.AddressId)
	from
		vwCMC_Providers prov
		cross apply
		(
			select cast(case when count(*) > 0 then 1 else 0 end as bit) as IsPCP
			from CM_PT_CMC_NWNW_Mapping nwm
			join CMC_NWPR_RELATION nwpr
				on nwm.NWNW_ID = nwpr.NWNW_ID and
				   nwpr.PRPR_ID = prov.ProviderId
			where
				prov.PrimarySpecialtyCode in ('FP','FPG','GP','IM','IMG') and
				nwpr.NWPR_TERM_DT > getdate() and
				nwpr.NWPR_PCP_IND = 'Y' and
				nwm.PORTAL = @Portal
		) pcp
		inner join CM_PT_CMC_SpecialtyMapping sm1
			on prov.PrimarySpecialtyCode = sm1.SpecialtyMappingId
		left join CM_PT_CMC_SpecialtyMapping sm2
			on prov.SecondarySpecialtyCode = sm2.SpecialtyMappingId
		left join CM_OT_GeoZipcodeRadius gzr
			on left(prov.Zip, 5) = gzr.BaseZipCode
	where
		(Name like isnull(@SearchName, Name)) and
		(isnull(@Zip, gzr.BaseZipCode) = gzr.ToZipCode) and
		(isnull(gzr.DistanceMiles, 0.0) between 0.0 and isnull(@MaxDistance, 25.0)) and
		(
			(isnull(@Specialties, '') = '') or
			(',' + @Specialties + ',' like '%,' + PrimarySpecialtyCode + ',%') or
			(',' + @Specialties + ',' like '%,' + SecondarySpecialtyCode + ',%')
		) and
		(
			(isnull(@Languages, '') = '') or
			(',' + @Languages + ',' like '%,' + PrimaryLanguageCode + ',%') or
			([CM_PT_CMC_HasSecondaryLanguageCodes](prov.ProviderId, prov.AddressId, @Languages) > 0)
		) and
		(isnull(@City, City) = City) and
		(isnull(@County, County) = County) and
		(isnull(@State, State) = State) and
		(isnull(@IsPCP, IsPCP) = IsPCP) and
		(@Portal in
			(
			select PORTAL
			from CM_PT_CMC_NWNW_Mapping nwm
			join CMC_NWPR_RELATION nwpr
				on nwm.NWNW_ID = nwpr.NWNW_ID and
				   nwpr.PRPR_ID = prov.ProviderId
			where
				nwpr.NWPR_TERM_DT > getdate()
			)
		)
	order by
		Distance, Name
