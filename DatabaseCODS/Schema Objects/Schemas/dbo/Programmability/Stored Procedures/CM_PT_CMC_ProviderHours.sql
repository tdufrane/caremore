﻿-- Returns the hours for a single provider
create procedure [dbo].[CM_PT_CMC_ProviderHours]
(
	@ProviderId    char(12),
	@AddressId     char(12),
	@AddressType   char(3),
	@AddressDate   datetime
)
as
	select
		rtrim(PROF_DAYS) as DaysOfWeek,
		rtrim(PROF_FROM_HR1) as FromHours,
		rtrim(PROF_TO_HR1) as ToHours,
		rtrim(PROF_FROM_HR2) as FromHours2,
		rtrim(PROF_TO_HR2) as ToHours2
	from
		CMC_PROF_OFF_HRS c
	where
		(PRPR_ID   = @ProviderId) and
		(PRAD_ID   = @AddressId) and
		(PRAD_TYPE = @AddressType) and
		(PRAD_EFF_DT = @AddressDate)
	order by
		PROF_SEQ_NO
