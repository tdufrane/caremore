﻿-- Returns the last date and time providers were updated
create procedure [dbo].[CM_PT_CMC_ProvidersLastUpdated]
as

select top 1
	SYS_LAST_UPD_DTM as LastUpdatedOn
from
	CMC_PRPR_PROV
order by
	SYS_LAST_UPD_DTM desc
