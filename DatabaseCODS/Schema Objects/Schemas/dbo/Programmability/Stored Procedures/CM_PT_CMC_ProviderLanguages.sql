﻿create procedure [dbo].[CM_PT_CMC_ProviderLanguages]
(
	@ProviderId    char(12),
	@AddressId     char(12),
	@AddressType   char(3)
)
as
	select
		'P' as LanguageType,
		rtrim(prpr.PRPR_MCTR_LANG) as LanguageCode,
		rtrim(mctr.MCTR_DESC) as [Language],
		cast(0 as tinyint) as LanguageOrder
	from
		CMC_PRPR_PROV prpr
		join CMC_MCTR_CD_TRANS mctr
			on prpr.PRPR_MCTR_LANG = mctr.MCTR_VALUE and
			   mctr.MCTR_ENTITY = 'ALL' and
			   mctr.MCTR_TYPE = 'LANG'
	where
		(prpr.PRPR_ID   = @ProviderId) and
		(prpr.PRAD_ID   = @AddressId)
union
	select
		'S' as LanguageType,
		rtrim(prla.PRLA_MCTR_LANG) as LanguageCode,
		rtrim(mctr.MCTR_DESC) as [Language],
		cast(1 as tinyint) as LanguageOrder
	from
		CMC_PRPR_PROV prpr
		join CMC_PRLA_LANG prla
			on prpr.PRCR_ID = prla.PRCR_ID
		join CMC_MCTR_CD_TRANS mctr
			on prla.PRLA_MCTR_LANG = mctr.MCTR_VALUE and
			   mctr.MCTR_ENTITY = 'ALL' and
			   mctr.MCTR_TYPE = 'LANG'
	where
		(prpr.PRPR_ID   = @ProviderId) and
		(prpr.PRAD_ID   = @AddressId)
union
	select
		'O' as LanguageType,
		rtrim(pral.PRAL_MCTR_LANG) as LanguageCode,
		rtrim(mctr.MCTR_DESC) as [Language],
		cast(2 as tinyint) as LanguageOrder
	from
		CMC_PRPR_PROV prpr
		join CMC_PRAL_LANG pral
			on prpr.PRPR_ID = pral.PRPR_ID
		join CMC_MCTR_CD_TRANS mctr
			on pral.PRAL_MCTR_LANG = mctr.MCTR_VALUE and
			   mctr.MCTR_ENTITY = 'ALL' and
			   mctr.MCTR_TYPE = 'LANG'
	where
		(prpr.PRPR_ID   = @ProviderId) and
		(prpr.PRAD_ID   = @AddressId) and
		(pral.PRAD_TYPE = isnull(@AddressType, 'P'))
order by
	LanguageOrder
