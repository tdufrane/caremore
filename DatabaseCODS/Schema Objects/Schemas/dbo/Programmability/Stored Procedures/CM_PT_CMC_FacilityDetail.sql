﻿-- Returns the details for a single facility
create procedure [dbo].[CM_PT_CMC_FacilityDetail]
(
	@Portal      tinyint,
	@ProviderId  char(12),
	@AddressType char(3)
)
as
	set nocount on;

	select top 1
		ProviderId,
		ClassDescription,
		NpiId,
		AddressId,
		AddressEffectiveDate,
		Name,
		Address1,
		Address2,
		Address3,
		City,
		County,
		State,
		Zip,
		Phone,
		Fax,
		(
			select min(NWPR_EFF_DT)
			from CM_PT_CMC_NWNW_Mapping nwm
			join CMC_NWPR_RELATION nwpr
				on nwm.NWNW_ID = nwpr.NWNW_ID and
				   nwpr.PRPR_ID = prov.ProviderId
			where
				nwm.PORTAL = @Portal
		) as EffectiveDate,
		Accessibility,
		isnull(prsq.PRSQ_COMMENT, '') as Transportation,
		isnull(prcf.PRCF_STS, 'U') as CredentialingStatus
	from
		vwCMC_Facilities prov
		left join CMC_PRSQ_SITE_QA prsq
			on prov.ProviderId = prsq.PRPR_ID and
			   prov.AddressId = prsq.PRAD_ID and
			   prov.AddressType = prsq.PRAD_TYPE and
			   prsq.PRSQ_MCTR_ITEM = 'PT'
		left join CMC_PRCF_CERT prcf
			on prov.CredentialingId = prcf.PRCR_ID and
			   prov.PrimarySpecialtyCode = prcf.PRCF_MCTR_SPEC
where
		(ProviderId = @ProviderId) and
		(AddressType = isnull(@AddressType, AddressType)) and
		(@Portal in
			(
			select PORTAL
			from CM_PT_CMC_NWNW_Mapping nwm
			join CMC_NWPR_RELATION nwpr
				on nwm.NWNW_ID = nwpr.NWNW_ID and
				   nwpr.PRPR_ID = prov.ProviderId
			where
				nwpr.NWPR_TERM_DT > getdate()
			)
		)
	order by
		AddressType
