-- Synchronizes specialty mapping table with Facets
create procedure CM_PT_CMC_SpecMapSynch
as
	-- Create a temp table with all specialties in use in Facets
	create table #SpecialtyTemp
	(
		Code         varchar(4),
		Description  varchar(255)
	)

	insert into #SpecialtyTemp
	select distinct
		rtrim(prpr.PRCF_MCTR_SPEC) as Code,
		rtrim(mctr.MCTR_DESC) as Description
	from
		CMC_PRPR_PROV prpr
		join CMC_MCTR_CD_TRANS mctr
			on prpr.PRCF_MCTR_SPEC = mctr.MCTR_VALUE and
			   mctr.MCTR_ENTITY = 'PRAC' and
			   mctr.MCTR_TYPE = 'SPEC'
	where
		(prpr.PRCF_MCTR_SPEC <> '')
union
	select distinct
		rtrim(prpr.PRCF_MCTR_SPEC2) as Code,
		rtrim(mctr.MCTR_DESC) as Description
	from
		CMC_PRPR_PROV prpr
		join CMC_MCTR_CD_TRANS mctr
			on prpr.PRCF_MCTR_SPEC2 = mctr.MCTR_VALUE and
			   mctr.MCTR_ENTITY = 'PRAC' and
			   mctr.MCTR_TYPE = 'SPEC'
	where
		(prpr.PRCF_MCTR_SPEC2 <> '')


	-- Add any new specialties, set as not shown on website to force user to flag
	insert into
		CM_PT_CMC_SpecialtyMapping
	select
		Code,
		Description,
		0,
		NULL
	from
		#SpecialtyTemp
	where
		(Code not in (select SpecialtyMappingId from CM_PT_CMC_SpecialtyMapping))


	-- Delete any specialties no longer present
	delete
	from
		CM_PT_CMC_SpecialtyMapping
	where
		(SpecialtyMappingId not in (select Code from #SpecialtyTemp))

	-- Remove temp table
	drop table #SpecialtyTemp
