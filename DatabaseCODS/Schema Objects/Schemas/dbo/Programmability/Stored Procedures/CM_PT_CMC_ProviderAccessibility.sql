﻿create procedure [dbo].[CM_PT_CMC_ProviderAccessibility]
(
	@ProviderId    char(12),
	@AddressId     char(12),
	@AddressType   char(3)
)
as
	select distinct
		rtrim(PRSQ_MCTR_ITEM) as AccessibilityCode,
		rtrim(mctr.MCTR_DESC) as Accessibility,
		acce.LimitedAccessMain,
		acce.BasicAccessMain,
		acce.BasicAccessSub,
		rtrim(prsq.PRSQ_COMMENT) as Comments,
		acce.DoNotDisplay,
		acce.MappingCode
	from
		CMC_PRSQ_SITE_QA prsq
		join CMC_MCTR_CD_TRANS mctr
			on prsq.PRSQ_MCTR_ITEM = mctr.MCTR_VALUE and
			   mctr.MCTR_ENTITY = 'PRSQ' and
			   mctr.MCTR_TYPE = 'ITEM'
		join CM_PT_DUALS_Accessibility acce
			on prsq.PRSQ_MCTR_ITEM = acce.AccessibilityId
	where
		(PRPR_ID   = @ProviderId) and
		(PRAD_ID   = @AddressId) and
		(PRAD_TYPE = @AddressType)
	order by
		AccessibilityCode
