﻿-- Returns a list of providers matching one or more filters
create procedure [dbo].[CM_PT_CMC_FacilitySearch]
(
	@Portal      tinyint,
	@ClassCode   varchar(50),
	@Name        varchar(103),
	@Zip         varchar(10),
	@City        varchar(30),
	@County      varchar(20),
	@State       varchar(2),
	@MaxDistance float(53)
)
as
	declare @SearchName  varchar(105)
	declare @SearchCodes varchar(52)

	set nocount on;

	if (@Name is not null)
	begin
		select @Name = replace(@Name, '*', '%')

		if (charindex(@Name, '%', 1) > 0)
			select @SearchName = @Name
		else
			select @SearchName = '%' + @Name + '%'
	end

	if (left(@ClassCode, 1) <> ',')
		select @SearchCodes = ',' + @ClassCode
	else
		select @SearchCodes = @ClassCode

	if (right(@ClassCode, 1) <> ',')
		select @SearchCodes = @SearchCodes + ','


-- When zip code is passed in, using a different query based on a radius from that zip code
if (@Zip is null) or (@Zip = '')
	select distinct
		ProviderId,
		ClassCode,
		NpiId,
		AddressId,
		AddressType,
		AddressEffectiveDate,
		Name,
		Address1,
		Address2,
		Address3,
		City,
		County,
		State,
		Zip,
		Phone,
		cast(0.0 as float) as Distance,
		(
			select min(NWPR_EFF_DT)
			from CM_PT_CMC_NWNW_Mapping nwm
			join CMC_NWPR_RELATION nwpr on
			     nwpr.PRPR_ID = prov.ProviderId and
			     nwm.NWNW_ID = nwpr.NWNW_ID and
			     nwm.PORTAL = @Portal
		) as EffectiveDate
	from
		vwCMC_Facilities prov
	where
		(CHARINDEX(',' + ClassCode + ',', @SearchCodes) > 0) and
		(Name like isnull(@SearchName, Name)) and
		(isnull(@City, City) = City) and
		(isnull(@County, County) = County) and
		(isnull(@State, State) = State) and
		(@Portal in (select PORTAL
		             from CM_PT_CMC_NWNW_Mapping nwm
		             join CMC_NWPR_RELATION nwpr on
		                  nwpr.PRPR_ID = prov.ProviderId and
		                  nwm.NWNW_ID = nwpr.NWNW_ID
		             where nwpr.NWPR_TERM_DT > getdate()))
	order by
		Name

else
	select distinct
		ProviderId,
		ClassCode,
		NpiId,
		AddressId,
		AddressType,
		AddressEffectiveDate,
		Name,
		ClassDescription,
		Address1,
		Address2,
		Address3,
		City,
		County,
		State,
		Zip,
		Phone,
		isnull(DistanceMiles, 0.0) as Distance,
		(
			select min(NWPR_EFF_DT)
			from CM_PT_CMC_NWNW_Mapping nwm
			join CMC_NWPR_RELATION nwpr on
			     nwpr.PRPR_ID = prov.ProviderId and
			     nwm.NWNW_ID = nwpr.NWNW_ID and
			     nwm.PORTAL = @Portal
		) as EffectiveDate
	from
		vwCMC_Facilities prov
		left join CM_OT_GeoZipcodeRadius gzr
			on gzr.ToZipCode = @Zip and
			   gzr.DistanceMiles between 0.0 and isnull(@MaxDistance, 25.0) and
			   gzr.BaseZipCode = prov.Zip
	where
		(CHARINDEX(',' + ClassCode + ',', @SearchCodes) > 0) and
		(Name like isnull(@SearchName, Name)) and
		(isnull(@City, City) = City) and
		(isnull(@County, County) = County) and
		(isnull(@State, State) = State) and
		(@Portal in (select PORTAL
		             from CM_PT_CMC_NWNW_Mapping nwm
		             join CMC_NWPR_RELATION nwpr on
		                  nwpr.PRPR_ID = prov.ProviderId and
		                  nwm.NWNW_ID = nwpr.NWNW_ID
		             where nwpr.NWPR_TERM_DT > getdate())) and
		(isnull(gzr.BaseZipCode, @Zip) = left(prov.Zip, 5))
	order by
		Distance, Name
