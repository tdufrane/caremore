﻿-- Returns the addresses for a single provider
create procedure [dbo].[CM_PT_CMC_ProviderAddresses]
(
	@AddressId char(12),
	@State     varchar(255)
)
as
	select
		cast(1 as tinyint) as DisplayOrder,
		AddressType,
		AddressEffectiveDate as EffectiveDate,
		Address1,
		Address2,
		Address3,
		City,
		StateName,
		Zip,
		County,
		Phone,
		Fax,
		Accessibility
	from
		vwCMC_Providers
	where
		(ProviderId = @AddressId) and
		(StateName = @State)
union
	select
		cast(2 as tinyint) as DisplayOrder,
		AddressType,
		AddressEffectiveDate as EffectiveDate,
		Address1,
		Address2,
		Address3,
		City,
		StateName,
		Zip,
		County,
		Phone,
		Fax,
		Accessibility
	from
		vwCMC_Providers
	where
		(ProviderId = @AddressId) and
		(StateName <> @State)
order by
	DisplayOrder,
	StateName,
	County,
	City,
	AddressType
