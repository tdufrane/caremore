-- Returns a single SpecialtyMapping row based on Id
create procedure CM_PT_CMC_SpecMapItem
(
	@SpecialtyMappingId  varchar(4)
)
as
	select
		*
	from
		CM_PT_CMC_SpecialtyMapping
	where
		(SpecialtyMappingId = @SpecialtyMappingId)
