﻿create procedure [dbo].[CM_PT_CMC_AllProvidersList]
(
	@Portal      tinyint,
	@Zip         varchar(10),
	@County      varchar(20),
	@State       varchar(2),
	@MaxDistance float(53)
)
as
	select distinct
		ProviderId,
		'P' as ProviderType,
		NpiId,
		AddressId,
		AddressType,
		AddressEffectiveDate,
		Name,
		Title,
		ClassCode,
		ClassDescription,
		Address1,
		Address2,
		Address3,
		City,
		County,
		State,
		Zip,
		Phone,
		isnull(DistanceMiles, 0.0) as Distance,
		(
			select min(NWPR_EFF_DT)
			from CM_PT_CMC_NWNW_Mapping nwm
			join CMC_NWPR_RELATION nwpr on
			     nwpr.PRPR_ID = prov.ProviderId and
			     nwm.NWNW_ID = nwpr.NWNW_ID and
			     nwm.PORTAL = @Portal
		) as EffectiveDate
	from
		vwCMC_Providers prov
		inner join CM_PT_CMC_SpecialtyMapping sm1
			on prov.PrimarySpecialtyCode = sm1.SpecialtyMappingId
		left join CM_PT_CMC_SpecialtyMapping sm2
			on prov.SecondarySpecialtyCode = sm2.SpecialtyMappingId
		left join CM_OT_GeoZipcodeRadius gzr
			on gzr.ToZipCode = @Zip and
			   gzr.DistanceMiles between 0.0 and isnull(@MaxDistance, 25.0) and
			   gzr.BaseZipCode = prov.Zip
	where
		(isnull(@County, County) = County) and
		(isnull(@State, State) = State) and
		(@Portal in (select PORTAL
		             from CM_PT_CMC_NWNW_Mapping nwm
		             join CMC_NWPR_RELATION nwpr on
		                  nwpr.PRPR_ID = prov.ProviderId and
		                  nwm.NWNW_ID = nwpr.NWNW_ID
		             where nwpr.NWPR_TERM_DT > getdate())) and
		(isnull(gzr.BaseZipCode, @Zip) = left(prov.Zip, 5))
union
	select distinct
		ProviderId,
		'F' as ProviderType,
		NpiId,
		AddressId,
		AddressType,
		AddressEffectiveDate,
		Name,
		'' as Title,
		ClassCode,
		ClassDescription,
		Address1,
		Address2,
		Address3,
		City,
		County,
		State,
		Zip,
		Phone,
		isnull(DistanceMiles, 0.0) as Distance,
		(
			select min(NWPR_EFF_DT)
			from CM_PT_CMC_NWNW_Mapping nwm
			join CMC_NWPR_RELATION nwpr on
			     nwpr.PRPR_ID = prov.ProviderId and
			     nwm.NWNW_ID = nwpr.NWNW_ID and
			     nwm.PORTAL = @Portal
		) as EffectiveDate
	from
		vwCMC_Facilities prov
		left join CM_OT_GeoZipcodeRadius gzr
			on gzr.ToZipCode = @Zip and
			   gzr.DistanceMiles between 0.0 and isnull(@MaxDistance, 25.0) and
			   gzr.BaseZipCode = prov.Zip
	where
		(isnull(@County, County) = County) and
		(isnull(@State, State) = State) and
		(@Portal in (select PORTAL
		             from CM_PT_CMC_NWNW_Mapping nwm
		             join CMC_NWPR_RELATION nwpr on
		                  nwpr.PRPR_ID = prov.ProviderId and
		                  nwm.NWNW_ID = nwpr.NWNW_ID
		             where nwpr.NWPR_TERM_DT > getdate())) and
		(isnull(gzr.BaseZipCode, @Zip) = left(prov.Zip, 5))
order by
	Distance, Name
